# from multiprocessing.connection import Listener
#
# address = ('localhost', 6000)  # family is deduced to be 'AF_INET'
# listener = Listener(address, authkey=b'secret password')
# conn = listener.accept()
# print('connection accepted from', listener.last_accepted)
# while True:
#     msg = conn.recv()
#     print('msg = ', msg)
#     # do something with msg
#     if msg == 'close':
#         conn.close()
#         break
#
# listener.close()
import time

import zmq
import json

# server
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind('tcp://127.0.0.1:5555')
while True:
    msg = socket.recv()
    print('msg = ', msg)
    msg_string = msg.decode('utf-8')
    msg_json = json.loads(msg_string)
    print('msg_json = ', msg_json)
    # if msg == 'zeromq':
    #     socket.send('ah ha!')
    # else:
    #     socket.send('...nah'.encode('ascii'))
    # Delay my reply while I debug an issue
    # time.sleep(5)
    socket.send('OK'.encode('ascii'))
