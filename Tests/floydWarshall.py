# # Python Program for Floyd Warshall Algorithm
#
# # Number of vertices in the graph
# import sys
#
# from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader
#
# V = None
#
# # Define infinity as the large enough value. This value will be
# # used for vertices not connected to each other
# INF = 9999999999999
#
#
# # Solves all pair shortest path via Floyd Warshall Algorithm
# def floydWarshall(graph):
#     """ dist[][] will be the output matrix that will finally
#         have the shortest distances between every pair of vertices """
#     """ initializing the solution matrix same as input graph matrix
#     OR we can say that the initial values of shortest distances
#     are based on shortest paths considering no
#     intermediate vertices """
#
#     # dist = map(lambda i: map(lambda j: j, i), graph)
#
#     print("graph = ", graph)
#
#     # TODO, why not just do a copy?????
#     dist = graph.copy()
#
#     # dist = []
#     # for i in range(0, len(graph)):
#     #     # print("i = " + str(i))
#     #     dist.append([])
#     #
#     #     for j in range(0, len(graph[i])):
#     #         # print("j = " + str(j))
#     #
#     #         # print("dist = " + str(dist))
#     #         # print("graph[i] = " + str(graph[i]))
#     #         # print("graph[i][j] = " + str(graph[i][j]))
#     #
#     #         dist[i].append(graph[i][j])
#     #
#
#     print("dist = ", dist)
#
#     #         int[,] dist = new int[V, V];
#     #         int i, j, k;
#     #
#     #         // Initialize the solution matrix
#     #         // same as input graph matrix
#     #         // Or we can say the initial
#     #         // values of shortest distances
#     #         // are based on shortest paths
#     #         // considering no intermediate
#     #         // vertex
#     #         for (i = 0; i < V; i++) {
#     #             for (j = 0; j < V; j++) {
#     #                 dist[i, j] = graph[i, j];
#     #             }
#     #         }
#
#     """ Add all vertices one by one to the set of intermediate
#     vertices.
#     ---> Before start of an iteration, we have shortest distances
#     between all pairs of vertices such that the shortest
#     distances consider only the vertices in the set
#     {0, 1, 2, .. k-1} as intermediate vertices.
#     ----> After the end of a iteration, vertex no. k is
#     added to the set of intermediate vertices and the
#     set becomes {0, 1, 2, .. k}
#     """
#     for k in range(V):
#         PrintAndLog_FuncNameHeader("k = " + str(k))
#         # pick all vertices as source one by one
#         for i in range(V):
#             PrintAndLog_FuncNameHeader("i = " + str(i))
#             # Pick all vertices as destination for the
#             # above picked source
#             for j in range(V):
#                 PrintAndLog_FuncNameHeader("j = " + str(j))
#                 # If vertex k is on the shortest path from
#                 # i to j, then update the value of dist[i][j]
#                 PrintAndLog_FuncNameHeader("dist = " + str(dist))
#                 thing1 = dist[i][j]
#                 thing2 = dist[i][k]
#                 thing3 = dist[k][j]
#                 dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j])
#
#     printSolution(dist)
#
#
# # A utility function to print the solution
# def printSolution(dist):
#     print("Following matrix shows the shortest distances between every pair of vertices")
#     for i in range(V):
#         for j in range(V):
#             PrintAndLog_FuncNameHeader(str(dist[i][j]))
#             # if dist[i][j] == INF:
#             #     print("%7s" % ("INF"))
#             # else:
#             #     print("%7d\t" % (dist[i][j]))
#             if j == V - 1:
#                 # print("")
#                 PrintAndLog_FuncNameHeader("")
#
#
# # Driver program to test the above program
# # Let us create the following weighted graph
# graph = [
#     [0, 5, INF, 10],
#     [INF, 0, 3, INF],
#     [INF, INF, 0, 1],
#     [INF, INF, INF, 0]
# ]
# V = len(graph)
# PrintAndLog_FuncNameHeader("graph = " + str(graph))
# PrintAndLog_FuncNameHeader("V = " + str(V))
# # Print the solution
# floydWarshall(graph)
# # This code is contributed by Nikhil Kumar Singh(nickzuck_007)


# Floyd Warshall Algorithm in python


# The number of vertices
# nV = 4

INF = 99999999999999


# Algorithm implementation
def floyd_warshall(G):
    nV = len(G)
    distance = list(map(lambda i: list(map(lambda j: j, i)), G))

    # Adding vertices individually
    for k in range(nV):
        for i in range(nV):
            for j in range(nV):
                distance[i][j] = min(distance[i][j], distance[i][k] + distance[k][j])
    print_solution(distance, nV)


# Printing the solution
def print_solution(distance, nV):
    for i in range(nV):
        for j in range(nV):
            if distance[i][j] == INF:
                print("INF", end=" ")
            else:
                print(distance[i][j], end="  ")
        print(" ")

# G = [[0, 3, INF, 5],
#      [2, 0, INF, 4],
#      [INF, 1, 0, INF],
#      [INF, INF, 2, 0]]
# floyd_warshall(G)
