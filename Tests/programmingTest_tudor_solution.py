from typing import Tuple, List
from math import log

# rates = [[1, 0.050050050050050046, 0.002597672485453034], [20.975, 1, 0.05455], [394.9, 19.76284584980237, 1]]

myPythonDictionary = {
    'exchanges': [
        {
            'name': 'Coinbase',
            'prices': [
                {
                    'quoteToken': 'usdc',
                    'baseToken': 'eth',
                    'bid': 384.95,
                    'ask': 384.96,
                },
                {
                    'quoteToken': 'eth',
                    'baseToken': 'rep',
                    'bid': 0.05455,
                    'ask': 0.05456,
                },
                {
                    'quoteToken': 'usdc',
                    'baseToken': 'rep',
                    'bid': 20.975,
                    'ask': 20.976,
                },
            ]
        },
        {
            'name': 'Creepy white van',
            'prices': [
                {
                    'quoteToken': 'usdc',
                    'baseToken': 'eth',
                    'bid': 384.9,
                    'ask': 385,
                },
                {
                    'quoteToken': 'eth',
                    'baseToken': 'rep',
                    'bid': 0.0505,
                    'ask': 0.0506,
                },
                {
                    'quoteToken': 'usdc',
                    'baseToken': 'rep',
                    'bid': 20.97,
                    'ask': 20.98,
                },
            ]
        },
        {
            'name': 'Sketchy dude at the bar',
            'prices': [
                {
                    'quoteToken': 'usdc',
                    'baseToken': 'eth',
                    'bid': 384.9,
                    'ask': 385,
                },
                {
                    'quoteToken': 'eth',
                    'baseToken': 'rep',
                    'bid': 0.0545,
                    'ask': 0.0546,
                },
                {
                    'quoteToken': 'usdc',
                    'baseToken': 'rep',
                    'bid': 19.97,
                    'ask': 19.98,
                },
            ]
        },
        {
            'name': 'Facebook market place',
            'prices': [
                {
                    'quoteToken': 'usdc',
                    'baseToken': 'eth',
                    'bid': 394.9,
                    'ask': 395,
                },
                {
                    'quoteToken': 'eth',
                    'baseToken': 'rep',
                    'bid': 0.0545,
                    'ask': 0.0546,
                },
                {
                    'quoteToken': 'usdc',
                    'baseToken': 'rep',
                    'bid': 20.97,
                    'ask': 20.98,
                },
            ]
        },
    ]
}

currencies = ('usdc', 'rep', 'eth')


def negate_logarithm_convertor(graph: Tuple[Tuple[float]]) -> List[List[float]]:
    ''' log of each rate in graph and negate it'''
    result = [[-log(edge) for edge in row] for row in graph]
    return result


def arbitrage(currency_tuple: tuple, rates_matrix: Tuple[Tuple[float, ...]]):
    ''' Calculates arbitrage situations and prints out the details of this calculations'''

    trans_graph = negate_logarithm_convertor(rates_matrix)

    # Pick any source vertex -- we can run Bellman-Ford from any vertex and get the right result

    source = 0
    n = len(trans_graph)
    min_dist = [float('inf')] * n

    pre = [-1] * n

    min_dist[source] = source

    # 'Relax edges |V-1| times'
    for _ in range(n - 1):
        for source_curr in range(n):
            for dest_curr in range(n):
                if min_dist[dest_curr] > min_dist[source_curr] + trans_graph[source_curr][dest_curr]:
                    min_dist[dest_curr] = min_dist[source_curr] + trans_graph[source_curr][dest_curr]
                    pre[dest_curr] = source_curr

    paths = []

    # if we can still relax edges, then we have a negative cycle
    for source_curr in range(n):
        for dest_curr in range(n):
            if min_dist[dest_curr] > min_dist[source_curr] + trans_graph[source_curr][dest_curr]:
                # negative cycle exists, and use the predecessor chain to print the cycle
                print_cycle = [dest_curr, source_curr]
                # Start from the source and go backwards until you see the source vertex again or any vertex that already exists in print_cycle array
                while pre[source_curr] not in print_cycle:
                    print_cycle.append(pre[source_curr])
                    source_curr = pre[source_curr]
                print_cycle.append(pre[source_curr])
                print(print_cycle)

                profitability = 1
                for i in range(1, len(print_cycle)):
                    profitability = profitability * rates_matrix[print_cycle[i - 1]][print_cycle[i]]
                print(profitability)

                print("Arbitrage Opportunity:")
                print(" --> ".join([currencies[p] for p in print_cycle[::-1]]))

                if (profitability > 1):
                    paths.append([profitability] + print_cycle)

                print()

    print()
    paths = sorted(paths, key=lambda x: -x[0])
    print('Profitable arbitrage paths:', paths)

    print('details:')
    for path in paths:
        print('profitability: {0:.2f}%'.format(100 * (path[0] - 1)))
        for path_step in range(1, len(path) - 1):
            print('{0} -> {1}'.format(currencies[path[path_step]], currencies[path[path_step + 1]]))
        print()


def compute_rates():
    rates = [[1 for _ in range(len(currencies))] for _ in range(len(currencies))]
    currencies_index = {}
    for i, currency in enumerate(currencies):
        currencies_index[currency] = i
    print(currencies_index)

    for exchangeDict in myPythonDictionary['exchanges']:
        for priceDict in exchangeDict['prices']:
            quote_token = priceDict['quoteToken']
            base_token = priceDict['baseToken']
            bid = priceDict['bid']
            ask = priceDict['ask']

            # selling from the bid
            if rates[currencies_index[base_token]][currencies_index[quote_token]] == 1:
                rates[currencies_index[base_token]][currencies_index[quote_token]] = bid
            else:
                rates[currencies_index[base_token]][currencies_index[quote_token]] = max(bid, rates[currencies_index[base_token]][currencies_index[quote_token]])

            # buying from the ask
            if (rates[currencies_index[quote_token]][currencies_index[base_token]]) == 1:
                rates[currencies_index[quote_token]][currencies_index[base_token]] = 1 / ask
            else:
                rates[currencies_index[quote_token]][currencies_index[base_token]] = max(1 / ask, rates[currencies_index[quote_token]][currencies_index[base_token]])

    return rates


if __name__ == "__main__":
    rates = compute_rates()
    print('rates = ', rates)

    arbitrage(currencies, rates)

# Time Complexity: O(N^3)
# Space Complexity: O(N^2)
