import networkx as nx

from Libraries.loggingConfig import PrintAndLog

# from .bellmannx import NegativeWeightFinder
# from .utils import get_least_edge_in_bunch

__all__ = [
    'NegativeWeightFinderMulti',
    'bellman_ford_multi',
]


class NegativeWeightFinder:
    __slots__ = ['graph', 'predecessor_to', 'distance_to', 'seen_nodes']

    def __init__(self, graph: nx.Graph):
        self.graph = graph
        self.predecessor_to = {}
        # the maximum weight which can be transferred from source to each node
        self.distance_to = {}

        self.seen_nodes = set()

    def reset_all_but_graph(self):
        """
        Call this to look for opportunities after updating the graph
        """
        self.predecessor_to = {}
        self.distance_to = {}

        self.seen_nodes = set()

    def initialize(self, source):
        for node in self.graph:
            # Initialize all distance_to values to infinity and all predecessor_to values to None
            self.distance_to[node] = float('Inf')
            self.predecessor_to[node] = None

        # The distance from any node to (itself) == 0
        self.distance_to[source] = 0

    def bellman_ford(self, source='BTC', unique_paths=True):
        """
        Finds arbitrage opportunities in self.graph and yields them
        Parameters
        ----------
        source
            A node (currency) in self.graph. Opportunities will be yielded only if they are "reachable" from source.
            Reachable means that a series of trades can be executed to buy one of the currencies in the opportunity.
            For the most part, it does not matter what the value of source is, because typically any currency can be
            reached from any other via only a few trades.
        unique_paths : bool
            unique_paths: If True, each opportunity is not yielded more than once
        :return: a generator of profitable (negatively-weighted) arbitrage paths in self.graph
        """
        PrintAndLog('Running bellman_ford')
        self.initialize(source)

        PrintAndLog('Relaxing edges')
        # After len(graph) - 1 passes, algorithm is complete.
        for i in range(len(self.graph) - 1):
            # for each node in the graph, test if the distance to each of its siblings is shorter by going from
            # source->base_currency + base_currency->quote_currency
            for edge in self.graph.edges(data=True):
                self.relax(edge)
        PrintAndLog('Finished relaxing edges')

        for edge in self.graph.edges(data=True):
            if self.distance_to[edge[0]] + edge[2]['weight'] < self.distance_to[edge[1]]:
                if unique_paths and edge[1] in self.seen_nodes:
                    continue
                path = self._retrace_negative_cycle(edge[1], unique_paths)
                if path is None or path == (None, None):
                    continue
                yield path

        PrintAndLog('Ran bellman_ford')

    def relax(self, edge):
        if self.distance_to[edge[0]] + edge[2]['weight'] < self.distance_to[edge[1]]:
            self.distance_to[edge[1]] = self.distance_to[edge[0]] + edge[2]['weight']
            self.predecessor_to[edge[1]] = edge[0]

        return True

    def _retrace_negative_cycle(self, start, unique_paths):
        """
        Retraces an arbitrage opportunity (negative cycle) which a currency can reach and returns it.
        Parameters
        ----------
        start
            A node (currency) from which it is known an arbitrage opportunity is reachable
        unique_paths : bool
            unique_paths: If True, no duplicate opportunities are returned
        Returns
        -------
        list
            An arbitrage opportunity reachable from start. Value is None if seen_nodes is True and a
            duplicate opportunity would be returned.
        """
        arbitrage_loop = [start]
        prior_node = start
        while True:
            prior_node = self.predecessor_to[prior_node]
            # if negative cycle is complete
            if prior_node in arbitrage_loop:
                arbitrage_loop = arbitrage_loop[:last_index_in_list(arbitrage_loop, prior_node) + 1]
                arbitrage_loop.insert(0, prior_node)
                return arbitrage_loop

            # because if prior_node is in arbitrage_loop prior_node must be in self.seen_nodes. thus, this conditional
            # must proceed checking if prior_node is in arbitrage_loop
            if unique_paths and prior_node in self.seen_nodes:
                return None

            arbitrage_loop.insert(0, prior_node)
            self.seen_nodes.add(prior_node)


class NegativeWeightFinderMulti(NegativeWeightFinder):

    def __init__(self, graph: nx.MultiGraph):
        super(NegativeWeightFinderMulti, self).__init__(graph)
        self.new_graph = nx.DiGraph()

    def bellman_ford(self, source='BTC', unique_paths=True):
        self.initialize(source)

        # on first iteration, load market prices.
        self._first_iteration()

        # After len(graph) - 1 passes, algorithm is complete.
        for i in range(1, len(self.graph) - 1):
            for edge in self.new_graph.edges(data=True):
                self.relax(edge)

        for edge in self.new_graph.edges(data=True):
            # todo: does this indicate that there is a negative cycle beginning and ending with edge[1]? or just that
            # edge[1] connects to a negative cycle?
            if self.distance_to[edge[0]] + edge[2]['weight'] < self.distance_to[edge[1]]:
                path = yield self._retrace_negative_cycle(edge[1], unique_paths=unique_paths)
                if path is None or path is (None, None):
                    continue
                yield path

    def _first_iteration(self):
        """
        On the first iteration, finds the least-weighted edge between in each edge bunch in self.graph and creates
        a DiGraph, self.new_graph using those least-weighted edges. Also completes the first relaxation iteration. This
        is why in bellman_ford, there are only len(self.graph) - 1 iterations of relaxing the edges. (The first
        iteration is completed in the method.)
        """
        [self._process_edge_bunch(edge_bunch) for edge_bunch in self.graph.edge_bunches(data=True)]

    def _process_edge_bunch(self, edge_bunch):
        ideal_edge = get_least_edge_in_bunch(edge_bunch)
        # todo: does this ever happen? if so, the least weighted edge in edge_bunch would have to be of infinite weight
        if ideal_edge['weight'] == float('Inf'):
            return

        self.new_graph.add_edge(edge_bunch[0], edge_bunch[1], **ideal_edge)

        # todo: these conditionals are rarely both true. how to detect when this is the case?
        if self.distance_to[edge_bunch[0]] + ideal_edge['weight'] < self.distance_to[edge_bunch[1]]:
            self.distance_to[edge_bunch[1]] = self.distance_to[edge_bunch[0]] + ideal_edge['weight']
            self.predecessor_to[edge_bunch[1]] = edge_bunch[0]


def bellman_ford_multi(graph: nx.MultiGraph, source, unique_paths=True):
    """
    Returns a 2-tuple containing the graph with most negative weights in every edge bunch and a generator which iterates
    over the negative cycle in graph
    """
    finder = NegativeWeightFinderMulti(graph)
    paths = finder.bellman_ford(source, unique_paths)
    return finder.new_graph, paths


def get_greatest_edge_in_bunch(edge_bunch, weight='weight'):
    """
    Edge bunch must be of the format (u, v, d) where u and v are the tail and head nodes (respectively) and d is a list
    of dicts holding the edge_data for each edge in the bunch
    Not optimized because currently the only place that calls it first checks len(edge_bunch[2]) > 0
    todo: could take only edge_bunch[2] as parameter
    todo: not needed for this project: could put in wardbradt/networkx
    """
    if len(edge_bunch[2]) == 0:
        raise ValueError("Edge bunch must contain more than one edge.")
    greatest = {weight: -float('Inf')}
    for data in edge_bunch[2]:
        if data[weight] > greatest[weight]:
            greatest = data

    return greatest


def get_least_edge_in_bunch(edge_bunch, weight='weight'):
    """
    Edge bunch must be of the format (u, v, d) where u and v are the tail and head nodes (respectively) and d is a list
    of dicts holding the edge_data for each edge in the bunch
    todo: add this to some sort of utils file/ module in wardbradt/networkx
    """
    if len(edge_bunch[2]) == 0:
        raise ValueError("Edge bunch must contain more than one edge.")

    least = {weight: float('Inf')}
    for data in edge_bunch[2]:
        if data[weight] < least[weight]:
            least = data

    return least


def last_index_in_list(li: list, element):
    """
    Thanks to https://stackoverflow.com/questions/6890170/how-to-find-the-last-occurrence-of-an-item-in-a-python-list
    """
    return len(li) - next(i for i, v in enumerate(reversed(li), 1) if v == element)


# graph = create_weighted_multi_exchange_digraph(['bittrex', 'gemini', 'kraken'], log=True)

graph = [[1, 398.40637450199205, 396.8253968253968], [0.00252, 1, 0.9990009990009991], [0.00251, 0.999, 1]]

graph, paths = bellman_ford_multi(graph, 'ETH')
print("paths = ", paths)
for path in paths:
    print("path = ", path)
#     print_profit_opportunity_for_path_multi(graph, path)
