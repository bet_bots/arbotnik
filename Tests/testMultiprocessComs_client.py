# from multiprocessing.connection import Client
#
# address = ('localhost', 6000)
# conn = Client(address, authkey=b'secret password')
# # conn.send('close')
# # can also send arbitrary objects:
# # conn.send(['a', 2.5, None, int, sum])
# conn.send("Hello")
# conn.close()
import datetime
import threading
import time

import zmq
import json

# client

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('tcp://127.0.0.1:5555')
# socket.send("woot".encode('ascii'))

Lock_ZmqClient = threading.Lock()

# graph = [[1, 2, 3], [1, 2, 3], [1, 2, 3]]

testDict = {'quoteToken': '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', 'blockNumber': 10911766, 'resultId': '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee_10911766_90311102747181',
            'Libraries.ratesGraph.RatesGraph_Dict__Process_QuoteToken': {
                '0': [[1, 342.388167546248, None, 345.43178867878106, 0.03310515356110365], [0.003053086542503955, 1, None, None, None], [None, None, 1, None, None],
                      [0.0029534552222662295, None, None, 1, None], [30.561690457155215, None, None, None, 1]]}, 'Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames__Process_QuoteToken': {
        '0': [[None, 'Balancer', None, 'Balancer', 'Balancer'], ['Balancer', None, None, None, None], [None, None, None, None, None], ['Balancer', None, None, None, None],
              ['UniswapV2', None, None, None, None]]}, 'Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens__Process_QuoteToken': {
        '0': [[None, '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', None, '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'],
              ['0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', None, None, None, None], [None, None, None, None, None],
              ['0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', None, None, None, None], ['0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', None, None, None, None]]}}


def SendMessage():
    global Lock_ZmqClient

    msg = None
    before = datetime.datetime.now()
    Lock_ZmqClient.acquire()
    try:
        duration_s = (datetime.datetime.now() - before).total_seconds()
        print("I had to wait " + str(duration_s) + " seconds for the Lock_ZmqClient. Now I can finally make my call")
        socket.send(json.dumps(testDict).encode('ascii'))
        msg = socket.recv()

    finally:
        Lock_ZmqClient.release()

    print(msg)


# socket.send(json.dumps(testDict).encode('ascii'))
# msg = socket.recv()
# print(msg)
# time.sleep(5)
#
# socket.send(json.dumps(testDict).encode('ascii'))
# msg = socket.recv()
# print(msg)
# time.sleep(5)
#
# socket.send(json.dumps(testDict).encode('ascii'))
# msg = socket.recv()
# print(msg)
# time.sleep(5)

t = threading.Thread(target=SendMessage, args=())
t.start()

t = threading.Thread(target=SendMessage, args=())
t.start()

t = threading.Thread(target=SendMessage, args=())
t.start()

time.sleep(100)
