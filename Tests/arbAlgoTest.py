from typing import Tuple, List
from math import log

from Libraries.loggingConfig import PrintAndLog


class GraphLabel:
    exchangeName = None
    token = None

    def __init__(self, _exchangeName, _token):
        self.exchangeName = _exchangeName
        self.token = _token

    def GetName(self):
        return self.exchangeName + '-' + self.token


class GraphEntry:
    graphLabel_A = None
    graphLabel_B = None
    price = None

    def __init__(self, _graphLabel_A, _graphLabel_B):
        self.graphLabel_A = _graphLabel_A
        self.graphLabel_B = _graphLabel_B

    def GetName(self):
        return self.graphLabel_A.GetName() + '-' + self.graphLabel_B.GetName()

    def SetPrice(self, _price):
        self.price = _price


def Arbitrage_BellmanFord(graph):
    transformed_graph = [[-log(edge) for edge in row] for row in graph]

    # Pick any source vertex -- we can run Bellman-Ford from any vertex and
    # get the right result
    source = 0
    n = len(transformed_graph)
    min_dist = [float('inf')] * n

    min_dist[source] = 0

    # Relax edges |V - 1| times
    for i in range(n - 1):
        for v in range(n):
            for w in range(n):
                if min_dist[w] > min_dist[v] + transformed_graph[v][w]:
                    min_dist[w] = min_dist[v] + transformed_graph[v][w]

    # If we can still relax edges, then we have a negative cycle
    for v in range(n):
        for w in range(n):
            if min_dist[w] > min_dist[v] + transformed_graph[v][w]:
                return True

    return False


def Arbitrage_FloydWarshall(graph):
    PrintAndLog("graph before = " + str(graph))
    if len(graph) < 2:
        return False

    for k in range(0, len(graph)):
        # for (int k = 0; k < g.length; k++)
        for i in range(0, len(graph)):
            # for (int i = 0; i < g.length; i++)
            for j in range(0, len(graph)):
                # for (int j = 0; j < g.length; j++)
                d = graph[i][k] * graph[k][j]
                if graph[i][j] < d:
                    graph[i][j] = d

    PrintAndLog("graph after = " + str(graph))
    return graph[0][0] > 1


def negate_logarithm_convertor(graph: Tuple[Tuple[float]]) -> List[List[float]]:
    ''' log of each rate in graph and negate it'''
    result = [[-log(edge) for edge in row] for row in graph]
    return result


# def Arbitrage_NewTest(currency_tuple: tuple, rates_matrix: Tuple[Tuple[float, ...]]):
#     ''' Calculates arbitrage situations and prints out the details of this calculations'''
#
#     trans_graph = negate_logarithm_convertor(rates_matrix)
#
#     # Pick any source vertex -- we can run Bellman-Ford from any vertex and get the right result
#
#     source = 0
#     n = len(trans_graph)
#     min_dist = [float('inf')] * n
#
#     pre = [-1] * n
#
#     min_dist[source] = source
#
#     # 'Relax edges |V-1| times'
#     for _ in range(n - 1):
#         for source_curr in range(n):
#             for dest_curr in range(n):
#                 if min_dist[dest_curr] > min_dist[source_curr] + trans_graph[source_curr][dest_curr]:
#                     min_dist[dest_curr] = min_dist[source_curr] + trans_graph[source_curr][dest_curr]
#                     pre[dest_curr] = source_curr
#
#     # if we can still relax edges, then we have a negative cycle
#     for source_curr in range(n):
#         for dest_curr in range(n):
#             if min_dist[dest_curr] > min_dist[source_curr] + trans_graph[source_curr][dest_curr]:
#                 # negative cycle exists, and use the predecessor chain to print the cycle
#                 print_cycle = [dest_curr, source_curr]
#                 # Start from the source and go backwards until you see the source vertex again or any vertex that already exists in print_cycle array
#                 while pre[source_curr] not in print_cycle:
#                     print_cycle.append(pre[source_curr])
#                     source_curr = pre[source_curr]
#                 print_cycle.append(pre[source_curr])
#                 print("Arbitrage Opportunity: \n")
#                 print(" --> ".join([currency_tuple[p] for p in print_cycle[::-1]]))
#
# # Time Complexity: O(N^3)
# # Space Complexity: O(N^2)


def negate_logarithm_convertor(graph: Tuple[Tuple[float]]) -> List[List[float]]:
    ''' log of each rate in graph and negate it'''
    result = [[-log(edge) for edge in row] for row in graph]
    return result


def Arbitrage_NewTest(currencies: tuple, rates_matrix: Tuple[Tuple[float, ...]]):
    ''' Calculates arbitrage situations and prints out the details of this calculations'''

    trans_graph = negate_logarithm_convertor(rates_matrix)

    # Pick any source vertex -- we can run Bellman-Ford from any vertex and get the right result

    source = 0
    n = len(trans_graph)
    min_dist = [float('inf')] * n

    pre = [-1] * n

    min_dist[source] = source

    # 'Relax edges |V-1| times'
    for _ in range(n - 1):
        for source_curr in range(n):
            for dest_curr in range(n):
                if min_dist[dest_curr] > min_dist[source_curr] + trans_graph[source_curr][dest_curr]:
                    min_dist[dest_curr] = min_dist[source_curr] + trans_graph[source_curr][dest_curr]
                    pre[dest_curr] = source_curr

    paths = []

    # if we can still relax edges, then we have a negative cycle
    for source_curr in range(n):
        for dest_curr in range(n):
            if min_dist[dest_curr] > min_dist[source_curr] + trans_graph[source_curr][dest_curr]:
                # negative cycle exists, and use the predecessor chain to print the cycle
                print_cycle = [dest_curr, source_curr]
                # Start from the source and go backwards until you see the source vertex again or any vertex that already exists in print_cycle array
                while pre[source_curr] not in print_cycle:
                    print_cycle.append(pre[source_curr])
                    source_curr = pre[source_curr]
                print_cycle.append(pre[source_curr])
                print(print_cycle)

                profitability = 1
                for i in range(1, len(print_cycle)):
                    profitability = profitability * rates_matrix[print_cycle[i - 1]][print_cycle[i]]
                print(profitability)

                print("Arbitrage Opportunity:")
                print(" --> ".join([currencies[p] for p in print_cycle[::-1]]))

                PrintAndLog("   print_cycle = " + str(print_cycle))
                for i in range(1, len(print_cycle)):
                    PrintAndLog("      thingy = " + str(rates_matrix[print_cycle[i - 1]][print_cycle[i]]))

                PrintAndLog("   profitability = " + str(profitability))

                actualProfitability = 1.0 - profitability
                PrintAndLog("   actualProfitability = " + str(actualProfitability))

                if actualProfitability > 0.0:
                    paths.append([actualProfitability] + print_cycle)

                print()

    print()
    paths = sorted(paths, key=lambda x: -x[0])
    print('Profitable arbitrage paths:', paths)

    print('details:')
    for path in paths:
        print('profitability: {0:.2f}%'.format(100 * (path[0])))
        for path_step in range(1, len(path) - 1):
            print('{0} -> {1}'.format(currencies[path[path_step]], currencies[path[path_step + 1]]))
        print()


#
# def negate_logarithm_convertor(graph: Tuple[Tuple[float]]) -> List[List[float]]:
#     ''' log of each rate in graph and negate it'''
#     result = [[-log(edge) for edge in row] for row in graph]
#     return result
#
#
# def Arbitrage_NewTest(currencies: tuple, rates_matrix: Tuple[Tuple[float, ...]]):
#     ''' Calculates arbitrage situations and prints out the details of this calculations'''
#
#     trans_graph = negate_logarithm_convertor(rates_matrix)
#
#     # Pick any source vertex -- we can run Bellman-Ford from any vertex and get the right result
#
#     source = 0
#     n = len(trans_graph)
#     min_dist = [float('inf')] * n
#
#     pre = [-1] * n
#
#     min_dist[source] = source
#
#     # 'Relax edges |V-1| times'
#     for _ in range(n - 1):
#         for source_curr in range(n):
#             for dest_curr in range(n):
#                 if min_dist[dest_curr] > min_dist[source_curr] + trans_graph[source_curr][dest_curr]:
#                     min_dist[dest_curr] = min_dist[source_curr] + trans_graph[source_curr][dest_curr]
#                     pre[dest_curr] = source_curr
#
#     # if we can still relax edges, then we have a negative cycle
#     for source_curr in range(n):
#         for dest_curr in range(n):
#             if min_dist[dest_curr] > min_dist[source_curr] + trans_graph[source_curr][dest_curr]:
#                 # negative cycle exists, and use the predecessor chain to print the cycle
#                 print_cycle = [dest_curr, source_curr]
#                 # Start from the source and go backwards until you see the source vertex again or any vertex that already exists in print_cycle array
#                 while pre[source_curr] not in print_cycle:
#                     print_cycle.append(pre[source_curr])
#                     source_curr = pre[source_curr]
#                 print_cycle.append(pre[source_curr])
#
#                 profitability = 1
#                 for i in range(1, len(print_cycle)):
#                     profitability = profitability * rates_matrix[print_cycle[i - 1]][print_cycle[i]]
#
#                 # print("Arbitrage Opportunity: \n")
#                 # print(" --> ".join([currencies[p] for p in print_cycle[::-1]]))
#
#                 print("Arbitrage Opportunity: \n")
#                 print(" --> ".join([currencies[p] for p in print_cycle[::-1]]))
#
#                 print("   profitability = ", profitability)
#
#                 PrintAndLog("   print_cycle = " + str(print_cycle))
#                 for i in range(1, len(print_cycle)):
#                     PrintAndLog("      thingy = " + str(rates_matrix[print_cycle[i - 1]][print_cycle[i]]))


# https://www.dailycodingproblem.com/blog/how-to-find-arbitrage-opportunities-in-python/
def arbitrage(graph):
    transformed_graph = [[-log(edge) for edge in row] for row in graph]

    # Pick any source vertex -- we can run Bellman-Ford from any vertex and
    # get the right result
    source = 0
    n = len(transformed_graph)
    min_dist = [float('inf')] * n

    min_dist[source] = 0

    # Relax edges |V - 1| times
    for i in range(n - 1):
        for v in range(n):
            for w in range(n):
                if min_dist[w] > min_dist[v] + transformed_graph[v][w]:
                    min_dist[w] = min_dist[v] + transformed_graph[v][w]

    # If we can still relax edges, then we have a negative cycle
    for v in range(n):
        for w in range(n):
            thingy = min_dist[w] - (min_dist[v] + transformed_graph[v][w])
            PrintAndLog("min_dist[w] = " + str(min_dist[w]))
            PrintAndLog("min_dist[v] + transformed_graph[v][w] = " + str(min_dist[v] + transformed_graph[v][w]))
            PrintAndLog("thingy = " + str(thingy) + ", is this profit percentage??")
            if min_dist[w] > min_dist[v] + transformed_graph[v][w]:
                PrintAndLog("Arbitrage found")
                return True

    return False
