# import json, sys, math
# import networkx as nx
#
#
# def parse_point(t):
#     tn = t[0].split("_")
#     return (tn[0], tn[1], -1.0 * math.log(float(t[1])))
#
#
# def build_graph(parsed_points):
#     dg = nx.DiGraph()
#     dg.add_weighted_edges_from(parsed_points)
#     return dg
#
#
# def find_path(digraph, start="USD", target="USD"):
#     # path = nx.bellman_ford(digraph, start, return_negative_cycle=True)
#     path = nx.bellman_ford_path(digraph, start, target)
#     return path
#
#
# def output_path(path, g, start="USD"):
#     visited = set(start)
#     tot = 1.0
#     pred = path[start]
#     x = start
#     while pred not in visited:
#         print(pred, "-->", x, math.exp(-g[pred][x]['weight']))
#         tot *= math.exp(-g[pred][x]['weight'])
#         visited.add(pred)
#         x = pred
#         pred = path[pred]
#
#     tot *= math.exp(-g[start][x]['weight'])
#     print(start, "-->", x, math.exp(-g[start][x]['weight']))
#     print("Total:", tot)
#     if tot < 1.0:
#         print("Note: no arbitrage opportunity detected.")
#
#
# def main():
#     # obj = {"USD_JPY": "95.2187140", "USD_USD": "1.0000000", "JPY_EUR": "0.0081233", "BTC_USD": "105.8268898", "JPY_BTC": "0.0000868", "USD_EUR": "0.7229295", "EUR_USD": "1.4406794",
#     #        "EUR_JPY": "147.6091807", "JPY_USD": "0.0104426", "BTC_BTC": "1.0000000", "EUR_BTC": "0.0126134", "BTC_JPY": "10833.0981089", "JPY_JPY": "1.0000000", "BTC_EUR": "78.0606009",
#     #        "EUR_EUR": "1.0000000", "USD_BTC": "0.0077152"}
#     # print("obj = ", obj)
#     # parsed_points = map(parse_point, obj.items())
#     # print("parsed_points = ", parsed_points)
#
#     parsed_points = [[1, 0.00261, 0.00282, 0.051], [398.40637450199205, 1, 1.0, 20.1], [367.6470588235294, 1.0, 1, 1e-28], [18.51851851851852, 0.05, 1e-28, 1]]
#
#     dg = build_graph(parsed_points)
#     print("dg = ", dg)
#     path = find_path(dg)
#     output_path(path, dg)
#
#
# if __name__ == "__main__":
#     main()


import networkx as nx

G = nx.DiGraph()
G.add_weighted_edges_from([(1, 2, -50), (2, 3, 40), (3, 4, -50), (4, 5, -90), (1, 6, -105)])

# G.add_weighted_edges_from([(1, 0.00261, 0.00282, 0.051), (398.40637450199205, 1, 1.0, 20.1), (367.6470588235294, 1.0, 1, 1), (18.51851851851852, 0.05, 1, 1)])


def func(graph, source, target, start_weight):
    total = start_weight
    path = []
    p = nx.bellman_ford_path(graph, source, target)
    for u, v in zip(p, p[1:]):
        total += G[u][v]['weight']
        path.append(u)
        if total < 0:
            return path
    else:
        return path


print(func(G, 1, 5, 100))
# [1, 2, 3, 4]

print(func(G, 1, 6, 100))
# [1]
