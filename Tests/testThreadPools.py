# import datetime
# import threading
# from multiprocessing.dummy import Pool as ThreadPool
#
# pool = ThreadPool(100)
#
# JoeyZThreadPool = []
# for i in range(0, 100):
#     JoeyZThreadPool.append(threading.Thread())
#
# JoeyZThreadPoolIndex = 0
#
#
# def GetThreadFromJoeyZThreadPool(target, args):
#     global JoeyZThreadPool
#     global JoeyZThreadPoolIndex
#
#     t = JoeyZThreadPool[JoeyZThreadPoolIndex]
#     # increment the index
#     JoeyZThreadPoolIndex += 1
#
#     t.target = target
#     t.args = args
#     return t
#
#
# def squareNumber(n):
#     print('ere')
#     return n ** 2
#
#
# def squareNumber2(n, resultDict, key):
#     print('ere')
#     resultDict[key] = n ** 2
#
#
# # function to be mapped over
# def calculateParallel(numbers):
#     # Completed with duration_s = 0.104364 seconds
#     global pool
#
#     results = pool.map(squareNumber, numbers)
#     pool.close()
#     pool.join()
#     return results
#
#
# def calculateCreatingNewThreads(numbers):
#     # Completed with duration_s = 0.000508 seconds
#     threads = []
#     resultDict = {}
#     for number in range(0, len(numbers)):
#         t = threading.Thread(target=squareNumber2, args=(number, resultDict, number))
#         threads.append(t)
#         t.start()
#
#     for thread in threads:
#         thread.join()
#
#     return list(resultDict.values())
#
#
# def calculateCreatingNewThreads_JoeyZThreadPool(numbers):
#     # Completed with duration_s = 0.000508 seconds
#     threads = []
#     resultDict = {}
#     for number in range(0, len(numbers)):
#         thread = GetThreadFromJoeyZThreadPool(squareNumber2, (number, resultDict, number))
#         thread.start()
#
#     for thread in threads:
#         thread.join()
#
#     return list(resultDict.values())
#
#
# if __name__ == "__main__":
#     numbers = [1, 2, 3, 4, 5]
#     before = datetime.datetime.now()
#     # squaredNumbers = calculateParallel(numbers)
#     # squaredNumbers = calculateCreatingNewThreads(numbers)
#     squaredNumbers = calculateCreatingNewThreads_JoeyZThreadPool(numbers)
#     for n in squaredNumbers:
#         print(n)
#
#     duration_s = (datetime.datetime.now() - before).total_seconds()
#     print("Completed with duration_s = " + str(duration_s) + " seconds")
