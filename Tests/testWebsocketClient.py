import json
import threading
import time
import traceback
import websocket

Port_ws = 8080

Url_Base = '127.0.0.1'

WebsocketClient = None


def GetUrl_Ws():
    return 'ws://' + Url_Base + ':' + str(Port_ws) + '/'


def ConnectWebSocketClient():
    global WebsocketClient
    WebsocketClient = WebsocketClientObject()


class WebsocketClientObject:
    ws = None
    exchangeName = None

    def __init__(self):
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        websocket.enableTrace(False)
        print("GetUrl_Ws() = " + str(GetUrl_Ws()))
        websocket.http_proxy_host = GetUrl_Ws()
        self.ws = websocket.WebSocketApp(GetUrl_Ws(),
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)

        while True:
            try:
                # Subscribe to events
                # Libraries.executeOnInterval.IsTimeToExecute("SubscribeWebsockets_OrderEvents", 0, SubscribeWebsockets_OrderEvents, True)

                print("Connecting to websockets server")
                self.ws.run_forever(ping_interval=30, ping_timeout=10)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                print("exception in  websockets = " + traceback.format_exc())

            print("websockets disconnected! Sleeping a bit then trying again")
            time.sleep(30)

    def on_message(self, ws, message):
        printMessage = "Websockets: data received: " + message[0:90] + "....." + message[-25:]
        print(printMessage + ": received at " + str(time.time()))
        try:
            jData = json.loads(message)
            print("message was valid JSON. jData = " + str(jData))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            print("exception, " + traceback.format_exc())

    def on_error(self, ws, error):
        print(str(error))

    def on_close(self, ws):
        message = "Disconnected from websockets."
        print(message)


ConnectWebSocketClient()

while True:
    time.sleep(20)
    # WebsocketClient.ws.send('something')
