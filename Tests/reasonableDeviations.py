import pandas as pd
import numpy as np
import networkx as nx
import math
from collections import defaultdict


def bellman_ford_negative_cycles(g, s):
    """
    Bellman Ford, modified so that it returns cycles.
    Runtime is O(VE).
    :param g: graph
    :type g: networkx weighted DiGraph
    :param s: source vertex
    :type s: str
    :return: all negative-weight cycles reachable from a source vertex
    :rtype: str list (empty if no neg-weight cyc)
    """
    n = len(g.nodes())
    d = defaultdict(lambda: math.inf)  # distances dict
    p = defaultdict(lambda: -1)  # predecessor dict
    d[s] = 0

    for _ in range(n - 1):
        for u, v in g.edges():
            # Bellman-Ford relaxation
            weight = g[u][v]["weight"]
            if d[u] + weight < d[v]:
                d[v] = d[u] + weight
                p[v] = u  # update pred

    # Find cycles if they exist
    all_cycles = []
    seen = defaultdict(lambda: False)

    for u, v in g.edges():
        weight = g[u][v]["weight"]
        # If we can relax further, there must be a neg-weight cycle
        if seen[v]:
            continue

        if d[u] + weight < d[v]:
            cycle = []
            x = v
            while True:
                # Walk back along predecessors until a cycle is found
                seen[x] = True
                cycle.append(x)
                x = p[x]
                if x == v or x in cycle:
                    break
            # Slice to get the cyclic portion
            idx = cycle.index(x)
            cycle.append(x)
            all_cycles.append(cycle[idx:][::-1])
    return all_cycles


def all_negative_cycles(g):
    """
    Get all negative-weight cycles by calling Bellman-Ford on
    each vertex. O(V^2 E)
    :param g: graph
    :type g: networkx weighted DiGraph
    :return: list of negative-weight cycles
    :rtype: list of str list
    """
    all_paths = []
    for v in g.nodes():
        all_paths.append(bellman_ford_negative_cycles(g, v))
    flatten = lambda l: [item for sublist in l for item in sublist]
    return [list(i) for i in set(tuple(j) for j in flatten(all_paths))]


def calculate_arb(cycle, g, verbose=True):
    """
    For a given negative-weight cycle on the log graph, calculate and
    print the arbitrage
    :param cycle: the negative-weight cycle
    :type cycle: list
    :param g: graph
    :type g: networkx weighted DiGraph
    :param verbose: whether to print path and arb
    :type verbose: bool
    :return: fractional value of the arbitrage
    :rtype: float
    """
    total = 0
    for (p1, p2) in zip(cycle, cycle[1:]):
        total += g[p1][p2]["weight"]
    arb = np.exp(-total) - 1
    if verbose:
        print("Path:", cycle)
        print(f"{arb * 100:.2g}%\n")
    return arb


def find_arbitrage(filename="snapshot.csv", find_all=False, sources=None):
    """
    Looks for arbitrage opportunities within a snapshot, i.e negative-weight cycles
    that include the currencies given in the sources list
    :param filename: filename of snapshot, defaults to "snapshot.csv"
    :type filename: str, optional
    :param find_all: whether to find all paths, defaults to False.
                     If false, sources must be provided.
    :type find_all: bool, optional
    :param sources: list of starting nodes – should choose the 'most connected' pairs,
                    defaults to None.
    :type sources: str list, optional
    :return: list of negative-weight cycles, or None if none exist
    :rtype: str list
    """

    graphArray = [
        [1, 0.00261, 0.00282, 0.051],
        [398.40637450199205, 1, 1.0, 20.1],
        [367.6470588235294, 1.0, 1, None],
        [18.51851851851852, 0.05, None, 1]]

    tokenArray = [
        '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',
        '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48',
        '0x6b175474e89094c44da98b954eedeac495271d0f',
        '0x221657776846890989a759ba2973e427dff5c9bb'
    ]

    # tokenArray = [
    #     'ETH',
    #     'USDC',
    #     'DAI',
    #     'REP'
    # ]

    # ,ETH,TFUEL,VET
    # ETH,,,2.764e-05
    # TFUEL,,,
    # VET,36088.05485384338,,

    graphArray_CSVFormat = ''
    delimiter = ','

    graphArray_CSVFormat += delimiter
    for index, token in enumerate(tokenArray):
        graphArray_CSVFormat += token
        if index < len(tokenArray) - 1:
            graphArray_CSVFormat += delimiter

    graphArray_CSVFormat += '\n'

    for index_rowList, rowList in enumerate(graphArray):
        # print('index_rowList = ', index_rowList, ', rowList = ', rowList)

        graphArray_CSVFormat += tokenArray[index_rowList] + delimiter
        for index_entry, entry in enumerate(rowList):
            # print('index_entry = ', index_entry, ', entry = ', entry)
            doAddDelimiter = True

            if index_entry >= len(rowList) - 1:
                doAddDelimiter = False

            # print('doAddDelimiter = ', doAddDelimiter)

            if entry is not None:
                graphArray_CSVFormat += str(entry)

            if doAddDelimiter:
                graphArray_CSVFormat += delimiter

        if index_rowList < len(graphArray) - 1:
            graphArray_CSVFormat += '\n'

    # print("graphArray_CSVFormat = ", graphArray_CSVFormat)

    # TESTDATA = StringIO("""col1;col2;col3
    #     1;4.4;99
    #     2;4.5;200
    #     3;4.7;65
    #     4;3.2;140
    #     """)

    from io import StringIO
    TESTDATA = StringIO(graphArray_CSVFormat)

    # Read df and convert to negative logs so we can use Bellman Ford
    # Negative weight cycles thus correspond to arbitrage opps
    # Transpose log_df so that graph has same API as the dataframe
    df = pd.read_csv(TESTDATA, header=0, index_col=0)

    # filename = "snapshot.csv"
    filename = "snapshot_joeyz.csv"

    # df = pd.read_csv(filename, header=0, index_col=0)
    # print('df of type ' + str(type(df)) + ' = ', df)
    # print('stuff = ', -np.log(df).fillna(0).T)

    g = nx.DiGraph(-np.log(df).fillna(0).T)

    # g = nx.DiGraph()
    # g.add_edges_from([(1, 2), (1, 3)])
    # g.add_edges_from([(1, 0.00261, 0.00282, 0.051), (398.40637450199205, 1, 1.0, 20.1), (367.6470588235294, 1.0, 1, 1e-28), (18.51851851851852, 0.05, 1e-28, 1)])
    #

    if nx.negative_edge_cycle(g):
        print("ARBITRAGE FOUND\n" + "=" * 15 + "\n")

        if find_all:
            unique_cycles = all_negative_cycles(g)
        else:
            all_paths = []
            for s in sources:
                all_paths.append(bellman_ford_negative_cycles(g, s))
            flatten = lambda l: [item for sublist in l for item in sublist]
            unique_cycles = [list(i) for i in set(tuple(j) for j in flatten(all_paths))]

        for p in unique_cycles:
            calculate_arb(p, g)
        return unique_cycles

    else:
        print("No arbitrage opportunities")
        return None


if __name__ == "__main__":
    find_arbitrage(find_all=True)
