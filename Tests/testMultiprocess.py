# # importing the multiprocessing module
# import multiprocessing
# import time
# from enum import Enum
#
#
# class TestEnumClass(Enum):
#     Option1 = 1
#     Option2 = 2
#     Option3 = 3
#
#
# class TestClass:
#     testStuff1 = None
#     testStuff2 = None
#     testStuff3 = None
#
#     def __init__(self, _testStuff1, _testStuff2, _testStuff3):
#         self.testStuff1 = _testStuff1
#         self.testStuff2 = _testStuff2
#         self.testStuff3 = _testStuff3
#
#
# def print_cube(num, resultDict, key):
#     returnValue = num * num * num
#     print("Cube: {}".format(num * num * num))
#     resultDict[key] = returnValue
#     resultDict['woot'] = 'tea'
#     print("resultDict = ", resultDict)
#
#     while True:
#         time.sleep(1)
#
#     # return returnValue
#
#
# def print_square(num, resultDict, key, testInt, testDouble, testString, customDict):
#     print("testInt = ", testInt.value)
#     print("testDouble = ", testDouble.value)
#     print("testString = ", testString.value)
#     print("customDict = ", customDict)
#     print("customDict['TestClassInstance'].testStuff1 = ", customDict['TestClassInstance'].testStuff1)
#     # print("customDict['TestClassInstance'].testStuff2 = ", customDict['TestClassInstance'].testStuff2)
#     # print("customDict['TestClassInstance'].testStuff3 = ",
#     #       customDict['TestClassInstance'].testStuff3, customDict['TestClassInstance'].testStuff3.name, customDict['TestClassInstance'].testStuff3.value)
#
#     customDict['TestClassInstance'].testStuff1 = 1337
#     # customDict['TestClassInstance'].testStuff2 = 'changed it yo'
#     # customDict['TestClassInstance'].testStuff3 = TestEnumClass.Option1
#
#     print("changing customDict contents in another process")
#     print("customDict['TestClassInstance'].testStuff1 = ", customDict['TestClassInstance'].testStuff1)
#     # print("customDict['TestClassInstance'].testStuff2 = ", customDict['TestClassInstance'].testStuff2)
#     # print("customDict['TestClassInstance'].testStuff3 = ",
#     #       customDict['TestClassInstance'].testStuff3, customDict['TestClassInstance'].testStuff3.name, customDict['TestClassInstance'].testStuff3.value)
#
#     returnValue = num * num
#     print("Square: {}".format(num * num))
#     resultDict[key] = returnValue
#     resultDict['woot'] = 'cup'
#     resultDict['TestClassInstance'].testStuff1 = 1337
#     print("resultDict = ", resultDict)
#     return returnValue
#
#
# if __name__ == "__main__":
#
#     # Shared mem dict
#     manager = multiprocessing.Manager()
#     resultDict = manager.dict()
#     resultDict['woot'] = 'sauce'
#     resultDict['TestClassInstance'] = TestClass(1, "hello woot i'm cool", TestEnumClass.Option2)
#     # Shared mem array
#
#     # Shared mem object?
#
#     # Shared mem int, i = int, d = double
#     testInt = multiprocessing.Value('i', 792089237316195423570985008687907853269984665640564039457584007913129639935)
#     print("testInt = ", testInt)
#     testDouble = multiprocessing.Value('d', 792089237316195423570985008687907853269984665640564039457584007913129639935)
#     print("testDouble = ", testDouble)
#     # Shared mem string
#     testString = multiprocessing.Array('c', b'hello world')
#
#     # Shared mem bool
#
#     # Shared mem enum
#
#     # # Shared mem dict with custom stuff in it
#     # manager = multiprocessing.Manager()
#     # customDict = manager.dict()
#     # customDict['TestClassInstance'] = TestClass(1, "hello woot i'm cool", TestEnumClass.Option2)
#
#     key_square = "square"
#     key_cube = "cube"
#     # creating processes
#     # p1 = multiprocessing.Process(target=print_square, args=(10, resultDict, key_cube, testInt, testDouble, testString, customDict,))
#     p2 = multiprocessing.Process(target=print_cube, args=(10, resultDict, key_square,))
#
#     # # starting process 1
#     # p1.start()
#     # starting process 2
#     p2.start()
#
#     while True:
#         time.sleep(1)
#
#     # # wait until process 1 is finished
#     # p1.join()
#     # wait until process 2 is finished
#     p2.join()
#
#     # both processes finished
#     print("Done! resultDict = ", resultDict)
#
#     print("customDict['TestClassInstance'].testStuff1 = ", customDict['TestClassInstance'].testStuff1)
#
#     # print("customDict['TestClassInstance'].testStuff2 = ", customDict['TestClassInstance'].testStuff2)
#     # print("customDict['TestClassInstance'].testStuff3 = ",
#     #       customDict['TestClassInstance'].testStuff3, customDict['TestClassInstance'].testStuff3.name, customDict['TestClassInstance'].testStuff3.value)
#
#
# #
# # import multiprocessing
# #
# #
# # def square_list(mylist, result, square_sum):
# #     """
# #     function to square a given list
# #     """
# #     # append squares of mylist to result array
# #     for idx, num in enumerate(mylist):
# #         result[idx] = num * num
# #
# #         # square_sum value
# #     square_sum.value = sum(result)
# #
# #     # print result Array
# #     print("Result(in process p1): {}".format(result[:]))
# #
# #     # print square_sum Value
# #     print("Sum of squares(in process p1): {}".format(square_sum.value))
# #
# #
# # if __name__ == "__main__":
# #     # input list
# #     mylist = [1, 2, 3, 4]
# #
# #     # creating Array of int data type with space for 4 integers
# #     result = multiprocessing.Array('i', 4)
# #
# #     # creating Value of int data type
# #     square_sum = multiprocessing.Value('i')
# #
# #     # creating new process
# #     p1 = multiprocessing.Process(target=square_list, args=(mylist, result, square_sum))
# #
# #     # starting process
# #     p1.start()
# #
# #     # wait until process is finished
# #     p1.join()
# #
# #     # print result array
# #     print("Result(in main program): {}".format(result[:]))
# #
# #     # print square_sum Value
# #     print("Sum of squares(in main program): {}".format(square_sum.value))
