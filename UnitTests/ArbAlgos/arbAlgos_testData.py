import Exchanges.ninja
import Libraries.ratesGraph


def PopulateRatesGraphWithTestData_NoArbitrage():
    Libraries.ratesGraph.CreateNewRatesGraphTokenList()
    Libraries.ratesGraph.AddTokenToRatesGraph('0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee')
    Libraries.ratesGraph.AddTokenToRatesGraph('0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48')
    Libraries.ratesGraph.AddTokenToRatesGraph('0x6b175474e89094c44da98b954eedeac495271d0f')
    Libraries.ratesGraph.CreateEmptyRatesGraph()
    Libraries.ratesGraph.InitializeRatesGraphForBlock(1)

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    bidPrice = 0.002511
    askPrice = 0.002521
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    bidPrice = 0.002512
    askPrice = 0.002522
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    quoteToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    bidPrice = 1.00
    askPrice = 1.00
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')


def PopulateRatesGraphWithTestData_YesArbitrage_2LegTrade():
    Libraries.ratesGraph.CreateNewRatesGraphTokenList()
    Libraries.ratesGraph.AddTokenToRatesGraph('0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee')
    Libraries.ratesGraph.AddTokenToRatesGraph('0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48')
    Libraries.ratesGraph.AddTokenToRatesGraph('0x6b175474e89094c44da98b954eedeac495271d0f')
    Libraries.ratesGraph.CreateEmptyRatesGraph()
    Libraries.ratesGraph.InitializeRatesGraphForBlock(1)

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    bidPrice = 0.002501
    askPrice = 0.002521
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '15')

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    bidPrice = 0.002521
    askPrice = 0.002511
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    bidPrice = 0.002501
    askPrice = 0.002521
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '15')


    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    bidPrice = 0.002512
    askPrice = 0.002522
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    quoteToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    bidPrice = 0.999
    askPrice = 1.001
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')


def PopulateRatesGraphWithTestData_YesArbitrage_3LegTrade():
    Libraries.ratesGraph.CreateNewRatesGraphTokenList()
    Libraries.ratesGraph.AddTokenToRatesGraph('0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee')
    Libraries.ratesGraph.AddTokenToRatesGraph('0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48')
    Libraries.ratesGraph.AddTokenToRatesGraph('0x6b175474e89094c44da98b954eedeac495271d0f')
    Libraries.ratesGraph.CreateEmptyRatesGraph()
    Libraries.ratesGraph.InitializeRatesGraphForBlock(1)

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    bidPrice = 0.002511
    askPrice = 0.002521
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    bidPrice = 0.002512
    askPrice = 0.002522
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    quoteToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    bidPrice = 1.10
    askPrice = 1.11
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')


def PopulateRatesGraphWithTestData_YesArbitrage_FuckedUpDataSetHopefullyWithBug():
    Libraries.ratesGraph.CreateNewRatesGraphTokenList()
    Libraries.ratesGraph.AddTokenToRatesGraph('0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee')
    Libraries.ratesGraph.AddTokenToRatesGraph('0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48')
    Libraries.ratesGraph.AddTokenToRatesGraph('0x6b175474e89094c44da98b954eedeac495271d0f')
    Libraries.ratesGraph.AddTokenToRatesGraph('0x221657776846890989a759ba2973e427dff5c9bb')
    Libraries.ratesGraph.CreateEmptyRatesGraph()
    Libraries.ratesGraph.InitializeRatesGraphForBlock(1)

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0x221657776846890989a759ba2973e427dff5c9bb'
    bidPrice = 0.054
    askPrice = 0.051
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    bidPrice = 0.002511
    askPrice = 0.002521
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    quoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    bidPrice = 0.002512
    askPrice = 0.002522
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    # So if I include this one, it finds only this arb and it doesn't find the REP arb above
    quoteToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    bidPrice = 1.12
    askPrice = 1.11
    Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

    # quoteToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'
    # bidPrice = 1.11
    # askPrice = 1.12
    # Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice, "Uniswap", "Balancer", 1, '1')

