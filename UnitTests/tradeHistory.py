import sys
import datetime
import time
from threading import Lock
from random import randint, uniform


class TradeHistoryUnitTest:
    name = None
    testTradeDataList = None
    desiredResult = None

    def __init__(self, _name, _testTradeDataList, _desiredResult):
        self.name = _name
        self.testTradeDataList = _testTradeDataList
        self.desiredResult = _desiredResult


TradeHistoryUnitTestDict = {}
# Increment the time_unixEpoch_milliseconds each time you have a new data point so that the transactions are ordered properly
time_unixEpoch_milliseconds = 1546404937047
incrementAmount_time_unixEpoch_milliseconds = 1000
# Here's the epoch time to human time converter for time_unixEpoch_milliseconds via https://www.epochconverter.com/
# GMT: Wednesday, January 2, 2019 4:55:37.047 AM
# According to CoinGecko BNT-USD value was:  "2019-01-02 00:00:00": 5.9874665684110315,
# According to CoinGecko, ETH-USD value was:    "2019-01-02 00:00:00": 138.14480220535194,

# Unit test data below

testTradeDataList = []
time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
testTradeDataList.append({
    'symbol': 'LINKETH',
    'id': None,
    'orderId': 34327635,
    'price': '0.00202184',
    'qty': '22.00000000',
    'commission': '0.00082294',
    'commissionAsset': 'BNB',
    'time': time_unixEpoch_milliseconds,
    'isBuyer': True,
    'isMaker': False,
    'isBestMatch': True
})
time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
testTradeDataList.append({
    'symbol': 'LINKETH',
    'id': None,
    'orderId': 34327635,
    'price': '0.00212184',
    'qty': '22.00000000',
    'commission': '0.00082294',
    'commissionAsset': 'BNB',
    'time': time_unixEpoch_milliseconds,
    'isBuyer': False,
    'isMaker': False,
    'isBestMatch': True
})
name = 'TestWithDavid1'
TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0.294063913376159)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellExactAmount'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0.294063913376159)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '7.00000000',
#     'commission': '0.00041147',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '4.00000000',
#     'commission': '0.00041147',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '11.00000000',
#     'commission': '0.00041147',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'Buy1SellSeveralExact'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0.29160025050725347)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '3.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '12.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '7.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00041147',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySeveralSell1Exact'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0.286672924769445)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '25.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellMoreThanWeBought'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0.29465519246469435)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '7.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellMoreThanWeBought2'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0.29406391337615734)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '21.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '8.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellMoreThanWeBought3'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0.2875688021763183)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '23.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '7.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '100.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '3.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellMoreThanWeBought4'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0.2942781449299751)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00108365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00109365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellExact_2DiffAssets'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 1.6656572839540598)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00108365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00109365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellExact_2DiffAssetsMixedOrder'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 1.6656572839540598)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellExactAmount_Loss'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, -0.313773216327391)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '7.00000000',
#     'commission': '0.00041147',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '4.00000000',
#     'commission': '0.00041147',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '11.00000000',
#     'commission': '0.00041147',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'Buy1SellSeveralExact_Loss'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, -0.31623687919629445)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '3.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '12.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '7.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00041147',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySeveralSell1Exact_Loss'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, -0.32116420493410364)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '25.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellMoreThanWeBought_Loss'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, -0.31318193723885307)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '7.00000000',
#     'commission': '0.00082294',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellMoreThanWeBought2_Loss'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, -0.3137732163273901)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00109365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00108365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellExact_2DiffAssetsMixedOrder_Loss'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, -1.7050758898565253)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00109365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00108365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00108365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellMoreThanBought1_2DiffAssetsMixedOrder_Loss'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, -1.7050758898565253)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00109365',
#     'qty': '1000.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00212184',
#     'qty': '22.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': True,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '44.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00108365',
#     'qty': '2000.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'BuySellMoreThanBought2_2DiffAssetsMixedOrder_Loss'
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, -1.700148564118717)
#
# testTradeDataList = []
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'LINKETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00202184',
#     'qty': '44.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# time_unixEpoch_milliseconds += incrementAmount_time_unixEpoch_milliseconds
# testTradeDataList.append({
#     'symbol': 'BATETH',
#     'id': None,
#     'orderId': 34327635,
#     'price': '0.00108365',
#     'qty': '2000.00000000',
#     'commission': '0.00082294',
#     # 'commission': '0',
#     'commissionAsset': 'BNB',
#     'time': time_unixEpoch_milliseconds,
#     'isBuyer': False,
#     'isMaker': False,
#     'isBestMatch': True
# })
# name = 'SellWithoutBuy'
#
# TradeHistoryUnitTestDict[name] = TradeHistoryUnitTest(name, testTradeDataList, 0)
#
