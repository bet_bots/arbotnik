import copy
from random import randint
import jsonpickle
from os.path import expanduser
import traceback
import datetime
import sys
import threading
import time
import os
from zero_ex.order_utils import generate_order_hash_hex
import numpy as np
import pandas as pd
import json

import Libraries.defaults
from Libraries.loggingConfig import InitLogging, PrintAndLog, PrintAndLogError
from Libraries.core import CreateDirectoryIfDoesNotAlreadyExist, ConsiderUpdatingLatestBlockNumber, API_PostOperatorNotification

CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_OperatorData)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_Snapshots)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_SnapshotsCSV)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_Snapshots_Test)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_SnapshotsCSV_Test)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_PKs)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_Cache)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_Logs)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_Logs_Set)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_Logs_PricesDicts)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_Forms)
CreateDirectoryIfDoesNotAlreadyExist(Libraries.defaults.Directory_RedisDB)

InitLogging()

PrintAndLog("Starting process main os.getpid() = " + str(os.getpid()))

import Libraries.network
from Contracts.contracts import Contract_Ninja_DiamondProxy, Contract_Ninja_Trade_A, Contract_Ninja_Trade_B, Contract_Ninja_Trade_C, Contract_Ninja_AssetManager, \
    Contract_Ninja_GasTokens, Contract_Ninja_Properties, Contract_0xv3, KeeperDAOLiquidityProviderAssetProxyAddress, \
    Contract_Ninja_Authentication_KeeperDAOLPPs, Contract_KeeperDAO_LP_Simple, ContractAddress_KeeperDAO_BorrowerProxy, Contract_Ninja_Exchange_Kyber, \
    Contract_Ninja_Exchange_Uniswap, Contract_Ninja_Exchange_0xv3, Contract_Ninja_Exchange_UniswapV2, Contract_Ninja_Exchange_Balancer, Contract_UniswapV2_Router, \
    Contract_Ninja_Trade_3, Contract_Curve_Pool_3Pool, Contract_Balancer_Exchange, Contract_Defiswap_Factory, Contract_Sushiswap_Router, Contract_UniswapV2_Router, \
    Contract_Balancer_Factory, Contract_Ninja_Exchange_Curve, Contract_Ninja_Exchange_0xv4

# Create directories
from Libraries.exchanges import ExchangeName_RadarRelay2, ApproveContractForTokenTransfer, \
    Exchange_0xv2, ExchangeName_0xMesh, ExchangeName_Kyber, ExchangeName_Uniswap, ExchangeName_0x, ExchangeName_Set, ExchangeDict, \
    GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken
from Libraries.marketObserver import UpdateCoinMarketCapMarkets, CoinMarketCapMarketsDict
from Libraries.ninjaUtils import PopulateTokenDict_DaiAndSai_BugFix, PopulateTokenDict_NinjaBetweenBancorAnd0x, PopulateTokenDict_NinjaBetween0xAndKyber, \
    PopulateTokenDict_NinjaBetweenUniswapAnd0x, PopulateTokenDict_Ninja_Set, PopulateTokenDict_NinjaBetweenUniswapAndKyber, PopulateTokenDict_NinjaBetweenBancorAndKyber
from Libraries.orders import MyOrder_0xv3, MyOrder_0xv4
from Libraries.transactions import Thread_PendingTransactionManager, API_SendEther_ToAddress, API_SendEther_ToContract, TransactionType, \
    API_PostCheckPendingTransaction, API_SendTokens_ToAddress, API_DeployContract, API_CancelTransaction, API_CancelTransaction_BaseGasPriceOnSpecificTx
from Libraries.gasStation import GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade, GasPriceSetting

from Libraries.accounts import TokenDict_Ninja, NinjaOpAccountDict, CreateNewReservableEthereumAccountPoolList, GetDetailsOfAListOfEthereumAccounts, SetSocialTestAccount, \
    UtilityAccountDict, HidingGameRfqOriginsAccount
from Libraries.aesCipher import AESCipher
from Libraries.utils import SweepEtherFromAccount, ConvertListOfStringsToLowercaseListOfStrings
from Libraries.config import SetActiveConfig, Config, Option, ConvertEnumListToEnumValueList, GetEnumValueFromArgument, GetArgsFromSysArgv
from Libraries.topics import BuildTopicsArray_KeeperDAO_BorrowProfitShare

import Exchanges.zrx
import Exchanges.zrxV2
import Exchanges.zrxV4
import Exchanges.bancor
import Exchanges.oasisDex
import Exchanges.kyber
import Exchanges.ninja
import Exchanges.radarRelay2
import Exchanges.uniswapV2
import Exchanges.balancer
import Exchanges.uniswap
import Exchanges.set
import Exchanges.zrxApi
import Exchanges.keeperDAO
import Exchanges.curve
import Exchanges.hidingBook

import Libraries.core
import Libraries.arbyUtility
import Libraries.gasStation
import Libraries.signingUtils
import Libraries.node
import Libraries.executeOnInterval
import Libraries.timeRecorder
import Libraries.coinGecko
import Libraries.cryptoCompare
import Libraries.cache
import Libraries.nonceUtils
import Libraries.customDicts
import Libraries.utils
import Libraries.security
import Libraries.nodes
import Exchanges.ninja_tailgating
import Libraries.gasToken_CHI
import Libraries.estimatedGas
import Libraries.priceOracle
import Libraries.orderAggregator
import Libraries.pendingTransactionMonitor
import Libraries.diamondProxy
import Libraries.notorious
import Libraries.ninjaEncoding
import Libraries.node_geth
import Libraries.blockNative
import Libraries.alchemy
import Libraries.bloxroute
import Libraries.f2pool
import Libraries.tokenTransferCost
import Libraries.arbAlgos
import Libraries.postMortem
import Libraries.relayProxy
import Libraries.rateLimiter
import UnitTests.ArbAlgos.arbAlgos_testData

import BotLoops.ninjaLoops

# ************************************ Begin Initialize Global Features and Variables ************************************ #

# Start a thread that manages pending transactions
PendingTransactionManager = Thread_PendingTransactionManager()

# TODO, refactor these warnings. I should not have to manually add them here. This is not maintainable
# Some warnings in case I leave something off by accident when deploying
Libraries.core.SendNotificationIfThisIsFalse(Libraries.defaults.EnterMainLoop, "EnterMainLoop")
Libraries.core.SendNotificationIfThisIsFalse(Libraries.defaults.EnableOrderAggregator_0xMesh, "EnableOrderAggregator_0xMesh")
Libraries.core.SendNotificationIfThisIsFalse(Libraries.defaults.EnableNinja_TradeExecution, "EnableNinja_TradeExecution")

Libraries.core.SendNotificationIfThisIsTrue(Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug, "TimeMachine_EnabledForHistoricBlockDebug")
Libraries.core.SendNotificationIfThisIsTrue(Libraries.defaults.Set_ForceTradeRegardlessOfPrice, "Set_ForceTradeRegardlessOfPrice")
Libraries.core.SendNotificationIfThisIsTrue(Libraries.defaults.QuitAfterTradeExecutionWhileIDebug, "QuitAfterTradeExecutionWhileIDebug")
Libraries.core.SendNotificationIfThisIsTrue(Libraries.defaults.LongSleepForDebug_Ninja, "LongSleepForDebug_Ninja")
Libraries.core.SendNotificationIfThisIsTrue(Libraries.defaults.Ninja_DeleteAllTokensExceptForThoseInThisList, "Ninja_DeleteAllTokensExceptForThoseInThisList")
Libraries.core.SendNotificationIfThisIsTrue(Libraries.defaults.Set_DeleteAllSetTokensExceptForOne, "Set_DeleteAllSetTokensExceptForOne")
Libraries.core.SendNotificationIfThisIsTrue(Libraries.defaults.Set_UseMainnetStagingContracts, "Set_UseMainnetStagingContracts")
Libraries.core.SendNotificationIfThisIsTrue(Libraries.defaults.Set_AddMyHardCodedStagingContractsToSetData, "Set_AddMyHardCodedStagingContractsToSetData")

# ************************************ End Initialize Global Features and Variables ************************************ #

# Load the security credentials AFTER handling the config
# Tell the function to skip the check since this is the initial load
Libraries.security.LoadSecurityCredentialsIntoMem(True)

# Skip over the "if" so that everything is an "elif"
if False:
    pass

elif sys.argv and len(sys.argv) >= 2 and "encpk" == sys.argv[1].lower():
    accountName = ''
    privateKey = ''

    # print("Libraries.security.GetP4s5wo0dFromMem() = " + str(Libraries.security.GetP4s5wo0dFromMem()))
    aesCipher = AESCipher(Libraries.security.GetP4s5wo0dFromMem())
    garby = aesCipher.encrypt(privateKey)
    # print(accountName + "'s enc'd text is = " + str(garby))

    # Store to file
    filepath = Libraries.defaults.Directory_PKs + "/" + accountName
    file = open(filepath, "w")
    file.write(garby)
    file.close()

elif sys.argv and len(sys.argv) >= 2 and "decpks" == sys.argv[1].lower():
    for key in NinjaOpAccountDict:
        print(key, NinjaOpAccountDict[key].publicAddress, NinjaOpAccountDict[key].DecryptPK())

elif sys.argv and len(sys.argv) >= 2 and "gasprices" == sys.argv[1].lower():
    while True:
        Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
        ConsiderUpdatingLatestBlockNumber()

        # fastBaseline = Libraries.gasStation.GetGasPrice_FastBaseline()
        # standardBaseline = Libraries.gasStation.GetGasPrice_StandardBaseline()
        # safeLowBaseline = Libraries.gasStation.GetGasPrice_SafeLowBaseline()
        # PrintAndLog("fastBaseline = " + str(fastBaseline))
        # PrintAndLog("standardBaseline = " + str(standardBaseline))
        # PrintAndLog("safeLowBaseline = " + str(safeLowBaseline))

        value = Libraries.gasStation.GetGasPrice_Fast()
        PrintAndLog("Libraries.gasStation.GetGasPrice_Fast() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        value = Libraries.gasStation.GetGasPrice_Cheap()
        PrintAndLog("Libraries.gasStation.GetGasPrice_Cheap() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        value = Libraries.gasStation.GetGasPrice_SafeLow()
        PrintAndLog("Libraries.gasStation.GetGasPrice_SafeLow() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        value = Libraries.gasStation.GetGasPrice_CancelOrder()
        PrintAndLog("Libraries.gasStation.GetGasPrice_CancelOrder() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        value = Libraries.gasStation.GetGasPrice_CancelOrder_HighPriority()
        PrintAndLog("Libraries.gasStation.GetGasPrice_CancelOrder_HighPriority() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        value = Libraries.gasStation.GetGasPrice_StaleOrderFromOrderBook_Min()
        PrintAndLog("Libraries.gasStation.GetGasPrice_StaleOrderFromOrderBook_Min() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        value = Libraries.gasStation.GetGasPrice_StaleOrderFromOrderBook_Max()
        PrintAndLog("Libraries.gasStation.GetGasPrice_StaleOrderFromOrderBook_Max() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        value = Libraries.gasStation.GetGasPrice_NewWebSocketOrder_Min()
        PrintAndLog("Libraries.gasStation.GetGasPrice_NewWebSocketOrder_Min() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        value = Libraries.gasStation.GetGasPrice_NewWebSocketOrder_Max()
        PrintAndLog("Libraries.gasStation.GetGasPrice_NewWebSocketOrder_Max() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        # value = Libraries.gasStation.GetGasPrice_SmashNotoriousBTC_Min()
        # PrintAndLog("Libraries.gasStation.GetGasPrice_SmashNotoriousBTC_Min() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        # value = Libraries.gasStation.GetGasPrice_SmashNotoriousBTC_Max()
        # PrintAndLog("Libraries.gasStation.GetGasPrice_SmashNotoriousBTC_Max() = " + str(Libraries.core.ConvertWeiToGwei(value)) + " gwei,  " + str(value) + " wei")

        # PrintAndLog("Libraries.gasStation.GetGasLimit_Other() = " + str(Libraries.gasStation.GetGasLimit_Other()))
        # PrintAndLog("Libraries.gasStation.GetGasLimit_Trade() = " + str(Libraries.gasStation.GetGasLimit_Trade()))

        # OrderPropertySet(2, OrderDuration.superShort, OrderMinimumProfitPercentageFromSourcePrice.radarPassive)
        # OrderPropertySet(2, OrderDuration.wickedShort, OrderMinimumProfitPercentageFromSourcePrice.radarSuperAggressive)

        Libraries.gasStation.ConsiderGettingBancorsMaxGasPriceValue()
        Libraries.gasStation.ConsiderGettingKybersMaxGasPriceValue()
        PrintAndLog("BancorMaxGasPrice gwei = " + str(Libraries.core.ConvertWeiToGwei(Libraries.gasStation.BancorMaxGasPrice_wei)))
        PrintAndLog("KybersMaxGasPrice gwei = " + str(Libraries.core.ConvertWeiToGwei(Libraries.gasStation.KyberMaxGasPrice_wei)))

        # PrintAndLog("Libraries.gasStation.GetSuggested_Taker_MinimumProfitRequirement_NewWebSocketOrder() = " + str(
        #     Libraries.gasStation.GetSuggested_Taker_MinimumProfitRequirement_NewWebSocketOrder()))
        # PrintAndLog("Libraries.gasStation.GetSuggested_Taker_MinimumProfitRequirement_StaleOrderFromOrderBook() = " + str(
        #     Libraries.gasStation.GetSuggested_Taker_MinimumProfitRequirement_StaleOrderFromOrderBook()))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.016, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.016, GasPriceSetting.newWebSocketOrder)))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.03, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.03, GasPriceSetting.newWebSocketOrder)))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.05, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.05, GasPriceSetting.newWebSocketOrder)))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.07, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.07, GasPriceSetting.newWebSocketOrder)))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.09, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.09, GasPriceSetting.newWebSocketOrder)))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.11, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.11, GasPriceSetting.newWebSocketOrder)))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.25, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(0.25, GasPriceSetting.newWebSocketOrder)))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(1.0, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(1.0, GasPriceSetting.newWebSocketOrder)))
        # PrintAndLog(
        #     "----------------------------------------------------------------------------------------------------------------------------------------------------")
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(4.0, GasPriceSetting.staleOrderFromOrderBook)))
        # PrintAndLog(str(GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(4.0, GasPriceSetting.newWebSocketOrder)))

        PrintAndLog("")
        PrintAndLog("Sleeping before I run again...")

        time.sleep(20)

elif sys.argv and len(sys.argv) >= 2 and "sweepmarket" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()

    destinationAddress = ''
    publicAddress_toSweep = ''
    privateKey_toSweep = ''
    # SweepEtherFromAccount(publicAddress_toSweep, privateKey_toSweep, destinationAddress, Libraries.gasStation.GetGasPrice_Cheap(), Libraries.gasStation.GetGasLimit_Other())

elif sys.argv and len(sys.argv) >= 2 and "test0x" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()

    # publicAddress = "yo"
    # privateKey = "yo"

    # publicAddress = "YOYO"
    # privateKey = "YOYO"
    #
    # # Did not work... "validationErrors":[{"field":"takerTokenAmount","code":1004,"reason":"Value out of range"}]}
    # # ether = 0.000001
    # # tokens = 0.0000000224074
    #
    # orderType = "buy"
    # tokens = 0.001
    # # price = 0.0240103   # NMR
    # price = 0.00109400   # ZRX
    # ether = Libraries.core.ConvertTokensToEther(0.004, price)
    # # tokenAddress = "0x1776e1f26f98b1a5df9cd347953a26dd3cb46671"  # NMR token
    # tokenAddress = "0xe41d2489571d322189246dafa5ebde1f4699f498"  # ZRX token
    #
    # ether_base = Libraries.core.ConvertEtherAmountToBaseAmount(ether, Libraries.core.Ether_Decimals)
    # token_base = Libraries.core.ConvertEtherAmountToBaseAmount(tokens, float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))

elif sys.argv and len(sys.argv) >= 2 and "testapproves_0x" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    Libraries.core.API_GetLatestBlockNumber()

    # Submitted approve, some worked some did not... I should check it's approved amount
    # marketName = "mkr-eth"
    # Approve_Tokens_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), MarketDict[marketName].erc20TokenContractAddress)
    # Approve_Ether_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK())

    # For ocean to approve zrx transfer on a market
    # marketName = "tusd-eth"
    # Approve_Tokens_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), MarketDict["zrx-eth"].erc20TokenContractAddress)

    # Submitted approve, some worked some did not... I should check it's approved amount
    # marketName = "fun-eth"
    # ApproveReset_Tokens_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), MarketDict[marketName].erc20TokenContractAddress)
    # Approve_Tokens_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), MarketDict[marketName].erc20TokenContractAddress)
    # Approve_Ether_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK())

    # ApproveReset_Ether_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK())
    # Approve_Tokens_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), MarketDict[marketName].erc20TokenContractAddress)
    # ApproveReset_Tokens_0x(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), MarketDict[marketName].erc20TokenContractAddress)
    # Exchanges.zrx.Get_TokenPairs()

elif sys.argv and len(sys.argv) >= 2 and "testapproves_0xv4" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    Libraries.core.API_GetLatestBlockNumber()

    Exchanges.zrxV4.SetNetwork(Libraries.network.Network.mainnet)

    account = NinjaOpAccountDict["ninja-op-116"]
    # token = Exchanges.keeperDAO.Contract_USDC
    token = Exchanges.zrxV4.Contract_WETH
    spenderToApprove = Exchanges.zrxV4.Contract_Exchange
    nonce = None
    transactionId = ApproveContractForTokenTransfer(account.publicAddress, account.DecryptPK(), token, spenderToApprove, nonce)

elif sys.argv and len(sys.argv) >= 2 and "testapproves_keeperdao_withdraws_and_deposits" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    Libraries.core.API_GetLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    # Approve KeeperDAO LP kTokens so I can withdraw from the LP

    liquidityProviderAccount = NinjaOpAccountDict["ninja-op-100"]
    nonce = Libraries.core.API_GetTransactionCount(liquidityProviderAccount.publicAddress)

    kTokenDict = Exchanges.keeperDAO.GetKTokenDict()

    for keeperDAOLPContract in kTokenDict:
        for borrowableQuoteToken in kTokenDict[keeperDAOLPContract]:
            spenderToApprove = keeperDAOLPContract.address
            # Get the kTokenAddress from the dict
            kTokenAddress = kTokenDict[keeperDAOLPContract][borrowableQuoteToken]
            PrintAndLog("Calling ApproveContractForTokenTransfer from " + str(liquidityProviderAccount.publicAddress) + " for kToken " + str(
                kTokenAddress) + " which is for the spenderToApprove " + str(spenderToApprove))
            transactionId = ApproveContractForTokenTransfer(liquidityProviderAccount.publicAddress, liquidityProviderAccount.DecryptPK(),
                                                            kTokenAddress, spenderToApprove, nonce)
            if transactionId:
                nonce += 1

    borrowableQuoteTokensList = Exchanges.keeperDAO.GetBorrowableQuoteTokensList()
    for tokenAddress in borrowableQuoteTokensList:
        for keeperDAOLPContract in kTokenDict:
            spenderToApprove = keeperDAOLPContract.address
            PrintAndLog("Calling ApproveContractForTokenTransfer from " + str(liquidityProviderAccount.publicAddress) + " for token " + str(
                tokenAddress) + " which is for the spenderToApprove " + str(spenderToApprove))
            transactionId = ApproveContractForTokenTransfer(liquidityProviderAccount.publicAddress, liquidityProviderAccount.DecryptPK(),
                                                            tokenAddress, spenderToApprove, nonce)
            if transactionId:
                nonce += 1

elif sys.argv and len(sys.argv) >= 2 and "testradarrelay3" == sys.argv[1].lower():
    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.zrxV2.SetNetwork(Libraries.network.Network.kovanTestnet)

    # marketName = "mln-eth"  # Kovan
    # marketName = "rep-eth"
    marketName = "bat-eth"
    # marketName = "dai-eth"
    #
    jData_markets = Exchanges.radarRelay2.API_GetMarkets()

elif sys.argv and len(sys.argv) >= 2 and "testradarrelay3_getorderinfo" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.zrxV2.SetNetwork(Libraries.network.Network.kovanTestnet)

    orderType = "buy"
    # orderType = "sell"

    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "r")

    elif orderType == "buy":
        file = open("testLocalOrder_0x_buy", "r")

    data = file.read()
    file.close()
    # deserialize the file's data into an object
    order = jsonpickle.decode(data)
    PrintAndLog("order = " + str(order))

    Exchanges.zrxV2.GetOrderInfo(order)
    # Exchanges.zrxV2.API_GetOrderInfo_Batched([order])

# elif sys.argv and len(sys.argv) >= 2 and "testradarrelay3_getepoch" == sys.argv[1].lower():
#     Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
#     ConsiderUpdatingLatestBlockNumber()
#
#     Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
#     # Exchanges.zrxV2.SetNetwork(Libraries.network.Network.kovanTestnet)
#
#     # Not using??
#     # # marketName = "mln-eth"  # Kovan
#     # marketName = "rep-eth"
#     # # Exchanges.zrxV2.GetOrderInfo(order)
#     # senderAddress = '0x0000000000000000000000000000000000000000'
#     # Exchange_RadarRelay2.GetOrderEpochForOrder(marketName, MarketDict[marketName].account.publicAddress, senderAddress, None, None)

elif sys.argv and len(sys.argv) >= 2 and "testradarrelay3_websockets" == sys.argv[1].lower():
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_NinjaBetweenBancorAnd0x()
    PopulateTokenDict_NinjaBetween0xAndKyber()
    PopulateTokenDict_NinjaBetweenUniswapAnd0x()

    # Libraries.orderAggregator.CreateOrderAggregator_BasedOnTokenDict_Ninja(ExchangeName_RadarRelay2)
    # Libraries.orderAggregator.CreateOrderAggregator_BasedOnMarketDict(ExchangeName_RadarRelay2)

    Exchanges.radarRelay2.Set_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses(Libraries.orderAggregator.GetOrderAggregator_BasedOnTokenDict_Ninja)
    # Exchanges.radarRelay2.Set_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses(Libraries.orderAggregator.GetOrderAggregator_BasedOnMarketDict)
    Exchanges.radarRelay2.ConnectWebSocketClient(ExchangeName_RadarRelay2)

    while True:
        PrintAndLog("Sleeping while I test websockets")
        time.sleep(10)

elif sys.argv and len(sys.argv) >= 2 and "test0xv3_sign_local_order" == sys.argv[1].lower():
    PrintAndLog("Signing local test order")

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    publicAddress = ''
    fromPrivateKey = ''
    #
    # ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    # publicAddress = ninjaOp.publicAddress
    # fromPrivateKey = ninjaOp.DecryptPK()
    # tokenAddress = '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359'  # sai
    # tokenAddress = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'  # wbtc
    # tokenAddress = '0x0d8775f648430679a709e98d2b0cb6250d2887ef'  # bat
    tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'  # usdc

    orderType = "buy"
    # orderType = "sell"
    tokens = 0.001
    # price = 0.05686397  # rep   buy
    # price = 0.07686397  # rep   sell
    # price = 0.00104924  # bat buy
    # price = 0.00164924  # bat sell
    price = 0.00171962  # dai/sai/usdc
    # price = 50.14  # wbtc buy
    # price = 56.14  # wbtc sell

    ether = Libraries.core.ConvertTokensToEther(tokens, price)

    ether_base = Libraries.core.ConvertEtherAmountToBaseAmount(ether, Libraries.core.Ether_Decimals)
    token_base = Libraries.core.ConvertEtherAmountToBaseAmount(tokens, float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))

    exchangeContractAddress = Exchanges.zrxV2.Contract_Exchange.lower()
    maker = publicAddress.lower()
    taker = Libraries.core.GetNullAddress().lower()  # Anyone can fill

    if orderType == "buy":
        makerTokenAddress = Exchanges.zrxV2.Contract_WETH.lower()
        takerTokenAddress = tokenAddress.lower()
        makerTokenAmount = str(ether_base)
        takerTokenAmount = str(token_base)
    elif orderType == "sell":
        makerTokenAddress = tokenAddress.lower()
        takerTokenAddress = Exchanges.zrxV2.Contract_WETH.lower()
        makerTokenAmount = str(token_base)
        takerTokenAmount = str(ether_base)

    feeRecipient = Libraries.core.GetNullAddress().lower()
    senderAddress = Libraries.core.GetNullAddress().lower()
    makerFee = "0"
    takerFee = "0"
    # This is how RadarRelay specifies no fee, is this correct??
    makerFeeTokenAddress = "0x"
    takerFeeTokenAddress = "0x"

    # orderDuration_seconds = int(12)
    orderDuration_seconds = int(10000)
    expirationUnixTimestampSec = str(int(time.time()) + orderDuration_seconds)
    # Set the salt based on unix epoch time in milliseconds instead of seconds.
    salt = str(int(time.time()) * 1000)

    order = MyOrder_0xv3("_" + orderType, exchangeContractAddress, maker, taker, makerTokenAddress, takerTokenAddress,
                         feeRecipient, makerTokenAmount, takerTokenAmount, makerFee, takerFee, makerFeeTokenAddress,
                         takerFeeTokenAddress, expirationUnixTimestampSec, orderDuration_seconds, salt, senderAddress)
    Libraries.signingUtils.SignOrder_0xv3(order, fromPrivateKey)

    # Write the order to a file so we can keep track of it even if the bot crashes or stops
    serializedOrderObject = jsonpickle.encode(order)
    PrintAndLog("serializedOrderObject = " + str(serializedOrderObject))

    if orderType == "buy":
        file = open("testLocalOrder_0x_buy", "w")
        file.write(serializedOrderObject)
        file.close()
    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "w")
        file.write(serializedOrderObject)
        file.close()

elif sys.argv and len(sys.argv) >= 2 and "test0xv3_filllocalorder" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    marketName = "wbtc-eth"

    # orderType = "sell"
    orderType = "buy"
    erc20TokenContractAddress = ''

    tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(erc20TokenContractAddress)

    fillTakerTokenAmount_baseUnit = None
    fillTakerTokenAmount_etherUnit = None
    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "r")
        fillTakerTokenAmount_etherUnit = 0.001
        PrintAndLog("The taker is buying, so taker token is ether")
        fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit, Libraries.core.Ether_Decimals)
    elif orderType == "buy":
        file = open("testLocalOrder_0x_buy", "r")
        fillTakerTokenAmount_etherUnit = 0.000001
        PrintAndLog("The taker is selling, so taker token is tokens")
        fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit,
                                                                                      float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))

    PrintAndLog("fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit))
    data = file.read()
    file.close()
    # deserialize the file's data into an object
    order = jsonpickle.decode(data)
    PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(order.GetTokenQuantity()) + ", ether quantity + " + str(
        order.GetEtherQuantity()) + ", fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit) + ", fillTakerTokenAmount_etherUnit = " + str(
        fillTakerTokenAmount_etherUnit))

    # exchange_DX = MarketDict[marketName].ExchangeDict[ExchangeName_0xMesh]
    exchange_DX = None
    gas = Libraries.gasStation.GetGasLimit_TradeCustomContract()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    fromAddress = ninjaOp.publicAddress
    fromPrivateKey = ninjaOp.DecryptPK()
    # exchange_DX.FillOrder(fromAddress, fromPrivateKey, order, fillTakerTokenAmount_baseUnit, gas, gasPrice)
    # exchange_DX.BatchFillOrders(fromAddress, fromPrivateKey,
    #                                     [order, order, order],
    #                                     [fillTakerTokenAmount_baseUnit, fillTakerTokenAmount_baseUnit, fillTakerTokenAmount_baseUnit],
    #                                     gas, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "test0xv3_quick_sign_and_fill_test" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()

    PrintAndLog("Perform quick order fill test")

    PrintAndLog("Signing local test order")

    doFillOrder = False
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.core.ConvertGweiToWei(60)  # DOES NOT WORK, current fast gas price is 45
    gasPrice = Libraries.core.ConvertGweiToWei(80)  # FAILED 1 time, WORKED 1 time.
    # gasPrice = Libraries.core.ConvertGweiToWei(100)  # WORKS!

    orderDuration_seconds = int(1)
    # orderDuration_seconds = int(1000)

    # Get the latest block number until we get a brand new block
    Libraries.core.API_GetLatestBlockNumber()
    currentBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    # Begin the test on the next block
    blockNumberToBeginTest = currentBlockNumber + 1
    targetBlocksTimestamp = None
    while currentBlockNumber < blockNumberToBeginTest:
        time.sleep(0.1)
        Libraries.core.API_GetLatestBlockNumber()
        currentBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
        # The next block's timestamp is the time the previous block was mined in
        targetBlocksTimestamp = int(time.time())

    # We need our fill order mined in exactly 1 block after this one
    blockNumberWeNeedOurFillOrderMinedIn = currentBlockNumber + 1
    PrintAndLog("Found the block number to begin the test! targetBlocksTimestamp = " + str(targetBlocksTimestamp))

    PrintAndLog("Signing a test order")

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    # tokenAddress = ''
    # publicAddress = ''
    # fromPrivateKey = ''

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    publicAddress = ninjaOp.publicAddress
    fromPrivateKey = ninjaOp.DecryptPK()

    tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'  # usdc
    # tokenAddress = '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359'  # sai
    # tokenAddress = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'  # wbtc
    # tokenAddress = '0x0d8775f648430679a709e98d2b0cb6250d2887ef'  # bat

    # orderType = "buy"
    orderType = "sell"
    tokens = 0.06
    # price = 0.05686397  # rep   buy
    # price = 0.07686397  # rep   sell
    # price = 0.00104924  # bat buy
    # price = 0.00164924  # bat sell
    price = 0.00473096  # dai/sai/usdc
    # price = 50.14  # wbtc buy
    # price = 56.14  # wbtc sell

    ether = Libraries.core.ConvertTokensToEther(tokens, price)

    ether_base = Libraries.core.ConvertEtherAmountToBaseAmount(ether, Libraries.core.Ether_Decimals)
    token_base = Libraries.core.ConvertEtherAmountToBaseAmount(tokens, float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))

    exchangeContractAddress = Exchanges.zrxV2.Contract_Exchange.lower()
    maker = publicAddress.lower()
    taker = Libraries.core.GetNullAddress().lower()  # Anyone can fill
    if orderType == "buy":
        makerTokenAddress = Exchanges.zrxV2.Contract_WETH.lower()
        takerTokenAddress = tokenAddress.lower()
        makerTokenAmount = str(ether_base)
        takerTokenAmount = str(token_base)
    elif orderType == "sell":
        makerTokenAddress = tokenAddress.lower()
        takerTokenAddress = Exchanges.zrxV2.Contract_WETH.lower()
        makerTokenAmount = str(token_base)
        takerTokenAmount = str(ether_base)

    feeRecipient = Libraries.core.GetNullAddress().lower()
    senderAddress = Libraries.core.GetNullAddress().lower()
    makerFee = "0"
    takerFee = "0"
    # This is how RadarRelay specifies no fee, is this correct??
    makerFeeTokenAddress = "0x"
    takerFeeTokenAddress = "0x"

    expirationUnixTimestampSec = str(targetBlocksTimestamp + orderDuration_seconds)
    # Set the salt based on unix epoch time in milliseconds instead of seconds.
    salt = str(int(time.time()) * 1000)

    order = MyOrder_0xv3("_" + orderType, exchangeContractAddress, maker, taker, makerTokenAddress, takerTokenAddress,
                         feeRecipient, makerTokenAmount, takerTokenAmount, makerFee, takerFee, makerFeeTokenAddress,
                         takerFeeTokenAddress, expirationUnixTimestampSec, orderDuration_seconds, salt, senderAddress)
    Libraries.signingUtils.SignOrder_0xv3(order, fromPrivateKey)

    # Write the order to a file so we can keep track of it even if the bot crashes or stops
    serializedOrderObject = jsonpickle.encode(order)
    PrintAndLog("serializedOrderObject = " + str(serializedOrderObject))

    if orderType == "buy":
        file = open("testLocalOrder_0x_buy", "w")
        file.write(serializedOrderObject)
        file.close()
    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "w")
        file.write(serializedOrderObject)
        file.close()

    PrintAndLog("Logic for filling the order, doFillOrder = " + str(doFillOrder))

    fillTakerTokenAmount_baseUnit = None
    fillTakerTokenAmount_etherUnit = None
    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "r")
        fillTakerTokenAmount_etherUnit = 0.0001
        PrintAndLog("The taker is buying, so taker token is ether")
        fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit, Libraries.core.Ether_Decimals)
    elif orderType == "buy":
        file = open("testLocalOrder_0x_buy", "r")
        fillTakerTokenAmount_etherUnit = 0.000001
        PrintAndLog("The taker is selling, so taker token is tokens")
        fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit,
                                                                                      float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))

    PrintAndLog("fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit))
    data = file.read()
    file.close()
    # deserialize the file's data into an object
    order = jsonpickle.decode(data)
    PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(order.GetTokenQuantity()) + ", ether quantity + " + str(
        order.GetEtherQuantity()) + ", fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit) + ", fillTakerTokenAmount_etherUnit = " + str(
        fillTakerTokenAmount_etherUnit))

    gas = Libraries.gasStation.GetGasLimit_TradeCustomContract()

    if doFillOrder:
        ninjaOp = NinjaOpAccountDict["ninja-op-100"]
        fromAddress = ninjaOp.publicAddress
        fromPrivateKey = ninjaOp.DecryptPK()
        transactionId = Exchange_0xv2.FillOrder(fromAddress, fromPrivateKey, order, fillTakerTokenAmount_baseUnit, gas, gasPrice)
        PrintAndLog("transactionId = " + str(transactionId))

    PrintAndLog("Monitoring to the fill and the next block's mining")
    while currentBlockNumber < blockNumberToBeginTest + 1:
        time.sleep(0.1)
        Libraries.core.API_GetLatestBlockNumber()
        currentBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    PrintAndLog("Found the block where our test order should have gotten mined in, does it have a targetBlocksTimestamp of = " + str(targetBlocksTimestamp) + "?")
    jData_block = Libraries.core.API_GetBlock(currentBlockNumber)
    blockTimestamp_unixEpochSeconds = Libraries.core.ConvertHexToInt(jData_block['timestamp'])
    PrintAndLog("blockTimestamp_unixEpochSeconds = " + str(blockTimestamp_unixEpochSeconds))
    PrintAndLog("order's expiration = " + str(expirationUnixTimestampSec))
    PrintAndLog("blockNumberWeNeedOurFillOrderMinedIn = " + str(blockNumberWeNeedOurFillOrderMinedIn))

elif sys.argv and len(sys.argv) >= 2 and "test0xv3_isvalidhashsignature" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    marketName = "bat-eth"

    orderType = "sell"
    # orderType = "buy"
    erc20TokenContractAddress = ''

    tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(erc20TokenContractAddress)

    fillTakerTokenAmount_baseUnit = None
    fillTakerTokenAmount_etherUnit = None
    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "r")
        fillTakerTokenAmount_etherUnit = 0.00000000001
        PrintAndLog("The taker is buying, so sending ether")
        fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit, Libraries.core.Ether_Decimals)
    elif orderType == "buy":
        file = open("testLocalOrder_0x_buy", "r")
        fillTakerTokenAmount_etherUnit = 0.00000001
        PrintAndLog("The taker is selling, so sending tokens")
        fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit,
                                                                                      float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))

    PrintAndLog("fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit))
    data = file.read()
    file.close()
    # deserialize the file's data into an object
    order = jsonpickle.decode(data)
    PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(order.GetTokenQuantity()) + ", ether quantity + " + str(
        order.GetEtherQuantity()) + ", fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit) + ", fillTakerTokenAmount_etherUnit = " + str(
        fillTakerTokenAmount_etherUnit))

    isValid = Exchange_0xv2.IsValidHashSignature(order.hash, order.maker, order.signature)
    PrintAndLog("isValid = " + str(isValid))

elif sys.argv and len(sys.argv) >= 2 and "test0xv3_protocolfee" == sys.argv[1].lower():
    gasPrice_gwei = 1
    gasPrice = Libraries.core.ConvertGweiToWei(gasPrice_gwei)
    protocolFee_eth_weiUnits = Exchanges.zrxV2.GetProtocolFee(gasPrice, 1)
    PrintAndLog("for gas price of " + str(gasPrice_gwei) + " gwei, protocolFee_eth_weiUnits = " + str(protocolFee_eth_weiUnits) + " wei, or " + str(
        Libraries.core.ConvertWeiToEther(protocolFee_eth_weiUnits, Libraries.core.Ether_Decimals)) + " ETH")

elif sys.argv and len(sys.argv) >= 2 and "test0xapi" == sys.argv[1].lower():
    jData_tokens = Exchanges.zrxApi.API_GetTokens()
    # PrintAndLog("jData_tokens = " + str(jData_tokens))
    # for record in jData_tokens['records']:
    #     tokenAddress = record['address']
    #     PrintAndLog("Found tokenAddress " + str(tokenAddress))

    # jData_quote = Exchanges.zrxApi.API_GetQuote("0x6b175474e89094c44da98b954eedeac495271d0f", "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2", 100000000000000000, None)
    jData_quote = Exchanges.zrxApi.API_GetQuote("0x6b175474e89094c44da98b954eedeac495271d0f", "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2", None, 100000000000000000)
    # jData_quote = Exchanges.zrxApi.API_GetQuote("USDC", "WETH", 100000)
    PrintAndLog("jData_quote = " + str(jData_quote))
    PrintAndLog("jData_quote price = " + str(jData_quote['price']))
    # jData_quote = Exchanges.zrxApi.API_GetQuote("WETH", "USDC", 100000000000000000)
    # PrintAndLog("jData_quote price = " + str(jData_quote['price']))

    # jData_quote = Exchanges.zrxApi.API_GetAssetPairs('0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48')
    # baseTokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # quoteTokenAddress = '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2'
    # baseTokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # quoteTokenAddress = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'
    # baseTokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # quoteTokenAddress = '0x514910771af9ca656af840dff83e8264ecf986ca'
    # jData_quote = Exchanges.zrxApi.API_GetOrderbook(baseTokenAddress, quoteTokenAddress)
    #
    # PrintAndLog('jData_quote = ' + str(jData_quote))

# elif sys.argv and len(sys.argv) >= 2 and "test0xapi_websockets" == sys.argv[1].lower():
#     ConsiderUpdatingLatestBlockNumber()
#
#     Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
#
#     PopulateTokenDict_DaiAndSai_BugFix()
#     PopulateTokenDict_NinjaBetweenBancorAnd0x()
#     PopulateTokenDict_NinjaBetween0xAndKyber()
#     PopulateTokenDict_NinjaBetweenUniswapAnd0x()
#
#     Libraries.orderAggregator.CreateOrderAggregator_BasedOnTokenDict_Ninja(ExchangeName_0xMesh)
#
#     Exchanges.zrxApi.Set_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses(Libraries.orderAggregator.GetOrderAggregator_BasedOnTokenDict_Ninja)
#     Exchanges.zrxApi.ConnectWebSocketClient(ExchangeName_0xMesh)
#
#     while True:
#         PrintAndLog("Sleeping while I test websockets")
#         time.sleep(2)

elif sys.argv and len(sys.argv) >= 2 and "splitstring_tx_inputdata" == sys.argv[1].lower():
    # Example usage: splitstring_tx_inputdata 0x08f03b4f00000000000000000000000000000000000000000000000006f05b59d3b20000000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc20000000000000000000000000000000000000000000000023963217c0ed06000000000000000000000000000f37ed742819ec006b0802df5c2b0e9132f22c625

    myStringIWantToSplit = sys.argv[2]
    # Extract the function hash
    length = len('0x22742564')
    myStringIWantToSplit = myStringIWantToSplit[length:]
    splitLength = Libraries.core.LengthOfDataProperty
    # [line[i:i + n] for i in range(0, len(line), n)]
    splitArray = [myStringIWantToSplit[i:i + splitLength] for i in range(0, len(myStringIWantToSplit), splitLength)]
    PrintAndLog("splitArray = " + str(splitArray))
    assumedDecimalsForNumberConversions = Libraries.core.Ether_Decimals
    for thingy in splitArray:
        # PrintAndLog(" item: " + str(thingy))
        PrintAndLog(" item: " + str(thingy) + ", data interpreted as " + str(Libraries.core.GetStringContentsOfDataProperty(thingy, assumedDecimalsForNumberConversions)))

elif sys.argv and len(sys.argv) >= 2 and "splitstring_tx_inputdata_villain_ninja_0x8018280076d7fa2caa1147e441352e8a89e1ddbe" == sys.argv[1].lower():
    myStringIWantToSplit = "0x010706040413abadab42289c40eaeaeaafb1058cfd010119000000000000000000f43b2f981efc5a611a97951ce4fd7d3bd87f4902000000000000000000000000000000000000000000000003d1725d33ece8c000000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc20000000000000000000000006b175474e89094c44da98b954eedeac495271d0f000000000000000000000000000000000000000000000000000009184e72a00000000000000000000000000000000000000000000000000d8d726b7177a8000001010000000000000000000063825c174ab367968ec60f061753d3bbd36a0d8f0000000000000000000000006b175474e89094c44da98b954eedeac495271d0f00000000000000000000000000000000000000000000000000193958f883b3b4000000000000000000000000000000000000000000000775c63efc31bbc00000"
    assumedDecimalsForNumberConversions = Libraries.core.Ether_Decimals
    # assumedDecimalsForNumberConversions = None
    # This villain isn't sending data to the contract that's divisible by 64. So he must be sending data encoded differently, or in some new format i'm not aware of
    # Try and dynamically figure out what the format is and assume the custom stuff is at the beginning of the data
    # See this as an example: https://etherscan.io/tx/0x415d0cdecbfe397c947ca1a77baec35041fa213c1a0b02d66c377d2a8458c190
    # Extract the custom stuff, we're assuming the function hash is in the custom stuff but maybe that's wrong???  Or maybe the function hash is just longer in this case

    PrintAndLog("len of myStringIWantToSplit is " + str(len(myStringIWantToSplit)) + ", myStringIWantToSplit = " + str(myStringIWantToSplit))

    numOfDataProperties = len(myStringIWantToSplit) / Libraries.core.LengthOfDataProperty
    # PrintAndLog("numOfDataProperties = " + str(numOfDataProperties))
    numOfFullDataProperties = int(len(myStringIWantToSplit) / Libraries.core.LengthOfDataProperty)
    leftOverDataCharacters = len(myStringIWantToSplit) % Libraries.core.LengthOfDataProperty
    # PrintAndLog("numOfFullDataProperties = " + str(numOfFullDataProperties))
    # PrintAndLog("leftOverDataCharacters = " + str(leftOverDataCharacters))

    stuffIdontKnowWhatItIs = Libraries.core.GetFirstXCharsInString(myStringIWantToSplit, leftOverDataCharacters)
    PrintAndLog("stuffIdontKnowWhatItIs = " + str(stuffIdontKnowWhatItIs))
    myStringIWantToSplit = myStringIWantToSplit[leftOverDataCharacters:]
    splitLength = Libraries.core.LengthOfDataProperty
    # [line[i:i + n] for i in range(0, len(line), n)]
    splitArray = [myStringIWantToSplit[i:i + splitLength] for i in range(0, len(myStringIWantToSplit), splitLength)]
    # PrintAndLog("splitArray = " + str(splitArray))
    for thingy in splitArray:
        PrintAndLog(" item: " + str(thingy) + ", data interpreted as " + str(Libraries.core.GetStringContentsOfDataProperty(thingy, assumedDecimalsForNumberConversions)))

elif sys.argv and len(sys.argv) >= 2 and "splitstring" == sys.argv[1].lower():
    myStringIWantToSplit = "0x7cd442720000000000000000000000006b175474e89094c44da98b954eedeac495271d0f000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee0000000000000000000000000000000000000000000001a96c25fc7fea800000000000000000000000000000000000000000000000000000000000000093d979"
    splitLength = Libraries.core.LengthOfDataProperty
    # [line[i:i + n] for i in range(0, len(line), n)]
    splitArray = [myStringIWantToSplit[i:i + splitLength] for i in range(0, len(myStringIWantToSplit), splitLength)]
    PrintAndLog("splitArray = " + str(splitArray))
    for thingy in splitArray:
        PrintAndLog(" item: " + str(thingy))

elif sys.argv and len(sys.argv) >= 2 and "testoasisdex_prices" == sys.argv[1].lower():
    ConsiderUpdatingLatestBlockNumber()

    # # sending WETH receiving DAI
    # kenAddress, receiveTokenAddress, receiveAmount_etherUnits, decimals_receiveToken)

    # receiveAmount_weth_etherUnits = 10.0
    # sendTokenAddress = MarketDict['zrx-eth'].erc20TokenContractAddress
    # price, payAmount_token_etherUnits = Exchanges.oasisDex.API_GetDAIsellDetails(receiveAmount_weth_etherUnits, sendTokenAddress)
    # PrintAndLog("I can send " + str(payAmount_token_etherUnits) + " DAI, receive " + str(receiveAmount_weth_etherUnits) + " WETH, at price = " + str(price))
    #
    receiveAmount_token_etherUnits = 2
    receiveTokenAddress = ''
    price, payAmount_weth_etherUnits = Exchanges.oasisDex.API_GetDAIbuyDetails(receiveAmount_token_etherUnits, receiveTokenAddress)
    PrintAndLog("I can send " + str(payAmount_weth_etherUnits) + " WETH, receive " + str(receiveAmount_token_etherUnits) + " DAI, at price = " + str(price))
    #
    # sendAmount_weth_etherUnits = 10.0
    # receiveTokenAddress = MarketDict['zrx-eth'].erc20TokenContractAddress
    # price, receiveAmount_token_etherUnits = Exchanges.oasisDex.API_GetWETHsellDetails(sendAmount_weth_etherUnits, receiveTokenAddress)
    # PrintAndLog("I can receive " + str(receiveAmount_token_etherUnits) + " DAI, send " + str(sendAmount_weth_etherUnits) + " WETH, at price = " + str(price))

    sendAmount_token_etherUnits = 2
    sendTokenAddress = ''
    price, receiveAmount_weth_etherUnits = Exchanges.oasisDex.API_GetWETHbuyDetails(sendAmount_token_etherUnits, sendTokenAddress)
    PrintAndLog("I can receive " + str(receiveAmount_weth_etherUnits) + " WETH, send " + str(sendAmount_token_etherUnits) + " DAI, at price = " + str(price))

elif sys.argv and len(sys.argv) >= 2 and "testoasisdex_trade" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()

    fromAddress = "0x65c2698f4a2d5883d9ae6d22e4cb633d5371ce9d"
    fromPrivateKey = "pk"
    gas = Libraries.gasStation.GetGasLimit_TradeDaiMatchingMaker()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    # Buy DAI by calling API_OasisDex_SellAllAmount
    # sendTokenAddress = Exchanges.oasisDex.Contract_WETH
    # sendAmount_etherUnits = 0.0010
    # decimals_sendToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(sendTokenAddress)))
    # receiveTokenAddress = Exchanges.oasisDex.Contract_DAI
    # receiveMinFillAmount_etherUnits = 0.00000160637
    # decimals_receiveToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(receiveTokenAddress)))
    # Exchange_OasisDex.API_OasisDex_SellAllAmount(sendTokenAddress, sendAmount_etherUnits, decimals_sendToken,
    #                                     receiveTokenAddress, receiveMinFillAmount_etherUnits, decimals_receiveToken, gasPrice)

    # Sell DAI by calling API_OasisDex_SellAllAmount
    # sendTokenAddress = Exchanges.oasisDex.Contract_DAI
    # sendAmount_etherUnits = 0.1
    # decimals_sendToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(sendTokenAddress)))
    # receiveTokenAddress = Exchanges.oasisDex.Contract_WETH
    # receiveMinFillAmount_etherUnits = 0.000161309
    # decimals_receiveToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(receiveTokenAddress)))
    # Exchange_OasisDex.API_OasisDex_SellAllAmount(sendTokenAddress, sendAmount_etherUnits, decimals_sendToken,
    #                                     receiveTokenAddress, receiveMinFillAmount_etherUnits, decimals_receiveToken, gasPrice)

    # Buy DAI by calling API_OasisDex_BuyAllAmount
    # receiveTokenAddress = Exchanges.oasisDex.Contract_DAI
    # receiveAmount_etherUnits = 0.1
    # decimals_receiveToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(receiveTokenAddress)))
    # sendTokenAddress = Exchanges.oasisDex.Contract_WETH
    # sendMaxFillAmount_etherUnits = 0.000191243
    # decimals_sendToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(sendTokenAddress)))
    # Exchange_OasisDex.API_OasisDex_BuyAllAmount(receiveTokenAddress, receiveAmount_etherUnits, decimals_receiveToken,
    #                                    sendTokenAddress, sendMaxFillAmount_etherUnits, decimals_sendToken, gasPrice)

    # Sell DAI by calling API_OasisDex_BuyAllAmount
    # receiveTokenAddress = Exchanges.oasisDex.Contract_WETH
    # receiveAmount_etherUnits = 0.000181243
    # decimals_receiveToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(receiveTokenAddress)))
    # sendTokenAddress = Exchanges.oasisDex.Contract_DAI
    # sendMaxFillAmount_etherUnits = 0.11
    # decimals_sendToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(sendTokenAddress)))
    # Exchange_OasisDex.API_OasisDex_BuyAllAmount(receiveTokenAddress, receiveAmount_etherUnits, decimals_receiveToken,
    #                                    sendTokenAddress, sendMaxFillAmount_etherUnits, decimals_sendToken, gasPrice)

    # receiveAmount_dai_etherUnits = 1
    # price, payAmount_weth_etherUnits = Exchanges.oasisDex.API_GetDAIbuyDetails(receiveAmount_dai_etherUnits)
    # PrintAndLog("I can send " + str(payAmount_weth_etherUnits) + " WETH, receive " + str(receiveAmount_dai_etherUnits) + " DAI, at price = " + str(price))
    # MarketDict["dai-eth"].GetExchange(ExchangeName_OasisDex).PerformMarketOrder(receiveAmount_dai_etherUnits, "buy")

    # sendAmount_dai_etherUnits = 0.01
    # price, receiveAmount_weth_etherUnits = Exchanges.oasisDex.API_GetWETHbuyDetails(sendAmount_dai_etherUnits)
    # PrintAndLog("I can receive " + str(receiveAmount_weth_etherUnits) + " WETH, send " + str(sendAmount_dai_etherUnits) + " DAI, at price = " + str(price))
    # MarketDict["dai-eth"].GetExchange(ExchangeName_OasisDex).PerformMarketOrder(sendAmount_dai_etherUnits, "sell")

elif sys.argv and len(sys.argv) >= 2 and "testkyber" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()

    marketName = "mkr-eth"

    # Buying tokens
    sourceAddress = Exchanges.kyber.EtherToken
    destinationAddress = ''
    sourceQuantity_etherUnits = 100

    # Selling tokens
    # destinationAddress = Exchanges.kyber.EtherToken
    # sourceAddress = MarketDict[marketName].erc20TokenContractAddress
    # sourceQuantity_etherUnits = 50

    decimals_sourceQuantity = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(sourceAddress)))
    Exchanges.kyber.API_GetExpectedRate(sourceAddress, destinationAddress, sourceQuantity_etherUnits, decimals_sourceQuantity)
    Exchanges.kyber.API_FindBestRate(sourceAddress, destinationAddress, sourceQuantity_etherUnits, decimals_sourceQuantity)
    Exchanges.kyber.API_SearchBestRate(sourceAddress, destinationAddress, sourceQuantity_etherUnits, decimals_sourceQuantity)

    # Buying tokens
    sourceAddress = Exchanges.kyber.EtherToken
    destinationAddress = ''
    sourceQuantityList_etherUnits = [0.01, 0.1, 1, 10, 100]
    decimals_sourceQuantity = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(sourceAddress)))
    Libraries.arbyUtility.API_Kyber_GetExpectedRates(Exchanges.kyber.Contract_KyberNetworkProxy, sourceAddress, destinationAddress, sourceQuantityList_etherUnits,
                                                     decimals_sourceQuantity)

    # Selling tokens
    destinationAddress = Exchanges.kyber.EtherToken
    sourceAddress = ''
    sourceQuantityList_etherUnits = [4.9, 49.0, 490, 4900, 49000]

    decimals_sourceQuantity = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(sourceAddress)))
    Libraries.arbyUtility.API_Kyber_GetExpectedRates(Exchanges.kyber.Contract_KyberNetworkProxy, sourceAddress, destinationAddress, sourceQuantityList_etherUnits,
                                                     decimals_sourceQuantity)

    # PrintAndLog("sourceQuantityList_etherUnits[0] = " + str(sourceQuantityList_etherUnits[0]))
    # Exchanges.kyber.API_GetExpectedRate(sourceAddress, destinationAddress, sourceQuantityList_etherUnits[0], decimals_sourceQuantity)

elif sys.argv and len(sys.argv) >= 2 and "testkyber_getreserves_tradeenabled" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # tradableKyberReserveDict = Libraries.cache.GetDataFromCache("Exchanges.kyber.GetTradeableReserves")

    # tradeEnabled = Exchanges.kyber.API_GetTradeEnabled("0x63825c174ab367968ec60f061753d3bbd36a0d8f")
    # PrintAndLog("tradeEnabled = " + str(tradeEnabled))

    specifiedBlockNumber_int = 9088315  # Fails???
    # specifiedBlockNumber_int = 9088015  #
    tradeEnabled = Exchanges.kyber.API_GetTradeEnabled("0xa467b88bbf9706622be2784af724c4b44a9d26f4", specifiedBlockNumber_int)
    PrintAndLog("tradeEnabled = " + str(tradeEnabled))

    # tradableKyberReserveList = list(tradableKyberReserveDict.keys())
    # # PrintAndLog("tradableKyberReserveList = " + str(tradableKyberReserveList))
    # resultDict = Exchanges.kyber.API_GetTradeEnabled_Batched(tradableKyberReserveList)
    # PrintAndLog("resultDict = " + str(resultDict))

elif sys.argv and len(sys.argv) >= 2 and "testkyber_getreserves_tradeenabled_watch_loop" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    tradableKyberReserveDict = Libraries.cache.GetDataFromCache("Exchanges.kyber.GetTradeableReserves")
    tradableKyberReserveList = list(tradableKyberReserveDict.keys())

    resultDict = None
    while True:
        newResultDict = Exchanges.kyber.API_GetTradeEnabled_Batched(tradableKyberReserveList)
        PrintAndLog("newResultDict = " + str(newResultDict))
        if resultDict:
            if newResultDict != resultDict:
                message = "KyberReserve tradeEnabled has changed!"
                Libraries.core.API_PostOperatorNotification(message)
                PrintAndLog("resultDict = " + str(resultDict))
                PrintAndLog("newResultDict = " + str(newResultDict))
            else:
                message = "KyberReserve tradeEnabled has NOT changed!"
                PrintAndLog(message)

        resultDict = newResultDict

        time.sleep(60)

elif sys.argv and len(sys.argv) >= 2 and "testkyber_getconversionrates_batched" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # tradableKyberReserveDict = Libraries.cache.GetDataFromCache("Exchanges.kyber.GetTradeableReserves")

    arrayLen = 3
    kyberReserveAddressList = ['0x63825c174ab367968ec60f061753d3bbd36a0d8f'] * arrayLen
    srcList = ['0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE'] * arrayLen
    destList = ['0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'] * arrayLen
    srcQtyList = [100000000000000, 40000000000000000000, 800000000000000000000]

    if len(srcQtyList) != arrayLen:
        raise Exception("Lengths need to be the same")

    resultDict_kyberRates = Exchanges.kyber.API_GetConversionRate_Batched_GivenAllLists(kyberReserveAddressList, srcList, destList, srcQtyList)
    PrintAndLog("resultDict_kyberRates = " + str(resultDict_kyberRates))

elif sys.argv and len(sys.argv) >= 2 and "testkyber_getconversionratescontract" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # kyberReserveAddress = '0x63825c174ab367968ec60f061753d3bbd36a0d8f'
    # contractAddress = Exchanges.kyber.API_GetReservesConversionRateContract(kyberReserveAddress)
    # PrintAndLog("contractAddress = " + str(contractAddress))

    # kyberReserveAddressList = ['0x63825c174ab367968ec60f061753d3bbd36a0d8f', '0x2ed6f2bc006da5897a0c3cd2686283c05e50c573', '0x45eb33d008801d547990caf3b63b4f8ae596ea57']
    kyberReserveAddressList = ['0x63825c174ab367968ec60f061753d3bbd36a0d8f', '0x4cb01bd05e4652cbb9f312ae604f4549d2bf2c99', '0x2295fc6bc32cd12fdbb852cff4014ceac6d79c10',
                               '0x742e8bb8e6bde9cb2df5449f8de7510798727fb1', '0x57f8160e1c59d16c01bbe181fd94db4e56b60495', '0x3e9ffba3c3eb91f501817b031031a71de2d3163b',
                               '0x0232ba609782cea145ec3663f52cf7aeb4ac773c', '0xa33c7c22d0bb673c2aea2c048bb883b679fa1be9', '0x1d57ef26709beb756e026308413f685339a73a9d',
                               '0x751eea622edd1e3d768c18afbcaec7dce7750c65', '0x45eb33d008801d547990caf3b63b4f8ae596ea57', '0x31e085afd48a1d6e51cc193153d625e8f0514c7f',
                               '0xa467b88bbf9706622be2784af724c4b44a9d26f4', '0x1670dfb52806de7789d5cf7d5c005cf7083f9a5d', '0xaa14dcaa0adbe79cbf00edc6cc4ed17ed39240ac',
                               '0x7a3370075a54b187d7bd5dcebf0ff2b5552d4f7d', '0x05461124c86c0ad7c5d8e012e1499fd9109ffb7d', '0x1833ad67362249823515b59a8aa8b4f6b4358d1b',
                               '0x302b35bd0b01312ec2652783c04955d7200c3d9b', '0x6b84dbd29643294703dbabf8ed97cdef74edd227', '0x7e2fd015616263add31a2acc2a437557cee80fc4',
                               '0x485c4ec93d18ebd16623d455567886475ae28d04', '0xa107dfa919c3f084a7893a260b99586981beb528', '0x3480e12b6c2438e02319e34b4c23770679169190',
                               '0x08030715560a146e306b87ca93fd618bb2a80363', '0x95f1f428485bd41729938d620af61718ea9b1f9e', '0xb45c8956a080d336934cee52a35d4dbabf025b6f',
                               '0x28ad54a7208608d17b8d4a4a4034f254a28df85e', '0x8ea5cf9f61824e8a3ca8aa370ab37e0202b2cc7d', '0xa9742ee9a5407f4c2f8a49f65e3a440f3694960a',
                               '0x607d7751d9f4845c5a1de9eed39c56f4fc0f855d', '0xc6c8bce5e9383df025f982d6bbd84163957a6979', '0x1fe867bfe9cbe0045467605b959a355223e3885d',
                               '0x4e6d0f492fd139151de4728cac47dace56c56af4', '0x0994c18ed0c328f38d2c451b2a2e1ceb1ae6a812', '0x2485a4e3dd95a3ef445b786acf7bacc5c99986f7',
                               '0x4f32bbe8dfc9efd54345fc936f9fef1048746fcf', '0x9e3db227cf04fbf8ac180bf8b6acd64cd0420d32', '0x977c9abb01ed3e99e9953fd1f472ae9f459e7e70',
                               '0x1e158c0e93c30d24e918ef83d1e0be23595c3c0f', '0x2ed6f2bc006da5897a0c3cd2686283c05e50c573', '0x55a8fda671a257b80258d2a03abd6e0e1e3dbe79',
                               '0x0d1267ae18d7e23a3ab0243cd4ab8a3bc4e1eee2', '0xfe06bc8bc12595c1c871ff7c2ea9cadc42735d7d', '0x0ce59e811024c4aa040389fb8917dd9edaef1693',
                               '0xb89f41cd2c8b6cba8b851289198b06be8b4dec65', '0xb06cf173da7e297aa6268139c7cb67c53d8e4f90', '0x141104687b51985d6210eb4b398f1dc5b5b9e9f5',
                               '0x0b798b89155ea31f1312791b9fdfaae7c5f48460', '0x3e59c69952a4cfeaf653eedf8ff907d4b6b8762d', '0x10db2a136ee3e0c963d82af4c86ca483199f2816']
    contractAddressList = Exchanges.kyber.API_GetReservesConversionRateContract_Batched(kyberReserveAddressList)
    PrintAndLog("contractAddressList = " + str(contractAddressList))

elif sys.argv and len(sys.argv) >= 2 and "testkyber_getreservesconversionrateupdateblock" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # kyberReserveConversionRatesContract = '0x798abda6cc246d0edba912092a2a3dbd3d11191b'
    # tokenAddress = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'
    # blockNumber = Exchanges.kyber.API_GetReservesConversionRateUpdateBlock(kyberReserveConversionRatesContract, tokenAddress)
    # PrintAndLog("blockNumber = " + str(blockNumber))

    # tokenAddress = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'
    tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # tokenAddress = '0xa3d58c4e56fedcae3a7c43a725aee9a71f0ece4e'
    # tokenAddress = Exchanges.kyber.EtherToken
    tradableKyberReserveDict = Libraries.cache.GetDataFromCache("Exchanges.kyber.GetTradeableReserves")
    for kyberReserve in tradableKyberReserveDict:
        kyberReserveConversionRatesContract = tradableKyberReserveDict[kyberReserve]['conversionRateContract']
        didSucceed = False
        try:
            blockNumber = Exchanges.kyber.API_GetReservesConversionRateUpdateBlock(kyberReserveConversionRatesContract, tokenAddress)
            didSucceed = True

        except:
            # try again a second time, just in case the call failed due to some fluke internet reason
            time.sleep(0.05)
            try:
                blockNumber = Exchanges.kyber.API_GetReservesConversionRateUpdateBlock(kyberReserveConversionRatesContract, tokenAddress)
                didSucceed = True

            except:
                PrintAndLogError("kyberReserve " + str(kyberReserve) + " does not seem to support the function API_GetReservesConversionRateUpdateBlock. "
                                                                       "We cannot use it for this kyber reserve")

        if didSucceed:
            symbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)
            PrintAndLog("kyberReserve " + str(kyberReserve) + " last updated it's rate for " + str(symbol) + " on blockNumber = " + str(blockNumber))

elif sys.argv and len(sys.argv) >= 2 and "testkyber_getreserves" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    # tradableKyberReserveDict = Exchanges.kyber.GetTradeableReserves()
    # PrintAndLog("tradableKyberReserveDict = " + str(tradableKyberReserveDict))
    Libraries.cache.GetAndCache_Kyber_GetTradeableReserves()

elif sys.argv and len(sys.argv) >= 2 and "testkyber_getconversionrate" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # # src = Exchanges.kyber.EtherToken
    # # dest = "0x744d70fdbe2ba4cf95131626614a1763df805b9e"
    # dest = Exchanges.kyber.EtherToken
    # src = "0x744d70fdbe2ba4cf95131626614a1763df805b9e"
    # srcQty = 1000000
    # kyberReserveAddress = "0x63825c174ab367968ec60f061753d3bbd36a0d8f"
    # Exchanges.kyber.API_GetConversionRate(kyberReserveAddress, src, dest, srcQty)

    src = Exchanges.kyber.EtherToken
    dest = "0xdd974d5c2e2928dea5f71b9825b8b646686bd200"
    srcQty = 533371973486104448
    specifiedBlockNumber_int = 9088314
    rate = Exchanges.kyber.API_GetConversionRate("0xa467b88bbf9706622be2784af724c4b44a9d26f4", src, dest, srcQty, specifiedBlockNumber_int)
    PrintAndLog("rate = " + str(rate))

elif sys.argv and len(sys.argv) >= 2 and "testkyber_gasprice" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.kyber.API_GetMaxGasPriceValue()

elif sys.argv and len(sys.argv) >= 2 and "testapproves_ninja_setkyber0xuniswap" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    # Get and cache data we need from various DEXs

    # Short calls, make them all the time
    Libraries.cache.GetAndCache_RadarRelay_API_GetListOfTokenAddresses()
    Libraries.cache.GetAndCache_Kyber_API_GetListOfTokenAddresses()
    Libraries.cache.GetAndCache_Bancor_API_GetAllCurrencies()
    Libraries.cache.GetAndCache_Set_RebalancingSets()

    # Long calls only need to be called once in a while
    if Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.uniswap.API_GetExchangeDataDict') > 1.0:
        Libraries.cache.GetAndCache_Uniswap_API_GetExchangeDataDict()

    if Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.kyber.GetTradeableReserves') > 1.0:
        Libraries.cache.GetAndCache_Kyber_GetTradeableReserves()

    Exchanges.set.PopulateSetTokenDataDict()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    ninjaName = "ninja-op-100"
    publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()
    gas = Libraries.gasStation.GetGasLimit_Other()
    # gasPrice = Libraries.core.ConvertGweiToWei(2.01)
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
    # NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    nonceToUse = Libraries.core.API_GetTransactionCount(publicAddress_Ninja)
    PrintAndLog("Exchanges.set.GetListOfTradableTokens() = " + str(Exchanges.set.GetListOfTradableTokens(NinjaObject)))

    underlyingTokensList = []
    # CTokenListSubstitutions.append((Contract_USDC, Exchanges.compound.Contract_cUSDC))
    PrintAndLog("Exchanges.set.CTokenListSubstitutions = " + str(Exchanges.set.CTokenListSubstitutions))
    for pair in Exchanges.set.CTokenListSubstitutions:
        originalTokenAddress = pair[0]
        # compoundTokenAddress = pair[1]
        underlyingTokensList.append(originalTokenAddress.lower())

    PrintAndLog("underlyingTokensList = " + str(underlyingTokensList))

    listOfAllTokensToApprove = []
    for address in Exchanges.set.GetListOfTradableTokens(NinjaObject):
        if address.lower() not in listOfAllTokensToApprove:
            listOfAllTokensToApprove.append(address.lower())

    for address in underlyingTokensList:
        if address.lower() not in listOfAllTokensToApprove:
            listOfAllTokensToApprove.append(address.lower())

    PrintAndLog("listOfAllTokensToApprove = " + str(listOfAllTokensToApprove))

    # Get a list of tokens that we'll trade
    for tokenAddress in listOfAllTokensToApprove:
        # Kyber needs to move our token
        if NinjaObject.ApproveContractForTokenTransfer(publicAddress_Ninja, privateKey_Ninja, tokenAddress, Exchanges.kyber.Contract_KyberNetworkProxy,
                                                       nonceToUse, False, True, gas, gasPrice):
            nonceToUse += 1

        # Set needs to move our token
        if NinjaObject.ApproveContractForTokenTransfer(publicAddress_Ninja, privateKey_Ninja, tokenAddress, Exchanges.set.Contract_TransferProxy,
                                                       nonceToUse, False, True, gas, gasPrice):
            nonceToUse += 1

        # Set needs to move our token
        if NinjaObject.ApproveContractForTokenTransfer(publicAddress_Ninja, privateKey_Ninja, tokenAddress, Exchanges.set.Contract_SetCTokenBidderContract,
                                                       nonceToUse, False, True, gas, gasPrice):
            nonceToUse += 1

        # 0xv2 needs to move our token
        if NinjaObject.ApproveContractForTokenTransfer(publicAddress_Ninja, privateKey_Ninja, tokenAddress, Exchanges.zrxV2.Contract_ERC20Proxy,
                                                       nonceToUse, False, True, gas, gasPrice):
            nonceToUse += 1

        # Uniswap needs to move our token
        if NinjaObject.ApproveContractForTokenTransfer(publicAddress_Ninja, privateKey_Ninja, tokenAddress,
                                                       Exchanges.uniswap.ContractDict_Exchange[tokenAddress.lower()]['exchange'],
                                                       nonceToUse, False, True, gas, gasPrice):
            nonceToUse += 1

elif sys.argv and len(sys.argv) >= 2 and "testapproves_ninja_kyberbancoruniswapand0x_specifictokenonly" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    PopulateTokenDict_DaiAndSai_BugFix()
    # PopulateTokenDict_NinjaBetweenBancorAnd0x()
    # PopulateTokenDict_NinjaBetweenBancorAndKyber()
    # PopulateTokenDict_NinjaBetween0xAndKyber()
    PopulateTokenDict_NinjaBetweenUniswapAndKyber()
    # PopulateTokenDict_NinjaBetweenUniswapAnd0x()

    ninjaName = "ninja-op-100"
    publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.A)

    tokenName = 'snt'
    token = TokenDict_Ninja[tokenName]
    tokenToApprove = token.erc20TokenContractAddress
    gas = Libraries.gasStation.GetGasLimit_Other()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.core.ConvertGweiToWei(1.1)

    nonceToUse = Libraries.core.API_GetTransactionCount(publicAddress_Ninja)

    spenderToApproveList = []
    spenderToApproveList.append(Exchanges.kyber.Contract_KyberNetworkProxy)
    # spenderToApproveList.append(Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract))
    # spenderToApproveList.append(Exchanges.zrxV2.Contract_ERC20Proxy)
    spenderToApproveList.append(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])
    for spenderToApprove in spenderToApproveList:
        PrintAndLog("Calling ApproveContractForTokenTransfer on tokenToApprove = " + str(tokenToApprove) + " with spenderToApprove = " + str(
            spenderToApprove) + " and nonceToUse = " + str(nonceToUse))
        if NinjaObject.ApproveContractForTokenTransfer(publicAddress_Ninja, privateKey_Ninja, tokenToApprove, spenderToApprove, nonceToUse,
                                                       False, True, gas, gasPrice):
            nonceToUse += 1

elif sys.argv and len(sys.argv) >= 2 and "testapproves_ninja_kyberbancoruniswapand0x_allpossibletokens" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    # Get and cache data we need from various DEXs

    # Short calls, make them all the time
    Libraries.cache.GetAndCache_RadarRelay_API_GetListOfTokenAddresses()
    Libraries.cache.GetAndCache_Kyber_API_GetListOfTokenAddresses()
    Libraries.cache.GetAndCache_Bancor_API_GetAllCurrencies()
    Libraries.cache.GetAndCache_Set_RebalancingSets()

    # Long calls only need to be called once in a while
    if Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.uniswap.API_GetExchangeDataDict') > 1.0:
        Libraries.cache.GetAndCache_Uniswap_API_GetExchangeDataDict()

    if Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.kyber.GetTradeableReserves') > 1.0:
        Libraries.cache.GetAndCache_Kyber_GetTradeableReserves()

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    PopulateTokenDict_DaiAndSai_BugFix()
    # PopulateTokenDict_NinjaBetweenBancorAnd0x()
    # PopulateTokenDict_NinjaBetweenBancorAndKyber()
    # PopulateTokenDict_NinjaBetween0xAndKyber()
    PopulateTokenDict_NinjaBetweenUniswapAndKyber()

    ninjaName = "ninja-op-100"
    publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()
    gas = Libraries.gasStation.GetGasLimit_Other()

    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.core.ConvertGweiToWei(3.000003)
    # gasPrice = Libraries.core.ConvertGweiToWei(0.800003)

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.A)

    nonceToUse = Libraries.core.API_GetTransactionCount(publicAddress_Ninja)

    for tokenName in TokenDict_Ninja:
        PrintAndLog("tokenName = " + str(tokenName))
        token = TokenDict_Ninja[tokenName]
        tokenToApprove = token.erc20TokenContractAddress
        PrintAndLog("found tokenName " + str(tokenName) + " whose tokenAddress = " + str(tokenToApprove))
        if token.IsTokenEnabledAtLeastOnePlace():
            PrintAndLog("tokenName " + str(tokenName) + " is enabled! tokenAddress = " + str(tokenToApprove))

            spenderToApproveList = []

            # Append this to list when approving Kyber
            spenderToApproveList.append(Exchanges.kyber.Contract_KyberNetworkProxy)

            # # Append this to list when approving Bancor
            # if token.bancorTokenConverterContract:
            #     spenderToApproveList.append(Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract))
            # else:
            #     PrintAndLog("Did not find " + str(tokenName.upper()) + "'s token.bancorTokenConverterContract. Ignoring it and moving on.")

            # # Append this to list when approving 0xv2
            # spenderToApproveList.append(Exchanges.zrxV2.Contract_ERC20Proxy)

            # Append this to list when approving Uniswap
            spenderToApproveList.append(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

            # Consider issuing approvals for all spenders in the spenderToApproveList
            for spenderToApprove in spenderToApproveList:
                PrintAndLog("Calling ApproveContractForTokenTransfer on tokenToApprove = " + str(tokenToApprove) + " with spenderToApprove = " + str(
                    spenderToApprove) + " and nonceToUse = " + str(nonceToUse))
                if NinjaObject.ApproveContractForTokenTransfer(publicAddress_Ninja, privateKey_Ninja, tokenToApprove, spenderToApprove,
                                                               nonceToUse, False, True, gas, gasPrice):
                    nonceToUse += 1

elif sys.argv and len(sys.argv) >= 2 and "testapproves_ninja_keeperdao" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # Approve KeeperDAOs transfer proxy to move Ninja's ERC20 tokens... I forget why I need to do this

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    # Get and cache data we need from various DEXs

    # Short calls, make them all the time
    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    ninjaName = "ninja-op-100"
    publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()
    gas = Libraries.gasStation.GetGasLimit_Other()
    # gasPrice = Libraries.core.ConvertGweiToWei(2.01)
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    # NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    doIssueApproves = True

    borrowableQuoteTokensList = Exchanges.keeperDAO.GetBorrowableQuoteTokensList()
    PrintAndLog("borrowableQuoteTokensList = " + str(borrowableQuoteTokensList))
    borrowableQuoteTokensList = ConvertListOfStringsToLowercaseListOfStrings(borrowableQuoteTokensList)
    PrintAndLog("borrowableQuoteTokensList = " + str(borrowableQuoteTokensList))
    # approvableTokensList = set(tradeableTokensList).intersection(borrowableQuoteTokensList)
    # PrintAndLog("approvableTokensList = " + str(approvableTokensList))
    approvableTokensList = borrowableQuoteTokensList

    nonceToUse = Libraries.core.API_GetTransactionCount(publicAddress_Ninja)
    # Get a list of tokens that we'll trade
    for tokenAddress in approvableTokensList:
        PrintAndLog("Approve KeeperDAOLiquidityProviderAssetProxyAddress " + str(KeeperDAOLiquidityProviderAssetProxyAddress) + " to move our token " + str(tokenAddress))
        # KeeperDAO needs to move our token
        if doIssueApproves:
            if NinjaObject.ApproveContractForTokenTransfer(publicAddress_Ninja, privateKey_Ninja, tokenAddress, KeeperDAOLiquidityProviderAssetProxyAddress,
                                                           nonceToUse, False, True, gas, gasPrice):
                nonceToUse += 1

elif sys.argv and len(sys.argv) >= 2 and "testninja" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    ninjaOp = NinjaOpAccountDict["ninja-op-3"]

    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()
    gas = Libraries.gasStation.GetGasLimit_Other()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()

    ninjaType = Exchanges.ninja.NinjaType.A
    NinjaObject = Exchanges.ninja.CreateNinjaObject(ninjaType)
    version_utility = NinjaObject.API_GetVersion_Utility()
    version_ninja = NinjaObject.API_GetVersion_Ninja()
    PrintAndLog("version_utility = " + str(version_utility) + " for ninjaType = " + str(ninjaType.value))
    PrintAndLog("version_ninja = " + str(version_ninja) + " for ninjaType = " + str(ninjaType.value))
    PrintAndLog("Ninja Address in use = " + str(NinjaObject.contractAddress_Ninja))
    PrintAndLog("Utility Address in use = " + str(NinjaObject.contractAddress_Utility))

    ninjaType = Exchanges.ninja.NinjaType.B
    NinjaObject = Exchanges.ninja.CreateNinjaObject(ninjaType)
    version_utility = NinjaObject.API_GetVersion_Utility()
    version_ninja = NinjaObject.API_GetVersion_Ninja()
    PrintAndLog("version_utility = " + str(version_utility) + " for ninjaType: " + str(ninjaType.value))
    PrintAndLog("version_ninja = " + str(version_ninja) + " for ninjaType: " + str(ninjaType.value))
    PrintAndLog("Ninja Address in use = " + str(NinjaObject.contractAddress_Ninja))
    PrintAndLog("Utility Address in use = " + str(NinjaObject.contractAddress_Utility))

elif sys.argv and len(sys.argv) >= 2 and "testninja_send_ether_to_ninjaops" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()
    Libraries.bloxroute.ConnectToAllWebSocketClients()

    doSendEther = False
    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()

    listOfDestinations = []
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-100"].publicAddress)
    listOfDestinations.append(NinjaOpAccountDict["ninja-op-102"].publicAddress)
    listOfDestinations.append(NinjaOpAccountDict["ninja-op-103"].publicAddress)
    listOfDestinations.append(NinjaOpAccountDict["ninja-op-104"].publicAddress)
    listOfDestinations.append(NinjaOpAccountDict["ninja-op-105"].publicAddress)
    listOfDestinations.append(NinjaOpAccountDict["ninja-op-106"].publicAddress)
    listOfDestinations.append(NinjaOpAccountDict["ninja-op-107"].publicAddress)
    listOfDestinations.append(NinjaOpAccountDict["ninja-op-108"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-109"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-110"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-111"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-112"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-113"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-114"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-116"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-117"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-118"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-119"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-120"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-121"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-122"].publicAddress)
    # listOfDestinations.append(NinjaOpAccountDict["ninja-op-123"].publicAddress)

    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()
    # gasPrice = Libraries.gasStation.ConvertGweiToWei(78)
    value_ether_int = 6
    value_wei_int = Libraries.core.ConvertEtherToWei(value_ether_int, Libraries.core.Ether_Decimals)

    nonce = Libraries.core.API_GetTransactionCount(publicAddress_Ninja)
    for destinationAddress in listOfDestinations:
        PrintAndLog("Sending " + str(value_ether_int) + " Ether (or " + str(value_wei_int) + " wei) to " + str(destinationAddress) + " with nonce = " + str(nonce))
        if doSendEther:
            txId = API_SendEther_ToAddress(publicAddress_Ninja, privateKey_Ninja, destinationAddress, value_wei_int,
                                           Libraries.gasStation.GetGasLimit_SendingEther(), gasPrice, TransactionType.other, nonce)

        nonce += 1

elif sys.argv and len(sys.argv) >= 2 and "testninja_send_ether_to_ninjaops_from_self_to_self" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()
    Libraries.bloxroute.ConnectToAllWebSocketClients()

    doSendEther = True

    ninjaOpsList = []
    ninjaOpsList.append(NinjaOpAccountDict["ninja-op-116"])
    ninjaOpsList.append(NinjaOpAccountDict["ninja-op-117"])
    ninjaOpsList.append(NinjaOpAccountDict["ninja-op-118"])
    ninjaOpsList.append(NinjaOpAccountDict["ninja-op-119"])
    ninjaOpsList.append(NinjaOpAccountDict["ninja-op-120"])
    ninjaOpsList.append(NinjaOpAccountDict["ninja-op-121"])
    ninjaOpsList.append(NinjaOpAccountDict["ninja-op-122"])
    ninjaOpsList.append(NinjaOpAccountDict["ninja-op-123"])

    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()
    # gasPrice = Libraries.gasStation.ConvertGweiToWei(78)
    value_ether_int = 0
    value_wei_int = Libraries.core.ConvertEtherToWei(value_ether_int, Libraries.core.Ether_Decimals)

    for ninjaOp in ninjaOpsList:
        publicAddress_Ninja = ninjaOp.publicAddress
        privateKey_Ninja = ninjaOp.DecryptPK()
        nonce = 0
        PrintAndLog("Sending " + str(value_ether_int) + " Ether (or " + str(value_wei_int) + " wei) to " + str(publicAddress_Ninja) + " with nonce = " + str(nonce))
        if doSendEther:
            txId = API_SendEther_ToAddress(publicAddress_Ninja, privateKey_Ninja, publicAddress_Ninja, value_wei_int,
                                           Libraries.gasStation.GetGasLimit_SendingEther(), gasPrice, TransactionType.other, nonce)

elif sys.argv and len(sys.argv) >= 2 and "testninja_sweep_ether_from_ninjaops" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    ninjaOp = NinjaOpAccountDict["ninja-op-108"]

    ninjaAccounts = []
    ninjaAccounts.append(NinjaOpAccountDict["ninja-op-109"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-3"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-4"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-5"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-6"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-7"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-8"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-9"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-10"])
    # ninjaAccounts.append(NinjaOpAccountDict["ninja-op-11"])

    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    for ninjaAccount in ninjaAccounts:
        SweepEtherFromAccount(ninjaAccount.publicAddress, ninjaAccount.DecryptPK(), ninjaOp.publicAddress, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_withdrawether" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()
    Libraries.bloxroute.ConnectToAllWebSocketClients()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    # ninjaOp = SetSocialTestAccount

    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()
    gas = Libraries.gasStation.GetGasLimit_Other()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()
    # gasPrice = Libraries.gasStation.ConvertGweiToWei(90)

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.Generic)

    # Withdraw ALL Ether from contract
    # NinjaObject.API_WithdrawAllEther(publicAddress_Ninja, privateKey_Ninja, gas, gasPrice)

    # Withdraw specific amount of Ether from contract
    quantity_etherUnits = 20
    NinjaObject.API_WithdrawEther(publicAddress_Ninja, privateKey_Ninja, quantity_etherUnits, gas, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_withdrawtokens" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]

    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()
    # gasPrice = Libraries.gasStation.ConvertGweiToWei(90)

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.Generic)

    # tokenAddress = Libraries.gasToken_CHI.Contract_CHI
    # tokenAddress = '0x0000000000b3f879cb30fe243b4dfee438691c04'  # gst2
    tokenAddress = '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2'  # weth
    # tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'  # usdc
    # tokenAddress = '0xdac17f958d2ee523a2206206994597c13d831ec7'  # usdt
    # tokenAddress = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'  # wbtc
    # tokenAddress = "0x6B175474E89094C44Da98b954EedeAC495271d0F"  # dai
    # tokenAddress = '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359'  # sai
    # tokenAddress = '0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2'  # mkr
    # tokenAddress = '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984'  # uni
    # tokenAddress = '0xeb4c2781e4eba804ce9a9803c67d0893436bb27d'  # renBTC
    # tokenAddress = '0xfa5047c9c78b8877af97bdcb85db743fd7313d4a'  # rook
    # tokenAddress = '0x0000000000004946c0e9F43F4Dee607b0eF1fA1c'  # chi

    nonce = None
    # nonce = 2711

    # Withdraw specific quantity of tokens
    quantity_etherUnits = 36
    quantity_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(quantity_etherUnits, tokenAddress)
    NinjaObject.API_WithdrawTokens(publicAddress_Ninja, privateKey_Ninja, tokenAddress, quantity_weiUnits, gasPrice)

    # Withdraw all tokens
    # NinjaObject.API_WithdrawAllTokens(publicAddress_Ninja, privateKey_Ninja, tokenAddress, gasPrice, nonce)

elif sys.argv and len(sys.argv) >= 2 and "testninja_getproperties" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    gas = Libraries.gasStation.GetGasLimit_Other()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()

    Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.Generic).PrintProperties()

elif sys.argv and len(sys.argv) >= 2 and "testninja_setaddresses" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]

    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.Generic)

    NinjaObject.API_SetAddresses(publicAddress_Ninja, privateKey_Ninja, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_setints" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]

    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.Generic)

    NinjaObject.API_SetInts(publicAddress_Ninja, privateKey_Ninja, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_mintgastokens" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    account = NinjaOpAccountDict["ninja-op-100"]
    # account = NinjaOpAccountDict["ninja-op-102"]

    # TODO create new minting accounts to replace TestAccount_JoeyZ_0015 and TestAccount_JoeyZ_65C2

    publicAddress_Ninja = account.publicAddress
    privateKey_Ninja = account.DecryptPK()

    numOfTransactionsToQueue = 30

    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()

    # Approx 37k per minted gas token + 24k base to issue transaction

    # gasPrice = Libraries.core.ConvertGweiToWei(0.800002)
    # numOfTokensToMint = 140

    gasPrice = Libraries.core.ConvertGweiToWei(16.100001)
    numOfTokensToMint = 140

    # Works rarely, uses 0.000023083208674 eth per token, 1,973,242 gas
    # gasPrice = Libraries.core.ConvertGweiToWei(0.62000001)
    # numOfTokensToMint = 140

    # Trying
    # gasPrice = Libraries.core.ConvertGweiToWei(0.22220001)
    # numOfTokensToMint = 2

    # Trying
    # gasPrice = Libraries.core.ConvertGweiToWei(0.62000001)
    # numOfTokensToMint = 10

    # Trying
    # gasPrice = Libraries.core.ConvertGweiToWei(0.52000001)
    # numOfTokensToMint = 7

    # Works, uses 0.00001520989598 eth per token, 182,537 gas
    # gasPrice = Libraries.core.ConvertGweiToWei(0.33330001)
    # numOfTokensToMint = 4

    # Works, uses 0.000025741691429 eth per token, 290,632 gas
    # gasPrice = Libraries.core.ConvertGweiToWei(0.62000001)
    # numOfTokensToMint = 7  # This worked at 0.62 Gwei

    # Works, uses 0.000037201924528 eth per token, 1,971,702 gas
    # gasPrice = Libraries.core.ConvertGweiToWei(1.00000001)
    # numOfTokensToMint = 140  # Works at 1.0 Gwei

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.A)

    nonce = Libraries.core.API_GetTransactionCount(publicAddress_Ninja)

    forceIncrementNonce = True
    for x in range(numOfTransactionsToQueue):
        transactionId = None
        try:
            transactionId = NinjaObject.API_MintGasTokens(publicAddress_Ninja, privateKey_Ninja, numOfTokensToMint, gasPrice, nonce)
            # TODO, this doesn't seem to work
            # Libraries.f2pool.API_PushTx_Single(transactionId, Libraries.f2pool.GetTimeoutForMintGasTokens())

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            message = "Exception when minting gas tokens: " + traceback.format_exc()
            PrintAndLogError(message)

        if transactionId or forceIncrementNonce:
            nonce += 1

elif sys.argv and len(sys.argv) >= 2 and "testninja_share_profit_with_keeperdao" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]

    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    ninjaType = Exchanges.ninja.NinjaType.Generic
    NinjaObject = Exchanges.ninja.CreateNinjaObject(ninjaType)

    token = Exchanges.keeperDAO.EthTokenContract
    # token = Exchanges.zrxV2.Contract_WETH
    # token = Exchanges.keeperDAO.Contract_USDC
    # token = Exchanges.keeperDAO.Contract_DAI
    # token = Exchanges.keeperDAO.Contract_renBTC
    profitToShare_etherUnits = 0.00001
    NinjaObject.API_ShareProfitWithKeeperDAO(publicAddress_Ninja, privateKey_Ninja, profitToShare_etherUnits, token, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_share_profit_with_keeperdao_batched" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]

    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    ninjaType = Exchanges.ninja.NinjaType.Generic
    NinjaObject = Exchanges.ninja.CreateNinjaObject(ninjaType)

    nonce = Libraries.core.API_GetTransactionCount(publicAddress_Ninja)

    doShare = False

    multiplier_profitToShare = 1.0
    # multiplier_profitToShare = 0.000001

    # (MainThread) ----------------------------------------------------------------------
    # (MainThread) Stats for epoch 15. Epoch has concluded
    # (MainThread)    GetThreadSafeCopyOf_LatestBlockNumber_int = 11896446
    # (MainThread)    latestBlockNumber = 11896446
    # (MainThread)    startBlock 11889694
    # (MainThread)    endBlock 11896339
    # (MainThread)    sumDict_profit_afterGas_quoteToken =
    # {
    #   '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee': 33.037606554517525,
    #   '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2': 209.0235392891224,
    #   '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48': 28296.518730167678,
    # }
    
    shareDataList = []
    shareDataList.append((Exchanges.keeperDAO.EthTokenContract, 33.037606554517525 * multiplier_profitToShare))
    shareDataList.append((Exchanges.zrxV2.Contract_WETH, 208.0235392891224 * multiplier_profitToShare))
    # shareDataList.append((Exchanges.keeperDAO.Contract_DAI, 221.78551664477789 * multiplier_profitToShare))
    shareDataList.append((Exchanges.keeperDAO.Contract_USDC, 28296.518730167678 * multiplier_profitToShare))
    # shareDataList.append((Exchanges.keeperDAO.Contract_renBTC, 0.114663727271933 * multiplier_profitToShare))

    for shareData in shareDataList:
        token, profitToShare_etherUnits = shareData
        PrintAndLog("Calling API_ShareProfitWithKeeperDAO with " + str(profitToShare_etherUnits) + " " + str(token) + " and nonce = " + str(nonce))
        if doShare:
            val = input("Are you sure you want to share this profit????  Y/n: ")
            if val.lower() == 'n':
                PrintAndLog("Chickened out did ya")
                sys.exit()
            elif val.lower() == 'y':
                NinjaObject.API_ShareProfitWithKeeperDAO(publicAddress_Ninja, privateKey_Ninja, profitToShare_etherUnits, token, gasPrice, nonce)
            else:
                PrintAndLog("Invalid response")
                sys.exit()

        nonce += 1

elif sys.argv and len(sys.argv) >= 2 and "testninja_event_logs_keeper_contributions" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.uniswapV2.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswapV2.API_GetExchangeDataDict")

    Libraries.priceOracle.StartService()

    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    numOfRookRewardedPerEpoch = 1555
    blocksPerRewardEpoch = 6646

    # So the last snapshot is (11284908 -> 11291554) and the rewards would be reflected after 11291654 is mined.
    currentBlockNumber = latestBlockNumber
    epochBlockList = []
    epochBlockList.append(latestBlockNumber)
    numOfEpochsToScan = 1

    epochCount = 0
    while True:
        if currentBlockNumber % blocksPerRewardEpoch == 0:
            epochBlockList.append(currentBlockNumber)
            epochCount += 1
            if epochCount >= numOfEpochsToScan:
                break

        currentBlockNumber -= 1

    # Find the current epoch end datetime
    currentBlockNumber = latestBlockNumber
    while True:
        if currentBlockNumber % blocksPerRewardEpoch == 0:
            blockNumber_currentEpochEnd = currentBlockNumber
            break

        currentBlockNumber += 1

    # PrintAndLog("blockNumber_currentEpochEnd = " + str(blockNumber_currentEpochEnd))
    blocksInFuture = blockNumber_currentEpochEnd - latestBlockNumber
    PrintAndLog("blocksInFuture = " + str(blocksInFuture))
    secondsInFuture = Libraries.core.ConvertBlocksToSeconds(blocksInFuture)
    # PrintAndLog("secondsInFuture = " + str(secondsInFuture))
    minutesInFuture = secondsInFuture / 60
    # PrintAndLog("minutesInFuture = " + str(minutesInFuture))
    hoursInFuture = minutesInFuture / 60

    PrintAndLog("epochBlockList = " + str(epochBlockList))
    for i in range(len(epochBlockList) - 1):
        fromBlock_int = epochBlockList[i + 1]
        toBlock_int = epochBlockList[i] - 1  # - 1 at the end to make sure we don't overlap

        PrintAndLog("fromBlock_int = " + str(fromBlock_int))
        PrintAndLog("toBlock_int = " + str(toBlock_int))

        jData = Libraries.core.API_GetLogs_Safe(Contract_KeeperDAO_LP_Simple.address, BuildTopicsArray_KeeperDAO_BorrowProfitShare(), fromBlock_int, toBlock_int)
        PrintAndLog("jData = " + str(jData))

        keeperResultsDict = {}
        for eventLog in jData:
            PrintAndLog("transactionHash = " + str(eventLog['transactionHash']))
            keeperAddress = Libraries.core.GetAddressFromDataProperty(eventLog['topics'][1])
            tokenAddress = Libraries.core.GetAddressFromDataProperty(eventLog['topics'][2])
            dataSplit = Libraries.core.SplitStringIntoChunks(eventLog['data'].replace("0x", ""), Libraries.core.LengthOfDataProperty)
            # PrintAndLog("dataSplit = " + str(dataSplit))
            amountBorrowed_weiUnits = Libraries.core.ConvertHexToInt(dataSplit[0])
            profitShared_weiUnits = Libraries.core.ConvertHexToInt(dataSplit[1])

            if keeperAddress.lower() not in keeperResultsDict:
                keeperResultsDict[keeperAddress.lower()] = {}

            if tokenAddress.lower() not in keeperResultsDict[keeperAddress.lower()]:
                keeperResultsDict[keeperAddress.lower()][tokenAddress.lower()] = 0

            keeperResultsDict[keeperAddress.lower()][tokenAddress.lower()] += profitShared_weiUnits

        # Init keeperSumDict_usd
        keeperSumDict_usd = {}
        for keeper in keeperResultsDict:
            keeperSumDict_usd[keeper] = 0

        PrintAndLog("keeperResultsDict = " + str(keeperResultsDict))
        price_EthUsd = 1 / Libraries.priceOracle.GetOnChainPrice_FromCache(Exchanges.keeperDAO.Contract_USDC)
        printFriendlyString = ''
        sum_usd = 0
        tokenSumDict_usd = {}
        for keeper in keeperResultsDict:
            printFriendlyString += "Keeper shared profit: " + str(keeper) + "\n"
            for token in keeperResultsDict[keeper]:
                symbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token)
                amount_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(keeperResultsDict[keeper][token], token)
                # Convert amount to USD value
                price_token = Libraries.priceOracle.GetOnChainPrice_FromCache(token)
                amount_usd = amount_etherUnits * price_token * price_EthUsd
                printFriendlyString += "   " + str(amount_etherUnits) + " " + str(symbol) + ", USD value = $" + str(amount_usd) + "\n"

                # track each keeper's individual contribution
                keeperSumDict_usd[keeper] += amount_usd

                # track sums
                sum_usd += amount_usd
                if token not in tokenSumDict_usd:
                    tokenSumDict_usd[token] = 0
                tokenSumDict_usd[token] += amount_usd

        PrintAndLog("keeperResultsDict = ...")
        PrintAndLog(printFriendlyString)
        PrintAndLog("----------------------------------------------------------------------------")
        PrintAndLog("sum_usd = " + str(sum_usd))
        PrintAndLog("tokenSumDict_usd = " + str(tokenSumDict_usd))
        PrintAndLog("keeperSumDict_usd = " + str(keeperSumDict_usd))
        PrintAndLog("Current ROOK reward price = $" + str(round(sum_usd / numOfRookRewardedPerEpoch, 2)))
        PrintAndLog("Current epoch ends in approximately " + str(hoursInFuture) + " hours")
        PrintAndLog("----------------------------------------------------------------------------")
        PrintAndLog("----------------------------------------------------------------------------")

elif sys.argv and len(sys.argv) >= 2 and "testninja_whitelist_ops" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    ninjaOwner = NinjaOpAccountDict["ninja-op-100"]

    listOfUsersToWhitelist = []
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-100"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-102"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-103"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-104"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-105"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-106"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-107"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-108"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-109"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-110"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-111"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-112"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-113"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-114"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-116"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-117"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-118"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-119"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-120"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-121"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-122"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-123"].publicAddress)
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["dedicated-tx-prediction"].publicAddress)

    # Relay proxys were found to not be effective, so i'm no longer using them
    # listOfUsersToWhitelist.append(Libraries.relayProxy.RelayerProxyContract)

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.Generic)

    NinjaObject.API_WhitelistUsers(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), listOfUsersToWhitelist, gasPrice)
    # NinjaObject.API_BlacklistUsers(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), listOfUsersToWhitelist, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_blacklist_ops" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    ninjaOwner = NinjaOpAccountDict["ninja-op-100"]

    listOfUsersToWhitelist = []
    # listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-102"].publicAddress)

    ninjaType = Exchanges.ninja.NinjaType.A
    NinjaObject = Exchanges.ninja.CreateNinjaObject(ninjaType)

    NinjaObject.API_BlacklistUsers(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), listOfUsersToWhitelist, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_whitelist_keeperdao_borrow_proxy" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    ninjaOwner = NinjaOpAccountDict["ninja-op-100"]

    listOfLpsToWhitelist = []
    listOfLpsToWhitelist.append(ContractAddress_KeeperDAO_BorrowerProxy)

    ninjaType = Exchanges.ninja.NinjaType.A
    NinjaObject = Exchanges.ninja.CreateNinjaObject(ninjaType)

    boolValue = True
    nonce = None
    NinjaObject.API_WhitelistKeeperDAOBorrowProxy(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), listOfLpsToWhitelist, boolValue, gasPrice, nonce)

elif sys.argv and len(sys.argv) >= 2 and "testninja_sanity_check_withdraw_whitelist" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    toAddress = "0x3d71d79c224998e608d03c5ec9b405e7a38505f0"
    # fromAddress = "0x211b6a1137bf539b2750e02b9e525cf5757a35ae"
    # fromAddress = "0x82151ca501c81108d032c490e25f804787bef3b8"
    fromAddress = ContractAddress_KeeperDAO_BorrowerProxy
    data = "0x3bed33ce00000000000000000000000000000000000000000000000000005af3107a4000"
    result = Libraries.core.API_EstimateGas(toAddress, fromAddress, data)
    PrintAndLog("result = " + str(result))

elif sys.argv and len(sys.argv) >= 2 and "testninja_whitelist_relay_proxy_ops" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    ninjaOwner = NinjaOpAccountDict["ninja-op-100"]

    listOfUsersToWhitelist = []
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-100"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-102"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-103"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-104"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-105"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-106"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-107"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-108"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-109"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-110"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-111"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-112"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-113"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-114"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-116"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-117"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-118"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-119"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-120"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-121"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-122"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["ninja-op-123"].publicAddress)
    listOfUsersToWhitelist.append(NinjaOpAccountDict["dedicated-tx-prediction"].publicAddress)

    Libraries.relayProxy.API_WhitelistUsers(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(),
                                            Libraries.relayProxy.RelayerProxyContract, listOfUsersToWhitelist, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_getarbitrage_uniswapandkyber_using_kyberreserve" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    Libraries.priceOracle.StartService()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_NinjaBetweenUniswapAndKyber()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.A)

    amountsToConvert_weiUnits = [1000000000000000000, 2000000000000000000, 5000000000000000000]
    # amountsToConvert_weiUnits = [10000000000]
    # amountsToConvert_weiUnits = [1000000000000000000, 2000000000000000000, 50000000000000000000]
    # amountsToConvert_weiUnits = [100000000000]
    # amountsToConvert_weiUnits = [10900332662584072192, 9488549958586077184, 8174637805991760896, 6958596204801120256, 5840425155014150144, 4820124656630855680, 3897694709651236352, 3073135314075290624, 2346446469903018496, 1717628177134422016, 1186680435769498880, 753603245808250496, 418396607250676480, 181060520096776576, 41594984346551136]
    # amountsToConvert_weiUnits = [10900332662584072192, 41594984346551136]
    # amountsToConvert_weiUnits = [109003326625840721920000000, 41]

    # kyberReserve = '0x63825c174ab367968ec60f061753d3bbd36a0d8f'

    tokensThatFailed = []
    tokensThatSucceeded = []
    tokenName = 'bat'
    # for tokenName in TokenDict_Ninja:
    if TokenDict_Ninja[tokenName].tokenIsEnabled_betweenUniswapAndKyber:
        try:
            # Hard code for debug only
            kyberReserveList = Exchanges.kyber.GetAllKyberReservesTradingThisToken(TokenDict_Ninja[tokenName].erc20TokenContractAddress)
            # kyberReserve = kyberReserveList[0]
            for kyberReserve in kyberReserveList:
                PrintAndLog("tokenName = " + str(tokenName) + " (" + str(TokenDict_Ninja[tokenName].erc20TokenContractAddress) + "), kyberReserve = " + str(
                    kyberReserve) + " ------------------------------------------------- ")

                resultDict_BuyOnUniswapSellOnKyber = {}
                resultDict_BuyOnKyberSellOnUniswap = {}

                returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnUniswapSellOnKyber(
                    TokenDict_Ninja[tokenName], amountsToConvert_weiUnits, kyberReserve, resultDict_BuyOnUniswapSellOnKyber)
                returnEthList_BuyOnUniswapSellOnKyber = resultDict_BuyOnUniswapSellOnKyber['returnList_Eth']
                returnTokensList_BuyOnUniswapSellOnKyber = resultDict_BuyOnUniswapSellOnKyber['returnList_Tokens']
                returnRateList_BuyOnUniswapSellOnKyber = resultDict_BuyOnUniswapSellOnKyber['returnList_Rate']
                quantityEth_BuyOnUniswapSellOnKyber = resultDict_BuyOnUniswapSellOnKyber['balanceEther_UniswapContract_etherUnits']
                PrintAndLog("   returnEthList_BuyOnUniswapSellOnKyber = " + str(returnEthList_BuyOnUniswapSellOnKyber))
                PrintAndLog("   returnTokensList_BuyOnUniswapSellOnKyber = " + str(returnTokensList_BuyOnUniswapSellOnKyber))
                PrintAndLog("   returnRateList_BuyOnUniswapSellOnKyber = " + str(returnRateList_BuyOnUniswapSellOnKyber))
                PrintAndLog("   quantityEth_BuyOnUniswapSellOnKyber = " + str(quantityEth_BuyOnUniswapSellOnKyber))

                returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnKyberSellOnUniswap(
                    TokenDict_Ninja[tokenName], amountsToConvert_weiUnits, kyberReserve, resultDict_BuyOnKyberSellOnUniswap)
                returnEthList_BuyOnKyberSellOnUniswap = resultDict_BuyOnKyberSellOnUniswap['returnList_Eth']
                returnTokensList_BuyOnKyberSellOnUniswap = resultDict_BuyOnKyberSellOnUniswap['returnList_Tokens']
                returnRateList_BuyOnKyberSellOnUniswap = resultDict_BuyOnKyberSellOnUniswap['returnList_Rate']
                quantityEth_BuyOnKyberSellOnUniswap = resultDict_BuyOnKyberSellOnUniswap['balanceEther_UniswapContract_etherUnits']
                PrintAndLog("   returnEthList_BuyOnKyberSellOnUniswap = " + str(returnEthList_BuyOnKyberSellOnUniswap))
                PrintAndLog("   returnTokensList_BuyOnKyberSellOnUniswap = " + str(returnTokensList_BuyOnKyberSellOnUniswap))
                PrintAndLog("   returnRateList_BuyOnKyberSellOnUniswap = " + str(returnRateList_BuyOnKyberSellOnUniswap))
                PrintAndLog("   quantityEth_BuyOnKyberSellOnUniswap = " + str(quantityEth_BuyOnKyberSellOnUniswap))

                # Use for debug only
                # sys.exit()

            tokensThatSucceeded.append(tokenName)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            message = "exception: " + traceback.format_exc()
            PrintAndLogError(message)
            tokensThatFailed.append(tokenName)
            pass

    PrintAndLog("tokensThatFailed = " + str(tokensThatFailed))
    PrintAndLog("tokensThatSucceeded = " + str(tokensThatSucceeded))

# elif sys.argv and len(sys.argv) >= 2 and "testninja_getarbitrage_kyberand0xv2" == sys.argv[1].lower():
#     Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
#     ConsiderUpdatingLatestBlockNumber()
#
#     Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
#     Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
#
#     PopulateTokenDict_DaiAndSai_BugFix()
#     PopulateTokenDict_NinjaBetweenBancorAnd0x()
#     PopulateTokenDict_NinjaBetweenBancorAndKyber()
#     PopulateTokenDict_NinjaBetween0xAndKyber()
#
#     NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
#
#     tokenName = 'usdc'
#
#     # Open a local test 0xv2 order so that I can test my getManyArbitrages function with kyber vs this order
#     orderType = "sell"
#     # orderType = "buy"
#     tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(TokenDict_Ninja[tokenName].erc20TokenContractAddress)
#
#     if orderType == "sell":
#         file = open("testLocalOrder_0x_sell", "r")
#     elif orderType == "buy":
#         file = open("testLocalOrder_0x_buy", "r")
#
#     data = file.read()
#     file.close()
#     # deserialize the file's data into an object
#     order = jsonpickle.decode(data)
#     PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(
#         order.GetTokenQuantity()) + ", ether quantity + " + str(order.GetEtherQuantity()))
#
#     amountsToConvert_weiUnits = [1000000000000000000, 2000000000000000000, 5000000000000000000]  # tokensThatFailed =
#     # amountsToConvert_weiUnits = [10000000000]   # tokensThatFailed =
#     # amountsToConvert_weiUnits = [1000000, 20000000000000000, 3000000000000000000]  # tokensThatFailed =
#
#     tokensThatFailed = []
#     tokensThatSucceeded = []
#
#     if Libraries.core.IsSell(orderType):
#         result = NinjaObject.API_GetManyArbitrages_BuyOn0xv2SellOnKyber(TokenDict_Ninja[tokenName], amountsToConvert_weiUnits, order)
#         PrintAndLog("result = " + str(result))
#     elif Libraries.core.IsBuy(orderType):
#         result = NinjaObject.API_GetManyArbitrages_BuyOnKyberSellOn0xv2(TokenDict_Ninja[tokenName], amountsToConvert_weiUnits, order)
#         PrintAndLog("result = " + str(result))
#     else:
#         raise Exception("not buy or sell?")
#
#     tokensThatSucceeded.append(tokenName)
#
#     PrintAndLog("tokensThatFailed = " + str(tokensThatFailed))
#     PrintAndLog("tokensThatSucceeded = " + str(tokensThatSucceeded))

elif sys.argv and len(sys.argv) >= 2 and "testninja_getarbitrage_setand0xv3" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    # setKey = 'ethlovol'
    # token = TokenDict_Ninja['sai']
    setKey = 'ethbtc26emaco'
    token = TokenDict_Ninja['wbtc']

    # Open a local test 0x order
    # orderType = "sell"
    orderType = "buy"
    tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress)

    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "r")
    elif orderType == "buy":
        file = open("testLocalOrder_0x_buy", "r")

    data = file.read()
    file.close()
    # deserialize the file's data into an object
    order = jsonpickle.decode(data)
    PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(
        order.GetTokenQuantity()) + ", ether quantity + " + str(order.GetEtherQuantity()))

    amountsToConvert_weiUnits = [1000000000000000000, 2000000000000000000, 5000000000000000000]  # tokensThatFailed =
    # amountsToConvert_weiUnits = [10000000000]   # tokensThatFailed =
    # amountsToConvert_weiUnits = [1000000, 20000000000000000, 3000000000000000000]  # tokensThatFailed =

    tokensThatFailed = []
    tokensThatSucceeded = []

    if Libraries.core.IsSell(orderType):
        Exchanges.ninja.API_GetManyArbitrages_New_BuyOn0xv3SellOnSet(amountsToConvert_weiUnits, setKey, order)
    elif Libraries.core.IsBuy(orderType):
        Exchanges.ninja.API_GetManyArbitrages_New_BuyOnSetSellOn0xv3(amountsToConvert_weiUnits, setKey, order)
    else:
        raise Exception("not buy or sell?")

    tokensThatSucceeded.append(token.tokenName)

    PrintAndLog("tokensThatFailed = " + str(tokensThatFailed))
    PrintAndLog("tokensThatSucceeded = " + str(tokensThatSucceeded))

# elif sys.argv and len(sys.argv) >= 2 and "testninja_getreturn0xv2" == sys.argv[1].lower():
#     Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
#     ConsiderUpdatingLatestBlockNumber()
#     Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
#
#     marketName = "snt-eth"
#
#     orderType = "sell"
#     # orderType = "buy"
#
#     tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(MarketDict[marketName].erc20TokenContractAddress)
#
#     fillTakerTokenAmount_baseUnit = None
#     fillTakerTokenAmount_etherUnit = None
#     if orderType == "sell":
#         file = open("testLocalOrder_0x_sell", "r")
#         fillTakerTokenAmount_etherUnit = 0.00000000002
#         PrintAndLog("The taker is buying, so sending ether")
#         fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit, Libraries.core.Ether_Decimals)
#     elif orderType == "buy":
#         file = open("testLocalOrder_0x_buy", "r")
#         fillTakerTokenAmount_etherUnit = 0.00000001
#         PrintAndLog("The taker is selling, so sending tokens")
#         fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit,
#                                                                                       float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))
#
#     PrintAndLog("fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit))
#     data = file.read()
#     file.close()
#     # deserialize the file's data into an object
#     order = jsonpickle.decode(data)
#     PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(order.GetTokenQuantity()) + ", ether quantity + " + str(
#         order.GetEtherQuantity()) + ", fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit) + ", fillTakerTokenAmount_etherUnit = " + str(
#         fillTakerTokenAmount_etherUnit))
#
#     amountToConvert_weiUnits = 1000000000
#
#     fromAssetData = Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)
#     toAssetData = Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)
#     # fromAssetData = Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)
#     # toAssetData = Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)
#
#     Exchanges.ninja.API_GetReturn_0xv2(amountToConvert_weiUnits, order, fromAssetData, toAssetData)

# elif sys.argv and len(sys.argv) >= 2 and "testninja_getarbitrages_bancorand0xv2" == sys.argv[1].lower():
#     Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
#     ConsiderUpdatingLatestBlockNumber()
#
#     Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
#
#     PopulateTokenDict_DaiAndSai_BugFix()
#     PopulateTokenDict_NinjaBetweenBancorAndKyber()
#
#     marketName = "snt-eth"
#
#     # orderType = "sell"
#     orderType = "buy"
#
#     tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(MarketDict[marketName].erc20TokenContractAddress)
#
#     fillTakerTokenAmount_baseUnit = None
#     fillTakerTokenAmount_etherUnit = None
#     if orderType == "sell":
#         file = open("testLocalOrder_0x_sell", "r")
#         fillTakerTokenAmount_etherUnit = 0.00000000002
#         PrintAndLog("The taker is buying, so sending ether")
#         fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit, Libraries.core.Ether_Decimals)
#     elif orderType == "buy":
#         file = open("testLocalOrder_0x_buy", "r")
#         fillTakerTokenAmount_etherUnit = 0.00000001
#         PrintAndLog("The taker is selling, so sending tokens")
#         fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit,
#                                                                                       float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))
#
#     PrintAndLog("fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit))
#     data = file.read()
#     file.close()
#     # deserialize the file's data into an object
#     order = jsonpickle.decode(data)
#     PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(order.GetTokenQuantity()) + ", ether quantity + " + str(
#         order.GetEtherQuantity()) + ", fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit) + ", fillTakerTokenAmount_etherUnit = " + str(
#         fillTakerTokenAmount_etherUnit))
#
#     amountsToConvert_Bancor = [int(10000000)]
#     Exchanges.ninja.API_GetManyArbitrages_BuyOnBancorSellOn0xv2(TokenDict_Ninja[MarketDict[marketName].GetSecondaryTokenName()], amountsToConvert_Bancor, order)
#
#     # amountsToConvert_0xv2 = [int(10000000)]
#     # Exchanges.ninja.API_GetManyArbitrages_BuyOn0xv2SellOnBancor(TokenDict_Ninja[MarketDict[marketName].GetSecondaryTokenName()], amountsToConvert_0xv2, order)

# elif sys.argv and len(sys.argv) >= 2 and "testninja_getorderinfo" == sys.argv[1].lower():
#     Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
#     ConsiderUpdatingLatestBlockNumber()
#     Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
#
#     PopulateTokenDict_DaiAndSai_BugFix()
#     PopulateTokenDict_NinjaBetweenBancorAndKyber()
#
#     marketName = "snt-eth"
#
#     # orderType = "sell"
#     orderType = "buy"
#
#     tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(MarketDict[marketName].erc20TokenContractAddress)
#
#     fillTakerTokenAmount_baseUnit = None
#     fillTakerTokenAmount_etherUnit = None
#     if orderType == "sell":
#         file = open("testLocalOrder_0x_sell", "r")
#         fillTakerTokenAmount_etherUnit = 0.00000000002
#         PrintAndLog("The taker is buying, so sending ether")
#         fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit, Libraries.core.Ether_Decimals)
#     elif orderType == "buy":
#         file = open("testLocalOrder_0x_buy", "r")
#         fillTakerTokenAmount_etherUnit = 0.00000001
#         PrintAndLog("The taker is selling, so sending tokens")
#         fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit,
#                                                                                       float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))
#
#     PrintAndLog("fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit))
#     data = file.read()
#     file.close()
#     # deserialize the file's data into an object
#     order = jsonpickle.decode(data)
#     PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(order.GetTokenQuantity()) + ", ether quantity + " + str(
#         order.GetEtherQuantity()) + ", fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit) + ", fillTakerTokenAmount_etherUnit = " + str(
#         fillTakerTokenAmount_etherUnit))
#
#     # Exchanges.zrxV2.GetOrderInfo(order)
#     Exchanges.zrxV2.GetManyOrderInfos([order, order, order, order, order, order])

# elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_bancorand0xv2" == sys.argv[1].lower():
#     Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
#     ConsiderUpdatingLatestBlockNumber()
#     Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
#
#     PrintAndLog("Fill local test order that I made myself")
#
#     marketName = "usdc-eth"
#
#     # orderType = "sell"
#     orderType = "buy"
#     tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(MarketDict[marketName].erc20TokenContractAddress)
#
#     fillTakerTokenAmount_baseUnit = None
#     fillTakerTokenAmount_etherUnit = None
#     if orderType == "sell":
#         file = open("testLocalOrder_0x_sell", "r")
#         fillTakerTokenAmount_etherUnit = 0.00000000002
#         PrintAndLog("The taker is buying, so sending ether")
#         fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit, Libraries.core.Ether_Decimals)
#     elif orderType == "buy":
#         file = open("testLocalOrder_0x_buy", "r")
#         fillTakerTokenAmount_etherUnit = 0.00000001
#         PrintAndLog("The taker is selling, so sending tokens")
#         fillTakerTokenAmount_baseUnit = Libraries.core.ConvertEtherAmountToBaseAmount(fillTakerTokenAmount_etherUnit,
#                                                                                       float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))
#
#     PrintAndLog("fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit))
#     data = file.read()
#     file.close()
#     # deserialize the file's data into an object
#     order = jsonpickle.decode(data)
#     PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(order.GetTokenQuantity()) + ", ether quantity + " + str(
#         order.GetEtherQuantity()) + ", fillTakerTokenAmount_baseUnit = " + str(fillTakerTokenAmount_baseUnit) + ", fillTakerTokenAmount_etherUnit = " + str(
#         fillTakerTokenAmount_etherUnit))
#
#     ninjaOwner = NinjaOpAccountDict["ninja-op-1"]
#
#     gas = Libraries.gasStation.GetGasLimit_TradeCustomContract()
#     Libraries.gasStation.ConsiderGettingBancorsMaxGasPriceValue()
#     # Use this gas price when on production
#     # gasPrice = Libraries.gasStation.BancorMaxGasPrice_wei
#     # Use this gas price when testing
#     gasPrice = min(Libraries.gasStation.GetGasPrice_Cheap(), Libraries.gasStation.BancorMaxGasPrice_wei)
#
#     # Ninja.API_TradeTokensOn0xv2(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), order, fillTakerTokenAmount_baseUnit, gas, gasPrice)
#
#     # # Trade, buy on 0xv2 and sell on Bancor
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     # # Path for selling SNT on Bancor
#     # pathList_Bancor = [
#     #     "0x744d70fdbe2ba4cf95131626614a1763df805b9e",
#     #     "0xa3b3c5a8b22c044d5f2d372f628245e2106d310d",
#     #     "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c",
#     #     "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c",
#     #     "0xc0829421c1d260bd3cb3e0f06cfe2d52db2ce315",
#     # ]
#     #
#     # Ninja.API_Trade_BuyOn0xv2SellOnBancor(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), bancorConverterContract,
#     #                                       pathList_Bancor, order, fillTakerTokenAmount_baseUnit, gas, gasPrice)
#
#     # # Trade, buy on Bancor and sell on 0xv2
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     # # Path for buying SNT on Bancor
#     # pathList_Bancor = [
#     #     "0xc0829421c1d260bd3cb3e0f06cfe2d52db2ce315",
#     #     "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c",
#     #     "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c",
#     #     "0xa3b3c5a8b22c044d5f2d372f628245e2106d310d",
#     #     "0x744d70fdbe2ba4cf95131626614a1763df805b9e",
#     # ]
#     #
#     # Ninja.API_Trade_BuyOnBancorSellOn0xv2(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), bancorConverterContract,
#     #                                       pathList_Bancor, order, fillTakerTokenAmount_baseUnit, gas, gasPrice)
#
#     # # Trade Safe, buy on 0xv2 and sell on Bancor
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     # bancorTokenConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     # # Path for selling SNT on Bancor
#     # pathList_Bancor = [
#     #     "0x744d70fdbe2ba4cf95131626614a1763df805b9e",
#     #     "0xa3b3c5a8b22c044d5f2d372f628245e2106d310d",
#     #     "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c",
#     #     "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c",
#     #     "0xc0829421c1d260bd3cb3e0f06cfe2d52db2ce315",
#     # ]
#     # amountToConvert_0xv2_weiUnits = 10000000
#     # Ninja.API_TradeSafe_BuyOn0xv2SellOnBancor(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), tokenAddress,
#     #                                           bancorConverterContract, bancorTokenConverterContract,
#     #                                           pathList_Bancor, order, amountToConvert_0xv2_weiUnits,
#     #                                           gas, gasPrice)
#
#     # # Trade Safe, buy on Bancor and sell on 0xv2
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens)
#     # bancorTokenConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     # # Path for selling SNT on Bancor
#     # pathList_Bancor = [
#     #     "0xc0829421c1d260bd3cb3e0f06cfe2d52db2ce315",
#     #     "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c",
#     #     "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c",
#     #     "0xa3b3c5a8b22c044d5f2d372f628245e2106d310d",
#     #     "0x744d70fdbe2ba4cf95131626614a1763df805b9e",
#     # ]
#     # amountToConvert_Bancor_weiUnits = 10000000
#     # Ninja.API_TradeSafe_BuyOnBancorSellOn0xv2(ninjaOwner.publicAddress, ninjaOwner.DecryptPK(), tokenAddress,
#     #                                           bancorConverterContract, bancorTokenConverterContract,
#     #                                           pathList_Bancor, order, amountToConvert_Bancor_weiUnits,
#     #                                           gas, gasPrice)

# elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_bancorandkyber" == sys.argv[1].lower():
#     Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
#     ConsiderUpdatingLatestBlockNumber()
#
#     Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
#
#     PopulateTokenDict_DaiAndSai_BugFix()
#     PopulateTokenDict_NinjaBetweenBancorAnd0x()
#     PopulateTokenDict_NinjaBetweenBancorAndKyber()
#
#     ninjaOp = NinjaOpAccountDict["ninja-op-1"]
#     publicAddress_Ninja = ninjaOp.publicAddress
#     privateKey_Ninja = ninjaOp.DecryptPK()
#
#     token = TokenDict_Ninja['rlc']
#     marketName = token.tokenName + '-eth'
#
#     NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.A)
#
#     kyberProxyContract = Exchanges.kyber.Contract_KyberNetworkProxy
#     etherToConvert_etherUnits = 0.0000001
#     etherToConvert_weiUnits = Libraries.core.ConvertEtherToWei(etherToConvert_etherUnits, Libraries.core.Ether_Decimals)
#
#     gas = Libraries.gasStation.GetGasLimit_Ninja_BancorAndKyber()
#     Libraries.gasStation.ConsiderGettingBancorsMaxGasPriceValue()
#     Libraries.gasStation.ConsiderGettingKybersMaxGasPriceValue()
#     # Use this gas price when on production
#     # gasPrice = Libraries.gasStation.GetMaxGasPrice_WhenArbitragingBetweenBancorAndKyber()
#     # Use this gas price when testing
#     # gasPrice = min(Libraries.gasStation.GetGasPrice_Cheap(), Libraries.gasStation.GetMaxGasPrice_WhenArbitragingBetweenBancorAndKyber())
#     gasPrice = min(Libraries.gasStation.GetGasPrice_SafeLow(), Libraries.gasStation.GetMaxGasPrice_WhenArbitragingBetweenBancorAndKyber())
#     # gasPrice = Libraries.core.ConvertGweiToWei(2.300002)
#
#     # # Percentage for worstPriceIllAccept
#     # percentageForWorstPriceIllAccept = 0.2
#     # # Get the current market price from CoinMarketCap so I can determine the worst price i'll accept
#     # # When arbitraging, i'll want to base this on the CX instead of CoinMarketCap...
#     UpdateCoinMarketCapMarkets(1000, "id")
#     currentMarketPrice_withRespectToETH = CoinMarketCapMarketsDict[MarketDict[marketName].coinMarketCapId].GetPriceEth(MarketDict[marketName].coinMarketCapId,
#                                                                                                                        CoinMarketCapMarketsDict)
#     PrintAndLog(MarketDict[marketName].GetSecondaryTokenName() + "'s currentMarketPrice_withRespectToETH = " + str(currentMarketPrice_withRespectToETH))
#
#     # # Buy on Bancor
#     # minReturn_weiUnits = 1
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens)
#     # pathList = token.pathList_BuyingTokens_Bancor
#     #
#     # NinjaObject.API_TradeTokensOnBancor(publicAddress_Ninja, privateKey_Ninja, etherToConvert_weiUnits,
#     #                                     bancorConverterContract, pathList, etherToConvert_weiUnits, minReturn_weiUnits,
#     #                                     0, 0, 0, 0, gas, gasPrice)
#
#     # # Sell on Bancor
#     # minReturn_weiUnits = 1
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     # pathList = token.pathList_SellingTokens_Bancor
#     # tokensToConvert_etherUnits = 0.00000010
#     # tokensToConvert_weiUnits = Libraries.core.ConvertEtherToWei(tokensToConvert_etherUnits, MarketDict[marketName].decimals)
#     # NinjaObject.API_TradeTokensOnBancor(publicAddress_Ninja, privateKey_Ninja, 0,
#     #                                     bancorConverterContract, pathList, tokensToConvert_weiUnits, minReturn_weiUnits,
#     #                                     0, 0, 0, 0, gas, gasPrice)
#
#     # # Buy on Kyber
#     # # Hard coding a very large number...  Who cares if I get a ton more than i'm asking for?
#     # maxDestAmount = 69203865833239757421118596509098632427930889272824243351707071692229331386368
#     # minConversionRate = None
#     # orderType_Kyber = "buy"
#     # if Libraries.core.IsBuy(orderType_Kyber):
#     #     # I'm buying tokens, so the worstPriceIllAccept will be higher than my currentMarketPrice_withRespectToETH
#     #     worstPriceIllAccept = currentMarketPrice_withRespectToETH + (currentMarketPrice_withRespectToETH * percentageForWorstPriceIllAccept)
#     #     PrintAndLog("I'm " + orderType_Kyber + "ing " + MarketDict[marketName].GetSecondaryTokenName().upper() + ", currentMarketPrice_withRespectToETH is " + str(
#     #         currentMarketPrice_withRespectToETH) + ", worstPriceIllAccept = " + str(worstPriceIllAccept))
#     #     # When buying, you have to do 1 divided by the price, this is a Kyber thing...
#     #     minConversionRate = 1 / float(worstPriceIllAccept)
#     #
#     #     src = Exchanges.kyber.EtherToken
#     #     dest = MarketDict[marketName].erc20TokenContractAddress
#     #
#     # elif Libraries.core.IsSell(orderType_Kyber):
#     #     # I'm selling tokens, so the worstPriceIllAccept will be lower than my currentMarketPrice_withRespectToETH
#     #     worstPriceIllAccept = currentMarketPrice_withRespectToETH - (currentMarketPrice_withRespectToETH * percentageForWorstPriceIllAccept)
#     #     PrintAndLog("I'm " + orderType_Kyber + "ing " + MarketDict[marketName].GetSecondaryTokenName().upper() + ", currentMarketPrice_withRespectToETH is " + str(
#     #         currentMarketPrice_withRespectToETH) + ", worstPriceIllAccept = " + str(worstPriceIllAccept))
#     #     minConversionRate = float(worstPriceIllAccept)
#     #
#     #     src = MarketDict[marketName].erc20TokenContractAddress
#     #     dest = Exchanges.kyber.EtherToken
#     #
#     # walletId = Libraries.core.GetNullAddress()
#     #
#     # NinjaObject.API_TradeTokensOnKyber(publicAddress_Ninja, privateKey_Ninja, etherToConvert_weiUnits,
#     #                                    kyberProxyContract, src, etherToConvert_weiUnits, dest, maxDestAmount, minConversionRate, walletId,
#     #                                    gas, gasPrice)
#
#     # # Sell on Kyber
#     # orderType_Kyber = "sell"
#     # sourceTokens_weiUnits = 14108410390872 # SNT
#     # maxDestAmount = 69203865833239757421118596509098632427930889272824243351707071692229331386368
#     #
#     # minConversionRate = None
#     # if Libraries.core.IsBuy(orderType_Kyber):
#     #     # I'm buying tokens, so the worstPriceIllAccept will be higher than my currentMarketPrice_withRespectToETH
#     #     worstPriceIllAccept = currentMarketPrice_withRespectToETH + (currentMarketPrice_withRespectToETH * percentageForWorstPriceIllAccept)
#     #     PrintAndLog("I'm " + orderType_Kyber + "ing " + MarketDict[marketName].GetSecondaryTokenName().upper() + ", currentMarketPrice_withRespectToETH is " + str(
#     #         currentMarketPrice_withRespectToETH) + ", worstPriceIllAccept = " + str(worstPriceIllAccept))
#     #     # When buying, you have to do 1 divided by the price, this is a Kyber thing...
#     #     minConversionRate = 1 / float(worstPriceIllAccept)
#     #
#     #     src = Exchanges.kyber.EtherToken
#     #     dest = MarketDict[marketName].erc20TokenContractAddress
#     #
#     # elif Libraries.core.IsSell(orderType_Kyber):
#     #     # I'm selling tokens, so the worstPriceIllAccept will be lower than my currentMarketPrice_withRespectToETH
#     #     worstPriceIllAccept = currentMarketPrice_withRespectToETH - (currentMarketPrice_withRespectToETH * percentageForWorstPriceIllAccept)
#     #     PrintAndLog("I'm " + orderType_Kyber + "ing " + MarketDict[marketName].GetSecondaryTokenName().upper() + ", currentMarketPrice_withRespectToETH is " + str(
#     #         currentMarketPrice_withRespectToETH) + ", worstPriceIllAccept = " + str(worstPriceIllAccept))
#     #     minConversionRate = float(worstPriceIllAccept)
#     #
#     #     src = MarketDict[marketName].erc20TokenContractAddress
#     #     dest = Exchanges.kyber.EtherToken
#     #
#     # walletId = Libraries.core.GetNullAddress()
#     #
#     # NinjaObject.API_TradeTokensOnKyber(publicAddress_Ninja, privateKey_Ninja, 0,
#     #                                    kyberProxyContract, src, sourceTokens_weiUnits, dest, maxDestAmount, minConversionRate, walletId,
#     #                                    gas, gasPrice)
#
#     # Trade, buy on Bancor and sell on Kyber
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens)
#     # amountToConvert_Bancor_weiUnits = etherToConvert_weiUnits
#     #
#     # pathList_Bancor = token.pathList_BuyingTokens_Bancor
#     # pathList_Kyber = token.pathList_SellingTokens_Kyber
#     # PrintAndLog("bancorConverterContract = " + str(bancorConverterContract))
#     # PrintAndLog("pathList_Bancor = " + str(pathList_Bancor))
#     # PrintAndLog("pathList_Kyber = " + str(pathList_Kyber))
#
#     # NinjaObject.API_Trade_BuyOnBancorSellOnKyber(publicAddress_Ninja, privateKey_Ninja,
#     #                                        bancorConverterContract, kyberProxyContract,
#     #                                        pathList_Bancor, pathList_Kyber, amountToConvert_Bancor_weiUnits,
#     #                                        gas, gasPrice)
#
#     # # # # Trade, buy on Kyber and sell on Bancor
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     #
#     # amountToConvert_Kyber_weiUnits = etherToConvert_weiUnits
#     #
#     # pathList_Bancor = token.pathList_SellingTokens_Bancor
#     # pathList_Kyber = token.pathList_BuyingTokens_Kyber
#     #
#     # # PrintAndLog("bancorConverterContract = " + str(bancorConverterContract))
#     # # PrintAndLog("pathList_Bancor = " + str(pathList_Bancor))
#     # # PrintAndLog("pathList_Kyber = " + str(pathList_Kyber))
#     #
#     # NinjaObject.API_Trade_BuyOnKyberSellOnBancor(publicAddress_Ninja, privateKey_Ninja,
#     #                                              bancorConverterContract, kyberProxyContract,
#     #                                              pathList_Bancor, pathList_Kyber, amountToConvert_Kyber_weiUnits,
#     #                                              gas, gasPrice)
#
#     # # Trade only when there's profit, buy on Bancor and sell on Kyber
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens)
#     # bancorTokenConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     #
#     # pathList_Bancor = token.pathList_BuyingTokens_Bancor
#     # pathList_Kyber = token.pathList_SellingTokens_Kyber
#     #
#     # amountToConvert_Bancor_weiUnits = etherToConvert_weiUnits
#     #
#     # NinjaObject.API_TradeSafe_BuyOnBancorSellOnKyber(publicAddress_Ninja, privateKey_Ninja,
#     #                                            bancorConverterContract, bancorTokenConverterContract, kyberProxyContract,
#     #                                            pathList_Bancor, pathList_Kyber, amountToConvert_Bancor_weiUnits,
#     #                                            gas, gasPrice)
#
#     # # Trade only when there's profit, buy on Kyber and sell on Bancor
#     # bancorConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     # bancorTokenConverterContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[MarketDict[marketName].GetSecondaryTokenName().upper()])
#     # amountToConvert_Kyber_weiUnits = etherToConvert_weiUnits
#     #
#     # pathList_Bancor = token.pathList_SellingTokens_Bancor
#     # pathList_Kyber = token.pathList_BuyingTokens_Kyber
#     #
#     # NinjaObject.API_TradeSafe_BuyOnKyberSellOnBancor(publicAddress_Ninja, privateKey_Ninja,
#     #                                                  bancorConverterContract, bancorTokenConverterContract, kyberProxyContract,
#     #                                                  pathList_Bancor, pathList_Kyber, amountToConvert_Kyber_weiUnits,
#     #                                                  gas, gasPrice)
#
#     # Trade only when simple requirements are met, buy on Bancor and sell on Kyber
#     expectedTokensToTrade_etherUnits = Libraries.core.ConvertEtherToTokens(etherToConvert_etherUnits, currentMarketPrice_withRespectToETH)
#     # Just put something in here for now that will pass the requirement
#     maxBancorBntQuantityAcceptable_etherUnits = 1000000000000000000  # Should pass
#     # maxBancorBntQuantityAcceptable_etherUnits = 1  # Should fail
#     # Just put something in here for now that will pass the requirement
#     # minKyberPriceAcceptable = 0.00350204  # DAI  # Should pass
#     # minKyberPriceAcceptable = 0.00750204  # DAI  # Should fail
#     minKyberPriceAcceptable = 0.00218880  # RLC  # Should pass
#     # minKyberPriceAcceptable = 0.00618880  # RLC  # Should fail
#     bancorConverter_payingWithEtherOnly = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens)
#     bancorConverter_payingWithSpecificTokenOnly = Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract)
#     bancorRelayerToken = token.bancorRelayerTokenAddress
#     kyberReserve = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")
#     PrintAndLog("token.pathList_BuyingTokens_Bancor = " + str(token.pathList_BuyingTokens_Bancor))
#     PrintAndLog("token.pathList_SellingTokens_Bancor = " + str(token.pathList_SellingTokens_Bancor))
#     # NinjaObject.API_TradeSimpleRequirements_BuyOnBancorSellOnKyber(publicAddress_Ninja, privateKey_Ninja,
#     #                                                                etherToConvert_etherUnits, expectedTokensToTrade_etherUnits,
#     #                                                                token.erc20TokenContractAddress, token.decimals,
#     #                                                                maxBancorBntQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
#     #                                                                bancorConverter_payingWithEtherOnly, bancorConverter_payingWithSpecificTokenOnly, bancorRelayerToken, kyberReserve,
#     #                                                                gas, gasPrice)
#
#     # # Trade only when simple requirements are met, buy on Kyber and sell on Bancor
#     # # Just put something in here for now that will pass the requirement
#     # minBancorBntQuantityAcceptable_etherUnits = 1  # Should pass
#     # # minBancorBntQuantityAcceptable_etherUnits = 1000000000000000000  # Should fail
#     # # Just put something in here for now that will pass the requirement
#     # # maxKyberPriceAcceptable = 0.00750204  # DAI  # Should pass
#     # # maxKyberPriceAcceptable = 0.00350204  # DAI  # Should fail
#     # maxKyberPriceAcceptable = 0.00618880  # RLC  # Should pass
#     # # maxKyberPriceAcceptable = 0.00218880  # RLC  # Should fail
#     # bancorConverter_payingWithSpecificTokenOnly = Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract)
#     # bancorRelayerToken = token.bancorRelayerTokenAddress
#     # kyberReserve = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")
#     # NinjaObject.API_TradeSimpleRequirements_BuyOnKyberSellOnBancor(publicAddress_Ninja, privateKey_Ninja,
#     #                                                                etherToConvert_etherUnits, token.erc20TokenContractAddress,
#     #                                                                minBancorBntQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
#     #                                                                bancorConverter_payingWithSpecificTokenOnly, bancorRelayerToken, kyberReserve,
#     #                                                                gas, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_uniswapandkyber" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_NinjaBetweenUniswapAndKyber()

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()

    token = TokenDict_Ninja['snt']
    coinMarketCapId = 'status'

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.A)

    keeperDAOsMaxBorrowableBalance = Exchanges.keeperDAO.API_GetBorrowableBalance(Contract_KeeperDAO_LP_Simple, Exchanges.keeperDAO.EthTokenContract)

    etherToConvert_etherUnits = 0.00000001
    # Ensure the lp has enough balance to borrow for our trade
    PrintAndLog("keeperDAOsMaxBorrowableBalance = " + str(keeperDAOsMaxBorrowableBalance))
    if keeperDAOsMaxBorrowableBalance < etherToConvert_etherUnits:
        raise Exception("LP balance is " + str(keeperDAOsMaxBorrowableBalance) + " and we tried to trade more then what was available, etherToConvert_etherUnits = " + str(
            etherToConvert_etherUnits))

    etherToConvert_weiUnits = Libraries.core.ConvertEtherToWei(etherToConvert_etherUnits, Libraries.core.Ether_Decimals)
    PrintAndLog("etherToConvert_etherUnits = " + str(etherToConvert_etherUnits) + ", etherToConvert_weiUnits = " + str(etherToConvert_weiUnits))

    gas = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_QuoteTokenIsEther(
        token.erc20TokenContractAddress, [ExchangeName_Uniswap, ExchangeName_Kyber], False)
    Libraries.gasStation.ConsiderGettingKybersMaxGasPriceValue()

    gasPrice = Libraries.core.ConvertGweiToWei(28.00001)
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # Enforcer kyber max gas price
    gasPrice = min(gasPrice, Libraries.gasStation.KyberMaxGasPrice_wei)

    UpdateCoinMarketCapMarkets(1000, "id")
    currentMarketPrice_withRespectToETH = CoinMarketCapMarketsDict[coinMarketCapId].GetPriceEth(coinMarketCapId, CoinMarketCapMarketsDict)
    PrintAndLog(str(token.tokenName) + "'s currentMarketPrice_withRespectToETH = " + str(currentMarketPrice_withRespectToETH))

    # Trade only when simple requirements are met, buy on Uniswap and sell on Kyber
    expectedTokensToTrade_etherUnits = Libraries.core.ConvertEtherToTokens(etherToConvert_etherUnits, currentMarketPrice_withRespectToETH)
    # Just put something in here for now that will pass the requirement
    maxUniswapEthQuantityAcceptable_etherUnits = 1000000000000000000  # Should pass
    # maxUniswapEthQuantityAcceptable_etherUnits = 1  # Should fail
    # Just put something in here for now that will pass the requirement
    minKyberPriceAcceptable = 0.00004617  # SNT  # Should pass
    # minKyberPriceAcceptable = 0.00032617  # SNT  # Should fail
    # minKyberPriceAcceptable = 0.00350204  # DAI  # Should pass
    # minKyberPriceAcceptable = 0.00750204  # DAI  # Should fail
    # minKyberPriceAcceptable = 0.00218880  # RLC  # Should pass
    # minKyberPriceAcceptable = 0.00618880  # RLC  # Should fail
    # minKyberPriceAcceptable = 0.00104924  # BAT  # Should pass
    # minKyberPriceAcceptable = 0.00184924  # BAT  # Should fail
    kyberReserve = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")
    NinjaObject.API_TradeSimpleRequirements_BuyOnUniswapSellOnKyber(publicAddress_Ninja, privateKey_Ninja,
                                                                    etherToConvert_etherUnits, expectedTokensToTrade_etherUnits,
                                                                    token.erc20TokenContractAddress, token.decimals,
                                                                    maxUniswapEthQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
                                                                    kyberReserve, gas, gasPrice)

    # # Trade only when simple requirements are met, buy on Kyber and sell on Uniswap
    # # Just put something in here for now that will pass the requirement
    # minUniswapEthQuantityAcceptable_etherUnits = 1  # Should pass
    # # minUniswapEthQuantityAcceptable_etherUnits = 1000000000000000000  # Should fail
    # # Just put something in here for now that will pass the requirement
    # maxKyberPriceAcceptable = 0.00032617  # SNT  # Should pass
    # # maxKyberPriceAcceptable = 0.00002617  # SNT  # Should fail
    # # maxKyberPriceAcceptable = 0.00750204  # DAI  # Should pass
    # # maxKyberPriceAcceptable = 0.00350204  # DAI  # Should fail
    # # maxKyberPriceAcceptable = 0.00618880  # RLC  # Should pass
    # # maxKyberPriceAcceptable = 0.00218880  # RLC  # Should fail
    # # maxKyberPriceAcceptable = 0.00184924  # BAT  # Should pass
    # # maxKyberPriceAcceptable = 0.00104924  # BAT  # Should fail
    #
    # kyberReserve = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")
    # NinjaObject.API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap(publicAddress_Ninja, privateKey_Ninja,
    #                                                                 etherToConvert_etherUnits, token.erc20TokenContractAddress,
    #                                                                 minUniswapEthQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
    #                                                                 kyberReserve, gas, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_uniswapand0xv3" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_NinjaBetweenUniswapAnd0x()

    ninjaOp = NinjaOpAccountDict["ninja-op-1"]
    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()

    token = TokenDict_Ninja['bat']
    marketName = token.tokenName + '-eth'

    # orderType = "sell"
    orderType = "buy"

    if orderType == "sell":
        # file = open("testLocalOrder_0x_sell", "r")
        PrintAndLog("The taker is buying, so sending ether")
    elif orderType == "buy":
        # file = open("testLocalOrder_0x_buy", "r")
        PrintAndLog("The taker is selling, so sending tokens")

    data = Libraries.defaults.file.read()
    Libraries.defaults.file.close()
    # deserialize the file's data into an object
    order = jsonpickle.decode(data)
    PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(
        order.GetTokenQuantity()) + ", ether quantity + " + str(order.GetEtherQuantity()))

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.A)

    # etherToConvert_etherUnits = 0.00000001
    etherToConvert_etherUnits = 0.002  # Shipping more than I need, it should change 0.002 to about 0.001 because that's all it needs to fill the order full based on my math
    etherToConvert_weiUnits = Libraries.core.ConvertEtherToWei(etherToConvert_etherUnits, Libraries.core.Ether_Decimals)

    gas = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_QuoteTokenIsEther(
        token.erc20TokenContractAddress, [ExchangeName_Uniswap, ExchangeName_0x], False)
    Libraries.gasStation.ConsiderGettingKybersMaxGasPriceValue()
    # gasPrice = min(Libraries.gasStation.GetGasPrice_Cheap(), Libraries.gasStation.KyberMaxGasPrice_wei)
    gasPrice = min(Libraries.gasStation.GetGasPrice_SafeLow(), Libraries.gasStation.KyberMaxGasPrice_wei)
    # gasPrice = Libraries.core.ConvertGweiToWei(2.300002)

    # Just put something in here for now that will pass the requirement
    maxUniswapEthQuantityAcceptable_etherUnits = 1000000000000000000  # Should pass
    # maxUniswapEthQuantityAcceptable_etherUnits = 1  # Should fail
    NinjaObject.API_TradeSimpleRequirements_BuyOnUniswapSellOn0xv3(publicAddress_Ninja, privateKey_Ninja,
                                                                   etherToConvert_etherUnits, token.erc20TokenContractAddress,
                                                                   maxUniswapEthQuantityAcceptable_etherUnits, order,
                                                                   gas, gasPrice)

    # # Trade only when simple requirements are met, buy on Kyber and sell on Uniswap
    # # Just put something in here for now that will pass the requirement
    # minUniswapEthQuantityAcceptable_etherUnits = 1  # Should pass
    # # minUniswapEthQuantityAcceptable_etherUnits = 1000000000000000000  # Should fail
    # # Just put something in here for now that will pass the requirement
    # # maxKyberPriceAcceptable = 0.00750204  # DAI  # Should pass
    # # maxKyberPriceAcceptable = 0.00350204  # DAI  # Should fail
    # # maxKyberPriceAcceptable = 0.00618880  # RLC  # Should pass
    # # maxKyberPriceAcceptable = 0.00218880  # RLC  # Should fail
    # maxKyberPriceAcceptable = 0.00184924  # BAT  # Should pass
    # # maxKyberPriceAcceptable = 0.00104924  # BAT  # Should fail
    # kyberReserve = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")
    # NinjaObject.API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap(publicAddress_Ninja, privateKey_Ninja,
    #                                                                 etherToConvert_etherUnits, token.erc20TokenContractAddress,
    #                                                                 minUniswapEthQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
    #                                                                 kyberReserve, gas, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_getarbitrage_setandkyber" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)

    # setKey = 'ethlovol'
    # token = TokenDict_Ninja['sai']
    setKey = 'ethbtc26emaco'
    token = TokenDict_Ninja['wbtc']

    kyberReserve = '0x63825c174ab367968ec60f061753d3bbd36a0d8f'

    etherToSpendList_weiUnits = [
        Libraries.core.ConvertEtherToWei(1, Libraries.core.Ether_Decimals),
        Libraries.core.ConvertEtherToWei(2, Libraries.core.Ether_Decimals),
        Libraries.core.ConvertEtherToWei(5, Libraries.core.Ether_Decimals),
        # Libraries.core.ConvertEtherToWei(0.001952 * 4, Libraries.core.Ether_Decimals),
        # Libraries.core.ConvertEtherToWei(0.001952 * 8, Libraries.core.Ether_Decimals),
        # Libraries.core.ConvertEtherToWei(0.001952 * 12, Libraries.core.Ether_Decimals),
        # Libraries.core.ConvertEtherToWei(0.001952 * 16, Libraries.core.Ether_Decimals),
    ]
    # returnList_Eth, returnList_Tokens, returnList_Rate = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnKyberSellOnSet(
    #     token, etherToSpendList_weiUnits, setKey, kyberReserve)

    returnList_Eth, returnList_Tokens, returnList_Rate = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnSetSellOnKyber(
        token, etherToSpendList_weiUnits, setKey, kyberReserve)

elif sys.argv and len(sys.argv) >= 2 and "testninja_getarbitrage_setandkyber_tokentotoken" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.priceOracle.StartService()

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)

    setKey = 'wbtcusdcv5'
    setTokenData = Exchanges.set.SetTokenDataDict[setKey]

    quoteToken = setTokenData.quoteTokenAddress
    baseToken, dontCare = setTokenData.GetBaseTokenAddress()
    decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteToken)))

    quoteTokenQuantityToSpendList_weiUnits = [
        Libraries.core.ConvertEtherToWei(0.0001, decimals_quoteToken),
        Libraries.core.ConvertEtherToWei(0.0002, decimals_quoteToken),
        Libraries.core.ConvertEtherToWei(0.0005, decimals_quoteToken),
    ]
    PrintAndLog("quoteTokenQuantityToSpendList_weiUnits = " + str(quoteTokenQuantityToSpendList_weiUnits))

    # returnList_Eth, returnList_Tokens, returnList_Rate = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnKyberSellOnSet_TokenForToken(
    #     quoteToken, baseToken, quoteTokenQuantityToSpendList_weiUnits, setKey)

    returnList_Eth, returnList_Tokens, returnList_Rate = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnSetSellOnKyber_TokenForToken(
        quoteToken, baseToken, quoteTokenQuantityToSpendList_weiUnits, setKey)

elif sys.argv and len(sys.argv) >= 2 and "testninja_getarbitrage_setanduniswap" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)

    etherToSpendList_weiUnits = [
        Libraries.core.ConvertEtherToWei(1, Libraries.core.Ether_Decimals),
        Libraries.core.ConvertEtherToWei(2, Libraries.core.Ether_Decimals),
        Libraries.core.ConvertEtherToWei(5, Libraries.core.Ether_Decimals),
    ]

    setKey = 'ethlovol'
    token = TokenDict_Ninja['sai']
    returnList_Eth, returnList_Tokens, returnList_Rate = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnUniswapSellOnSet(
        token, etherToSpendList_weiUnits, setKey)

    # setKey = 'ethbtc26emaco'
    # token = TokenDict_Ninja['wbtc']
    # returnList_Eth, returnList_Tokens, returnList_Rate = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnSetSellOnUniswap(
    #     token, etherToSpendList_weiUnits, setKey)

elif sys.argv and len(sys.argv) >= 2 and "testninja_getarbitrage_setanduniswap_tokentotoken" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.priceOracle.StartService()

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    setKey = 'wbtcusdcv5'
    setTokenData = Exchanges.set.SetTokenDataDict[setKey]

    quoteToken = setTokenData.quoteTokenAddress
    baseToken, dontCare = setTokenData.GetBaseTokenAddress()
    decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteToken)))

    quoteTokenQuantityToSpendList_weiUnits = [
        Libraries.core.ConvertEtherToWei(0.0001, decimals_quoteToken),
        Libraries.core.ConvertEtherToWei(0.0002, decimals_quoteToken),
        Libraries.core.ConvertEtherToWei(0.0005, decimals_quoteToken),
    ]
    PrintAndLog("quoteTokenQuantityToSpendList_weiUnits = " + str(quoteTokenQuantityToSpendList_weiUnits))

    # returnList_Eth, returnList_Tokens = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnUniswapSellOnSet_TokenForToken(
    #     quoteToken, baseToken, quoteTokenQuantityToSpendList_weiUnits, setKey)

    returnList_Eth, returnList_Tokens = Exchanges.ninja.API_GetManyArbitrages_New_BuyOnSetSellOnUniswap_TokenForToken(
        quoteToken, baseToken, quoteTokenQuantityToSpendList_weiUnits, setKey)

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_setandkyber" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Libraries.priceOracle.StartService()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
    # NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    setKey = 'jethcusdc'
    setTokenData = Exchanges.set.SetTokenDataDict[setKey]

    ninjaName = "ninja-op-1"
    publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()

    kyberReserve = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")

    gas_tokenForToken = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_TokenForToken(
        setTokenData.quoteTokenAddress, setTokenData.GetBaseTokenAddress()[0], [ExchangeName_Set, ExchangeName_Kyber], False)
    gas = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_QuoteTokenIsEther(
        setTokenData.GetBaseTokenAddress()[0], [ExchangeName_Set, ExchangeName_Kyber], False)
    # gas = Libraries.gasStation.GetGasLimit_Ninja_SetAndKyber()
    # gas_tokenForToken = Libraries.gasStation.GetGasLimit_Ninja_SetAndKyber_TokenForToken()

    Libraries.gasStation.ConsiderGettingKybersMaxGasPriceValue()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()  # Use this gas price when testing
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()  # Use this gas price when testing

    PrintAndLog("setKey = " + str(setKey))
    PrintAndLog("price = " + str(setTokenData.price))
    PrintAndLog("side = " + str(setTokenData.side))
    PrintAndLog("inflow token = " + str(setTokenData.inflowTokenAddress))
    PrintAndLog("outflow token = " + str(setTokenData.outflowTokenAddress))

    # # This is failing wtf?
    # etherToConvert_etherUnits = 0.000001
    # etherToConvert_weiUnits = Libraries.core.ConvertEtherToWei(0.000001, Libraries.core.Ether_Decimals)
    # minConveresionRate_weiUnits = 1
    # minConveresionRate_etherUnits = Libraries.core.ConvertWeiToEther(minConveresionRate_weiUnits, Libraries.core.Ether_Decimals)
    # NinjaObject.API_TradeTokensOnKyber(publicAddress_Ninja, privateKey_Ninja, etherToConvert_weiUnits,
    #                                    Exchanges.kyber.EtherToken, etherToConvert_weiUnits, token.erc20TokenContractAddress,
    #                                    69203865833239757421118596509098632427930889272824243351707071692229331386368,
    #                                    minConveresionRate_etherUnits, "0x0000000000000000000000000000000000000000", gas, gasPrice)

    # Buy on Set, Sell on Kyber
    etherToConvert_etherUnits = 0.00009
    priceForConversion = Libraries.priceOracle.GetOnChainPrice_FromCache(setTokenData.GetBaseTokenAddress()[0])
    expectedTokensToTrade_etherUnits = Libraries.core.ConvertEtherToTokens(etherToConvert_etherUnits, priceForConversion)
    # minKyberPriceAcceptable = 21.80  # WBTC  # Should pass
    # minKyberPriceAcceptable = 81.80  # WBTC  # Should fail
    minKyberPriceAcceptable = 0.001760  # USDC/DAI  # Should pass
    # minKyberPriceAcceptable = 0.008760  # USDC/DAI  # Should fail
    dontCare, useSetCTokenBidderContract = setTokenData.GetBaseTokenAddress()
    decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(setTokenData.GetBaseTokenAddress()[0])))
    NinjaObject.API_TradeSimpleRequirements_BuyOnSetSellOnKyber(publicAddress_Ninja, privateKey_Ninja, etherToConvert_etherUnits, expectedTokensToTrade_etherUnits,
                                                                setTokenData.GetBaseTokenAddress()[0], decimals_baseToken, setTokenData, useSetCTokenBidderContract,
                                                                minKyberPriceAcceptable, kyberReserve, gas, gasPrice)

    # # Buy on Kyber, Sell on Set
    # # etherToConvert_etherUnits = 0.000001
    # etherToConvert_etherUnits = 0.000095
    # # maxKyberPriceAcceptable = 21.80  # WBTC  # Should fail
    # # maxKyberPriceAcceptable = 81.80  # WBTC  # Should pass
    # # maxKyberPriceAcceptable = 0.0028  # DAI  # Should fail
    # maxKyberPriceAcceptable = 0.0088  # DAI  # Should pass
    # dontCare, useSetCTokenBidderContract = setTokenData.GetBaseTokenAddress()
    # NinjaObject.API_TradeSimpleRequirements_BuyOnKyberSellOnSet(publicAddress_Ninja, privateKey_Ninja, etherToConvert_etherUnits,
    #                                                             setTokenData.GetBaseTokenAddress()[0], setTokenData, useSetCTokenBidderContract,
    #                                                             maxKyberPriceAcceptable, kyberReserve, gas, gasPrice)

    # quoteTokensToSpend_etherUnits = 0.000160 # Worked, but profit received in base token!
    # # https://etherscan.io/tx/0x04ff38eaea106159be8eb0907141370ed41339e95df8d89fc9cfd202287719ad
    #
    # quoteTokensToSpend_etherUnits = 0.000155  # Worked, profit with Quote token, dust with base tokens
    # # https://etherscan.io/tx/0xe3401a89d86040b0b313247cb524d2bd3ce01f721a89b454d8dfac8343cabf0d
    #
    # # quoteTokensToSpend_etherUnits = 0.000165  # Worked, but Quote token saw a "loss" while I was left with dust base tokens
    # # https://etherscan.io/tx/0x6db466d4d3e153fd1955ab36ac966aab8548b8a1bd37bce0dbbcea1c86853a9f
    #
    # NinjaObject.API_TradeSimpleRequirements_BuyOnKyberSellOnSet_TokenForToken(publicAddress_Ninja, privateKey_Ninja, quoteTokensToSpend_etherUnits,
    #                                                                           setTokenData.quoteTokenAddress, setTokenData.GetBaseTokenAddress()[0],
    #                                                                           setTokenData, gas_tokenForToken, gasPrice)

    # quoteTokensToSpend_etherUnits = 0.000160  #
    #
    # quoteTokensToSpend_etherUnits = 0.000170  #
    #
    # quoteTokensToSpend_etherUnits = 0.000155  #
    #
    # quoteTokensToSpend_etherUnits = 0.0025
    # NinjaObject.API_TradeSimpleRequirements_BuyOnSetSellOnKyber_TokenForToken(publicAddress_Ninja, privateKey_Ninja, quoteTokensToSpend_etherUnits,
    #                                                                           setTokenData.quoteTokenAddress, setTokenData.GetBaseTokenAddress()[0],
    #                                                                           setTokenData, gas_tokenForToken, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_setand0xv3" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)

    setKey = 'jethcusdc'
    setTokenData = Exchanges.set.SetTokenDataDict[setKey]

    ninjaName = "ninja-op-1"
    publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()

    PrintAndLog("setKey = " + str(setKey))
    PrintAndLog("price = " + str(setTokenData.price))
    PrintAndLog("side = " + str(setTokenData.side))
    PrintAndLog("inflow token = " + str(setTokenData.inflowTokenAddress))
    PrintAndLog("outflow token = " + str(setTokenData.outflowTokenAddress))

    PrintAndLog("Fill local test order that I made myself")

    # Order type from the maker's perspective
    # orderType = "sell"
    orderType = "buy"

    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "r")
        PrintAndLog("We are taker in the 0x trade, taker is sending ether in the 0x trade")
    elif orderType == "buy":
        file = open("testLocalOrder_0x_buy", "r")
        PrintAndLog("We are taker in the 0x trade, but we're making the Set trade first. So we are sending ether in the Set trade")

    data = file.read()
    file.close()
    # deserialize the file's data into an object
    order = jsonpickle.decode(data)
    PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(
        order.GetTokenQuantity()) + ", ether quantity + " + str(order.GetEtherQuantity()))

    gas = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_QuoteTokenIsEther(
        setTokenData.GetBaseTokenAddress()[0], [ExchangeName_Set, ExchangeName_0x], False)
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()

    # Buy on Set, Sell on 0xv3
    etherToSpend_etherUnits = 0.000169
    dontCare, useSetCTokenBidderContract = setTokenData.GetBaseTokenAddress()
    NinjaObject.API_TradeSimpleRequirements_BuyOnSetSellOn0xv3(publicAddress_Ninja, privateKey_Ninja,
                                                               etherToSpend_etherUnits, order, setTokenData,
                                                               useSetCTokenBidderContract, gas, gasPrice)

    # # Buy on 0xv3, Sell on Set
    # etherToSpend_etherUnits = 0.004790
    # dontCare, useSetCTokenBidderContract = setTokenData.GetBaseTokenAddress()
    # NinjaObject.API_TradeSimpleRequirements_BuyOn0xv3SellOnSet(publicAddress_Ninja, privateKey_Ninja,
    #                                                            etherToSpend_etherUnits, order, setTokenData,
    #                                                            useSetCTokenBidderContract, gas, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_setanduniswap" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Set()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
    # NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    # setKey = 'joeyethdai'
    # setKey = 'wbtcusdcv5'
    setKey = 'jethcusdc'
    setTokenData = Exchanges.set.SetTokenDataDict[setKey]

    ninjaName = "ninja-op-1"
    publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()

    gas = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_QuoteTokenIsEther(
        setTokenData.GetBaseTokenAddress()[0], [ExchangeName_Set, ExchangeName_Uniswap], False)
    gas_tokenForToken = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_TokenForToken(
        setTokenData.quoteTokenAddress, setTokenData.GetBaseTokenAddress()[0], [ExchangeName_Set, ExchangeName_Uniswap], False)

    Libraries.gasStation.ConsiderGettingKybersMaxGasPriceValue()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    PrintAndLog("setKey = " + str(setKey))
    PrintAndLog("price = " + str(setTokenData.price))
    PrintAndLog("side = " + str(setTokenData.side))
    PrintAndLog("inflow token = " + str(setTokenData.inflowTokenAddress))
    PrintAndLog("outflow token = " + str(setTokenData.outflowTokenAddress))

    # # Buy on Set, Sell on Uniswap
    # etherToSpend_etherUnits = 0.0000001
    # # minUniswapEthQuantityAcceptable_etherUnits = 1  # Should pass
    # minUniswapEthQuantityAcceptable_etherUnits = 1000000000000000000  # Should fail
    # NinjaObject.API_TradeSimpleRequirements_BuyOnSetSellOnUniswap(publicAddress_Ninja, privateKey_Ninja, etherToSpend_etherUnits, setTokenData.GetBaseTokenAddress()[0],
    #                                                               minUniswapEthQuantityAcceptable_etherUnits, setTokenData, gas, gasPrice)

    # Buy on Uniswap, Sell on Set
    # etherToSpend_etherUnits = 0.00001
    # etherToSpend_etherUnits = 0.00009
    etherToSpend_etherUnits = 0.00705
    maxUniswapEthQuantityAcceptable_etherUnits = 1000000000000000000  # Should pass
    # maxUniswapEthQuantityAcceptable_etherUnits = 1  # Should fail
    NinjaObject.API_TradeSimpleRequirements_BuyOnUniswapSellOnSet(publicAddress_Ninja, privateKey_Ninja, etherToSpend_etherUnits, setTokenData.GetBaseTokenAddress()[0],
                                                                  maxUniswapEthQuantityAcceptable_etherUnits, setTokenData, gas, gasPrice)

    # quoteTokensToSpend_etherUnits = 0.000160  # Worked, made a sliver of profit in the base token instead of the quote token!
    # # https://etherscan.io/tx/0x210b50848de7ae8fcd4887c31408f9fef7b8808e92b124884c075ba84291e0d7
    #
    # quoteTokensToSpend_etherUnits = 0.0001550  # Dust on USDC, and WBTC would have showed a loss due to this dust.
    # # https://etherscan.io/tx/0x7252480c97b8d2721b395fe553666cc5b3059a297773770ff7fc04b0ab41fa16
    #
    # # quoteTokensToSpend_etherUnits = 0.000165  # Dust on USDC, and WBTC would have showed a loss due to this dust.
    # # https://etherscan.io/tx/0x694693be83d5969f4e3398b0b15c94a5b180b31059f4dd26d24da92f895b969d
    #
    # quoteTokensToSpend_etherUnits = 0.0023
    # NinjaObject.API_TradeSimpleRequirements_BuyOnUniswapSellOnSet_TokenForToken(publicAddress_Ninja, privateKey_Ninja, quoteTokensToSpend_etherUnits,
    #                                                                             setTokenData.quoteTokenAddress, setTokenData.GetBaseTokenAddress()[0],
    #                                                                             setTokenData, gas, gasPrice)

    # quoteTokensToSpend_etherUnits = 0.000160  # Profit left in quote token, base token traded in exact quantities
    # # https://etherscan.io/tx/0x68f8cd570ec334f04fdfc4ee72022fc6ceef60e39e70b603cea6568320367000
    #
    # # quoteTokensToSpend_etherUnits = 0.000155  # Profit left in quote token, base token traded in exact quantities
    # # https://etherscan.io/tx/0x655f6b5131488ba0e11082dd35b4e6350581a6e3b3b84c95cad073734af73165
    #
    # # quoteTokensToSpend_etherUnits = 0.000165  # Profit left in quote token, base token traded in exact quantities
    # # https://etherscan.io/tx/0xe4793d182028ab0b591c8d89c9fbe891afaa3c55479d2ea057b2ce9537f8e328
    #
    # Libraries.priceOracle.StartService()
    # price_withRespectToQuoteToken = Libraries.priceOracle.GetOnChainPrice_FromCache_TokenForToken(setTokenData.quoteTokenAddress, setTokenData.GetBaseTokenAddress()[0])
    # expectedBaseTokensToTrade_etherUnits = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, price_withRespectToQuoteToken)
    # NinjaObject.API_TradeSimpleRequirements_BuyOnSetSellOnUniswap_TokenForToken(publicAddress_Ninja, privateKey_Ninja, quoteTokensToSpend_etherUnits,
    #                                                                             setTokenData.quoteTokenAddress, setTokenData.GetBaseTokenAddress()[0],
    #                                                                             expectedBaseTokensToTrade_etherUnits,
    #                                                                             setTokenData, gas, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_kyberand0xv2" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_NinjaBetween0xAndKyber()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)

    ninjaOwner = NinjaOpAccountDict["ninja-op-1"]
    erc20TokenContractAddress = ''
    symbol = ''
    token = TokenDict_Ninja[symbol.lower()]

    publicAddress_Ninja = ninjaOwner.publicAddress
    privateKey_Ninja = ninjaOwner.DecryptPK()

    kyberProxyContract = Exchanges.kyber.Contract_KyberNetworkProxy

    gas = Libraries.gasStation.GetGasLimit_Ninja_KyberAnd0x()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()  # Use this gas price when testing

    PrintAndLog("Fill local test order that I made myself")

    amountToConvert_etherUnits = 0.00001
    amountToConvert_weiUnits = Libraries.core.ConvertEtherAmountToBaseAmount(amountToConvert_etherUnits, Libraries.core.Ether_Decimals)

    # Order type from the maker's perspective
    # orderType = "sell"
    orderType = "buy"
    tokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(erc20TokenContractAddress)
    PrintAndLog("tokenAddress for 0x trade to be paired with WETH = " + str(tokenAddress))

    if orderType == "sell":
        file = open("testLocalOrder_0x_sell", "r")
    elif orderType == "buy":
        file = open("testLocalOrder_0x_buy", "r")

    data = file.read()
    file.close()
    # deserialize the file's data into an object
    order = jsonpickle.decode(data)
    PrintAndLog("order = " + str(order) + ", price = " + str(order.GetPrice()) + ", token quantity = " + str(order.GetTokenQuantity()) + ", ether quantity + " + str(
        order.GetEtherQuantity()) + ", amountToConvert_weiUnits = " + str(amountToConvert_weiUnits) + ", amountToConvert_etherUnits = " + str(amountToConvert_etherUnits))

    gas = Libraries.gasStation.GetGasLimit_TradeCustomContract()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    # # Trade, buy on 0xv2 and sell on Kyber
    # pathList_Kyber = token.pathList_SellingTokens_Kyber
    # NinjaObject.API_Trade_BuyOn0xv2SellOnKyber(publicAddress_Ninja, privateKey_Ninja, kyberProxyContract,
    #                                            pathList_Kyber, order, amountToConvert_weiUnits,
    #                                            gas, gasPrice)

    # # Trade, buy on Kyber and sell on 0xv2
    # pathList_Kyber = token.pathList_BuyingTokens_Kyber
    # NinjaObject.API_Trade_BuyOnKyberSellOn0xv2(publicAddress_Ninja, privateKey_Ninja, kyberProxyContract,
    #                                            pathList_Kyber, order, amountToConvert_weiUnits,
    #                                            gas, gasPrice)

    # # Trade Safe, buy on 0xv2 and sell on Kyber
    # pathList_Kyber = token.pathList_SellingTokens_Kyber
    # NinjaObject.API_TradeSafe_BuyOn0xv2SellOnKyber(publicAddress_Ninja, privateKey_Ninja, token, kyberProxyContract,
    #                                                pathList_Kyber, order, amountToConvert_weiUnits,
    #                                                gas, gasPrice)

    # Trade Safe, buy on Kyber and sell on 0xv2
    pathList_Kyber = token.pathList_BuyingTokens_Kyber
    NinjaObject.API_TradeSafe_BuyOnKyberSellOn0xv2(publicAddress_Ninja, privateKey_Ninja, token, kyberProxyContract,
                                                   pathList_Kyber, order, amountToConvert_weiUnits,
                                                   gas, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_generic" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_NinjaBetween0xAndKyber()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.Generic)

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    token = TokenDict_Ninja['snt']

    quoteTokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.keeperDAO.EthTokenContract)

    exchangeNameList = [ExchangeName_Uniswap, ExchangeName_Kyber]

    gas = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_QuoteTokenIsEther(
        token.erc20TokenContractAddress, exchangeNameList, False)
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()

    quoteTokensToSpend_etherUnits = 0.00001
    keeperDAOsMaxBorrowableBalance = Exchanges.keeperDAO.API_GetBorrowableBalance(Contract_KeeperDAO_LP_Simple, quoteTokenAddress)
    quoteTokensToSpend_etherUnits = min(quoteTokensToSpend_etherUnits, keeperDAOsMaxBorrowableBalance)

    # UpdateCoinMarketCapMarkets(1000, "id")
    # currentMarketPrice_withRespectToETH = CoinMarketCapMarketsDict[coinMarketCapId].GetPriceEth(coinMarketCapId, CoinMarketCapMarketsDict)
    # PrintAndLog(str(token.tokenName) + "'s currentMarketPrice_withRespectToETH = " + str(currentMarketPrice_withRespectToETH))

    # # Trade only when simple requirements are met, buy on Uniswap and sell on Kyber
    # expectedTokensToTrade_etherUnits = Libraries.core.ConvertEtherToTokens(etherToConvert_etherUnits, currentMarketPrice_withRespectToETH)
    # # Just put something in here for now that will pass the requirement
    # maxUniswapEthQuantityAcceptable_etherUnits = 1000000000000000000  # Should pass
    # # maxUniswapEthQuantityAcceptable_etherUnits = 1  # Should fail
    # # Just put something in here for now that will pass the requirement
    # minKyberPriceAcceptable = 0.00004617  # SNT  # Should pass
    # # minKyberPriceAcceptable = 0.00032617  # SNT  # Should fail
    # # minKyberPriceAcceptable = 0.00350204  # DAI  # Should pass
    # # minKyberPriceAcceptable = 0.00750204  # DAI  # Should fail
    # # minKyberPriceAcceptable = 0.00218880  # RLC  # Should pass
    # # minKyberPriceAcceptable = 0.00618880  # RLC  # Should fail
    # # minKyberPriceAcceptable = 0.00104924  # BAT  # Should pass
    # # minKyberPriceAcceptable = 0.00184924  # BAT  # Should fail
    # kyberReserve = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")
    # NinjaObject.API_TradeSimpleRequirements_BuyOnUniswapSellOnKyber(publicAddress_Ninja, privateKey_Ninja,
    #                                                                 etherToConvert_etherUnits, expectedTokensToTrade_etherUnits,
    #                                                                 token.erc20TokenContractAddress, token.decimals,
    #                                                                 maxUniswapEthQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
    #                                                                 kyberReserve, gas, gasPrice)

    # # Trade only when simple requirements are met, buy on Kyber and sell on Uniswap
    # # Just put something in here for now that will pass the requirement
    # minUniswapEthQuantityAcceptable_etherUnits = 1  # Should pass
    # # minUniswapEthQuantityAcceptable_etherUnits = 1000000000000000000  # Should fail
    # # Just put something in here for now that will pass the requirement
    # maxKyberPriceAcceptable = 0.00032617  # SNT  # Should pass
    # # maxKyberPriceAcceptable = 0.00002617  # SNT  # Should fail
    # # maxKyberPriceAcceptable = 0.00750204  # DAI  # Should pass
    # # maxKyberPriceAcceptable = 0.00350204  # DAI  # Should fail
    # # maxKyberPriceAcceptable = 0.00618880  # RLC  # Should pass
    # # maxKyberPriceAcceptable = 0.00218880  # RLC  # Should fail
    # # maxKyberPriceAcceptable = 0.00184924  # BAT  # Should pass
    # # maxKyberPriceAcceptable = 0.00104924  # BAT  # Should fail
    #
    # kyberReserve = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")
    # NinjaObject.API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap(publicAddress_Ninja, privateKey_Ninja,
    #                                                                 etherToConvert_etherUnits, token.erc20TokenContractAddress,
    #                                                                 minUniswapEthQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
    #                                                                 kyberReserve, gas, gasPrice)

    checkCallDataArrays_encoded = []
    tradeCallDataArray_encoded = []

    NinjaObject.API_TradeSimpleRequirements(ninjaOp.publicAddress, ninjaOp.DecryptPK(), quoteTokenAddress, quoteTokensToSpend_etherUnits,
                                            checkCallDataArrays_encoded, tradeCallDataArray_encoded, gasPrice, gas)

elif sys.argv and len(sys.argv) >= 2 and "testninja_estimategas_trades" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_NinjaBetweenBancorAnd0x()
    PopulateTokenDict_NinjaBetweenBancorAndKyber()
    PopulateTokenDict_NinjaBetween0xAndKyber()
    PopulateTokenDict_NinjaBetweenUniswapAndKyber()
    PopulateTokenDict_Ninja_Set()

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    publicAddress_Ninja = ninjaOp.publicAddress

    Libraries.estimatedGas.CalculateAndCache_EstimatedGasCost_Ninja_Trade(TokenDict_Ninja, publicAddress_Ninja)

elif sys.argv and len(sys.argv) >= 2 and "testninja_estimategas" == sys.argv[1].lower():
    # tokenAddress = '0x514910771af9ca656af840dff83e8264ecf986ca'.lower()  # LINK
    # tokenAddress = '0x0f5d2fb29fb7d3cfee444a200298f468908cc942'.lower()  # MANA
    tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'.lower()  # USDC
    # tokenAddress = '0x744d70fdbe2ba4cf95131626614a1763df805b9e'.lower()  # SNT
    # tokenAddress = '0x960b236a07cf122663c4303350609a66a7b288c0'.lower()  # ANT
    # tokenAddress = '0x57ab1e02fee23774580c119740129eac7081e9d3'.lower()  #sUSD
    # quoteTokenAddress = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'.lower()  # WBTC
    quoteTokenAddress = Exchanges.keeperDAO.EthTokenContract.lower()

    doUseCTokenBidderContract = False

    # allPossibleExchangesList = [ExchangeName_Kyber, ExchangeName_Uniswap, ExchangeName_0x, ExchangeName_Set, ExchangeName_Bancor]
    allPossibleExchangesList = [ExchangeName_Kyber, ExchangeName_Uniswap, ExchangeName_0x, ExchangeName_Set]
    exchangePairList = Libraries.utils.GetAllPossibleUniqueCombinationsOfItemsInList(allPossibleExchangesList, 2)
    for exchangePair in exchangePairList:
        # If this trade doesn't involve set, don't set useCTokenBidderContract to True ever because it's for set only
        if ExchangeName_Set not in exchangePair:
            useCTokenBidderContract = False
        # If this trade does involve set, use the value specified above
        else:
            useCTokenBidderContract = doUseCTokenBidderContract

        estimatedGasCost = Libraries.estimatedGas.GetEstimatedGasCost_Ninja_Trade_QuoteTokenIsEther(tokenAddress, exchangePair, useCTokenBidderContract)
        estimatedGasLimit = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_QuoteTokenIsEther(tokenAddress, exchangePair, useCTokenBidderContract)
        PrintAndLog("QuoteTokenIsEther: " + str(exchangePair))
        PrintAndLog("                   " + str(estimatedGasCost) + "/" + str(estimatedGasLimit))
        estimatedGasCost = Libraries.estimatedGas.GetEstimatedGasCost_Ninja_Trade_TokenForToken(quoteTokenAddress, tokenAddress, exchangePair, useCTokenBidderContract)
        estimatedGasLimit = Libraries.estimatedGas.GetSuggestedGasLimit_Ninja_Trade_TokenForToken(quoteTokenAddress, tokenAddress, exchangePair, useCTokenBidderContract)
        PrintAndLog("TokenForToken: " + str(exchangePair))
        PrintAndLog("               " + str(estimatedGasCost) + "/" + str(estimatedGasLimit))

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_history" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.uniswapV2.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswapV2.API_GetExchangeDataDict")

    Libraries.priceOracle.StartService()

    addressList = []
    addressList.append(Contract_Ninja_DiamondProxy.address)

    tradeLogicContractList = []
    tradeLogicContractList.append(Contract_Ninja_Trade_A)
    tradeLogicContractList.append(Contract_Ninja_Trade_B)

    # TODO, receiving dust as an ERC20 is not yet supported. A certain percentage of my trades receive ERC20's as dust so some of my profit is not actually being tracked!
    # TODO, cost of minting gas tokens is not yet supported. Many of my trades are using gas tokens, the minting process costs ETH and that's not yet factored in here

    # startblock_int = Libraries.core.ConvertDateTimeToBlockNumber(datetime.datetime(2020, 1, 20))
    startblock_int = Libraries.core.ConvertDateTimeToBlockNumber(datetime.datetime.utcnow() - datetime.timedelta(days=2))  # Get last n days of data
    # startblock_int = Libraries.core.ConvertDateTimeToBlockNumber(datetime.datetime.utcnow() - datetime.timedelta(hours=1))  # Get last n hours of data
    endblock_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    doApplyFilter = False

    sleepTime_s = 0.5

    for address in addressList:
        quoteToken = Libraries.core.GetEtherContractAddress()

        jData_internalTxList = Libraries.core.API_GetInternalTransactions_EtherScan(address, startblock_int, endblock_int)
        # PrintAndLog("jData_internalTxList = " + str(jData_internalTxList))

        # Be respectful to Etherscan's rate limits
        time.sleep(sleepTime_s)
        jData_txList = Libraries.core.API_GetTransactions_EtherScan(address, startblock_int, endblock_int)
        # PrintAndLog("jData_txList = " + str(jData_txList))

        # # Be respectful to Etherscan's rate limits
        # time.sleep(sleepTime_s)
        # jData_tokenList = Libraries.core.API_GetErc20TokenTransactions_EtherScan(address, startblock_int, endblock_int)
        # # PrintAndLog("jData_tokenList = " + str(jData_tokenList))

        hashDict = {}
        for jData in jData_internalTxList:
            hashDict[jData['hash'].lower()] = 'dontCare'

        # We need the txInfo so we can check the data sent to the tx to make sure it's relevant
        responseList_txInfo = Libraries.core.API_GetTransactionInfo_Batched_Safe(list(hashDict.keys()))
        # Convert the responseList_txInfo to a dict so I can easily access the response given a hash
        responseDict_txInfo = {}
        for txInfo in responseList_txInfo:
            responseDict_txInfo[txInfo['hash'].lower()] = txInfo

        for jData in jData_internalTxList:
            Exchanges.ninja.ParseHistoricTrades_InternalTxs_EtherScan(
                address, jData['hash'], quoteToken, jData, responseDict_txInfo[jData['hash']], tradeLogicContractList)

        for jData in jData_txList:
            Exchanges.ninja.ParseHistoricTrades_Txs_EtherScan(jData['hash'], jData, tradeLogicContractList)

    Exchanges.ninja.UpdateHistoricTradeData()

    if doApplyFilter:
        # Use this to filter out some txs based on gas price
        Exchanges.ninja.RemoveTradesWithGasPriceLowerThanThreshold(30,
                                                                   # Libraries.utils.MathSymbol.GreaterThan)
                                                                   Libraries.utils.MathSymbol.LessThan)

    Exchanges.ninja.PrintAllHistoricTrades()

elif sys.argv and len(sys.argv) >= 2 and "testninja_trade_history_hiding_game" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.zrxV4.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.uniswapV2.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswapV2.API_GetExchangeDataDict")

    Libraries.priceOracle.StartService()

    keepersList = []
    keepersList.append(Contract_Ninja_DiamondProxy.address.lower())

    zrxV4ExchangeContractToMatch = Exchanges.zrxV4.Contract_Exchange.lower()

    verboseLogging = False
    debugOnlyThisQuoteToken = False
    # debugOnlyThisQuoteToken = '0x6b175474e89094c44da98b954eedeac495271d0f'

    # startblock_int = Libraries.core.ConvertDateTimeToBlockNumber(datetime.datetime(2020, 1, 20))
    # startblock_int = Libraries.core.ConvertDateTimeToBlockNumber(datetime.datetime.utcnow() - datetime.timedelta(days=2))  # Get last n days of data
    # startblock_int = Libraries.core.ConvertDateTimeToBlockNumber(datetime.datetime.utcnow() - datetime.timedelta(hours=12))  # Get last n hours of data
    # endblock_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    # startblock_int = 11564040
    # endblock_int = 11783752

    # Expect to see only 2 valid trades
    # startblock_int = 11779359
    # endblock_int = 11779365

    blocksPerRewardEpoch = 6646
    firstEpochStartBlock_int = 11790004  # Beginning of reward program
    # firstEpochStartBlock_int = 11803296  # Start of epoch 2
    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    currentEpochsEndBlockNumber = latestBlockNumber + blocksPerRewardEpoch

    # First epoch ended at 902am ET

    blockNumber = firstEpochStartBlock_int
    epochBlockList = []
    epochBlockList.append(firstEpochStartBlock_int)
    numOfEpochsToScan = 1

    epochBlockNumbers = []
    # Find the current epoch end datetime
    blockNumber = firstEpochStartBlock_int
    while blockNumber < currentEpochsEndBlockNumber:
        if blockNumber % blocksPerRewardEpoch == 0:
            epochBlockNumbers.append(blockNumber)

        blockNumber += 1

    PrintAndLog("epochBlockNumbers = " + str(epochBlockNumbers))
    epochTransactionsDict = {}
    epochTransactionsDict['epochs'] = []
    for index, epochStartBlock_int in enumerate(epochBlockNumbers):
        if index == len(epochBlockNumbers) - 1:
            break
        epochEndBlock_int = epochBlockNumbers[index + 1] - 1
        PrintAndLog("epochStartBlock_int = " + str(epochStartBlock_int))
        PrintAndLog("epochEndBlock_int = " + str(epochEndBlock_int))

        txHashesToInspect = []
        for keeper in keepersList:
            eventLogs = Libraries.core.API_GetLogs_Safe(keeper, Libraries.topics.BuildTopicsArray_Ninja_ProfitFromTrade(), epochStartBlock_int, epochEndBlock_int)
            PrintAndLog("eventLogs = " + str(eventLogs))
            for log in eventLogs:
                txHashesToInspect.append(log['transactionHash'])

        PrintAndLog("txHashesToInspect = " + str(txHashesToInspect))

        matchResultList = []
        thisEpochDict = {}
        epochTransactionsDict['epochs'].append(thisEpochDict)
        thisEpochDict['index'] = index
        thisEpochDict['transactions'] = matchResultList
        thisEpochDict['epochStartBlock_int'] = epochStartBlock_int
        thisEpochDict['epochEndBlock_int'] = epochEndBlock_int
        responseList_txReceipt = Libraries.core.API_GetTransactionReceipt_Batched_Safe(txHashesToInspect)
        responseList_txInfo = Libraries.core.API_GetTransactionInfo_Batched_Safe(txHashesToInspect)
        # PrintAndLog("responseList_txReceipt = " + str(responseList_txReceipt))
        for index, local_txReceipt in enumerate(responseList_txReceipt):
            try:
                local_txHash = txHashesToInspect[index]
                local_txInfo = responseList_txInfo[index]
                local_txReceipt = responseList_txReceipt[index]
                # PrintAndLog("local_txInfo = " + str(local_txInfo))
                # PrintAndLog("local_txReceipt = " + str(local_txReceipt))
                PrintAndLog("Analyzing tx " + str(local_txHash))
                # PrintAndLog("Found local_txReceipt for " + str(local_txHash))
                local_eventLogs = Libraries.core.GetEventLogsFromTxReceipt(local_txReceipt)
                # PrintAndLog("local_eventLogs = " + str(local_eventLogs))
                # [
                #   {
                #     'address': '0xba100000625a3754423978a60c9317c58a424e3d',
                #     'topics': [
                #       '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef',
                #       '0x0000000000000000000000002faf487a4414fe77e2327f0bf4ae2a264a776ad2',
                #       '0x000000000000000000000000caf2d3f6c4a375ccec74eb7ed5c03f5b6cd8876e'
                #     ],
                #     'data': '0x0000000000000000000000000000000000000000000000019177059c3af76800',
                #     'blockNumber': '0xb3bd20',
                #     'transactionHash': '0x0472fa840d216cfc02738210ca1f21831394d38094578b7fcc200aab175fb3f5',
                #     'transactionIndex': '0x0',
                #     'blockHash': '0xeacfea44a51e4c63b8d27d7d869ad4a5c1b6eaa450666e22652dae26acf6ffcd',
                #     'logIndex': '0x0',
                #     'removed': False
                #   }
                # ]

                # Ninja profit
                #   {
                #     'address': '0x3d71d79c224998e608d03c5ec9b405e7a38505f0',
                #     'topics': [
                #       '0x4643b65b54e79c3bf1066dbe4300ca46e42ee7ddeaa0ca4c1c43ff74ab241f72'
                #     ],
                #     'data': '0x000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee0000000000000000000000000000000000000000000000000211c8613ff32eaa',
                #     'blockNumber': '0xb3bd24',
                #     'transactionHash': '0x5e956b2a77b3dc66462de4e62be434fbe1bc149967b816ca9b4a9527ed49f729',
                #     'transactionIndex': '0x3b',
                #     'blockHash': '0x76c1da239654f0b37761283133087186a411cd73d08c522ea8f935ef7ae24074',
                #     'logIndex': '0x59',
                #     'removed': False
                #   },

                # 0xv4 fill
                #   {
                #     'address': '0xdef1c0ded9bec7f1a1670819833240f027b25eff',
                #     'topics': [
                #       '0x829fa99d94dc4636925b38632e625736a614c154d55006b7ab6bea979c210c32'
                #     ],
                #     'data': '0x9e4629d9decfffa594ab460a44bb2afec4911443b54870729800cfe55bf5d663000000000000000000000000211b6a1137bf539b2750e02b9e525cf5757a35ae0000000000000000000000003d71d79c224998e608d03c5ec9b405e7a38505f0000000000000000000000000a0b86991c6218b36c1d19d4a2e9eb0ce3606eb48000000000000000000000000fa5047c9c78b8877af97bdcb85db743fd7313d4a0000000000000000000000000000000000000000000000005c4aca4e7efd3c2f00000000000000000000000000000000000000000000000000000000bfb52159000000000000000000000000000000000000000000000000000000000000002d',
                #     'blockNumber': '0xb3bd24',
                #     'transactionHash': '0x5e956b2a77b3dc66462de4e62be434fbe1bc149967b816ca9b4a9527ed49f729',
                #     'transactionIndex': '0x3b',
                #     'blockHash': '0x76c1da239654f0b37761283133087186a411cd73d08c522ea8f935ef7ae24074',
                #     'logIndex': '0x56',
                #     'removed': False
                #   },

                # TODO
                #  1.) The profit event topic must be present in the transaction.
                #  2.) The profit event topic must also have been emitted by a whitelisted KeeperDAO keeper.
                #  3.) The 0xv4 fill event topic must be present in the transaction.
                #  4.) The 0xv4 fill event topic must also have been emitted by the 0xv4 exchange proxy address.
                #  5.) The 0xv4 fill event taker address should match the whitelisted KeeperDAO keeper.
                #  6.) At this point, we've verified that the trade is indeed a Hiding Game trade via a valid KeeperDAO keeper.

                # Libraries.topics.Topic_Ninja_ProfitFromTrade
                # Libraries.topics.Topic_0xv4_Fill_RFQ
                # Libraries.topics.BuildTopicsArray_Ninja_ProfitFromTrade
                # Libraries.topics.BuildTopicsArray_0xv4_Fill_RFQ

                matchedEvent_ProfitByWhitelistedKeeper = False
                matchedEvent_0xv4FillByWhitelistedKeeper = False

                topicsToMatch = Libraries.topics.BuildTopicsArray_Ninja_ProfitFromTrade()
                quoteToken = None
                profit_quoteToken_etherUnits = None
                for eventLog in local_eventLogs:
                    topics = eventLog['topics']
                    if len(topics) == len(topicsToMatch) and topics[0].lower() == topicsToMatch[0].lower():
                        PrintAndLog("Found a potential match")
                        if eventLog['address'].lower() in keepersList:
                            PrintAndLog("A whitelisted keeper performed this action: keeper " + str(eventLog['address']))
                            # Extract the data params
                            data_with0xRemoved = eventLog['data'].replace("0x", "")
                            splitArray = Libraries.core.SplitStringIntoChunks(data_with0xRemoved, Libraries.core.LengthOfDataProperty)
                            quoteToken = Libraries.core.GetAddressFromDataProperty(splitArray[0])
                            profit_quoteToken_etherUnits = Libraries.core.GetIntFromDataProperty_EtherUnits(
                                splitArray[1], Libraries.core.GetDecimalsForTokenContract(quoteToken, True))
                            matchedEvent_ProfitByWhitelistedKeeper = True
                        else:
                            PrintAndLog("An address not found on our whitelist performed this action: address = " + str(eventLog['address']))

                if debugOnlyThisQuoteToken and debugOnlyThisQuoteToken.lower() != quoteToken.lower():
                    PrintAndLog("Skipping this quoteToken because we're debugging")
                    continue

                if not matchedEvent_ProfitByWhitelistedKeeper:
                    continue

                topicsToMatch = Libraries.topics.BuildTopicsArray_0xv4_Fill_RFQ()
                for eventLog in local_eventLogs:
                    topics = eventLog['topics']
                    if len(topics) == len(topicsToMatch) and topics[0].lower() == topicsToMatch[0].lower():
                        PrintAndLog("Found a potential match")
                        if eventLog['address'].lower() == zrxV4ExchangeContractToMatch.lower():
                            # Extract the data params
                            data_with0xRemoved = eventLog['data'].replace("0x", "")
                            splitArray = Libraries.core.SplitStringIntoChunks(data_with0xRemoved, Libraries.core.LengthOfDataProperty)
                            maker = Libraries.core.GetAddressFromDataProperty(splitArray[1])
                            taker = Libraries.core.GetAddressFromDataProperty(splitArray[2])
                            PrintAndLog("Analyzing potential 0xv4 fill with taker " + str(taker))
                            if taker.lower() in keepersList:
                                keeper = taker
                                user = maker
                                PrintAndLog("A whitelisted keeper performed this action: keeper " + str(eventLog['address']) + ", and user " + str(user))
                                matchedEvent_0xv4FillByWhitelistedKeeper = True

                                # Verify that all matches were found one more time
                                if matchedEvent_ProfitByWhitelistedKeeper and matchedEvent_0xv4FillByWhitelistedKeeper:
                                    matchResultData = {}
                                    matchResultData['txHash'] = local_txHash
                                    matchResultData['user'] = user
                                    matchResultData['keeper'] = keeper
                                    matchResultData['quoteToken'] = quoteToken
                                    matchResultData['profit_beforeGas'] = profit_quoteToken_etherUnits
                                    matchResultData['gasUsed'] = Libraries.core.ConvertHexToInt(local_txReceipt['gasUsed'])
                                    matchResultData['gasPrice'] = Libraries.core.ConvertHexToInt(local_txInfo['gasPrice'])
                                    matchResultData['gasCost_eth'] = Libraries.core.GetGasCost(matchResultData['gasPrice'], matchResultData['gasUsed'])
                                    matchResultList.append(matchResultData)
                            else:
                                PrintAndLog("taker is not a whitelisted keeper: taker = " + str(taker))

                        else:
                            PrintAndLog("An address not found to be zrxV4ExchangeContractToMatch performed this action: address = " + str(eventLog['address']))

                if matchedEvent_ProfitByWhitelistedKeeper and matchedEvent_0xv4FillByWhitelistedKeeper:
                    PrintAndLog("FOUND A MATCH: " + str(local_txHash))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                errorMessage = "exception = " + traceback.format_exc()
                PrintAndLog(errorMessage)
                PrintAndLogError(errorMessage)

        PrintAndLog("matchResultList of len " + str(len(matchResultList)) + " = " + str(matchResultList))

    # PrintAndLog("epochTransactionsDict = " + str(epochTransactionsDict))
    Libraries.cache.Cache_HidingBook_TransactionsDict(epochTransactionsDict)

    # Populate some statistics
    for epoch in epochTransactionsDict['epochs']:
        # Be careful when analyzing this data because multiple trades can occur during the same tx
        # We only want to count the profit from each tx once, so let's keep a dict of txHashes to ensure we don't double count profit
        # We can safely clear this dict once per epoch so it doesn't get too big and slow us down during processing
        uniqueTxHashDict = {}

        sumDict_profit_afterGas_quoteToken = {}
        # sum_profit_afterGas_usd = 0
        for transaction in epoch['transactions']:
            if debugOnlyThisQuoteToken and debugOnlyThisQuoteToken.lower() != transaction['quoteToken'].lower():
                PrintAndLog("Skipping this quoteToken because we're debugging")
                continue

            if transaction['txHash'].lower() in uniqueTxHashDict:
                if verboseLogging:
                    PrintAndLog("We've already processed this txHash " + str(transaction['txHash']) + ", skipping it so we don't double count")
                continue

            if verboseLogging:
                PrintAndLog("txHash = " + transaction['txHash'])

            # None ETH token so lets use price oracle to convert
            price_quoteToken_wrtETH = Libraries.priceOracle.GetOnChainPrice_FromCache(transaction['quoteToken'])
            if verboseLogging:
                PrintAndLog("   price_quoteToken_wrtETH = " + str(price_quoteToken_wrtETH))
            if transaction['quoteToken'].lower() not in sumDict_profit_afterGas_quoteToken:
                sumDict_profit_afterGas_quoteToken[transaction['quoteToken'].lower()] = 0
            profit_beforeGas_quoteToken = transaction['profit_beforeGas']
            if verboseLogging:
                PrintAndLog("   profit_beforeGas_quoteToken = " + str(profit_beforeGas_quoteToken) + " " + str(transaction['quoteToken'].lower()))
                PrintAndLog("   gasCost_eth = " + str(transaction['gasCost_eth']) + " ETH")
            gasCost_quoteToken = Libraries.core.ConvertQuoteTokensToBaseTokens(transaction['gasCost_eth'], price_quoteToken_wrtETH)
            if verboseLogging:
                PrintAndLog("   gasCost_quoteToken = " + str(gasCost_quoteToken) + " " + str(transaction['quoteToken'].lower()))
            profit_afterGas_quoteToken = transaction['profit_beforeGas'] - gasCost_quoteToken
            if verboseLogging:
                PrintAndLog("   profit_afterGas_quoteToken = " + str(profit_afterGas_quoteToken) + " " + transaction['quoteToken'].lower())
            sumDict_profit_afterGas_quoteToken[transaction['quoteToken'].lower()] += profit_afterGas_quoteToken
            if verboseLogging:
                PrintAndLog("   sumDict_profit_afterGas_quoteToken[transaction['quoteToken'].lower()] = " +
                            str(sumDict_profit_afterGas_quoteToken[transaction['quoteToken'].lower()]) + " " + transaction['quoteToken'].lower())

            # profit_afterGas_eth = Libraries.core.ConvertBaseTokensToQuoteTokens(profit_afterGas_quoteToken, price_quoteToken_wrtETH)
            # if verboseLogging:
            #     PrintAndLog("   profit_afterGas_eth = " + str(profit_afterGas_eth) + " ETH")
            # price_usd_wrtETH = Libraries.priceOracle.GetOnChainPrice_FromCache(Exchanges.keeperDAO.Contract_USDC)
            # if verboseLogging:
            #     PrintAndLog("   price_usd_wrtETH = " + str(price_usd_wrtETH))
            # profit_afterGas_usd = Libraries.core.ConvertQuoteTokensToBaseTokens(profit_afterGas_eth, price_usd_wrtETH)
            # if verboseLogging:
            #     PrintAndLog("   profit_afterGas_usd = " + str(profit_afterGas_usd) + " USD")
            # sum_profit_afterGas_usd += profit_afterGas_usd

            uniqueTxHashDict[transaction['txHash'].lower()] = transaction['txHash'].lower()

        epochHasConcluded = True
        blocksLeftInEpoch = None
        epochInfoString = ""
        if epoch['epochEndBlock_int'] < latestBlockNumber:
            epochInfoString = "Epoch has concluded"
        else:
            epochHasConcluded = False
            blocksLeftInEpoch = epoch['epochEndBlock_int'] - latestBlockNumber
            epochInfoString = "Epoch still in progress, " + str(blocksLeftInEpoch) + " blocks remaining"

        PrintAndLog("Stats for epoch " + str(epoch['index']) + ". " + str(epochInfoString))
        PrintAndLog("   GetThreadSafeCopyOf_LatestBlockNumber_int = " + str(Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()))
        PrintAndLog("   latestBlockNumber = " + str(latestBlockNumber))
        PrintAndLog("   startBlock " + str(epoch['epochStartBlock_int']))
        PrintAndLog("   endBlock " + str(epoch['epochEndBlock_int']))
        PrintAndLog("   sumDict_profit_afterGas_quoteToken = " + str(sumDict_profit_afterGas_quoteToken))
        # PrintAndLog("   sum_profit_afterGas_usd = " + str(sum_profit_afterGas_usd) + " NOTE: Token prices converted to USD Prices as of NOW not at time of trade.")
        PrintAndLog("----------------------------------------------------------------------")

elif sys.argv and len(sys.argv) >= 2 and "testninja_cutdiamond_update" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    ninjaProxy = Libraries.diamondProxy.DiamondProxy(ninjaOp, Contract_Ninja_DiamondProxy.address)

    nonce = None
    # nonce = 1968

    doCutDiamond = True
    doRemoveAllFunctionSelectorsForLogicContractsWithin_logicContractList_whoseFunctionSelectorsAreEligibleToBeOverridden = True

    # Declare if any logic contracts are eligible to be overwritten
    # Logic contracts NOT listed in here will not be overwritten
    logicContractList_whoseFunctionSelectorsAreEligibleToBeOverridden = [
        Libraries.nodes.Instance_Web3.toChecksumAddress('0xad509bed09e33730004cb45a9ff7a1e3d9bfe011'),
    ]

    # Consider registering our logic contract with the proxy
    diamondCutDict_updates = {}
    diamondCutDict_removes = {}

    # Before we consider updating, check to see if it's already been registered
    facetAddressList = Libraries.diamondProxy.API_TestProxy_LogicContract_GetFacetAddresses()

    # logicContractTupleList contains tuples,
    # first item is the logic contract name
    # second item is the logic contract address,
    # third item is the contract object itself
    logicContractTupleList = []
    logicContractTupleList.append(("Contract_Ninja_Trade_A", Contract_Ninja_Trade_A.address, Contract_Ninja_Trade_A))
    logicContractTupleList.append(("Contract_Ninja_Trade_B", Contract_Ninja_Trade_B.address, Contract_Ninja_Trade_B))
    logicContractTupleList.append(("Contract_AssetManager", Contract_Ninja_AssetManager.address, Contract_Ninja_AssetManager))
    logicContractTupleList.append(("Contract_GasTokens", Contract_Ninja_GasTokens.address, Contract_Ninja_GasTokens))
    logicContractTupleList.append(("Contract_Properties", Contract_Ninja_Properties.address, Contract_Ninja_Properties))
    logicContractTupleList.append(("Contract_Authentication_KeeperDAOLPPs", Contract_Ninja_Authentication_KeeperDAOLPPs.address, Contract_Ninja_Authentication_KeeperDAOLPPs))

    # logicContractTupleList.append(("Contract_Ninja_Trade", Contract_Ninja_Trade.address, Contract_Ninja_Trade))
    # logicContractTupleList.append(("Contract_Ninja_Trade_2", Contract_Ninja_Trade_2.address, Contract_Ninja_Trade_2))
    logicContractTupleList.append(("Contract_Ninja_Trade_3", Contract_Ninja_Trade_3.address, Contract_Ninja_Trade_3))
    logicContractTupleList.append(("Contract_Ninja_Exchange_Kyber", Contract_Ninja_Exchange_Kyber.address, Contract_Ninja_Exchange_Kyber))
    logicContractTupleList.append(("Contract_Ninja_Exchange_Uniswap", Contract_Ninja_Exchange_Uniswap.address, Contract_Ninja_Exchange_Uniswap))
    logicContractTupleList.append(("Contract_Ninja_Exchange_0xv3", Contract_Ninja_Exchange_0xv3.address, Contract_Ninja_Exchange_0xv3))
    logicContractTupleList.append(("Contract_Ninja_Exchange_0xv4", Contract_Ninja_Exchange_0xv4.address, Contract_Ninja_Exchange_0xv4))
    logicContractTupleList.append(("Contract_Ninja_Exchange_Balancer", Contract_Ninja_Exchange_Balancer.address, Contract_Ninja_Exchange_Balancer))
    logicContractTupleList.append(("Contract_Ninja_Exchange_UniswapV2", Contract_Ninja_Exchange_UniswapV2.address, Contract_Ninja_Exchange_UniswapV2))
    logicContractTupleList.append(("Contract_Ninja_Exchange_Curve", Contract_Ninja_Exchange_Curve.address, Contract_Ninja_Exchange_Curve))

    if doRemoveAllFunctionSelectorsForLogicContractsWithin_logicContractList_whoseFunctionSelectorsAreEligibleToBeOverridden:
        for logicContract_toRemove in logicContractList_whoseFunctionSelectorsAreEligibleToBeOverridden:
            functionSelectorList = Libraries.diamondProxy.API_TestProxy_LogicContract_GetFacetFunctionSelectors(logicContract_toRemove)
            PrintAndLog('Updating diamondCutDict_removes with functionSelectorList = ' + str(functionSelectorList))

            # Add these functionSelectorList to the diamondCutDict_removes
            # If the null address is already in diamondCutDict_removes
            if Libraries.core.GetNullAddress().lower() in diamondCutDict_removes:
                # Combine lists since there's already a list in the value, combine that with our new list of functionSelectorList
                PrintAndLog("Combine lists since there's already a list in the value, combine that with our new list of functionSelectorList")
                PrintAndLog("diamondCutDict_removes[Libraries.core.GetNullAddress().lower()] was = " + str(diamondCutDict_removes[Libraries.core.GetNullAddress().lower()]))
                PrintAndLog("functionSelectorList is = " + str(functionSelectorList))
                diamondCutDict_removes[Libraries.core.GetNullAddress().lower()] += functionSelectorList

            # null address has not yet been added to diamondCutDict_removes
            else:
                PrintAndLog("null address has not yet been added to diamondCutDict_removes")
                diamondCutDict_removes[Libraries.core.GetNullAddress().lower()] = functionSelectorList

            PrintAndLog("diamondCutDict_removes[Libraries.core.GetNullAddress().lower()] after update is = " + str(
                diamondCutDict_removes[Libraries.core.GetNullAddress().lower()]))

        PrintAndLog('diamondCutDict_removes = ' + str(diamondCutDict_removes))

    # Lowercase all the addresses
    logicContractList_whoseFunctionSelectorsAreEligibleToBeOverridden = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(
        logicContractList_whoseFunctionSelectorsAreEligibleToBeOverridden)

    for logicContractTuple in logicContractTupleList:
        name, logicContractAddress, logicContractObject = logicContractTuple
        PrintAndLog("Checking proxy registration status of the following logic contract: " + str(name) + " " + str(logicContractAddress))
        # Set the key as the logic contract address when setting a new logic contract
        diamondCutDict_updates[logicContractAddress.lower()] = Libraries.signingUtils.GetFunctionSelectorsListForAllFunctionsInContract(logicContractObject)
        PrintAndLog("Found " + str(len(diamondCutDict_updates[logicContractAddress.lower()])) + " function selectors for this logic contract's ABI. " + str(
            diamondCutDict_updates[logicContractAddress.lower()]))

        functionSelectorList = Libraries.diamondProxy.API_TestProxy_LogicContract_GetFacetFunctionSelectors(logicContractAddress)

        PrintAndLog("Found " + str(len(functionSelectorList)) + " function selectors already registered in the proxy for this logic contract. " + str(
            functionSelectorList))

        # for each function selector, check to see if it's already registered. If it is, remove it from the list in diamondCutDict_updates[logicContractAddress.lower()]
        functionSelectorsToRemoveFromList = []
        for functionSelector in diamondCutDict_updates[logicContractAddress.lower()]:
            functionSelectorLogicAddress = Libraries.diamondProxy.API_TestProxy_LogicContract_GetFacetAddress('0x' + functionSelector)
            doesFunctionSelectorExistInProxy = functionSelectorLogicAddress.lower() != Libraries.core.GetNullAddress().lower()
            PrintAndLog("functionSelector = " + str(functionSelector) + ", doesFunctionSelectorExistInProxy = " + str(
                doesFunctionSelectorExistInProxy) + ", functionSelectorLogicAddress = " + str(functionSelectorLogicAddress))
            if doesFunctionSelectorExistInProxy:
                if functionSelectorLogicAddress.lower() == logicContractAddress.lower():
                    PrintAndLog("Remove the functionSelector from the diamondCutDict_updates's list because it's already set properly")
                    functionSelectorsToRemoveFromList.append(functionSelector)
                else:
                    # We have been instructed to overwrite a logic address' function selector.
                    # Check to see whether or not we have been given permission
                    if functionSelectorLogicAddress.lower() in logicContractList_whoseFunctionSelectorsAreEligibleToBeOverridden:
                        PrintAndLog("We have been given permission to overwrite function selectors in logic contract " + str(functionSelectorLogicAddress))
                    else:
                        PrintAndLog("We have NOT been given permission to overwrite function selectors in logic contract: name = " + str(
                            name) + ", new logicContractAddress = " + str(logicContractAddress) + " and functionSelector = " + str(
                            functionSelector) + ", old contract address = " + str(functionSelectorLogicAddress))
                        raise Exception("I caught myself trying to overwrite a function! "
                                        "Do I have the same exact function in two different logic contracts? This is bad, NEVER do this!")

            else:
                PrintAndLog("The function selector is not registered in the proxy, so we are free to add it at will")
                pass

        PrintAndLog("Removing " + str(len(functionSelectorsToRemoveFromList)) + " function selectors from " + str(name) + " because they are already registered")
        for functionSelector in functionSelectorsToRemoveFromList:
            diamondCutDict_updates[logicContractAddress.lower()].remove(functionSelector)

        PrintAndLog("diamondCutDict_updates[logicContractAddress.lower()] after removing already registered function selectors, = " + str(
            diamondCutDict_updates[logicContractAddress.lower()]))

        if len(diamondCutDict_updates[logicContractAddress.lower()]) <= 0:
            PrintAndLog("Removing " + str(name) + " " + str(logicContractAddress) + " from diamondCutDict_updates all together because it's already up to date")
            del diamondCutDict_updates[logicContractAddress.lower()]

        if logicContractAddress.lower() in diamondCutDict_updates and len(diamondCutDict_updates[logicContractAddress.lower()]) > 0:
            PrintAndLog("Adding " + str(len(diamondCutDict_updates[logicContractAddress.lower()])) + " function selectors for " + str(
                name) + " " + str(logicContractAddress) + " to diamondCutDict_updates. diamondCutDict_updates = " + str(diamondCutDict_updates))
        else:
            PrintAndLog("NOT updating function selectors for " + str(name) + " " + str(
                logicContractAddress) + " to diamondCutDict_updates because they would override. diamondCutDict_updates = " + str(diamondCutDict_updates))

    # Merge diamondCutDict_updates with diamondCutDict_removes
    PrintAndLog("diamondCutDict_removes = " + str(diamondCutDict_removes))
    PrintAndLog("diamondCutDict_updates = " + str(diamondCutDict_updates))
    diamondCutDict_merged = Libraries.core.MergeDictionaries(diamondCutDict_updates, diamondCutDict_removes)
    PrintAndLog("Merged diamondCutDict_merged and diamondCutDict_merged")
    PrintAndLog("diamondCutDict_merged = " + str(diamondCutDict_merged))

    if len(diamondCutDict_merged) <= 0:
        PrintAndLog("Not calling API_DiamondCut because diamondCutDict_merged was empty. diamondCutDict_merged = " + str(diamondCutDict_merged))
    elif doCutDiamond:
        ninjaProxy.API_DiamondCut(diamondCutDict_merged, gasPrice, nonce)
    else:
        PrintAndLog("Not calling API_DiamondCut because doCutDiamond was set to " + str(doCutDiamond))

elif sys.argv and len(sys.argv) >= 2 and "testninja_cutdiamond_remove" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    ninjaProxy = Libraries.diamondProxy.DiamondProxy(ninjaOp, Contract_Ninja_DiamondProxy.address)

    nonce = None

    # # Set the key to 0x0000....0000 when deleting a logic contract function
    logicContract_toRemove = ''
    functionSelectorList = Libraries.diamondProxy.API_TestProxy_LogicContract_GetFacetFunctionSelectors(logicContract_toRemove)
    PrintAndLog('functionSelectorList = ' + str(functionSelectorList))

    diamondCutDict = {}
    diamondCutDict[Libraries.core.GetEtherContractAddress()] = functionSelectorList
    PrintAndLog('diamondCutDict = ' + str(diamondCutDict))

    ninjaProxy.API_DiamondCut(diamondCutDict, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testninja_proxycalls" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    ninjaProxy = Libraries.diamondProxy.DiamondProxy(ninjaOp, Contract_Ninja_DiamondProxy.address)

    nonce = None
    # nonce = 92

    # Testing approvals
    # tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # spender = Exchanges.kyber.Contract_KyberNetworkProxy
    # # spender = Exchanges.uniswap.ContractDict_Exchange[tokenAddress.lower()]['exchange']
    # approvalAmount_weiUnits = 792089237316195423570985008687907853269984665640564039457584007913129639935
    # Exchanges.ninja.API_TestProxy_LogicContract_EnsureAllowance(ninjaOp.publicAddress, ninjaOp.DecryptPK(), tokenAddress, spender, approvalAmount_weiUnits, gasPrice)

    # addressList = [NinjaOpAccountDict["ninja-op-102"].publicAddress]
    # Exchanges.ninja.API_TestProxy_LogicContract_WhitelistUsers(ninjaOp.publicAddress, ninjaOp.DecryptPK(), addressList, gasPrice)

    # ******* NEW CALLS BELOW that have logic contracts broken down correctly ******** #

    # Exchanges.ninja.API_TestProxy_LogicContract_WithdrawAllEther(ninjaOp.publicAddress, ninjaOp.DecryptPK(), gasPrice)

    # Exchanges.ninja.API_TestProxy_LogicContract_MintGasTokens(ninjaOp.publicAddress, ninjaOp.DecryptPK(), 10, gasPrice)

    # Exchanges.ninja.API_TestProxy_LogicContract_SetInts(ninjaOp.publicAddress, ninjaOp.DecryptPK(), gasPrice, nonce)
    # Exchanges.ninja.API_TestProxy_LogicContract_SetAddresses(ninjaOp.publicAddress, ninjaOp.DecryptPK(), gasPrice, nonce)

    # etherToSpend_etherUnits = 0.000001
    # tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # nonce = None
    # # nonce = 87

    # Exchanges.ninja.API_TestProxy_LogicContract_TradeTestUniswap(
    #     ninjaOp.publicAddress, ninjaOp.DecryptPK(), etherToSpend_etherUnits, tokenAddress,
    #     gasPrice, nonce, False, True)

    # Exchanges.ninja.API_TestProxy_LogicContract_TradeTestKyber(
    #     ninjaOp.publicAddress, ninjaOp.DecryptPK(), etherToSpend_etherUnits, tokenAddress,
    #     gasPrice, nonce, False, True)

elif sys.argv and len(sys.argv) >= 2 and "testninja_tracetrade" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # Example usage txHash, comma separated tokens involved in the trade
    # testninja_tracetrade 0x285a6f0df0c7e1f889ec71c7b34452d072fbc36824b7458242d95edb3f6d23b5 0x543ff227f64aa17ea132bf9886cab5db55dcaddf
    # testninja_tracetrade 0x285a6f0df0c7e1f889ec71c7b34452d072fbc36824b7458242d95edb3f6d23b5 0x543ff227f64aa17ea132bf9886cab5db55dcaddf,0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2

    # Suppress these PrintAndLog messages for this feature since it would make our output messy
    Libraries.core.DoSuppressPrintAndLog_SendRequestToSpecifiedNodes = True

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")
    Exchanges.uniswapV2.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswapV2.API_GetExchangeDataDict")
    Exchanges.balancer.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.balancer.API_GetExchangeDataDict")

    txHash = sys.argv[2].lower()
    PrintAndLog("txHash = " + str(txHash))
    tokenAddressesToTraceList = sys.argv[3].lower().split(',')
    PrintAndLog("tokenAddressesToTraceList = " + str(tokenAddressesToTraceList))
    Libraries.ninjaUtils.TraceTrade(txHash, tokenAddressesToTraceList)

elif sys.argv and len(sys.argv) >= 2 and "testninja_tracetrade_from_log_file" == sys.argv[1].lower():
    # Suppress these PrintAndLog messages for this feature since it would make our output messy
    Libraries.core.DoSuppressPrintAndLog_SendRequestToSpecifiedNodes = True

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")
    Exchanges.uniswapV2.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswapV2.API_GetExchangeDataDict")
    Exchanges.balancer.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.balancer.API_GetExchangeDataDict")

    directory = '/Users/pako/Desktop/Ninja Nov 28/'
    Libraries.postMortem.PostMortem_Logs(directory)

elif sys.argv and len(sys.argv) >= 2 and "testninja_tailgate_uniswapv2" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    txInputData = '0x7ff36ab50000000000000000000000000000000000000000000000000000002cfd33a9650000000000000000000000000000000000000000000000000000000000000080000000000000000000000000ae9e4dee1d03404e91d5b22db811c10f7d251c22000000000000000000000000000000000000000000000000000000005f2067730000000000000000000000000000000000000000000000000000000000000002000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc2000000000000000000000000a0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'

    functionSelectorDict, functionNameDict = Libraries.signingUtils.GetFunctionSelectorsDictForAllFunctionsInContract(Contract_UniswapV2_Router)
    PrintAndLog("functionSelectorDict = " + str(functionSelectorDict))
    PrintAndLog("functionNameDict = " + str(functionNameDict))
    functionSelector = Libraries.signingUtils.GetFunctionSelectorFromTxInputData(txInputData)
    PrintAndLog("functionSelector = " + str(functionSelector))

    # Find the function name in the dict
    functionName = functionNameDict[functionSelector]
    PrintAndLog("functionName = " + str(functionName))

    # self.tradeFunctionName = "tokenToEthSwapInput"
    # PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
    # dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(data_withoutMethodHash, Libraries.core.LengthOfDataProperty)
    # PrintAndLog_FuncNameHeader("dataList = " + str(dataList))
    #
    # self.tokensToSpend_etherUnits = Libraries.core.GetIntFromDataProperty_EtherUnits(dataList[0], self.decimals_token)
    # self.etherToReceive_etherUnits = Libraries.core.GetIntFromDataProperty_EtherUnits(dataList[1], Libraries.core.Ether_Decimals)
    # self.deadline = Libraries.core.GetIntFromDataProperty_WeiUnits(dataList[2])
    # self.effectiveEtherValueOfTrade = self.etherToReceive_etherUnits
    # PrintAndLog_FuncNameHeader("tokensToSpend_etherUnits = " + str(self.tokensToSpend_etherUnits))
    # PrintAndLog_FuncNameHeader("etherToReceive_etherUnits = " + str(self.etherToReceive_etherUnits))
    # PrintAndLog_FuncNameHeader("deadline = " + str(self.deadline))
    # PrintAndLog_FuncNameHeader("effectiveEtherValueOfTrade = " + str(self.effectiveEtherValueOfTrade))
    #
    # self.worstPriceThisPersonAccepts = Libraries.core.GetPriceGivenEtherAndTokens(self.etherToReceive_etherUnits, self.tokensToSpend_etherUnits)
    # PrintAndLog_FuncNameHeader("worse price this person is accepting = " + str(self.worstPriceThisPersonAccepts))
    #
    # self.side = "sell"
    # PrintAndLog_FuncNameHeader("side = " + str(self.side))
    # self.priceThisPersonExpectsToGet = Exchanges.uniswap.CalculatePriceGivenBalances(
    #     self.side, self.tokensToSpend_etherUnits, self.uniswapBalance_ether, self.uniswapBalance_tokens)
    # PrintAndLog_FuncNameHeader("priceThisPersonExpectsToGet = " + str(self.priceThisPersonExpectsToGet))
    #
    # # Calculate the price we predict the exchange will have after this trade gets mined in
    # self.uniswapBalanceExpected_ether_afterTxMinesIn, self.uniswapBalanceExpected_token_afterTxMinesIn = \
    #     Exchanges.uniswap.CalculateNewExchangeBalancesAssumingTheseTradesGetMinedIn(
    #         [self.side], [self.etherToReceive_etherUnits], [self.tokensToSpend_etherUnits], self.uniswapBalance_ether, self.uniswapBalance_tokens)
    # PrintAndLog_FuncNameHeader(
    #     "uniswapBalanceExpected_ether_afterTxMinesIn after TX gets mined in = " + str(self.uniswapBalanceExpected_ether_afterTxMinesIn) + " ETH")
    # PrintAndLog_FuncNameHeader(
    #     "uniswapBalanceExpected_token_afterTxMinesIn after TX gets mined in = " + str(self.uniswapBalanceExpected_token_afterTxMinesIn) + " " + str(
    #         self.tokenSymbol))
    #
    # self.priceExpected_afterTxMinesIn = Exchanges.uniswap.CalculatePriceGivenBalances(
    #     self.side, self.tokensToSpend_etherUnits, self.uniswapBalanceExpected_ether_afterTxMinesIn, self.uniswapBalanceExpected_token_afterTxMinesIn)
    # PrintAndLog_FuncNameHeader("priceExpected_afterTxMinesIn = " + str(self.priceExpected_afterTxMinesIn))
    #
    # # Validations
    # PrintAndLog_FuncNameHeader("Validating transaction")
    # self.Validate_Price()
    # self.Validate_Deadline()
    # self.Validate_Balance_Tokens(self.tokensToSpend_etherUnits)
    # self.Validate_Allowance_Tokens(self.tokensToSpend_etherUnits)
    #
    # # If we've made it this far without exception, we've passed all validations
    #
    # PrintAndLog_FuncNameHeader("MemPool sniffer found a " + str(self.tradeFunctionName) + " on Uniswap of size " + str(
    #     self.etherToReceive_etherUnits) + " ETH, txHash = " + str(self.txHash))
    
elif sys.argv and len(sys.argv) >= 2 and "testninja_get_balances_over_history" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    address = "0x3D71d79C224998E608d03C5Ec9B405E7a38505F0"
    # address = "0x53463cd0b074E5FDafc55DcE7B1C82ADF1a43B2E"
    startBlock = 11803296
    endBlock = 11809941
    # endBlock = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    currentBlock = startBlock
    step = 100
    while currentBlock < endBlock + step:
        balance_ether = Libraries.core.API_GetEtherBalance(address, None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)
        balance_weth = Libraries.core.API_GetTokenBalance(address, Exchanges.zrxV2.Contract_WETH, Libraries.core.Ether_Decimals,
                                                          None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)

        token = Exchanges.keeperDAO.Contract_USDC
        decimals_token = Libraries.core.GetDecimalsForTokenContract(token, True)
        balance_usdc = Libraries.core.API_GetTokenBalance(address, token, decimals_token,
                                                          None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)

        token = Exchanges.keeperDAO.Contract_DAI
        decimals_token = Libraries.core.GetDecimalsForTokenContract(token, True)
        balance_dai = Libraries.core.API_GetTokenBalance(address, token, decimals_token,
                                                         None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)

        token = Exchanges.keeperDAO.Contract_USDT
        decimals_token = Libraries.core.GetDecimalsForTokenContract(token, True)
        balance_usdt = Libraries.core.API_GetTokenBalance(address, token, decimals_token,
                                                          None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)

        token = Exchanges.keeperDAO.Contract_wBTC
        decimals_token = Libraries.core.GetDecimalsForTokenContract(token, True)
        balance_wbtc = Libraries.core.API_GetTokenBalance(address, token, decimals_token,
                                                          None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)

        token = Exchanges.keeperDAO.Contract_renBTC
        decimals_token = Libraries.core.GetDecimalsForTokenContract(token, True)
        balance_renbtc = Libraries.core.API_GetTokenBalance(address, token, decimals_token,
                                                            None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)

        balance_total = balance_ether + balance_weth
        PrintAndLog("block " + str(currentBlock))
        PrintAndLog("   balance_ether = " + str(round(balance_ether, 1)))
        PrintAndLog("   balance_weth = " + str(round(balance_weth, 1)))
        PrintAndLog("   balance_usdc = " + str(round(balance_usdc, 1)))
        PrintAndLog("   balance_dai = " + str(round(balance_dai, 1)))
        PrintAndLog("   balance_usdt = " + str(round(balance_usdt, 1)))
        PrintAndLog("   balance_wbtc = " + str(round(balance_wbtc, 4)))
        PrintAndLog("   balance_renbtc = " + str(round(balance_renbtc, 4)))
        currentBlock += step

elif sys.argv and len(sys.argv) >= 2 and "testgetcode" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    isAContract = Libraries.core.IsAddressAContract('0x0015CB2187299D43B6313aE138a966899c72630c')
    # isAContract = Libraries.core.IsAddressAContract('0x3D71d79C224998E608d03C5Ec9B405E7a38505F0')

    # Libraries.core.API_GetCode('0x0015CB2187299D43B6313aE138a966899c72630c')
    # # Libraries.core.API_GetCode('0x3D71d79C224998E608d03C5Ec9B405E7a38505F0')

elif sys.argv and len(sys.argv) >= 2 and "testnode" == sys.argv[1].lower():
    PrintAndLog("Not entering main loop, do some functional testing here!")

    ConsiderUpdatingLatestBlockNumber()
    # Libraries.core.API_AllNodes_GetBalance()

    PrintAndLog("tx_receipt = " + str(API_PostCheckPendingTransaction("0x90c612e97bc8a8761bb80714a89a544479b4204cd1655f170ffe7ff354664143")))
    # PrintAndLog("Step 1")
    # tx_receipt = Libraries.core.API_GetTransactionReceipt("0x3af2888086ebbf03e58575f2e60007a2cafa48dcc851a28e559ec66a10371413")
    # PrintAndLog("Step 2")

    # tx_receipt = Libraries.core.API_GetTransactionReceipt("0x08d69f11b967d745e54c47feaa564d8f3d8a3282242d46a94a26fc1c5fcf20f6")
    # PrintAndLog("Step 3")

    # tx_receipt = Libraries.core.API_GetTransactionReceipt("0xfc0485a70ed3fed766c431b9a16efc2101cbc8f7ca20a224d87d40766b05a20b")
    # Libraries.core.API_GetTransactionReceipt("0x90fb7834a805ffa823860a922b2e82200781c765f20487cea330c02eda965016")
    # Libraries.core.API_GetTransactionReceipt("0x9acc445420cbd812cdde3a56294d980ee3ce5d7858d7a538893d04b03efb2220")

    # instance_Web3 = Web3(Libraries.nodes.URL_RemoteNode.joeyz_VA)
    # Web3("opp")
    # PrintAndLog("w3 = " + str(w3))
    # tx_receipt = w3.eth.getTransactionReceipt("0x0e8097df0eaaffcf93b778f0a79df605dd6e7813bdb596cbad0bb8bc8ecab164")
    # tx_receipt = Libraries.nodes.Instance_Web3.eth.getTransactionReceipt("0x0e8097df0eaaffcf93b778f0a79df605dd6e7813bdb596cbad0bb8bc8ecab164")     # Gave me shit
    # tx_receipt = Libraries.nodes.Instance_Web3.eth.getTransactionReceipt("0x0e8097df0eaaffcf93b778f0a79df605dd6e7813bdb596cbad0bb8bc8ecab164")       # Gave me shit
    # PrintAndLog("tx_receipt = " + str(tx_receipt))

    # PrintAndLog("Ether_Decimals = " + str(Libraries.core.Ether_Decimals))
    # PrintAndLog("1e+18 = " + str(int(1e+18)))

    # orderType_DX = "buy"
    # tx_receipt = Libraries.core.API_GetTransactionReceipt("0xfc0485a70ed3fed766c431b9a16efc2101cbc8f7ca20a224d87d40766b05a20b")  # Bancor
    # tx_receipt = Libraries.core.API_GetTransactionReceipt("0x775f721a7199167b32eb9aaf83efada2914a6e920b2ffe293c07fa1992d4ef39")    # Kyber
    # sum = Libraries.core.GetSumOfTokensTransferredInTransaction(tx_receipt, orderType_DX, MarketDict['powr-eth'].account.publicAddress, MarketDict['powr-eth'].erc20TokenContractAddress, MarketDict['powr-eth'].decimals)

    # orderType_DX = "sell"
    # tx_receipt = Libraries.core.API_GetTransactionReceipt("0xb4eb4a3cda70ac2248773c3a248e3a34aefda3aa269c50f9f6958a49ae7e3a0f")    # Bancor

    # PrintAndLog("tx_receipt = " + str(tx_receipt))

    # sumOfTokensInTrade_etherUnits = 0
    #
    # marketAddress = "0xe4c73387041da5d36cdD11Ebc119478FB399E1De"
    # tokenAddress = "0x595832f8fc6bf59c85c527fec3740a1b7a361269"
    # decimals = MarketDict["powr-eth"].decimals
    # for log in tx_receipt['logs']:
    #     if Libraries.core.IsBuy(orderType_DX):
    #         # Match the token address, and the Transfer function topic hash, and my address as the receiver of the transfer
    #         if log['address'].lower() == tokenAddress.lower() and \
    #                         log['topics'][0].lower() == Libraries.core.FunctionTopic_ERC20_Transfer.lower() and \
    #                         Libraries.core.Remove0XfromHexString(marketAddress.lower()) in log['topics'][2].lower():
    #             actualAmountOfTokensInTrade_weiUnits = Libraries.core.ConvertHexToInt(log['data'])
    #             actualAmountOfTokensInTrade_etherUnits = Libraries.core.ConvertWeiToEther(actualAmountOfTokensInTrade_weiUnits, decimals)
    #             sumOfTokensInTrade_etherUnits += actualAmountOfTokensInTrade_etherUnits
    #             PrintAndLog("Found it! Buying tokens on DX actualAmountOfTokensInTrade_etherUnits = " + str(actualAmountOfTokensInTrade_etherUnits))
    #
    #     elif Libraries.core.IsSell(orderType_DX):
    #         # Match the token address, and the Transfer function topic hash, and my address as the sender of the transfer
    #         if log['address'].lower() == tokenAddress.lower() and \
    #                         log['topics'][0].lower() == Libraries.core.FunctionTopic_ERC20_Transfer.lower() and \
    #                         Libraries.core.Remove0XfromHexString(marketAddress.lower()) in log['topics'][1].lower():
    #             actualAmountOfTokensInTrade_weiUnits = Libraries.core.ConvertHexToInt(log['data'])
    #             actualAmountOfTokensInTrade_etherUnits = Libraries.core.ConvertWeiToEther(actualAmountOfTokensInTrade_weiUnits, decimals)
    #             sumOfTokensInTrade_etherUnits += actualAmountOfTokensInTrade_etherUnits
    #             PrintAndLog("Found it! Selling tokens on DX actualAmountOfTokensInTrade_etherUnits = " + str(actualAmountOfTokensInTrade_etherUnits))
    #
    # PrintAndLog("sumOfTokensInTrade_etherUnits = " + str(sumOfTokensInTrade_etherUnits))

elif sys.argv and len(sys.argv) >= 2 and "testnode_sentinel" == sys.argv[1].lower():
    PrintAndLog("Not entering main loop, do some functional testing here!")

    while True:
        t = threading.Thread(target=Libraries.core.Sentinel_ConsiderCheckingOnAllRemoteNodes, args=())
        t.start()

        time.sleep(Libraries.core.Interval_seconds_SentinelLoopSleepTime)

elif sys.argv and len(sys.argv) >= 2 and "testnode_joeyz123" == sys.argv[1].lower():
    PrintAndLog("Not entering main loop, do some functional testing here!")

    while True:
        for remoteNode in Libraries.nodes.RemoteNodeList:
            url = remoteNode.value
            t = threading.Thread(target=Libraries.core.API_Sentinel_GetLatestBlockNumber, args=(url,))
            t.start()

        time.sleep(0.2)

elif sys.argv and len(sys.argv) >= 2 and "testnode_websockets" == sys.argv[1].lower():
    # WebSocketsClient = Exchanges.ercdex.Thread_WebSocketsClient()
    Libraries.node.ConnectWebSocketClient()

    time.sleep(Libraries.core.SleepTime_WaitingForWebsocketsToConnect_seconds)

    while True:
        time.sleep(0.5)
        # time.sleep(1)
        Libraries.node.Datetime_Before = datetime.datetime.now()
        # Libraries.node.WebsocketClient.ws.send('{"jsonrpc":"2.0","method":"eth_getFilterChanges","params":["' + Libraries.node.Filter_eth_newBlockFilter + '"],"id":74}')
        Libraries.node.WebsocketClient.ws.send('{"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":75}')

elif sys.argv and len(sys.argv) >= 2 and "test0xpython" == sys.argv[1].lower():
    makerAddress = 'TODO'
    privateKey = 'TODO'

    orderData_test = {
        'makerAddress': makerAddress,
        'takerAddress': "0x0000000000000000000000000000000000000000",
        'feeRecipientAddress': "0x0000000000000000000000000000000000000000",
        'senderAddress': "0x0000000000000000000000000000000000000000",
        'makerAssetAmount': "1000000000000000000",
        'takerAssetAmount': "1000000000000000000",
        'makerFee': "0",
        'takerFee': "0",
        'expirationTimeSeconds': "12345",
        'salt': "12345",
        # 'makerAssetData': "0x0000000000000000000000000000000000000000",
        # 'takerAssetData': "0x0000000000000000000000000000000000000000",
        'makerAssetData': Exchanges.zrxV2.EncodeERC20AssetData("0x0000000000000000000000000000000000000000"),
        'takerAssetData': Exchanges.zrxV2.EncodeERC20AssetData("0x0000000000000000000000000000000000000000"),
        'exchangeAddress': "0x0000000000000000000000000000000000000000",
    }

    orderHash = generate_order_hash_hex(orderData_test)
    PrintAndLog("orderHash = " + str(orderHash))
    orderHash_bytesArray = bytes.fromhex(orderHash)
    PrintAndLog("hashBytesDigest converted of type " + str(type(orderHash_bytesArray)) + " = " + str(orderHash_bytesArray))
    v, r, s = Libraries.signingUtils.SignHash(orderHash_bytesArray, privateKey)
    PrintAndLog("v = " + str(v))
    PrintAndLog("r = " + str(r))
    PrintAndLog("s = " + str(s))

    expectedSig = "0x1c6439f8515057277de3221aac900b94c964ecd18fecacb38492d623204f3ff65f0ffeb4f5b9bae527b3a33f9019a58adf1b6b302d8634ca36a7fd45964d99bf3103"
    PrintAndLog("expectedSig        = " + str(expectedSig))

    pythonGeneratedSig = Libraries.signingUtils.GetSignatureStringFromVRS(v, r, s)
    pythonGeneratedSig = Exchanges.zrxV2.AppendSignatureTypeToSignatureString(pythonGeneratedSig, Exchanges.zrxV2.SignatureType.EthSign)
    PrintAndLog("pythonGeneratedSig = " + str(pythonGeneratedSig))

elif sys.argv and len(sys.argv) >= 2 and "testuniswap" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.cache.GetAndCache_Uniswap_API_GetExchangeDataDict()

elif sys.argv and len(sys.argv) >= 2 and "testuniswap_getpricesmath" == sys.argv[1].lower():
    # price = Exchanges.uniswap.CalculatePriceGivenBalances("sell", 0.43399566, 17.495345641178335, 301292.5)
    # PrintAndLog("Uniswap FTX price just before trade = " + str(price))
    # # price = Exchanges.uniswap.CalculatePriceGivenBalances("sell", 0.43399566, 17.045111669633602, 309274)
    # price = Exchanges.uniswap.CalculatePriceGivenBalances("sell", 0.43399566, 17.061349981178335, 309274)
    # PrintAndLog("If someone else makes this trade, price becomes about  =  " + str(price))

    # So at the time of trade uniswap SNT had 88.049 ETH and 1095571.96 SNT

    price = Exchanges.uniswap.CalculatePriceGivenBalances("buy", 5, 19.3, 441.1)

    PrintAndLog("uniswap price  =  " + str(price))
    PrintAndLog("sample deadline = " + str(int(time.time()) + 10000))

    # price = Exchanges.uniswap.CalculatePriceGivenBalances("sell", 395.416, 6418.381182346756, 1113288.068217)
    # PrintAndLog("uniswap price  =  " + str(price))
    # #6418.381182346756 ETH, 1113288.068217 tokens, priceAtThisTime = 0.005747939999042969

    # balanceEther_UniswapContract_quoteToken_etherUnits, balanceTokens_UniswapContract_quoteToken_etherUnits = \
    #     Exchanges.uniswap.API_GetExchangeBalances('0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'.lower())
    #
    # balanceEther_UniswapContract_baseToken_etherUnits, balanceTokens_UniswapContract_baseToken_etherUnits = \
    #     Exchanges.uniswap.API_GetExchangeBalances('0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48'.lower())

    # balanceEther_UniswapContract_quoteToken_etherUnits = 2940.773076043187308299
    # balanceTokens_UniswapContract_quoteToken_etherUnits = 61.73526528
    # balanceEther_UniswapContract_baseToken_etherUnits = 6263.413808879359375946
    # balanceTokens_UniswapContract_baseToken_etherUnits = 1271918.597411

    # side = 'buy'
    # sourceTokenQuantity_etherUnits = 0.0001
    # # side = 'sell'
    # # sourceTokenQuantity_etherUnits = 1
    #
    # price_quoteTokensWithRespectToBaseTokens = Exchanges.uniswap.CalculatePriceGivenBalances_TokenForToken(
    #     side, sourceTokenQuantity_etherUnits,
    #     balanceEther_UniswapContract_quoteToken_etherUnits, balanceTokens_UniswapContract_quoteToken_etherUnits,
    #     balanceEther_UniswapContract_baseToken_etherUnits, balanceTokens_UniswapContract_baseToken_etherUnits)
    # PrintAndLog("price_quoteTokensWithRespectToBaseTokens = " + str(price_quoteTokensWithRespectToBaseTokens))

elif sys.argv and len(sys.argv) >= 2 and "testuniswap_getprices" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # Set Uniswap's ContractDict_Exchange based on blockchain discovery
    # Libraries.cache.GetAndCache_Uniswap_API_GetExchangeDataDict() # I don't need to call this every time here, just refer to the already cached data
    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    tokenAddress = '0x0000000000b3f879cb30fe243b4dfee438691c04'
    balance_ether, balance_tokens = Exchanges.uniswap.API_GetExchangeBalances(tokenAddress)

    price_buy = Exchanges.uniswap.CalculatePriceGivenBalances(
        'buy', 0.1, balance_ether, balance_tokens)
    price_sell = Exchanges.uniswap.CalculatePriceGivenBalances(
        'sell', 0.1, balance_ether, balance_tokens)
    PrintAndLog("Uniswap price for " + str(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)) + " price_buy = " + str(price_buy))
    PrintAndLog("Uniswap price for " + str(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)) + " price_sell = " + str(price_sell))
    sys.exit()

    # (Thread-441) Setting effectiveEther = 44.179011761531, based on exchange_Uniswap.etherBalance = 73.29229188363432 and exchangeCX.etherBalance = 207.99692656
    # (Thread-441) Setting effectiveTokens = 41510.0, based on exchange_Uniswap.tokenBalance = 41510.0 and exchangeCX.tokenBalance = 41510.34650352
    # (Thread-441) API_GetManyEthToTokenInputPrices: kwargs = {'uniswapContract': '0x2E642b8D59B45a1D8c5aEf716A84FF44ea665914', 'eth_sold_array': [43295431526300385280, 38877530350147280896, 34459629173994180608, 30041727997841084416, 25623826821687975936, 21205925645534879744, 16788024469381781504, 12370123293228681216, 7952222117075579904, 3534320940922480128, 1767160470461240064]}

    # etherList_etherUnits = [1, 2, 3]
    # # Does not work with USDC
    # # Specify how many Ether I want to spend on tokens
    # Libraries.arbyUtility.API_GetManyEthToTokenInputPrices(market.erc20TokenContractAddress, etherList_etherUnits, market.decimals)
    #
    # etherList_etherUnits = [1, 2, 3]
    # # Returns weird data with USDC
    # # Specify how many Ether I want to receive
    # Libraries.arbyUtility.API_GetManyTokenToEthOutputPrices(market.erc20TokenContractAddress, etherList_etherUnits, market.decimals)

    # tokensList_etherUnits = [100, 200, 300]
    # # Works with USDC
    # # Specify how many tokens I want to buy
    # Libraries.arbyUtility.API_GetManyEthToTokenOutputPrices(market.erc20TokenContractAddress, tokensList_etherUnits, market.decimals)
    #
    # tokensList_etherUnits = [100, 200, 300]
    # # Works with USDC
    # # Specify how many tokens I want to spend
    # Libraries.arbyUtility.API_GetManyTokenToEthInputPrices(market.erc20TokenContractAddress, tokensList_etherUnits, market.decimals)

    # Exchanges.uniswap.API_GetEthToTokenInputPrice(market.erc20TokenContractAddress)

    addressList = [
        Exchanges.uniswap.ContractDict_Exchange[MarketDict["usdc-eth"].erc20TokenContractAddress.lower()]['exchange'],
        Exchanges.uniswap.ContractDict_Exchange[MarketDict["bat-eth"].erc20TokenContractAddress.lower()]['exchange']]

    tokenAddressList = [
        MarketDict["usdc-eth"].erc20TokenContractAddress,
        MarketDict["bat-eth"].erc20TokenContractAddress]

    decimalsList = [
        MarketDict["usdc-eth"].decimals,
        MarketDict["bat-eth"].decimals]

    resultList = Libraries.core.API_GetEtherBalance_Batched_Safe(addressList)
    PrintAndLog("API_GetEtherBalance_Batched_Safe resultList = " + str(resultList))

    resultList = Libraries.core.API_GetTokenBalance_Batched(addressList, tokenAddressList, decimalsList)
    PrintAndLog("API_GetTokenBalance_Batched resultList = " + str(resultList))

    side = "buy"
    # side = "sell"

    uniswapContractList = [
        Exchanges.uniswap.ContractDict_Exchange[MarketDict["usdc-eth"].erc20TokenContractAddress.lower()]['exchange'],
        Exchanges.uniswap.ContractDict_Exchange[MarketDict["bat-eth"].erc20TokenContractAddress.lower()]['exchange']
    ]

    balanceEtherList_UniswapContract = [
        Libraries.core.API_GetEtherBalance(Exchanges.uniswap.ContractDict_Exchange[MarketDict["usdc-eth"].erc20TokenContractAddress.lower()]['exchange']),
        Libraries.core.API_GetEtherBalance(Exchanges.uniswap.ContractDict_Exchange[MarketDict["bat-eth"].erc20TokenContractAddress.lower()]['exchange'])]

    balanceTokensList_UniswapContract = [
        Libraries.core.API_GetTokenBalance(Exchanges.uniswap.ContractDict_Exchange[MarketDict["usdc-eth"].erc20TokenContractAddress.lower()]['exchange'],
                                           MarketDict["usdc-eth"].erc20TokenContractAddress, MarketDict["usdc-eth"].decimals),
        Libraries.core.API_GetTokenBalance(Exchanges.uniswap.ContractDict_Exchange[MarketDict["bat-eth"].erc20TokenContractAddress.lower()]['exchange'],
                                           MarketDict["bat-eth"].erc20TokenContractAddress, MarketDict["bat-eth"].decimals)]

    sourceTokenQuantityListofLists_etherUnits = [
        [1, 5, 20],
        [1, 5, 20]]

    returnPriceDict = Exchanges.uniswap.API_GetManyTradePrices(
        side, uniswapContractList, sourceTokenQuantityListofLists_etherUnits, balanceEtherList_UniswapContract, balanceTokensList_UniswapContract)

    PrintAndLog("returnPriceDict = " + str(returnPriceDict))

elif sys.argv and len(sys.argv) >= 2 and "testcoingecko" == sys.argv[1].lower():
    jData = Libraries.coinGecko.API_GetMarkets_Safe("usd", 50)
    PrintAndLog("jData = " + str(jData))

    marketName = "wbtc-eth"
    UpdateCoinMarketCapMarkets(1000, "id")
    coinMarketCapId = ''
    currentMarketPrice_withRespectToETH = CoinMarketCapMarketsDict[coinMarketCapId].GetPriceEth(coinMarketCapId, CoinMarketCapMarketsDict)
    PrintAndLog("currentMarketPrice_withRespectToETH = " + str(currentMarketPrice_withRespectToETH))

    # jData = Libraries.coinGecko.API_GetHistory("ethereum", "29", "01", "2018")
    # priceAtThatTime = jData['market_data']['current_price']['usd']
    # PrintAndLog("priceAtThatTime = " + str(priceAtThatTime))

    # datetime_begin = datetime.datetime(2018, 11, 1)
    # datetime_end = datetime.datetime(2018, 12, 1)
    # resultDict = Libraries.coinGecko.API_GetPriceDuringRangeInHistory_GivenDateTimeRange(datetime_begin, datetime_end, "ethereum", 'usd')
    # PrintAndLog("resultDict = " + str(resultDict))

elif sys.argv and len(sys.argv) >= 2 and "testcryptocompare" == sys.argv[1].lower():
    # jData = Libraries.cryptoCompare.API_GetMaxHistory("bat", "usd")
    # PrintAndLog("jData = " + str(jData))

    datetime_end = datetime.datetime(2018, 12, 1)
    resultDict = Libraries.cryptoCompare.API_GetPriceDuringRangeInHistory_GivenDateTimeFromHistory(datetime_end, "bat", "usd")

    PrintAndLog("resultDict = " + str(resultDict))

elif sys.argv and len(sys.argv) >= 2 and "testgetalladdressesfromfile" == sys.argv[1].lower():
    file = open("testFileContainingAddresses.txt", "r")
    data = file.read()
    file.close()

    addressList = []
    splitString = data.split()
    for item in splitString:
        if item.startswith("0x") and len(item) == Libraries.core.LengthOfPublicAddress_Including0x:
            address = item.lower()
            if address not in addressList:
                # PrintAndLog("Found address = " + address)
                addressList.append(address)

    PrintAndLog("addressList = " + str(addressList))

elif sys.argv and len(sys.argv) >= 2 and "fixchrissnapshoterror" == sys.argv[1].lower():
    Directory_Snapshots_FixedChrisWithdrawBug = "Snapshots_FixedChrisWithdrawBug"

    for file in os.listdir(Libraries.defaults.Directory_Snapshots):
        PrintAndLog("file = " + file)
        splitString = file.split("_")

        if len(splitString) >= 2 and splitString[0].lower() == "snapshot":
            # PrintAndLog("splitString = " + str(splitString))
            snapshotNumber = int(splitString[1])
            if snapshotNumber >= 62 and snapshotNumber <= 303:
                PrintAndLog("Eligible for the fix, file = " + str(file))
                snapshotName = None
                if len(splitString) > 2:
                    snapshotName = splitString[2]

                # PrintAndLog("Found snapshot " + str(snapshotNumber) + " " + str(snapshotName))

                # Read the snapshot data
                file = open(Libraries.defaults.Directory_Snapshots + "/" + file, "r")
                snapshotData = file.read()
                file.close()

                # PrintAndLog("snapshotData = " + str(snapshotData))

                # Find these two lines and add Chris' extra refuned amount to them
                # ETH Balances, All Accounts: 1867.92 ETH      || Bnc: 233.7 (0 pend)  ETH      || Btx: 23.4 (0 + 0.0 pend)  ETH      || ED: 113.6 ETH      || 0x: 466.1 ETH      || Wal: 1031.2 ETH.
                # ZRX-ETH  || Wal, 55.7 ETH,

                lines = snapshotData.splitlines()
                foundEthBalances = False
                # foundZrxBalances = False
                string_ethBalance = "ETH Balances, All Accounts:"
                # string_zrxBalance1 = "ZRX-ETH"
                # string_zrxBalance2 = "Wal, "
                lines_toSave = []
                indexToInjectNewStuff = None
                for index, line in enumerate(lines):
                    # PrintAndLog("analyzing line = " + str(line))

                    if string_ethBalance not in line:
                        # Save the old line
                        lines_toSave.append(line)

                    else:
                        foundEthBalances = True
                        # PrintAndLog("Found 1st part! line = " + str(line))
                        beginIndex = line.index(string_ethBalance)
                        tempSplit = line.split(string_ethBalance)[1]
                        beginNumberIndex = beginIndex + len(string_ethBalance)
                        endNumberIndex = tempSplit.index("ETH") + len(string_ethBalance) + beginIndex
                        foundEthBalances = float(line[beginNumberIndex:endNumberIndex])

                        # PrintAndLog("beginIndex = " + str(beginIndex))
                        # PrintAndLog("beginNumberIndex = " + str(beginNumberIndex))
                        # PrintAndLog("tempSplit = " + str(tempSplit))
                        # PrintAndLog("endNumberIndex = " + str(endNumberIndex))
                        PrintAndLog("foundEthBalances = " + str(foundEthBalances))

                        # Update the data with our new value with the fix

                        foundEthBalances += 9.82651132
                        foundEthBalances = round(foundEthBalances, 1)
                        PrintAndLog("Updating ETH balance to " + str(foundEthBalances))
                        PrintAndLog("oldline = " + str(line))
                        newline = line[:beginNumberIndex] + "  " + str(foundEthBalances) + " " + line[endNumberIndex:len(line)]

                        PrintAndLog("newline = " + str(newline))
                        # Save the newline
                        lines_toSave.append(newline)
                        # Inject it one line before this one
                        indexToInjectNewStuff = index - 1

                if not foundEthBalances:
                    PrintAndLog("foundEthBalances = " + str(foundEthBalances))
                    # PrintAndLog("foundZrxBalances = " + str(foundZrxBalances))
                    raise Exception("Did not find all the data we needed for snapshot number " + str(snapshotNumber))
                else:
                    # PrintAndLog("Snapshot number: " + str(snapshotNumber) + ", foundZrxBalances = " + str(foundZrxBalances) + ", foundEthBalances = " + str(foundEthBalances))
                    PrintAndLog("Snapshot number: " + str(snapshotNumber) + ", foundEthBalances = " + str(foundEthBalances))

                # PrintAndLog("lines = " + str(len(lines)))
                # PrintAndLog("lines_toSave = " + str(len(lines_toSave)))

                # PrintAndLog("lines_toSave = " + str(lines_toSave))

                snapshotData_new = ""
                for index, line in enumerate(lines_toSave):
                    snapshotData_new += line + "\n"

                    if index == indexToInjectNewStuff:
                        line1 = '2019-03-09 00:01:01.000000--------------------- Begin Chris Returned ETH ---------------------------------------------------------------'
                        line2 = 'ETH      || Wal, 9.82651132 ETH'
                        line3 = '2019-03-09 00:01:01.000000--------------------- End Chris Returned ETH ---------------------------------------------------------------'
                        snapshotData_new += line1 + "\n"
                        snapshotData_new += line2 + "\n"
                        snapshotData_new += line3 + "\n"

                # PrintAndLog("snapshotData_new = " + str(snapshotData_new))

                tempSplit = file.name.split('/')
                PrintAndLog("tempSplit = " + str(tempSplit))
                indexToUse = len(tempSplit) - 1
                PrintAndLog("indexToUse = " + str(indexToUse))

                # PrintAndLog("Directory_Snapshots_FixedChrisWithdrawBug = " + str(Directory_Snapshots_FixedChrisWithdrawBug))
                # PrintAndLog("Directory_Snapshots_FixedChrisWithdrawBug = " + str(type(Directory_Snapshots_FixedChrisWithdrawBug)))
                # PrintAndLog("file = " + str(file.name))
                # PrintAndLog("file = " + str(type(file.name)))

                filepath_new = os.path.join(Directory_Snapshots_FixedChrisWithdrawBug, tempSplit[indexToUse])
                PrintAndLog("filepath_new = " + str(filepath_new))

                file = open(filepath_new, "w")
                file.write(snapshotData_new)
                file.close()

            else:
                PrintAndLog("NOT Eligible for the fix, file = " + str(file))

elif sys.argv and len(sys.argv) >= 2 and "testuniswap_getexchangebalances" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    erc20TokenContractAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # specifiedBlockNumber_int = None
    specifiedBlockNumber_int = 9371060

    balance_ether, balance_tokens = Exchanges.uniswap.API_GetExchangeBalances(erc20TokenContractAddress, specifiedBlockNumber_int)
    PrintAndLog("balance_ether = " + str(balance_ether) + " ETH")
    PrintAndLog("balance_tokens = " + str(balance_tokens))

    # Make call over range of blocks
    while specifiedBlockNumber_int < 9371100:
        specifiedBlockNumber_int += 1
        balance_ether, balance_tokens = Exchanges.uniswap.API_GetExchangeBalances(erc20TokenContractAddress, specifiedBlockNumber_int)
        priceAtThisTime = Exchanges.uniswap.CalculatePriceGivenBalances('sell', 395.416, balance_ether, balance_tokens)
        PrintAndLog("block " + str(specifiedBlockNumber_int) + ", " + str(balance_ether) + " ETH, " + str(balance_tokens) + " tokens, priceAtThisTime = " + str(priceAtThisTime))

elif sys.argv and len(sys.argv) >= 2 and "testuniswapv2_factory" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.cache.GetAndCache_UniswapV2_API_GetExchangeDataDict()

elif sys.argv and len(sys.argv) >= 2 and "testsushiswap_factory" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.cache.GetAndCache_Sushiswap_API_GetExchangeDataDict()

elif sys.argv and len(sys.argv) >= 2 and "testdefiswap_factory" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.cache.GetAndCache_Defiswap_API_GetExchangeDataDict()

elif sys.argv and len(sys.argv) >= 2 and "testsakeswap_factory" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.cache.GetAndCache_Sakeswap_API_GetExchangeDataDict()

elif sys.argv and len(sys.argv) >= 2 and "testbalancer_factory" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.cache.GetAndCache_Balancer_API_GetExchangeDataDict()

elif sys.argv and len(sys.argv) >= 2 and "testset" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()
    setKey = 'eth20smaco'
    state = Exchanges.set.API_GetRebalanceState(Exchanges.set.SetTokenDataDict[setKey].setAddress)
    PrintAndLog("state = " + str(state))

elif sys.argv and len(sys.argv) >= 2 and "testset_get_all_sets" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    # requireThisSymbol = None
    requireThisSymbol = 'wbtc'

    requireThisSymbolIsNotIncluded = None
    requireThisSymbolIsNotIncluded = 'weth'

    sum_marketCap = 0
    PrintAndLog("Exchanges.set.SetTokenDataDict = " + str(Exchanges.set.SetTokenDataDict))
    for setKey in Exchanges.set.SetTokenDataDict:
        setTokenData = Exchanges.set.SetTokenDataDict[setKey]

        if requireThisSymbol and requireThisSymbol.lower() not in setTokenData.GetPrintFriendlyComponents().lower():
            continue

        if requireThisSymbolIsNotIncluded and requireThisSymbolIsNotIncluded.lower() in setTokenData.GetPrintFriendlyComponents().lower():
            continue

        PrintAndLog("https://www.tokensets.com/set/" + str(setKey) + "   , marketCap = " + str(setTokenData.marketCap))
        PrintAndLog("   components = " + str(setTokenData.GetPrintFriendlyComponents()))
        PrintAndLog("   strategy = " + str(setTokenData.strategy))
        PrintAndLog("   setAddress = " + str(setTokenData.setAddress))
        sum_marketCap += setTokenData.marketCap

    PrintAndLog("sum_marketCap = " + str(sum_marketCap))

elif sys.argv and len(sys.argv) >= 2 and "testset_get_sets_for_specific_ninja" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
    # NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    PrintAndLog("Exchanges.set.SetTokenDataDict = " + str(Exchanges.set.SetTokenDataDict))
    for setKey in Exchanges.set.SetTokenDataDict:
        setTokenData = Exchanges.set.SetTokenDataDict[setKey]
        if not Exchanges.set.CanNinjaObjectTradeOnThisSetTokenData(NinjaObject, setTokenData):
            continue

        PrintAndLog(str(setKey) + ", marketCap = " + str(setTokenData.marketCap))
        PrintAndLog("   components = " + str(setTokenData.GetPrintFriendlyComponents()))
        PrintAndLog("   strategy = " + str(setTokenData.strategy))
        PrintAndLog("   setAddress = " + str(setTokenData.setAddress))

elif sys.argv and len(sys.argv) >= 2 and "testset_getproperties" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    setKey = 'btceth7525-3'
    proposalPeriod_seconds = Exchanges.set.API_GetProposalPeriod(Exchanges.set.SetTokenDataDict[setKey].setAddress)
    proposalStartTime_seconds = Exchanges.set.API_GetProposalStartTime(Exchanges.set.SetTokenDataDict[setKey].setAddress)
    PrintAndLog("proposalPeriod_seconds = " + str(proposalPeriod_seconds) + " seconds, or " + str(round(proposalPeriod_seconds / float(60), 1)) + " minutes")
    PrintAndLog("proposalStartTime_seconds = " + str(proposalStartTime_seconds))
    currentTime_epoch_seconds = int(time.time())
    PrintAndLog("currentTime_epoch_seconds = " + str(currentTime_epoch_seconds))

    timeRemainingInProposalPeriod = None
    proposalPeriodEndTime = proposalStartTime_seconds + proposalPeriod_seconds
    PrintAndLog("proposalPeriodEndTime = " + str(proposalPeriodEndTime))
    if currentTime_epoch_seconds >= proposalPeriodEndTime:
        PrintAndLog(setKey + "'s rebalance proposal period has expired!")
    else:
        timeLeft_seconds = proposalPeriodEndTime - currentTime_epoch_seconds
        PrintAndLog(setKey + "'s rebalance proposal period is still in progress. You cannot call startRebalance until "
                             "this period has finished. Time left = " + str(timeLeft_seconds) + " seconds, or " + str(
            round(timeLeft_seconds / float(60), 1)) + " minutes")

elif sys.argv and len(sys.argv) >= 2 and "testset_check_rebalance_criteria" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    # Set one of these to true at a time, this will send the call to begin the rebalance
    doSend_initialPropose = False
    doSend_propose = False
    doSend_confirmPropose = False
    doSend_startRebalance = False
    doSend_settleRebalance = False
    doSend_socialUpdateAllocation = True

    # newAllocationPercentage = 1.0
    # newAllocationPercentage = 0.79
    newAllocationPercentage = 0.0

    # Use this when you want to run all sets, but I should never ever want to do this...
    # specifiedKeyOnly = None
    # Use this when you want to run a specified set only
    # specifiedKeyOnly = 'joeyethdai'  # Next newAllocationPercentage is 1.0
    specifiedKeyOnly = 'jethcusdc'  # Next newAllocationPercentage is 0.0
    # specifiedKeyOnly = 'eth20smaco'  # Next newAllocationPercentage is 1.0
    # specifiedKeyOnly = 'rbtccusdc'  # Next newAllocationPercentage is 0.0
    # specifiedKeyOnly = 'wbtcusdcv5'  # Next newAllocationPercentage is 0.0
    # specifiedKeyOnly = 'jlinkusdc'  # Next newAllocationPercentage is 1.0

    myOwnedSetsList = [
        'joeyethdai',
        'wbtcusdcv5',
        'jethcusdc',
        'rbtccusdc',
        'jlinkusdc',
    ]

    # NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
    # NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()

    # TODO, I need to re-encrypt this and add it back
    publicAddress_SetSocialManager = SetSocialTestAccount.publicAddress
    privateKey_SetSocialManager = SetSocialTestAccount.DecryptPK()

    gas = Libraries.gasStation.GetGasLimit_Set_StartRebalance()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    nonce = None

    jData_rebalancingSets = Exchanges.set.API_GetRebalancingSets()
    # PrintAndLog("len(jData_rebalancingSets) = " + str(len(jData_rebalancingSets)))

    listOfSetKeysNotFoundInSetTokenDataDict = []

    for set in jData_rebalancingSets:
        try:
            if specifiedKeyOnly:
                setKey = specifiedKeyOnly
            else:
                setKey = set['id']

            setTokenData = Exchanges.set.SetTokenDataDict[setKey]

            # Don't spam the screen if the key is not in the dict because I probably have some debug settings enabled to focus on one key at a time...
            # if not, however, that is a problem and a bug
            if setKey not in Exchanges.set.SetTokenDataDict:
                if setKey not in listOfSetKeysNotFoundInSetTokenDataDict:
                    listOfSetKeysNotFoundInSetTokenDataDict.append(setKey)
                continue

            PrintAndLog("------------------------------------------------------------------------------------------------")

            PrintAndLog(setKey + ", marketCap = " + str(setTokenData.marketCap) + ", components = " + str(
                setTokenData.GetPrintFriendlyComponents()))

            # if not Exchanges.set.CanNinjaObjectTradeOnThisSetTokenData(NinjaObject, setTokenData):
            #     PrintAndLog("CanNinjaObjectTradeOnThisSetTokenData returned False for " + str(setKey) + " continuing through this loop")
            #     PrintAndLog("------------------------------------------------------------------------------------------------")
            #     continue

            status, rebalanceCriteria, components = Exchanges.set.API_GetRebalancingSetState_Custom(setKey, jData_rebalancingSets)

            PrintAndLog("STATUS = " + str(status))
            PrintAndLog("rebalanceCriteria = " + str(rebalanceCriteria))
            PrintAndLog("components = " + str(components))

            if setTokenData.strategy == Exchanges.set.TradingStrategy.UserDefined:
                PrintAndLog(str(setKey) + " is " + str(Exchanges.set.TradingStrategy.UserDefined) + " and I have not yet handled parsing the API for this strategy's states")

                if status.lower() == "rebalance":
                    PrintAndLog(str(setKey) + " is in range. Rebalance mode in fact. I can trade against it right now!")
                else:
                    PrintAndLog(str(setKey) + "'s owner can issue a rebalance at any moment")

                    # If i'm in control of this, allow me to call API_UpdateAllocation to start the rebalance
                    if setKey in myOwnedSetsList:
                        if doSend_socialUpdateAllocation:
                            transactionId = Exchanges.set.API_UpdateAllocation(publicAddress_SetSocialManager, privateKey_SetSocialManager, setTokenData.managerAddress,
                                                                               setTokenData.setAddress, newAllocationPercentage, gasPrice, nonce)

                            PrintAndLog("transactionId = " + str(transactionId))
                    else:
                        PrintAndLog("I am not in control of this set " + str(setKey) + ". If this is a mistake, then add it to the list above (myOwnedSetsList)")

                if doSend_settleRebalance:
                    transactionId = Exchanges.set.API_SettleRebalance(publicAddress_Ninja, privateKey_Ninja, setTokenData.setAddress, gas, gasPrice)

            else:
                rebalanceValue = rebalanceCriteria['rebalance']['value']
                PrintAndLog("rebalanceValue = " + str(rebalanceValue))
                if rebalanceValue.lower() == "yes" and status.lower() == "default":
                    if Exchanges.set.DoesSetStrategyUseInitialProposeAndConfirmProposal(setTokenData.strategy):
                        PrintAndLog(str(setKey) + " is in range. Send initialPropose()")
                    else:
                        PrintAndLog(str(setKey) + " is in range. Send propose()")
                elif rebalanceValue.lower() == "yes" and status.lower() == "proposal":
                    PrintAndLog(str(setKey) + " is in range. Send startRebalance()")
                elif status.lower() == "rebalance":
                    PrintAndLog(str(setKey) + " is in range. Rebalance mode in fact. I can trade against it right now!")
                elif status.lower() == "initial_confirmation":
                    PrintAndLog(str(setKey) + " is in range. Send confirmPropose()")
                else:
                    PrintAndLog(str(setKey) + " is not in range, not ready for init proposal, nor rebalance")

                transactionId = None
                if doSend_initialPropose:
                    transactionId = Exchanges.set.API_InitialPropose(publicAddress_Ninja, privateKey_Ninja, setTokenData.managerAddress, gasPrice)
                elif doSend_propose:
                    transactionId = Exchanges.set.API_Propose(publicAddress_Ninja, privateKey_Ninja, setTokenData.managerAddress,
                                                              setTokenData.setAddress, gasPrice)
                elif doSend_confirmPropose:
                    transactionId = Exchanges.set.API_ConfirmPropose(publicAddress_Ninja, privateKey_Ninja, setTokenData.managerAddress, gasPrice)
                elif doSend_startRebalance:
                    transactionId = Exchanges.set.API_StartRebalance(publicAddress_Ninja, privateKey_Ninja, setTokenData.setAddress, gas, gasPrice)
                elif doSend_settleRebalance:
                    transactionId = Exchanges.set.API_SettleRebalance(publicAddress_Ninja, privateKey_Ninja, setTokenData.setAddress, gas, gasPrice)

                PrintAndLog("transactionId = " + str(transactionId))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            message = "Error when analyzing set data: " + traceback.format_exc()
            PrintAndLogError(message)

        if specifiedKeyOnly:
            break

        PrintAndLog("------------------------------------------------------------------------------------------------")

    PrintAndLog("listOfSetKeysNotFoundInSetTokenDataDict = " + str(listOfSetKeysNotFoundInSetTokenDataDict))

elif sys.argv and len(sys.argv) >= 2 and "testset_getmanager" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    key = ''

    managerAddress = Exchanges.set.API_GetManager(Exchanges.set.SetTokenDataDict[key].setAddress)
    PrintAndLog("managerAddress = " + str(managerAddress))

elif sys.argv and len(sys.argv) >= 2 and "testset_getbidprice" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    PrintAndLog("-------------------------------- JoeyZDEBUG --------------------------------")

    # key = 'joeyethdai'
    key = 'jethcusdc'
    # key = 'wbtcusdcv5'
    # key = 'ethemaapy'
    # key = 'rbtccusdc'
    # key = 'jlinkusdc'
    price = Exchanges.set.SetTokenDataDict[key].price
    side = Exchanges.set.SetTokenDataDict[key].side
    PrintAndLog("key = " + str(key))
    PrintAndLog("inflow token = " + str(Exchanges.set.SetTokenDataDict[key].inflowTokenAddress))
    PrintAndLog("outflow token = " + str(Exchanges.set.SetTokenDataDict[key].outflowTokenAddress))
    PrintAndLog("price = " + str(Exchanges.set.SetTokenDataDict[key].price))
    PrintAndLog("side = " + str(Exchanges.set.SetTokenDataDict[key].side))
    PrintAndLog("minBidSize_weiUnits = " + str(Exchanges.set.SetTokenDataDict[key].minBidSize_weiUnits))

    # inflowTokensToSpend_etherUnits = 0.001385
    # inflowTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei(inflowTokensToSpend_etherUnits, Exchanges.set.SetTokenDataDict[key].decimals_inflowToken)
    #
    # # PrintAndLog("inflowTokensToSpend_etherUnits = " + str(inflowTokensToSpend_etherUnits))
    # PrintAndLog("inflowTokensToSpend_weiUnits = " + str(inflowTokensToSpend_weiUnits))
    # sharesQuantityForTrade_weiUnits = Exchanges.set.SetTokenDataDict[key].GetSharesQuantityForSetTrade_GivenInflowTokenQuantityToSpend_WeiUnits(inflowTokensToSpend_weiUnits)
    # PrintAndLog("sharesQuantityForTrade_weiUnits = " + str(sharesQuantityForTrade_weiUnits))

    # 1234567899  shares should yield inflow of 0.000154 WETH
    # 1237882.417

elif sys.argv and len(sys.argv) >= 2 and "testset_getcombinedtokenarray" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()
    # setKey = 'joeyethdai'
    setKey = 'wbtcusdcv5'
    Exchanges.set.API_GetCombinedTokenArray(Exchanges.set.SetTokenDataDict[setKey].setAddress)

elif sys.argv and len(sys.argv) >= 2 and "testset_getbiddingparameters" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    # setKey = 'joeyethdai'
    # setKey = 'wbtcusdcv5'
    setKey = 'jethcusdc'
    minBidSize, remainingCurrentUnits = Exchanges.set.API_GetBiddingParameters(Exchanges.set.SetTokenDataDict[setKey].setAddress)
    PrintAndLog("remainingCurrentUnits = " + str(remainingCurrentUnits))
    remainingCurrentShares = float(remainingCurrentUnits) / float(minBidSize)
    PrintAndLog("remainingCurrentShares = " + str(remainingCurrentShares))

elif sys.argv and len(sys.argv) >= 2 and "testset_getaddressandbidpricearray" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    # setKey = 'joeyethdai'
    # setKey = 'wbtcusdcv5'
    setKey = 'jethcusdc'
    quantity = 10000000000000
    Exchanges.set.API_GetAddressAndBidPriceArray(Exchanges.set.SetTokenDataDict[setKey].setAddress, quantity)

elif sys.argv and len(sys.argv) >= 2 and "testset_trade" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # fromAddress = Exchanges.set.Account_KovanTestnet_FromAddress
    # fromPrivateKey = Exchanges.set.Account_KovanTestnet_FromPrivateKey

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    # Use ninja account
    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    fromAddress = ninjaOp.publicAddress
    fromPrivateKey = ninjaOp.DecryptPK()

    Exchanges.set.UpdateAllSetTokenData()

    doExecuteTrade = False

    # setKey = 'wbtcusdcv5'
    # setKey = 'joeyethdai'
    # setKey = 'jethcusdc'
    setKey = 'etas-1'
    # setKey = 'ethemaapy'

    # This will spend the min, plus some dust to make sure i'm over the min
    inflowTokensToSpend_etherUnits = Exchanges.set.SetTokenDataDict[setKey].minInflowTokenQuantity_etherUnits * 1.000001
    # inflowTokensToSpend_etherUnits = 500  # Half
    # inflowTokensToSpend_etherUnits = 1100  # Whole she-bang
    PrintAndLog("inflowTokensToSpend_etherUnits = " + str(inflowTokensToSpend_etherUnits))
    inflowTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei(inflowTokensToSpend_etherUnits, Exchanges.set.SetTokenDataDict[setKey].decimals_inflowToken)
    PrintAndLog("inflowTokensToSpend_weiUnits = " + str(inflowTokensToSpend_weiUnits))

    # gas = Libraries.gasStation.GetGasLimit_Trade_Set()
    dontCare, useSetCTokenBidderContract = Exchanges.set.SetTokenDataDict[setKey].GetBaseTokenAddress()

    gas = Libraries.gasStation.GetGasLimit_Arby_Set(useSetCTokenBidderContract)
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.core.ConvertGweiToWei(65.000002)

    PrintAndLog("Preparing to call Exchanges.set.Trade on setKey = " + str(setKey) + " to " + str(
        Libraries.core.InvertOrderType(Exchanges.set.SetTokenDataDict[setKey].side)) + " QuoteToken, price = " + str(
        float(Exchanges.set.SetTokenDataDict[setKey].price)) + ", inverted price = " + str(str(1 / float(Exchanges.set.SetTokenDataDict[setKey].price))))
    if doExecuteTrade:
        Exchanges.set.Trade(fromAddress, fromPrivateKey, setKey, inflowTokensToSpend_weiUnits, gas, gasPrice)
    else:
        PrintAndLog("doExecuteTrade was set to false, so not executing trade")

elif sys.argv and len(sys.argv) >= 2 and "testset_create_formula_for_converting_inflowtokens_to_shares" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
    # NinjaObject = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    PrintAndLog("-------------------------------- JoeyZDEBUG --------------------------------")

    # key = 'joeyethdai'
    key = 'jethcusdc'
    setTokenData = Exchanges.set.SetTokenDataDict[key]

    inflowTokensToSpendList_etherUnits = [
        # 6.421223, 12, 1, 0.1243, 0.879, 0.0008971598, 100, 98327498, 0.00007814589
        6.421223
    ]
    for inflowTokensToSpend_etherUnits in inflowTokensToSpendList_etherUnits:
        # inflowTokensToSpend_etherUnits = 6.421223

        inflowTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei(inflowTokensToSpend_etherUnits, Libraries.core.Ether_Decimals)
        sharesQuantity_smartContract = NinjaObject.API_ConvertInflowTokenQuantityToSharesQuantity(setTokenData, inflowTokensToSpend_weiUnits)
        PrintAndLog("inflowTokensToSpend_etherUnits = " + str(inflowTokensToSpend_etherUnits))
        PrintAndLog("inflowTokensToSpend_weiUnits = " + str(inflowTokensToSpend_weiUnits))
        PrintAndLog("sharesQuantity_smartContract = " + str(sharesQuantity_smartContract) + " using my smart contract")
        sharesQuantity_pythonCode = setTokenData.GetSharesQuantityForSetTrade_GivenInflowTokenQuantityToSpend_WeiUnits(inflowTokensToSpend_weiUnits)
        PrintAndLog("sharesQuantity_pythonCode    = " + str(sharesQuantity_pythonCode) + " using my python code")

        magicNumber = inflowTokensToSpend_weiUnits / sharesQuantity_pythonCode
        # Formula: sharesQuantity = inflowTokensToSpend_weiUnits / magicNumber_weiUnits
        PrintAndLog("magicNumber = " + str(magicNumber))
        # So this magic number is decreasing over time because the price gets better and better over time
        # So if I send in a magicNumber, that could change for the better but that would result in me being left with more dust than I thought... which isn't terrible but not ideal

elif sys.argv and len(sys.argv) >= 2 and "testset_predict_future_price" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    # Exchanges.set.UpdateAllSetTokenData()

    # key = 'joeyethdai'
    # key = 'jethcusdc'
    # key = 'wbtcusdcv5'
    key = 'ethemaapy'
    setTokenData = Exchanges.set.SetTokenDataDict[key]

    inflowToken = '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2'  # TODO, will need set
    outflowToken = '0x39aa39c021dfbae8fac545936693ac917d5e7563'  # TODO, use the token and NEVER the cToken regardless of what I'm going to trade with

    decimals_inflowToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(inflowToken)))  # TODO, will need set
    decimals_outflowToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(outflowToken)))  # TODO, will need set

    quoteTokensTradeDirection = Exchanges.set.TokenDirection.InflowToken  # TODO, will need set
    # quoteTokensTradeDirection = Exchanges.set.TokenDirection.OutflowToken

    startBlock = 9817190
    endBlock = 9817240
    resultList_price = []
    resultList_timestampUnixEpoch_seconds = []
    resultList_blockNumber = []
    for blockNumber in range(startBlock, endBlock + 1):
        blockData = Libraries.core.API_GetBlock(blockNumber)
        timestampUnixEpoch_seconds = Libraries.core.ConvertHexToInt(blockData['timestamp'])

        price, dontCare1, dontCare2, dontCare3 = Exchanges.set.API_GetBidPrice(
            setTokenData.setAddress, 12345, quoteTokensTradeDirection, decimals_outflowToken, decimals_inflowToken, blockNumber)

        PrintAndLog("At blockNumber " + str(blockNumber) + "-" + str(timestampUnixEpoch_seconds) + ", price = " + str(price))

        # If this is a new price we haven't seen before (AKA the price changed)
        if price not in resultList_price:
            resultList_price.append(price)
            resultList_timestampUnixEpoch_seconds.append(timestampUnixEpoch_seconds)
            resultList_blockNumber.append(blockNumber)

    resultList_timeSincePreviousPriceChange = []
    previousTimestamp = None
    for index, timestamp in enumerate(resultList_timestampUnixEpoch_seconds):
        if index != 0:
            resultList_timeSincePreviousPriceChange.append(timestamp - previousTimestamp)

        previousTimestamp = timestamp

    # Remove first item just to be safe since we may have started in the middle of a price occurance and not at the beginning,
    resultList_price.pop(0)
    resultList_blockNumber.pop(0)
    resultList_timestampUnixEpoch_seconds.pop(0)
    # resultList_timeSincePreviousPriceChange should have one less item than the other lists,
    # so only remove the last item from this array so no need to remove the first item from it

    PrintAndLog("Price changes over this timespan after removing the first and last values")
    for index, price in enumerate(resultList_price):
        PrintAndLog("At blockNumber " + str(resultList_blockNumber[index]) + "-" + str(resultList_timestampUnixEpoch_seconds[index]) + ", price = " + str(
            resultList_price[index]) + ", time diff between this and previous price change = " + str(
            round(resultList_timeSincePreviousPriceChange[index], 1)) + " seconds")

elif sys.argv and len(sys.argv) >= 2 and "testset_predict_future_price_2" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    # key = 'joeyethdai'
    key = 'jethcusdc'
    # key = 'wbtcusdcv5'
    # key = 'ethhivol'
    setTokenData = Exchanges.set.SetTokenDataDict[key]

    decimals_inflowToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(setTokenData.inflowTokenAddress)))  # TODO, will need set
    decimals_outflowToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(setTokenData.outflowTokenAddress)))  # TODO, will need set

    # quoteTokensTradeDirection = Exchanges.set.TokenDirection.InflowToken  # TODO, will need set
    quoteTokensTradeDirection = Exchanges.set.TokenDirection.OutflowToken

    maxBlocks = 75
    endBlock = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    startBlock = endBlock - maxBlocks

    successCount = 0
    resultList_price = []
    resultList_timestampUnixEpoch_seconds = []
    resultList_blockNumber = []
    minValidBlocksOfDataBeforeWeStartRecordingData = 10
    # Only start recording data when we get x valid blocks in a row. Let's make sure we don't start recording and then a node wipes out a block because it's old
    for blockNumber in range(startBlock, endBlock + 1):
        blockData = None
        try:
            blockData = Libraries.core.API_GetBlock(blockNumber)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception = " + traceback.format_exc())
            continue

        try:
            timestampUnixEpoch_seconds = Libraries.core.ConvertHexToInt(blockData['timestamp'])

            price, dontCare1, dontCare2, dontCare3 = Exchanges.set.API_GetBidPrice(
                setTokenData.setAddress, 12345, quoteTokensTradeDirection, decimals_outflowToken, decimals_inflowToken, blockNumber)

            # PrintAndLog("At blockNumber " + str(blockNumber) + "-" + str(timestampUnixEpoch_seconds) + ", price = " + str(price))

            # If we've made it this far, we've found a valid block of data
            # so let's compare successCount with minValidBlocksOfDataBeforeWeStartRecordingData to see if we can start recording data
            if successCount >= minValidBlocksOfDataBeforeWeStartRecordingData:
                # If this is a new price we haven't seen before (AKA the price changed)
                if price not in resultList_price:
                    resultList_price.append(price)
                    resultList_timestampUnixEpoch_seconds.append(timestampUnixEpoch_seconds)
                    resultList_blockNumber.append(blockNumber)
            else:
                pass
                # PrintAndLog("Found valid data for block " + str(blockNumber) + " but we're not using it just because we want to see a string of "
                #                                                                "valid blocks before we accept this just in case the node wipes the "
                #                                                                "next block's data out within the next second or so")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception = " + traceback.format_exc())
            continue

        # Keep track of how many calls succeeded in case for some reason I'm not getting enough
        successCount += 1

    if successCount < 35:
        message = "We didn't get very many successful blocks. Is something wrong with my nodes? successCount only = " + str(successCount)
        Libraries.core.API_PostOperatorNotification(message)
        raise Exception(message)

    resultList_timeSincePreviousPriceChange = []
    previousTimestamp = None
    for index, timestamp in enumerate(resultList_timestampUnixEpoch_seconds):
        if index != 0:
            resultList_timeSincePreviousPriceChange.append(timestamp - previousTimestamp)

        previousTimestamp = timestamp

    # We need to remove some items from the lists. We must ignore the first/second data points before we can trust the data to predict the future with it
    PrintAndLog("resultList_blockNumber before the pop = " + str(resultList_blockNumber))

    # Pop the first item in these lists
    resultList_price.pop(0)
    resultList_blockNumber.pop(0)
    resultList_timestampUnixEpoch_seconds.pop(0)

    # Pop the first item in all these lists including resultList_timeSincePreviousPriceChange
    resultList_price.pop(0)
    resultList_blockNumber.pop(0)
    resultList_timestampUnixEpoch_seconds.pop(0)
    resultList_timeSincePreviousPriceChange.pop(0)

    PrintAndLog("resultList_blockNumber after the pop = " + str(resultList_blockNumber))
    PrintAndLog("resultList_price len = " + str(len(resultList_price)))
    PrintAndLog("resultList_blockNumber len = " + str(len(resultList_blockNumber)))
    PrintAndLog("resultList_timestampUnixEpoch_seconds len = " + str(len(resultList_timestampUnixEpoch_seconds)))
    PrintAndLog("resultList_timeSincePreviousPriceChange len = " + str(len(resultList_timeSincePreviousPriceChange)))
    # resultList_timeSincePreviousPriceChange should have one less item than the other lists,
    # so only remove the last item from this array so no need to remove the first item from it

    PrintAndLog("Price changes over this timespan after removing the first and last values")
    sum_timeDifference = 0
    for index, price in enumerate(resultList_price):
        PrintAndLog("At blockNumber " + str(resultList_blockNumber[index]) + "-" + str(resultList_timestampUnixEpoch_seconds[index]) + ", price = " + str(
            resultList_price[index]) + ", time diff between this and previous price change = " + str(
            round(resultList_timeSincePreviousPriceChange[index], 1)) + " seconds")
        sum_timeDifference += resultList_timeSincePreviousPriceChange[index]

    averageTimeDifference_seconds = sum_timeDifference / len(resultList_timeSincePreviousPriceChange)
    PrintAndLog("averageTimeDifference_seconds = " + str(averageTimeDifference_seconds))

elif sys.argv and len(sys.argv) >= 2 and "testkeeperdao_deposit" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    account = NinjaOpAccountDict["ninja-op-100"]

    # tokenAddress = Exchanges.keeperDAO.EthTokenContract
    # tokenAddress = Exchanges.zrxV2.Contract_WETH
    tokenAddress = Exchanges.keeperDAO.Contract_USDC

    amount_etherUnits = 3000
    decimals_token = Libraries.core.GetDecimalsForTokenContract(tokenAddress, True)
    amount_weiUnits = Libraries.core.ConvertEtherToWei(amount_etherUnits, decimals_token)
    liquidityProviderContractObject = Contract_KeeperDAO_LP_Simple

    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    Exchanges.keeperDAO.API_DepositToLiquidityProvider(account.publicAddress, account.DecryptPK(), liquidityProviderContractObject,
                                                       tokenAddress, amount_weiUnits, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testkeeperdao_withdraw" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    account = NinjaOpAccountDict["ninja-op-100"]

    # tokenAddress = Exchanges.keeperDAO.EthTokenContract
    # tokenAddress = Exchanges.zrxV2.Contract_WETH
    tokenAddress = Exchanges.keeperDAO.Contract_USDC

    kTokenDict = Exchanges.keeperDAO.GetKTokenDict()
    # Determine what kToken we're withdrawing
    kTokenAddress = kTokenDict[Contract_KeeperDAO_LP_Simple][tokenAddress]
    kTokenAmount_etherUnits = 3000
    decimals_token = "1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))
    kTokenAmount_weiUnits = Libraries.core.ConvertEtherToWei(kTokenAmount_etherUnits, decimals_token)
    liquidityProviderContractObject = Contract_KeeperDAO_LP_Simple

    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    Exchanges.keeperDAO.API_WithdrawFromLiquidityProvider(account.publicAddress, account.DecryptPK(), liquidityProviderContractObject,
                                                          kTokenAddress, kTokenAmount_weiUnits, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testkeeperdao_getborrowablebalance" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # tokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    tokenAddress = Exchanges.keeperDAO.EthTokenContract
    liquidityProviderContractObject = Contract_KeeperDAO_LP_Simple

    balance_etherUnits = Exchanges.keeperDAO.API_GetBorrowableBalance(liquidityProviderContractObject, tokenAddress)
    PrintAndLog("balance_etherUnits = " + str(balance_etherUnits))

elif sys.argv and len(sys.argv) >= 2 and "testkeeperdao_discordmessage" == sys.argv[1].lower():
    pass
    # Exchanges.keeperDAO.API_PostDiscordNotification("testing rate limit 0")
    # Exchanges.keeperDAO.API_PostDiscordNotification("testing rate limit 1")
    # Exchanges.keeperDAO.API_PostDiscordNotification("testing rate limit 2")
    # Exchanges.keeperDAO.API_PostDiscordNotification("testing rate limit 3")
    # Exchanges.keeperDAO.API_PostDiscordNotification("testing rate limit 4")
    # Exchanges.keeperDAO.API_PostDiscordNotification("testing rate limit 5")
    # Exchanges.keeperDAO.API_PostDiscordNotification("testing rate limit 6")
    # Exchanges.keeperDAO.API_PostDiscordNotification("testing rate limit 7")

elif sys.argv and len(sys.argv) >= 2 and "testkeeperdao_getktoken" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    kTokenDict = Exchanges.keeperDAO.GetKTokenDict()
    PrintAndLog("kTokenDict = " + str(kTokenDict))

elif sys.argv and len(sys.argv) >= 2 and "testcoinmarketcapmarketsdict" == sys.argv[1].lower():
    UpdateCoinMarketCapMarkets(1000, "id")
    marketName = 'usdc-eth'
    PrintAndLog("CoinMarketCapMarketsDict = " + str(CoinMarketCapMarketsDict))

    coinMarketCapId = ''
    currentMarketPrice_withRespectToETH = CoinMarketCapMarketsDict[coinMarketCapId].GetPriceEth(coinMarketCapId, CoinMarketCapMarketsDict)
    PrintAndLog("currentMarketPrice_withRespectToETH = " + str(currentMarketPrice_withRespectToETH))

elif sys.argv and len(sys.argv) >= 2 and "test_dynamic_gas_price_calculation_ethquotetoken" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.priceOracle.StartService()

    profit_eth = 0.00902
    exchangeNameList = ['uniswap', 'uniswap']
    baseToken = '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359'
    useCTokenBidderContract = False
    gas = Libraries.estimatedGas.GetEstimatedGasCost_Ninja_Trade_QuoteTokenIsEther(baseToken, exchangeNameList, useCTokenBidderContract)
    PrintAndLog("gas = " + str(gas))

    percentageProfitToSpendOnGas = 0.9
    PrintAndLog("percentageProfitToSpendOnGas = " + str(percentageProfitToSpendOnGas))
    gasPrice_wei = Libraries.gasStation.CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens(
        profit_eth, gas, percentageProfitToSpendOnGas)
    PrintAndLog("gasPrice = " + str(Libraries.core.ConvertWeiToGwei(gasPrice_wei)) + " gwei")
    estimatedGasCost_ether = Libraries.gasStation.GetGasCost(gasPrice_wei, gas)
    PrintAndLog("estimatedGasCost_ether = " + str(estimatedGasCost_ether))

    minProfitRequirement_ether = Libraries.gasStation.GetMinProfitRequirement_GivenGasProperties(Libraries.core.ConvertGweiToWei(10.0003), gas)
    PrintAndLog("minProfitRequirement_ether for hard coded gas price = " + str(minProfitRequirement_ether))

elif sys.argv and len(sys.argv) >= 2 and "test_dynamic_gas_price_calculation_nonethquotetoken" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    Libraries.priceOracle.StartService()

    profit_quoteToken = 0.0014757034047898632
    exchangeNameList = ['kyber', 'set']
    quoteToken = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'
    baseToken = '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359'
    useCTokenBidderContract = False
    gas = Libraries.estimatedGas.GetEstimatedGasCost_Ninja_Trade_TokenForToken(quoteToken, baseToken, exchangeNameList, useCTokenBidderContract)
    PrintAndLog("gas = " + str(gas))

    percentageProfitToSpendOnGas = 0.9
    PrintAndLog("percentageProfitToSpendOnGas = " + str(percentageProfitToSpendOnGas))
    gasPrice_wei = Libraries.gasStation.CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens(
        profit_quoteToken, gas, percentageProfitToSpendOnGas, quoteToken)
    PrintAndLog("gasPrice = " + str(Libraries.core.ConvertWeiToGwei(gasPrice_wei)) + " gwei")
    estimatedGasCost_ether = Libraries.gasStation.GetGasCost(gasPrice_wei, gas)
    PrintAndLog("estimatedGasCost_ether = " + str(estimatedGasCost_ether))

    minProfitRequirement_ether = Libraries.gasStation.GetMinProfitRequirement_GivenGasProperties(Libraries.core.ConvertGweiToWei(10.0003), gas)
    PrintAndLog("minProfitRequirement_ether for hard coded gas price = " + str(minProfitRequirement_ether))

elif sys.argv and len(sys.argv) >= 2 and "testestimategas" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    toAddress = '0x60267829fcf7cf880f8bc8eae5f546cdb107ec1a'
    fromAddress = '0x89e99fe0b3b288c3553e3d3f343a3c4a56f1527e'
    data = '0x'
    Libraries.core.API_EstimateGas(toAddress, fromAddress, data, False)

elif sys.argv and len(sys.argv) >= 2 and "test_reservable_ethereum_account_pool" == sys.argv[1].lower():
    PrintAndLog("JoeyZ testing ReservableEthereumAccountPool")
    name = "te123st"
    reservableEthereumAccountPool = CreateNewReservableEthereumAccountPoolList(
        name,
        [
            NinjaOpAccountDict["ninja-op-5"],
            NinjaOpAccountDict["ninja-op-6"],
            NinjaOpAccountDict["ninja-op-7"],
        ])
    PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
    PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
    PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))

    account1, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(name + "_1")
    PrintAndLog("Reserved account from reservableEthereumAccountPool, account1 = " + str(account1.GetDetails()))

    PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
    PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
    PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))

    account2, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(name + "_2")
    PrintAndLog("Reserved a second account from reservableEthereumAccountPool, account2 = " + str(account2.GetDetails()))

    PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
    PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
    PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))

    time.sleep(0.5)
    PrintAndLog("Time account1 has been reserved = " + str(reservableEthereumAccountPool.GetTimeAccountHasBeenReserved(account1)) + " seconds")
    PrintAndLog("Time account2 has been reserved = " + str(reservableEthereumAccountPool.GetTimeAccountHasBeenReserved(account2)) + " seconds")

    PrintAndLog("Associated accounts with a fake tx id")
    reservableEthereumAccountPool.AssociateAccountWithNonce(account1, "txId123")
    reservableEthereumAccountPool.AssociateAccountWithNonce(account2, "txId456")

    PrintAndLog("Trying to reserve again with the same name as one that's already reserved, "
                "expecting it to return the same account in case I want to override a transaction with a larger gas price")
    if reservableEthereumAccountPool.ReserveAccountFromPool(name + "_1")[0] == account1:
        PrintAndLog("Passed the test!")
    else:
        PrintAndLog("Failed the test!")

    if reservableEthereumAccountPool.ReserveAccountFromPool(name + "_2")[0] == account2:
        PrintAndLog("Passed the test!")
    else:
        PrintAndLog("Failed the test!")

    PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
    PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
    PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))

    reservableEthereumAccountPool.ReleaseAccountBackToPool(account1)
    PrintAndLog("released account1 back to pool")
    PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
    PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
    PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))

    reservableEthereumAccountPool.ReleaseAccountBackToPool(account2)
    PrintAndLog("released account2 back to pool")
    PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
    PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
    PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))

    account3, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(name + "_3")
    PrintAndLog("Reserved account from reservableEthereumAccountPool, account3 = " + str(account3.GetDetails()))
    account4, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(name + "_4")
    PrintAndLog("Reserved account from reservableEthereumAccountPool, account4 = " + str(account4.GetDetails()))
    PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
    PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
    PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
    PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))

    # account5, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(name + "_5")
    # PrintAndLog("Reserved account from reservableEthereumAccountPool, account5 = " + str(account5.GetDetails()))
    # account6, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(name + "_6")
    # PrintAndLog("Reserved account from reservableEthereumAccountPool, account6 = " + str(account6.GetDetails()))

    while True:
        Libraries.executeOnInterval.IsTimeToExecute(
            "Libraries.accounts.ReleaseReservableEthereumAccountPoolAccounts_ThatHaveBeenInUseForTooLong", 10,
            Libraries.accounts.ReleaseReservableEthereumAccountPoolAccounts_ThatHaveBeenInUseForTooLong, True)

        time.sleep(1)

elif sys.argv and len(sys.argv) >= 2 and "test_find_when_a_contract_was_deployed_or_interacted_with" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    currentBlock = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() - 1
    # For some reason, when deploying contracts that middle number changes from 60 to 80 depending on what compiler version you use.
    # So i'm only going to search and match the other numbers
    contractDeployDataToMatch_exampleOfEntireThingToMatch = "6060604052"
    # contractDeployDataToMatch_exampleOfEntireThingToMatch = "6080604052"

    contractDeployDataToMatch_firstTwoCharsOnly = "60"
    contractDeployDataToMatch_nextSixCharsOnly = "604052"
    deployersAddress = "0x3bd189a9b1c11684b08f6464866dfa431362a06e"
    returnFullTransactionObjects = True
    blockData = Libraries.core.API_GetBlock(8778382, returnFullTransactionObjects)
    transactionHashList = []
    for transactionData in blockData['transactions']:
        txData = transactionData['input'].replace("0x", "")

        match_firstRequirement = False
        match_secondRequirement = False

        charactersToCheck = Libraries.core.GetFirstXCharsInString(txData, len(contractDeployDataToMatch_firstTwoCharsOnly))
        # PrintAndLog("contractDeployDataToMatch_firstTwoCharsOnly = " + str(contractDeployDataToMatch_firstTwoCharsOnly))
        # PrintAndLog("charactersToCheck = " + str(charactersToCheck))
        if contractDeployDataToMatch_firstTwoCharsOnly.lower() == charactersToCheck.lower():
            match_firstRequirement = True

        charactersToCheck = Libraries.core.GetFirstXCharsInString(txData, len(contractDeployDataToMatch_exampleOfEntireThingToMatch))
        # PrintAndLog("Removing first x chars in " + str(charactersToCheck))
        newCharactersToCheck = Libraries.core.RemoveFirstXCharsInString(charactersToCheck, 4)
        # PrintAndLog("contractDeployDataToMatch_nextSixCharsOnly = " + str(contractDeployDataToMatch_nextSixCharsOnly))
        # PrintAndLog("newCharactersToCheck = " + str(newCharactersToCheck))
        if contractDeployDataToMatch_nextSixCharsOnly.lower() == newCharactersToCheck.lower():
            match_secondRequirement = True

        if match_firstRequirement and match_secondRequirement:
            PrintAndLog("Found a Contract Deploy")
            transactionHashList.append(transactionData['hash'])
            if transactionData['from'].lower() == deployersAddress.lower():
                PrintAndLog("Matches my deployersAddress!")

elif sys.argv and len(sys.argv) >= 2 and "generate_new_account" == sys.argv[1].lower():
    privateKey_string, accountAddress, account = Libraries.signingUtils.GenerateNewEthereumAccount()
    print('privateKey_string = ', privateKey_string)
    print('accountAddress = ', accountAddress)
    print('account = ', account)

elif sys.argv and len(sys.argv) >= 2 and "generate_contract_addresses" == sys.argv[1].lower():
    sendersAddress = '0xbAa0d8E5c334aDe87A73ed2bB936d9D46B184Cd5'
    contractAddress_0 = Libraries.vrs.GenerateContractAddress(sendersAddress, 0)
    contractAddress_1 = Libraries.vrs.GenerateContractAddress(sendersAddress, 1)
    contractAddress_2 = Libraries.vrs.GenerateContractAddress(sendersAddress, 2)
    contractAddress_3 = Libraries.vrs.GenerateContractAddress(sendersAddress, 3)
    PrintAndLog("contractAddress_0 = " + str(contractAddress_0))
    PrintAndLog("contractAddress_1 = " + str(contractAddress_1))
    PrintAndLog("contractAddress_2 = " + str(contractAddress_2))
    PrintAndLog("contractAddress_3 = " + str(contractAddress_3))

elif sys.argv and len(sys.argv) >= 2 and "deploy_contract" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    fromAddress = ninjaOp.publicAddress
    fromPrivateKey = ninjaOp.DecryptPK()

    # fromAddress = Libraries.nodes.Instance_Web3.toChecksumAddress('0xbbC04efC6f6F82832d4e077c1314b0db151d9904')
    # fromPrivateKey = ''

    doDeployContracts = True

    gasPrice_int = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice_int = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice_int = Libraries.gasStation.GetGasPrice_Fast()
    # gasPrice_int = Libraries.core.ConvertGweiToWei(93.000001)

    nonce = Libraries.core.API_GetTransactionCount(fromAddress)
    # nonce = 3243

    contractDeploymentTupleList = []

    name = 'Exchange_0xV4'
    byteCode = '608060405234801561001057600080fd5b5061198f806100206000396000f3fe608060405234801561001057600080fd5b50600436106100625760003560e01c8063049e42c0146100675780631c3abbb31461009057806366720dba146100a3578063ab6fe07b146100b6578063d3859651146100d6578063e1413317146100e9575b600080fd5b61007a61007536600461104f565b6100fc565b604051610087919061136e565b60405180910390f35b61007a61009e366004611005565b610310565b61007a6100b136600461104f565b61044f565b6100c96100c436600461108b565b61065a565b6040516100879190611878565b61007a6100e4366004611005565b610790565b6100c96100f73660046111aa565b6108b3565b600083610107610e4f565b60405163346693c560e01b815273def1c0ded9bec7f1a1670819833240f027b25eff9063346693c59061013e9087906004016116bf565b60606040518083038186803b15801561015657600080fd5b505afa15801561016a573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061018e9190610f49565b90506001816020015160048111156101a257fe5b146101bc576101b160036109c1565b600092505050610309565b60408101516000906101d46080870160608801610f98565b036001600160801b03169050600061021d6101f56060880160408901610f98565b6001600160801b031661020e6080890160608a01610f98565b6001600160801b031684610a16565b905061022b8161270f610a33565b90506102378882610a61565b9750838810156102585761024b60186109c1565b6000945050505050610309565b6102ec886001600160a01b0389166370a0823161027b60a08b0160808c01610f0e565b6040518263ffffffff1660e01b8152600401610297919061130b565b60206040518083038186803b1580156102af57600080fd5b505afa1580156102c3573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906102e79190610fed565b610a61565b9750838810156103005761024b60196109c1565b60019450505050505b9392505050565b60008361031b610e4f565b604051639548088960e01b815273def1c0ded9bec7f1a1670819833240f027b25eff9063954808899061035290879060040161149f565b60606040518083038186803b15801561036a57600080fd5b505afa15801561037e573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906103a29190610f49565b90506001816020015160048111156103b657fe5b146103c5576101b160036109c1565b60408101516000906103dd6080870160608801610f98565b036001600160801b0316905060006103fe6101f56060880160408901610f98565b905061040c8161270f610a33565b90506104188882610a61565b97508388101561042c5761024b60186109c1565b6102ec886001600160a01b0389166370a0823161027b60c08b0160a08c01610f0e565b60008361045a610e4f565b60405163346693c560e01b815273def1c0ded9bec7f1a1670819833240f027b25eff9063346693c5906104919087906004016116bf565b60606040518083038186803b1580156104a957600080fd5b505afa1580156104bd573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906104e19190610f49565b90506001816020015160048111156104f557fe5b14610504576101b160036109c1565b604081015160009061051c6080870160608801610f98565b036001600160801b03169050610533601282610a77565b61053d8782610a61565b96508287101561055d5761055160186109c1565b60009350505050610309565b60006001600160a01b0387166370a0823161057e60a0890160808a01610f0e565b6040518263ffffffff1660e01b815260040161059a919061130b565b60206040518083038186803b1580156105b257600080fd5b505afa1580156105c6573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906105ea9190610fed565b905060006106296106016080890160608a01610f98565b6001600160801b031661061a60608a0160408b01610f98565b6001600160801b03168b610acf565b90508082101561064b5761063d60196109c1565b600095505050505050610309565b50600198975050505050505050565b6000610664610ae4565b600b01546001600160a01b031633148061069b5750610681610b08565b336000908152602091909152604090205460ff1615156001145b6106c05760405162461bcd60e51b81526004016106b7906113ac565b60405180910390fd5b6106e2836020015173def1c0ded9bec7f1a1670819833240f027b25eff610b2c565b600073def1c0ded9bec7f1a1670819833240f027b25eff63f6274f6661070b620111703a610c41565b8686896040518563ffffffff1660e01b815260040161072c939291906115ca565b60408051808303818588803b15801561074457600080fd5b505af1158015610758573d6000803e3d6000fd5b50505050506040513d601f19601f8201168201806040525081019061077d9190610fb4565b6001600160801b03169695505050505050565b60008361079b610e4f565b604051639548088960e01b815273def1c0ded9bec7f1a1670819833240f027b25eff906395480889906107d290879060040161149f565b60606040518083038186803b1580156107ea57600080fd5b505afa1580156107fe573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906108229190610f49565b905060018160200151600481111561083657fe5b14610845576101b160036109c1565b604081015160009061085d6080870160608801610f98565b036001600160801b03169050610874601282610a77565b61087e8782610a61565b9650828710156108925761055160186109c1565b60006001600160a01b0387166370a0823161057e60c0890160a08a01610f0e565b60006108bd610ae4565b600b01546001600160a01b03163314806108f457506108da610b08565b336000908152602091909152604090205460ff1615156001145b6109105760405162461bcd60e51b81526004016106b7906113ac565b610932836020015173def1c0ded9bec7f1a1670819833240f027b25eff610b2c565b604051632a9dd1db60e21b815260009073def1c0ded9bec7f1a1670819833240f027b25eff9063aa77476c9061097090879087908a906004016117ac565b6040805180830381600087803b15801561098957600080fd5b505af115801561099d573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061077d9190610fb4565b60006109cb610ae4565b9050806006015460011415610a12577f6afa7c09e54f46a385be04ce7ac1427483e0b51dd6fb76e80786cda402d033b982604051610a099190611881565b60405180910390a15b5050565b6000610a2b610a258386610c41565b84610c90565b949350505050565b6000610a3f8383610c41565b9250610a5583610a50846001610ca5565b610c90565b92508290505b92915050565b6000818310610a705781610309565b5090919050565b6000610a81610ae4565b9050806006015460011415610aca577f55f3e3ae10986ee56f72e0f34eb828eab097443abcee79da7499727bbdce81688383604051610ac192919061188f565b60405180910390a15b505050565b6000610a2b610ade8385610c41565b85610c90565b7f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff390565b7f4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f090565b604051636eb1769f60e11b81526000906001600160a01b0384169063dd62ed3e90610b5d903090869060040161131f565b60206040518083038186803b158015610b7557600080fd5b505afa158015610b89573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610bad9190610fed565b90506bffffffffffffffffffffffff811015610aca578015610c2057610c208363095ea7b360e01b846000604051602401610be9929190611339565b60408051601f198184030181529190526020810180516001600160e01b03166001600160e01b031990931692909217909152610cd7565b610aca8363095ea7b360e01b84600019604051602401610be9929190611355565b600082610c5057506000610a5b565b82820282848281610c5d57fe5b0414610c6857600080fd5b82848281610c7257fe5b04146103095760405162461bcd60e51b81526004016106b7906113f4565b600080828481610c9c57fe5b04949350505050565b600082820183811015610cb757600080fd5b838110156103095760405162461bcd60e51b81526004016106b7906113f4565b6060610d2c826040518060400160405280602081526020017f5361666545524332303a206c6f772d6c6576656c2063616c6c206661696c6564815250856001600160a01b0316610d669092919063ffffffff16565b805190915015610aca5780806020019051810190610d4a9190610f29565b610aca5760405162461bcd60e51b81526004016106b790611455565b6060610a2b848460008585610d7a85610e10565b610d965760405162461bcd60e51b81526004016106b79061141e565b60006060866001600160a01b03168587604051610db391906112ef565b60006040518083038185875af1925050503d8060008114610df0576040519150601f19603f3d011682016040523d82523d6000602084013e610df5565b606091505b5091509150610e05828286610e16565b979650505050505050565b3b151590565b60608315610e25575081610309565b825115610e355782518084602001fd5b8160405162461bcd60e51b81526004016106b79190611379565b6040805160608101909152600080825260208201908152600060209091015290565b80356001600160a01b0381168114610a5b57600080fd5b600060808284031215610e99578081fd5b610ea360806118a2565b9050813560048110610eb457600080fd5b8152602082013560ff81168114610eca57600080fd5b80602083015250604082013560408201526060820135606082015292915050565b8035610a5b81611944565b803567ffffffffffffffff81168114610a5b57600080fd5b600060208284031215610f1f578081fd5b6103098383610e71565b600060208284031215610f3a578081fd5b81518015158114610309578182fd5b600060608284031215610f5a578081fd5b610f6460606118a2565b82518152602083015160058110610f79578283fd5b60208201526040830151610f8c81611944565b60408201529392505050565b600060208284031215610fa9578081fd5b813561030981611944565b60008060408385031215610fc6578081fd5b8251610fd181611944565b6020840151909250610fe281611944565b809150509250929050565b600060208284031215610ffe578081fd5b5051919050565b60008060008385036101c081121561101b578182fd5b84359350602085013561102d8161192c565b9250610180603f1982011215611041578182fd5b506040840190509250925092565b6000806000838503610180811215611065578182fd5b8435935060208501356110778161192c565b9250610140603f1982011215611041578182fd5b60008060008385036102208112156110a1578182fd5b8435935061018080601f19830112156110b8578283fd5b6110c1816118a2565b91506110d08760208801610e71565b82526110df8760408801610e71565b60208301526110f18760608801610eeb565b60408301526111038760808801610eeb565b60608301526111158760a08801610eeb565b60808301526111278760c08801610e71565b60a08301526111398760e08801610e71565b60c083015261010061114d88828901610e71565b60e084015261012061116189828a01610e71565b82850152610140915081880135818501525061016061118289828a01610ef6565b91840191909152908601359082015291506111a1856101a08601610e88565b90509250925092565b60008060008385036101e08112156111c0578182fd5b8435935061014080601f19830112156111d7578283fd5b6111e0816118a2565b91506111ef8760208801610e71565b82526111fe8760408801610e71565b60208301526112108760608801610eeb565b60408301526112228760808801610eeb565b60608301526112348760a08801610e71565b60808301526112468760c08801610e71565b60a08301526112588760e08801610e71565b60c08301526101008087013560e084015261012061127889828a01610ef6565b91840191909152908601359082015291506111a1856101608601610e88565b6001600160a01b03169052565b8051600481106112b057fe5b825260208181015160ff169083015260408082015190830152606090810151910152565b6001600160801b03169052565b67ffffffffffffffff169052565b600082516113018184602087016118fc565b9190910192915050565b6001600160a01b0391909116815260200190565b6001600160a01b0392831681529116602082015260400190565b6001600160a01b0392909216825260ff16602082015260400190565b6001600160a01b03929092168252602082015260400190565b901515815260200190565b60006020825282518060208401526113988160408501602087016118fc565b601f01601f19169190910160400192915050565b60208082526028908201527f4d757374206265204e696e6a61206f722077686974656c6973746564204b6565604082015267070657244414f4c560c41b606082015260800190565b60208082526010908201526f55494e543235365f4f564552464c4f5760801b604082015260600190565b6020808252601d908201527f416464726573733a2063616c6c20746f206e6f6e2d636f6e7472616374000000604082015260600190565b6020808252602a908201527f5361666545524332303a204552433230206f7065726174696f6e20646964206e6040820152691bdd081cdd58d8d9595960b21b606082015260800190565b6101808101602083016114bb836114b68387610e71565b611297565b6114c581856118c9565b6114d26020850182611297565b50506114e160408401846118d6565b6114ee60408401826112d4565b506114fc60608401846118d6565b61150960608401826112d4565b5061151760808401846118d6565b61152460808401826112d4565b5061153260a08401846118c9565b61153f60a0840182611297565b5061154d60c08401846118c9565b61155a60c0840182611297565b5061156860e08401846118c9565b61157560e0840182611297565b50610100611585818501856118c9565b61159182850182611297565b505061012083810135908301526101406115ad818501856118e3565b6115b9828501826112e1565b505061016092830135919092015290565b6000610220820190506115de828651611297565b60208501516115f06020840182611297565b50604085015161160360408401826112d4565b50606085015161161660608401826112d4565b50608085015161162960808401826112d4565b5060a085015161163c60a0840182611297565b5060c085015161164f60c0840182611297565b5060e085015161166260e0840182611297565b506101008086015161167682850182611297565b5050610120858101519083015261014080860151611696828501826112e1565b505061016085810151908301526116b16101808301856112a4565b610a2b6102008301846112d4565b6101408101602083016116d6836114b68387610e71565b6116e081856118c9565b6116ed6020850182611297565b50506116fc60408401846118d6565b61170960408401826112d4565b5061171760608401846118d6565b61172460608401826112d4565b5061173260808401846118c9565b61173f6080840182611297565b5061174d60a08401846118c9565b61175a60a0840182611297565b5061176860c08401846118c9565b61177560c0840182611297565b5060e083013560e083015261010061178f818501856118e3565b61179b828501826112e1565b505061012092830135919092015290565b60006101e0820190506117c0828651611297565b60208501516117d26020840182611297565b5060408501516117e560408401826112d4565b5060608501516117f860608401826112d4565b50608085015161180b6080840182611297565b5060a085015161181e60a0840182611297565b5060c085015161183160c0840182611297565b5060e085015160e08301526101008086015161184f828501826112e1565b5050610120858101519083015261186a6101408301856112a4565b610a2b6101c08301846112d4565b90815260200190565b60ff91909116815260200190565b60ff929092168252602082015260400190565b60405181810167ffffffffffffffff811182821017156118c157600080fd5b604052919050565b600082356103098161192c565b6000823561030981611944565b6000823567ffffffffffffffff81168114610309578182fd5b60005b838110156119175781810151838201526020016118ff565b83811115611926576000848401525b50505050565b6001600160a01b038116811461194157600080fd5b50565b6001600160801b038116811461194157600080fdfea2646970667358221220a7a28fba9ee0712a3901a42178d4651052a22720458fb40a0a6511ad859eee6264736f6c63430006040033'
    estimatedGas = Libraries.core.API_EstimateGas_ContractDeployOnly(fromAddress, byteCode, nonce, True)
    contractDeploymentTupleList.append((name, byteCode, nonce, estimatedGas))
    nonce += 1

    # name = 'Trade_3'
    # byteCode = '608060405234801561001057600080fd5b50612ad8806100206000396000f3fe608060405234801561001057600080fd5b50600436106100575760003560e01c8063322851571461005c57806368c2c5fb1461007157806398dd43a41461009a578063ad7d78da146100ad578063c6285e56146100c0575b600080fd5b61006f61006a3660046121ce565b6100d3565b005b61008461007f366004612064565b61012c565b604051610091919061294e565b60405180910390f35b61006f6100a836600461219f565b6103ab565b61006f6100bb36600461219f565b610437565b61006f6100ce3660046121ce565b61057f565b60006100dd6105cf565b600b8101549091506001600160a01b031633146101155760405162461bcd60e51b815260040161010c906125a5565b60405180910390fd5b610124868686868660006105f3565b505050505050565b600080610137610706565b3360009081526020829052604090205490915060ff16151560011461016e5760405162461bcd60e51b815260040161010c90612920565b60006101798961072a565b905060606101868261077a565b905060005b60ff81168911156102a057600060ff821615806101ae575060ff82166000198b01145b156101ce578d8d60008181106101c057fe5b9050602002013590506101e7565b8d8d8360ff168181106101dd57fe5b9050602002013590505b600061025061024b8d8d8660ff168181106101fe57fe5b905060200281019061021091906129a5565b8080602002602001604051908101604052809392919081815260200183836020028082843760009201829052509250600191508290506107dc565b6108b9565b9050806102965761028a8460008151811061026757fe5b60200260200101518560018151811061027c57fe5b602002602001015187610a9d565b9650505050505061039e565b505060010161018b565b5084156102fd576102f88c8c60008181106102b757fe5b9050602002013583898980806020026020016040519081016040528093929190818152602001838360200280828437600092019190915250610b7592505050565b610366565b610366610361888880806020026020016040519081016040528093929190818152602001838360200280828437600081840152601f19601f820116905080830192505050505050508e8e600081811061035257fe5b905060200201356001806107dc565b610bfa565b6103998160008151811061037657fe5b60200260200101518260018151811061038b57fe5b602002602001015184610a9d565b935050505b5098975050505050505050565b60006103b5610c7d565b3360009081526020829052604090205490915060ff1615156001146103ec5760405162461bcd60e51b815260040161010c906127f2565b60006103f66105cf565b9050600061040384610ca1565b9050848110156104255760405162461bcd60e51b815260040161010c906128aa565b610430828686610d52565b5050505050565b6000610441610706565b3360009081526020829052604090205490915060ff1615156001146104785760405162461bcd60e51b815260040161010c90612920565b600061048383610ca1565b9050838110156104a55760405162461bcd60e51b815260040161010c906124bf565b600160006104b9868363ffffffff610e2716565b905060006104c56105cf565b60058101546040519192506001600160a01b031690636e2246a7908890869063263750e960e21b906104fd9088908590602401612957565b60408051601f198184030181529181526020820180516001600160e01b03166001600160e01b03199485161790525160e086901b90921682526105449392916004016123f3565b600060405180830381600087803b15801561055e57600080fd5b505af1158015610572573d6000803e3d6000fd5b5050505050505050505050565b6000610589610c7d565b3360009081526020829052604090205490915060ff1615156001146105c05760405162461bcd60e51b815260040161010c906127f2565b610124868686868660016105f3565b7f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff390565b60006105fe86610ca1565b9050831561060f5761060f87610e55565b600080805b87518160ff1610156106da5760ff811661063057899250610634565b8192505b606061065a898360ff168151811061064857fe5b602002602001015185600060016107dc565b90506000610666610ecd565b9050600061069d8b8560ff168151811061067c57fe5b602002602001015160008151811061069057fe5b6020026020010151610ef1565b6001600160e01b0319811660009081526020849052604090205490915060601c6106c8848284610f28565b95505060019093019250610614915050565b5084156106ea576106ea81611111565b83156106fb576106fb898985611187565b505050505050505050565b7f23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe690565b69bcd6227055d1ada50b6619016000610744826005611207565b6040805160148082528183019092529193506060919060208201818036833750505060148181018581529152519150505b919050565b604080516003808252608082019092526060916020820183803683370190505090506107a582610ca1565b816000815181106107b257fe5b6020026020010181815250505a816001815181106107cc57fe5b6020908102919091010152919050565b606082156107f0576107ed85611217565b94505b6000856000815181106107ff57fe5b6020026020010151905060008651905060008085156108265750506001016002600061082d565b5060019050805b604080516004600019860160209081028281018085526023909101601f19168401909101909352919060249082801561086d576020820181803683370190505b509750845b8681101561089457602085820181028e015190820285018a0152600101610872565b5060018914156108a4578a818901525b50908601949094525050508152949350505050565b6000806108c4610ecd565b905060006108d184611294565b6001600160e01b0319811660008181526020859052604090205491925060609190911c906310a814a960e11b141561091d5761091661091186600061129b565b6112dc565b9350610a95565b6001600160e01b0319821663e6582ba560e01b141561095f5761091661094486600061129b565b61094f87600161129b565b61095a88600261129b565b611393565b6001600160e01b03198216634a68197560e01b14156109a15761091661098686600061129b565b61099187600161129b565b61099c88600261129b565b61143c565b6001600160e01b0319821663ec15c77760e01b14156109d8576109166109c886600061129b565b6109d387600161129b565b6114d0565b6001600160e01b0319821663921bd00160e01b1415610a0f576109166109ff86600061129b565b610a0a87600161129b565b611504565b60006060826001600160a01b031687604051610a2b9190612359565b600060405180830381855af49150503d8060008114610a66576040519150601f19603f3d011682016040523d82523d6000602084013e610a6b565b606091505b509150915081610a8d5760405162461bcd60e51b815260040161010c9061247b565b602001519450505b505050919050565b600080610aa86105cf565b90506000610ab584610ca1565b905085811015610ad75760405162461bcd60e51b815260040161010c90612900565b610ae7818763ffffffff61152416565b92507f4643b65b54e79c3bf1066dbe4300ca46e42ee7ddeaa0ca4c1c43ff74ab241f728484604051610b1a9291906123a6565b60405180910390a181600801543a1115610b66576000601036025a876152080103019050600061a3db8261374a0181610b4f57fe5b0490508015610b6357610b6181611566565b505b50505b50610b6e9050565b9392505050565b6000610b7f6105cf565b60058101549091506001600160a01b0316636e2246a78486610ba486826001806107dc565b6040518463ffffffff1660e01b8152600401610bc2939291906123f3565b600060405180830381600087803b158015610bdc57600080fd5b505af1158015610bf0573d6000803e3d6000fd5b5050505050505050565b6000306001600160a01b0316600083604051610c169190612359565b60006040518083038185875af1925050503d8060008114610c53576040519150601f19603f3d011682016040523d82523d6000602084013e610c58565b606091505b5050905080610c795760405162461bcd60e51b815260040161010c90612776565b5050565b7f4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f090565b600073eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee6001600160a01b0383161415610ccf575047610775565b6040516370a0823160e01b81526001600160a01b038316906370a0823190610cfb903090600401612378565b60206040518083038186803b158015610d1357600080fd5b505afa158015610d27573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610d4b9190612187565b9050610775565b73eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee6001600160a01b0382161415610e005760058301546040516000916001600160a01b0316908490610d9790612375565b60006040518083038185875af1925050503d8060008114610dd4576040519150601f19603f3d011682016040523d82523d6000602084013e610dd9565b606091505b5050905080610dfa5760405162461bcd60e51b815260040161010c906126df565b50610e22565b6005830154610e22906001600160a01b0383811691168463ffffffff6115ec16565b505050565b600082820183811015610e4c5760405162461bcd60e51b815260040161010c906125cc565b90505b92915050565b6000610e5f6105cf565b905081471015610e815760405162461bcd60e51b815260040161010c90612562565b600181015460408051630d0e30db60e41b815290516001600160a01b0390921691829163d0e30db091869160048082019260009290919082900301818588803b158015610bdc57600080fd5b7faab760237e39f9aad10fcfc005484cb19a7fafbfa4772d5ae586b83789f67cd190565b6040805160048082528183019092526000916060919060208201818036833750505060048181019490945292835250506020015190565b60006001600160e01b03198216635400ac3160e01b1415610f7e57610f77610f5185600061129b565b610f5c86600161129b565b610f6787600261129b565b610f7288600361129b565b611642565b9050610b6e565b6001600160e01b031982166368f2bed360e01b1415610fcb57610f77610fa585600061129b565b610fb086600161129b565b610fbb87600261129b565b610fc688600361129b565b611818565b6001600160e01b031982166340f3395560e01b141561101857610f77610ff285600061129b565b610ffd86600161129b565b61100887600261129b565b61101388600361129b565b611993565b6001600160e01b03198216631d632f0360e01b141561104f57610f7761103f85600061129b565b61104a86600161129b565b611a40565b6001600160e01b0319821663d605428760e01b141561108657610f7761107685600061129b565b61108186600161129b565b611ac8565b60006060846001600160a01b0316866040516110a29190612359565b600060405180830381855af49150503d80600081146110dd576040519150601f19603f3d011682016040523d82523d6000602084013e6110e2565b606091505b5091509150816111045760405162461bcd60e51b815260040161010c9061269b565b6020015195945050505050565b600061111b6105cf565b6001810154604051632e1a7d4d60e01b81529192506001600160a01b0316908190632e1a7d4d9061115090869060040161294e565b600060405180830381600087803b15801561116a57600080fd5b505af115801561117e573d6000803e3d6000fd5b50505050505050565b60006111916105cf565b9050600061119e84610ca1565b90506000838211156111ed576111ba828563ffffffff61152416565b600a8401549091506111ea906111de83670de0b6b3a764000063ffffffff611b5216565b9063ffffffff611b8c16565b90505b61012483611201888463ffffffff610e2716565b87610d52565b61010081900382901c91901b1790565b60608151604051908082528060200260200182016040528015611244578160200160208202803683370190505b50905060005b825181101561128e5761126f83828151811061126257fe5b6020026020010151611bce565b82828151811061127b57fe5b602090810291909101015260010161124a565b50919050565b6020015190565b6000808260200260040160ff16905080602001845110156112ce5760405162461bcd60e51b815260040161010c906127bb565b929092016020015192915050565b600080826001600160a01b0316630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b15801561131857600080fd5b505afa15801561132c573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906113509190612133565b92505050640100000000428161136257fe5b0663ffffffff168163ffffffff16141561138a57611380601e611bdb565b6000915050610775565b50600192915050565b600080846001600160a01b03166370a08231846040518263ffffffff1660e01b81526004016113c29190612378565b60206040518083038186803b1580156113da57600080fd5b505afa1580156113ee573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906114129190612187565b90508381111561143157611427601c82611c2f565b6000915050610b6e565b506001949350505050565b600080846001600160a01b03166370a08231846040518263ffffffff1660e01b815260040161146b9190612378565b60206040518083038186803b15801561148357600080fd5b505afa158015611497573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906114bb9190612187565b90508381101561143157611427601c82611c2f565b60006001600160a01b03821631838111156114fa576114f0600a82611c2f565b6000915050610e4f565b5060019392505050565b60006001600160a01b03821631838110156114fa576114f0600a82611c2f565b6000610e4c83836040518060400160405280601e81526020017f536166654d6174683a207375627472616374696f6e206f766572666c6f770000815250611c86565b60405163d8ccd0f360e01b81526000906d4946c0e9f43f4dee607b0ef1fa1c9063d8ccd0f39061159a90859060040161294e565b602060405180830381600087803b1580156115b457600080fd5b505af11580156115c8573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610e4f9190612187565b610e228363a9059cbb60e01b848460405160240161160b9291906123a6565b60408051601f198184030181529190526020810180516001600160e01b03166001600160e01b031990931692909217909152611cb2565b6000806000846001600160a01b0316630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b15801561168057600080fd5b505afa158015611694573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906116b89190612133565b509150915060008590506000816001600160a01b0316630dfe16816040518163ffffffff1660e01b815260040160206040518083038186803b1580156116fd57600080fd5b505afa158015611711573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906117359190612048565b905060006117568a866001600160701b0316866001600160701b0316611d41565b90506117716001600160a01b0383168963ffffffff611df316565b8815611791576117916001600160a01b038316898c63ffffffff6115ec16565b604080516000808252602082019092526001600160a01b0385169163022c0d9f9184908b90835b506040518563ffffffff1660e01b81526004016117d89493929190612423565b600060405180830381600087803b1580156117f257600080fd5b505af1158015611806573d6000803e3d6000fd5b50929c9b505050505050505050505050565b6000806000846001600160a01b0316630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b15801561185657600080fd5b505afa15801561186a573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061188e9190612133565b509150915060008590506000816001600160a01b031663d21220a76040518163ffffffff1660e01b815260040160206040518083038186803b1580156118d357600080fd5b505afa1580156118e7573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061190b9190612048565b9050600061192c8a856001600160701b0316876001600160701b0316611d41565b90506119476001600160a01b0383168963ffffffff611df316565b8815611967576119676001600160a01b038316898c63ffffffff6115ec16565b604080516000808252602082019092526001600160a01b0385169163022c0d9f918491908b90826117b8565b60006119ae6001600160a01b0384168563ffffffff611df316565b604051638201aa3f60e01b81526001600160a01b03851690638201aa3f906119e590869089908790600190600019906004016123bf565b6040805180830381600087803b1580156119fe57600080fd5b505af1158015611a12573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190611a36919061230a565b5095945050505050565b6000816001600160a01b031663f39b5b9b846001609960f81b6040518463ffffffff1660e01b8152600401611a7692919061245a565b6020604051808303818588803b158015611a8f57600080fd5b505af1158015611aa3573d6000803e3d6000fd5b50505050506040513d601f19601f82011682018060405250810190610e4c9190612187565b6040516395e3c50b60e01b81526000906001600160a01b038316906395e3c50b90611b00908690600190609960f81b9060040161296e565b602060405180830381600087803b158015611b1a57600080fd5b505af1158015611b2e573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610e4c9190612187565b600082611b6157506000610e4f565b82820282848281611b6e57fe5b0414610e4c5760405162461bcd60e51b815260040161010c90612735565b6000610e4c83836040518060400160405280601a81526020017f536166654d6174683a206469766973696f6e206279207a65726f000000000000815250611ea6565b6000610e4f82600b611207565b6000611be56105cf565b9050806006015460011415610c79577f6afa7c09e54f46a385be04ce7ac1427483e0b51dd6fb76e80786cda402d033b982604051611c239190612984565b60405180910390a15050565b6000611c396105cf565b9050806006015460011415610e22577f55f3e3ae10986ee56f72e0f34eb828eab097443abcee79da7499727bbdce81688383604051611c79929190612992565b60405180910390a1505050565b60008184841115611caa5760405162461bcd60e51b815260040161010c9190612468565b505050900390565b6060611d07826040518060400160405280602081526020017f5361666545524332303a206c6f772d6c6576656c2063616c6c206661696c6564815250856001600160a01b0316611edd9092919063ffffffff16565b805190915015610e225780806020019051810190611d259190612117565b610e225760405162461bcd60e51b815260040161010c90612860565b6000808411611d625760405162461bcd60e51b815260040161010c9061250d565b600083118015611d725750600082115b611d8e5760405162461bcd60e51b815260040161010c90612603565b6000611da2856103e563ffffffff611b5216565b90506000611db6828563ffffffff611b5216565b90506000611ddc83611dd0886103e863ffffffff611b5216565b9063ffffffff610e2716565b9050808281611de757fe5b04979650505050505050565b604051636eb1769f60e11b81526bffffffffffffffffffffffff906001600160a01b0384169063dd62ed3e90611e2f903090869060040161238c565b60206040518083038186803b158015611e4757600080fd5b505afa158015611e5b573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190611e7f9190612187565b1015610c7957610c798263095ea7b360e01b8360001960405160240161160b9291906123a6565b60008183611ec75760405162461bcd60e51b815260040161010c9190612468565b506000838581611ed357fe5b0495945050505050565b6060611eec8484600085611ef4565b949350505050565b606082471015611f165760405162461bcd60e51b815260040161010c90612655565b611f1f85611fb5565b611f3b5760405162461bcd60e51b815260040161010c90612829565b60006060866001600160a01b03168587604051611f589190612359565b60006040518083038185875af1925050503d8060008114611f95576040519150601f19603f3d011682016040523d82523d6000602084013e611f9a565b606091505b5091509150611faa828286611fbb565b979650505050505050565b3b151590565b60608315611fca575081610b6e565b825115611fda5782518084602001fd5b8160405162461bcd60e51b815260040161010c9190612468565b60008083601f840112612005578182fd5b50813567ffffffffffffffff81111561201c578182fd5b602083019150836020808302850101111561203657600080fd5b9250929050565b8035610e4f81612a7f565b600060208284031215612059578081fd5b8151610e4c81612a67565b60008060008060008060008060a0898b03121561207f578384fd5b883567ffffffffffffffff80821115612096578586fd5b6120a28c838d01611ff4565b909a50985060208b0135975060408b01359150808211156120c1578586fd5b6120cd8c838d01611ff4565b909750955060608b01359150808211156120e5578485fd5b506120f28b828c01611ff4565b909450925050608089013561210681612a7f565b809150509295985092959890939650565b600060208284031215612128578081fd5b8151610e4c81612a7f565b600080600060608486031215612147578283fd5b835161215281612a8d565b602085015190935061216381612a8d565b604085015190925063ffffffff8116811461217c578182fd5b809150509250925092565b600060208284031215612198578081fd5b5051919050565b600080604083850312156121b1578182fd5b8235915060208301356121c381612a67565b809150509250929050565b600080600080600060a086880312156121e5578081fd5b853594506121f66020870135612a67565b6020860135935067ffffffffffffffff60408701351115612215578081fd5b6040860135860187601f82011261222a578182fd5b61223c6122378235612a17565b6129f0565b81358152602080820191908301845b84358110156122da578b603f833587010112612265578586fd5b602082358601013561227961223782612a17565b80828252602082019150604085358901018f60406020860288358c01010111156122a157898afd5b895b848110156122c15781358452602093840193909101906001016122a3565b505086525050602093840193919091019060010161224b565b50508095505050506122ef876060880161203d565b91506122fe876080880161203d565b90509295509295909350565b6000806040838503121561231c578182fd5b505080516020909101519092909150565b60008151808452612345816020860160208601612a37565b601f01601f19169290920160200192915050565b6000825161236b818460208701612a37565b9190910192915050565b90565b6001600160a01b0391909116815260200190565b6001600160a01b0392831681529116602082015260400190565b6001600160a01b03929092168252602082015260400190565b6001600160a01b03958616815260208101949094529190931660408301526060820192909252608081019190915260a00190565b600060018060a01b03851682528360208301526060604083015261241a606083018461232d565b95945050505050565b600085825284602083015260018060a01b038416604083015260806060830152612450608083018461232d565b9695505050505050565b918252602082015260400190565b600060208252610e4c602083018461232d565b60208082526024908201527f706572666f726d436865636b206661696c656420647572696e672065786563756040820152633a34b7b760e11b606082015260800190565b6020808252602e908201527f736861726550726f666974576974684b656570657244414f3a20496e7375666660408201526d696369656e742062616c616e636560901b606082015260800190565b60208082526035908201527f636f6e76657274416d6f756e74496e546f416d6f756e744f75743a20494e535560408201527411919250d251539517d25394155517d05353d55395605a1b606082015260800190565b60208082526023908201527f547269656420746f2077726170206d6f726520455448207468616e207765206860408201526261766560e81b606082015260800190565b6020808252600d908201526c4d757374206265204e696e6a6160981b604082015260600190565b6020808252601b908201527f536166654d6174683a206164646974696f6e206f766572666c6f770000000000604082015260600190565b60208082526032908201527f636f6e76657274416d6f756e74496e546f416d6f756e744f75743a20494e53556040820152714646494349454e545f4c495155494449545960701b606082015260800190565b60208082526026908201527f416464726573733a20696e73756666696369656e742062616c616e636520666f6040820152651c8818d85b1b60d21b606082015260800190565b60208082526024908201527f706572666f726d5472616465206661696c656420647572696e672065786563756040820152633a34b7b760e11b606082015260800190565b60208082526036908201527f5472616e73666572206f6620626f72726f77656451756f7465546f6b656e73206040820152753130b1b5903a37902628103430b9903330b4b632b21760511b606082015260800190565b60208082526021908201527f536166654d6174683a206d756c7469706c69636174696f6e206f766572666c6f6040820152607760f81b606082015260800190565b60208082526025908201527f696e6974696174655472616465206661696c656420647572696e6720657865636040820152643aba34b7b760d91b606082015260800190565b60208082526018908201527f6578747261637420617267206f7574206f662072616e67650000000000000000604082015260600190565b6020808252601f908201527f4d7573742062652077686974656c6973746564204b656570657244414f4c5000604082015260600190565b6020808252601d908201527f416464726573733a2063616c6c20746f206e6f6e2d636f6e7472616374000000604082015260600190565b6020808252602a908201527f5361666545524332303a204552433230206f7065726174696f6e20646964206e6040820152691bdd081cdd58d8d9595960b21b606082015260800190565b60208082526036908201527f736861726550726f666974576974684b656570657244414f43616c6c6261636b6040820152753a20496e73756666696369656e742062616c616e636560501b606082015260800190565b602080825260069082015265436f6465203560d01b604082015260600190565b60208082526014908201527326bab9ba103132903bb434ba32b634b9ba32b21760611b604082015260600190565b90815260200190565b9182526001600160a01b0316602082015260400190565b9283526020830191909152604082015260600190565b60ff91909116815260200190565b60ff929092168252602082015260400190565b6000808335601e198436030181126129bb578283fd5b8084018035925067ffffffffffffffff8311156129d6578384fd5b602081019350505060208102360382131561203657600080fd5b60405181810167ffffffffffffffff81118282101715612a0f57600080fd5b604052919050565b600067ffffffffffffffff821115612a2d578081fd5b5060209081020190565b60005b83811015612a52578181015183820152602001612a3a565b83811115612a61576000848401525b50505050565b6001600160a01b0381168114612a7c57600080fd5b50565b8015158114612a7c57600080fd5b6001600160701b0381168114612a7c57600080fdfea264697066735822122093bd884b79414df302d9ec480ef475177eaa58a6121e5462ee9ed9ce8407f0e264736f6c63430006040033'
    # estimatedGas = Libraries.core.API_EstimateGas_ContractDeployOnly(fromAddress, byteCode, nonce, True)
    # contractDeploymentTupleList.append((name, byteCode, nonce, estimatedGas))
    # nonce += 1

    # name = 'Debugger'
    # byteCode = '608060405234801561001057600080fd5b506106c7806100206000396000f3fe608060405234801561001057600080fd5b50600436106100885760003560e01c8063a7c088351161005b578063a7c08835146100fd578063a90fdf2014610119578063cb3537d314610135578063d8c0f3a51461015157610088565b806332beb3851461008d5780633e6ceb0b146100a95780634a5ad1a6146100c55780637ab50ed1146100e1575b600080fd5b6100a760048036038101906100a291906104af565b61016d565b005b6100c360048036038101906100be91906103b2565b610170565b005b6100df60048036038101906100da9190610486565b610173565b005b6100fb60048036038101906100f69190610348565b610176565b005b6101176004803603810190610112919061041c565b610179565b005b610133600480360381019061012e9190610445565b61017c565b005b61014f600480360381019061014a91906103f3565b61017f565b005b61016b60048036038101906101669190610371565b610182565b005b50565b50565b50565b50565b50565b50565b50565b50565b6000813590506101948161061e565b92915050565b600082601f8301126101ab57600080fd5b81356101be6101b982610505565b6104d8565b915081818352602084019350602081019050838560208402820111156101e357600080fd5b60005b8381101561021357816101f98882610185565b8452602084019350602083019250506001810190506101e6565b5050505092915050565b600082601f83011261022e57600080fd5b813561024161023c8261052d565b6104d8565b9150818183526020840193506020810190508385602084028201111561026657600080fd5b60005b83811015610296578161027c8882610333565b845260208401935060208301925050600181019050610269565b5050505092915050565b6000813590506102af81610635565b92915050565b6000813590506102c48161064c565b92915050565b600082601f8301126102db57600080fd5b81356102ee6102e982610555565b6104d8565b9150808252602083016020830185838301111561030a57600080fd5b61031583828461060f565b50505092915050565b60008135905061032d81610663565b92915050565b6000813590506103428161067a565b92915050565b60006020828403121561035a57600080fd5b600061036884828501610185565b91505092915050565b60006020828403121561038357600080fd5b600082013567ffffffffffffffff81111561039d57600080fd5b6103a98482850161019a565b91505092915050565b6000602082840312156103c457600080fd5b600082013567ffffffffffffffff8111156103de57600080fd5b6103ea8482850161021d565b91505092915050565b60006020828403121561040557600080fd5b6000610413848285016102a0565b91505092915050565b60006020828403121561042e57600080fd5b600061043c848285016102b5565b91505092915050565b60006020828403121561045757600080fd5b600082013567ffffffffffffffff81111561047157600080fd5b61047d848285016102ca565b91505092915050565b60006020828403121561049857600080fd5b60006104a68482850161031e565b91505092915050565b6000602082840312156104c157600080fd5b60006104cf84828501610333565b91505092915050565b6000604051905081810181811067ffffffffffffffff821117156104fb57600080fd5b8060405250919050565b600067ffffffffffffffff82111561051c57600080fd5b602082029050602081019050919050565b600067ffffffffffffffff82111561054457600080fd5b602082029050602081019050919050565b600067ffffffffffffffff82111561056c57600080fd5b601f19601f8301169050602081019050919050565b600061058c826105e5565b9050919050565b60008115159050919050565b60007fffffffff0000000000000000000000000000000000000000000000000000000082169050919050565b60006dffffffffffffffffffffffffffff82169050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000819050919050565b82818337600083830152505050565b61062781610581565b811461063257600080fd5b50565b61063e81610593565b811461064957600080fd5b50565b6106558161059f565b811461066057600080fd5b50565b61066c816105cb565b811461067757600080fd5b50565b61068381610605565b811461068e57600080fd5b5056fea2646970667358221220054c182275ee0c128031b0714b6bc376cc69b01e97c64771e6ac54dd20abc6be64736f6c63430006060033'
    # estimatedGas = Libraries.core.API_EstimateGas_ContractDeployOnly(fromAddress, byteCode, nonce)
    # contractDeploymentTupleList.append((name, byteCode, nonce, estimatedGas))
    # nonce += 1

    # name = 'Exchange_UniswapV2'
    # byteCode = '608060405234801561001057600080fd5b50611c2b806100206000396000f3fe608060405234801561001057600080fd5b50600436106100625760003560e01c8063215029521461006757806337320f12146100c35780634bd66ddf146101495780635400ac31146101e057806368f2bed31461026e578063dcf70368146102fc575b600080fd5b6100a96004803603602081101561007d57600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610382565b604051808215151515815260200191505060405180910390f35b61012f600480360360608110156100d957600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff16906020019092919050505061045f565b604051808215151515815260200191505060405180910390f35b6101ca6004803603604081101561015f57600080fd5b81019080803590602001909291908035906020019064010000000081111561018657600080fd5b82018360208201111561019857600080fd5b803590602001918460208302840111640100000000831117156101ba57600080fd5b9091929391929390505050610546565b6040518082815260200191505060405180910390f35b610258600480360360808110156101f657600080fd5b8101908080359060200190929190803515159060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610817565b6040518082815260200191505060405180910390f35b6102e66004803603608081101561028457600080fd5b8101908080359060200190929190803515159060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610c6d565b6040518082815260200191505060405180910390f35b6103686004803603606081101561031257600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506110c3565b604051808215151515815260200191505060405180910390f35b6000808273ffffffffffffffffffffffffffffffffffffffff16630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b1580156103cb57600080fd5b505afa1580156103df573d6000803e3d6000fd5b505050506040513d60608110156103f557600080fd5b8101908080519060200190929190805190602001909291908051906020019092919050505092505050640100000000428161042c57fe5b0663ffffffff168163ffffffff1614156104545761044a601e6111aa565b600091505061045a565b60019150505b919050565b6000808473ffffffffffffffffffffffffffffffffffffffff166370a08231846040518263ffffffff1660e01b8152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060206040518083038186803b1580156104df57600080fd5b505afa1580156104f3573d6000803e3d6000fd5b505050506040513d602081101561050957600080fd5b81019080805190602001909291905050509050838110156105395761052f601b82611205565b600091505061053f565b60019150505b9392505050565b600080610551611269565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610618576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b606073f164fc0ec4e93095b804a4795bbe1e041497b92a73ffffffffffffffffffffffffffffffffffffffff166338ed17398760018888307f99000000000000000000000000000000000000000000000000000000000000006040518763ffffffff1660e01b815260040180878152602001868152602001806020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018381526020018281038252868682818152602001925060200280828437600081840152601f19601f820116905080830192505050975050505050505050600060405180830381600087803b15801561071d57600080fd5b505af1158015610731573d6000803e3d6000fd5b505050506040513d6000823e3d601f19601f82011682018060405250602081101561075b57600080fd5b810190808051604051939291908464010000000082111561077b57600080fd5b8382019150602082018581111561079157600080fd5b82518660208202830111640100000000821117156107ae57600080fd5b8083526020830192505050908051906020019060200280838360005b838110156107e55780820151818401526020810190506107ca565b5050505090500160405250505090508060018251038151811061080457fe5b6020026020010151925050509392505050565b600080610822611269565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146108e9576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b6000808573ffffffffffffffffffffffffffffffffffffffff16630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b15801561093257600080fd5b505afa158015610946573d6000803e3d6000fd5b505050506040513d606081101561095c57600080fd5b810190808051906020019092919080519060200190929190805190602001909291905050505091509150600086905060008173ffffffffffffffffffffffffffffffffffffffff16630dfe16816040518163ffffffff1660e01b815260040160206040518083038186803b1580156109d357600080fd5b505afa1580156109e7573d6000803e3d6000fd5b505050506040513d60208110156109fd57600080fd5b810190808051906020019092919050505090506000610a3d8b866dffffffffffffffffffffffffffff16866dffffffffffffffffffffffffffff16611291565b9050610a49828a6113c1565b8915610b13578173ffffffffffffffffffffffffffffffffffffffff1663a9059cbb8a8d6040518363ffffffff1660e01b8152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050602060405180830381600087803b158015610ad657600080fd5b505af1158015610aea573d6000803e3d6000fd5b505050506040513d6020811015610b0057600080fd5b8101908080519060200190929190505050505b8273ffffffffffffffffffffffffffffffffffffffff1663022c0d9f6000838b60006040519080825280601f01601f191660200182016040528015610b675781602001600182028036833780820191505090505b506040518563ffffffff1660e01b8152600401808581526020018481526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200180602001828103825283818151815260200191508051906020019080838360005b83811015610bf5578082015181840152602081019050610bda565b50505050905090810190601f168015610c225780820380516001836020036101000a031916815260200191505b5095505050505050600060405180830381600087803b158015610c4457600080fd5b505af1158015610c58573d6000803e3d6000fd5b50505050809650505050505050949350505050565b600080610c78611269565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610d3f576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b6000808573ffffffffffffffffffffffffffffffffffffffff16630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b158015610d8857600080fd5b505afa158015610d9c573d6000803e3d6000fd5b505050506040513d6060811015610db257600080fd5b810190808051906020019092919080519060200190929190805190602001909291905050505091509150600086905060008173ffffffffffffffffffffffffffffffffffffffff1663d21220a76040518163ffffffff1660e01b815260040160206040518083038186803b158015610e2957600080fd5b505afa158015610e3d573d6000803e3d6000fd5b505050506040513d6020811015610e5357600080fd5b810190808051906020019092919050505090506000610e938b856dffffffffffffffffffffffffffff16876dffffffffffffffffffffffffffff16611291565b9050610e9f828a6113c1565b8915610f69578173ffffffffffffffffffffffffffffffffffffffff1663a9059cbb8a8d6040518363ffffffff1660e01b8152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050602060405180830381600087803b158015610f2c57600080fd5b505af1158015610f40573d6000803e3d6000fd5b505050506040513d6020811015610f5657600080fd5b8101908080519060200190929190505050505b8273ffffffffffffffffffffffffffffffffffffffff1663022c0d9f8260008b60006040519080825280601f01601f191660200182016040528015610fbd5781602001600182028036833780820191505090505b506040518563ffffffff1660e01b8152600401808581526020018481526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200180602001828103825283818151815260200191508051906020019080838360005b8381101561104b578082015181840152602081019050611030565b50505050905090810190601f1680156110785780820380516001836020036101000a031916815260200191505b5095505050505050600060405180830381600087803b15801561109a57600080fd5b505af11580156110ae573d6000803e3d6000fd5b50505050809650505050505050949350505050565b6000808473ffffffffffffffffffffffffffffffffffffffff166370a08231846040518263ffffffff1660e01b8152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060206040518083038186803b15801561114357600080fd5b505afa158015611157573d6000803e3d6000fd5b505050506040513d602081101561116d57600080fd5b810190808051906020019092919050505090508381111561119d57611193601b82611205565b60009150506111a3565b60019150505b9392505050565b60006111b4611269565b9050600181600601541415611201577f6afa7c09e54f46a385be04ce7ac1427483e0b51dd6fb76e80786cda402d033b982604051808260ff1660ff16815260200191505060405180910390a15b5050565b600061120f611269565b9050600181600601541415611264577f55f3e3ae10986ee56f72e0f34eb828eab097443abcee79da7499727bbdce81688383604051808360ff1660ff1681526020018281526020019250505060405180910390a15b505050565b60007f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3905090565b60008084116112eb576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401808060200182810382526035815260200180611b1e6035913960400191505060405180910390fd5b6000831180156112fb5750600082115b611350576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401808060200182810382526032815260200180611b536032913960400191505060405180910390fd5b60006113676103e58661168090919063ffffffff16565b9050600061137e848361168090919063ffffffff16565b905060006113a98361139b6103e88961168090919063ffffffff16565b61170690919063ffffffff16565b90508082816113b457fe5b0493505050509392505050565b60008273ffffffffffffffffffffffffffffffffffffffff1663dd62ed3e30846040518363ffffffff1660e01b8152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019250505060206040518083038186803b15801561147457600080fd5b505afa158015611488573d6000803e3d6000fd5b505050506040513d602081101561149e57600080fd5b810190808051906020019092919050505090507fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6bffffffffffffffffffffffff1681101561167b57600081146115a7576115a68363095ea7b360e01b846000604051602401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018260ff16815260200192505050604051602081830303815290604052907bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19166020820180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff838183161783525050505061178e565b5b61167a8363095ea7b360e01b847fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff604051602401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050604051602081830303815290604052907bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19166020820180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff838183161783525050505061178e565b5b505050565b6000808314156116935760009050611700565b60008284029050828482816116a457fe5b04146116fb576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401808060200182810382526021815260200180611bab6021913960400191505060405180910390fd5b809150505b92915050565b600080828401905083811015611784576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252601b8152602001807f536166654d6174683a206164646974696f6e206f766572666c6f77000000000081525060200191505060405180910390fd5b8091505092915050565b60606117f0826040518060400160405280602081526020017f5361666545524332303a206c6f772d6c6576656c2063616c6c206661696c65648152508573ffffffffffffffffffffffffffffffffffffffff1661187d9092919063ffffffff16565b90506000815111156118785780806020019051602081101561181157600080fd5b8101908080519060200190929190505050611877576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252602a815260200180611bcc602a913960400191505060405180910390fd5b5b505050565b606061188c8484600085611895565b90509392505050565b6060824710156118f0576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401808060200182810382526026815260200180611b856026913960400191505060405180910390fd5b6118f985611a3e565b61196b576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252601d8152602001807f416464726573733a2063616c6c20746f206e6f6e2d636f6e747261637400000081525060200191505060405180910390fd5b600060608673ffffffffffffffffffffffffffffffffffffffff1685876040518082805190602001908083835b602083106119bb5780518252602082019150602081019050602083039250611998565b6001836020036101000a03801982511681845116808217855250505050505090500191505060006040518083038185875af1925050503d8060008114611a1d576040519150601f19603f3d011682016040523d82523d6000602084013e611a22565b606091505b5091509150611a32828286611a51565b92505050949350505050565b600080823b905060008111915050919050565b60608315611a6157829050611b16565b600083511115611a745782518084602001fd5b816040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825283818151815260200191508051906020019080838360005b83811015611adb578082015181840152602081019050611ac0565b50505050905090810190601f168015611b085780820380516001836020036101000a031916815260200191505b509250505060405180910390fd5b939250505056fe636f6e76657274416d6f756e74496e546f416d6f756e744f75743a20494e53554646494349454e545f494e5055545f414d4f554e54636f6e76657274416d6f756e74496e546f416d6f756e744f75743a20494e53554646494349454e545f4c4951554944495459416464726573733a20696e73756666696369656e742062616c616e636520666f722063616c6c536166654d6174683a206d756c7469706c69636174696f6e206f766572666c6f775361666545524332303a204552433230206f7065726174696f6e20646964206e6f742073756363656564a2646970667358221220b530594495a09c1f81d7a9be5708d0bd344ef37cb52adb8ed40c76172d00d6f964736f6c63430006040033'
    # # For some reason I cannot get Libraries.core.API_EstimateGas to work with contract deployments so I'm using web3's estimateGas feature
    # estimatedGas = Libraries.nodes.Instance_Web3.eth.estimateGas({"from": fromAddress, "nonce": nonce, "data": byteCode, })
    # contractDeploymentTupleList.append((name, byteCode, nonce, estimatedGas))
    # nonce += 1

    # name = 'Exchange_Balancer'
    # byteCode = '608060405234801561001057600080fd5b506113ff806100206000396000f3fe608060405234801561001057600080fd5b506004361061004c5760003560e01c806340f33955146100515780634a68197514610081578063e6582ba5146100b1578063ff9cc60e146100e1575b600080fd5b61006b60048036038101906100669190610b6d565b610111565b60405161007891906111de565b60405180910390f35b61009b60048036038101906100969190610acc565b610284565b6040516100a89190611121565b60405180910390f35b6100cb60048036038101906100c69190610acc565b61033c565b6040516100d89190611121565b60405180910390f35b6100fb60048036038101906100f69190610bd0565b6103f4565b60405161010891906111de565b60405180910390f35b60008061011c610549565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146101b0576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016101a79061115e565b60405180910390fd5b6101ba8486610571565b60008573ffffffffffffffffffffffffffffffffffffffff16638201aa3f86898760017fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6040518663ffffffff1660e01b815260040161021e959493929190611072565b6040805180830381600087803b15801561023757600080fd5b505af115801561024b573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061026f9190610c62565b80925081945050508292505050949350505050565b6000808473ffffffffffffffffffffffffffffffffffffffff166370a08231846040518263ffffffff1660e01b81526004016102c09190610fdc565b60206040518083038186803b1580156102d857600080fd5b505afa1580156102ec573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906103109190610b44565b90508381101561032f57610325601c82610768565b6000915050610335565b60019150505b9392505050565b6000808473ffffffffffffffffffffffffffffffffffffffff166370a08231846040518263ffffffff1660e01b81526004016103789190610fdc565b60206040518083038186803b15801561039057600080fd5b505afa1580156103a4573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906103c89190610b44565b9050838111156103e7576103dd601c82610768565b60009150506103ed565b60019150505b9392505050565b6000806103ff610549565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610493576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161048a9061115e565b60405180910390fd5b736317c5e82a06e1d8bf200d21f4510ac2c038ac8173ffffffffffffffffffffffffffffffffffffffff1663f4378240888888888d896040518763ffffffff1660e01b81526004016104ea969594939291906110c5565b602060405180830381600087803b15801561050457600080fd5b505af1158015610518573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061053c9190610b44565b9150509695505050505050565b60007f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3905090565b60008273ffffffffffffffffffffffffffffffffffffffff1663dd62ed3e30846040518363ffffffff1660e01b81526004016105ae929190610ff7565b60206040518083038186803b1580156105c657600080fd5b505afa1580156105da573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906105fe9190610b44565b90507fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6bffffffffffffffffffffffff1681101561076357600081146106c1576106c08363095ea7b360e01b84600060405160240161065e929190611020565b604051602081830303815290604052907bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19166020820180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff83818316178352505050506107c0565b5b6107628363095ea7b360e01b847fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff604051602401610700929190611049565b604051602081830303815290604052907bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19166020820180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff83818316178352505050506107c0565b5b505050565b6000610772610549565b90506001816006015414156107bb577f55f3e3ae10986ee56f72e0f34eb828eab097443abcee79da7499727bbdce816883836040516107b29291906111f9565b60405180910390a15b505050565b6060610822826040518060400160405280602081526020017f5361666545524332303a206c6f772d6c6576656c2063616c6c206661696c65648152508573ffffffffffffffffffffffffffffffffffffffff166108879092919063ffffffff16565b905060008151111561088257808060200190518101906108429190610b1b565b610881576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610878906111be565b60405180910390fd5b5b505050565b6060610896848460008561089f565b90509392505050565b6060824710156108e4576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016108db9061117e565b60405180910390fd5b6108ed856109b4565b61092c576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016109239061119e565b60405180910390fd5b600060608673ffffffffffffffffffffffffffffffffffffffff1685876040516109569190610fc5565b60006040518083038185875af1925050503d8060008114610993576040519150601f19603f3d011682016040523d82523d6000602084013e610998565b606091505b50915091506109a88282866109c7565b92505050949350505050565b600080823b905060008111915050919050565b606083156109d757829050610a27565b6000835111156109ea5782518084602001fd5b816040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610a1e919061113c565b60405180910390fd5b9392505050565b600081359050610a3d81611384565b92915050565b60008083601f840112610a5557600080fd5b8235905067ffffffffffffffff811115610a6e57600080fd5b602083019150836080820283011115610a8657600080fd5b9250929050565b600081519050610a9c8161139b565b92915050565b600081359050610ab1816113b2565b92915050565b600081519050610ac6816113b2565b92915050565b600080600060608486031215610ae157600080fd5b6000610aef86828701610a2e565b9350506020610b0086828701610aa2565b9250506040610b1186828701610a2e565b9150509250925092565b600060208284031215610b2d57600080fd5b6000610b3b84828501610a8d565b91505092915050565b600060208284031215610b5657600080fd5b6000610b6484828501610ab7565b91505092915050565b60008060008060808587031215610b8357600080fd5b6000610b9187828801610aa2565b9450506020610ba287828801610a2e565b9350506040610bb387828801610a2e565b9250506060610bc487828801610a2e565b91505092959194509250565b60008060008060008060a08789031215610be957600080fd5b6000610bf789828a01610aa2565b965050602087013567ffffffffffffffff811115610c1457600080fd5b610c2089828a01610a43565b95509550506040610c3389828a01610a2e565b9350506060610c4489828a01610a2e565b9250506080610c5589828a01610aa2565b9150509295509295509295565b60008060408385031215610c7557600080fd5b6000610c8385828601610ab7565b9250506020610c9485828601610ab7565b9150509250929050565b6000610caa8383610f23565b60808301905092915050565b610cbf816112b5565b82525050565b610cce816112b5565b82525050565b6000610ce0838561124f565b9350610ceb82611222565b8060005b85811015610d2457610d018284611293565b610d0b8882610c9e565b9750610d1683611242565b925050600181019050610cef565b5085925050509392505050565b610d3a816112c7565b82525050565b6000610d4b8261122c565b610d558185611260565b9350610d65818560208601611340565b80840191505092915050565b610d7a8161130a565b82525050565b610d898161131c565b82525050565b610d988161132e565b82525050565b6000610da982611237565b610db3818561126b565b9350610dc3818560208601611340565b610dcc81611373565b840191505092915050565b6000610de4600d8361126b565b91507f4d757374206265204e696e6a61000000000000000000000000000000000000006000830152602082019050919050565b6000610e2460268361126b565b91507f416464726573733a20696e73756666696369656e742062616c616e636520666f60008301527f722063616c6c00000000000000000000000000000000000000000000000000006020830152604082019050919050565b6000610e8a601d8361126b565b91507f416464726573733a2063616c6c20746f206e6f6e2d636f6e74726163740000006000830152602082019050919050565b6000610eca602a8361126b565b91507f5361666545524332303a204552433230206f7065726174696f6e20646964206e60008301527f6f742073756363656564000000000000000000000000000000000000000000006020830152604082019050919050565b60808201610f34600083018361127c565b610f416000850182610cb6565b50610f4f602083018361129e565b610f5c6020850182610f98565b50610f6a604083018361129e565b610f776040850182610f98565b50610f85606083018361129e565b610f926060850182610f98565b50505050565b610fa1816112f3565b82525050565b610fb0816112f3565b82525050565b610fbf816112fd565b82525050565b6000610fd18284610d40565b915081905092915050565b6000602082019050610ff16000830184610cc5565b92915050565b600060408201905061100c6000830185610cc5565b6110196020830184610cc5565b9392505050565b60006040820190506110356000830185610cc5565b6110426020830184610d71565b9392505050565b600060408201905061105e6000830185610cc5565b61106b6020830184610fa7565b9392505050565b600060a0820190506110876000830188610cc5565b6110946020830187610fa7565b6110a16040830186610cc5565b6110ae6060830185610d8f565b6110bb6080830184610d80565b9695505050505050565b600060a08201905081810360008301526110e081888a610cd4565b90506110ef6020830187610cc5565b6110fc6040830186610cc5565b6111096060830185610fa7565b6111166080830184610fa7565b979650505050505050565b60006020820190506111366000830184610d31565b92915050565b600060208201905081810360008301526111568184610d9e565b905092915050565b6000602082019050818103600083015261117781610dd7565b9050919050565b6000602082019050818103600083015261119781610e17565b9050919050565b600060208201905081810360008301526111b781610e7d565b9050919050565b600060208201905081810360008301526111d781610ebd565b9050919050565b60006020820190506111f36000830184610fa7565b92915050565b600060408201905061120e6000830185610fb6565b61121b6020830184610fa7565b9392505050565b6000819050919050565b600081519050919050565b600081519050919050565b6000608082019050919050565b600082825260208201905092915050565b600081905092915050565b600082825260208201905092915050565b600061128b6020840184610a2e565b905092915050565b600082905092915050565b60006112ad6020840184610aa2565b905092915050565b60006112c0826112d3565b9050919050565b60008115159050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000819050919050565b600060ff82169050919050565b6000611315826112fd565b9050919050565b6000611327826112f3565b9050919050565b6000611339826112f3565b9050919050565b60005b8381101561135e578082015181840152602081019050611343565b8381111561136d576000848401525b50505050565b6000601f19601f8301169050919050565b61138d816112b5565b811461139857600080fd5b50565b6113a4816112c7565b81146113af57600080fd5b50565b6113bb816112f3565b81146113c657600080fd5b5056fea264697066735822122081ee275f4bba796fc74dbd2f8361e99c0db550bdb64fcf26cf80dc9b2fd296bd64736f6c63430006040033'
    # gas = 1405017
    # contractDeploymentTupleList.append((name, byteCode, nonce, gas))
    # nonce += 1
    #
    # name = 'Exchange_Uniswap'
    # byteCode = '608060405234801561001057600080fd5b5061068c806100206000396000f3fe608060405234801561001057600080fd5b506004361061004c5760003560e01c80631d632f0314610051578063921bd001146100b3578063d605428714610119578063ec15c7771461017b575b600080fd5b61009d6004803603604081101561006757600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506101e1565b6040518082815260200191505060405180910390f35b6100ff600480360360408110156100c957600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506102e9565b604051808215151515815260200191505060405180910390f35b6101656004803603604081101561012f57600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff16906020019092919050505061032f565b6040518082815260200191505060405180910390f35b6101c76004803603604081101561019157600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610437565b604051808215151515815260200191505060405180910390f35b6000806101ec61047d565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146102b3576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b6102e0838560017f99000000000000000000000000000000000000000000000000000000000000006104a5565b91505092915050565b6000808273ffffffffffffffffffffffffffffffffffffffff163190508381101561032357610319600a82610548565b6000915050610329565b60019150505b92915050565b60008061033a61047d565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610401576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b61042e838560017f99000000000000000000000000000000000000000000000000000000000000006105ac565b91505092915050565b6000808273ffffffffffffffffffffffffffffffffffffffff163190508381111561047157610467600a82610548565b6000915050610477565b60019150505b92915050565b60007f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3905090565b60008473ffffffffffffffffffffffffffffffffffffffff1663f39b5b9b8585856040518463ffffffff1660e01b815260040180838152602001828152602001925050506020604051808303818588803b15801561050257600080fd5b505af1158015610516573d6000803e3d6000fd5b50505050506040513d602081101561052d57600080fd5b81019080805190602001909291905050509050949350505050565b600061055261047d565b90506001816006015414156105a7577f55f3e3ae10986ee56f72e0f34eb828eab097443abcee79da7499727bbdce81688383604051808360ff1660ff1681526020018281526020019250505060405180910390a15b505050565b60008473ffffffffffffffffffffffffffffffffffffffff166395e3c50b8585856040518463ffffffff1660e01b8152600401808481526020018381526020018281526020019350505050602060405180830381600087803b15801561061157600080fd5b505af1158015610625573d6000803e3d6000fd5b505050506040513d602081101561063b57600080fd5b8101908080519060200190929190505050905094935050505056fea2646970667358221220474afc84c01e6a681c9336bf46bf365af9c457e2985114a53c4e3af3a006505064736f6c63430006040033'
    # gas = 499854
    # contractDeploymentTupleList.append((name, byteCode, nonce, gas))
    # nonce += 1
    #
    # name = 'Exchange_Sushiswap'
    # byteCode = '608060405234801561001057600080fd5b506119e9806100206000396000f3fe608060405234801561001057600080fd5b50600436106100625760003560e01c806316f115fd1461006757806329dd79ee146100c95780634dfd434d1461014f5780639a35b2b5146101ab578063c4bb430a14610231578063fd0616d614610293575b600080fd5b6100b36004803603604081101561007d57600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff16906020019092919050505061032a565b6040518082815260200191505060405180910390f35b610135600480360360608110156100df57600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610836565b604051808215151515815260200191505060405180910390f35b6101916004803603602081101561016557600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919050505061091d565b604051808215151515815260200191505060405180910390f35b610217600480360360608110156101c157600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506109fa565b604051808215151515815260200191505060405180910390f35b61027d6004803603604081101561024757600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610ae1565b6040518082815260200191505060405180910390f35b610314600480360360408110156102a957600080fd5b8101908080359060200190929190803590602001906401000000008111156102d057600080fd5b8201836020820111156102e257600080fd5b8035906020019184602083028401116401000000008311171561030457600080fd5b9091929391929390505050610fed565b6040518082815260200191505060405180910390f35b6000806103356112be565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146103fc576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b6000808473ffffffffffffffffffffffffffffffffffffffff16630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b15801561044557600080fd5b505afa158015610459573d6000803e3d6000fd5b505050506040513d606081101561046f57600080fd5b81019080805190602001909291908051906020019092919080519060200190929190505050509150915060006104c687836dffffffffffffffffffffffffffff16856dffffffffffffffffffffffffffff166112e6565b9050600086905060008173ffffffffffffffffffffffffffffffffffffffff1663d21220a76040518163ffffffff1660e01b815260040160206040518083038186803b15801561051557600080fd5b505afa158015610529573d6000803e3d6000fd5b505050506040513d602081101561053f57600080fd5b8101908080519060200190929190505050905060008273ffffffffffffffffffffffffffffffffffffffff16630dfe16816040518163ffffffff1660e01b815260040160206040518083038186803b15801561059a57600080fd5b505afa1580156105ae573d6000803e3d6000fd5b505050506040513d60208110156105c457600080fd5b810190808051906020019092919050505090506105e1828a611416565b60008173ffffffffffffffffffffffffffffffffffffffff166370a08231306040518263ffffffff1660e01b8152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060206040518083038186803b15801561066057600080fd5b505afa158015610674573d6000803e3d6000fd5b505050506040513d602081101561068a57600080fd5b810190808051906020019092919050505090508373ffffffffffffffffffffffffffffffffffffffff1663022c0d9f866000306040518463ffffffff1660e01b8152600401808481526020018381526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200180602001828103825260008152602001602001945050505050600060405180830381600087803b15801561074157600080fd5b505af1158015610755573d6000803e3d6000fd5b50505050610823818373ffffffffffffffffffffffffffffffffffffffff166370a08231306040518263ffffffff1660e01b8152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060206040518083038186803b1580156107da57600080fd5b505afa1580156107ee573d6000803e3d6000fd5b505050506040513d602081101561080457600080fd5b81019080805190602001909291905050506116f290919063ffffffff16565b9850889850505050505050505092915050565b6000808473ffffffffffffffffffffffffffffffffffffffff166370a08231846040518263ffffffff1660e01b8152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060206040518083038186803b1580156108b657600080fd5b505afa1580156108ca573d6000803e3d6000fd5b505050506040513d60208110156108e057600080fd5b810190808051906020019092919050505090508381111561091057610906601b82611775565b6000915050610916565b60019150505b9392505050565b6000808273ffffffffffffffffffffffffffffffffffffffff16630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b15801561096657600080fd5b505afa15801561097a573d6000803e3d6000fd5b505050506040513d606081101561099057600080fd5b810190808051906020019092919080519060200190929190805190602001909291905050509250505064010000000042816109c757fe5b0663ffffffff168163ffffffff1614156109ef576109e5601e6117d9565b60009150506109f5565b60019150505b919050565b6000808473ffffffffffffffffffffffffffffffffffffffff166370a08231846040518263ffffffff1660e01b8152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060206040518083038186803b158015610a7a57600080fd5b505afa158015610a8e573d6000803e3d6000fd5b505050506040513d6020811015610aa457600080fd5b8101908080519060200190929190505050905083811015610ad457610aca601b82611775565b6000915050610ada565b60019150505b9392505050565b600080610aec6112be565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610bb3576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b6000808473ffffffffffffffffffffffffffffffffffffffff16630902f1ac6040518163ffffffff1660e01b815260040160606040518083038186803b158015610bfc57600080fd5b505afa158015610c10573d6000803e3d6000fd5b505050506040513d6060811015610c2657600080fd5b8101908080519060200190929190805190602001909291908051906020019092919050505050915091506000610c7d87846dffffffffffffffffffffffffffff16846dffffffffffffffffffffffffffff166112e6565b9050600086905060008173ffffffffffffffffffffffffffffffffffffffff16630dfe16816040518163ffffffff1660e01b815260040160206040518083038186803b158015610ccc57600080fd5b505afa158015610ce0573d6000803e3d6000fd5b505050506040513d6020811015610cf657600080fd5b8101908080519060200190929190505050905060008273ffffffffffffffffffffffffffffffffffffffff1663d21220a76040518163ffffffff1660e01b815260040160206040518083038186803b158015610d5157600080fd5b505afa158015610d65573d6000803e3d6000fd5b505050506040513d6020811015610d7b57600080fd5b81019080805190602001909291905050509050610d98828a611416565b60008173ffffffffffffffffffffffffffffffffffffffff166370a08231306040518263ffffffff1660e01b8152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060206040518083038186803b158015610e1757600080fd5b505afa158015610e2b573d6000803e3d6000fd5b505050506040513d6020811015610e4157600080fd5b810190808051906020019092919050505090508373ffffffffffffffffffffffffffffffffffffffff1663022c0d9f600087306040518463ffffffff1660e01b8152600401808481526020018381526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200180602001828103825260008152602001602001945050505050600060405180830381600087803b158015610ef857600080fd5b505af1158015610f0c573d6000803e3d6000fd5b50505050610fda818373ffffffffffffffffffffffffffffffffffffffff166370a08231306040518263ffffffff1660e01b8152600401808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060206040518083038186803b158015610f9157600080fd5b505afa158015610fa5573d6000803e3d6000fd5b505050506040513d6020811015610fbb57600080fd5b81019080805190602001909291905050506116f290919063ffffffff16565b9850889850505050505050505092915050565b600080610ff86112be565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146110bf576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b606073d9e1ce17f2641f24ae83637ab66a2cca9c378b9f73ffffffffffffffffffffffffffffffffffffffff166338ed17398760018888307f99000000000000000000000000000000000000000000000000000000000000006040518763ffffffff1660e01b815260040180878152602001868152602001806020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018381526020018281038252868682818152602001925060200280828437600081840152601f19601f820116905080830192505050975050505050505050600060405180830381600087803b1580156111c457600080fd5b505af11580156111d8573d6000803e3d6000fd5b505050506040513d6000823e3d601f19601f82011682018060405250602081101561120257600080fd5b810190808051604051939291908464010000000082111561122257600080fd5b8382019150602082018581111561123857600080fd5b825186602082028301116401000000008211171561125557600080fd5b8083526020830192505050908051906020019060200280838360005b8381101561128c578082015181840152602081019050611271565b505050509050016040525050509050806001825103815181106112ab57fe5b6020026020010151925050509392505050565b60007f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3905090565b6000808411611340576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252603581526020018061194d6035913960400191505060405180910390fd5b6000831180156113505750600082115b6113a5576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260328152602001806119826032913960400191505060405180910390fd5b60006113bc6103e58661183490919063ffffffff16565b905060006113d3848361183490919063ffffffff16565b905060006113fe836113f06103e88961183490919063ffffffff16565b6118c990919063ffffffff16565b905080828161140957fe5b0493505050509392505050565b60008273ffffffffffffffffffffffffffffffffffffffff1663dd62ed3e30846040518363ffffffff1660e01b8152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019250505060206040518083038186803b1580156114c957600080fd5b505afa1580156114dd573d6000803e3d6000fd5b505050506040513d60208110156114f357600080fd5b810190808051906020019092919050505090507fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6bffffffffffffffffffffffff168110156116ed5760008114611609578273ffffffffffffffffffffffffffffffffffffffff1663095ea7b38360006040518363ffffffff1660e01b8152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050602060405180830381600087803b1580156115cc57600080fd5b505af11580156115e0573d6000803e3d6000fd5b505050506040513d60208110156115f657600080fd5b8101908080519060200190929190505050505b8273ffffffffffffffffffffffffffffffffffffffff1663095ea7b3837fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6040518363ffffffff1660e01b8152600401808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200182815260200192505050602060405180830381600087803b1580156116b057600080fd5b505af11580156116c4573d6000803e3d6000fd5b505050506040513d60208110156116da57600080fd5b8101908080519060200190929190505050505b505050565b600082828403915081111561176f576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260158152602001807f64732d6d6174682d7375622d756e646572666c6f77000000000000000000000081525060200191505060405180910390fd5b92915050565b600061177f6112be565b90506001816006015414156117d4577f55f3e3ae10986ee56f72e0f34eb828eab097443abcee79da7499727bbdce81688383604051808360ff1660ff1681526020018281526020019250505060405180910390a15b505050565b60006117e36112be565b9050600181600601541415611830577f6afa7c09e54f46a385be04ce7ac1427483e0b51dd6fb76e80786cda402d033b982604051808260ff1660ff16815260200191505060405180910390a15b5050565b600080821480611851575082828385029250828161184e57fe5b04145b6118c3576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260148152602001807f64732d6d6174682d6d756c2d6f766572666c6f7700000000000000000000000081525060200191505060405180910390fd5b92915050565b6000828284019150811015611946576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260148152602001807f64732d6d6174682d6164642d6f766572666c6f7700000000000000000000000081525060200191505060405180910390fd5b9291505056fe636f6e76657274416d6f756e74496e546f416d6f756e744f75743a20494e53554646494349454e545f494e5055545f414d4f554e54636f6e76657274416d6f756e74496e546f416d6f756e744f75743a20494e53554646494349454e545f4c4951554944495459a264697066735822122084357b4d5d3f06c141d58b75c68033bec38a19563693e9a1148abecaacb8293664736f6c63430006040033'
    # gas = 1890475
    # contractDeploymentTupleList.append((name, byteCode, nonce, gas))
    # nonce += 1
    #
    # name = 'Exchange_0xv3'
    # byteCode = '608060405234801561001057600080fd5b50611736806100206000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c80635fccaec31461004657806365a558b214610076578063dee67f0b146100a6575b600080fd5b610060600480360381019061005b9190610e6d565b6100d6565b60405161006d9190611440565b60405180910390f35b610090600480360381019061008b9190610e06565b6101a4565b60405161009d919061137e565b60405180910390f35b6100c060048036038101906100bb9190610e06565b6103ce565b6040516100cd919061137e565b60405180910390f35b6000806100e1610601565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610175576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161016c90611399565b60405180910390fd5b61017d61094d565b61019285878661018d6001610629565b61064b565b90508060000151925050509392505050565b6000806101af610601565b905060008590506101be61097c565b8260020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16639d3fa4b9866040518263ffffffff1660e01b815260040161021b91906113d9565b60606040518083038186803b15801561023357600080fd5b505afa158015610247573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061026b9190610db4565b90506003600681111561027a57fe5b60ff16816000015160ff16146102a057610294600361071e565b600093505050506103c7565b600081604001518660a001350390506102ba601282610773565b6102c488826107cb565b9750828810156102e5576102d8601861071e565b60009450505050506103c7565b60008773ffffffffffffffffffffffffffffffffffffffff166370a082318860000160208101906103169190610d62565b6040518263ffffffff1660e01b81526004016103329190611363565b60206040518083038186803b15801561034a57600080fd5b505afa15801561035e573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906103829190610ddd565b905060006103998860a0013589608001358c6107e4565b9050808210156103bc576103ad601961071e565b600096505050505050506103c7565b600196505050505050505b9392505050565b6000806103d9610601565b905060008590506103e861097c565b8260020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16639d3fa4b9866040518263ffffffff1660e01b815260040161044591906113d9565b60606040518083038186803b15801561045d57600080fd5b505afa158015610471573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906104959190610db4565b9050600360068111156104a457fe5b60ff16816000015160ff16146104ca576104be600361071e565b600093505050506105fa565b600081604001518660a0013503905060006104ee87608001358860a0013584610802565b90506104fc8161270f610820565b905061050889826107cb565b98508389101561052a5761051c601861071e565b6000955050505050506105fa565b6105ce898973ffffffffffffffffffffffffffffffffffffffff166370a082318a600001602081019061055d9190610d62565b6040518263ffffffff1660e01b81526004016105799190611363565b60206040518083038186803b15801561059157600080fd5b505afa1580156105a5573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906105c99190610ddd565b6107cb565b9850838910156105f0576105e2601961071e565b6000955050505050506105fa565b6001955050505050505b9392505050565b60007f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3905090565b600061064461063b620111703a61084d565b8360ff1661084d565b9050919050565b61065361094d565b600061065d610601565b90508060020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16639b44d556848888886040518563ffffffff1660e01b81526004016106c1939291906113fb565b60a0604051808303818588803b1580156106da57600080fd5b505af11580156106ee573d6000803e3d6000fd5b50505050506040513d601f19601f820116820180604052508101906107139190610d8b565b915050949350505050565b6000610728610601565b905060018160060154141561076f577f6afa7c09e54f46a385be04ce7ac1427483e0b51dd6fb76e80786cda402d033b982604051610766919061145b565b60405180910390a15b5050565b600061077d610601565b90506001816006015414156107c6577f55f3e3ae10986ee56f72e0f34eb828eab097443abcee79da7499727bbdce816883836040516107bd929190611476565b60405180910390a15b505050565b60008183106107da57816107dc565b825b905092915050565b60006107f96107f3838561084d565b856108d2565b90509392505050565b6000610817610811838661084d565b846108d2565b90509392505050565b600061082c838361084d565b92506108428361083d8460016108eb565b6108d2565b925082905092915050565b60008083141561086057600090506108cc565b600082840290508284828161087157fe5b041461087c57600080fd5b8284828161088657fe5b04146108c7576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016108be906113b9565b60405180910390fd5b809150505b92915050565b6000808284816108de57fe5b0490508091505092915050565b60008082840190508381101561090057600080fd5b83811015610943576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161093a906113b9565b60405180910390fd5b8091505092915050565b6040518060a0016040528060008152602001600081526020016000815260200160008152602001600081525090565b6040518060600160405280600060ff16815260200160008019168152602001600081525090565b6000813590506109b2816116a4565b92915050565b6000815190506109c7816116bb565b92915050565b600082601f8301126109de57600080fd5b81356109f16109ec826114cc565b61149f565b91508082526020830160208301858383011115610a0d57600080fd5b610a18838284611651565b50505092915050565b600082601f830112610a3257600080fd5b8135610a45610a40826114f8565b61149f565b91508082526020830160208301858383011115610a6157600080fd5b610a6c838284611651565b50505092915050565b600060a08284031215610a8757600080fd5b610a9160a061149f565b90506000610aa184828501610d38565b6000830152506020610ab584828501610d38565b6020830152506040610ac984828501610d38565b6040830152506060610add84828501610d38565b6060830152506080610af184828501610d38565b60808301525092915050565b600060608284031215610b0f57600080fd5b610b19606061149f565b90506000610b2984828501610d4d565b6000830152506020610b3d848285016109b8565b6020830152506040610b5184828501610d38565b60408301525092915050565b60006101c08284031215610b7057600080fd5b81905092915050565b60006101c08284031215610b8c57600080fd5b610b976101c061149f565b90506000610ba7848285016109a3565b6000830152506020610bbb848285016109a3565b6020830152506040610bcf848285016109a3565b6040830152506060610be3848285016109a3565b6060830152506080610bf784828501610d23565b60808301525060a0610c0b84828501610d23565b60a08301525060c0610c1f84828501610d23565b60c08301525060e0610c3384828501610d23565b60e083015250610100610c4884828501610d23565b61010083015250610120610c5e84828501610d23565b6101208301525061014082013567ffffffffffffffff811115610c8057600080fd5b610c8c848285016109cd565b6101408301525061016082013567ffffffffffffffff811115610cae57600080fd5b610cba848285016109cd565b6101608301525061018082013567ffffffffffffffff811115610cdc57600080fd5b610ce8848285016109cd565b610180830152506101a082013567ffffffffffffffff811115610d0a57600080fd5b610d16848285016109cd565b6101a08301525092915050565b600081359050610d32816116d2565b92915050565b600081519050610d47816116d2565b92915050565b600081519050610d5c816116e9565b92915050565b600060208284031215610d7457600080fd5b6000610d82848285016109a3565b91505092915050565b600060a08284031215610d9d57600080fd5b6000610dab84828501610a75565b91505092915050565b600060608284031215610dc657600080fd5b6000610dd484828501610afd565b91505092915050565b600060208284031215610def57600080fd5b6000610dfd84828501610d38565b91505092915050565b600080600060608486031215610e1b57600080fd5b6000610e2986828701610d23565b9350506020610e3a868287016109a3565b925050604084013567ffffffffffffffff811115610e5757600080fd5b610e6386828701610b5d565b9150509250925092565b600080600060608486031215610e8257600080fd5b6000610e9086828701610d23565b935050602084013567ffffffffffffffff811115610ead57600080fd5b610eb986828701610b79565b925050604084013567ffffffffffffffff811115610ed657600080fd5b610ee286828701610a21565b9150509250925092565b610ef5816115f2565b82525050565b610f04816115f2565b82525050565b610f1381611604565b82525050565b6000610f25838561153a565b9350610f32838584611651565b610f3b83611693565b840190509392505050565b6000610f518261152f565b610f5b818561154b565b9350610f6b818560208601611660565b610f7481611693565b840191505092915050565b6000610f8a82611524565b610f94818561153a565b9350610fa4818560208601611660565b610fad81611693565b840191505092915050565b6000610fc5600d8361155c565b91507f4d757374206265204e696e6a61000000000000000000000000000000000000006000830152602082019050919050565b600061100560108361155c565b91507f55494e543235365f4f564552464c4f57000000000000000000000000000000006000830152602082019050919050565b60006101c0830161104c600084018461156d565b6110596000860182610eec565b50611067602084018461156d565b6110746020860182610eec565b50611082604084018461156d565b61108f6040860182610eec565b5061109d606084018461156d565b6110aa6060860182610eec565b506110b860808401846115db565b6110c56080860182611336565b506110d360a08401846115db565b6110e060a0860182611336565b506110ee60c08401846115db565b6110fb60c0860182611336565b5061110960e08401846115db565b61111660e0860182611336565b506111256101008401846115db565b611133610100860182611336565b506111426101208401846115db565b611150610120860182611336565b5061115f610140840184611584565b858303610140870152611173838284610f19565b92505050611185610160840184611584565b858303610160870152611199838284610f19565b925050506111ab610180840184611584565b8583036101808701526111bf838284610f19565b925050506111d16101a0840184611584565b8583036101a08701526111e5838284610f19565b925050508091505092915050565b60006101c08301600083015161120c6000860182610eec565b50602083015161121f6020860182610eec565b5060408301516112326040860182610eec565b5060608301516112456060860182610eec565b5060808301516112586080860182611336565b5060a083015161126b60a0860182611336565b5060c083015161127e60c0860182611336565b5060e083015161129160e0860182611336565b506101008301516112a6610100860182611336565b506101208301516112bb610120860182611336565b506101408301518482036101408601526112d58282610f7f565b9150506101608301518482036101608601526112f18282610f7f565b91505061018083015184820361018086015261130d8282610f7f565b9150506101a08301518482036101a08601526113298282610f7f565b9150508091505092915050565b61133f8161163a565b82525050565b61134e8161163a565b82525050565b61135d81611644565b82525050565b60006020820190506113786000830184610efb565b92915050565b60006020820190506113936000830184610f0a565b92915050565b600060208201905081810360008301526113b281610fb8565b9050919050565b600060208201905081810360008301526113d281610ff8565b9050919050565b600060208201905081810360008301526113f38184611038565b905092915050565b6000606082019050818103600083015261141581866111f3565b90506114246020830185611345565b81810360408301526114368184610f46565b9050949350505050565b60006020820190506114556000830184611345565b92915050565b60006020820190506114706000830184611354565b92915050565b600060408201905061148b6000830185611354565b6114986020830184611345565b9392505050565b6000604051905081810181811067ffffffffffffffff821117156114c257600080fd5b8060405250919050565b600067ffffffffffffffff8211156114e357600080fd5b601f19601f8301169050602081019050919050565b600067ffffffffffffffff82111561150f57600080fd5b601f19601f8301169050602081019050919050565b600081519050919050565b600081519050919050565b600082825260208201905092915050565b600082825260208201905092915050565b600082825260208201905092915050565b600061157c60208401846109a3565b905092915050565b6000808335600160200384360303811261159d57600080fd5b83810192508235915060208301925067ffffffffffffffff8211156115c157600080fd5b6001820236038413156115d357600080fd5b509250929050565b60006115ea6020840184610d23565b905092915050565b60006115fd8261161a565b9050919050565b60008115159050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000819050919050565b600060ff82169050919050565b82818337600083830152505050565b60005b8381101561167e578082015181840152602081019050611663565b8381111561168d576000848401525b50505050565b6000601f19601f8301169050919050565b6116ad816115f2565b81146116b857600080fd5b50565b6116c481611610565b81146116cf57600080fd5b50565b6116db8161163a565b81146116e657600080fd5b50565b6116f281611644565b81146116fd57600080fd5b5056fea264697066735822122069e84168ef8b20b8976703e832a970eaad82b66d50c5cbbc7c8b587f2fd4b05b64736f6c63430006040033'
    # gas = 1637490
    # contractDeploymentTupleList.append((name, byteCode, nonce, gas))
    # nonce += 1
    #
    # name = 'Exchange_Kyber'
    # byteCode = '608060405234801561001057600080fd5b50610966806100206000396000f3fe608060405234801561001057600080fd5b506004361061004c5760003560e01c80632aa06d29146100515780636727fbb9146100e15780639c6b7f9e14610143578063b72b86fa146101a5575b600080fd5b6100c76004803603608081101561006757600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190505050610235565b604051808215151515815260200191505060405180910390f35b61012d600480360360408110156100f757600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610375565b6040518082815260200191505060405180910390f35b61018f6004803603604081101561015957600080fd5b8101908080359060200190929190803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610495565b6040518082815260200191505060405180910390f35b61021b600480360360808110156101bb57600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190803590602001909291905050506105b6565b604051808215151515815260200191505060405180910390f35b6000808373ffffffffffffffffffffffffffffffffffffffff16637cd4427273eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8789436040518563ffffffff1660e01b8152600401808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200183815260200182815260200194505050505060206040518083038186803b15801561030d57600080fd5b505afa158015610321573d6000803e3d6000fd5b505050506040513d602081101561033757600080fd5b81019080805190602001909291905050509050828110156103675761035d6007826106f6565b600091505061036d565b60019150505b949350505050565b60008061038061075a565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610447576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b61048c8473eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee86867f990000000000000000000000000000000000000000000000000000000000000060016000610782565b91505092915050565b6000806104a061075a565b905080600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610567576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600d8152602001807f4d757374206265204e696e6a610000000000000000000000000000000000000081525060200191505060405180910390fd5b6105ad6000848673eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee7f990000000000000000000000000000000000000000000000000000000000000060016000610782565b91505092915050565b6000808473ffffffffffffffffffffffffffffffffffffffff16637cd442728773eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee87436040518563ffffffff1660e01b8152600401808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200183815260200182815260200194505050505060206040518083038186803b15801561068e57600080fd5b505afa1580156106a2573d6000803e3d6000fd5b505050506040513d60208110156106b857600080fd5b81019080805190602001909291905050509050828110156106e8576106de6007826106f6565b60009150506106ee565b60019150505b949350505050565b600061070061075a565b9050600181600601541415610755577f55f3e3ae10986ee56f72e0f34eb828eab097443abcee79da7499727bbdce81688383604051808360ff1660ff1681526020018281526020019250505060405180910390a15b505050565b60007f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3905090565b60008061078d61075a565b90508060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1663cb3c28c78a8a8a8a308b8b8b6040518963ffffffff1660e01b8152600401808873ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018781526020018673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018481526020018381526020018273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019750505050505050506020604051808303818588803b1580156108e657600080fd5b505af11580156108fa573d6000803e3d6000fd5b50505050506040513d602081101561091157600080fd5b810190808051906020019092919050505091505097965050505050505056fea264697066735822122050847e76d9d5873db01b9a11bd10579cef7e93ec17a520038d536c4ea86ce19b64736f6c63430006040033'
    # gas = 689126
    # contractDeploymentTupleList.append((name, byteCode, nonce, gas))
    # nonce += 1

    # name = 'Properties'
    # byteCode = '608060405234801561001057600080fd5b50610f15806100206000396000f3fe608060405234801561001057600080fd5b506004361061004c5760003560e01c80631b5fc9a114610051578063a39fac121461006d578063b95717211461008b578063c6ee701e146100a7575b600080fd5b61006b60048036038101906100669190610b86565b6100c5565b005b610075610227565b6040516100829190610d11565b60405180910390f35b6100a560048036038101906100a09190610b45565b61058c565b005b6100af6108c8565b6040516100bc9190610d33565b60405180910390f35b60006100cf6109c5565b9050600115158160000160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151514610166576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161015d90610d55565b60405180910390fd5b60006101706109ed565b9050600080905083818060010192508151811061018957fe5b602002602001015182600601819055508381806001019250815181106101ab57fe5b602002602001015182600701819055508381806001019250815181106101cd57fe5b602002602001015182600801819055508381806001019250815181106101ef57fe5b6020026020010151826009018190555083818060010192508151811061021157fe5b602002602001015182600a018190555050505050565b606060006102336109ed565b90506000809050600760405190808252806020026020018201604052801561026a5781602001602082028036833780820191505090505b5092508160000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff168382806001019350815181106102a457fe5b602002602001019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff16815250508160010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1683828060010193508151811061031557fe5b602002602001019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff16815250508160020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1683828060010193508151811061038657fe5b602002602001019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff16815250508160030160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff168382806001019350815181106103f757fe5b602002602001019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff16815250508160040160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1683828060010193508151811061046857fe5b602002602001019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff16815250508160050160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff168382806001019350815181106104d957fe5b602002602001019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff168152505081600b0160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1683828060010193508151811061054a57fe5b602002602001019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff1681525050829250505090565b60006105966109c5565b9050600115158160000160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615151461062d576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161062490610d55565b60405180910390fd5b60006106376109ed565b9050600080905083818060010192508151811061065057fe5b60200260200101518260000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055508381806001019250815181106106ac57fe5b60200260200101518260010160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555083818060010192508151811061070857fe5b60200260200101518260020160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555083818060010192508151811061076457fe5b60200260200101518260030160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055508381806001019250815181106107c057fe5b60200260200101518260040160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555083818060010192508151811061081c57fe5b60200260200101518260050160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555083818060010192508151811061087857fe5b602002602001015182600b0160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050505050565b606060006108d46109ed565b90506000809050600560405190808252806020026020018201604052801561090b5781602001602082028036833780820191505090505b509250816006015483828060010193508151811061092557fe5b602002602001018181525050816007015483828060010193508151811061094857fe5b602002602001018181525050816008015483828060010193508151811061096b57fe5b602002602001018181525050816009015483828060010193508151811061098e57fe5b60200260200101818152505081600a01548382806001019350815181106109b157fe5b602002602001018181525050829250505090565b60007f23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6905090565b60007f2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3905090565b600081359050610a2481610eb1565b92915050565b600082601f830112610a3b57600080fd5b8135610a4e610a4982610da2565b610d75565b91508181835260208401935060208101905083856020840282011115610a7357600080fd5b60005b83811015610aa35781610a898882610a15565b845260208401935060208301925050600181019050610a76565b5050505092915050565b600082601f830112610abe57600080fd5b8135610ad1610acc82610dca565b610d75565b91508181835260208401935060208101905083856020840282011115610af657600080fd5b60005b83811015610b265781610b0c8882610b30565b845260208401935060208301925050600181019050610af9565b5050505092915050565b600081359050610b3f81610ec8565b92915050565b600060208284031215610b5757600080fd5b600082013567ffffffffffffffff811115610b7157600080fd5b610b7d84828501610a2a565b91505092915050565b600060208284031215610b9857600080fd5b600082013567ffffffffffffffff811115610bb257600080fd5b610bbe84828501610aad565b91505092915050565b6000610bd38383610bf7565b60208301905092915050565b6000610beb8383610d02565b60208301905092915050565b610c0081610e75565b82525050565b6000610c1182610e12565b610c1b8185610e42565b9350610c2683610df2565b8060005b83811015610c57578151610c3e8882610bc7565b9750610c4983610e28565b925050600181019050610c2a565b5085935050505092915050565b6000610c6f82610e1d565b610c798185610e53565b9350610c8483610e02565b8060005b83811015610cb5578151610c9c8882610bdf565b9750610ca783610e35565b925050600181019050610c88565b5085935050505092915050565b6000610ccf601483610e64565b91507f4d7573742062652077686974656c69737465642e0000000000000000000000006000830152602082019050919050565b610d0b81610ea7565b82525050565b60006020820190508181036000830152610d2b8184610c06565b905092915050565b60006020820190508181036000830152610d4d8184610c64565b905092915050565b60006020820190508181036000830152610d6e81610cc2565b9050919050565b6000604051905081810181811067ffffffffffffffff82111715610d9857600080fd5b8060405250919050565b600067ffffffffffffffff821115610db957600080fd5b602082029050602081019050919050565b600067ffffffffffffffff821115610de157600080fd5b602082029050602081019050919050565b6000819050602082019050919050565b6000819050602082019050919050565b600081519050919050565b600081519050919050565b6000602082019050919050565b6000602082019050919050565b600082825260208201905092915050565b600082825260208201905092915050565b600082825260208201905092915050565b6000610e8082610e87565b9050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000819050919050565b610eba81610e75565b8114610ec557600080fd5b50565b610ed181610ea7565b8114610edc57600080fd5b5056fea26469706673582212207634166b50c679ca16e40f665a05c59ff4432d5361c5922bb7518026ce7c2c0c64736f6c63430006040033'
    # gas = 1077865
    # contractDeploymentTupleList.append((name, byteCode, nonce, gas))
    # nonce += 1

    # name = 'Contract_Ninja_Authentication_KeeperDAOLPPs'
    # byteCode = '608060405234801561001057600080fd5b50610441806100206000396000f3fe608060405234801561001057600080fd5b506004361061002b5760003560e01c806373827d7814610030575b600080fd5b61004a60048036038101906100459190610285565b61004c565b005b6000610056610188565b9050600115158160000160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff161515146100ed576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016100e490610319565b60405180910390fd5b60006100f76101b0565b905060008090505b8451811015610181578382600001600087848151811061011b57fe5b602002602001015173ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff02191690831515021790555080806001019150506100ff565b5050505050565b60007f23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6905090565b60007f4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f0905090565b6000813590506101e7816103dd565b92915050565b600082601f8301126101fe57600080fd5b813561021161020c82610366565b610339565b9150818183526020840193506020810190508385602084028201111561023657600080fd5b60005b83811015610266578161024c88826101d8565b845260208401935060208301925050600181019050610239565b5050505092915050565b60008135905061027f816103f4565b92915050565b6000806040838503121561029857600080fd5b600083013567ffffffffffffffff8111156102b257600080fd5b6102be858286016101ed565b92505060206102cf85828601610270565b9150509250929050565b60006102e660148361038e565b91507f4d7573742062652077686974656c69737465642e0000000000000000000000006000830152602082019050919050565b60006020820190508181036000830152610332816102d9565b9050919050565b6000604051905081810181811067ffffffffffffffff8211171561035c57600080fd5b8060405250919050565b600067ffffffffffffffff82111561037d57600080fd5b602082029050602081019050919050565b600082825260208201905092915050565b60006103aa826103bd565b9050919050565b60008115159050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6103e68161039f565b81146103f157600080fd5b50565b6103fd816103b1565b811461040857600080fd5b5056fea26469706673582212205d0ec8fa9159bce6e766da150acc12711ad36f2ea66273787435312a848cbdcc64736f6c63430006040033'
    # gas = 350827
    # contractDeploymentTupleList.append((name, byteCode, nonce, gas))
    # nonce += 1

    # name = 'GasTokens_CHI'
    # byteCode = '608060405234801561001057600080fd5b506103dc806100206000396000f3fe608060405234801561001057600080fd5b50600436106100365760003560e01c80638b61e16b1461003b578063d0847f1c1461006b575b600080fd5b61005560048036038101906100509190610298565b610087565b6040516100629190610359565b60405180910390f35b61008560048036038101906100809190610298565b6101ca565b005b600080610092610246565b9050600115158160000160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151514610129576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161012090610339565b60405180910390fd5b6d4946c0e9f43f4dee607b0ef1fa1c73ffffffffffffffffffffffffffffffffffffffff1663d8ccd0f3846040518263ffffffff1660e01b81526004016101709190610359565b602060405180830381600087803b15801561018a57600080fd5b505af115801561019e573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906101c291906102c1565b915050919050565b6d4946c0e9f43f4dee607b0ef1fa1c73ffffffffffffffffffffffffffffffffffffffff1663a0712d68826040518263ffffffff1660e01b81526004016102119190610359565b600060405180830381600087803b15801561022b57600080fd5b505af115801561023f573d6000803e3d6000fd5b5050505050565b60007f23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6905090565b60008135905061027d8161038f565b92915050565b6000815190506102928161038f565b92915050565b6000602082840312156102aa57600080fd5b60006102b88482850161026e565b91505092915050565b6000602082840312156102d357600080fd5b60006102e184828501610283565b91505092915050565b60006102f7601483610374565b91507f4d7573742062652077686974656c69737465642e0000000000000000000000006000830152602082019050919050565b61033381610385565b82525050565b60006020820190508181036000830152610352816102ea565b9050919050565b600060208201905061036e600083018461032a565b92915050565b600082825260208201905092915050565b6000819050919050565b61039881610385565b81146103a357600080fd5b5056fea2646970667358221220c847722d665a184ff83ccf431d86f5545a1da9c0a58e091c9370433f4bb7f4f064736f6c63430006040033'
    # gas = 324118
    # contractDeploymentTupleList.append((name, byteCode, nonce, gas))
    # nonce += 1

    # estimatedGas = Libraries.core.API_EstimateGas(None, fromAddress, byteCode)
    # PrintAndLog("estimatedGas = " + str(estimatedGas))
    # sys.exit()
    # API_DeployContract(fromAddress, fromPrivateKey, gas, gasPrice_int, byteCode, nonce)

    contractDeploymentDict = {}
    for tupleItem in contractDeploymentTupleList:
        name, byteCode, nonce, gas = tupleItem

        if doDeployContracts:
            PrintAndLog("gas = " + str(gas))
            PrintAndLog("gasPrice_int = " + str(gasPrice_int))
            PrintAndLog("nonce = " + str(nonce))
            transactionId = API_DeployContract(fromAddress, fromPrivateKey, gas, gasPrice_int, byteCode, nonce)
        else:
            transactionId = '0xdoDeployContracts=False'

        contractDeploymentDict[nonce] = {
            'name': name,
            'byteCode': byteCode,
            'transactionId': transactionId,
            'gas': gas,
        }

    PrintAndLog("contractDeploymentDict = " + str(contractDeploymentDict))

elif sys.argv and len(sys.argv) >= 2 and "send_message_on_chain" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    fromAddress = ninjaOp.publicAddress
    privateKey = ninjaOp.DecryptPK()
    # destinationAddress = ninjaOp.publicAddress
    # destinationAddress = '0x8af2890098b827271e92df55bf51f092dfd7972c'
    destinationAddress = '0x5c7c6d069ba232718f37c27a9549b547c359e31c'

    doSendTx = False
    etherToSend_etherUnits = 0.01

    stringToSend = "Greetings fellow keepers. It's exciting to see keeper communication. Coordination is the name of the game. " \
                   "Let's coordinate together, reduce gas auctions, and share profits. Reach out to us at KeeperDAO so we can chat off chain."
    hexString = '0x'
    for char in stringToSend:
        hexString += hex(ord(char)).replace("0x", "")

    PrintAndLog("hexString = " + str(hexString))

    value_wei_int = Libraries.core.ConvertEtherToWei(etherToSend_etherUnits, Libraries.core.Ether_Decimals)
    estimatedGas = Libraries.core.API_EstimateGas(destinationAddress, fromAddress, hexString, value_wei_int)
    PrintAndLog("estimatedGas = " + str(estimatedGas))
    PrintAndLog("etherToSend_etherUnits = " + str(etherToSend_etherUnits))
    PrintAndLog("value_wei_int = " + str(value_wei_int))

    if doSendTx:
        txId = API_SendEther_ToContract(fromAddress, privateKey, destinationAddress, value_wei_int, estimatedGas,
                                        Libraries.gasStation.GetGasPrice_SafeLow(), hexString, TransactionType.other)

elif sys.argv and len(sys.argv) >= 2 and "generate_new_account_for_joeyz_gastoken" == sys.argv[1].lower():
    # leadingStringToMatch = '0x' + '0' * 10
    leadingStringToMatch = '0x' + '0' * 2
    # leadingStringToMatch = '0x0000'
    PrintAndLog("leadingStringToMatch + " + str(leadingStringToMatch))
    contractAddressNonceDepth = 1

    loopForever = True
    datetimeBefore = datetime.datetime.now()
    while loopForever:
        # Generate a new account
        privateKey_string, accountAddress, account = Libraries.signingUtils.GenerateNewEthereumAccount()
        # print('privateKey_string = ', privateKey_string)
        # print('accountAddress = ', accountAddress)
        # print('account = ', account)
        # Check the account to see if any of the first few contract addresses it would generate will match our requirements
        for nonce in range(contractAddressNonceDepth):
            contractAddress = Libraries.signingUtils.GenerateContractAddress(accountAddress, nonce)
            if contractAddress.startswith(leadingStringToMatch):
                duration_s = (datetime.datetime.now() - datetimeBefore).total_seconds()
                message = "Found an account matching my requirements! took " + str(round(duration_s, 1)) + " seconds"
                API_PostOperatorNotification(message)
                PrintAndLog(message)
                print('   privateKey_string = ', privateKey_string)
                print('   accountAddress = ', accountAddress)
                print('   account = ', account)
                print('   contractAddress for nonce ', str(nonce), ' = ', str(contractAddress))
                print('   ----------------------------------------------------------------------------------------------------')
                # loopForever = False
                # break

elif sys.argv and len(sys.argv) >= 2 and "test_find_low_gasprice_transactions_in_history" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    maxBlockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    numOfBlocksToScan = 50
    minBlockNumber_int = maxBlockNumber_int - numOfBlocksToScan
    PrintAndLog("minBlockNumber_int = " + str(minBlockNumber_int))
    PrintAndLog("maxBlockNumber_int = " + str(maxBlockNumber_int))

    gasPrice_maxRequirement = 1.0
    gasUsed_minRequirement = 100000

    transactionData = {}
    for blockNumber_int in range(minBlockNumber_int, maxBlockNumber_int):
        try:
            PrintAndLog("blockNumber_int = " + str(blockNumber_int))

            blockData = Libraries.core.API_GetBlock(blockNumber_int, True)
            # PrintAndLog("blockData = " + str(blockData))

            for transaction in blockData['transactions']:
                txHash = transaction['hash'].lower()
                transactionData[txHash] = {}
                transactionData[txHash]['blockNumber'] = blockNumber_int
                transactionData[txHash]['gasPrice'] = Libraries.core.ConvertWeiToGwei(Libraries.core.ConvertHexToInt(transaction['gasPrice']))
                transactionData[txHash]['gasLimit'] = Libraries.core.ConvertHexToInt(transaction['gas'])
                # transactionData['txReceipt'] = Libraries.core.API_GetTransactionReceipt(txHash)
                # transactionData['gasUsed'] = Libraries.core.ConvertHexToInt(txReceipt['gasUsed'])
                # PrintAndLog("transactionData[txHash] = " + str(transactionData[txHash]))

            # PrintAndLog("transactionData before gasUsed updated = " + str(transactionData))

            resultList_transactionReceipt = Libraries.core.API_GetTransactionReceipt_Batched_Safe(list(transactionData.keys()))
            # PrintAndLog("resultList_transactionReceipt = " + str(resultList_transactionReceipt))
            for result_transactionReceipt in resultList_transactionReceipt:
                txHash = result_transactionReceipt['transactionHash'].lower()
                if txHash in transactionData:
                    transactionData[txHash]['gasUsed'] = Libraries.core.ConvertHexToInt(result_transactionReceipt['gasUsed'])

            # PrintAndLog("transactionData with all gasUsed updated = " + str(transactionData))
            # Sleep a bit to give it a break
            time.sleep(0.05)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception = " + traceback.format_exc())
            pass

    countUnderThreshold = 0
    for txHash in transactionData:
        gasPrice_gwei = transactionData[txHash]['gasPrice']
        gasUsed = transactionData[txHash]['gasUsed']
        if gasPrice_gwei < gasPrice_maxRequirement and \
                gasUsed > gasUsed_minRequirement:
            gasCost_ether = Libraries.gasStation.GetGasCost(Libraries.core.ConvertGweiToWei(gasPrice_gwei), gasUsed)
            PrintAndLog("Found tx with gas Price below threshold: gasPrice = " + str(gasPrice_gwei) + " gwei, gasUsed = " + str(
                Libraries.core.FormatNumberWithCommas(gasUsed)) + ", gasCost_ether = " + str(round(gasCost_ether, 6)) + " ETH, txHash = " + str(txHash))
            countUnderThreshold += 1

    PrintAndLog("Analyzed " + str(numOfBlocksToScan) + " blocks and found " + str(len(transactionData)) + " transactions. " + str(
        countUnderThreshold) + " of them were under the gas price threshold of " + str(gasPrice_maxRequirement) + " gwei")

elif sys.argv and len(sys.argv) >= 2 and "test_send_ether" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    gasPrice = Libraries.core.ConvertGweiToWei(0.5)
    account = NinjaOpAccountDict["ninja-op-4"]
    fromAddress = account.publicAddress
    fromPrivateKey = account.DecryptPK()
    API_SendEther_ToAddress(fromAddress, fromPrivateKey, fromAddress, 500,
                            Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt,
                            gasPrice, TransactionType.other)

elif sys.argv and len(sys.argv) >= 2 and "test_cancel_transaction" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    account = NinjaOpAccountDict["ninja-op-100"]
    # account = Libraries.accounts.TestAccount_JoeyZ_65C2
    # account = Libraries.accounts.TestAccount_JoeyZ_0015
    fromAddress = account.publicAddress
    fromPrivateKey = account.DecryptPK()

    gasPrice = Libraries.core.ConvertGweiToWei(73.26000111)
    nonce = 3639
    API_CancelTransaction(fromAddress, fromPrivateKey, gasPrice, nonce)

elif sys.argv and len(sys.argv) >= 2 and "test_cancel_transaction_given_tx" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    account = NinjaOpAccountDict["ninja-op-100"]
    fromAddress = account.publicAddress
    fromPrivateKey = account.DecryptPK()
    transactionId = '0x56d5c8391e2d42cee667cc911f7afd4c3a80b0ad4914fa6564fcf403a1f57da7'
    API_CancelTransaction_BaseGasPriceOnSpecificTx(fromAddress, fromPrivateKey, transactionId)

elif sys.argv and len(sys.argv) >= 2 and "test_estimate_gas_cost" == sys.argv[1].lower():
    PrintAndLog("Not entering main loop, do some functional testing here!")

    gasUsed = 98000
    gasPrice_gwei = 0.222
    gasPrice_wei = Libraries.core.ConvertGweiToWei(gasPrice_gwei)
    expectedGasCost_eth = Libraries.gasStation.GetGasCost(gasPrice_wei, 98000)
    PrintAndLog("expectedGasCost_eth = " + str(expectedGasCost_eth))

elif sys.argv and len(sys.argv) >= 2 and "test_get_balance_at_block" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    specifiedBlockNumber_int = 9088314
    address_toGetEtherBalance = '0x2c4bd064b998838076fa341a83d007fc2fa50957'
    address_toGetTokenBalance = '0x2c4bd064b998838076fa341a83d007fc2fa50957'
    tokenAddress = '0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2'
    decimals_token = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress)))
    PrintAndLog("decimals_token = " + str(decimals_token))
    etherBalance = Libraries.core.API_GetEtherBalance(
        address_toGetEtherBalance, None, None, None, Libraries.core.RequestTimeout_seconds, specifiedBlockNumber_int)
    PrintAndLog("etherBalance = " + str(etherBalance))
    tokenBalance = Libraries.core.API_GetTokenBalance(
        address_toGetTokenBalance, tokenAddress, decimals_token, None, None, None, Libraries.core.RequestTimeout_seconds, specifiedBlockNumber_int)
    PrintAndLog("tokenBalance = " + str(tokenBalance))

    sourceTokenQuantity_etherUnits = 0.533371973486104448
    balanceEther_UniswapContract = etherBalance
    balanceTokens_UniswapContract = tokenBalance
    price = Exchanges.uniswap.CalculatePriceGivenBalances('sell', sourceTokenQuantity_etherUnits, balanceEther_UniswapContract, balanceTokens_UniswapContract)
    PrintAndLog("price = " + str(price))

elif sys.argv and len(sys.argv) >= 2 and "populate_cache_decimals" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    # Libraries.cache.GetDecimalsForTokenContract_UseCachingSystem('0xdd974d5c2e2928dea5f71b9825b8b646686bd200')

    # addressList = Exchanges.kyber.API_GetListOfTokenAddresses()
    # addressList = Exchanges.radarRelay2.API_GetListOfTokenAddresses()

    # addressList = []
    # for marketName in MarketDict:
    #     addressList.append(MarketDict[marketName].erc20TokenContractAddress.lower())

    # Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")
    # addressList = list(Exchanges.uniswap.ContractDict_Exchange.keys())
    #
    # for tokenAddress in addressList:
    #     try:
    #         decimals_token = Libraries.cache.GetDecimalsForTokenContract_UseCachingSystem(tokenAddress)
    #         PrintAndLog("decimals_token = " + str(decimals_token))
    #     except:
    #         PrintAndLogError("exception = " + traceback.format_exc())
    #         pass

elif sys.argv and len(sys.argv) >= 2 and "test_priceoracle" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    erc20TokenContractAddress = '0x514910771af9ca656af840dff83e8264ecf986ca'
    price = Libraries.priceOracle.GetOnChainPriceFromAllSources(erc20TokenContractAddress)
    PrintAndLog("price = " + str(price))

elif sys.argv and len(sys.argv) >= 2 and "test_get_all_hotwallets" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    addressList = Libraries.accounts.GetListOfAllKnownHotWalletAddresses()
    PrintAndLog("AddressList = " + str(addressList))

elif sys.argv and len(sys.argv) >= 2 and "test_count_txs_for_all_hotwallets" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    addressList = Libraries.accounts.GetListOfAllKnownHotWalletAddresses(True)
    # addressList = ['0x0015CB2187299D43B6313aE138a966899c72630c', '0x4a45AFD5A9691407B2b8E6Ed8052A511EE7f01E9']

    PrintAndLog("addressList of len " + str(len(addressList)) + " = " + str(addressList))

    sum_transactionCount = 0

    for address in addressList:
        try:
            transactionCount = Libraries.core.API_GetTransactionCount(address)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            # Treat this as transactionCount = 0
            transactionCount = 0
            PrintAndLogError("exception = " + traceback.format_exc())
            pass

        sum_transactionCount += transactionCount

    PrintAndLog("sum_transactionCount = " + str(sum_transactionCount))

    dateTime_start = datetime.datetime(2017, 10, 1)
    dateTime_end = datetime.datetime.now()
    duration_seconds = (dateTime_end - dateTime_start).total_seconds()
    duration_days = duration_seconds / 60 / 60 / 24
    PrintAndLog("duration_days = " + str(duration_days))
    duration_years = duration_days / 365
    PrintAndLog("duration_years = " + str(duration_years))

    transactionsPerDay = sum_transactionCount / duration_days
    PrintAndLog("transactionsPerDay = " + str(transactionsPerDay))

elif sys.argv and len(sys.argv) >= 2 and "testpriceoracle" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.uniswapV2.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswapV2.API_GetExchangeDataDict")

    Libraries.priceOracle.StartService()

    # address_baseToken = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48"
    address_baseToken = "0xD533a949740bb3306d119CC777fa900bA034cd52"
    # Contract_renBTC = Libraries.nodes.Instance_Web3.toChecksumAddress("0xeb4c2781e4eba804ce9a9803c67d0893436bb27d")
    # Contract_USDT = Libraries.nodes.Instance_Web3.toChecksumAddress("0xdac17f958d2ee523a2206206994597c13d831ec7")
    # Contract_wBTC = Libraries.nodes.Instance_Web3.toChecksumAddress("0x2260fac5e5542a773aa44fbcfedf7c193bc2c599")
    # Contract_UNI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x1f9840a85d5af5bf1d1762f925bdaddc4201f984")
    # Contract_COMP = Libraries.nodes.Instance_Web3.toChecksumAddress("0xc00e94cb662c3520282e6f5717214004a7f26888")
    # Contract_YFI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x0bc529c00c6401aef6d220be8c6ea1667f6ad93e")
    # Contract_CRV = Libraries.nodes.Instance_Web3.toChecksumAddress("0xD533a949740bb3306d119CC777fa900bA034cd52")

    price_weth = Libraries.priceOracle.GetOnChainPrice_FromCache(Exchanges.zrxV2.Contract_WETH)
    PrintAndLog("price_weth = " + str(price_weth))
    price_token = Libraries.priceOracle.GetOnChainPrice_FromCache(address_baseToken)
    PrintAndLog("price_token = " + str(price_token))
    price_weth_token = Libraries.priceOracle.GetOnChainPrice_FromCache_TokenForToken(Exchanges.zrxV2.Contract_WETH, address_baseToken)
    PrintAndLog("price_weth_token = " + str(price_weth_token))
    price_token_weth = Libraries.priceOracle.GetOnChainPrice_FromCache_TokenForToken(address_baseToken, Exchanges.zrxV2.Contract_WETH)
    PrintAndLog("price_token_weth = " + str(price_token_weth))
    price_eth_token = Libraries.priceOracle.GetOnChainPrice_FromCache(address_baseToken)
    PrintAndLog("price_eth_token = " + str(price_eth_token))

elif sys.argv and len(sys.argv) >= 2 and "testmultiprocess" == sys.argv[1].lower():
    import Libraries.testMultiprocess

    Libraries.testMultiprocess.DoStuff()

elif sys.argv and len(sys.argv) >= 2 and "testtransactioncountcache" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    copy_transactionCountCache = Libraries.cache.GetThreadSafeCopyOf_TransactionCountCache()
    PrintAndLog("copy_transactionCountCache BEFORE of len " + str(len(copy_transactionCountCache)) + " = " + str(copy_transactionCountCache))

    Libraries.cache.RegisterAddressesInTransactionCountCache_AllTradingAddresses()
    copy_transactionCountCache = Libraries.cache.GetThreadSafeCopyOf_TransactionCountCache()
    PrintAndLog("copy_transactionCountCache AFTER REGISTER of len " + str(len(copy_transactionCountCache)) + " = " + str(copy_transactionCountCache))

    Libraries.cache.UpdateTransactionCountCache()
    copy_transactionCountCache = Libraries.cache.GetThreadSafeCopyOf_TransactionCountCache()
    PrintAndLog("copy_transactionCountCache AFTER UPDATE of len " + str(len(copy_transactionCountCache)) + " = " + str(copy_transactionCountCache))

    for x in range(6):
        PrintAndLog("Calling UpdateTransactionCountCache")
        Libraries.executeOnInterval.IsTimeToExecute("Libraries.cache.UpdateTransactionCountCache", 0, Libraries.cache.UpdateTransactionCountCache)

        PrintAndLog("Sending a test transaction to myself to make sure nonce is working properly")
        ninjaAccount = NinjaOpAccountDict["ninja-op-9"]
        destinationAddress = ninjaAccount.publicAddress
        value_wei_int = Libraries.core.ConvertEtherToWei(0, Libraries.core.Ether_Decimals)
        txId = API_SendEther_ToAddress(ninjaAccount.publicAddress, ninjaAccount.DecryptPK(), destinationAddress, value_wei_int,
                                       Libraries.gasStation.GetGasLimit_SendingEther(), Libraries.gasStation.GetGasPrice_Cheap(), TransactionType.other)

        time.sleep(60)

elif sys.argv and len(sys.argv) >= 2 and "keccak256" == sys.argv[1].lower():
    # def Keccak256(itemsList):
    # // ds_slot = keccak256("diamond.standard.diamond.storage");
    # assembly
    # {ds_slot := 0xc8fcad8db84d3cc18b4c41d551ea0ee66dd599cde068d998e57d5e09332c131c}

    myText = 'diamond.storage.ninja.properties'
    from web3 import Web3

    hashedText = Web3.sha3(text=myText).hex()
    PrintAndLog("myText = " + str(myText))
    PrintAndLog("hashedText = " + str(hashedText))

elif sys.argv and len(sys.argv) >= 2 and "testencode_ordersig" == sys.argv[1].lower():
    signature = '0x1cfb85fe481630add69bd6ef046a3961eaa86c1eb129f065fc9bc169db4d224d825b5d7a9c0f9a4cba6f4001ebc0b98f4a579421dce80ea78bc214a78609ad4f9303'
    # signature = '0x1b76168c47c0a95baa213ae59ee1155b6ab88f2b602edf010e37ab1572410611746723344556acdc943f60c654ae4e00d62f426c93e0253e0e0276f7243e25ddf703'
    Libraries.ninjaEncoding.PackOrderSignature(signature)

elif sys.argv and len(sys.argv) >= 2 and "testencode_bytes" == sys.argv[1].lower():
    functionSelector = '0xcb3c28c7'
    callData = '0000000000000000000000000000000000000000000000000000000001b8141a0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000'
    encodedList_functionSelector = Libraries.ninjaEncoding.PackBytes(functionSelector)
    encodedList_callData = Libraries.ninjaEncoding.PackBytes(callData)
    PrintAndLog("encodedList_functionSelector = " + str(encodedList_functionSelector))
    PrintAndLog("encodedList_callData = " + str(encodedList_callData))

elif sys.argv and len(sys.argv) >= 2 and "testencode_0xassetdata" == sys.argv[1].lower():
    zrxAssetData = '0xf47261b000000000000000000000000041e5560054824ea6b0732e656e3ad64e20e94e45'
    Libraries.ninjaEncoding.PackOrder0xAssetData(zrxAssetData)

elif sys.argv and len(sys.argv) >= 2 and "testencode_address" == sys.argv[1].lower():
    Libraries.ninjaEncoding.PackAddress('0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48')

elif sys.argv and len(sys.argv) >= 2 and "testencode_number" == sys.argv[1].lower():
    # Libraries.ninjaEncoding.PackInt(1586988336)
    # Libraries.ninjaEncoding.PackInt(1586988336123)
    Libraries.ninjaEncoding.PackInt(115792089237316195423570985008687907853269984665640564039457584007913129639935)  # This is 0xFFFF.....FFFF

elif sys.argv and len(sys.argv) >= 2 and "tradehistory_search_block_range" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    fromAddressFilterList = Libraries.notorious.KnownOperatorAddresses
    PrintAndLog("fromAddressFilterList = " + str(fromAddressFilterList))

    toAddressFilterList = Libraries.notorious.KnownProxyContractAddresses
    PrintAndLog("toAddressFilterList = " + str(toAddressFilterList))

    # Block I actually care about 9861623 for this test case
    blockNumber_start = 9861622
    blockNumber_end = 9861626

    for blockNumber in range(blockNumber_start, blockNumber_end + 1):
        blockData = Libraries.core.API_GetBlock(blockNumber, True)
        # PrintAndLog("blockData = " + str(blockData))
        for transaction in blockData['transactions']:
            if transaction['to'].lower() in toAddressFilterList or transaction['from'].lower() in fromAddressFilterList:
                txHash = transaction['hash']
                PrintAndLog("Found potential notorious transaction at blockNumber " + str(blockNumber) + " : txHash = " + str(txHash))

elif sys.argv and len(sys.argv) >= 2 and "test_debug_get_arbitrage_function" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.kovanTestnet)

    Libraries.cache.GetAndCache_Set_RebalancingSets()

    Libraries.priceOracle.StartService()

    Exchanges.set.PopulateSetTokenDataDict()

    Exchanges.set.UpdateAllSetTokenData()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_NinjaBetweenBancorAnd0x()
    PopulateTokenDict_NinjaBetweenBancorAndKyber()
    PopulateTokenDict_NinjaBetween0xAndKyber()
    PopulateTokenDict_NinjaBetweenUniswapAndKyber()
    PopulateTokenDict_Ninja_Set()

    PrintAndLog("-------------------------------- JoeyZDEBUG --------------------------------")

    key = 'leloap'
    PrintAndLog("key = " + str(key))
    PrintAndLog("inflow token = " + str(Exchanges.set.SetTokenDataDict[key].inflowTokenAddress))
    PrintAndLog("outflow token = " + str(Exchanges.set.SetTokenDataDict[key].outflowTokenAddress))
    PrintAndLog("price = " + str(Exchanges.set.SetTokenDataDict[key].price))
    PrintAndLog("side = " + str(Exchanges.set.SetTokenDataDict[key].side))
    PrintAndLog("minBidSize_weiUnits = " + str(Exchanges.set.SetTokenDataDict[key].minBidSize_weiUnits))

    token = TokenDict_Ninja['link']
    etherToSpendList_etherUnits = [29.099384024928444, 23.544047074714793, 18.5765764684391, 14.196972206101364, 10.405234287701585, 7.201362713239762, 4.585357482715896,
                                   2.5572185961299865, 1.1169460534820344, 0.26453985477203873]
    etherToSpendList_weiUnits = Libraries.core.ConvertListOfEthersToWeis(etherToSpendList_etherUnits, Libraries.core.Ether_Decimals)
    kyberReserve = '0x7a3370075a54b187d7bd5dcebf0ff2b5552d4f7d'
    Exchanges.ninja.API_GetManyArbitrages_New_BuyOnSetSellOnKyber(token, etherToSpendList_weiUnits, key, kyberReserve)

elif sys.argv and len(sys.argv) >= 2 and "test_get_function_selectors_for_all_functions_in_contract" == sys.argv[1].lower():
    Libraries.signingUtils.GetFunctionSelectorsListForAllFunctionsInContract(Contract_Ninja_Trade_3, True)

elif sys.argv and len(sys.argv) >= 2 and "testblocknative_webhooks" == sys.argv[1].lower():
    Libraries.blockNative.CreateWebhookListener()

    while True:
        time.sleep(1)

elif sys.argv and len(sys.argv) >= 2 and "testblocknative_webhooks_sendtestmessage" == sys.argv[1].lower():
    Libraries.blockNative.API_SendTestWebhook('woot')

elif sys.argv and len(sys.argv) >= 2 and "testblocknative_add_watch_addresses" == sys.argv[1].lower():
    # Libraries.utils.SetActiveScriptName(Libraries.utils.ScriptName.NinjaArb)
    Libraries.utils.SetActiveScriptName(Libraries.utils.ScriptName.NinjaTail)

    addresses = [
        '0xf164fC0Ec4E93095b804a4795bBe1e041497b92a'
    ]

    Libraries.blockNative.API_AddWatchAddresses(addresses)
    # Libraries.blockNative.API_DeleteWatchAddresses(addresses)

elif sys.argv and len(sys.argv) >= 2 and "testalchemy_websockets" == sys.argv[1].lower():
    Libraries.alchemy.ConnectWebSocketClient()

    while True:
        time.sleep(10)

elif sys.argv and len(sys.argv) >= 2 and "testbloxroute_websockets_mempool" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    for bloxroute in Libraries.nodes.BloxrouteList:
        Libraries.bloxroute.ConnectWebSocketClient(bloxroute, True)

    while True:
        time.sleep(10)

# elif sys.argv and len(sys.argv) >= 2 and "testbloxroute_websockets_mempool_subprocess" == sys.argv[1].lower():
#     Subprocesses.bloxrouteMempool.ConnectToAllWebSocketClients_InSubAProcess()
#
#     while True:
#         time.sleep(10)

elif sys.argv and len(sys.argv) >= 2 and "testbloxroute_websockets_sendtx" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.bloxroute.ConnectToAllWebSocketClients()

    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    publicAddress_Ninja = ninjaOp.publicAddress
    privateKey_Ninja = ninjaOp.DecryptPK()
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    txId = API_SendEther_ToAddress(ninjaOp.publicAddress, ninjaOp.DecryptPK(), ninjaOp.publicAddress, 0,
                                   Libraries.gasStation.GetGasLimit_SendingEther(), gasPrice, TransactionType.other)
    while True:
        time.sleep(10)

elif sys.argv and len(sys.argv) >= 2 and "testbloxroute_tx_send_test" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    runCode = int(sys.argv[2])
    startTime_seconds = int(sys.argv[3])
    PrintAndLog("runCode = " + str(runCode))
    PrintAndLog("startTime_seconds = " + str(startTime_seconds))

    accountName = 'bloxroute-tx-send-test-' + str(runCode)
    account = UtilityAccountDict[accountName]
    PrintAndLog("accountName = " + str(accountName))

    nonce = Libraries.core.API_GetTransactionCount(account.publicAddress)

    doSendTxs = True

    if runCode == 1:
        # Enable Alchemy
        if Libraries.nodes.URL_RemoteNode.alchemy not in Libraries.nodes.RemoteNodeList_All:
            Libraries.nodes.RemoteNodeList_All.append(Libraries.nodes.URL_RemoteNode.alchemy)
    else:
        # Disable Alchemy
        if Libraries.nodes.URL_RemoteNode.alchemy in Libraries.nodes.RemoteNodeList_All:
            Libraries.nodes.RemoteNodeList_All.remove(Libraries.nodes.URL_RemoteNode.alchemy)

    if runCode == 2:
        # Enable Direct to Bloxroute gateway
        Libraries.bloxroute.ConnectToAllWebSocketClients()
        time.sleep(1)

        Libraries.defaults.BroadcastTxsDirectlyToBloxrouteGateways = True
    else:
        # Disable Direct to Bloxroute gateway
        Libraries.defaults.BroadcastTxsDirectlyToBloxrouteGateways = False

    PrintAndLog("Libraries.nodes.RemoteNodeList_All = " + str(Libraries.nodes.RemoteNodeList_All))
    PrintAndLog("Libraries.nodes.RemoteNodeList_BroadcastingTxsAndPendingTxsOnly = " + str(Libraries.nodes.RemoteNodeList_BroadcastingTxsAndPendingTxsOnly))
    PrintAndLog("Libraries.defaults.BroadcastTxsDirectlyToBloxrouteGateways = " + str(Libraries.defaults.BroadcastTxsDirectlyToBloxrouteGateways))

    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    currentTime_s = None
    # Wait for the next minute to start
    distanceToStartTime_s = startTime_seconds - time.time()
    PrintAndLog("Waiting for start time. distanceToStartTime_s = " + str(distanceToStartTime_s) + " seconds")
    while not currentTime_s or startTime_seconds > currentTime_s:
        currentTime_s = int(time.time())
        time.sleep(0.001)

    time_ms = int(time.time() * 1000)
    PrintAndLog("GO!")
    PrintAndLog("time_ms = " + str(time_ms))
    PrintAndLog("currentTime_s = " + str(currentTime_s))

    # sys.exit()
    sendTxsEveryXMs = 1000
    sendForThisDuration_ms = 2000
    sendTxInterval = 0
    sleepTime_s = 0.1
    while True:
        currentTime_ms = int(time.time() * 1000)
        elapsedTime_ms = currentTime_ms - time_ms
        PrintAndLog("elapsedTime_ms = " + str(elapsedTime_ms) + ", sendTxInterval = " + str(sendTxInterval))
        if sendTxInterval <= elapsedTime_ms:
            PrintAndLog("Calling SendTestTx")
            if doSendTxs:
                PrintAndLog("Not calling SendTestTx because doSendTxs was False")
                t = threading.Thread(target=Libraries.bloxroute.SendTestTx, args=(account, gasPrice, nonce, sendTxInterval, currentTime_ms, runCode,))
                t.start()

            sendTxInterval += sendTxsEveryXMs
            # Increment the nonce
            nonce += 1

        if sendTxInterval >= sendForThisDuration_ms:
            break

        time.sleep(sleepTime_s)
        pass

    while True:
        PrintAndLog("Done, just sleeping")
        time.sleep(60)
        PrintAndLog("Libraries.bloxroute.SendTxTestDict = " + str(Libraries.bloxroute.SendTxTestDict))
        Libraries.bloxroute.UpdateSendTxTestDictWithBlockNumbers()
        PrintAndLog("Libraries.bloxroute.SendTxTestDict after update = " + str(Libraries.bloxroute.SendTxTestDict))

elif sys.argv and len(sys.argv) >= 2 and "testbloxroute_tx_send_test_hardcodeddict" == sys.argv[1].lower():
    Libraries.bloxroute.SendTxTestDict = {'0xfe3b307a0e83100cddc195252dbc88b4a83a659c': {
        '0': {'sendTxInterval': 0, 'currentTime_ms': 1595806786002, 'txHash': '0x17d732300a490b944f3eabbb079f4bb6d87813b1018522aa66952f98215f9556', 'gasPrice': 36.0005005, 'nonce': 9,
              'runCode': 2},
        '1000': {'sendTxInterval': 1000, 'currentTime_ms': 1595806787041, 'txHash': '0xd747a4a4dcb4fb1661083db6e231ba6a3e70b1942f07f2888992261fa4317817', 'gasPrice': 36.0005005,
                 'nonce': 10, 'runCode': 2}}}
    Libraries.bloxroute.UpdateSendTxTestDictWithBlockNumbers()
    PrintAndLog("Libraries.bloxroute.SendTxTestDict after update = " + str(Libraries.bloxroute.SendTxTestDict))

elif sys.argv and len(sys.argv) >= 2 and "test_transfer_gas_cost" == sys.argv[1].lower():

    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    # tokenList = ['0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48']  # USDC
    # tokenList = ['0xe41d2489571d322189246dafa5ebde1f4699f498']  # ZRX
    # tokenList = ['0xdd974d5c2e2928dea5f71b9825b8b646686bd200']  # KNC
    # tokenList = ['0xdac17f958d2ee523a2206206994597c13d831ec7']  # USDT
    # erc20TokenContractAddress = "0x1985365e9f78359a9B6AD760e32412f4a445E862".lower()  # REPv1
    # erc20TokenContractAddress = "0x221657776846890989a759ba2973e427dff5c9bb".lower()  # REPv2
    tokenList = [
        # '0xdac17f958d2ee523a2206206994597c13d831ec7',  # USDT
        # '0xdd974d5c2e2928dea5f71b9825b8b646686bd200',  # KNC
        # '0xe41d2489571d322189246dafa5ebde1f4699f498',  # ZRX
        # '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48',  # USDC
        # '0xba11d00c5f74255f56a5e366f4f77f5a186d7f55',  # BAND
        # '0x1985365e9f78359a9B6AD760e32412f4a445E862',  # REPv1
        '0x221657776846890989a759ba2973e427dff5c9bb',  # REPv2
        '0xc011a73ee8576fb46f5e1c5751ca3b9fe0af2a6f',  # SNX
    ]

    Libraries.tokenTransferCost.API_GetTokenTransferCost(tokenList)

elif sys.argv and len(sys.argv) >= 2 and "testcurve" == sys.argv[1].lower():
    Exchanges.curve.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.curve.API_GetExchangeDataDict")

    poolAddress = '0xbebc44782c7db0a1a60cb6fe97d0b483032ff1c7'  # Pool

    # token = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'  # USDC
    # token = '0xdac17f958d2ee523a2206206994597c13d831ec7'  # USDT
    token = '0x6b175474e89094c44da98b954eedeac495271d0f'  # DAI

    index = Exchanges.curve.GetIndexOfTokenInExchange(poolAddress, token)
    PrintAndLog("index = " + str(index))

elif sys.argv and len(sys.argv) >= 2 and "testcurve_factory" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Libraries.cache.GetAndCache_Curve_API_GetExchangeDataDict()

elif sys.argv and len(sys.argv) >= 2 and "testcurve_price_joeyz_debug" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.curve.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.curve.API_GetExchangeDataDict")

    # blockNumber = 11498920
    blockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    # poolAddress = '0x93054188d876f558f4a66b2ef1d97d16edf0895b'  # Pool BTC
    # poolAddress = '0x7fc77b5c7614e1533320ea6ddc2eb61fa00a9714'  # Pool BTC with SBTC
    # poolAddress = '0xbebc44782c7db0a1a60cb6fe97d0b483032ff1c7'  # Pool USD
    # poolAddress = '0xa5407eae9ba41422680e2e00537571bcc53efbfd'  # Pool USD with SUSD
    # poolAddress = '0x52EA46506B9CC5Ef470C5bf89f17Dc28bB35D85C'.lower()  # cUSD, cDAI, USDT  # TODO BROKEN POOL
    # poolAddress = '0xa2b47e3d5c44877cca798226b7b8118f9bfb7a56'  # Pool cDAI, cUSDC
    # poolAddress = '0x45f783cce6b7ff23b2ab2d70e416cdb7d6055f51'  # Pool, yDAI, yUSDC, yUSDT, yTUSD
    # poolAddress = '0x329239599afb305da0a2ec69c58f8a6697f9f88d'  # Swerve  # PASSED
    # poolAddress = '0x3ba734d5e4e78801ab22cf55c5760e121e1c2c42'  # banned curve pool  # PASSED
    # poolAddress = '0x4ca9b3063ec5866a4b82e437059d2c43d1be596f'  # Pool HBTC WBTC  # PASSED
    # poolAddress = '0x4f062658eaaf2c1ccf8c8e36d6824cdf41167956'  # Pool GUSD 3CRV  # FAILED
    # poolAddress = '0x3ef6a01a0f81d6046290f3e2a8c5b843e738e604'  # Pool HUSD 3CRV  # FAILED
    # poolAddress = '0xd81da8d904b52208541bade1bd6595d8a251f8dd'  # Pool oBTC crvBTC
    # poolAddress = '0xf178c0b5bb7e7abf4e12a4838c7b7c5ba2c623c0'  # Pool LINK sLINK
    # poolAddress = '0xdebf20617708857ebe4f679508e7b7863a8a8eee'  # aTokens  # PASSED

    #     "tokens": [
    #       "0x16de59092dae5ccf4a1e6439d611fd0653f0bd01",
    #       "0xd6ad7a6750a7593e092a9b218d66c0a814a3436e",
    #       "0x83f798e925bcd4017eb265844fddabb448f1707d",
    #       "0x73a052500105205d34daf004eab301916da8190f"
    #     ]

    # quoteToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'  # USDC
    # quoteToken = '0xdac17f958d2ee523a2206206994597c13d831ec7'  # USDT
    # quoteToken = '0x6b175474e89094c44da98b954eedeac495271d0f'  # DAI
    # quoteToken = '0x57ab1ec28d129707052df4df418d58a2d46d5f51'  # SUSD
    # quoteToken = '0xeb4c2781e4eba804ce9a9803c67d0893436bb27d'  # renBTC
    # quoteToken = '0xfe18be6b3bd88a2d2a7f928d00292e7a9963cfc6'  # SBTC
    # quoteToken = '0x5d3a536E4D6DbD6114cc1Ead35777bAB948E3643'.lower()  # cDAI
    # quoteToken = '0x16de59092dae5ccf4a1e6439d611fd0653f0bd01'  # yDAI
    # quoteToken = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'  # WBTC
    # quoteToken = '0x6c3F90f043a72FA612cbac8115EE7e52BDe6E490'  # 3CRV
    # quoteToken = '0x075b1bb99792c9E1041bA13afEf80C91a1e70fB3'  # crvBTC
    # quoteToken = '0x514910771af9ca656af840dff83e8264ecf986ca'  # LINK
    # quoteToken = '0x3ed3b47dd13ec9a98b44e6204a523e766b225811'  # aUSDT
    quoteToken = '0x028171bca77440897b824ca71d1c56cac55b68a3'  # aDAI

    # baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'  # DAI
    # baseToken = '0x57ab1ec28d129707052df4df418d58a2d46d5f51'  # SUSD
    # baseToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'  # USDC
    # baseToken = '0xdac17f958d2ee523a2206206994597c13d831ec7'  # USDT
    # baseToken = '0x57ab1ec28d129707052df4df418d58a2d46d5f51'  # SUSD
    # baseToken = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'  # WBTC
    # baseToken = '0xfe18be6b3bd88a2d2a7f928d00292e7a9963cfc6'  # SBTC
    # baseToken = '0x39aa39c021dfbae8fac545936693ac917d5e7563'.lower()  # cUSDC
    # baseToken = '0x73a052500105205d34daf004eab301916da8190f'  # yTUSD
    # baseToken = '0x0000000000085d4780B73119b644AE5ecd22b376'  # TUSD
    # baseToken = '0x8e870d67f660d95d5be530380d0ec0bd388289e1'  # PAX
    # baseToken = '0x0316EB71485b0Ab14103307bf65a021042c6d380'  # HBTC
    # baseToken = '0x056fd409e1d7a124bd7017459dfea2f387b6d5cd'  # GUSD
    # baseToken = '0xdf574c24545e5ffecb9a659c229253d4111d87e1'  # HUSD
    # baseToken = '0x8064d9Ae6cDf087b1bcd5BDf3531bD5d8C537a68'  # oBTC
    # baseToken = '0xbbc455cb4f1b9e4bfc4b73970d360c8f032efee6'  # sLINK
    baseToken = '0xbcca60bb61934080951369a648fb03df4f96263c'  # aUSDC
    # baseToken = '0x028171bca77440897b824ca71d1c56cac55b68a3'  # aDAI

    # rates_hardCodedTest = [100000000, 100000000]
    # rates_hardCodedTest = [1000000000000000000, 1000000000000000000, 10000000000000000000000000000]
    # rates_hardCodedTest = [10000000000000000000000000000, 10000000000000000000000000000, 1000000000000000000]
    # rates_hardCodedTest = [1000000000000000000, 1000000000000000000, 100000000]
    # rates_hardCodedTest = [100000000000000000000, 100000000000000000000000000000000, 100000000000000000000000000000000]
    # rates_hardCodedTest = [100000000000000000000, 100000000000000000000000000000000, 100000000000000000000000000000000, 100000000000000000000]
    # rates_hardCodedTest = [100000000000000000000000000000000, 100000000000000000000, 100000000000000000000, 100000000000000000000000000000000]
    # rates_hardCodedTest = [100000000000000000000, 100000000000000000000000000000000, 100000000000000000000000000000000, 100000000000000000000]
    rates_hardCodedTest = None

    # for 8 decimals it's 18 zeros.  f = decimals + 10 ???
    # for 18 decimals it's 8 zeros.  f = decimals - 10 ????
    # f = 26 - decimals
    # rates_hardCodedTest = None
    poolDataDict = Exchanges.curve.ContractDict_Exchange[poolAddress.lower()]

    subSetOfCurvePoolBalancesDict = Exchanges.curve.API_GetAllTokenBalancesInPool([poolAddress], blockNumber)
    exchangeBalances_weiUnits = subSetOfCurvePoolBalancesDict[poolAddress]

    # resultList_balances = Exchanges.curve.API_GetBalances_Batched([poolAddress], blockNumber)
    PrintAndLog("exchangeBalances_weiUnits = " + str(exchangeBalances_weiUnits))
    # resultList_adminBalances = Exchanges.curve.API_GetAdminBalances_Batched([poolAddress], blockNumber)
    # PrintAndLog("resultList_adminBalances = " + str(resultList_adminBalances))

    # Trying get balances
    # WRONG RESULT
    # distanceFromRightAnswer = 0.000407668928583349
    # exchangeBalances_weiUnits = resultList_balances[0]
    
    # Trying get balances - get admin balances
    # WRONG RESULT
    # distanceFromRightAnswer = 0.0004072525065882404
    # exchangeBalances_weiUnits = []
    # for index, balance in enumerate(resultList_balances[0]):
    #     adminBalance = resultList_adminBalances[0][index]
    #     exchangeBalances_weiUnits.append(balance - adminBalance)

    # Trying actual token balances
    # distanceFromRightAnswer = 0.0004080852876494623
    # tokenAddressList = poolDataDict['tokens'].copy()
    # walletAddressList = [poolAddress] * len(tokenAddressList)
    # PrintAndLog("walletAddressList = " + str(walletAddressList))
    # PrintAndLog("tokenAddressList = " + str(tokenAddressList))
    # exchangeBalances_etherUnits = Libraries.core.API_GetTokenBalance_Batched_Safe(walletAddressList, tokenAddressList, None, None, None, blockNumber)
    # PrintAndLog("exchangeBalances_etherUnits = " + str(exchangeBalances_etherUnits))
    # exchangeBalances_weiUnits = []
    #
    # for index, balance_etherUnits in enumerate(exchangeBalances_etherUnits):
    #     exchangeBalances_weiUnits.append(Libraries.core.ConvertEtherToWei_GivenTokenAddress(balance_etherUnits, tokenAddressList[index]))

    PrintAndLog("exchangeBalances_weiUnits = " + str(exchangeBalances_weiUnits))

    inputAmount_etherUnits = 160
    side = 'buy'
    spendToken, receiveToken, spendTokenId, receiveTokenId = Exchanges.curve.GetTokenFlowGivenQuoteAndBase(
        side, quoteToken, baseToken, poolDataDict)
    inputAmount_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(inputAmount_etherUnits, spendToken)
    PrintAndLog("inputAmount_etherUnits = " + str(inputAmount_etherUnits) + ", spendToken = " + str(spendToken))
    PrintAndLog("inputAmount_weiUnits = " + str(inputAmount_weiUnits) + ", spendToken = " + str(spendToken))
    decimals_outputToken = Libraries.core.GetDecimalsForTokenContract(receiveToken, True)
    # PrintAndLog("decimals_outputToken = " + str(decimals_outputToken))

    outputAmount_weiUnits, outputAmount_etherUnits = Exchanges.curve.API_GetDy(
        poolAddress, spendTokenId, receiveTokenId, inputAmount_weiUnits, decimals_outputToken, blockNumber)
    PrintAndLog("outputAmount_weiUnits calculated by curve = " + str(outputAmount_weiUnits) + ", receiveToken = " + str(receiveToken))
    PrintAndLog("outputAmount_etherUnits calculated by curve = " + str(outputAmount_etherUnits) + ", receiveToken = " + str(receiveToken))
    calculatedByCurve_outputAmount_weiUnits = outputAmount_weiUnits

    price_buy, outputAmount_weiUnits = Exchanges.curve.CalculateTradePrice(side, poolAddress, quoteToken, baseToken,
                                                                           exchangeBalances_weiUnits, inputAmount_weiUnits, False, rates_hardCodedTest)
    outputAmount_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(outputAmount_weiUnits, receiveToken)
    PrintAndLog("price_buy calculated by us " + str(price_buy))
    PrintAndLog("outputAmount_weiUnits calculated by us = " + str(outputAmount_weiUnits) + ", receiveToken = " + str(receiveToken))
    PrintAndLog("outputAmount_etherUnits calculated by us = " + str(outputAmount_etherUnits) + ", receiveToken = " + str(receiveToken))
    calculatedByUs_outputAmount_weiUnits = outputAmount_weiUnits

    PrintAndLog("------------------------------------------------------------------------------------")

    PrintAndLog("calculatedByCurve_outputAmount_weiUnits = " + str(calculatedByCurve_outputAmount_weiUnits))
    PrintAndLog("calculatedByUs_outputAmount_weiUnits = " + str(calculatedByUs_outputAmount_weiUnits))

    var1 = (calculatedByCurve_outputAmount_weiUnits / calculatedByUs_outputAmount_weiUnits)
    PrintAndLog("var1 = " + str(var1))
    distanceFromRightAnswer = abs(var1 - 1)
    PrintAndLog("distanceFromRightAnswer = " + str(distanceFromRightAnswer))

    PrintAndLog("------------------------------------------------------------------------------------")
    PrintAndLog("------------------------------------------------------------------------------------")

    inputAmount_etherUnits = 159.96042682
    side = 'sell'
    spendToken, receiveToken, spendTokenId, receiveTokenId = Exchanges.curve.GetTokenFlowGivenQuoteAndBase(
        side, quoteToken, baseToken, poolDataDict)
    inputAmount_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(inputAmount_etherUnits, spendToken)
    PrintAndLog("inputAmount_etherUnits = " + str(inputAmount_etherUnits) + ", spendToken = " + str(spendToken))
    PrintAndLog("inputAmount_weiUnits = " + str(inputAmount_weiUnits) + ", spendToken = " + str(spendToken))
    decimals_outputToken = Libraries.core.GetDecimalsForTokenContract(receiveToken, True)
    # PrintAndLog("decimals_outputToken = " + str(decimals_outputToken))

    outputAmount_weiUnits, outputAmount_etherUnits = Exchanges.curve.API_GetDy(
        poolAddress, spendTokenId, receiveTokenId, inputAmount_weiUnits, decimals_outputToken, blockNumber)
    PrintAndLog("outputAmount_weiUnits calculated by curve = " + str(outputAmount_weiUnits) + ", receiveToken = " + str(receiveToken))
    PrintAndLog("outputAmount_etherUnits calculated by curve = " + str(outputAmount_etherUnits) + ", receiveToken = " + str(receiveToken))
    calculatedByCurve_outputAmount_weiUnits = outputAmount_weiUnits

    # Machine learning loop to determine what the right answer is

    # PrintAndLog("--------------------------------------BEGIN MACHINE LEARNING----------------------------------------------")
    # best_distanceFromRightAnswer = None
    # best_rates_hardCodedTest = None
    #
    # minDecimals = 16
    # maxDecimals = 41
    # rangeDecimals = maxDecimals - minDecimals
    # # min = 1 * 10 ** minDecimals
    # # max = 1 * 10 ** maxDecimals
    # # PrintAndLog("min = " + str(min))
    # # PrintAndLog("max = " + str(max))
    # arrayLength = len(exchangeBalances_weiUnits)
    # rates_hardCodedTest = [1] * arrayLength
    # PrintAndLog("rates_hardCodedTest = " + str(rates_hardCodedTest))
    # for i in range(rangeDecimals):
    #     for j in range(rangeDecimals):
    #         PrintAndLog("Do stuff with " + str(i) + " and " + str(j))
    #         rates_hardCodedTest[0] = 10 ** (minDecimals + i)
    #         rates_hardCodedTest[1] = 10 ** (minDecimals + j)
    #         PrintAndLog("  rates_hardCodedTest = " + str(rates_hardCodedTest))
    #
    #         try:
    #             price_sell, outputAmount_weiUnits = Exchanges.curve.CalculateTradePrice(side, poolAddress, quoteToken, baseToken,
    #                                                                                     exchangeBalances_weiUnits, inputAmount_weiUnits, False, rates_hardCodedTest)
    #
    #             outputAmount_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(outputAmount_weiUnits, receiveToken)
    #             PrintAndLog("price_sell calculated by us " + str(price_sell))
    #             PrintAndLog("outputAmount_weiUnits calculated by us = " + str(outputAmount_weiUnits) + ", receiveToken = " + str(receiveToken))
    #             PrintAndLog("outputAmount_etherUnits calculated by us = " + str(outputAmount_etherUnits) + ", receiveToken = " + str(receiveToken))
    #             calculatedByUs_outputAmount_weiUnits = outputAmount_weiUnits
    #
    #             PrintAndLog("------------------------------------------------------------------------------------")
    #
    #             PrintAndLog("calculatedByCurve_outputAmount_weiUnits = " + str(calculatedByCurve_outputAmount_weiUnits))
    #             PrintAndLog("calculatedByUs_outputAmount_weiUnits = " + str(calculatedByUs_outputAmount_weiUnits))
    #
    #             var1 = (calculatedByCurve_outputAmount_weiUnits / calculatedByUs_outputAmount_weiUnits)
    #             PrintAndLog("var1 = " + str(var1))
    #             distanceFromRightAnswer = abs(var1 - 1)
    #             PrintAndLog("distanceFromRightAnswer = " + str(distanceFromRightAnswer))
    #
    #             if distanceFromRightAnswer < 0.00005:
    #                 PrintAndLog("Found a decent distanceFromRightAnswer")
    #                 if not best_distanceFromRightAnswer or distanceFromRightAnswer < best_distanceFromRightAnswer:
    #                     best_distanceFromRightAnswer = distanceFromRightAnswer
    #                     best_rates_hardCodedTest = rates_hardCodedTest
    #
    #         except (KeyboardInterrupt, SystemExit):
    #             print('\nkeyboard interrupt caught')
    #             print('\n...Program Stopped Manually!')
    #             raise
    #
    #         except:
    #             PrintAndLogError("exception, " + traceback.format_exc())
    #
    #         PrintAndLog("------------------------------------------------------------------------------------")
    #         PrintAndLog("------------------------------------------------------------------------------------")

    price_sell, outputAmount_weiUnits = Exchanges.curve.CalculateTradePrice(side, poolAddress, quoteToken, baseToken,
                                                                            exchangeBalances_weiUnits, inputAmount_weiUnits, False, rates_hardCodedTest)

    outputAmount_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(outputAmount_weiUnits, receiveToken)
    PrintAndLog("price_sell calculated by us " + str(price_sell))
    PrintAndLog("outputAmount_weiUnits calculated by us = " + str(outputAmount_weiUnits) + ", receiveToken = " + str(receiveToken))
    PrintAndLog("outputAmount_etherUnits calculated by us = " + str(outputAmount_etherUnits) + ", receiveToken = " + str(receiveToken))
    calculatedByUs_outputAmount_weiUnits = outputAmount_weiUnits

    PrintAndLog("------------------------------------------------------------------------------------")

    PrintAndLog("calculatedByCurve_outputAmount_weiUnits = " + str(calculatedByCurve_outputAmount_weiUnits))
    PrintAndLog("calculatedByUs_outputAmount_weiUnits = " + str(calculatedByUs_outputAmount_weiUnits))

    var1 = (calculatedByCurve_outputAmount_weiUnits / calculatedByUs_outputAmount_weiUnits)
    PrintAndLog("var1 = " + str(var1))
    distanceFromRightAnswer = abs(var1 - 1)
    PrintAndLog("distanceFromRightAnswer = " + str(distanceFromRightAnswer))

    PrintAndLog("------------------------------------------------------------------------------------")
    PrintAndLog("------------------------------------------------------------------------------------")

elif sys.argv and len(sys.argv) >= 2 and "testcurve_events" == sys.argv[1].lower():
    # curve_dict = Exchanges.curve.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.curve.API_GetExchangeDataDict")
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()
    # events = Libraries.core.API_GetLogs_Safe(
    #     contractAddress='0xbEbc44782C7dB0a1A60Cb6fe97d0b483032FF1C7',
    #     topics='0x8b3e96f2b889fa771c53c981b40daf005f63f637f1869f707052d15a3dd97140',
    #     fromBlock_int=Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() - 500000,
    #     toBlock_int=Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())          
    tokenExchanges = Exchanges.curve.API_GetExchangeEvents()
    print(tokenExchanges)
    print('done')

# for little random testing that does not need an own sandbox
elif sys.argv and len(sys.argv) >= 2 and "nick" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()
    Libraries.cache.GetAndCache_Curve_API_GetExchangeDataDict()
    df = Exchanges.curve.API_GetExchangeDataDict()
    print(dict)
    print('####################### Test Nick - start - #######################')
    row = df.loc[df['exchange'] == '0xa5407eae9ba41422680e2e00537571bcc53efbfd']
    print(row)
    print('####################### Test Nick - done - #######################')

elif sys.argv and len(sys.argv) >= 2 and "arbalgos_graph" == sys.argv[1].lower():
    sourceTokensList = [
        '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'.lower(),
        '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'.lower(),
        '0x6b175474e89094c44da98b954eedeac495271d0f'.lower(),
        # '0x221657776846890989a759ba2973e427dff5c9bb'.lower(),
    ]

    # UnitTests.ArbAlgos.arbAlgos_testData.PopulateRatesGraphWithTestData_NoArbitrage()
    # # uniqueCycles = Libraries.arbAlgos.find_arbitrage(Exchanges.ninja.RatesGraph, Libraries.ratesGraph.RatesGraphTokenList, True, None)
    # # Arbitrage_NewTest(Libraries.ratesGraph.RatesGraphTokenList, Exchanges.ninja.RatesGraph)
    # # PrintAndLog("result = " + str(result))
    # ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken = Libraries.ratesGraph.GetRatesGraphDicts(1, '1')
    # Libraries.arbAlgos.BruteForceArbitrage(ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken,
    #                                        Libraries.ratesGraph.RatesGraphTokenList, sourceTokensList, id)
    # PrintAndLog("This should NOT have found arbitrage. ratesGraph = " + str(ratesGraph) + ", Libraries.ratesGraph.RatesGraphTokenList = " + str(Libraries.ratesGraph.RatesGraphTokenList))

    UnitTests.ArbAlgos.arbAlgos_testData.PopulateRatesGraphWithTestData_YesArbitrage_2LegTrade()
    # uniqueCycles = Libraries.arbAlgos.find_arbitrage(Exchanges.ninja.RatesGraph, Libraries.ratesGraph.RatesGraphTokenList, True, None)
    # Arbitrage_NewTest(Libraries.ratesGraph.RatesGraphTokenList, Exchanges.ninja.RatesGraph)
    # PrintAndLog("result = " + str(result))
    ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken = Libraries.ratesGraph.GetRatesGraphDicts(1, '1')
    Libraries.arbAlgos.BruteForceArbitrage(ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken,
                                           Libraries.ratesGraph.RatesGraphTokenList, sourceTokensList, 'id-1')
    # PrintAndLog("This SHOULD have found arbitrage")

    ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken = Libraries.ratesGraph.GetRatesGraphDicts(1, '15')
    Libraries.arbAlgos.BruteForceArbitrage(ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken,
                                           Libraries.ratesGraph.RatesGraphTokenList, sourceTokensList, 'id-15')

    # UnitTests.ArbAlgos.arbAlgos_testData.PopulateRatesGraphWithTestData_YesArbitrage_3LegTrade()
    # # uniqueCycles = Libraries.arbAlgos.find_arbitrage(Exchanges.ninja.RatesGraph, Libraries.ratesGraph.RatesGraphTokenList, True, None)
    # # Arbitrage_NewTest(Libraries.ratesGraph.RatesGraphTokenList, Exchanges.ninja.RatesGraph)
    # # PrintAndLog("result = " + str(result))
    # ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken = Libraries.ratesGraph.GetRatesGraphDicts(1, '1')
    # Libraries.arbAlgos.BruteForceArbitrage(ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken,
    #                                        Libraries.ratesGraph.RatesGraphTokenList, sourceTokensList, id)
    # PrintAndLog("This SHOULD have found arbitrage")

    # UnitTests.ArbAlgos.arbAlgos_testData.PopulateRatesGraphWithTestData_YesArbitrage_FuckedUpDataSetHopefullyWithBug()
    # # uniqueCycles = Libraries.arbAlgos.find_arbitrage(Exchanges.ninja.RatesGraph, Libraries.ratesGraph.RatesGraphTokenList, True, None)
    # # Arbitrage_NewTest(Libraries.ratesGraph.RatesGraphTokenList, Exchanges.ninja.RatesGraph)
    # # PrintAndLog("result = " + str(result))
    # # BruteForceArbitrage(graph_rates, graph_exchangeNames, tokenList, sourceTokensList, id,
    # ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken = Libraries.ratesGraph.GetRatesGraphDicts(1, '1')
    # Libraries.arbAlgos.BruteForceArbitrage(ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken,
    #                                        Libraries.ratesGraph.RatesGraphTokenList, sourceTokensList, id)
    # PrintAndLog("This should NOT have found arbitrage. ratesGraph = " + str(ratesGraph) + ", Libraries.ratesGraph.RatesGraphTokenList = " + str(Libraries.ratesGraph.RatesGraphTokenList))

    PrintAndLog("This SHOULD have found several sets of arbitrage")

    # Arbitrage_NewTest(Libraries.ratesGraph.RatesGraphTokenList, Exchanges.ninja.RatesGraph)

    # result = arbitrage(Exchanges.ninja.RatesGraph)
    # PrintAndLog("result = " + str(result))

    # rates = [
    #     [1, 0.23, 0.25, 16.43, 18.21, 4.94],
    #     [4.34, 1, 1.11, 71.40, 79.09, 21.44],
    #     [3.93, 0.90, 1, 64.52, 71.48, 19.37],
    #     [0.061, 0.014, 0.015, 1, 1.11, 0.30],
    #     [0.055, 0.013, 0.014, 0.90, 1, 0.27],
    #     [0.20, 0.047, 0.052, 3.33, 3.69, 1],
    # ]
    #
    # currencies = ('PLN', 'EUR', 'USD', 'RUB', 'INR', 'MXN')
    #
    # Arbitrage_NewTest(currencies, rates)

elif sys.argv and len(sys.argv) >= 2 and "test_trace_transaction" == sys.argv[1].lower():
    Libraries.core.API_GetTransactionTrace(sys.argv[2])

elif sys.argv and len(sys.argv) >= 2 and "test_graph_1" == sys.argv[1].lower():
    # This approach simply uses graph.max(axis=0) where the graph is a 2d array of numbers
    # This is by far the most performant but note that it doest not solve my problem because I need to track more than 1 property
    # I actually need rate + a few other properties, where as this only tracks rate...
    durationsList = []
    runs = 200
    for i in range(runs):
        before = datetime.datetime.now()
        graph = np.ndarray(shape=(4, 3, 3), dtype=float, order='F')
        graph[0] = [[0, 0, 1], [1, 0, 1], [2, 0, 0]]
        graph[1] = [[0, 0, 1], [1, 0, 1], [2, 0, 0]]
        graph[2] = [[5, 0, 0], [1, 0, 1], [2, 0, 0]]
        graph[3] = [[2, 1, 0], [9, 0, 1], [0, 0, 0]]

        PrintAndLog("graph of type " + str(type(graph)) + " = \n" + str(graph))
        PrintAndLog("\n\n")
        resultGraph = graph.max(axis=0)
        duration_s = (datetime.datetime.now() - before).total_seconds()
        durationsList.append(duration_s)
        PrintAndLog("Completed with duration_s = " + str(duration_s) + " seconds")
        PrintAndLog("resultGraph of type " + str(type(resultGraph)) + " = \n" + str(resultGraph))

    averageDuration_s = Libraries.utils.AverageListContents(Libraries.utils.RemoveOutliersFromList(durationsList))
    PrintAndLog("averageDuration_s = " + str(averageDuration_s))

elif sys.argv and len(sys.argv) >= 2 and "test_graph_2a" == sys.argv[1].lower():
    from Libraries.ratesGraph import ExchangeRate

    # This approach uses the object ExchangeRate which has the __ge__ implementation so I can still use graph.max(axis=0) to merge the graphs
    # This also uses np.array for only the merged graph, but not the original graphs. The original graphs use lists instead
    durationsList = []
    runs = 200
    for i in range(runs):
        before = datetime.datetime.now()
        graph1 = [[ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)], [ExchangeRate(1, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(1, 0, 1)],
                  [ExchangeRate(2, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)]]
        graph2 = [[ExchangeRate(5, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)], [ExchangeRate(1, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(1, 0, 1)],
                  [ExchangeRate(2, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)]]
        graph3 = [[ExchangeRate(2, 0, 1), ExchangeRate(1, 0, 1), ExchangeRate(0, 0, 1)], [ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(1, 0, 1)],
                  [ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)]]
        graph4 = [[ExchangeRate(1, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(1, 0, 1)], [ExchangeRate(9, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)],
                  [ExchangeRate(2, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)]]
        mergedGraph = np.array([graph1, graph2, graph3, graph4]).max(axis=0)
        duration_s = (datetime.datetime.now() - before).total_seconds()
        durationsList.append(duration_s)
        PrintAndLog("Completed with duration_s = " + str(duration_s) + " seconds")

        # PrintAndLog("mergedGraph = \n" + str(mergedGraph))
        printString = ''
        for row in mergedGraph:
            for col in row:
                printString += str(col.rate) + ", "
            printString += "\n"

        PrintAndLog("printString = \n" + str(printString))

    averageDuration_s = Libraries.utils.AverageListContents(Libraries.utils.RemoveOutliersFromList(durationsList))
    PrintAndLog("averageDuration_s = " + str(averageDuration_s))

elif sys.argv and len(sys.argv) >= 2 and "test_graph_2b" == sys.argv[1].lower():
    from Libraries.ratesGraph import ExchangeRate

    # This approach uses the object ExchangeRate which has the __ge__ implementation so I can still use graph.max(axis=0) to merge the graphs
    # This is also using np.array for every array in the graph
    durationsList = []
    runs = 200
    for i in range(runs):
        before = datetime.datetime.now()
        graph1 = np.array([np.array([ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)]), np.array([ExchangeRate(1, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(1, 0, 1)]),
                           np.array([ExchangeRate(2, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)])])
        graph2 = np.array([np.array([ExchangeRate(5, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)]), np.array([ExchangeRate(1, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(1, 0, 1)]),
                           np.array([ExchangeRate(2, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)])])
        graph3 = np.array([np.array([ExchangeRate(2, 0, 1), ExchangeRate(1, 0, 1), ExchangeRate(0, 0, 1)]), np.array([ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(1, 0, 1)]),
                           np.array([ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)])])
        graph4 = np.array([np.array([ExchangeRate(1, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(1, 0, 1)]), np.array([ExchangeRate(9, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)]),
                           np.array([ExchangeRate(2, 0, 1), ExchangeRate(0, 0, 1), ExchangeRate(0, 0, 1)])])
        mergedGraph = np.array([graph1, graph2, graph3, graph4]).max(axis=0)
        duration_s = (datetime.datetime.now() - before).total_seconds()
        durationsList.append(duration_s)
        PrintAndLog("Completed with duration_s = " + str(duration_s) + " seconds")

        # PrintAndLog("mergedGraph = \n" + str(mergedGraph))
        printString = ''
        for row in mergedGraph:
            for col in row:
                printString += str(col.rate) + ", "
            printString += "\n"

        PrintAndLog("printString = \n" + str(printString))

    averageDuration_s = Libraries.utils.AverageListContents(Libraries.utils.RemoveOutliersFromList(durationsList))
    PrintAndLog("averageDuration_s = " + str(averageDuration_s))

elif sys.argv and len(sys.argv) >= 2 and "test_graph_3" == sys.argv[1].lower():
    # This approach uses a brute force merge method where there are 3 graphs, one graph per property of interest
    # notice when merging, i'm manually merging based on rate but also having the other graphs follow along with how rate merges
    # This approach only makes use of numpy for np.ndarray but not graph.max(axis=0)
    durationsList = []
    runs = 200
    for i in range(runs):
        before = datetime.datetime.now()
        graph_rate = np.ndarray(shape=(4, 3, 3), dtype=float, order='F')
        graph_rate[0] = [[0, 0, 1], [1, 0, 1], [2, 0, 0]]
        graph_rate[1] = [[0, 0, 1], [1, 0, 1], [2, 0, 0]]
        graph_rate[2] = [[5, 0, 0], [1, 0, 1], [2, 0, 0]]
        graph_rate[3] = [[2, 1, 0], [9, 0, 1], [0, 0, 0]]

        graph_name = np.ndarray(shape=(4, 3, 3), dtype=float, order='F')
        graph_name[0] = [[0, 0, 1], [1, 0, 1], [2, 0, 0]]
        graph_name[1] = [[0, 0, 1], [1, 0, 1], [2, 0, 0]]
        graph_name[2] = [[5, 0, 0], [1, 0, 1], [2, 0, 0]]
        graph_name[3] = [[2, 1, 0], [9, 0, 1], [0, 0, 0]]

        graph_otherUsefulProperty = np.ndarray(shape=(4, 3, 3), dtype=float, order='F')
        graph_otherUsefulProperty[0] = [[0, 0, 1], [1, 0, 1], [2, 0, 0]]
        graph_otherUsefulProperty[1] = [[0, 0, 1], [1, 0, 1], [2, 0, 0]]
        graph_otherUsefulProperty[2] = [[5, 0, 0], [1, 0, 1], [2, 0, 0]]
        graph_otherUsefulProperty[3] = [[2, 1, 0], [9, 0, 1], [0, 0, 0]]

        # Brute force merge based on rate, but bringing other graph's data along
        resultGraph_rate = None
        resultGraph_name = None
        resultGraph_otherUsefulProperty = None

        for index, sub_graph_rate in enumerate(graph_rate):
            sub_graph_name = graph_name[index]
            sub_graph_otherUsefulProperty = graph_otherUsefulProperty[index]

            if index == 0:
                resultGraph_rate = sub_graph_rate
                resultGraph_name = sub_graph_name
                resultGraph_otherUsefulProperty = sub_graph_otherUsefulProperty
            else:
                for index_row, row in enumerate(sub_graph_rate):
                    for index_col, col in enumerate(row):
                        if sub_graph_rate[index_row][index_col] > resultGraph_rate[index_row][index_col]:
                            PrintAndLog("Found new max")
                            resultGraph_rate[index_row][index_col] = sub_graph_rate[index_row][index_col]
                            resultGraph_name[index_row][index_col] = sub_graph_name[index_row][index_col]
                            resultGraph_otherUsefulProperty[index_row][index_col] = sub_graph_otherUsefulProperty[index_row][index_col]

        duration_s = (datetime.datetime.now() - before).total_seconds()
        durationsList.append(duration_s)
        PrintAndLog("Completed with duration_s = " + str(duration_s) + " seconds")
        PrintAndLog("resultGraph_rate of type " + str(type(resultGraph_rate)) + " = \n" + str(resultGraph_rate))

    averageDuration_s = Libraries.utils.AverageListContents(Libraries.utils.RemoveOutliersFromList(durationsList))
    PrintAndLog("averageDuration_s = " + str(averageDuration_s))

elif sys.argv and len(sys.argv) >= 2 and "test_graph_4" == sys.argv[1].lower():
    # This approach uses a brute force merge method where there are 3 graphs, one graph per property of interest
    # notice when merging, i'm manually merging based on rate but also having the other graphs follow along with how rate merges
    # This approach does not use numpy at all, only using python lists
    durationsList = []
    runs = 200
    for i in range(runs):
        before = datetime.datetime.now()
        graph_rate = []
        graph_rate.append([[0, 0, 1], [1, 0, 1], [2, 0, 0]])
        graph_rate.append([[0, 0, 1], [1, 0, 1], [2, 0, 0]])
        graph_rate.append([[5, 0, 0], [1, 0, 1], [2, 0, 0]])
        graph_rate.append([[2, 1, 0], [9, 0, 1], [0, 0, 0]])

        graph_name = []
        graph_name.append([[0, 0, 1], [1, 0, 1], [2, 0, 0]])
        graph_name.append([[0, 0, 1], [1, 0, 1], [2, 0, 0]])
        graph_name.append([[5, 0, 0], [1, 0, 1], [2, 0, 0]])
        graph_name.append([[2, 1, 0], [9, 0, 1], [0, 0, 0]])

        graph_otherUsefulProperty = []
        graph_otherUsefulProperty.append([[0, 0, 1], [1, 0, 1], [2, 0, 0]])
        graph_otherUsefulProperty.append([[0, 0, 1], [1, 0, 1], [2, 0, 0]])
        graph_otherUsefulProperty.append([[5, 0, 0], [1, 0, 1], [2, 0, 0]])
        graph_otherUsefulProperty.append([[2, 1, 0], [9, 0, 1], [0, 0, 0]])

        # Brute force merge based on rate, but bringing other graph's data along
        resultGraph_rate = None
        resultGraph_name = None
        resultGraph_otherUsefulProperty = None

        for index, sub_graph_rate in enumerate(graph_rate):
            sub_graph_name = graph_name[index]
            sub_graph_otherUsefulProperty = graph_otherUsefulProperty[index]

            if index == 0:
                resultGraph_rate = sub_graph_rate
                resultGraph_name = sub_graph_name
                resultGraph_otherUsefulProperty = sub_graph_otherUsefulProperty
            else:
                for index_row, row in enumerate(sub_graph_rate):
                    for index_col, col in enumerate(row):
                        if sub_graph_rate[index_row][index_col] > resultGraph_rate[index_row][index_col]:
                            PrintAndLog("Found new max")
                            resultGraph_rate[index_row][index_col] = sub_graph_rate[index_row][index_col]
                            resultGraph_name[index_row][index_col] = sub_graph_name[index_row][index_col]
                            resultGraph_otherUsefulProperty[index_row][index_col] = sub_graph_otherUsefulProperty[index_row][index_col]

        duration_s = (datetime.datetime.now() - before).total_seconds()
        durationsList.append(duration_s)
        PrintAndLog("Completed with duration_s = " + str(duration_s) + " seconds")
        PrintAndLog("resultGraph_rate of type " + str(type(resultGraph_rate)) + " = \n" + str(resultGraph_rate))

    averageDuration_s = Libraries.utils.AverageListContents(Libraries.utils.RemoveOutliersFromList(durationsList))
    PrintAndLog("averageDuration_s = " + str(averageDuration_s))

elif sys.argv and len(sys.argv) >= 2 and "test_mempool_comparison" == sys.argv[1].lower():
    PrintAndLog("Not entering main loop, do some functional testing here!")

    dataDict = {'0x18b53d0e3b90aeba64fe62e664680260bd8e7c8b2adbd4b4ebcaa3ecf5c7798c': {'blocknative': 0},
                '0x3cd82e46067219e4a34ea5f865bd7a21523b6d2b72dbbad2c211d256b072055a': {'blocknative': 0},
                '0x2ef05b1f38dbd525c12b06b5eff3bb058d81eeb84f06593d570c7b56997bf33b': {'blocknative': 0},
                '0xf326f98be371f62d8686a6753ea20045d30cb80de5681437dcc59f618cc5151b': {'blocknative': 0},
                '0xe42ab4baa4f514b861130c7ccd7dc973b0084d18bd886b6701b77b8dfeae7687': {'blocknative': 0},
                '0x64551a17fb685218746924f457d739a0b2f8783b7cba9799f717a842a6cfab03': {'blocknative': 0, 'alchemy': 1703.7},
                '0xf9e387ee2a23c441483d20e3c965bbb12035ff84a6a039f15882b18d570ac89d': {'bloxroute': 0, 'blocknative': 203.6, 'alchemy': 1550.1},
                '0x5f94fc355da7767bcfa698fa47f433b8b5e4b07dd069f02296996f71a1c05740': {'bloxroute': 0, 'blocknative': 234.3, 'alchemy': 1075.6},
                '0xe50cf4911c029bd19edd606f23b50958feae743d51bb0c2b5f6425f4d1922f75': {'bloxroute': 0, 'blocknative': 192.3, 'alchemy': 994.8},
                '0xad9faa5aa59286e27096fdf58b0f836df9b2b0228d01ca9020fb3626edd6c4ea': {'bloxroute': 0, 'blocknative': 178.0, 'alchemy': 1594.3},
                '0x737f651d45f3e265bc19c7e9215c8b29d98e099452f5203303bfe91fb7d0d94c': {'bloxroute': 0, 'blocknative': 149.6, 'alchemy': 1243.5},
                '0xabbcae1558194a413ccecc3384db837509ecca5031c1677da5d7a788d301b844': {'bloxroute': 0, 'blocknative': 182.2, 'alchemy': 2046.8},
                '0xc68563abd52d309d5734bf7cb89c30bb8f6ecb33169994aa131d3381c24af55f': {'bloxroute': 0, 'blocknative': 190.3, 'alchemy': 3012.5},
                '0xcd801719e837be27a2262cf12a93b5b14d2a5e94d31eec437351e49f7d71e5ce': {'bloxroute': 0, 'blocknative': 183.8, 'alchemy': 2878.4},
                '0xf0e8fd6210fa2b214467ad4a4f24932643e0c4e03175da5c4cc79f9d5b4fe23c': {'bloxroute': 0, 'blocknative': 194.1, 'alchemy': 2655.2},
                '0xedcdac3725f90374c2df8e8a50b20290376a25d45ed5e83c91e14497ad31c886': {'bloxroute': 0, 'blocknative': 245.8, 'alchemy': 1446.8},
                '0xd7f58b648115613d31e29891d964d9ff379b3928b0912ad0f730dcd199a35ded': {'bloxroute': 0, 'blocknative': 238.3, 'alchemy': 894.9},
                '0x0da84a62d185d6b1bee1bf7e91a536e3e47b53b98ca8fdf43c5afdc5eb3b70c6': {'bloxroute': 0, 'blocknative': 471.8, 'alchemy': 1882.4},
                '0x7530b46264f6eebd6b4424d7a73f2b60f42010c4259f743191002fc7d7690216': {'bloxroute': 0, 'alchemy': 1037.7, 'blocknative': 1673.8},
                '0xfa06bf0bb4259cee962a1ca9d63b938d425fd426661887e93b74c2a5a2d20cd1': {'blocknative': 0, 'alchemy': 661.3},
                '0x9290566f2a793daab97408fbb1a00091d36241f3e870e2e30905b0fdc8d4943b': {'blocknative': 0, 'bloxroute': 75.7, 'alchemy': 160.5},
                '0x9428cf818a84def18b2b137d73beb68c438dd5a0c80c2dca9b419890d25224d1': {'blocknative': 0, 'bloxroute': 488.8, 'alchemy': 617.4},
                '0x8a7ae5b45a78806271aa5384f39f755c529d7d1293ee94b41c4d1a9fa4fb8cfd': {'bloxroute': 0, 'blocknative': 131.0, 'alchemy': 1305.6},
                '0x13065cda0dbd41a7f744156e0b35890ea18495a2847732baeeb824b186b0676c': {'bloxroute': 0, 'blocknative': 54.9, 'alchemy': 557.1},
                '0xf2eca118cb7f797acce5a5df2480c3e2d91a84e1fbcd90d6deefec64746783d7': {'blocknative': 0, 'bloxroute': 232.2, 'alchemy': 1170.2},
                '0xe49f3af8e17098bdf1a3f7aefea8273b6a07bf9c1c981f1fb5dc4f972eef61d1': {'bloxroute': 0, 'blocknative': 159.5, 'alchemy': 775.7},
                '0xe64f406521228f31db59be462783bdd8c25c36d40ccfee3f1112634dd3283092': {'bloxroute': 0, 'blocknative': 193.8, 'alchemy': 3852.1},
                '0x02a7fb503ffbf0d637a81d37d0876284f31820b0478182042b951583913cabc0': {'bloxroute': 0, 'blocknative': 149.8, 'alchemy': 752.6},
                '0x1e3c3696fcafd9560f4052f3ff375a6f330a56fa2f617bb010925ac21f16b628': {'bloxroute': 0, 'blocknative': 135.2, 'alchemy': 849.4},
                '0x56c14c1085d1b64503f369c35977c7f381f024df1006802bd113e55bdc3bfd45': {'bloxroute': 0, 'blocknative': 128.8, 'alchemy': 1763.7},
                '0x9a077c4de89ac5c4eaebbfe5e3e03583d824985800ce55187d24938ad01d3e60': {'bloxroute': 0, 'blocknative': 119.8, 'alchemy': 895.3},
                '0x9f57edf08d9652beb9ce05e768abf5a1872923c7affebcec27a7cc00aa7ed8ea': {'bloxroute': 0, 'blocknative': 177.8, 'alchemy': 685.5},
                '0x0bc9c69cd775d9eea5fcc0b4c84d8adfd4039431c07e7b0ddc266876baebc175': {'bloxroute': 0, 'blocknative': 181.5, 'alchemy': 1434.2},
                '0x5ca92c22debdc7abb7f69f47e737486b818a56d0020a6a6906503d13dd789e07': {'bloxroute': 0, 'blocknative': 183.9, 'alchemy': 1339.0},
                '0xc3aeaa22f4226a08022553ef746c62b0feefb694677a0f568603bc7e3626b0f6': {'bloxroute': 0, 'blocknative': 130.7, 'alchemy': 2165.2},
                '0x81afed127e04f524587648f4ea456ce89c3c3b670989126e7e37bc3f3819404a': {'bloxroute': 0, 'blocknative': 179.8, 'alchemy': 1517.1},
                '0xb69bbe67e4a1de099e3f29b81225d54359ff988a529229091a6c8946f9a086ef': {'bloxroute': 0, 'blocknative': 184.7, 'alchemy': 1246.3},
                '0x1ced5232e0103d94533d25e364e603187f4b7a08100d71f85b20bc8cb6e92ff4': {'bloxroute': 0, 'blocknative': 188.4, 'alchemy': 1178.8},
                '0xc907be67df79501e1d34261af3bd35e55c30d7e03e6e73091f933db0ad965d6f': {'bloxroute': 0, 'blocknative': 118.8, 'alchemy': 2113.2},
                '0xb464f0d4208f2eb3c059ec891b8a8bd6c1366bc13f47f230a318f2e68ac5365f': {'bloxroute': 0, 'blocknative': 188.1, 'alchemy': 1005.9},
                '0x7b0ab8dcd30a28079e38a2d64aebc086ec9ea09e876aa7a64dc541f1ba22a523': {'bloxroute': 0, 'blocknative': 157.5, 'alchemy': 1608.0},
                '0x4d88a7fadf306bcdc522d2ddfb71ab605fb6cbc01582f775ca23458990642552': {'bloxroute': 0, 'blocknative': 147.8, 'alchemy': 1352.3},
                '0xf21716a6792ae7a966746d201ad0df9e45f8348132bae731795ae8283a802701': {'bloxroute': 0, 'blocknative': 191.6, 'alchemy': 1936.4},
                '0x0e96fa83c6f6c93542c3cd668eec81fcf0ba72a913cd6255e65d60f311124602': {'bloxroute': 0, 'blocknative': 153.4, 'alchemy': 1663.7},
                '0xc37a2e35c4bc1e1245015a3fcf9d0ff7399ce1ed021e9bcaaa38fe485bd8831b': {'bloxroute': 0, 'blocknative': 212.4, 'alchemy': 1374.5},
                '0x79138e92eee8422633fc308550b72429496cc6fd4367566d244903fb5faa985c': {'bloxroute': 0, 'blocknative': 439.9, 'alchemy': 918.7},
                '0x01caaed8636d4328411a093bca7ca0d793f54d5921fc27f48db67322b767a9f5': {'bloxroute': 0, 'blocknative': 217.7, 'alchemy': 1058.0},
                '0xb1f343b6e45314dba2ba7c5f2d367ad4aa51296628c969e36d4697535c56eb34': {'bloxroute': 0, 'blocknative': 118.0, 'alchemy': 817.8},
                '0xfdec5538d12f5e240e5358565eb2dfc4d75794888f926ce6e40ca67c9108cd2f': {'bloxroute': 0, 'blocknative': 76.2, 'alchemy': 2343.6},
                '0xa8dffb2def4e48a9d7d225a3ab08521fdaa358d48fc174c4cf9c7a30abb83dc2': {'bloxroute': 0, 'blocknative': 194.7, 'alchemy': 2049.4},
                '0x608661a07285b8c765fba62855aba52e50321c628685a058f099cb8d3041d8c8': {'bloxroute': 0, 'blocknative': 70.0, 'alchemy': 1637.9},
                '0x4cac1ced8bf1b61ad48e32548b3cfe2a45ac5165713cc8bd634a13b3826ae214': {'bloxroute': 0, 'blocknative': 153.5, 'alchemy': 1942.1},
                '0x72c5764d10610aa63146b2d403a1def684671741390ac0562a3541aecf3ee757': {'bloxroute': 0, 'blocknative': 176.6, 'alchemy': 993.0},
                '0x66e04218ef7fe6503945d2bf26c1859947a1d7bab05762c51b31b64408bc0115': {'bloxroute': 0, 'blocknative': 226.1, 'alchemy': 2369.8},
                '0xd257321ed267403cdc32b9baf3b011b662c12f9f68788715b28f1811485fcc34': {'bloxroute': 0, 'blocknative': 193.0, 'alchemy': 2277.4},
                '0x42003f55c769e6da09e2d285364fcd61a2aa54924eae8345c19edd8cc08fac04': {'bloxroute': 0, 'blocknative': 205.8, 'alchemy': 2883.2},
                '0xe0db9017aa50e7baa98528bb9652012677d548fb83a25934b912db5fe3078391': {'bloxroute': 0, 'blocknative': 129.2, 'alchemy': 2444.7},
                '0xe0b1afb8600c5857b76b7019d6d7d78d2aad1ed6668dcb7dc3496fda834dd7a9': {'bloxroute': 0, 'blocknative': 169.2, 'alchemy': 1896.6},
                '0xd1fc74574226a8efdcd437c794d81e8b31f0d604bb130e373ee9279af4bc7a0d': {'bloxroute': 0, 'blocknative': 128.7, 'alchemy': 1835.5},
                '0xa81d36515799f693775c70671121ad74a78672fe08fa1546af3ac84e0ef06623': {'bloxroute': 0, 'blocknative': 222.0, 'alchemy': 1751.7},
                '0x40de26a18777805b0abf69f4b0c9d1cf3846d537e53ffde7b5514a8684975d7e': {'bloxroute': 0, 'blocknative': 195.5, 'alchemy': 1442.8},
                '0xe90308dcac44d7f0ba851f27a5620ba7ba3d0b5f69ec2481e8fdc117ed5a7fb0': {'bloxroute': 0, 'blocknative': 212.5, 'alchemy': 1049.1},
                '0xa8b4a383f5e8d89cff341a02d7bef9cc2bbc455cf33c3d2ce0a9ae2ac67d1e7b': {'bloxroute': 0, 'blocknative': 228.5, 'alchemy': 1537.4},
                '0x2e1f1108d2fa991f19c0ab76e287ba6e6fee258f8eadc6e8a8bc0796d95aca67': {'bloxroute': 0, 'blocknative': 138.5, 'alchemy': 2369.5},
                '0xf48e4a52e213de59565492e770696a1e3fa7f8184da1ac92675a6435a6b2610b': {'bloxroute': 0, 'blocknative': 188.7, 'alchemy': 999.9},
                '0x3d8c050a39132e9108cd0016b0115c24b1080a6334dcce49d89197c463628e78': {'bloxroute': 0, 'blocknative': 136.0, 'alchemy': 1241.7},
                '0x82be667cdbef7c386b05e1c1c5483744221fd2ff6dda7bd4214e4c1d41b4a208': {'bloxroute': 0, 'blocknative': 150.5, 'alchemy': 2196.4},
                '0x4f0a8d267230035489d8dd0241a9cf9041e5d3ca05475d03778764ccd81f977d': {'bloxroute': 0, 'blocknative': 169.5, 'alchemy': 2922.3},
                '0xe67ba65082e8595a3545318254000dd6182f53181c15cddbdbba2cfc103f5988': {'bloxroute': 0, 'blocknative': 172.0, 'alchemy': 2543.3},
                '0x283371038f021c71467381fc9af68e92ca7a4e02a879053e822c56d9ca9b3388': {'bloxroute': 0, 'blocknative': 202.2, 'alchemy': 2417.2},
                '0xbfd4e87aed9f47bc522981be4ebc8ae06a6f51bfc4c631e27a0f52ae0dcb82fc': {'bloxroute': 0, 'blocknative': 123.4, 'alchemy': 1601.0},
                '0xcd35686d435aa6e086669c2dcf10c1ab763fb0564ec129bef94f1614563e7bd6': {'bloxroute': 0, 'blocknative': 169.1, 'alchemy': 1446.6},
                '0xeb1d8598db75408a6db333618d88cd8c127d042a5ad57881c9a8eefcdc5a67e7': {'bloxroute': 0, 'blocknative': 200.4, 'alchemy': 1768.9},
                '0x5e37ee8fbbdfb936a2c7cf0444572a9f5e474523e2e60a667e37ac2861449aaf': {'bloxroute': 0, 'blocknative': 194.6, 'alchemy': 1669.8},
                '0xed83c613ade20248ea46df53109995501953f0256f8c9b2e000333f3e56d48b8': {'bloxroute': 0, 'blocknative': 174.5, 'alchemy': 1426.0},
                '0x6581e80d872b65ed3618afde4f84c9a8955277437837fba0ffd736f45239c25e': {'bloxroute': 0, 'blocknative': 162.9, 'alchemy': 1067.9},
                '0x25f0bb9eb25b7ad84c864214f71a95b6ce2858901e0788c7e71d84d60fd4aba7': {'bloxroute': 0, 'blocknative': 95.9, 'alchemy': 1225.6},
                '0xd45461798b91f09161ad8a548de8dff9c102c367a2d5c5c9b086429d148065c4': {'bloxroute': 0, 'blocknative': 158.4, 'alchemy': 2174.5},
                '0x225be98f51142562c16d82f790e426ebd418aae281b750ed4f6ff686a20ef67f': {'bloxroute': 0, 'blocknative': 235.1, 'alchemy': 1483.5},
                '0xfadc149d5928ba372f712afbf81eb338a5cf77fe17cb69328520c57a2b4a763b': {'bloxroute': 0, 'blocknative': 111.3, 'alchemy': 3244.8},
                '0x7209e0aa378c6f5a18685d949b2137b905c9ebc9a57b93fe5773e610ca2a3e09': {'bloxroute': 0, 'blocknative': 174.0, 'alchemy': 1712.9},
                '0x258cdf8fa7a3aa2a20f7511853160518a32a68ef32b87abd7b1c1ad8081bfb81': {'bloxroute': 0, 'blocknative': 193.4, 'alchemy': 1065.3},
                '0x341319c145df55ad38ba3cb005df96a5494c39747313f4cfc4de7d3b3658c485': {'bloxroute': 0, 'blocknative': 173.7, 'alchemy': 2170.7},
                '0x4d906a8ae2e28403dd29f88a409e8ee3a1306aa91aa15d188d6348bed70bd335': {'bloxroute': 0, 'blocknative': 139.1, 'alchemy': 1795.0},
                '0x0c08ddc48efc4f259feb530869977d10cb4c0724cc5260a6b956116f65e22e57': {'bloxroute': 0, 'blocknative': 184.6, 'alchemy': 3143.7},
                '0x34ddbd2c0668abc78f84accd02233395c3ca857dd00c0693b31e4461bec37d76': {'bloxroute': 0, 'blocknative': 156.9, 'alchemy': 2719.7},
                '0x68113d7e2f4782e72638f7442752e68501254d8fb7578158449143d2ea779e33': {'bloxroute': 0, 'blocknative': 120.5, 'alchemy': 1681.8},
                '0xa9673196a90d8372549544834ef1190cf06ee05e47acc582bd3923b49480ea73': {'bloxroute': 0, 'blocknative': 178.9, 'alchemy': 1375.3},
                '0xcc4622caed5eea0848cd6821c8b49e97e59fcd9144fc47a80f59c3365fe9879d': {'bloxroute': 0, 'blocknative': 172.9, 'alchemy': 2140.9},
                '0xab593573243f44945a6476b48f23c63ae26824d1e2c39b2e61636fc14440e4b3': {'bloxroute': 0, 'blocknative': 195.7, 'alchemy': 1968.0},
                '0x2ef45a2d75e1452b68e2e5c0b920de9ea25c9beb5eb3ec9a21b513ace7857fcd': {'bloxroute': 0, 'blocknative': 182.3, 'alchemy': 1507.9},
                '0x9761331332e62d1f283b5cca1dda1020c5bd381c884d545f2e1e80ca47e3652a': {'bloxroute': 0, 'blocknative': 191.6, 'alchemy': 1349.6},
                '0xe907d63c0b588fbaacee7c70bcb31e812b03cd938bae458267c7f64273fbac73': {'bloxroute': 0, 'blocknative': 88.1, 'alchemy': 1018.8},
                '0x60c6d2657692f30e69c9fffd94088d87fcf505aa0c297fa9981bd94f81834e32': {'bloxroute': 0, 'blocknative': 187.1, 'alchemy': 1477.2},
                '0x68445904fba25253cf30655260c8481aa1c6666cb1bb36b186ca9ace86299b84': {'bloxroute': 0, 'blocknative': 175.0, 'alchemy': 1332.2},
                '0x2538eb93b401ed7b558f2d3c13d1c619df4d94507d6689a67d5bc8ca27e50b56': {'bloxroute': 0, 'blocknative': 159.9, 'alchemy': 1421.1},
                '0x4cf593a7377970f1cb42e99190834185a775fface3952fc31a690d86011cf7ef': {'bloxroute': 0, 'blocknative': 392.6, 'alchemy': 0},
                '0xb0f893c227b684cacec5e8a1a4708dffbd43644a9a0e9046d2ccd028ec93af83': {'bloxroute': 0, 'blocknative': 319.7, 'alchemy': 953.1},
                '0xd6b42738afcb51867130548b44674426f83e0037e5888290775d77d0ed5fd60f': {'bloxroute': 0, 'blocknative': 387.7, 'alchemy': 539.5},
                '0xab4d779c226d993d28223fbcc50c4788381fa68286e7566eef3e8a6606bdbb00': {'bloxroute': 0, 'blocknative': 47.3, 'alchemy': 1781.9},
                '0x96b6244d241b80227ef4daa45c00c9a7345b2a6a36707bd630acb892ecdd23ce': {'bloxroute': 0, 'alchemy': 665.9, 'blocknative': 1604.3},
                '0xa1fa36cbacdefc9d9b65063404b2975291f4910ed53b16ec49b58bef43d85dae': {'bloxroute': 0, 'blocknative': 518.9, 'alchemy': 831.5},
                '0xca3bf821fc29ac8a9e354656f9824c886bb30b99a09a2f2022464164251a9acc': {'bloxroute': 0, 'blocknative': 25.0, 'alchemy': 1213.3},
                '0x82bc51a103d51d0b52d06bc48d732180a0e074829a43c3821a912fd094603e91': {'alchemy': 0}, '0x2628466d9e7dd50bfee7259a1fa87782151881bbeaf91b612e66e878f59495fa': {'alchemy': 0},
                '0x3a726e6c997eb1d9f0784ee421e838d3ddae592c71cf3f5038cf75626425b87e': {'bloxroute': 0, 'blocknative': 133.7, 'alchemy': 1892.5},
                '0xd7c7468d537d30e43a0af3f5ea7a11b38d54200ccd05c94cbf72b41d0ec7ae87': {'bloxroute': 0, 'blocknative': 203.2, 'alchemy': 532.7},
                '0x8a1bef48cdba2a3f5fa505694c13bd74e9ebe941a8b9aedbb9894f77dd19627b': {'bloxroute': 0, 'blocknative': 193.3, 'alchemy': 1446.4},
                '0x55e924b4cc01596f8cefea8ccc3d43547d458932ea3cba784cd9bea730d28eb7': {'bloxroute': 0, 'blocknative': 195.2, 'alchemy': 1296.7},
                '0x7c4d40db600fa818e481b5d3c96e38cfe65564ecd6329af3bea417876054c20f': {'bloxroute': 0, 'blocknative': 133.9, 'alchemy': 867.2},
                '0x76867c23e71f0fa387ca6e029b4141d84c1443fa0d115106644159be5fb1757c': {'bloxroute': 0, 'blocknative': 194.0, 'alchemy': 1971.8},
                '0xaa6c2811db1ac09d1d5ac0d079e7f6589a101b753260fd66f999def51b7922ef': {'bloxroute': 0, 'blocknative': 185.8, 'alchemy': 1169.4},
                '0x6d125d24eea94aac969aefb1469213557451e18470855d40d4843384dd71bac5': {'bloxroute': 0, 'blocknative': 106.6, 'alchemy': 1421.1},
                '0x46620f374447fd3760c12943659604d6a6a344c8410c599ec5a31ed88b1b3fcf': {'bloxroute': 0, 'blocknative': 171.2, 'alchemy': 1864.9},
                '0x196a72adca9c540d76cf28ed515540a9624d4d0f13f8e9184420319fa16494b9': {'bloxroute': 0, 'blocknative': 215.6, 'alchemy': 2220.9},
                '0xfe140022539e70cdeb588925ff3593d3b37c8d3ff7c3db39cf312c893d9bc6b5': {'bloxroute': 0, 'blocknative': 162.2, 'alchemy': 2062.0},
                '0x5e3c847e102078e1e258972bee10dc36eff4037fdcb5e861d6052c4c5eca8e72': {'bloxroute': 0, 'blocknative': 40.4, 'alchemy': 1671.3},
                '0x6333bea3432221d02ddedc494ae7ebb8357778c1b7387d2eb0538e8ed8ea2af3': {'bloxroute': 0, 'blocknative': 183.8, 'alchemy': 1217.2},
                '0x4d4954fe4d61a738981377175aedbcc5ddf9135bf8e044d1ac242a4ddc5b3bf1': {'bloxroute': 0, 'blocknative': 167.3, 'alchemy': 1848.6},
                '0x0b329ea92166df4e2f65bbbf0d07d562bea495a10839f4ca48d55d61c73104bf': {'bloxroute': 0, 'blocknative': 169.8, 'alchemy': 876.2},
                '0xb324e79fe3ee287d287ee880dc55a7bfce438723c9553e021ecf757bb521fc24': {'bloxroute': 0, 'alchemy': 380.0, 'blocknative': 385.6},
                '0x8e77590cb52a38c4cc9be5d1da3196303637596e22cf84e4f2686ef443ad1db6': {'bloxroute': 0, 'blocknative': 179.1, 'alchemy': 919.0},
                '0xc21a58bf721060a35fd39d3d585a1e88a0c5ec7e2f412d05eb75a28622550e14': {'bloxroute': 0, 'blocknative': 197.5, 'alchemy': 631.1},
                '0x5bd1e9f3c191332b5a2bfe450a48a8d16e384203d9142ca93b636ad656791d15': {'bloxroute': 0, 'blocknative': 212.1, 'alchemy': 1041.9},
                '0xda94514743958b02d54f59a188111fcfb7379116ef191cf72cebdff1c4943499': {'bloxroute': 0, 'blocknative': 16.7, 'alchemy': 428.4},
                '0x130323e578e1bb5cfe21ec7d9459b24e36b6e954bdc7b97fa89915e448b40a4e': {'bloxroute': 0, 'blocknative': 193.5, 'alchemy': 1240.9},
                '0xdaf19aff2551899856d637540f7530b69a1314471c2f0ad1f5d0a9b45929ff09': {'bloxroute': 0, 'blocknative': 110.1, 'alchemy': 1068.8},
                '0x4a214140cf4b7abd69d89234a7b8a02460a307b2547e9ee9589a2bf8409ea820': {'bloxroute': 0, 'blocknative': 152.1, 'alchemy': 1306.1},
                '0x2f9efff024306fbc4d95ecd6128e62b9730841b1ceb0d29502a13e7901950f17': {'bloxroute': 0, 'blocknative': 116.7, 'alchemy': 1343.2},
                '0xe4beea4ce30f7bf4715837990351dd95365917d1c0f2c82e0da4bf1eb9c54eb3': {'bloxroute': 0, 'blocknative': 139.3, 'alchemy': 631.1},
                '0x3f4c91171a9dab5067db76c8fb5f5344e822bd097ecaab76e72b7864cfe025bb': {'bloxroute': 0, 'blocknative': 206.0, 'alchemy': 1180.9},
                '0x9bbe1d09d21a5306a2d32035d0765688360a4eea6e7def8d7e837867d91088a3': {'bloxroute': 0, 'blocknative': 186.7, 'alchemy': 913.7},
                '0x0ab213670f1bcbc731a33c90b0f10f20ed3d9387c498daa4b898b65225737661': {'bloxroute': 0, 'blocknative': 122.3, 'alchemy': 1825.0},
                '0xbe87bb960fa7153acdc6be124a07f9291b35393b4edde9f78e2e0d61ad646fde': {'bloxroute': 0, 'blocknative': 196.4, 'alchemy': 1448.6},
                '0x564fc853a4742f1a15e2fa101b59272a9b0eb8f54dfb19dad619ef9d008afdb7': {'bloxroute': 0, 'blocknative': 129.7, 'alchemy': 1279.7},
                '0xd7b5599c250d6e0f7f4038d88dd9dfe2e8a902910a101690e20880bb443dec2b': {'bloxroute': 0, 'blocknative': 187.9, 'alchemy': 1769.5},
                '0x9da0653fdeb4cb3ccbf8fb482b0096b1b449ad2fa2664089f9ebbd8b3b0a7d89': {'bloxroute': 0, 'blocknative': 169.3, 'alchemy': 1592.6},
                '0x0a9f3c43d5115e1e5aade508437a1787e0c6539f9f29ff304fcefeb844694f78': {'bloxroute': 0, 'blocknative': 161.1, 'alchemy': 2939.8},
                '0x28cfa98469fc087357d8023aa904ba1c0dd05017730f8a2119e3eaf12df82d8d': {'bloxroute': 0, 'blocknative': 153.1, 'alchemy': 2272.6},
                '0xe438e3fd57d38f9b2c7668cdc5c1d8243c99ba8e40bfad65aa1389a52af31352': {'bloxroute': 0, 'blocknative': 188.2, 'alchemy': 1517.1},
                '0x218f0df34c8c8ca26afc02be40deb8e50ff23caa214a3e69ee596fca60ab22b8': {'bloxroute': 0, 'blocknative': 149.8, 'alchemy': 1260.4},
                '0xd8ff0124f8956ec22d26e3b80dd6a3bdb25dd6e49c8ab86875afe71e97cdbc42': {'bloxroute': 0, 'blocknative': 151.9, 'alchemy': 2405.3},
                '0x6371d20c930247f11ab3b90d36e5ec9b128e028b203cff278eac309e0031cc44': {'bloxroute': 0, 'blocknative': 143.3, 'alchemy': 1365.9},
                '0xbc8c783fd4a2983db922731c064a0055547c3a78c4d4da47ca5234f1caf51747': {'bloxroute': 0, 'blocknative': 171.5, 'alchemy': 1182.0},
                '0xde0752084236c4c64e7171dfb65ec35eedf6c9ca72a6c84af69a75aa8ce5a9a8': {'bloxroute': 0, 'blocknative': 170.2, 'alchemy': 1426.4},
                '0xea196c44356f9ee279c264fc72cbdf3093f043ec9e6b6903fb8c0f6565fdea8f': {'blocknative': 0, 'bloxroute': 18.5, 'alchemy': 465.9},
                '0x30eb2ad3c8276134d1c8c790d00cf22d991a20e963509b920ea0410feadbe685': {'blocknative': 0, 'bloxroute': 131.6, 'alchemy': 832.2},
                '0x81bc680c360826c5ef23aa43168ccb85605dffe809033933afd5af9f542cb9ba': {'blocknative': 0, 'bloxroute': 238.7, 'alchemy': 703.4},
                '0x35701169b673f134d36b399c8794c254abea06439a4fe581b51f49215d39b04c': {'blocknative': 0, 'bloxroute': 141.8, 'alchemy': 1204.2},
                '0x0d10d572673da9c37bc85dea5dac922ac567194d15a5520ce329a81ba082c159': {'bloxroute': 0, 'blocknative': 153.0, 'alchemy': 1099.0},
                '0x787a88c288060510e371dcf1c1bde53f85b3f20d9e8ea46f81bb3ef8332da40c': {'bloxroute': 0, 'blocknative': 172.7, 'alchemy': 928.0},
                '0x7a927b9ee825287875b22497c18e2d675d2896e8d0e09db2fb53cb7a2bb0fc4c': {'bloxroute': 0, 'blocknative': 167.5, 'alchemy': 1506.4},
                '0x184c69872ee18a68628ce7179ecead7601bac44f2133878d1594edfbfa9a55b6': {'bloxroute': 0, 'blocknative': 174.1, 'alchemy': 1245.4},
                '0xc16fd4c8a9d7d345116b798ce76a8577afd2563ec54bcb213926b4240029443b': {'bloxroute': 0, 'blocknative': 173.2, 'alchemy': 1134.0},
                '0xf6a24446938a0d21974e59c7a1fc5d4ffbeeea9c7e9790467b451c9a6db75de3': {'bloxroute': 0, 'blocknative': 197.7, 'alchemy': 705.3},
                '0xb6cab6f5f3be50270b5824e33262708b133efc3abba973b06433ce6774bff12a': {'bloxroute': 0, 'blocknative': 221.5, 'alchemy': 2794.6},
                '0x4e162fb18d87307617de7fc93a1b9855fac10a6695204b0873973b7ab5dfb29b': {'bloxroute': 0, 'blocknative': 92.9, 'alchemy': 2314.9},
                '0x3464c8f779724039fb29f74f7e2c799a22e5315d1b32c22289cf306255f20a75': {'bloxroute': 0, 'blocknative': 200.7, 'alchemy': 2918.1},
                '0x7058dadc562956016013349a01e33b97fc6dd19e472cc84a5894fad887a9e70b': {'bloxroute': 0, 'blocknative': 186.5, 'alchemy': 2771.4},
                '0x2d946902433012a04d3429ff56eb8b65e684414a7204849bcca0d912f98c6a32': {'bloxroute': 0, 'blocknative': 98.3, 'alchemy': 2583.6},
                '0x0a39b3838d9044d763468688a9193b64dac25221164eabf237b7b54613467307': {'bloxroute': 0, 'blocknative': 183.5, 'alchemy': 2534.1},
                '0x48fc67f4f575cf536cc38af0869bdd187848713904ad0402a2c49287f626301a': {'bloxroute': 0, 'blocknative': 182.3, 'alchemy': 2249.6},
                '0x39182e223ce2d6e2a9e68e1b63c2d3a65c99503604f936b9a06ca5c603a4edf3': {'bloxroute': 0, 'blocknative': 101.3, 'alchemy': 2110.2},
                '0x3c06e62899f02efe1b2ec3f3234a9e63a74e0776bfa80bc4849dd2ef7e179a24': {'bloxroute': 0, 'blocknative': 152.0, 'alchemy': 2803.2},
                '0x681c03437ecc26aaf7e782799ca35d466b9820c591c57b23615fa6826e64252b': {'bloxroute': 0, 'blocknative': 175.2, 'alchemy': 1269.0},
                '0x78ff346692fd856dd782665ee5e7a3a7ddbbb82ebc0995e13cf833c2551024f9': {'bloxroute': 0, 'blocknative': 108.9, 'alchemy': 2475.8},
                '0x71db8b883e9e0ad135507ac066107e5b22ee3c85242bdbaa4a183ffe9f72ee0d': {'bloxroute': 0, 'blocknative': 184.4, 'alchemy': 1222.4},
                '0x0b2bc8a50af4caf618b13e4e4753edbc8b023bb5b13c8bdcd93f17c7c25ece0b': {'bloxroute': 0, 'blocknative': 195.2, 'alchemy': 2175.3},
                '0x8a043a35d8773574734e1879677408449da22bb5e60115d03e2e9e5fc9acb7f9': {'bloxroute': 0, 'blocknative': 155.5, 'alchemy': 2036.8},
                '0xa2281e3f48dd282618cf643274afb3a144c04ce2481fe0a2fc092e54765b4768': {'bloxroute': 0, 'blocknative': 153.5, 'alchemy': 1464.6},
                '0xb41e43c26c887a8080b9e22517a75212b8292c59e204676026b953ee65d51dd3': {'bloxroute': 0, 'blocknative': 190.1, 'alchemy': 956.5},
                '0x641fef7bc6ab4b42a252a322d65553f291432cdf9d319f17249cf05b0e4a1763': {'bloxroute': 0, 'blocknative': 141.9, 'alchemy': 2822.7},
                '0x020ceb1288053a86fc0d2da0336bc4ea1b682d4a58494f6bdb14c2d581243cc9': {'bloxroute': 0, 'blocknative': 142.7, 'alchemy': 1905.2},
                '0x8e8bacc9d2f0b6ff1051a4eed182a5d31b3cb1cee580036f74d6b309e63e01ac': {'bloxroute': 0, 'blocknative': 129.6, 'alchemy': 2451.9},
                '0xa615094c91e3eefcf6e826d326f273f208c910e2c7480f3334dbb24e6f465f29': {'bloxroute': 0, 'blocknative': 157.8, 'alchemy': 2276.0},
                '0x5cbe2c7c697e66dc493e354440753ff31631f5826fd37cbf563a9a7ec1aac744': {'bloxroute': 0, 'blocknative': 169.7, 'alchemy': 1109.3},
                '0xa739c1941ea66818b236074654b5b5ce5a3dee0081b01048cbe1e2e74b1be27e': {'bloxroute': 0, 'blocknative': 176.8, 'alchemy': 1339.0},
                '0x113cad6d00c811e8eabacede7ad831cea2d49e950fdfe27c99bd922128ffac7a': {'bloxroute': 0, 'blocknative': 152.8, 'alchemy': 1029.2},
                '0xbd5cdd1c918f141b1437893a5b8f1d03d3e8015dd0f205d1a881eea3bf56d095': {'bloxroute': 0, 'blocknative': 166.8, 'alchemy': 2383.4},
                '0xb4d03f9be0269d31749bbf14855ba5f8eb4bda6d5ff89bbf804b8eda25653c39': {'bloxroute': 0, 'blocknative': 167.7, 'alchemy': 1696.9},
                '0x1072dd13de47859563caaee22b9c83683e2c6e7407fcd18c3e32ae8b3c52a18b': {'bloxroute': 0, 'blocknative': 203.9, 'alchemy': 1213.8},
                '0xdc38fc2680633f6a95fecf80b38d0f568e5e870070e9146af715d3f0c78dbfc1': {'bloxroute': 0, 'blocknative': 375.4, 'alchemy': 0},
                '0x33974261a1d0a1dbe7aff7d455b3f91d8f846a305cc4cf293e1f336d5b227599': {'bloxroute': 0, 'blocknative': 144.6, 'alchemy': 1964.1},
                '0x44cf4ecdcdfad9fdc8ba10a0d80e332647b27a2ed68a1ad9f4d19ae90da50212': {'bloxroute': 0, 'alchemy': 925.4, 'blocknative': 1013.2},
                '0x6eccb3e2f0ed4679508477c154e866f738b3852cfdbdc6a6b527af563e129865': {'bloxroute': 0, 'blocknative': 243.9, 'alchemy': 3156.6},
                '0xb081032b3401cab3293ea9d733ab71b9a15afbc729e1b9fa023954f4d6ea980c': {'bloxroute': 0, 'blocknative': 227.3, 'alchemy': 3125.4},
                '0x083bd0dcd87ed9fb0bdc9f49a3124eaa7bf8433df8d4e95dc262251137a173dd': {'bloxroute': 0, 'blocknative': 23.0, 'alchemy': 1772.0},
                '0xa11c5588619f33e3701adc13a311833e8604e117829bd78710822da3876dc3ef': {'bloxroute': 0, 'blocknative': 130.4, 'alchemy': 857.8},
                '0xc260211bf482444542fc0ab2e01e46e0c4d0e9c6023ccc9598cd96fa620306c8': {'bloxroute': 0, 'blocknative': 130.6, 'alchemy': 1679.9},
                '0x5cbf69e78c875a211e7b4712a8a46d9f39f3e55d3fb7cfc9d63daddaaf912e27': {'bloxroute': 0, 'blocknative': 103.0, 'alchemy': 1295.4},
                '0x2d77d0f98d9680822a1e9991173ee61f65c75606147efde7f9672a32a23a8034': {'bloxroute': 0, 'blocknative': 185.8, 'alchemy': 2837.0},
                '0x0996b4969ef31b4dc618b61e8ecb57eeca0c3bf1921215050f964c863a5e4216': {'bloxroute': 0, 'blocknative': 137.8, 'alchemy': 1444.4},
                '0x3f44108a9d936fdc437906fea38a0fc58bdaca36cf0a27208d44fddf84d9a1c3': {'bloxroute': 0, 'blocknative': 196.2, 'alchemy': 818.1},
                '0xd68b731f4df255eb63d9bd8b22f738b3d37466bfe68f0cc8248bc71ed39025aa': {'bloxroute': 0, 'blocknative': 143.6, 'alchemy': 659.7},
                '0x2862b29b5b3f206fb47e590008f7e8410d5cb06de2e280b4ea34be31850735c3': {'bloxroute': 0, 'blocknative': 195.3, 'alchemy': 1346.8},
                '0x7f3b4dbad1824a4c3895cef18617ab685e932c914370cfa8b05001c55778dee9': {'bloxroute': 0, 'blocknative': 175.1, 'alchemy': 1143.5},
                '0xd53165d8f5c07eca8ea2992e3248a8db25e64264adc5f6607b330de2f1b6ebe8': {'bloxroute': 0, 'blocknative': 165.2, 'alchemy': 0},
                '0x274db13d8d9b5aff52c7e2534a881ad43348c183eeca70f69b091d792116546a': {'bloxroute': 0, 'blocknative': 150.4, 'alchemy': 1555.0},
                '0x0ddf33181f03dbb90352188aaa6aeed93a55544b79fe633aabc4ac4065e59031': {'bloxroute': 0, 'blocknative': 191.1, 'alchemy': 1496.6},
                '0xc8b50182860689bdd79a2556fb9f314f6695d61821dbfdc5f5696ce32d4f7f96': {'bloxroute': 0, 'blocknative': 172.8, 'alchemy': 1368.4},
                '0xfbd97e02b2bd4b3cd85033cffbabd42bad9308e51de88ab61d052c5c23e69409': {'bloxroute': 0, 'blocknative': 137.8, 'alchemy': 2552.9},
                '0xc8a0f480d9f022df6ac26a309683db85b4cb8946ad24af25100b9844018311ac': {'bloxroute': 0, 'blocknative': 174.7, 'alchemy': 3010.0},
                '0xcbc8a48f18e2124cdcc12a2fbf251a011e139b7f6ee3cd18a8f33f3408576dfc': {'bloxroute': 0, 'blocknative': 239.6, 'alchemy': 2590.8},
                '0x1c1e722d7ec79edcef3842b2dc2d998d76d193dbc5637503da0a04b220082433': {'bloxroute': 0, 'blocknative': 191.6, 'alchemy': 1093.2},
                '0x5c47fe9792920ced04e668f392683e23e6150e7d1012438989652ffd1326c363': {'bloxroute': 0, 'blocknative': 272.2, 'alchemy': 2212.4},
                '0x995328e70aa67e5365b6790cdd5c901e268ffefe56df44c86ac40c9b457f100c': {'bloxroute': 0, 'blocknative': 226.3, 'alchemy': 1202.0},
                '0xae435419b1ced1341b508430a0f8e3139fae9ba8698ce316e5f03e3423d5239b': {'bloxroute': 0, 'blocknative': 181.9, 'alchemy': 1519.4},
                '0xd6741a74f4c0251fb95eda8952916c02af4d5a0aa80b0c4566a1800efa37a070': {'bloxroute': 0, 'blocknative': 133.7, 'alchemy': 1276.1},
                '0x547a73e72fa6b2c17850f0b2337ef2a3f0bf9eab657e7158ffe534aa88388794': {'bloxroute': 0, 'blocknative': 82.7, 'alchemy': 799.3},
                '0xed14fb6ee49a74787805a236c4e8192f37359f46872bd4de77a340e325449ac8': {'bloxroute': 0, 'blocknative': 117.3, 'alchemy': 1245.3},
                '0x14a9b9edc2e596609be3bb5355da4c9925c94744668cd2c06fda637f3a4b8be9': {'bloxroute': 0, 'blocknative': 151.8, 'alchemy': 3194.8},
                '0xdfd32d49514ccf0495045a87723d5a45c97b6684ef7ae5d0bb38ef3d89acddad': {'bloxroute': 0, 'blocknative': 161.7, 'alchemy': 2771.6},
                '0xaeea5925c33a4bb1094ef9136957ba82b1a610928e287e0159458170221f3434': {'bloxroute': 0, 'blocknative': 176.3, 'alchemy': 2572.9},
                '0x9a16d01cad50119eab81f69a501f04f7bf70264c9734e0734d04a561b42e36dc': {'bloxroute': 0, 'blocknative': 148.8, 'alchemy': 2483.4},
                '0x4561d09358f1b597c8c1d3ad19765945253729d9155f4627fa082f66305c028f': {'bloxroute': 0, 'blocknative': 169.0, 'alchemy': 0},
                '0xffae449124fda4f85fe5ee2d13e9bf21a5ab5bef931fcd6d678258a493262c67': {'bloxroute': 0, 'blocknative': 165.7, 'alchemy': 0},
                '0x8e6e688ece38df4944ce0209c0bce59a0d73d422ab610df8c66796e4e672bacf': {'bloxroute': 0, 'blocknative': 217.8, 'alchemy': 557.7},
                '0x93dc49a92cf6f799987cdb0fe8d2672120967c901a1188e51fe84168439ba44c': {'bloxroute': 0, 'blocknative': 237.2, 'alchemy': 684.6},
                '0x3baa03ed62cc4ab2faf54fbbd9a3ebcd0cd3a064e73c3718cd981f48b9fefcb9': {'bloxroute': 0, 'blocknative': 199.0, 'alchemy': 843.5},
                '0xa905139547b67a7f826126683c1dda1c4b9d5042c6d4aacf8b13650833374e89': {'bloxroute': 0, 'blocknative': 199.8, 'alchemy': 719.8},
                '0x2aa30cf469b63fdb3d2a127816560c43c5fffab5e75862512c49823e42fab30a': {'bloxroute': 0, 'blocknative': 170.9, 'alchemy': 675.8},
                '0x66b77f072fe479bd7ed036399fe7ffd974371971408ac95992440bacb3ebd662': {'bloxroute': 0, 'blocknative': 157.3, 'alchemy': 2576.6},
                '0x9fd0f1d43f6d450532a4dd29f1d74c21970b9e397f37b217b07349f1ca603700': {'bloxroute': 0, 'blocknative': 150.1, 'alchemy': 1961.3},
                '0xc9e4780c651c546e46629bed0b02786e814b419939552600de5cfe6eeeacd860': {'bloxroute': 0, 'blocknative': 130.9, 'alchemy': 1570.0},
                '0xa6d581f83147576a2b277eeee9511e3dc25585eaf2238a7367f93bd105976912': {'bloxroute': 0, 'blocknative': 167.0, 'alchemy': 2368.3},
                '0xab9acf7bd2ea48d88fe4574542e96820982faa89f7e9e0d74cdfc4f4246f429d': {'bloxroute': 0, 'blocknative': 97.5, 'alchemy': 1539.0},
                '0xee6b253d45cccae8af6db03718aea793c54fe431673c64e0c490e85ad9327ee4': {'bloxroute': 0, 'blocknative': 135.0, 'alchemy': 1680.8},
                '0x537fdea8498120a6479280b79a8b16a4fef4ea114e8315343a913157f7d3d806': {'bloxroute': 0, 'blocknative': 204.8, 'alchemy': 1238.0},
                '0xe055f792701195c7c6e839cf78141a37d2eb217cfa14992cf0e70c1ea8d8dc37': {'bloxroute': 0, 'blocknative': 142.9, 'alchemy': 1127.1},
                '0xe6c79a62b5e112338d7f259d68840904f0f94ad7f6635b60ee434ef11701e025': {'bloxroute': 0, 'blocknative': 122.7, 'alchemy': 1622.3},
                '0xc04f71848a379c44b2f3f20f3d42f35715f77d8265cc4c46a06f226bd433d30c': {'bloxroute': 0, 'blocknative': 176.7, 'alchemy': 1609.1},
                '0x25ed18461a28c0c9231c0ef091ba6131c8fb9cf232117900b768737f942e3010': {'bloxroute': 0, 'blocknative': 164.1, 'alchemy': 1169.4},
                '0xca843bce37f2b35760b7bc11bbdd5eda4ec14a065b441d6794ffd510e890eebd': {'bloxroute': 0, 'blocknative': 142.5, 'alchemy': 1918.2},
                '0x472fa07fd60f961a048c770499645d488cd4e6ac8561b39c9b69fc281087bcd2': {'bloxroute': 0, 'blocknative': 199.3, 'alchemy': 902.4},
                '0x58a01d8f68a93c8fae89427ec0d62d42616c5dfbb6e01b46054090a4eb7f79fb': {'bloxroute': 0, 'blocknative': 169.2, 'alchemy': 2570.6},
                '0x03958c2f5d8aaa6dc89f8023fb9672b36125e3090adddd92c04f1e52c4af9013': {'bloxroute': 0, 'blocknative': 187.2, 'alchemy': 1566.8},
                '0xb9818a13f53506f9ddead36a339d32122b37672b257f70033e4150fe36091945': {'bloxroute': 0, 'blocknative': 165.5, 'alchemy': 1454.9},
                '0xe841be031d6e24a4ad63333bada538cc2bed1a895c2b67ac1bca6f1086fd7fca': {'bloxroute': 0, 'blocknative': 142.8, 'alchemy': 2175.1},
                '0x31885e01dcfeff2f012152bad540c2581345014199ef3e32ebc78205a6886064': {'bloxroute': 0, 'blocknative': 220.0, 'alchemy': 1146.2},
                '0x53f3070865caf9b60dd53c4e23fa901324a6f5b16dde41f42efa658424fa3320': {'bloxroute': 0, 'blocknative': 142.0, 'alchemy': 2336.0},
                '0xbe2b0a22b87fbd8ba73878172867d5fea20d9309a0f1e25fc0f185109dd56032': {'bloxroute': 0, 'blocknative': 142.4, 'alchemy': 1704.7},
                '0xa7350c5915fb582a95a57a143d83d461938b1520babd2d69ca060c3f8ac8aad0': {'bloxroute': 0, 'blocknative': 145.5, 'alchemy': 2518.7},
                '0xa82091acb884d149ea3a9e91bfb9b09dc142ab09a807d114fb10891456208ad5': {'bloxroute': 0, 'alchemy': 139.1, 'blocknative': 587.6},
                '0x897f32247719c1eb20b62bfc9fecdbce85fbc8defe179a30540e3c2256200dc1': {'bloxroute': 0, 'blocknative': 157.4, 'alchemy': 729.6},
                '0x3200be87fac8f292fb3e95f7a5f0e90ff112587870462f8186dbbefd4763a66c': {'bloxroute': 0, 'blocknative': 386.0},
                '0x0c3d47e3b834c39c51f4dd89fef57ba04cca69999e340ada9cbe376e396d155f': {'bloxroute': 0, 'blocknative': 245.6}}

    updatedDataDict = {}

    csvString = ''
    csvString += 'txHash,alchemy,blocknative,bloxroute\n'
    requiredServices = 3
    for txHash in dataDict:
        try:
            if len(dataDict[txHash]) != requiredServices:
                continue

            updatedDataDict[txHash] = dataDict[txHash]
            PrintAndLog("dataDict[txHash] = " + str(dataDict[txHash]))
            # csvString += 'txHash,alchemy,blocknative,bloxroute,\n'
            csvString += txHash + ',' + str(dataDict[txHash]['alchemy']) + ',' + str(dataDict[txHash]['blocknative']) + ',' + str(dataDict[txHash]['bloxroute']) + '\n'

        except:
            PrintAndLogError("exception: " + traceback.format_exc())

    filepath_MempoolServiceCompareDict = "MempoolServiceCompareDict.txt"

    PrintAndLog("updatedDataDict = " + str(updatedDataDict))

    text_file = open(filepath_MempoolServiceCompareDict, "w")
    text_file.write(csvString)
    text_file.close()
    PrintAndLog("csvString = " + str(csvString))

elif sys.argv and len(sys.argv) >= 2 and "get_all_addresses" == sys.argv[1].lower():
    addressList = []
    for ninjaOpName in NinjaOpAccountDict:
        ninjaOp = NinjaOpAccountDict[ninjaOpName]
        addressList.append(ninjaOp.publicAddress)

    PrintAndLog("addressList = " + str(addressList))

elif sys.argv and len(sys.argv) >= 2 and "testhidingbook" == sys.argv[1].lower():
    tokenList = Exchanges.hidingBook.API_GetListOfTokenAddresses()
    PrintAndLog("tokenList of len " + str(len(tokenList)) + " = " + str(tokenList))
    # jData = Exchanges.hidingBook.API_GetOrders(True)
    # orders = jData['orders']
    # PrintAndLog("found " + str(len(orders)) + " orders: " + str(orders))

elif sys.argv and len(sys.argv) >= 2 and "testhidingbook_ratelimiter" == sys.argv[1].lower():
    # Exchanges.hidingBook.API_GetTokenList()
    successCount = 0
    failCount = 0
    for i in range(0, 100):
        try:
            jData = Exchanges.hidingBook.API_GetOrders(True)
            orders = jData['orders']
            PrintAndLog("found " + str(len(orders)) + " orders: " + str(orders))
            successCount += 1

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception, " + traceback.format_exc())
            failCount += 1

        time.sleep(0.1)

    PrintAndLog("successCount = " + str(successCount))
    PrintAndLog("failCount = " + str(failCount))

elif sys.argv and len(sys.argv) >= 2 and "testhidingbook_register_rfq_tx_origins" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV4.SetNetwork(Libraries.network.Network.mainnet)

    gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    txOriginAddressList = []
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-100"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-102"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-103"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-104"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-105"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-106"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-107"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-108"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-109"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-110"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-111"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-112"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-113"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-114"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-116"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-117"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-118"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-119"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-120"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-121"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-122"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["ninja-op-123"].publicAddress)
    txOriginAddressList.append(NinjaOpAccountDict["dedicated-tx-prediction"].publicAddress)

    # Set True to whitelist the addresses
    # Set False to blacklist the addresses
    isAllowed = True

    Exchanges.zrxV4.API_RegisterAllowedRfqOrigins(
        HidingGameRfqOriginsAccount.publicAddress, HidingGameRfqOriginsAccount.DecryptPK(),
        txOriginAddressList, isAllowed, gasPrice)

elif sys.argv and len(sys.argv) >= 2 and "testhidingbook_websockets" == sys.argv[1].lower():
    Exchanges.hidingBook.ConnectWebSocketClient(ExchangeName_0xMesh)

    time.sleep(2)
    Exchanges.hidingBook.SubscribeToOrders()
    while True:
        # try:
        #     Exchanges.hidingBook.SendTestMessage("test 10")
        # except:
        #     PrintAndLogError("exception: " + traceback.format_exc())

        time.sleep(2)

elif sys.argv and len(sys.argv) >= 2 and "testhidingbook_post_order_0xv4" == sys.argv[1].lower():
    ConsiderUpdatingLatestBlockNumber()
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()

    Exchanges.zrxV4.SetNetwork(Libraries.network.Network.mainnet)

    # ninjaOp = NinjaOpAccountDict["ninja-op-116"]
    ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    quoteToken = Exchanges.zrxV4.Contract_WETH.lower()
    # baseToken = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'  # wbtc
    # baseToken = '0x0d8775f648430679a709e98d2b0cb6250d2887ef'  # bat
    # baseToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'  # usdc
    baseToken = '0x6b175474e89094c44da98b954eedeac495271d0f'  # dai

    doWriteOrderToLogsForTesting = False
    doSubmitOrder = True
    doImmediatelyCancelOrder = False

    # amount_baseToken_etherUnits = 600  # WORKS
    # amount_baseToken_etherUnits = 1000
    amount_baseToken_etherUnits = 0.0005

    # price = 0.05686397  # rep   buy
    # price = 0.07686397  # rep   sell
    # price = 0.00104924  # bat buy
    # price = 0.00164924  # bat sell
    # price = 0.000580302221397  # usd market value
    # price = 50.14  # wbtc buy
    # price = 56.14  # wbtc sell

    price_buy = 0.000780302221397  # usd
    price_sell = 0.000380302221397  # usd

    # price_sell1 = 0.000340302221397  # usd
    # price_sell2 = 0.000380302221397  # usd
    # price_sell1 = 0.000340302221397  # usd
    # price_sell2 = 0.000340302221390  # usd

    # price_buy1 = 0.000780302221397  # usd
    # price_buy2 = 0.000780302221390  # usd

    orderDuration_seconds = 60
    # orderDuration_seconds = 300
    # orderDuration_seconds = 2000
    # orderDuration_seconds = 200000

    order_buy = Exchanges.hidingBook.Sign0xV4Order(ninjaOp.publicAddress, ninjaOp.DecryptPK(), quoteToken, baseToken,
                                                   "buy", amount_baseToken_etherUnits, price_buy, orderDuration_seconds, doWriteOrderToLogsForTesting)
    order_sell = Exchanges.hidingBook.Sign0xV4Order(ninjaOp.publicAddress, ninjaOp.DecryptPK(), quoteToken, baseToken,
                                                    "sell", amount_baseToken_etherUnits, price_sell, orderDuration_seconds, doWriteOrderToLogsForTesting)
    Exchanges.hidingBook.API_PostOrders_0xV4([order_buy, order_sell])

    # order_sell1 = Exchanges.hidingBook.Sign0xV4Order(ninjaOp.publicAddress, ninjaOp.DecryptPK(), quoteToken, baseToken,
    #                                                  "sell", amount_baseToken_etherUnits, price_sell1, orderDuration_seconds, doWriteOrderToLogsForTesting)
    # order_sell2 = Exchanges.hidingBook.Sign0xV4Order(ninjaOp.publicAddress, ninjaOp.DecryptPK(), quoteToken, baseToken,
    #                                                  "sell", amount_baseToken_etherUnits, price_sell2, orderDuration_seconds, doWriteOrderToLogsForTesting)
    # Exchanges.hidingBook.API_PostOrders_0xV4([order_sell1, order_sell2])

    # order_buy1 = Exchanges.hidingBook.Sign0xV4Order(ninjaOp.publicAddress, ninjaOp.DecryptPK(), quoteToken, baseToken,
    #                                                  "buy", amount_baseToken_etherUnits, price_buy1, orderDuration_seconds, doWriteOrderToLogsForTesting)
    # order_buy2 = Exchanges.hidingBook.Sign0xV4Order(ninjaOp.publicAddress, ninjaOp.DecryptPK(), quoteToken, baseToken,
    #                                                  "buy", amount_baseToken_etherUnits, price_buy2, orderDuration_seconds, doWriteOrderToLogsForTesting)
    # Exchanges.hidingBook.API_PostOrders_0xV4([order_buy1, order_buy2])

    if doImmediatelyCancelOrder:
        PrintAndLog("Sleeping a bit before i cancel so my salt timestamp is higher")
        time.sleep(2)
        gasPrice = Libraries.gasStation.GetGasPrice_Fast()
        Exchanges.zrxV4.API_CancelOrder_RFQ(ninjaOp.publicAddress, ninjaOp.DecryptPK(), order_sell, 300000, gasPrice)
        # makerTokens = [baseToken]
        # takerTokens = [quoteToken]
        # minValidSalts = [int(time.time()) * 1000]
        # Exchanges.zrxV4.API_BatchCancelPair_RFQ(ninjaOp.publicAddress, ninjaOp.DecryptPK(), makerTokens, takerTokens, minValidSalts, 300000, gasPrice)

        while True:
            ConsiderUpdatingLatestBlockNumber()
            result = Exchanges.zrxV4.API_GetOrderInfo_Batched_Safe([order_sell])
            PrintAndLog("result = " + str(result))
            time.sleep(5)

elif sys.argv and len(sys.argv) >= 2 and "testratelimiter" == sys.argv[1].lower():
    while True:
        passedDict = {}

        key = 'hello'
        passedDict[key] = Libraries.rateLimiter.EnforceRateLimit(key)

        key = 'botcjes'
        passedDict[key] = Libraries.rateLimiter.EnforceRateLimit(key)

        key = 'imcoo'
        passedDict[key] = Libraries.rateLimiter.EnforceRateLimit(key)

        PrintAndLog("passedDict = " + str(passedDict))
        time.sleep(0.1)

elif sys.argv and len(sys.argv) >= 2 and "get_balances_at_point_in_history" == sys.argv[1].lower():
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)

    tokenList = [
        Exchanges.keeperDAO.Contract_DAI
    ]

    address = "0x3D71d79C224998E608d03C5Ec9B405E7a38505F0"
    startBlock = 11877474
    endBlock = 11877478
    # endBlock = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    currentBlock = startBlock
    step = 1
    while currentBlock < endBlock + step:
        balance_ether = Libraries.core.API_GetEtherBalance(address, None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)
        balance_weth = Libraries.core.API_GetTokenBalance(address, Exchanges.zrxV2.Contract_WETH, Libraries.core.Ether_Decimals,
                                                          None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)

        tokenBalanceDict = {}
        for token in tokenList:
            tokenBalanceDict[token] = None
            decimals_token = Libraries.core.GetDecimalsForTokenContract(token, True)
            tokenBalanceDict[token] = Libraries.core.API_GetTokenBalance(address, token, decimals_token,
                                                                         None, None, None, Libraries.core.RequestTimeout_seconds, currentBlock)

        balance_total = balance_ether + balance_weth
        PrintAndLog("block " + str(currentBlock))
        PrintAndLog("   balance_ether = " + str(round(balance_ether, 1)))
        for token in tokenBalanceDict:
            PrintAndLog("   balance_token = " + str(round(tokenBalanceDict[token], 1)) + " " + str(token))

        currentBlock += step

elif sys.argv and len(sys.argv) >= 2 and "dissect_logs" == sys.argv[1].lower():
    # context manager `with` takes care of file closing, error handling
    # filename = 'logfile ninja uniswap errors dec 11'
    # filepath = '/Users/pako/Desktop/' + filename
    # filepath = os.path.expanduser("~/Desktop/" + filename)

    stringToMatch = 'NinjaGeneric_Config_A 10821744-465785'
    inputFilepath = '~/Desktop/logfile_zz_NinjaGeneric_Config_A.log'
    outputFilepath = '~/Desktop/dissect_logs_output'

    # os.system('grep -hnr "import" ~/Desktop/testVRS.py > output.txt')
    # os.system('grep -hnr "(Thread-3788347)" ~/Desktop/logfile_ninja_uniswap_errors_dec_11')

    # Search a file and get each line that contains our stringToMatch
    os.system('grep -hnr "' + stringToMatch + '" ' + inputFilepath + ' > ' + outputFilepath)
    # Open the file
    os.system('open -a Sublime\ Text ' + outputFilepath)

    # filepath = 'testVRS.py'

    # file = open(filepath, "r")
    # content = file.read()
    # PrintAndLog('content = ' + str(content))
    #
    # f = open(os.path.expanduser("~/Desktop/a"))
    # f = open(os.path.expanduser("~/Desktop/testVRS.py"))
    # PrintAndLog("f = " + str(f.readline()))
    # PrintAndLog("f = " + str(f.readline()))
    # PrintAndLog("f = " + str(f.readlines()))

    # with open(filename, 'r') as handle:
    #     for line in handle:
    #         if line.startswith('SequenceName_'):
    #             print line.split()
    #             # Write to file, etc.

elif sys.argv and len(sys.argv) >= 2 and "test" == sys.argv[1].lower():
    PrintAndLog("Not entering main loop, do some functional testing here!")

    # ninjaName = "ninja-op-100"
    # publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    # privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()

    # {
    #   "orders": [
    #     {
    #       "order": {
    #         "signature": {
    #           "r": "0x8482129e963f107df9693b89c26aec9f1d0f0b374321b8fe3ad694c382dc5d59",
    #           "s": "0x4ef38b8e3df1a613541725e2d9d6465320657f4bc03b145539d37de05f88e457",
    #           "v": 28,
    #           "signatureType": 2
    #         },
    #         "makerToken": "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2",
    #         "takerToken": "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48",
    #         "txOrigin": "0xbd49a97300e10325c78d6b4ec864af31623bb5dd",
    #         "maker": "0x211b6a1137bf539b2750e02b9e525cf5757a35ae",
    #         "taker": "0x0000000000000000000000000000000000000000",
    #         "makerAmount": "1000000000000000000",
    #         "takerAmount": "1176030000",
    #         "pool": "0x000000000000000000000000000000000000000000000000000000000000002d",
    #         "expiry": 1612042264,
    #         "salt": "26534341672976876561918473664416107830775407922693082728169112560453981999963",
    #         "chainId": 1,
    #         "verifyingContract": "0xdef1c0ded9bec7f1a1670819833240f027b25eff"
    #       },
    #       "metaData": {
    #         "orderHash": "0xce843dcc1cb3cc053dfb3cfc41fc139ea3396c0bcfee3efede168aadb6d73f1b",
    #         "status": 1,
    #         "filledAmount_takerToken": 0,
    #         "remainingFillableAmount_takerToken": 1176030000
    #       }
    #     }
    #   ],
    #   "message": "Ok"
    # }

    # Exchanges.zrxV4.SetNetwork(Libraries.network.Network.mainnet)
    # # salt = Libraries.core.ConvertHexToInt('0x3aa9e7b960e3aaeb7d144cd268fec703b1793b601e7a92603333586ba55e935b')
    # salt = 26534341672976876561918473664416107830775407922693082728169112560453981999963
    #
    # exchangeContractAddress = Exchanges.zrxV4.Contract_Exchange
    # maker = publicAddress_Ninja
    # taker = '0x0000000000000000000000000000000000000000'
    # makerTokenAddress = '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2'
    # takerTokenAddress = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # makerTokenAmount = 1000000000000000000
    # takerTokenAmount = 1176030000
    # expirationUnixTimestampSec = 1612042264
    # orderDuration_seconds = 120
    # txOrigin = '0xbd49a97300e10325c78d6b4ec864af31623bb5dd'
    # zrxStakingPool = Exchanges.zrxV4.ZrXStakingPool_KeeperDAO
    # order = MyOrder_0xv4("_dontcare", exchangeContractAddress, maker, taker, makerTokenAddress, takerTokenAddress,
    #                      makerTokenAmount, takerTokenAmount, expirationUnixTimestampSec,
    #                      orderDuration_seconds, salt, txOrigin, zrxStakingPool)
    # import Libraries.signingUtils_0xv4
    # Libraries.signingUtils_0xv4.SignOrder_0xv4(order, privateKey_Ninja)
    # # Write the order to a file so we can keep track of it even if the bot crashes or stops
    # serializedOrderObject = jsonpickle.encode(order)
    # PrintAndLog("serializedOrderObject = " + str(serializedOrderObject))
    #
    #
    #
    # sys.exit()

    # Data obtained from the BruteForceArbitrage call
    # ratesGraph = [
    #     [1, 0, 0, 0, 67529.9850152513, 540.188925699368],
    #     [0, 1, 0, 0, 0, 0],
    #     [0, 0, 1, 0, 0, 0],
    #     [0, 0, 0, 1, 0, 540.1889267028934],
    #     [1.2358890698440596e-05, 0, 0, 0, 1, 0.007344997456229914],
    #     [0.0018425068364379476, 0, 0, 0.0018425061395366228, 134.02776876846255, 1]]
    # ratesGraph_ExchangeNames = [[0, 0, 0, 0, 3, 4], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 4], [3, 0, 0, 0, 0, 6], [2, 0, 0, 2, 6, 0]]
    # ratesGraph_QuoteToken = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 5], [0, 0, 0, 0, 0, 5], [0, 0, 0, 5, 5, 0]]

    # Don't remember where i obtained this data
    # ratesGraph = [
    #     [1, 0, 0, 0, 67529.9850152513, 540.3082999647177],
    #     [0, 1, 0, 0, 0, 0],
    #     [0, 0, 1, 0, 0, 0],
    #     [0, 0, 0, 1, 0, 540.1889267028934],
    #     [1.2358890698440596e-05, 0, 0, 0, 1, 0.007344997456229914],
    #     [0.0018425068364379476, 0, 0, 0.0018425061395366228, 171.87696947414207, 1]]
    # ratesGraph_ExchangeNames = [[0, 0, 0, 0, 3, 6], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 4], [3, 0, 0, 0, 0, 6], [2, 0, 0, 2, 6, 0]]
    # ratesGraph_QuoteToken = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 5], [0, 0, 0, 0, 0, 5], [0, 0, 0, 5, 5, 0]]
    # tokenList = [
    #     '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', '0x6b175474e89094c44da98b954eedeac495271d0f',
    #     '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359', '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2',
    #     '0x3e9bc21c9b189c09df3ef1b824798658d5011937', '0xdac17f958d2ee523a2206206994597c13d831ec7']
    # sourceTokensList = [
    #     '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', '0xdac17f958d2ee523a2206206994597c13d831ec7']
    # Libraries.arbAlgos.BruteForceArbitrage(ratesGraph, ratesGraph_ExchangeNames, ratesGraph_QuoteToken, tokenList, sourceTokensList, 'id-1')

    # quantity_quoteTokens_etherUnits = 0.0001
    # uniswapBalanceExpected_quoteTokens_afterTxMinesIn = 61366.07281871449
    # uniswapBalanceExpected_baseTokens_afterTxMinesIn = 8086770.208931205
    # price_ask_pendingTradeTx = 0.007611286521498734
    # price_bid_pendingTradeTx = 9.86954855271794e-05

    # sourceTokenQuantity_etherUnits = 0.0001
    # price1 = Exchanges.uniswap.CalculatePriceGivenBalances(
    #     "buy", sourceTokenQuantity_etherUnits, uniswapBalanceExpected_quoteTokens_afterTxMinesIn, uniswapBalanceExpected_baseTokens_afterTxMinesIn)
    # PrintAndLog("price1 = " + str(price1))
    #
    # destinationTokenQuantity_etherUnits = 0.0001
    # price2 = Exchanges.uniswap.CalculatePriceGivenBalances_GivenOutputAmount(
    #     "sell", destinationTokenQuantity_etherUnits, uniswapBalanceExpected_quoteTokens_afterTxMinesIn, uniswapBalanceExpected_baseTokens_afterTxMinesIn)
    # PrintAndLog("price2 = " + str(price2))
    #
    # sys.exit()

    # toAddress = '0xdac17f958d2ee523a2206206994597c13d831ec7'
    # fromAddress = '0x3D71d79C224998E608d03C5Ec9B405E7a38505F0'
    # # data = '0x095ea7b3000000000000000000000000dac17f958d2ee523a2206206994597c13d831ec7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'
    # # data = '0x095ea7b3000000000000000000000000dac17f958d2ee523a2206206994597c13d831ec70000000000000000000000000000000000000000000000000000000000000000'
    # data = '0x095ea7b3000000000000000000000000dac17f958d2ee523a2206206994597c13d831ec700000000000000000000000000000000000000000000000000f0000000000000'
    # estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data)
    # PrintAndLog("estimatedGas = " + str(estimatedGas))

    # data_hex = '0x4a681975000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc2000000000000000000000000000000000000000000000014d6bf816a0b300000000000000000000000000000930ae255053e40f430a4fda533eae0de5b131924'
    # PrintAndLog("data_hex before = " + str(data_hex))
    # data_hex = Libraries.core.RemoveFirstParameterFromCallDataString(data_hex)
    # PrintAndLog("data_hex after = " + str(data_hex))

    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    ConsiderUpdatingLatestBlockNumber()

    walletAddressList = ['0xb4e16d0168e52d35cacd2c6185b44281ec28c9dc', '0x3139ffc91b99aa94da8a2dc13f1fc36f9bdc98ee', '0x12ede161c702d1494612d19f05992f43aa6a26fb',
                         '0xa478c2975ab1ea89e8196811f51a7b7ade33eb11', '0x07f068ca326a469fc1d87d85d448990c8cba7df9', '0xae461ca67b15dc8dc81ce7615e0320da1a9ab8d5',
                         '0xce407cd7b95b39d3b4d53065e711e713dd5c5999', '0x33c2d48bc95fb7d0199c5c693e7a9f527145a9af', '0xb6909b960dbbe7392d405429eb2b3649752b4838']
    tokenAddressList = ['0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48', '0x8e870d67f660d95d5be530380d0ec0bd388289e1', '0x06af07097c9eeb7fd685c692751d5c66db49c215',
                        '0x6b175474e89094c44da98b954eedeac495271d0f', '0x408e41876cccdc0f92210600ef50372656052a38', '0x6b175474e89094c44da98b954eedeac495271d0f',
                        '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2', '0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c', '0x0d8775f648430679a709e98d2b0cb6250d2887ef']

    walletAddressList2 = ['0x211B6a1137BF539B2750e02b9E525CF5757A35aE', '0x65C2698F4a2D5883D9ae6d22E4CB633d5371ce9D', '0x12ede161c702d1494612d19f05992f43aa6a26fb',
                          '0xd72fBaE4B64079eDd64969F760cdf240eC7533Eb', '0x89D2495fd1D327ecB7fD420447D460E5A92e6DaC', '0x57845987C8C859D52931eE248D8d84aB10532407',
                          '0x89D2495fd1D327ecB7fD420447D460E5A92e6DaC', '0x65C2698F4a2D5883D9ae6d22E4CB633d5371ce9D', '0x211B6a1137BF539B2750e02b9E525CF5757A35aE']

    resultsDict = {}
    key1 = "resultList1"
    key2 = "resultList2"
    # key1 = None
    # key2 = None
    key3 = "resultList3"
    key4 = "resultList4"
    # key3 = None
    # key4 = None

    resultList1 = Libraries.core.API_GetTokenBalance_Batched_Safe(walletAddressList, tokenAddressList, None, resultsDict, key1)
    resultList2 = Libraries.core.API_GetTokenBalance_Batched(walletAddressList, tokenAddressList, None, resultsDict, key2)

    resultList3 = Libraries.core.API_GetEtherBalance_Batched_Safe(walletAddressList2, resultsDict, key3)
    resultList4 = Libraries.core.API_GetEtherBalance_Batched(walletAddressList2, resultsDict, key4)

    # 
    # walletAddressList = ['0xb4e16d0168e52d35cacd2c6185b44281ec28c9dc', '0x3139ffc91b99aa94da8a2dc13f1fc36f9bdc98ee', '0x12ede161c702d1494612d19f05992f43aa6a26fb',
    #                      '0xa478c2975ab1ea89e8196811f51a7b7ade33eb11', '0x07f068ca326a469fc1d87d85d448990c8cba7df9', '0xae461ca67b15dc8dc81ce7615e0320da1a9ab8d5',
    #                      '0xce407cd7b95b39d3b4d53065e711e713dd5c5999', '0x33c2d48bc95fb7d0199c5c693e7a9f527145a9af', '0xb6909b960dbbe7392d405429eb2b3649752b4838']
    # tokenAddressList = ['0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48', '0x8e870d67f660d95d5be530380d0ec0bd388289e1', '0x06af07097c9eeb7fd685c692751d5c66db49c215',
    #                     '0x6b175474e89094c44da98b954eedeac495271d0f', '0x408e41876cccdc0f92210600ef50372656052a38', '0x6b175474e89094c44da98b954eedeac495271d0f',
    #                     '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2', '0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c', '0x0d8775f648430679a709e98d2b0cb6250d2887ef']
    # 
    # walletAddressList2 = ['0x211B6a1137BF539B2750e02b9E525CF5757A35aE', '0x65C2698F4a2D5883D9ae6d22E4CB633d5371ce9D', '0x12ede161c702d1494612d19f05992f43aa6a26fb',
    #                       '0xd72fBaE4B64079eDd64969F760cdf240eC7533Eb', '0x89D2495fd1D327ecB7fD420447D460E5A92e6DaC', '0x57845987C8C859D52931eE248D8d84aB10532407',
    #                       '0x89D2495fd1D327ecB7fD420447D460E5A92e6DaC', '0x65C2698F4a2D5883D9ae6d22E4CB633d5371ce9D', '0x211B6a1137BF539B2750e02b9E525CF5757A35aE']
    # 
    # resultsDict = {}
    # key1 = "resultList1"
    # key2 = "resultList2"
    # # key1 = None
    # # key2 = None
    # key3 = "resultList3"
    # key4 = "resultList4"
    # # key3 = None
    # # key4 = None
    # 
    # resultList1 = Libraries.core.API_GetTokenBalance_Batched_Safe(walletAddressList, tokenAddressList, None, resultsDict, key1)
    # resultList2 = Libraries.core.API_GetTokenBalance_Batched(walletAddressList, tokenAddressList, None, resultsDict, key2)
    # 
    # resultList3 = Libraries.core.API_GetEtherBalance_Batched_Safe(walletAddressList2, resultsDict, key3)
    # resultList4 = Libraries.core.API_GetEtherBalance_Batched(walletAddressList2, resultsDict, key4)
    # 
    # PrintAndLog("resultList1 = " + str(resultList1))
    # PrintAndLog("resultList2 = " + str(resultList2))
    # PrintAndLog("resultsDict[key1] = " + str(resultsDict[key1]))
    # PrintAndLog("resultsDict[key2] = " + str(resultsDict[key2]))
    # PrintAndLog("resultList3 = " + str(resultList3))
    # PrintAndLog("resultList4 = " + str(resultList4))
    # PrintAndLog("resultsDict[key3] = " + str(resultsDict[key3]))
    # PrintAndLog("resultsDict[key4] = " + str(resultsDict[key4]))

    # idDict_coinGecko = Exchanges.keeperDAO.GetCoinGeckoIdDict()
    #
    # UpdateCoinMarketCapMarkets(1000, "id")
    #
    # coinMarketCapId = ''
    # currentMarketPrice_withRespectToETH = CoinMarketCapMarketsDict[coinMarketCapId].GetPriceEth(coinMarketCapId, CoinMarketCapMarketsDict)
    # PrintAndLog("currentMarketPrice_withRespectToETH = " + str(currentMarketPrice_withRespectToETH))

    # for remoteNode in Libraries.nodes.RemoteNodeWebsocketsList:
    #     Libraries.node_geth.ConnectWebSocketClient(remoteNode)
    #
    # while True:
    #     PrintAndLog("Waiting")
    #     time.sleep(10)

    # pricesDict = Libraries.utils.ReadJsonFromFile('UnitTests/PriceDict/testPriceDict_ArbyBetweenUniswapAndKyber_1.json')
    # PrintAndLog("pricesDict = " + str(pricesDict))
    # for quoteToken in pricesDict[pdk_quoteTokens]:
    #     PrintAndLog("Found quoteToken " + str(quoteToken))
    #
    #     for exchangeName in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges]:
    #         PrintAndLog("Found exchangeName " + str(exchangeName))

    # PrintAndLog("API_GetBlock_Batched")
    # blockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    # jDataList = Libraries.core.API_GetBlock_Batched([blockNumber, blockNumber-1, blockNumber-2, blockNumber-3], True)
    # for blockData in jDataList:
    #     PrintAndLog("result = " + str(Libraries.core.ConvertHexToInt(blockData['timestamp'])))
    #
    # PrintAndLog("API_GetBlock")
    # jData = Libraries.core.API_GetBlock(blockNumber, True)
    # PrintAndLog("result = " + str(Libraries.core.ConvertHexToInt(jData['timestamp'])))
    # jData = Libraries.core.API_GetBlock(blockNumber-1, True)
    # PrintAndLog("result = " + str(Libraries.core.ConvertHexToInt(jData['timestamp'])))
    # jData = Libraries.core.API_GetBlock(blockNumber-2, True)
    # PrintAndLog("result = " + str(Libraries.core.ConvertHexToInt(jData['timestamp'])))
    # jData = Libraries.core.API_GetBlock(blockNumber-3, True)
    # PrintAndLog("result = " + str(Libraries.core.ConvertHexToInt(jData['timestamp'])))

    # marketName = "link-eth"
    # destinationAddress = MarketDict["bat-eth"].account.publicAddress
    # value_wei_int = Libraries.core.ConvertEtherToWei(0, Libraries.core.Ether_Decimals)
    # txId = API_SendEther_ToAddress(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), destinationAddress,
    #                                value_wei_int, Libraries.gasStation.GetGasLimit_SendingEther(), Libraries.gasStation.GetGasPrice_Cheap(), TransactionType.other)

    # addressList = ['0x0015CB2187299D43B6313aE138a966899c72630c', '0x65bf64ff5f51272f729bdcd7acfb00677ced86cd']
    # addressList = [Libraries.accounts.TestAccount_JoeyZ_0015.publicAddress, Libraries.accounts.TestAccount_JoeyZ_65C2.publicAddress]
    # resultDict = Libraries.core.API_GetTransactionCount_Batched(addressList)
    # PrintAndLog("resultDict = " + str(resultDict))

    # txCount = Libraries.core.API_GetTransactionCount(addressList[0])
    # PrintAndLog("txCount = " + str(txCount))

    # toBlock_int = 6989615
    # fromBlock_int = 6988615
    # # toBlock_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() - 10000
    # # fromBlock_int = toBlock_int - 10000 - 30
    # jData_logs = Libraries.core.API_GetLogs_Batched(fromBlock_int, toBlock_int, Libraries.topics.BuildTopicsArray_WETH_WrapEther())
    # PrintAndLog("jData_logs = " + str(jData_logs))

    # blockNumber = 9548376
    # blockData = Libraries.core.API_GetBlock(blockNumber, True)
    # PrintAndLog("Block " + str(blockNumber) + ", timestamp = " + str(Libraries.core.ConvertHexToInt(blockData['timestamp'])))

    # tokenArray = [
    #     # ERROR
    #     '0xeb9951021698b42e4399f9cbb6267aa35f82d59d',
    #     '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
    # ]
    # resultDict = Libraries.core.API_ERC20Contract_GetDecimals_Batched(tokenArray)
    # PrintAndLog("resultDict = " + str(resultDict))

    # Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    # Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)
    #
    # tokenAddress_usdc = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48"
    # tokenAddress_weth = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2"
    #
    # tokenAddress = tokenAddress_weth
    #
    # decimals = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress)))
    #
    # # spenderContractAddress = Exchanges.set.Contract_TransferProxy
    # spenderContractAddress = Exchanges.zrxV2.Contract_ERC20Proxy
    #
    # approvedTokenAmount_ether = Libraries.core.API_GetTokenAllowance(Exchanges.ninja.Contract_Ninja, tokenAddress, spenderContractAddress, decimals)
    # PrintAndLog("approvedTokenAmount_ether = " + str(approvedTokenAmount_ether))

    # transactionHashList = ['0xd0cb705b683e07c74df1cdea72f9b4bc6fb4a41e54f197fd02b9e1464e691326', '0xb222a79cea471ba6265cb9515fa3f2cccbb987865b5cc6d28767871197e90dc7', '0x97452eacf884d3d69736c4b205beaa2b3aece64925bb6926f2d92b243029375e', '0xef553b02dafb1ba2bd31f3059d18cfc5df0ba666048a6d022658196351e781dd', '0xecc2077f7ce4bd0424278412bdb054dae96521d05480c6cac84ba3ecfaa43302', '0xb26e27c6afc4406bb1356cbf315c872beb3687c3cc0baf143ac3365d3458b2e1', '0x98ab0e3800a24740d51f62558cbf64691f876119d42c57b2e4366c0ae14fe1cd', '0x81ed46a1fb7577b61718571980de02d33b5f782d736c80d0f288b76e2f2da5d0', '0xffa54d97c42e7a79acfcda570f5083be545ee23fe646d58c7e75039c1b8c591e', '0xd721ffedc20627c47f707d4ee9e8bc18880153b6fde43061e38af439fa812874', '0x4be39684444d6289383b5c733e0568439d4971bfea40255410d14e5912899497', '0x45679d18d7db6ab408726aef559b65bc1e36c8f4374500e8017c6e6eb9985ce2', '0x1695a4498f46d4a741a96d3bb9f40e67a6f24223f3bb30628b46f82c1fd0cbeb', '0x59fe518f1c43fc27d21e0c08c6ecac232c5988f24e7494b18d0b5690bd7777e4', '0xaa167fe0897198ccdf4da693176dd3d906eda9bb9899a51f048cf437d6d0193d', '0xaefb6c45ff803a0caa4961b64e8eb110f151fc6ee1308cecfa7c46626e99affb', '0x54ffa5f243eec705225cd5ae4154e2a21b5728f542023295451f54c8878ce72b', '0x39a56c70cad784e2eaa5bb69936943a32e7aca78a609a32536827c0052b295e3', '0x593bc1698074ee6c3fc4096c4eee41585ed200bdd0aec185f85fab51404384b2', '0x922e6deb334f93403074b1e8f67664a8871f5f2bbc9997224664b093c7adeb6c', '0xfb82842a89d8a50a05719a4475b08f37f13a1cc29c31ad89392ba4852f734a80', '0x26cae27693d6d682ea5b173d85ac2b1ec31d247db2b65a142e6dc3c8c91ddf98']
    # # sum_usd_spentOnOrderCancels, sum_eth_spentOnOrderCancels = Libraries.tradeHistory.GetGasCostForTransactionList_InUSD_BasedOnTransactionDateTime(transactionHashList)
    # # PrintAndLog("sum_usd_spentOnOrderCancels = " + str(sum_usd_spentOnOrderCancels))
    # # PrintAndLog("sum_eth_spentOnOrderCancels = " + str(sum_eth_spentOnOrderCancels))
    # # resultList_transactionInfo = Libraries.core.API_GetTransactionInfo_Batched_Safe(transactionHashList)
    # # PrintAndLog("resultList_transactionInfo = " + str(resultList_transactionInfo))
    # resultList_transactionReceipt = Libraries.core.API_GetTransactionReceipt_Batched_Safe(transactionHashList)
    # PrintAndLog("resultList_transactionReceipt = " + str(resultList_transactionReceipt))
    #
    # if len(resultList_transactionInfo) != len(resultList_transactionReceipt):
    #     raise Exception("transaction info and transaction receipt array lenghts were not the same!")
    #
    # blockHashList = []
    # # Key is the blockHash, value is a tuple of (transactionHash, etherSpentOnTransaction)
    # blockHashTransactionHashRelationDict = {}
    # for index, transactionHash in enumerate(transactionHashList):
    #     gasPrice_wei = Libraries.core.ConvertHexToInt(resultList_transactionInfo[index]['gasPrice'])
    #     # PrintAndLog("gasPrice_wei = " + str(gasPrice_wei))
    #     gasPrice_ether = Libraries.core.ConvertWeiToEther(gasPrice_wei, Libraries.core.Ether_Decimals)
    #     # PrintAndLog("gasPrice_ether = " + str(gasPrice_ether))
    #     gasUsed = Libraries.core.ConvertHexToInt(resultList_transactionReceipt[index]['gasUsed'])
    #     # PrintAndLog("gasUsed = " + str(gasUsed))
    #     # gasUsed_ether = Libraries.core.ConvertWeiToEther(gasUsed_wei, Libraries.core.Ether_Decimals)
    #     # PrintAndLog("gasUsed_ether = " + str(gasUsed_ether))
    #
    #     etherSpentOnTransaction = gasUsed * gasPrice_ether
    #
    #     blockHash = resultList_transactionInfo[index]['blockHash']
    #     blockHashList.append(blockHash)
    #     # Maintain this relationship between the blockHash and the transactionHash and etherSpentOnTransaction
    #     blockHashTransactionHashRelationDict[blockHash] = (transactionHash, etherSpentOnTransaction)
    #
    # # We need to convert the ether to USD.  In order to do that I need to take the blockHashList and make a call against each of them
    # blockDataReturnList = Libraries.core.API_GetBlockByBlockHash_Batched_Safe(blockHashList, True)
    # # PrintAndLog("blockDataReturnList of len " + str(len(blockDataReturnList)))
    #
    # sum_usd_spentOnOrderCancels = 0
    # for index, jData_blockData in enumerate(blockDataReturnList):
    #     # PrintAndLog("jData_blockData = " + str(jData_blockData))
    #     blockHash = jData_blockData['hash']
    #     timestamp_unixEpoch = Libraries.core.ConvertHexToInt(jData_blockData['timestamp'])
    #     timestamp_datetime_UTC = datetime.datetime.utcfromtimestamp(timestamp_unixEpoch)
    #     # PrintAndLog("timestamp_datetime_UTC of blockHash " + str(blockHash) + " = " + str(timestamp_datetime_UTC))
    #     transactionHash, etherSpentOnTransaction = blockHashTransactionHashRelationDict[blockHash]
    #
    #     price_usd_forConversion = Libraries.tradeHistory.GetPrice_SymbolToUsd_AtDateTime('eth', timestamp_datetime_UTC)
    #     # PrintAndLog("etherSpentOnTransaction = " + str(etherSpentOnTransaction) + " ETH, transactionHash = " + str(transactionHash))
    #     usdSpentOnTransaction = etherSpentOnTransaction * price_usd_forConversion
    #     # PrintAndLog("usdSpentOnTransaction = $" + str(usdSpentOnTransaction) + " USD, price_usd_forConversion = " + str(price_usd_forConversion) + ", transactionHash = " + str(transactionHash))
    #     sum_usd_spentOnOrderCancels += usdSpentOnTransaction
    #
    #     PrintAndLog("I spent " + str(etherSpentOnTransaction) + " ETH which was $" + str(usdSpentOnTransaction) + " USD on transactionHash = " + str(transactionHash) + " on " + str(timestamp_datetime_UTC))
    #
    # PrintAndLog("sum_usd_spentOnOrderCancels = $" + str(round(sum_usd_spentOnOrderCancels, 2)) + " USD")

    # txHashList = []
    # for i in range(40000):
    #     txHashList.append("0xa2cfb38c30d5c1284dfb43c3efce375f8d85cf099e42a185a0ff104aa14401f5")
    #
    # responseList_txInfo = Libraries.core.API_GetTransactionInfo_Batched_Safe(txHashList)
    # responseList_txReceipt = Libraries.core.API_GetTransactionReceipt_Batched_Safe(txHashList)
    # # responseList_txInfo = Libraries.core.API_GetTransactionInfo_Batched(txHashList)
    # # responseList_txReceipt = Libraries.core.API_GetTransactionReceipt_Batched(txHashList)
    # PrintAndLog("responseList_txInfo of len " + str(len(responseList_txInfo)) + " = " + str(responseList_txInfo))
    # PrintAndLog("responseList_txReceipt of len " + str(len(responseList_txReceipt)) + " = " + str(responseList_txReceipt))

    # myTx = '0xa2cfb38c30d5c1284dfb43c3efce375f8d85cf099e42a185a0ff104aa14401f5'
    # response = Libraries.core.API_GetTransactionInfo(myTx)
    # PrintAndLog("response = " + str(response))

    # result = Libraries.core.API_GetBlockByBlockHash('0xedb79e9d8a23de16c65767043fc6db02484eb07aafcb5f1cefec31a2cac52fb7', False)
    # PrintAndLog("result = " + str(result))

    # result = Libraries.core.API_GetERC20Symbol('0x0d8775f648430679a709e98d2b0cb6250d2887ef')
    # PrintAndLog("result = " + str(result))

    # result = Libraries.core.API_GetTransactionReceipt('0x0b01ef1ede49a6e2aa9f92628918316f63f283ea712fd991da4a081a086de5b5')
    # PrintAndLog('result = ' + str(result))

    # # list = ['0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359', '0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2', '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48', '0xe41d2489571d322189246dafa5ebde1f4699f498', '0x0d8775f648430679a709e98d2b0cb6250d2887ef', '0xd26114cd6ee289accf82350c8d8487fedb8a0c07', '0x8f8221afbb33998d8584a2b05749ba73c37a938a', '0xb97048628db6b661d4c2aa833e95dbe1a905b280', '0x41e5560054824ea6b0732e656e3ad64e20e94e45', '0x744d70fdbe2ba4cf95131626614a1763df805b9e', '0x419d0d8bdd9af5e606ae2232ed285aff190e711b',  '0x9992ec3cf6a55b00978cddf2b27bc6882d88d1ec', '0x39bb259f66e1c59d5abef88375979b4d20d98022', '0xa15c7ebe1f07caf6bff097d8a589fb8ac49ae5b3', '0x1985365e9f78359a9b6ad760e32412f4a445e862', '0xdf2c7238198ad8b389666574f2d8bc411a4b7428', '0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c']
    # list = ['0x0d8775f648430679a709e98d2b0cb6250d2887ef']
    # list = ['0x02c4c78c462e32cca4a90bc499bf411fb7bc6afb', '0x01b3ec4aae1b8729529beb4965f27d008788b0eb']
    # resultDict = Libraries.core.API_GetERC20Symbol_Batched(list)
    # PrintAndLog("resultDict = " + str(resultDict))

    # for tokenAddress in list:
    #     if tokenAddress not in resultDict:
    #         PrintAndLog("tokenAddress " + str(tokenAddress) + " was not found in resultDict")

    # list = []
    # # TradesList_UnableToGenerateAKey_MissingTokenSymbol has 8364 trades that need data to be properly stored
    # for i in range(8364):
    # # for i in range(1360):
    #     list.append('0xddfd3470a3a40735111e593153497ded4de7e862b0d2c0dd50bf68e792caa879')
    #
    # list = ['0xddfd3470a3a40735111e593153497ded4de7e862b0d2c0dd50bf68e792caa879']
    # # list = ['0xddfd3470a3a40735111e593153497ded4de7e862b0d2c0dd50bf68e792caa879', '0xddfd3470a3a40735111e593153497ded4de7e862b0d2c0dd50bf68e792caa879','0x852884aaf821118e457822543b9757f262f881316dea94d451a055975bb1d904', '0x190387c9a6132df6ae79507c304525a1a31b8e1103a5a9b0f70b242191bc4df8', '0x4d839af7c39f147de25bf675eb08152ca44e7d9cca8c6ef4b16c82428cea738f', '0xcf853bc22c8d2ee155418ddade3e47c7445d0be558af650e685c1e959cb56fa7', '0xd16c85a4f7853fa3390512cb0786c1193cc857b11f32a7472de5bc963353a579', '0x58ec888b818d86e5c37283115c67d86ea945666840deeef018465d8f0205a484']
    # # resultList = Libraries.core.API_GetBlockByBlockHash_Batched(list, False)
    # resultList = Libraries.core.API_GetBlockByBlockHash_Batched_Safe(list, False)
    # PrintAndLog("resultList of len " + str(len(resultList)))

    # jData = Libraries.core.API_GetLogs(7009084, 7009484, Libraries.topics.BuildTopicsArray_0xv2_Fill(), Exchanges.zrxV2.Contract_Exchange)
    # toBLockNumber = 7009484
    # fromBlockNumber = toBLockNumber - 100000
    # jData = Libraries.core.API_GetLogs_Batched(fromBlockNumber, toBLockNumber, Libraries.topics.BuildTopicsArray_0xv2_Fill(), Exchanges.zrxV2.Contract_Exchange)
    # PrintAndLog("jData of len = " + str(len(jData)))
    #
    # testAddress = MarketDict['mkr-eth'].account.publicAddress
    # # Libraries.nonceUtils.SetNonceUsed(testAddress, 4557)
    # # Libraries.nonceUtils.SetNonceUsed(testAddress, 4558)
    #
    # transactionCount = Libraries.core.API_GetTransactionCount(testAddress, False)
    # PrintAndLog("transactionCount = " + str(transactionCount))
    #
    #
    #
    # fillOrderSucceededOn0xBasedOnEventLogs = False
    # successMatch = Libraries.core.DoesThisTransactionReceiptContain_0xFillOrderSuccess(txReceipt)
    # PrintAndLog("successMatch = " + str(successMatch))
    # if successMatch:
    #     fillOrderSucceededOn0xBasedOnEventLogs = True
    #
    # PrintAndLog("fillOrderSucceededOn0xBasedOnEventLogs = " + str(fillOrderSucceededOn0xBasedOnEventLogs))
    #

    # marketName = "bqx-eth"
    # MarketDict[marketName].account.SetNewPendingTransaction("0xfaf3a097bc71e3a3b168828efbea9a35e317fd862f9528f6793132432a27b8c3", TransactionType.other, 714)
    # MarketDict[marketName].account.RemovePendingTransaction_GivenTransactionId("0xfaf3a097bc71e3a3b168828efbea9a35e317fd862f9528f6793132432a27b8c3")

    # minProfit_eth = Libraries.gasStation.GetSuggested_Taker_MinimumProfitRequirement_Bancor()
    # PrintAndLog("minProfit_eth = " + str(minProfit_eth))

    # ConsiderUpdatingCoinMarketCapMarkets()
    #
    # for marketName in MarketDict:
    #     price_eth = CoinMarketCapMarketsDict[MarketDict[marketName].coinMarketCapId].GetPriceEth(MarketDict[marketName].coinMarketCapId, CoinMarketCapMarketsDict)
    #     PrintAndLog(marketName + "'s price_eth = " + str(price_eth))

    # ------ Buy/Sell on CX ------ #
    # MarketDict["bnb-eth"].GetFirstCX().PerformMarketOrder(9, "buy")
    # MarketDict["poe-eth"].GetFirstCX().PerformMarketOrder(45135.5, "buy")
    # MarketDict["rcn-eth"].GetFirstCX().PerformMarketOrder(250, "buy")
    # MarketDict["pay-eth"].GetFirstCX().PerformMarketOrder(409.6, "buy")
    # MarketDict["mft-eth"].GetFirstCX().PerformMarketOrder(1500, "buy")
    # MarketDict["rlc-eth"].GetFirstCX().PerformMarketOrder(15, "buy")
    # MarketDict["zil-eth"].GetFirstCX().PerformMarketOrder(200, "buy")
    # MarketDict["fun-eth"].GetFirstCX().PerformMarketOrder(8000, "sell")
    # MarketDict["storm-eth"].GetFirstCX().PerformMarketOrder(48305.4, "sell")
    # MarketDict["mft-eth"].GetFirstCX().PerformMarketOrder(30000, "sell")
    # MarketDict["npxs-eth"].GetFirstCX().PerformMarketOrder(50000, "sell")
    # MarketDict["vee-eth"].GetFirstCX().PerformMarketOrder(1000, "sell")

    # MarketDict["usdc-eth"].GetFirstDX().Deposit_Ether(5)

    # MarketDict["vee-eth"].GetExchange(ExchangeName_EtherDelta).ApproveAndDeposit_Tokens(0.1)
    # MarketDict["dnt-eth"].GetExchange(ExchangeName_ErcDex).Withdraw_Ether(11.998302883736539)

    # MarketDict["bnb-eth"].GetFirstDX().ApproveReset_Tokens()
    # MarketDict["bnb-eth"].GetFirstDX().ApproveAndDeposit_Tokens(300)

    # MarketDict["usdc-eth"].GetExchange(ExchangeName_RadarRelay2).Deposit_Ether(0.1)
    # MarketDict["wbtc-eth"].GetExchange(ExchangeName_RadarRelay2).Deposit_Ether(0.4)
    # MarketDict["usdc-eth"].GetExchange(ExchangeName_RadarRelay2).Deposit_Tokens(90420)
    # MarketDict["usdc-eth"].GetExchange(ExchangeName_RadarRelay2).Withdraw_Ether(900)
    # MarketDict["dai-eth"].GetExchange(ExchangeName_RadarRelay2).Withdraw_Ether(0.02)
    # MarketDict["bat-eth"].GetExchange(ExchangeName_RadarRelay2).Withdraw_Ether(32)
    # MarketDict["link-eth"].GetExchange(ExchangeName_RadarRelay2).Withdraw_Ether(34)

    # MarketDict["usdc-eth"].GetFirstCX().Deposit_Tokens(174880)
    # MarketDict["link-eth"].GetFirstCX().Deposit_Tokens(2592.15)
    # MarketDict["dai-eth"].GetFirstCX().Deposit_Tokens(6000)
    # MarketDict["bat-eth"].GetFirstCX().Deposit_Tokens(2)
    # MarketDict["wbtc-eth"].GetFirstCX().Deposit_Tokens(0.00015)

    # MarketDict["usdc-eth"].GetFirstCX().Deposit_Ether(120)
    # MarketDict["wbtc-eth"].GetFirstCX().Deposit_Ether(150)
    # MarketDict["dai-eth"].GetFirstCX().Deposit_Ether(30)
    # MarketDict["link-eth"].GetFirstCX().Deposit_Ether(25)
    # MarketDict["bat-eth"].GetFirstCX().Deposit_Ether(25)

    # MarketDict["bat-eth"].GetExchange(ExchangeName_EtherDelta).Withdraw_Tokens(12.0)
    # MarketDict["bat-eth"].GetExchange(ExchangeName_EtherDelta).Withdraw_Ether(11.9983028836)

    # MarketDict["usdc-eth"].GetFirstCX().Withdraw_Ether(90)

    # MarketDict["usdc-eth"].GetFirstCX().Withdraw_Tokens(26000)
    # MarketDict["dai-eth"].GetFirstCX().Withdraw_Tokens(1)
    # MarketDict["wbtc-eth"].GetFirstCX().Withdraw_Tokens(1.8627)

    # ------ Send Ether from one account to a specified address ------ #
    # marketName = "gto-eth"
    # destinationAddress = NinjaOpAccountDict["ninja-op-1"].publicAddress
    # value_wei_int = Libraries.core.ConvertEtherToWei(10, Libraries.core.Ether_Decimals)
    # txId = API_SendEther_ToAddress(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), destinationAddress, value_wei_int, Libraries.gasStation.GetGasLimit_Other(), Libraries.gasStation.GetGasPrice_Cheap(), TransactionType.other)

    # ------ Send Ether from one account to another ------ #
    # ninjaOp = NinjaOpAccountDict["ninja-op-100"]
    # destinationAddress = ninjaOp.publicAddress
    # value_wei_int = Libraries.core.ConvertEtherToWei(0, Libraries.core.Ether_Decimals)
    # txId = API_SendEther_ToAddress(ninjaOp.publicAddress, ninjaOp.DecryptPK(), destinationAddress, value_wei_int,
    #                                Libraries.gasStation.GetGasLimit_SendingEther(), Libraries.gasStation.GetGasPrice_Fast(), TransactionType.other)

    # ------ Send Tokens from one account to a specified address ------ #
    # marketName = "vib-eth"
    # destinationAddress = NinjaOpAccountDict["ninja-op-1"].publicAddress
    # tokenAmount = 23070
    # transactionId = API_SendTokens_ToAddress(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), tokenAmount, MarketDict[marketName].erc20TokenContractAddress, destinationAddress, MarketDict[marketName].decimals, TransactionType.other)

    # ------ Send Tokens from one account to another ------ #
    # marketName = "usdc-eth"
    # destinationAddress = MarketDict["dai-eth"].account.publicAddress
    # tokenAmount = 100
    # transactionId = API_SendTokens_ToAddress(MarketDict[marketName].account.publicAddress, MarketDict[marketName].account.DecryptPK(), tokenAmount, MarketDict[marketName].erc20TokenContractAddress, destinationAddress, MarketDict[marketName].decimals, TransactionType.other)
    # destinationAddress = "0x0015CB2187299D43B6313aE138a966899c72630c"
    # value_wei_int = Libraries.core.ConvertEtherToWei(0, Libraries.core.Ether_Decimals)
    # txId = API_SendEther_ToAddress("0x0015CB2187299D43B6313aE138a966899c72630c", "", destinationAddress, value_wei_int, Libraries.gasStation.GetGasLimit_Other(), Libraries.gasStation.GetGasPrice_Cheap(), TransactionType.other)

    # marketName = "cvc-eth"
    # destinationAddress = MarketDict[marketName].account.publicAddress
    # tokenAmount = 165.77616098
    # transactionId = API_SendTokens_ToAddress("0x65C2698F4a2D5883D9ae6d22E4CB633d5371ce9D", "", tokenAmount, MarketDict[marketName].erc20TokenContractAddress, destinationAddress, MarketDict[marketName].decimals, TransactionType.other)

    # PrintAndLog("Latest block = " + str(Libraries.core.API_PostGetLatestBlockNumber()))

    # transactionResult, txReceipt = API_PostCheckPendingTransaction("0x93954da4d1175864fd6a9fbb5ece873354a82b551c67ab8ed800046545881bb6")
    # PrintAndLog("transactionResult = " + str(transactionResult))
    # PrintAndLog("txReceipt = " + str(txReceipt))
    #
    # if txReceipt['to'].lower() == Exchanges.oasisDex.Contract_Exchange.lower():
    #     if transactionResult == TransactionResult.confirmed:
    #         Libraries.core.API_PostOperatorNotification("OasisDex transaction succeeded!")
    #     else:
    #         Libraries.core.API_PostOperatorNotification("OasisDex transaction failed")

    # txReceipt = Libraries.core.API_GetTransactionReceipt("0x61f338a4c86cbd94e55faf81ad97df4d0118d94b8c4eeec4af028f20037b6b4a")
    # PrintAndLog("txReceipt = " + str(txReceipt))
    # PrintAndLog(str(API_PostCheckPendingTransaction("0x61f338a4c86cbd94e55faf81ad97df4d0118d94b8c4eeec4af028f20037b6b4a")))

    # latestBlockNumber_hex = Libraries.core.API_PostGetLatestBlockNumber()
    # PrintAndLog(" latest block number " + str(int(latestBlockNumber_hex, 16)))

elif sys.argv and len(sys.argv) >= 2 and ("arb" == sys.argv[1].lower() or "tail" == sys.argv[1].lower()):
    PrintAndLog("len(sys.argv) = " + str(len(sys.argv)))

    # Extract Enums from the args passed in
    arguments = GetArgsFromSysArgv(sys.argv.copy())
    # PrintAndLog("arguments = " + str(arguments))

    # Must populate this list of Enum classes with each Enum we want to be automatically parsed when Ninja is started
    eligibleEnumClasses = [Config, Option]

    enumList = []
    # Parse all the arguments into Enums
    for arg in arguments:
        for enumClass in eligibleEnumClasses:
            enumResult = GetEnumValueFromArgument(arg, enumClass)
            if enumResult:
                enumList.append(enumResult)

    # Handle each Enum value we parsed and modify the ninja's configuration based on each one
    for localEnum in enumList:
        if isinstance(localEnum, Config):
            Libraries.config.SetActiveConfig(localEnum)
        elif isinstance(localEnum, Option):
            Libraries.config.SetOption(localEnum)
        else:
            raise Exception("localEnum not yet implemented: " + str(localEnum))

    # Enforce rules
    Libraries.config.EnforceRules(enumList)
    Libraries.config.SetConditionalOptions()

    # Load the security credentials AFTER handling the config
    Libraries.security.LoadSecurityCredentialsIntoMem()

    if "arb" == sys.argv[1].lower():
        # Arbitrage across DXs
        Libraries.utils.SetActiveScriptName(Libraries.utils.ScriptName.NinjaArb)
        BotLoops.ninjaLoops.NinjaLoop()
    elif "tail" == sys.argv[1].lower():
        # Tailgate across DXs
        Libraries.utils.SetActiveScriptName(Libraries.utils.ScriptName.NinjaTail)
        BotLoops.ninjaLoops.NinjaLoop()
    else:
        raise Exception("Not yet implemented: " + str(sys.argv[1].lower()))

else:
    PrintAndLog("Invalid arguments")
