import Libraries.defaults
import Libraries.config
from Libraries.config import IsOptionActive, Option
from Libraries.executeOnInterval import IsTimeToExecute

from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader

LoadingComplete_YouCanNowProcessEvents = False


# When a new block is mined, I must evaluate the data and trade based on the new information
def NinjaEvent_NewBlock(blockNumber):
    from Exchanges.ninja_arbitrage import NinjaGeneric_Config_B, NinjaGeneric_Config_A, NinjaGeneric_Config_C, NinjaGeneric_Config_Hide
    import Libraries.cache
    import Exchanges.set
    import Exchanges.ninja
    from Libraries.utils import GetDurationSinceDateTime_seconds
    from Libraries.core import API_PostOperatorNotification

    global LoadingComplete_YouCanNowProcessEvents

    PrintAndLog_FuncNameHeader("blockNumber = " + str(blockNumber))

    if not LoadingComplete_YouCanNowProcessEvents:
        PrintAndLog_FuncNameHeader("LoadingComplete_YouCanNowProcessEvents was False, let's wait for loading to complete before do process anything")
        return

    if not Libraries.defaults.EnableNinja_Events:
        message = "EnableNinja_Events is disabled, skipping all ninja event trading features"
        if IsTimeToExecute(message, 600):
            API_PostOperatorNotification(message)

        # It will continue so it skips all ninja logic after this
        return

    # trailing comma after here because it's the only argument and the comma basically declares it as a tuple...
    functionArguments_NinjaGeneric = blockNumber,

    # TODO, move my revert safety check to be inside the NinjaGeneric_Config_B where it can be path specific
    #  In other words, have it only block specific paths that revert. example: Uniswap-UniswapV2-Eth-Shitcoin
    #  this way when a series of reverts occur I can ban that one thing for x minutes/hours and I can review the logs later
    if GetDurationSinceDateTime_seconds(Exchanges.ninja.LastDateTime_NinjaTradeReverted) < Libraries.defaults.Ninja_StopTradingAfterReverts_seconds:
        message = "A Ninja trade failed recently so we're not trading for a few minutes"
        PrintAndLog_FuncNameHeader(message)
    else:
        # message = "No Ninja trades have failed recently. All good in the hood."
        # PrintAndLog_FuncNameHeader(message)

        # We want to call both UpdateTransactionCountCache and CleanUpOldMempoolTxs so that our mempool can be updated as fast as possible
        # to consider even the most recent block's transactions so we don't accidentally front run a transaction that mined in during the previous block
        IsTimeToExecute("Libraries.cache.UpdateTransactionCountCache_ThenCall_CleanUpOldMempoolTxs", 0,
                        Libraries.cache.UpdateTransactionCountCache_ThenCall_CleanUpOldMempoolTxs, True)

        # Efficient exchange protocols only
        if IsOptionActive(Option.ForceArb):
            message = "Option.ForceArb is using NinjaGeneric_Config_A, this should only be used for testing purposes"
            if IsTimeToExecute(message, 60):
                API_PostOperatorNotification(message)

            IsTimeToExecute("NinjaGeneric_Config_A", 0, NinjaGeneric_Config_A, True, True, True, *functionArguments_NinjaGeneric)
        elif Libraries.config.ActiveConfig == Libraries.config.Config.Hide:
            IsTimeToExecute("NinjaGeneric_Config_Hide", 0, NinjaGeneric_Config_Hide, True, True, True, *functionArguments_NinjaGeneric)
        else:
            IsTimeToExecute("NinjaGeneric_Config_B", 0, NinjaGeneric_Config_B, True, True, True, *functionArguments_NinjaGeneric)

        # IsTimeToExecute(key, interval_seconds_provideOnlyWhenRegisteringNew, functionToCallOnExecute=None,
        #                 callFunctionInBackgroundThread=False, doExecuteOnFirstKeyRegister=True,
        #                 doCreateUniqueLogFileForThisThreadsContents=False, *functionArguments)

        # Inefficient exchange protocols
        # TODO, I cannot re-enable anymore NinjaGenerics like NinjaGeneric_Config_C until I upgrade the GetPricesLogic
        #  Reason -> pricesCache within the Balancer exchange cannot be accessed by multiple threads
        #  So I must make that refactor where I start all threads making calls to exchange.GetPrices() exactly once and
        #  when it completes I then examine for arbitrage on only the completed exchanges.
        # TODO to get tailgating working faster, NinjaGeneric_Config_C and inefficient exchanges are now completely broken until I make that refactor listed above
        # IsTimeToExecute("NinjaGeneric_Config_C", 0, NinjaGeneric_Config_C, True, True, True, *functionArguments_NinjaGeneric)

        # TODO NinjaGeneric_Config_Set will need called again after this is completed with only Set related trade considerations
        # Call this before calling Ninja Set functions so that the Set data is up to date before we made decisions based on it
        if Libraries.defaults.EnableSet:
            Exchanges.set.UpdateAllSetTokenData_OnlySetsCurrentlyInRebalanceMode()
