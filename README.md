# Ninja
Ninja is a Keeper on KeeperDAO.  It's responsible for trading on Ethereum DXs through the KeeperDAO Liquidity Provider contracts with the goal of generating profit and paying fees into the Liquidity Provider Pool.  

Ninja is comprised of python code and a series of Ethereum smart contracts.  The Ninja python code has two basic functions: 1.) Look for profitable trade opportunities, and 2.) Trade through KeeperDAO to capture and distribute profit.  The Ninja smart contracts make it possible to perform a series of logic and trades within one single Ethereum transaction.  The contracts live under a proxy which provide scalability and upgradability.  

## Developer tools

#### Pycharm: https://www.jetbrains.com/pycharm/
#### VSCode: https://code.visualstudio.com/
#### Ethereum Remixer: https://remix.ethereum.org
#### Tenderly for gas analysis: https://dashboard.tenderly.co/tx/main/0xTxIdGoesHere/debugger
#### Parity tracing for tx internal tracing: https://etherscan.io/vmtrace?txhash=0xTxIdGoesHere&type=parity

## Python installation

    # We want Python 3.6.x.  I've used the following and all seem stable: 3.6.4, 3.6.8, 3.6.12
    # My personal OS preferences:
    #   MacOSX for development
    #   Ubuntu 20.04 (or optionally 18.04/16.04) for the servers that run the server 24/7/365
    # The following are instructions for installing Python on Ubuntu, 
    #   the MacOSX instructions will differ slightly due to Mac having one or two python package differences
    # Regardless of what OS you're one, the important thing is you install Python of the right version 
    #   AND you get all the dev tools.  If you do not get all the dev tools, 
    #   when installing the Ethereum related python packages you'll get strange errors and package conflicts
    
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt-get update
    sudo apt-get install python3.6 python3.6-dev python-dev build-essential libssl-dev libffi-dev automake pkg-config libtool libffi-dev libgmp-dev
    sudo apt install python3-pip
    sudo apt install virtualenv

## How to checkout and operate this repository
You'll want to checkout this repository within another folder that's dedicated to the Ninja repository.  I created a folder called "KeeperDAO" and then checkedout out the Ninja repository within there.  That KeeperDAO folder is only used for this Ninja codebase.  The reason for this is that the Ninja codebase will need things, like encrypted private keys, that should not be commited to the repository.  

    # Navigate to where you want the repository to exist
    mkdir KeeperDAO
    cd KeeperDAO
    git clone https://github.com/keeperdao/Ninja
    
    # The Ninja codebase will generate a folder called ArbyOperatorData as a sibling directory to Ninja
    # Below, you will create a python environment which is a sibling directory to Ninja

## Python environment configuration 
I use virtualenv which is very simple to use and maintain.  It allows us to manage python packages specifically for this project.  So we'll need to install virtualenv, then activate our environment, and then install the python packages that Ninja requires
    
    # Create environment using python3
    # First, locate your python3 that you just installed by doing the following command 
    which python3.6
    
    # Take the path you get from the command above, and use it for the command below
    # Also do a sanity check of the python verison to make sure you're using the right version
    # The below command should output the correct version you intend to install
    /usr/bin/python3.6 --version
    virtualenv -p /usr/bin/python3.6 Env_Ninja
    
    # Activate the environment
    source Env_Ninja/bin/activate
    
    # Install python packages
    # The root directory of the codebase contains requirements.txt files 
    #   based on which OS you're using, for this example we're using Ubuntu's
    pip install -r Ninja/requirements_ubuntu.txt

## Python IDE
For python, I recommend coding in PyCharm.  https://www.jetbrains.com/pycharm/.  It's free and works well.  It behaves similarly to Webstorm or Visual Studio Code.  

# Ninja sample usage

## Get Properties
`python ninja.py testninja_getproperties`

    (MainThread) -------------------------- Ninja 0xE8bEA818de87dD3E5cE9547120936c822b5B9502 --------------------------
    (MainThread)    Ether balance = 180.96267 ETH
    (MainThread)    Token balance = 0.69113 WETH
    (MainThread)    Token balance = 0.57266 WBTC
    (MainThread)    Token balance = 5163.3453 USDC
    (MainThread)    Token balance = 1.33423 LINK
    (MainThread)    Gas token quantity = 231.84
    (MainThread)    kyberProxyContract = 0x818e6fecd516ecc3849daf6845e3ec868087b755
    (MainThread)    wethContract = 0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2
    (MainThread)    zrxV3ExchangeContract = 0x61935cbdd02287b511119ddb11aeb42f1593b7ef
    (MainThread)    kyberEthTokenContract = 0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
    (MainThread)    doLogEvents = 1
    (MainThread)    killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx = 36622
    (MainThread)    gasPriceThresholdForSpendingGasTokens = 5040000000
    (MainThread)    minQuoteTokenAllowedToSpendDivider = 3
    (MainThread) -------------------------- Ninja Operators --------------------------
    (MainThread) SendRequestToSpecifiedNodes: resultDict for ['eth_getBalance', 'eth_getBalance'] = {'joeyz_FRA1_geth_6': <Response [200]>}
    (MainThread) ninja-op-100: 10.08 ETH
    (MainThread) ninja-op-101: 1.052 ETH


## Ninja smart contract overview

This ninja deployment consists of arbs across various protocols like Uniswap/Kyber/Set/0x.  Several protocols (OasisDEX/Bancor/etc) were left behind during a recent upgrade.  They can be added back the future.  This is a temporary repo until we all dive into the ninja code and decide where this will live. 

## This Ninja is currently live on Mainnet

For Ninja, there is effectivly one single keeper and that's the python-based automation program.  I have a series of "ninja operators" (AKA "ninjaOps") which are Ethereum accounts that perform trades to the Ninja smart contract.  In this case, I'm also the liquidity provider as well.  I have a pool of ETH, USDC, WBTC from which I trade.  I can support any ERC20 as a quote token and any ERC20 as the base token.  

#### `Ninja_Proxy.sol`: 
mainnet: 0x3d71d79c224998e608d03c5ec9b405e7a38505f0

Proxy contract using the Diamond Standard https://github.com/mudgen/Diamond.git.  This enables my smart contracts to be upgradable and scalable.  It enables me to route calls through various logic contracts underneith the proxy.  It allows me to host all assets in one single smart contract.  

#### `Ninja_AssetManagement.sol`: 
mainnet: 

Manages asset deposits/withdraws/allowances.  

#### `Ninja_GasTokens.sol`: 
mainnet: 

Enables use of gas tokens to minimize gas expense when trading. 

#### `Ninja_Properties.sol`: 
mainnet: 

Configurable properties that the logic contracts need to access.  

#### `Ninja_Authentication_KeeperDAOLPPs.sol`: 
mainnet: 

Whitelist authentication for KeeperDAO LPPs that are allowed to call the Ninja's trade functions inside of a flash loan.

#### `Ninja_Trade_2.sol`: 
mainnet: 

Trades on all exchanges using ETH or any ERC20 as the quote token.


## Exchange integrations for generic Ninja

#### `Operational`

 - Uniswap
 - UniswapV2
 - Kyber
 - 0xv3
 - Balancer
 - Sushiswap
 - Defiswap
 - Sakeswap
 - Curve

#### `In progress`
 - Swerve
 - Set (currently operational on old Ninja v1)

#### `TODO`
 - Compound 
 - dYdX
 - MakerDAO
 - mStable
 - Oasis
 - Bancor
 

## Multiprocessing

Ninja's python code is currently using multiple processes for increased efficiency.  The pro to multiprocessing is that it dramatically increases performance and scalability to the system.  The con to multiprocessing is that it dramatically increases code complexity and maintainability.  I believe that I have worked out most all the kinds related to this feature and I'm now considering Ninja stable again since making this upgrade.  This will, however, make things more difficult for new hires and new people entering into the code base.  

Ninja currently has one main process, and one sub process per quoteToken that's being traded.  Each sub process is responsible for making node network calls to get price data associated with trades that originate from that quoteToken.  Once the price data is gathered, it's then sent back to the main process.  The main process receives the data from all sub processess, merges the data into one single ratesGraph, then calculates arbitrage on all the data and determines where and when to trade.  

#### QuoteTokens
- ETH/WETH
- USDC
- DAI
- renBTC
- USDT (will get added soon once I fix a bug related to approving USDT token transfers)

One of the biggest challenges I ran into with multiprocessing was communication between processes.  I've found several ways and am listing them below

#### multiprocessing.Manager().Queue()
These are simple and easy event driven messages, but they have some limitations.  Do not send large pieces of data through queues, as they do not perform well.  Only send simple data/objects/json/etc.  They are one directional.  If you want to send data bi-directional, you'll have to make two queues.  One queue for each direction.  

#### PyZMQ
This is a messanging service that works very well for any size chunk of data.  I'm currently using this for sending very large chunks of data, specifically the ratesGraphs which are massive arrays of rate/price data for Ninja's arbitrage algorithms.  https://pyzmq.readthedocs.io/en/latest/index.html

#### Shared memory
These are shared memory variables.  I've found this to not be very useful due to limitations so I'm not using it at the moment.  You can really only share lists, dicts, numbers, and strings.  And they are not event driven.  And even the lists and dicts have extreme limitations.  This does not perform well with large chunks of data.  

# Ninja production commands
#### Start ninja that trades in the wild on production
`python ninja.py arb wild trade notfiy`

#### Start ninja that trades through the Hiding Game only on production
`python ninja.py arb hide trade notify`

#### Generate a token list for the HidingBook
`python ninja.py arb hide genhidelist`
 
# Ninja commands

You must choose exactly one command.

## Arbitrage 
Arbitrage across DXs based on trade opportunities that are mined into blocksacross DXs based on trade opportunities that are mined into blocks
 
`python ninja.py arb`

## Tailgating
 Tailgate popular DXs by back running other traders to capture arbitrage before it ever gets mined into a block
 
`python ninja.py tail`

# Ninja configs 

You must choose exactly one config.  

## wild
Trade in the wild on chain on all DXs.  You will be battling villains and rival keepers.  Prepare to be front run, grim triggered, and tailgated.

`python ninja.py arb wild`

## hide
The Hiding Game only.  Connect to the Hiding Game's server and facilitate KeeperDAO user's limit order trades via other on chain DXs. Do not trade in the wild unless it's helping fill a user's order.

`python ninja.py arb hide`

# Ninja options 

You may choose as many options as you like.

## lite
Run ninja with significantly fewer quoteTokens, baseTokens, and quoteTokenAmounts than normal. This is ideal for testing since it doesn't hammer the logs with text and is easier on CPU/memory usage.

`python ninja.py arb wild lite`

## de
Use only Germany nodes  

`python ninja.py arb wild de`

## us
Use only United States nodes

`python ninja.py arb wild us`

## trade
Enable trade execution, trade transactions will be signed by any private keys. This could be dangerous if you do not know what you're doing. 

`python ninja.py arb wild trade`

## nokeys
Inform Ninja that no private keys are available for signing transactions. It will still be able to find arbitrage and go through the motions, but it will not sign any transactions since no private keys are available.  

`python ninja.py arb wild nokeys`

## notify
Inform Ninja that the operator should receive notifications during run time. Only JoeyZ uses this for now

`python ninja.py arb wild notify`

## tm
Time machine TODO, doesn't yet work with the config

`python ninja.py arb tail tm=11585596`

## forcearb
Force find arbitrage by lowering trade amount to dust revealing crossing spot prices on AMMs. 
Note: this is extremely dangerous. It's great for testing but can be extremely expensive if you do not know exactly what you're doing.

`python ninja.py arb wild forcearb`

## forceflash
Force a flash loan regardless of whether or not ninja has the tokens needed to facilitate the trade

`python ninja.py arb wild forceflash`

## recommended commands for nick to use when developing/testing

`python ninja.py arb wild nokeys notify`

`python ninja.py arb wild lite nokeys notify`
