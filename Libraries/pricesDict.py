# These are short hand optimized strings for json dictionaries. If I use full meaningful strings, the data will get large and slower
# Making them short makes them hard to read so I made variables for them so that the code is readable.
# But the value remains short and light weight for processing efficiency
# pdk means pricesDictKey

pdk_quoteTokens = 'qt'
pdk_baseTokens = 'bt'
pdk_exchanges = 'x'
pdk_prices = 'p'
pdk_quoteTokensToSpend_etherUnits = 'q'
pdk_bid = 'b'
pdk_ask = 'a'
pdk_metaData = 'md'
pdk_exchangeQuoteTokenBalance_etherUnits = 'eq'
pdk_exchangeBaseTokenBalance_etherUnits = 'eb'
pdk_best_bidPrice_exchange_quoteTokenBalance = 'beq'
pdk_best_bidPrice_exchange = 'be'
pdk_best_askPrice_exchange_quoteTokenBalance = 'aeq'
pdk_best_askPrice_exchange = 'ae'
pdk_value = 'v'
