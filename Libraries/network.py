from enum import Enum


class Network(Enum):
    none = "None"
    mainnet = "Mainnet"
    rinkebyTestnet = "Rinkeby Testnet"
    kovanTestnet = "Kovan Testnet"


def GetChainId():
    import Libraries.defaults

    if Libraries.defaults.ActiveNetwork == Network.mainnet:
        return int(1)
    else:
        raise Exception("Not yet implemented")


class NodeInfrastructure(Enum):
    DE = "DE"
    US = "US"
    AlchemyOnly = "AlchemyOnly"
    