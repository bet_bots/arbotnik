import xlrd
import datetime


def GetDateTimeFromExcelDate(book, excelDate):
    return datetime.datetime(*xlrd.xldate_as_tuple(excelDate, book.datemode))
