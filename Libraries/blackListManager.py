from Libraries.loggingConfig import PrintAndLog
from threading import Lock
import datetime


class BlacklistedOrderManagerObject:
    BlacklistedOrderDict = None
    MyLock = None

    def __init__(self):
        self.BlacklistedOrderDict = {}
        self.MyLock = Lock()

    class BlacklistedOrder:
        id = None
        dateTimeOfBlacklist = None

        def __init__(self, _id):
            self.id = _id
            self.dateTimeOfBlacklist = datetime.datetime.now()

    def BlacklistThisOrderId(self, id):
        self.MyLock.acquire()
        try:
            id = id.lower()
            self.BlacklistedOrderDict[id] = self.BlacklistedOrder(id)

        finally:
            self.MyLock.release()

    def IsIDBlacklisted(self, id):
        returnValue = False

        self.MyLock.acquire()
        try:
            id = id.lower()
            if id in self.BlacklistedOrderDict:
                returnValue = True

        finally:
            self.MyLock.release()

        return returnValue

    def ConsiderExpiringBlacklistedOrders(self):
        self.MyLock.acquire()
        try:
            PrintAndLog("ConsiderExpiringBlacklistedOrders: len(self.BlacklistedOrderDict) Before = " + str(len(self.BlacklistedOrderDict)))
            dateTimeNow = datetime.datetime.now()
            # Expire all old blacklisted orders by removing them from the list
            for id in list(self.BlacklistedOrderDict):
                if (dateTimeNow - self.BlacklistedOrderDict[id].dateTimeOfBlacklist).total_seconds() > 18000:
                    # remove it
                    PrintAndLog("Expiring blacklisted order and removing it from the self.BlacklistedOrderDict: " + id)
                    del self.BlacklistedOrderDict[id]
                    pass

            PrintAndLog("ConsiderExpiringBlacklistedOrders: len(self.BlacklistedOrderDict) After = " + str(len(self.BlacklistedOrderDict)))

        finally:
            self.MyLock.release()
