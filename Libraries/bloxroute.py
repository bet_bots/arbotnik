import copy
import json
import traceback
from random import randint
from enum import Enum

import jsonpickle
import websocket
import time
import threading
import datetime

import Libraries.core
import Libraries.gasStation
import Libraries.defaults
from Libraries.loggingConfig import PrintAndLog_Websockets, PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader, PrintAndLog_Blocks, PrintAndLog_Header
from Libraries.executeOnInterval import IsTimeToExecute

WebsocketClientDict = {}

LatestBlockNumberDict = {}

NodeType = "Bloxroute"

SendTxTestDict = {}

BroadcastTxSubscriptionPayloadIdDict = {}


class SubscriptionType(Enum):
    NewTxs = "newTxs"
    PendingTxs = "pendingTxs"
    BlxrTx = "blxr_tx"


def ConnectToAllWebSocketClients(subscribeToMempool=True):
    from Libraries.nodes import BloxrouteList

    for bloxroute in BloxrouteList:
        ConnectWebSocketClient(bloxroute, subscribeToMempool)


def ConnectWebSocketClient(bloxroute, subscribeToMempool=True):
    global WebsocketClientDict
    WebsocketClientDict[bloxroute.value] = WebsocketClientObject(bloxroute, subscribeToMempool)


def BroadcastTxToAllBloxrouteGateways(raw_tx_bytes):
    global WebsocketClientDict

    if len(WebsocketClientDict.keys()) <= 0:
        raise Exception("No WebsocketClientDicts have been set!")

    for bloxroute in WebsocketClientDict:
        try:
            WebsocketClientDict[bloxroute].BroadcastTx(raw_tx_bytes)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("Exception in BroadcastTxToAllBloxrouteGateways: " + traceback.format_exc())


def BroadcastTxToAllBloxrouteGateways_Synchronous(raw_tx_bytes, resultsDict, key):
    global WebsocketClientDict
    global BroadcastTxSubscriptionPayloadIdDict

    if len(WebsocketClientDict.keys()) <= 0:
        raise Exception("No WebsocketClientDicts have been set!")

    subscriptionPayloadId = randint(0, 99999999999999)
    # Prep the subscriptionPayloadId to wait for the response
    BroadcastTxSubscriptionPayloadIdDict[subscriptionPayloadId] = None
    for bloxroute in WebsocketClientDict:
        try:
            WebsocketClientDict[bloxroute].BroadcastTx(raw_tx_bytes, subscriptionPayloadId)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("Exception in BroadcastTxToAllBloxrouteGateways: " + traceback.format_exc())

    timeout_s = Libraries.core.RequestTimeout_broadcastTx_seconds
    sleepInterval_s = 0.005
    before = datetime.datetime.now()
    # Wait for the response for a second or so, if it does not come back just return
    while True:
        # Check to see if the callback has given us a txHash yet
        txHash = BroadcastTxSubscriptionPayloadIdDict[subscriptionPayloadId]
        if txHash:
            PrintAndLog_FuncNameHeader("Found a txHash from the websocket response callback, txHash = " + str(txHash))
            resultsDict[key] = txHash
            break

        duration_s = (datetime.datetime.now() - before).total_seconds()

        if duration_s > timeout_s:
            PrintAndLog_FuncNameHeader("Breaking from loop due to timeout")
            break

        time.sleep(sleepInterval_s)


class WebsocketClientObject(object):
    ws = None
    nodeName = None
    subscribeToMempool = None
    url_ws = None
    # When we subscribe to a feature, we populate this dict with the id used in the subscribe payload
    # When we receive a message via websockets we'll use that id to match with the subscriptionId
    subscriptionPayloadIdDict = None
    # We need a dictionary to associate the subscriptionType with the subscriptionPayloadId
    subscriptionIdDict = None

    def __init__(self, _bloxroute, _subscribeToMempool):
        self.nodeName = _bloxroute.name
        self.url_ws = _bloxroute.value
        self.subscribeToMempool = _subscribeToMempool
        self.subscriptionPayloadIdDict = {}
        self.subscriptionIdDict = {}

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        websocket.enableTrace(False)
        websocket.http_proxy_host = self.url_ws
        self.ws = websocket.WebSocketApp(self.url_ws,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)

        while True:
            try:
                # Check the defaults flag here because I will be using this websockets connection for a variety of things
                # So if I want to disable mempool for this service I should just not subscribe to it here
                if Libraries.defaults.EnableMempool and self.subscribeToMempool and Libraries.defaults.EnableMempool_Bloxroute:
                    # Subscribe to events
                    IsTimeToExecute("SubscribeWebsockets_Mempool", 0, self.SubscribeWebsockets_Mempool, True)

                Libraries.core.API_PostOperatorNotification("Connecting to websockets: " + str(self.GetUniqueNodeName()))
                self.ws.run_forever(ping_interval=30, ping_timeout=10)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in websockets, " + str(self.GetUniqueNodeName()) + " = " + traceback.format_exc())
                pass

            PrintAndLog("Websockets disconnected! Sleeping a bit then trying again. " + str(self.GetUniqueNodeName()))
            time.sleep(30)

    def on_message(self, ws, message):
        from Libraries.mempool import HandleIncomingMempoolTx, MempoolServiceName, Key_MempoolServiceName

        global LatestBlockNumberDict

        # Sample response for blxr_tx with synchronous false
        # {'jsonrpc': '2.0', 'id': 45950363634314, 'result': {'tx_hash': 'not available with async', 'quota_type': 'paid_daily_quota', 'synchronous': 'False'}}
        # Sample response for blxr_tx with synchronous true
        # {'jsonrpc': '2.0', 'id': 91465051432194, 'result': {'tx_hash': '6bbf76b0ca334d8cb6e93067524e9967f162d67e259ac075c9990d3406c9c600', 'quota_type': 'paid_daily_quota', 'account_id': '4e5eaa2d-feaf-4561-b537-406245606e44'}}
        # Sample subscription response
        # {'jsonrpc': '2.0', 'id': 85050627463811, 'result': '0xc624794b6a328d430ab7160e3c2ec38'}
        # new Sample response for blxr_tx with synchronous true, they changed the format
        # {'jsonrpc': '2.0', 'id': 68407634286232, 'result': {'txHash': '84f1805bcc2f7a5145ff9882d2cc2e2ce75902e2475bbc9881ff5133ff36f651', 'quotaType': 'paid_daily_quota', 'accountId': '4e5eaa2d-feaf-4561-b537-406245606e44'}}
        # Sample mempool response
        # {'jsonrpc': '2.0', 'id': None, 'method': 'subscribe', 'params': {'subscription': 'e62ef7c6-813d-4741-902b-64fe5ee3c9a5', 'result': {'txHash': '0x36b3e86bd8caa39f956395c1127aa12c5b2961c8e6d3f5ab052362522eebcb84'}}}

        header = str(self.GetUniqueNodeName()) + ": Websockets"
        try:
            jData = json.loads(message)
            # PrintAndLog_Header(header, "jData = " + str(jData))

            if 'id' in jData:
                # For some reason they're sending an id of None for mempool txs
                if jData['id'] is None:
                    if 'params' in jData and 'result' in jData['params'] and 'txHash' in jData['params']['result']:
                        txHash = jData['params']['result']['txHash']
                        # PrintAndLog_Header(header, "found txHash " + str(txHash))
                        # Update the data to include the mempoolServiceName
                        jData['params']['result']['txContents'][Key_MempoolServiceName] = MempoolServiceName.Bloxroute.value
                        HandleIncomingMempoolTx(jData['params']['result']['txContents'])

                else:
                    # Check for a response to a tx broadcast
                    if 'result' in jData and 'txHash' in jData['result']:
                        txHash = jData['result']['txHash'].lower()
                        # For some reason they do not include the 0x...
                        if len(txHash) == Libraries.core.LengthOfTransactionHash_Excludes0x:
                            txHash = '0x' + txHash
                        # PrintAndLog_Header(header, "received tx_hash result, txHash = " + str(txHash))
                        # Set the result in the dict
                        BroadcastTxSubscriptionPayloadIdDict[jData['id']] = txHash

                    # Else assume it's a subscription response
                    else:
                        # subscription response
                        # {'jsonrpc': '2.0', 'id': 85050627463811, 'result': '0xc624794b6a328d430ab7160e3c2ec38'}
                        for subscriptionType in SubscriptionType:
                            PrintAndLog_Header(header, "subscriptionType.value = " + str(subscriptionType.value))
                            PrintAndLog_Header(header, "self.subscriptionPayloadIdDict = " + str(self.subscriptionPayloadIdDict))
                            if subscriptionType.value in self.subscriptionPayloadIdDict and jData['id'] == self.subscriptionPayloadIdDict[subscriptionType.value]:
                                # Associate the subscriptionType with the subscriptionPayloadId
                                self.subscriptionIdDict[jData['result']] = subscriptionType.value

                        PrintAndLog_Header(header, "self.subscriptionIdDict = " + str(self.subscriptionIdDict))

            elif 'method' in jData and jData['method'] == 'eth_subscription':
                params = jData['params']
                subscriptionId = params['subscription']
                subscriptionTypeValue = self.subscriptionIdDict[subscriptionId]
                content = params['result']
                blockNumber = Libraries.core.ConvertHexToInt(content['number'])
                blockTimestamp = Libraries.core.ConvertHexToInt(content['timestamp'])
                # PrintAndLog_Blocks(str(self.nodeName) + " found " + str(subscriptionTypeValue) + " with blockNumber = " + str(
                #     blockNumber) + ", blockTimestamp = " + str(blockTimestamp) + ", " + str(time.time()))

                LatestBlockNumberDict[self.nodeName] = blockNumber
                # PrintAndLog_Header(header, "Calling ResultHandlerDelegate_DetermineHighestResult_int_LatestBlockNumberOnly_Generic with "
                #                    "LatestBlockNumberDict = " + str(LatestBlockNumberDict))

                Libraries.core.ResultHandlerDelegate_DetermineHighestResult_int_LatestBlockNumberOnly_Generic(
                    copy.deepcopy(LatestBlockNumberDict), None, True)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when parsing message in WebsocketClientObject: " + traceback.format_exc() + ", message = " + str(message))
            pass

    def on_error(self, ws, error):
        PrintAndLogError(str(error))

    def on_close(self, ws):
        message = str(self.GetUniqueNodeName()) + ": Disconnected from websockets."
        PrintAndLogError(message)
        Libraries.core.API_PostOperatorNotification(message)

    def GetUniqueNodeName(self):
        return NodeType + " " + self.url_ws

    def SubscribeWebsockets_Mempool(self, doSleepFirst=True):
        # Give websockets time to connect
        if doSleepFirst:
            time.sleep(4)

        # NewTxs is every single new tx and you're notified before any validation is done, so these txs could be either invalid or not make mempool
        # PendingTxs includes txs that made the mempool and were validated, but they come in a hair slower
        subscriptionType = SubscriptionType.PendingTxs.value
        # Associate the subscriptionPayloadId with the SubscriptionTypes
        subscriptionPayloadId = randint(0, 99999999999999)
        self.subscriptionPayloadIdDict[subscriptionType] = subscriptionPayloadId

        payload = {
            "id": subscriptionPayloadId,
            "method": "subscribe",
            "params": [subscriptionType, {}]
        }
        self.SendWebsocketPayload(payload)

    def BroadcastTx(self, raw_tx_bytes, subscriptionPayloadId=None):
        # PrintAndLog_Websockets("BroadcastTx raw_tx_bytes = " + str(raw_tx_bytes))
        raw_tx_bytes = raw_tx_bytes.replace("0x", "")
        if not subscriptionPayloadId:
            subscriptionPayloadId = randint(0, 99999999999999)
        # Associate the subscriptionPayloadId with the SubscriptionTypes
        self.subscriptionPayloadIdDict[SubscriptionType.BlxrTx.value] = subscriptionPayloadId

        # synchronous = False  # Bloxroute will not return the txHash
        synchronous = True  # Bloxroute will return the txHash
        payload = {
            "id": subscriptionPayloadId,
            "method": SubscriptionType.BlxrTx.value,
            "params": {
                "transaction": raw_tx_bytes,
                "synchronous": str(synchronous),
            },
        }
        self.SendWebsocketPayload(payload)

    def SendWebsocketPayload(self, payload):
        PrintAndLog_Websockets("SendWebsocketPayload to payload = " + str(payload))
        self.ws.send(jsonpickle.encode(payload))


def SendTestTx(account, gasPrice, nonce, sendTxInterval, currentTime_ms, runCode):
    from Libraries.transactions import API_SendEther_ToAddress, TransactionType

    global SendTxTestDict

    txHash = API_SendEther_ToAddress(account.publicAddress, account.DecryptPK(), account.publicAddress, 0,
                                     Libraries.gasStation.GetGasLimit_SendingEther(), gasPrice, TransactionType.other, nonce)
    # txHash = randint(1, 10000000)

    if account.publicAddress.lower() not in SendTxTestDict:
        SendTxTestDict[account.publicAddress.lower()] = {}

    SendTxTestDict[account.publicAddress.lower()][str(sendTxInterval)] = {
        'sendTxInterval': sendTxInterval,
        'currentTime_ms': currentTime_ms,
        'txHash': txHash,
        'gasPrice': Libraries.core.ConvertWeiToGwei(gasPrice),
        'nonce': nonce,
        'runCode': runCode,
    }


def UpdateSendTxTestDictWithBlockNumbers():
    global SendTxTestDict

    # Construct a tx hash list
    txHashList = []
    for accountAddress in Libraries.bloxroute.SendTxTestDict:
        for sendTxInterval in Libraries.bloxroute.SendTxTestDict[accountAddress]:
            txHash = Libraries.bloxroute.SendTxTestDict[accountAddress][sendTxInterval]['txHash']
            txHashList.append(txHash)

    txReceiptList = Libraries.core.API_GetTransactionReceipt_Batched_Safe(txHashList)
    for txReceipt in txReceiptList:
        # Update each entry in the dict with the block number it was mined in
        for accountAddress in Libraries.bloxroute.SendTxTestDict:
            for sendTxInterval in Libraries.bloxroute.SendTxTestDict[accountAddress]:
                txHash = Libraries.bloxroute.SendTxTestDict[accountAddress][sendTxInterval]['txHash']
                if txHash.lower() == txReceipt['transactionHash'].lower():
                    blockNumber = Libraries.core.ConvertHexToInt(txReceipt['blockNumber'])
                    Libraries.bloxroute.SendTxTestDict[accountAddress][sendTxInterval]['blockNumber'] = blockNumber
