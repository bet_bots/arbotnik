def GetNinjaOpAccountGivenPublicAddress(address):
    from Libraries.accounts import NinjaOpAccountDict

    for name in NinjaOpAccountDict:
        if NinjaOpAccountDict[name].publicAddress.lower() == address.lower():
            return NinjaOpAccountDict[name]
