import json
from web3 import Web3

from Libraries.loggingConfig import PrintAndLog
import Libraries.core
import Libraries.nodes
from Libraries.utils import ConsiderSettingResultDict
import Exchanges.zrx
import Exchanges.zrxV2
import Exchanges.etherDelta
import Exchanges.kyber
import Exchanges.oasisDex
import Exchanges.uniswap


def API_GetAllBalances(exchange, user, tokensList):
    from Contracts.contracts import Contract_ArbyUtility

    kwargs = {
        'exchange': exchange,
        'user': user,
        'tokens': tokensList,
    }

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('allBalances', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog("API_GetAllBalances responseData = " + str(responseData))
        jData = json.loads(responseData)
        data = jData['result']

        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        PrintAndLog("dataList = " + str(dataList))
        return ParseResponseData_GetAllBalancesForManyAccounts(dataList, [user], tokensList)

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetAllBalances response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetAllBalancesForManyAccounts(exchange, usersList, tokensList, mergedResultDict={}, lock_mergedResultDict=None):
    # PrintAndLog("API_GetAllBalancesForManyAccounts with " + str(len(usersList)) + " users and " + str(len(tokensList)) + " tokens")
    from Contracts.contracts import Contract_ArbyUtility

    kwargs = {
        'exchange': exchange,
        'users': usersList,
        'tokens': tokensList,
    }

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('allBalancesForManyAccounts', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetAllBalancesForManyAccounts responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetAllBalancesForManyAccounts jData = " + str(jData))
        data = jData['result']

        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("dataList = " + str(dataList))
        # Set and return the resultDict
        resultDict = ParseResponseData_GetAllBalancesForManyAccounts(dataList, usersList, tokensList)

        # Lock so I can safely update mergedResultDict with the resultDict
        if lock_mergedResultDict:
            lock_mergedResultDict.acquire()
            try:
                mergedResultDict.update(resultDict)  # modifies z with y's keys and values & returns None
                # mergedResultDict = Libraries.core.MergeDictionaries(resultDict, mergedResultDict)
                # Libraries.core.MergeDictionaries(mergedResultDict, resultDict)

            finally:
                lock_mergedResultDict.release()

        else:
            mergedResultDict.update(resultDict)  # modifies z with y's keys and values & returns None

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetAllBalancesForManyAccounts response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def ParseResponseData_GetManyReturns_Ints(dataList, decimals):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))

    returnList = []
    for data in dataList:
        value_base = Libraries.core.ConvertHexToInt(data)
        value_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(value_base), decimals)
        # PrintAndLog("value_ether = " + str(value_ether) + ", value_base = " + str(value_base))
        returnList.append(value_ether)

    return returnList


def ParseResponseData_GetManyReturns_Bools(dataList):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))

    returnList = []
    for data in dataList:
        value_base = Libraries.core.ConvertHexToInt(data)
        if value_base == 0:
            returnList.append(False)
        elif value_base == 1:
            returnList.append(True)
        else:
            returnList.append(None)

    return returnList


def ParseResponseData_GetManyFills_Ints(dataList, decimalsList):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))

    returnList = []
    for index, data in enumerate(dataList):
        value_base = Libraries.core.ConvertHexToInt(data)
        value_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(value_base), decimalsList[index])
        # PrintAndLog("value_ether = " + str(value_ether) + ", value_base = " + str(value_base))
        returnList.append(value_ether)

    return returnList


def ParseResponseData_GetAllBalancesForManyAccounts(dataList, usersList, tokensList):
    # The dataList will contain data for each token in tokensList for each user in usersList... All units are in wei, so let's make the data more readable
    # Remove the first two items in the array
    # Divide the rest of the times by the number of users
    # Send each divided item into this function

    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))

    numOfItemsExpectedPerResponse = len(tokensList) * 2
    usersResultList = Libraries.core.DivideListIntoNsmallerLists(dataList, numOfItemsExpectedPerResponse)
    # PrintAndLog("usersResultList = " + str(usersResultList))

    # Key the return dict by the user
    returnDict = {}
    for index, userData in enumerate(usersResultList):
        # PrintAndLog("userData =  " + str(userData))
        # userData_number = []
        userData_dict = {}
        indexCorrespondingWith_tokensList = 0
        maxCount_indexCorrespondingWith_tokensList = 2
        counter_indexCorrespondingWith_tokensList = 0
        for x in range(0, len(userData)):
            data = userData[x]
            # PrintAndLog("Iterating: x = " + str(x) + ", indexCorrespondingWith_tokensList = " + str(indexCorrespondingWith_tokensList) + ", where data = " + str(data))
            balance_base = Libraries.core.ConvertHexToInt(data)
            # PrintAndLog("balance_base = " + str(balance_base))
            # Convert the base to ether
            balance_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(balance_base),
                                                                          float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokensList[indexCorrespondingWith_tokensList]))))
            # PrintAndLog("balance_ether = " + str(balance_ether))
            # userData_number.append(balance_ether)
            # I need to store the result (balance_ether) in a dictionary keyed by the token (tokensList[indexCorrespondingWith_tokensList])

            # only increment indexCorrespondingWith_tokensList every other time since we see each token from the tokensList twice (since it's checking balances in two places)
            counter_indexCorrespondingWith_tokensList += 1

            doIncrementIndexAndResetCounter = False
            # If this is the second time through
            if counter_indexCorrespondingWith_tokensList >= maxCount_indexCorrespondingWith_tokensList:
                # PrintAndLog("Second time through, key = " + str(tokensList[indexCorrespondingWith_tokensList]) + ". userData_dict[tokensList[indexCorrespondingWith_tokensList]] = " + str(userData_dict[tokensList[indexCorrespondingWith_tokensList]]))
                doIncrementIndexAndResetCounter = True

            # Else this is the first time through
            else:
                # PrintAndLog("First time through, key = " + str(tokensList[indexCorrespondingWith_tokensList]) + ". userData_dict[tokensList[indexCorrespondingWith_tokensList]] = Key Error")
                # Create a new array in userData_dict for the results
                userData_dict[tokensList[indexCorrespondingWith_tokensList]] = []

            # append balance_ether to the userData_dict array it as the first item
            userData_dict[Libraries.nodes.Instance_Web3.toChecksumAddress(tokensList[indexCorrespondingWith_tokensList])].append(balance_ether)

            if doIncrementIndexAndResetCounter:
                indexCorrespondingWith_tokensList += 1
                counter_indexCorrespondingWith_tokensList = 0

        # set the result in returnDict
        returnDict[Libraries.nodes.Instance_Web3.toChecksumAddress(usersList[index])] = userData_dict

    # PrintAndLog("returnDict = " + str(returnDict))
    return returnDict


def ParseResponseData_OasisDex_GetOrderbook(dataList, decimals_pay_token, decimals_buy_token):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))
    # PrintAndLog("dataList with fist two items removed is of len " + str(len(dataList)))

    returnList = []
    index = 0
    while index < len(dataList):
        # Each item has 4 sets of data
        # uint     pay_amt;
        # Token    pay_gem;
        # uint     buy_amt;
        # Token    buy_gem;

        pay_amt_weiUnits = Libraries.core.ConvertHexToInt(dataList[index + 0])
        pay_token = Libraries.core.GetAddressFromDataProperty(dataList[index + 1])
        buy_amt_weiUnits = Libraries.core.ConvertHexToInt(dataList[index + 2])
        buy_token = Libraries.core.GetAddressFromDataProperty(dataList[index + 3])

        pay_amt_etherUnits = Libraries.core.ConvertBaseAmountToEtherAmount(pay_amt_weiUnits, decimals_pay_token)
        buy_amt_etherUnits = Libraries.core.ConvertBaseAmountToEtherAmount(buy_amt_weiUnits, decimals_buy_token)

        index += 4

        if pay_amt_etherUnits == 0 and buy_amt_etherUnits == 0 \
                and pay_token.lower() == Libraries.core.GetNullAddress().lower() and buy_token.lower() == Libraries.core.GetNullAddress().lower():
            # Do not include this one, it's all zeros...
            pass
        else:
            # PrintAndLog("pay_amt_etherUnits = " + str(pay_amt_etherUnits) + ", pay_token = " + str(pay_token) + ", buy_amt_etherUnits = " + str(buy_amt_etherUnits) + ", buy_token = " + str(buy_token))
            returnList.append((pay_amt_etherUnits, pay_token, buy_amt_etherUnits, buy_token))

    return returnList


def API_GetAllWETHbalances(wethAddress, usersList):
    from Contracts.contracts import Contract_ArbyUtility

    PrintAndLog("API_GetAllWETHbalances with " + str(len(usersList)) + " users")

    kwargs = {
        'wethAddress': Libraries.nodes.Instance_Web3.toChecksumAddress(wethAddress),
        'users': usersList,
    }

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('allWETHbalances', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetAllWETHbalances responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetAllWETHbalances jData = " + str(jData))
        data = jData['result']

        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetAllWETHbalances dataList = " + str(dataList))

        # Remove the first two items from the array, we don't need them
        dataList.pop(0)
        dataList.pop(0)
        # PrintAndLog("dataList after first two items are removed = " + str(dataList))

        # Key the return dict by the user
        returnDict = {}
        for index, data in enumerate(dataList):
            balance_base = Libraries.core.ConvertHexToInt(data)
            # PrintAndLog("balance_base = " + str(balance_base))
            # Convert the base to ether
            balance_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(balance_base), Libraries.core.Ether_Decimals)
            # PrintAndLog("balance_ether = " + str(balance_ether))

            returnDict[Libraries.nodes.Instance_Web3.toChecksumAddress(usersList[index])] = balance_ether

        # PrintAndLog("returnDict = " + str(returnDict))
        return returnDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetAllWETHbalances response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


# Calls to my ArbyUtility contract which can make many calls within one request
# Libraries.arbyUtility.API_GetManyReturns_Bancor(bancorConverterContract, fromToken, toToken, amountsList)
def API_ConverterContract_GetManyReturns(bancorConverterContractAddress, fromTokenAddress, toTokenAddress, amountList_inFromTokens_ether, decimals_fromToken, decimals_toToken):
    from Contracts.contracts import Contract_ArbyUtility

    # PrintAndLog("API_ConverterContract_GetManyReturns bancorConverterContractAddress = " + str(bancorConverterContractAddress) + ", fromTokenAddress = " + str(
    #     fromTokenAddress) + ", toTokenAddress = " + str(toTokenAddress) + ", amountList_inFromTokens_ether = " + str(amountList_inFromTokens_ether))

    # PrintAndLog("bancorConverterContractAddress = " + str(bancorConverterContractAddress))
    amountList_inFromTokens_wei = []
    for amount_inFromTokens_ether in amountList_inFromTokens_ether:
        # PrintAndLog("amount_inFromTokens_ether = " + str(amount_inFromTokens_ether) + ", decimals_fromToken = " + str(decimals_fromToken))
        amount_inFromTokens_wei = Libraries.core.ConvertEtherToWei(amount_inFromTokens_ether, decimals_fromToken)
        # PrintAndLog("amount_inFromTokens_wei = " + str(amount_inFromTokens_wei))
        amountList_inFromTokens_wei.append(amount_inFromTokens_wei)

    kwargs = {
        'bancorConverterContract': Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverterContractAddress),
        'fromToken': Libraries.nodes.Instance_Web3.toChecksumAddress(fromTokenAddress),
        'toToken': Libraries.nodes.Instance_Web3.toChecksumAddress(toTokenAddress),
        'amounts': amountList_inFromTokens_wei
    }

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyReturns_Bancor', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_ConverterContract_GetManyReturns responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_ConverterContract_GetManyReturns jData = " + str(jData))

        # Parse the resulting array.  Break it into items, skip the first two items, then each item after that is a result. Populate and return the array of results.
        # Check ParseResponseData in arbyUtility.py
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_ConverterContract_GetManyReturns: dataList = " + str(dataList))
        returnValueList_ether = ParseResponseData_GetManyReturns_Ints(dataList, decimals_toToken)
        # PrintAndLog("returnValueList_ether = " + str(returnValueList_ether))
        return returnValueList_ether

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_ConverterContract_GetManyReturns response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_Airswap_GetManyFills(airswapContractAddress, orderHashList):
    from Contracts.contracts import Contract_ArbyUtility

    # PrintAndLog("API_Airswap_GetManyFills: airswapContractAddress = " + str(airswapContractAddress) + ", orderHashList = " + str(orderHashList))

    hashesList_toUse = []
    for orderHash in orderHashList:
        hashesList_toUse.append(Web3.toBytes(hexstr=str(orderHash)))

    kwargs = {
        'airswapContract': Libraries.nodes.Instance_Web3.toChecksumAddress(airswapContractAddress),
        'hashes': hashesList_toUse,
    }
    # PrintAndLog("kwargs = " + str(kwargs))

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyFills_Airswap', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_Airswap_GetManyFills responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_Airswap_GetManyFills jData = " + str(jData))

        # Parse the resulting array.  Break it into items, skip the first two items, then each item after that is a result. Populate and return the array of results.
        # Check ParseResponseData in arbyUtility.py
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_Airswap_GetManyFills: dataList = " + str(dataList))
        returnValueList = ParseResponseData_GetManyReturns_Bools(dataList)
        # PrintAndLog("API_Airswap_GetManyFills: returnValueList = " + str(returnValueList))
        return returnValueList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_Airswap_GetManyFills response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_0xv1_GetManyFills_Safe(orderHashList, orderAmountFilledDecimalsList, resultsDict, resultKey):
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 300  # No breaking point found even up to 500, I could increase this number if need be

    orderHashList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(orderHashList, chunkSize)
    orderAmountFilledDecimalsList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(orderAmountFilledDecimalsList, chunkSize)

    concatenatedList = []
    for i in range(len(orderHashList_chunked)):
        concatenatedList += API_0xv1_GetManyFills(orderHashList_chunked[i], orderAmountFilledDecimalsList_chunked[i])

    if resultKey:
        resultsDict[resultKey] = concatenatedList

    return concatenatedList


def API_0xv2_GetManyFills_Safe(orderHashList, orderAmountFilledDecimalsList, resultsDict, resultKey):
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 300  # No breaking point found even up to 500, I could increase this number if need be

    orderHashList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(orderHashList, chunkSize)
    orderAmountFilledDecimalsList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(orderAmountFilledDecimalsList, chunkSize)

    concatenatedList = []
    for i in range(len(orderHashList_chunked)):
        concatenatedList += API_0xv2_GetManyFills(orderHashList_chunked[i], orderAmountFilledDecimalsList_chunked[i])

    if resultKey:
        resultsDict[resultKey] = concatenatedList

    return concatenatedList


def API_0xv1_GetManyFills(orderHashList, orderAmountFilledDecimalsList):
    return API_0x_GetManyFills(orderHashList, orderAmountFilledDecimalsList, Exchanges.zrx.Contract_Exchange)


def API_0xv2_GetManyFills(orderHashList, orderAmountFilledDecimalsList):
    return API_0x_GetManyFills(orderHashList, orderAmountFilledDecimalsList, Exchanges.zrxV2.Contract_Exchange)


def API_0x_GetManyFills(orderHashList, orderAmountFilledDecimalsList, contractAddress_0x):
    from Contracts.contracts import Contract_ArbyUtility

    PrintAndLog("API_0x_GetManyFills: contractAddress_0x = " + str(contractAddress_0x) + ", orderHashList = " + str(orderHashList))

    if len(orderHashList) != len(orderAmountFilledDecimalsList):
        raise Exception("API_0x_GetManyFills: length of lists must be the same")

    hashesList_toUse = []
    for orderHash in orderHashList:
        hashesList_toUse.append(Web3.toBytes(hexstr=str(orderHash)))

    kwargs = {
        'contractAddress0x': Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress_0x),
        'hashes': hashesList_toUse,
    }
    # PrintAndLog("kwargs = " + str(kwargs))

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyFills_0x', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_0x_GetManyFills responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_0x_GetManyFills jData = " + str(jData))

        # Parse the resulting array.  Break it into items, skip the first two items, then each item after that is a result. Populate and return the array of results.
        # Check ParseResponseData in arbyUtility.py
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_0x_GetManyFills: dataList = " + str(dataList))
        returnValueList = ParseResponseData_GetManyFills_Ints(dataList, orderAmountFilledDecimalsList)
        # PrintAndLog("API_0x_GetManyFills: returnValueList = " + str(returnValueList))
        return returnValueList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_0x_GetManyFills response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_EtherDelta_GetManyAmountFilled_Safe(tokenGetList, amountGetList, tokenGiveList, amountGiveList, expiresList,
                                            nonceList, userList, vList, rList, sList, orderAmountFilledDecimalsList,
                                            resultsDict, resultKey):
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 100  # Found a breaking point somewhere between 200 and 500.

    tokenGetList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(tokenGetList, chunkSize)
    amountGetList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(amountGetList, chunkSize)
    tokenGiveList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(tokenGiveList, chunkSize)
    amountGiveList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(amountGiveList, chunkSize)
    expiresList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(expiresList, chunkSize)
    nonceList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(nonceList, chunkSize)
    userList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(userList, chunkSize)
    vList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(vList, chunkSize)
    rList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(rList, chunkSize)
    sList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(sList, chunkSize)
    orderAmountFilledDecimalsList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(orderAmountFilledDecimalsList, chunkSize)

    concatenatedList = []
    for i in range(len(tokenGetList_chunked)):
        concatenatedList += API_EtherDelta_GetManyAmountFilled(tokenGetList_chunked[i], amountGetList_chunked[i], tokenGiveList_chunked[i],
                                                               amountGiveList_chunked[i], expiresList_chunked[i], nonceList_chunked[i],
                                                               userList_chunked[i], vList_chunked[i], rList_chunked[i], sList_chunked[i], orderAmountFilledDecimalsList_chunked[i])
    if resultKey:
        resultsDict[resultKey] = concatenatedList

    return concatenatedList


def API_EtherDelta_GetManyAmountFilled(tokenGetList, amountGetList, tokenGiveList, amountGiveList, expiresList,
                                       nonceList, userList, vList, rList, sList, orderAmountFilledDecimalsList):
    from Contracts.contracts import Contract_ArbyUtility

    if len(tokenGetList) != len(amountGetList) or len(tokenGetList) != len(tokenGiveList) or \
            len(tokenGetList) != len(amountGiveList) or len(tokenGetList) != len(expiresList) or \
            len(tokenGetList) != len(nonceList) or len(tokenGetList) != len(userList) or \
            len(tokenGetList) != len(vList) or len(tokenGetList) != len(rList) or \
            len(tokenGetList) != len(sList) or len(tokenGetList) != len(orderAmountFilledDecimalsList):
        raise Exception("API_EtherDelta_GetManyAmountFilled: length of lists must be the same")

    tokenGetList_toUse = []
    amountGetList_toUse = []
    tokenGiveList_toUse = []
    amountGiveList_toUse = []
    expiresList_toUse = []
    nonceList_toUse = []
    userList_toUse = []
    vList_toUse = []
    rList_toUse = []
    sList_toUse = []
    for index, tokenGet in enumerate(tokenGetList):
        tokenGetList_toUse.append(str(tokenGetList[index]))
        amountGetList_toUse.append(int(amountGetList[index]))
        tokenGiveList_toUse.append(str(tokenGiveList[index]))
        amountGiveList_toUse.append(int(amountGiveList[index]))
        expiresList_toUse.append(int(expiresList[index]))
        nonceList_toUse.append(int(nonceList[index]))
        userList_toUse.append(str(userList[index]))
        vList_toUse.append(int(vList[index]))
        rList_toUse.append(Web3.toBytes(hexstr=str(rList[index])))
        sList_toUse.append(Web3.toBytes(hexstr=str(sList[index])))

    kwargs = {
        'contractAddressEtherDelta': Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.etherDelta.Contract_Exchange),
        'tokenGet': tokenGetList_toUse,
        'amountGet': amountGetList_toUse,
        'tokenGive': tokenGiveList_toUse,
        'amountGive': amountGiveList_toUse,
        'expires': expiresList_toUse,
        'nonce': nonceList_toUse,
        'user': userList_toUse,
        'v': vList_toUse,
        'r': rList_toUse,
        's': sList_toUse,
    }
    # PrintAndLog("kwargs = " + str(kwargs))

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyAmountFilled_EtherDelta', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_EtherDelta_GetManyAmountFilled responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_EtherDelta_GetManyAmountFilled jData = " + str(jData))

        # Parse the resulting array.  Break it into items, skip the first two items, then each item after that is a result. Populate and return the array of results.
        # Check ParseResponseData in arbyUtility.py
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_EtherDelta_GetManyAmountFilled: dataList = " + str(dataList))
        returnValueList = ParseResponseData_GetManyFills_Ints(dataList, orderAmountFilledDecimalsList)
        # PrintAndLog("API_EtherDelta_GetManyAmountFilled: returnValueList = " + str(returnValueList))
        return returnValueList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_EtherDelta_GetManyAmountFilled response was not ok response = " + str(response))
        response.raise_for_status()


def API_Kyber_GetExpectedRates(kyberProxyContractAddress, sourceTokenAddress, destinationTokenAddress,
                               sourceQuantityList_etherUnits, decimals_sourceToken, decimals_destinationToken=None):
    from Contracts.contracts import Contract_ArbyUtility

    # PrintAndLog("API_Kyber_GetExpectedRates kyberProxyContractAddress = " + str(kyberProxyContractAddress) + ", sourceTokenAddress = " + str(
    #     sourceTokenAddress) + ", destinationTokenAddress = " + str(destinationTokenAddress) + ", sourceQuantityList_etherUnits = " + str(sourceQuantityList_etherUnits))

    sourceQuantityList_weiUnits = []
    for sourceQuantity_etherUnits in sourceQuantityList_etherUnits:
        # PrintAndLog("sourceQuantity_etherUnits = " + str(sourceQuantity_etherUnits))
        amount_inFromTokens_wei = Libraries.core.ConvertEtherToWei(sourceQuantity_etherUnits, decimals_sourceToken)
        # PrintAndLog("amount_inFromTokens_wei = " + str(amount_inFromTokens_wei))
        sourceQuantityList_weiUnits.append(amount_inFromTokens_wei)

    kwargs = {
        'kyberProxyContract': Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContractAddress),
        'sourceToken': Libraries.nodes.Instance_Web3.toChecksumAddress(sourceTokenAddress),
        'destinationToken': Libraries.nodes.Instance_Web3.toChecksumAddress(destinationTokenAddress),
        'sourceQuantities': sourceQuantityList_weiUnits
    }
    # PrintAndLog("API_Kyber_GetExpectedRates kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getExpectedRates_Kyber', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_Kyber_GetExpectedRates responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_Kyber_GetExpectedRates jData = " + str(jData))

        # Parse the resulting array.  Break it into items, skip the first two items, then each item after that is a result. Populate and return the array of results.
        # Check ParseResponseData in arbyUtility.py
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_Kyber_GetExpectedRates: dataList = " + str(dataList))
        rateList_ether = ParseResponseData_GetManyReturns_Ints(dataList, Libraries.core.Ether_Decimals)
        # PrintAndLog("API_Kyber_GetExpectedRates: rateList_ether = " + str(rateList_ether))

        priceList_etherUnits = []
        for rate_etherUnits in rateList_ether:
            side = None
            if sourceTokenAddress.lower() == Exchanges.kyber.EtherToken.lower():
                side = 'buy'
            elif destinationTokenAddress.lower() == Exchanges.kyber.EtherToken.lower():
                side = 'sell'
            else:
                raise Exception("Not by or sell?")

            price_etherUnits = Exchanges.kyber.ConvertRateToPrice_GivenAddresses(sourceTokenAddress, destinationTokenAddress, rate_etherUnits)
            priceList_etherUnits.append(price_etherUnits)

        # PrintAndLog("API_Kyber_GetExpectedRates: priceList_etherUnits = " + str(priceList_etherUnits))
        return priceList_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_Kyber_GetExpectedRates response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyBalances(usersList, tokensList, tokenDecimalsList):
    from Contracts.contracts import Contract_ArbyUtility

    # PrintAndLog("API_GetManyBalances usersList = " + str(usersList) + ", tokensList = " + str(tokensList))

    #     function getManyBalances(
    #     address[] users,
    #     address[] tokens

    usersList_toUse = []
    for user in usersList:
        usersList_toUse.append(Libraries.nodes.Instance_Web3.toChecksumAddress(user))

    tokensList_toUse = []
    for token in tokensList:
        tokensList_toUse.append(Libraries.nodes.Instance_Web3.toChecksumAddress(token))

    kwargs = {
        'users': usersList_toUse,
        'tokens': tokensList_toUse,
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyBalances', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetManyBalances responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyBalances jData = " + str(jData))

        # Parse the resulting array.  Break it into items, skip the first two items, then each item after that is a result. Populate and return the array of results.
        # Check ParseResponseData in arbyUtility.py
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyBalances: dataList = " + str(dataList))
        valueList_etherUnits = ParseResponseData_GetManyFills_Ints(dataList, tokenDecimalsList)
        # PrintAndLog("API_GetManyBalances: valueList_etherUnits = " + str(valueList_etherUnits))

        return valueList_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyBalances response was not ok response = " + str(response))
        response.raise_for_status()


# TODO, make a API_GetManyAllowances_Batched, and a API_GetManyAllowances_Batched_Safe
def API_GetManyAllowances(usersList, tokensList, tokenDecimalsList, spender, resultsDict=None, key=None):
    from Contracts.contracts import Contract_ArbyUtility

    # PrintAndLog("API_GetManyAllowances usersList = " + str(usersList) + ", tokensList = " + str(tokensList))

    #     function getManyAllowances(
    #         address[] users,
    #         address[] tokens,
    #         address spender

    usersList_toUse = []
    for user in usersList:
        usersList_toUse.append(Libraries.nodes.Instance_Web3.toChecksumAddress(user))

    tokensList_toUse = []
    for token in tokensList:
        tokensList_toUse.append(Libraries.nodes.Instance_Web3.toChecksumAddress(token))

    kwargs = {
        'users': usersList_toUse,
        'tokens': tokensList_toUse,
        'spender': Libraries.nodes.Instance_Web3.toChecksumAddress(spender)
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyAllowances', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetManyAllowances responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyAllowances jData = " + str(jData))

        # Parse the resulting array.  Break it into items, skip the first two items, then each item after that is a result. Populate and return the array of results.
        # Check ParseResponseData in arbyUtility.py
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyAllowances: dataList = " + str(dataList))
        valueList_etherUnits = ParseResponseData_GetManyFills_Ints(dataList, tokenDecimalsList)
        # PrintAndLog("API_GetManyAllowances: valueList_etherUnits = " + str(valueList_etherUnits))
        ConsiderSettingResultDict(valueList_etherUnits, resultsDict, key)
        return valueList_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyAllowances response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetOasisDexOrderbook(buy_gem, pay_gem, maxIterations):
    from Contracts.contracts import Contract_ArbyUtility

    PrintAndLog("API_GetOasisDexOrderbook buy_gem = " + str(buy_gem) + ", pay_gem = " + str(pay_gem) + ", maxIterations = " + str(maxIterations))

    # function getOasisDexOrderbook(
    #     address oasisDexContract,
    #     Token buy_gem,
    #     Token pay_gem,
    #     uint maxIterations
    # ) public view returns (OasisDex_Lib.OfferInfo[]) {

    oasisDexContract = Exchanges.oasisDex.Contract_Exchange

    kwargs = {
        'oasisDexContract': Libraries.nodes.Instance_Web3.toChecksumAddress(oasisDexContract),
        'buy_gem': Libraries.nodes.Instance_Web3.toChecksumAddress(buy_gem),
        'pay_gem': Libraries.nodes.Instance_Web3.toChecksumAddress(pay_gem),
        'maxIterations': int(maxIterations)
    }
    PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getOasisDexOrderbook', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetOasisDexOrderbook responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetOasisDexOrderbook jData = " + str(jData))

        # Parse the resulting array.  Break it into items, skip the first two items, then each item after that is a result. Populate and return the array of results.
        # Check ParseResponseData in arbyUtility.py
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetOasisDexOrderbook: dataList = " + str(dataList))

        decimals_pay_gem = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(pay_gem)))
        decimals_buy_gem = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(buy_gem)))

        valueList = ParseResponseData_OasisDex_GetOrderbook(dataList, decimals_pay_gem, decimals_buy_gem)
        # PrintAndLog("API_GetOasisDexOrderbook: valueList = " + str(valueList))

        return valueList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetOasisDexOrderbook response was not ok response = " + str(response))
        response.raise_for_status()


def ParseResponseData_Uniswap_GetManyPrices(dataList, decimals):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))
    # PrintAndLog("dataList with fist two items removed is of len " + str(len(dataList)))

    returnList = []
    for data in dataList:
        amount_weiUnits = Libraries.core.ConvertHexToInt(data)
        amount_etherUnits = Libraries.core.ConvertBaseAmountToEtherAmount(amount_weiUnits, decimals)
        returnList.append(amount_etherUnits)
        # PrintAndLog("data = " + str(data) + ", amount_weiUnits = " + str(amount_weiUnits) + ", amount_etherUnits = " + str(amount_etherUnits))

    return returnList


def API_GetManyEthToTokenInputPrices(erc20TokenContractAddress, amountList_etherUnits, decimals):
    from Contracts.contracts import Contract_ArbyUtility

    uniswapContract = Exchanges.uniswap.ContractDict_Exchange[erc20TokenContractAddress.lower()]['exchange']

    amountList_weiUnits = []
    for amount_etherUnits in amountList_etherUnits:
        # PrintAndLog("amount_etherUnits = " + str(amount_etherUnits) + ", decimals = " + str(decimals))
        amount_weiUnits = Libraries.core.ConvertEtherToWei(amount_etherUnits, decimals)
        # PrintAndLog("amount_weiUnits = " + str(amount_weiUnits))
        amountList_weiUnits.append(amount_weiUnits)

    kwargs = {
        'uniswapContract': Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract),
        'eth_sold_array': amountList_weiUnits,
    }
    PrintAndLog("API_GetManyEthToTokenInputPrices: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyEthToTokenInputPrices_Uniswap', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyEthToTokenInputPrices jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyEthToTokenInputPrices: dataList = " + str(dataList))

        valueList = ParseResponseData_Uniswap_GetManyPrices(dataList, decimals)
        # PrintAndLog("API_GetManyEthToTokenInputPrices: valueList = " + str(valueList))

        # Convert valueList to priceList
        priceList = []
        for index, orderPropertySet in enumerate(valueList):
            priceList.append(amountList_etherUnits[index] / valueList[index])

        PrintAndLog("API_GetManyEthToTokenInputPrices: priceList = " + str(priceList))
        return priceList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyEthToTokenInputPrices response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyEthToTokenOutputPrices(erc20TokenContractAddress, amountList_etherUnits, decimals):
    from Contracts.contracts import Contract_ArbyUtility

    uniswapContract = Exchanges.uniswap.ContractDict_Exchange[erc20TokenContractAddress.lower()]['exchange']

    amountList_weiUnits = []
    for amount_etherUnits in amountList_etherUnits:
        # PrintAndLog("amount_etherUnits = " + str(amount_etherUnits) + ", decimals = " + str(decimals))
        amount_weiUnits = Libraries.core.ConvertEtherToWei(amount_etherUnits, decimals)
        # PrintAndLog("amount_weiUnits = " + str(amount_weiUnits))
        amountList_weiUnits.append(amount_weiUnits)

    kwargs = {
        'uniswapContract': Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract),
        'tokens_bought_array': amountList_weiUnits,
    }
    # PrintAndLog("API_GetManyEthToTokenOutputPrices: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyEthToTokenOutputPrices_Uniswap', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
        ]
    }
    PrintAndLog("API_GetManyEthToTokenOutputPrices: payload = " + str(payload))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyEthToTokenOutputPrices jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyEthToTokenOutputPrices: dataList = " + str(dataList))

        valueList = ParseResponseData_Uniswap_GetManyPrices(dataList, decimals)
        # PrintAndLog("API_GetManyEthToTokenOutputPrices: valueList = " + str(valueList))

        # Convert valueList to priceList
        priceList = []
        for index, orderPropertySet in enumerate(valueList):
            priceList.append(valueList[index] / amountList_etherUnits[index])

        PrintAndLog("API_GetManyEthToTokenOutputPrices: priceList = " + str(priceList))
        return priceList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyEthToTokenOutputPrices response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyTokenToEthInputPrices(erc20TokenContractAddress, amountList_etherUnits, decimals):
    from Contracts.contracts import Contract_ArbyUtility

    uniswapContract = Exchanges.uniswap.ContractDict_Exchange[erc20TokenContractAddress.lower()]['exchange']

    amountList_weiUnits = []
    for amount_etherUnits in amountList_etherUnits:
        # PrintAndLog("amount_etherUnits = " + str(amount_etherUnits) + ", decimals = " + str(decimals))
        amount_weiUnits = Libraries.core.ConvertEtherToWei(amount_etherUnits, decimals)
        # PrintAndLog("amount_weiUnits = " + str(amount_weiUnits))
        amountList_weiUnits.append(amount_weiUnits)

    kwargs = {
        'uniswapContract': Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract),
        'tokens_sold_array': amountList_weiUnits,
    }
    # PrintAndLog("API_GetManyTokenToEthInputPrices: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyTokenToEthInputPrices_Uniswap', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyTokenToEthInputPrices jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyTokenToEthInputPrices: dataList = " + str(dataList))

        valueList = ParseResponseData_Uniswap_GetManyPrices(dataList, decimals)
        # PrintAndLog("API_GetManyTokenToEthInputPrices: valueList = " + str(valueList))

        # Convert valueList to priceList
        priceList = []
        for index, orderPropertySet in enumerate(valueList):
            priceList.append(valueList[index] / amountList_etherUnits[index])

        PrintAndLog("API_GetManyTokenToEthInputPrices: priceList = " + str(priceList))
        return priceList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyTokenToEthInputPrices response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyTokenToEthOutputPrices(erc20TokenContractAddress, amountList_etherUnits, decimals):
    from Contracts.contracts import Contract_ArbyUtility

    uniswapContract = Exchanges.uniswap.ContractDict_Exchange[erc20TokenContractAddress.lower()]['exchange']

    amountList_weiUnits = []
    for amount_etherUnits in amountList_etherUnits:
        # PrintAndLog("amount_etherUnits = " + str(amount_etherUnits) + ", decimals = " + str(decimals))
        amount_weiUnits = Libraries.core.ConvertEtherToWei(amount_etherUnits, decimals)
        # PrintAndLog("amount_weiUnits = " + str(amount_weiUnits))
        amountList_weiUnits.append(amount_weiUnits)

    kwargs = {
        'uniswapContract': Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract),
        'eth_bought_array': amountList_weiUnits,
    }
    # PrintAndLog("API_GetManyTokenToEthOutputPrices: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_ArbyUtility.encodeABI('getManyTokenToEthOutputPrices_Uniswap', kwargs=kwargs),
                "to": Contract_ArbyUtility.address
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyTokenToEthOutputPrices jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyTokenToEthOutputPrices: dataList = " + str(dataList))

        valueList = ParseResponseData_Uniswap_GetManyPrices(dataList, decimals)
        # PrintAndLog("API_GetManyTokenToEthOutputPrices: valueList = " + str(valueList))

        # Convert valueList to priceList
        priceList = []
        for index, orderPropertySet in enumerate(valueList):
            priceList.append(amountList_etherUnits[index] / valueList[index])

        PrintAndLog("API_GetManyTokenToEthOutputPrices: priceList = " + str(priceList))
        return priceList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyTokenToEthOutputPrices response was not ok response = " + str(response))
        response.raise_for_status()
