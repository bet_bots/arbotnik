from eth_utils import keccak, to_bytes

from Libraries.loggingConfig import PrintAndLog_FuncNameHeader
import Libraries.network
from Libraries.signingUtils import SignHash

SignatureType = 3

eip191_header = b"\x19\x01"

eip712_domain_separator_schema_hash = keccak(
    b"EIP712Domain("
    + b"string name,"
    + b"string version,"
    + b"uint256 chainId,"
    + b"address verifyingContract"
    + b")"
)

eip712_order_schema_hash = keccak(
    b"RfqOrder("
    + b"address makerToken,"
    + b"address takerToken,"
    + b"uint128 makerAmount,"
    + b"uint128 takerAmount,"
    + b"address maker,"
    + b"address taker,"
    + b"address txOrigin,"
    + b"bytes32 pool,"
    + b"uint64 expiry,"
    + b"uint256 salt"
    + b")"
)


def pad_20_bytes_to_32(twenty_bytes: bytes):
    return bytes(12) + twenty_bytes


def int_to_32_big_endian_bytes(i: int):
    return i.to_bytes(32, byteorder="big")


def make_eip712_domain_struct_header_hash(chain_id: int, verifying_contract: str):
    return keccak(
        eip712_domain_separator_schema_hash
        + keccak(b"ZeroEx")
        + keccak(b"1.0.0")
        + int_to_32_big_endian_bytes(int(chain_id))
        + pad_20_bytes_to_32(to_bytes(hexstr=verifying_contract))
    )


def SignOrder_0xv4(order, privateKey):
    SignOrder_GivenHash_0xv4(order, privateKey, GenerateOrderHash_0xv4(order))


def GenerateOrderHash_0xv4(order):
    # This codebase is hard wired to use only this SignatureType
    order.signatureType = SignatureType

    orderData_jData = {
        "makerToken": order.makerTokenAddress,
        "takerToken": order.takerTokenAddress,
        "makerAmount": order.makerTokenAmount,
        "takerAmount": order.takerTokenAmount,
        "maker": order.maker,
        "taker": order.taker,
        "txOrigin": order.txOrigin,
        "pool": order.pool,
        "expiry": order.expirationUnixTimestampSec,
        "salt": order.salt,
        "chainId": 1,  # TODO, do i need this??
        "verifyingContract": order.exchangeContractAddress,
        "signature": {
            "signatureType": order.signatureType,
            "v": order.v,
            "r": order.r,
            "s": order.s,
        }
    }
    # PrintAndLog_FuncNameHeader("orderData_jData = " + str(orderData_jData))
    return GenerateOrderHash_GivenOrderDict_0xv4(orderData_jData)


def GenerateOrderHash_GivenOrderDict_0xv4(orderData_jData):
    PrintAndLog_FuncNameHeader("orderData_jData = " + str(orderData_jData))

    chainId = Libraries.network.GetChainId()

    thingy = eip712_order_schema_hash + \
             pad_20_bytes_to_32(to_bytes(hexstr=orderData_jData['makerToken'])) + \
             pad_20_bytes_to_32(to_bytes(hexstr=orderData_jData['takerToken'])) + \
             int_to_32_big_endian_bytes(int(orderData_jData['makerAmount'])) + \
             int_to_32_big_endian_bytes(int(orderData_jData['takerAmount'])) + \
             pad_20_bytes_to_32(to_bytes(hexstr=orderData_jData['maker'])) + \
             pad_20_bytes_to_32(to_bytes(hexstr=orderData_jData['taker'])) + \
             pad_20_bytes_to_32(to_bytes(hexstr=orderData_jData['txOrigin'])) + \
             int_to_32_big_endian_bytes(int(orderData_jData['pool'], 16)) + \
             int_to_32_big_endian_bytes(int(orderData_jData['expiry'])) + \
             int_to_32_big_endian_bytes(int(orderData_jData['salt']))

    # PrintAndLog_FuncNameHeader("thingy = " + str(thingy))

    eip712_order_struct_hash = keccak(thingy)
    # PrintAndLog_FuncNameHeader("eip712_order_struct_hash = " + str(eip712_order_struct_hash))

    eip712_domain_struct_hash = make_eip712_domain_struct_header_hash(
        chain_id=chainId,
        verifying_contract=orderData_jData['verifyingContract']
    )

    return keccak(
        eip191_header
        + eip712_domain_struct_hash
        + eip712_order_struct_hash
    ).hex()


def SignOrder_GivenHash_0xv4(order, privateKey, orderHash):
    orderHash_bytesArray = bytes.fromhex(orderHash)
    v, r, s = SignHash(orderHash_bytesArray, privateKey)

    order.hash = '0x' + orderHash
    order.v = v
    order.r = r
    order.s = s
    PrintAndLog_FuncNameHeader("order.hash = " + str(order.hash))
    PrintAndLog_FuncNameHeader("order.v = " + str(order.v))
    PrintAndLog_FuncNameHeader("order.r = " + str(order.r))
    PrintAndLog_FuncNameHeader("order.s = " + str(order.s))
