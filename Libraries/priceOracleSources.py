import Exchanges.uniswap
import Exchanges.uniswapV2
import Libraries.core
from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader

SourceTokenQuantity_toUse_etherUnits = 0.1


class PriceSource:
    name = None

    def __init__(self, _name):
        self.name = _name

    def GetOnChainPrice(self, baseToken):
        raise Exception("Not yet implemented")


class PriceSource_UniswapV2(PriceSource):

    def GetOnChainPrice(self, baseToken):
        from Exchanges.zrxV2 import Contract_WETH
        from Libraries.utils import RemoveOutliersFromList, AverageListContents

        quoteToken = Contract_WETH
        # If the tokens are the same
        if quoteToken.lower() == baseToken.lower():
            # the price is 1
            return 1

        latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

        priceList = []
        # Get prices for a range of block numbers
        for i in range(0, 20, 5):
            blockNumber = latestBlockNumber - i
            exchange = Exchanges.uniswapV2.GetExchangeGivenTokenAddresses(quoteToken, baseToken)

            PrintAndLog_FuncNameHeader("Calling API_GetManyExchangeTokenBalances with blockNumber = " + str(
                blockNumber) + ", quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken) + ", exchange = " + str(exchange))

            exchangeBalanceList_quoteTokens, exchangeBalanceList_baseTokens = \
                Exchanges.uniswapV2.API_GetManyExchangeTokenBalances(
                    [exchange], [quoteToken], [Libraries.core.GetDecimalsForTokenContract(quoteToken, True)],
                    [baseToken], [Libraries.core.GetDecimalsForTokenContract(baseToken, True)], blockNumber)

            exchangeBalance_quoteTokens = exchangeBalanceList_quoteTokens[0]
            exchangeBalance_baseTokens = exchangeBalanceList_baseTokens[0]

            price_buyingTokens = Exchanges.uniswap.CalculatePriceGivenBalances('buy', SourceTokenQuantity_toUse_etherUnits, exchangeBalance_quoteTokens, exchangeBalance_baseTokens)
            tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(SourceTokenQuantity_toUse_etherUnits, price_buyingTokens)
            price_sellingTokens = Exchanges.uniswap.CalculatePriceGivenBalances('sell', tokenQuantity_etherUnits, exchangeBalance_quoteTokens, exchangeBalance_baseTokens)
            # PrintAndLog("price_buyingTokens = " + str(price_buyingTokens) + " for baseToken = " + str(baseToken))
            # PrintAndLog("price_sellingTokens = " + str(price_sellingTokens) + " for baseToken = " + str(baseToken))

            # Let's assume the user wants the "spot" price and take the average of the buy and sell price
            priceList.append((price_buyingTokens + price_sellingTokens) / float(2))

        # Remove outliers
        # PrintAndLog_FuncNameHeader("priceList = " + str(priceList))
        priceList_withOutliersRemoved = RemoveOutliersFromList(priceList)
        # PrintAndLog_FuncNameHeader("priceList_withOutliersRemoved = " + str(priceList_withOutliersRemoved))
        # Get the average value from the list
        return AverageListContents(priceList_withOutliersRemoved)
