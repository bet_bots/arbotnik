import time

from Libraries.loggingConfig import PrintAndLog, PrintAndLog_Orderbooks_CX, PrintAndLog_FuncNameHeader

import Exchanges.zrxV2
import Libraries.core
import Libraries.nodes
import Libraries.defaults


class Order:
    # I'm using bisort, so I must implement this method so it can sort the object
    def __lt__(self, otherOrder):
        if self.IsBuyOrder():
            return self.GetPrice() > otherOrder.GetPrice()
        elif self.IsSellOrder():
            return self.GetPrice() < otherOrder.GetPrice()
        else:
            return None

    def IsBuyOrder(self):
        if "_buy" in self.id:
            return True
        else:
            return False

    def IsSellOrder(self):
        if "_sell" in self.id:
            return True
        else:
            return False

    def GetSide(self):
        if self.IsSellOrder():
            return 'sell'
        elif self.IsBuyOrder():
            return 'buy'
        else:
            raise Exception("Not buy or sell?")

    def GetOrderType(self):
        if "_sell" in self.id:
            return "sell"
        elif "_buy" in self.id:
            return "buy"
        else:
            raise Exception("OrderType was not buy or sell! Found a bug...")

    def PrintOrderDebugMessage(self, market, myPublicAddress):
        # If this order's user matches my myPublicAddress, then let's print it so I can see where my order is
        myOrderString = ""
        userString = ""
        blocksLeftUntilExpireString = "-"
        message = "{0:24}{1:4}{2:11}{3:6}{4:11}{5:17}{6:44}{7:13}".format(str(self.GetPrice())[:17], "ETH", str(
            round(float(self.GetEtherQuantity()), 6)), market.GetSecondaryTokenName().upper(), str(
            round(float(self.GetTokenQuantity()), 3)), blocksLeftUntilExpireString, userString, myOrderString)

        # For CXs, use PrintAndLog_Orderbooks_CX
        if issubclass(type(self), Order_CX):
            PrintAndLog_Orderbooks_CX(message)
            # PrintAndLog(message)
        else:
            PrintAndLog(message)

    def GetTokenQuantity(self):
        raise Exception("Not yet implemented!")

    def GetEtherQuantity(self):
        raise Exception("Not yet implemented!")

    def GetPrice(self):
        raise Exception("Not yet implemented!")

    def GetExpireTime(self):
        raise Exception("Not yet implemented!")

    def GetExpireTime_blocks(self):
        raise Exception("Not yet implemented!")

    def GetStartTime_blocks(self):
        raise Exception("Not yet implemented!")

    def GetMaker(self):
        raise Exception("Not yet implemented!")


class Order_CX(Order):
    # id is made up to better match with EtherDelta
    id = None
    quantity = None
    price = None
    exchangeName = None

    def __init__(self, _id, _quantity, _price, _exchangeName):
        self.id = _id
        self.quantity = _quantity
        self.price = _price
        self.exchangeName = _exchangeName

    def PrintOrderDetails(self, market=None):
        secondaryTokenName = "ERC20 token"
        if market:
            secondaryTokenName = market.GetSecondaryTokenName().upper()

        printString = self.exchangeName + " " + self.GetOrderType() + " order at " + str(self.GetPrice()) + " for " + str(
            self.GetTokenQuantity()) + " " + secondaryTokenName + ", and " + str(self.GetEtherQuantity()) + " ETH"
        return printString

    def GetTokenQuantity(self):
        return float(self.quantity)

    def GetEtherQuantity(self):
        # return float(self.quantity) / float(self.price)
        return float(self.quantity) * float(self.price)

    def GetPrice(self):
        if self.price:
            return float(self.price)
        else:
            return None

    def GetAmountFilledString(self):
        return 0


class Order_DX(Order):
    def PrintOrderDetails(self, market=None):
        printString = ""

        secondaryTokenName = "ERC20 token"
        myOrderString = ""
        if market:
            secondaryTokenName = market.GetSecondaryTokenName().upper()
            if self.GetMaker() and market and market.account and market.account.publicAddress and \
                    self.GetMaker().lower() == market.account.publicAddress.lower():
                myOrderString = "~~~~MY ORDER!~~~~"

        if self.IsBuyOrder():
            printString += "Buy order at " + str(self.GetPrice()) + " for " + str(self.GetTokenQuantity()) + " " + secondaryTokenName + ", and " + str(
                self.GetEtherQuantity()) + " ETH. " + myOrderString
        elif self.IsSellOrder():
            printString += "Sell order at " + str(self.GetPrice()) + " for " + str(self.GetTokenQuantity()) + " " + secondaryTokenName + ", and " + str(
                self.GetEtherQuantity()) + " ETH. " + myOrderString

        return printString


class Order_EtherDelta(Order_DX):
    id = None
    amount = None
    price = None
    tokenGet = None
    amountGet = None
    tokenGive = None
    amountGive = None
    expires = None
    nonce = None
    v = None
    r = None
    s = None
    user = None
    updated = None
    availableVolume = None
    ethAvailableVolume = None
    availableVolumeBase = None
    ethAvailableVolumeBase = None
    amountFilled = None

    def __init__(self, _id, _amount, _price, _tokenGet, _amountGet, _tokenGive, _amountGive, _expires, _nonce,
                 _v, _r, _s, _user, _updated, _availableVolume, _ethAvailableVolume, _availableVolumeBase,
                 _ethAvailableVolumeBase, _amountFilled):
        self.id = _id
        self.amount = _amount
        self.price = _price
        self.tokenGet = _tokenGet
        self.amountGet = _amountGet
        self.tokenGive = _tokenGive
        self.amountGive = _amountGive
        self.expires = _expires
        self.nonce = _nonce
        self.v = _v
        self.r = _r
        self.s = _s
        self.user = _user
        self.updated = _updated
        self.availableVolume = _availableVolume
        self.ethAvailableVolume = _ethAvailableVolume
        self.availableVolumeBase = _availableVolumeBase
        self.ethAvailableVolumeBase = _ethAvailableVolumeBase
        self.amountFilled = _amountFilled

    def GetTokenQuantity(self):
        return float(self.ethAvailableVolume)

    def GetEtherQuantity(self):
        return float(self.ethAvailableVolumeBase)

    def GetPrice(self):
        return float(self.price)

    def GetExpireTime(self):
        return int(float(self.expires))

    # This works for EtherDelta because expires is already in units of blocks. But some other DXs have non-blocks units
    def GetExpireTime_blocks(self):
        return self.GetExpireTime()

    def GetMaker(self):
        return self.user

    def GetEtherBaseAmount(self):
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.amountGive
        elif "sell" in self.id:
            return self.amountGet

    def GetTokenBaseAmount(self):
        # PrintAndLog("GetTokenBaseAmount with amountGet = " + str(self.amountGet) + " self.amountGive = " + str(self.amountGive))
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.amountGet
        elif "sell" in self.id:
            return self.amountGive

    def GetTokenAddress(self):
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.tokenGet
        elif "sell" in self.id:
            return self.tokenGive

    def GetTakerTokenAddress(self):
        return self.tokenGet

    def GetMakerTokenAddress(self):
        return self.tokenGive

    def GetAmountFilledString(self):
        amountFilled_toReturn = 0
        if self.amountFilled:
            amountFilled_toReturn = str(self.amountFilled)

        return amountFilled_toReturn

    def PrintOrderDebugMessage(self, market, myPublicAddress):
        # If this order's user matches my myPublicAddress, then let's print it so I can see where my order is
        myOrderString = ""

        if myPublicAddress and self.user.lower() == myPublicAddress.lower():
            myOrderString = "~~~~~~~~~ MY ORDER ~~~~~~~~~"

        blocksLeftUntilExpireString = "-"
        userString = self.user.lower()
        if Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int():
            # blocksLeftUntilExpireString = str(int(self.expires) - Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())         # Gave me an ValueError: invalid literal for int() with base 10: '1.000000000000515E+19'
            blocksLeftUntilExpireString = str(int(float(self.expires)) - Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())
        else:
            PrintAndLog("Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() was None so I cannot determine blocksLeftUntilExpireString")

        message = "{0:24}{1:4}{2:11}{3:6}{4:11}{5:17}{6:44}{7:13}".format(str(self.GetPrice())[:17], "ETH", str(
            round(float(self.GetEtherQuantity()), 6)), market.GetSecondaryTokenName().upper(), str(
            round(float(self.GetTokenQuantity()), 3)), blocksLeftUntilExpireString, userString, myOrderString)
        PrintAndLog(message)


# The MyOrder_EtherDelta class differs from Order_EtherDelta in that it is an order I submitted and is cancelable
class MyOrder_EtherDelta(Order_EtherDelta):
    amountFilled = None
    cancelOrderTxId = None
    dateTimeCanceled_unixEpochTime_int = None
    # Units are in blocks
    duration = None

    def __init__(self, _id, _tokenGet, _amountGet, _tokenGive, _amountGive,
                 _user, _expires, _duration, _nonce, _v, _r, _s):
        self.id = _id
        self.tokenGet = _tokenGet
        self.amountGet = _amountGet
        self.tokenGive = _tokenGive
        self.amountGive = _amountGive
        self.user = _user
        self.expires = _expires
        self.duration = _duration
        self.nonce = _nonce
        self.v = _v
        self.r = _r
        self.s = _s

        self.amountFilled = 0
        self.cancelOrderTxId = None
        self.dateTimeCanceled_unixEpochTime_int = None

    def GetTokenQuantity(self):
        # PrintAndLog("GetTokenQuantity where self.GetTokenBaseAmount() = " + str(self.GetTokenBaseAmount()) + " self.GetTokenAddress() = " + str(self.GetTokenAddress()) + " and Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress()) = " + str(Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress())))
        decimals_token = Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress())
        return Libraries.core.ConvertBaseAmountToEtherAmount(self.GetTokenBaseAmount(), float("1e" + str(decimals_token)))

    def GetEtherQuantity(self):
        return Libraries.core.ConvertBaseAmountToEtherAmount(self.GetEtherBaseAmount(), Libraries.core.Ether_Decimals)

    def GetPrice(self):
        return float(self.GetEtherQuantity() / self.GetTokenQuantity())

    def GetStartTime(self):
        return int(self.GetExpireTime() - int(self.duration))

    # This works for EtherDelta because expires and duration are already in units of blocks. But some other DXs have non-blocks units
    def GetStartTime_blocks(self):
        return self.GetStartTime()

    def HasBeenCanceled(self):
        if self.cancelOrderTxId:
            return True
        else:
            return False

    def SetOrderAsCanceled(self, transactionId):
        self.cancelOrderTxId = transactionId
        self.dateTimeCanceled_unixEpochTime_int = int(time.time())

    def GetSecondsSinceCanceled(self):
        # Return current unix epoch time minus the unix epoch time it was when it was canceled
        return int(time.time()) - int(self.dateTimeCanceled_unixEpochTime_int)

    def SetAmountFilled(self, newAmountFilled):
        self.amountFilled = newAmountFilled

    def GetOrderFillID(self):
        # This ID is used for uniquely identifying order fills.
        # The order hash is unique for the order, and the amountFilled at that very moment is unique per fill in the event there are many partial fills
        return self.id + "_" + str(self.amountFilled)


class Order_0x(Order_DX):
    id = None
    exchangeContractAddress = None
    maker = None
    taker = None
    makerTokenAddress = None
    takerTokenAddress = None
    feeRecipient = None
    makerTokenAmount = None
    takerTokenAmount = None
    makerFee = None
    takerFee = None
    expirationUnixTimestampSec = None
    salt = None
    v = None
    r = None
    s = None

    # Optimization: Cache the values that I call frequently so they are faster to return the next time around
    cache_GetTokenQuantity = None
    cache_GetEtherQuantity = None

    def __init__(self, _id, _exchangeContractAddress, _maker, _taker, _makerTokenAddress,
                 _takerTokenAddress, _feeRecipient, _makerTokenAmount, _takerTokenAmount,
                 _makerFee, _takerFee, _expirationUnixTimestampSec, _salt, _v, _r, _s):
        self.id = _id
        self.exchangeContractAddress = _exchangeContractAddress
        self.maker = _maker
        self.taker = _taker
        self.makerTokenAddress = _makerTokenAddress
        self.takerTokenAddress = _takerTokenAddress
        self.feeRecipient = _feeRecipient
        self.makerTokenAmount = _makerTokenAmount
        self.takerTokenAmount = _takerTokenAmount
        self.makerFee = _makerFee
        self.takerFee = _takerFee
        self.expirationUnixTimestampSec = _expirationUnixTimestampSec
        self.salt = _salt
        self.v = _v
        self.r = _r
        self.s = _s

        self.cache_GetTokenQuantity = None
        self.cache_GetEtherQuantity = None

    def GetStartTime(self):
        raise Exception("Not yet implemented")

    def GetTokenQuantity(self):
        # PrintAndLog("GetTokenQuantity where self.GetTokenBaseAmount() = " + str(self.GetTokenBaseAmount()) + " self.GetTokenAddress() = " + str(
        #     self.GetTokenAddress()) + " and Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress()) = " + str(
        #     Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress())))
        # Update the cache if not already set
        if not self.cache_GetTokenQuantity:
            decimals = Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress(), True)
            self.cache_GetTokenQuantity = Libraries.core.ConvertBaseAmountToEtherAmount(self.GetTokenBaseAmount(), decimals)

        return self.cache_GetTokenQuantity

    def GetEtherQuantity(self):
        # PrintAndLog("GetEtherQuantity: self.GetEtherBaseAmount() = " + str(self.GetEtherBaseAmount()))
        # Update the cache if not already set
        if not self.cache_GetEtherQuantity:
            decimals = Libraries.core.GetDecimalsForTokenContract(self.GetEtherAddress(), True)
            self.cache_GetEtherQuantity = Libraries.core.ConvertBaseAmountToEtherAmount(self.GetEtherBaseAmount(), decimals)

        return self.cache_GetEtherQuantity

    def GetPrice(self):
        # PrintAndLog("GetPrice: self.GetEtherQuantity() = " + str(self.GetEtherQuantity()))
        # PrintAndLog("GetPrice: self.GetTokenQuantity() = " + str(self.GetTokenQuantity()))
        return float(self.GetEtherQuantity() / self.GetTokenQuantity())

    def GetExpireTime(self):
        return int(self.expirationUnixTimestampSec)

    # For this to work for 0x, I need to convert seconds to blocks.
    # To do this correctly i'd need to make a call to a node and ask them for the seconds time of a block number, but I'd need to make a lot of API calls very frequently...
    # So I'm going to estimate this conversion of time to blocks in order to prevent myself from making dozens of API calls per minute.
    def GetExpireTime_blocks(self):
        return Libraries.core.ConvertUnixEpochTimeToApproximateBlockNumber_GivenEpochTimeAndValidUnixEpochTimeBlockNumberPair(
            self.GetExpireTime(),
            Libraries.core.GetThreadSafeCopyOf_LatestUnixEpochTime_int(),
            Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())

    def GetDurationRemaining_seconds(self):
        # PrintAndLog("GetDurationRemaining_seconds: self.GetExpireTime() = " + str(self.GetExpireTime()) + ", time.time() = " + str(time.time()))
        return self.GetExpireTime() - time.time()

    def GetDurationRemaining_blocks(self):
        return Libraries.core.ConvertUnixEpochTimeToApproximateBlockNumber_GivenEpochTimeAndValidUnixEpochTimeBlockNumberPair(
            self.GetDurationRemaining_seconds(),
            Libraries.core.GetThreadSafeCopyOf_LatestUnixEpochTime_int(),
            Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())

    # For this to work for 0x, I need to convert seconds to blocks.
    # To do this correctly i'd need to make a call to a node and ask them for the seconds time of a block number, but I'd need to make a lot of API calls very frequently...
    # So I'm going to estimate this conversion of time to blocks in order to prevent myself from making dozens of API calls per minute.
    def GetStartTime_blocks(self):
        return Libraries.core.ConvertUnixEpochTimeToApproximateBlockNumber_GivenEpochTimeAndValidUnixEpochTimeBlockNumberPair(
            self.GetStartTime(),
            Libraries.core.GetThreadSafeCopyOf_LatestUnixEpochTime_int(),
            Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())

    def GetMaker(self):
        return self.maker

    def GetEtherBaseAmount(self):
        # PrintAndLog("GetEtherBaseAmount with id = " + self.id + ", takerTokenAmount = " + str(self.takerTokenAmount) + " self.makerTokenAmount = " + str(self.makerTokenAmount))
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.makerTokenAmount
        elif "sell" in self.id:
            return self.takerTokenAmount

    def GetTokenBaseAmount(self):
        # PrintAndLog("GetTokenBaseAmount with id = " + self.id + ", takerTokenAmount = " + str(self.takerTokenAmount) + " self.makerTokenAmount = " + str(self.makerTokenAmount))
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.takerTokenAmount
        elif "sell" in self.id:
            return self.makerTokenAmount

    # TODO rename to GetQuoteToken
    def GetEtherAddress(self):
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.makerTokenAddress
        elif "sell" in self.id:
            return self.takerTokenAddress

    # TODO rename to GetBaseToken
    def GetTokenAddress(self):
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.takerTokenAddress
        elif "sell" in self.id:
            return self.makerTokenAddress

    def GetTakerTokenAddress(self):
        return self.takerTokenAddress

    def GetMakerTokenAddress(self):
        return self.makerTokenAddress

    def PrintOrderDebugMessage(self, market, myPublicAddress):
        # If this order's user matches my myPublicAddress, then let's print it so I can see where my order is
        myOrderString = ""

        # Only do this for order types Order_EtherDelta
        if myPublicAddress and self.maker.lower() == myPublicAddress.lower():
            myOrderString = "~~~~~~~~~ MY ORDER ~~~~~~~~~"

        blocksLeftUntilExpireString = "-"
        userString = self.maker.lower()
        # PrintAndLog("Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() = " + str(Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()))
        if Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int():
            # PrintAndLog("self.GetExpireTime_blocks() = " + str(self.GetExpireTime_blocks()))
            blocksLeftUntilExpireString = str(int(float(self.GetExpireTime_blocks())) - Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())
        else:
            PrintAndLog("Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() was None so I cannot determine blocksLeftUntilExpireString")

        message = "{0:24}{1:4}{2:11}{3:6}{4:11}{5:17}{6:44}{7:13}".format(str(self.GetPrice())[:17], "ETH", str(
            round(float(self.GetEtherQuantity()), 6)), market.GetSecondaryTokenName().upper(), str(
            round(float(self.GetTokenQuantity()), 3)), blocksLeftUntilExpireString, userString, myOrderString)
        PrintAndLog(message)


# The MyOrder_0x class differs from Order_0x in that it is an order I submitted and is cancelable
class MyOrder_0x(Order_0x):
    amountFilled = None
    cancelOrderTxId = None
    dateTimeCanceled_unixEpochTime_int = None
    hash = None
    # Units are in seconds
    duration = None

    def __init__(self, _id, _exchangeContractAddress, _maker, _taker, _makerTokenAddress,
                 _takerTokenAddress, _feeRecipient, _makerTokenAmount, _takerTokenAmount,
                 _makerFee, _takerFee, _expirationUnixTimestampSec, _duration, _salt, _v=None, _r=None, _s=None):
        self.id = _id
        self.exchangeContractAddress = _exchangeContractAddress
        self.maker = _maker
        self.taker = _taker
        self.makerTokenAddress = _makerTokenAddress
        self.takerTokenAddress = _takerTokenAddress
        self.feeRecipient = _feeRecipient
        self.makerTokenAmount = _makerTokenAmount
        self.takerTokenAmount = _takerTokenAmount
        self.makerFee = _makerFee
        self.takerFee = _takerFee
        self.expirationUnixTimestampSec = _expirationUnixTimestampSec
        self.salt = _salt
        self.v = _v
        self.r = _r
        self.s = _s

        self.amountFilled = 0
        self.cancelOrderTxId = None
        self.dateTimeCanceled_unixEpochTime_int = None
        self.hash = None
        self.duration = _duration

    def GetStartTime(self):
        return int(self.GetExpireTime() - int(self.duration))

    def HasBeenCanceled(self):
        if self.cancelOrderTxId:
            return True
        else:
            return False

    def SetOrderAsCanceled(self, transactionId):
        self.cancelOrderTxId = transactionId
        self.dateTimeCanceled_unixEpochTime_int = int(time.time())

    def GetSecondsSinceCanceled(self):
        # Return current unix epoch time minus the unix epoch time it was when it was canceled
        return int(time.time()) - int(self.dateTimeCanceled_unixEpochTime_int)

    def SetAmountFilled(self, newAmountFilled):
        self.amountFilled = newAmountFilled

    def GetOrderFillID(self):
        # This ID is used for uniquely identifying order fills.
        # The order hash is unique for the order, and the amountFilled at that very moment is unique per fill in the event there are many partial fills
        return self.hash + "_" + str(self.amountFilled)


class Order_0xv3(Order_0x):
    senderAddress = None
    hash = None
    signature = None
    makerFeeTokenAddress = None
    takerFeeTokenAddress = None

    def __init__(self, _id, _exchangeContractAddress, _maker, _taker, _makerTokenAddress,
                 _takerTokenAddress, _feeRecipient, _makerTokenAmount, _takerTokenAmount,
                 _makerFee, _takerFee, _makerFeeTokenAddress, _takerFeeTokenAddress,
                 _expirationUnixTimestampSec, _salt, _senderAddress, _hash, _signature):
        self.id = _id
        self.exchangeContractAddress = _exchangeContractAddress
        self.maker = _maker
        self.taker = _taker
        self.makerTokenAddress = _makerTokenAddress
        self.takerTokenAddress = _takerTokenAddress
        self.feeRecipient = _feeRecipient
        self.makerTokenAmount = _makerTokenAmount
        self.takerTokenAmount = _takerTokenAmount
        self.makerFee = _makerFee
        self.takerFee = _takerFee
        self.makerFeeTokenAddress = _makerFeeTokenAddress
        self.takerFeeTokenAddress = _takerFeeTokenAddress
        self.expirationUnixTimestampSec = _expirationUnixTimestampSec
        self.salt = _salt
        self.senderAddress = _senderAddress
        self.hash = _hash
        self.signature = _signature

    def GenerateOrderTupleForTransaction(self, doEncode=False):
        from Exchanges.zrxV2 import GenerateOrderTupleForTransaction, GenerateOrderTupleForTransaction_Encoded
        if doEncode:
            raise Exception("Deprecated, I should not be using this anymore")
            return GenerateOrderTupleForTransaction_Encoded(self)
        else:
            return GenerateOrderTupleForTransaction(self)


class MyOrder_0xv3(MyOrder_0x):
    senderAddress = None
    signature = None
    makerFeeTokenAddress = None
    takerFeeTokenAddress = None

    def __init__(self, _id, _exchangeContractAddress, _maker, _taker, _makerTokenAddress,
                 _takerTokenAddress, _feeRecipient, _makerTokenAmount, _takerTokenAmount,
                 _makerFee, _takerFee, _makerFeeTokenAddress, _takerFeeTokenAddress,
                 _expirationUnixTimestampSec, _duration, _salt, _senderAddress, _signature=None):
        self.id = _id
        self.exchangeContractAddress = _exchangeContractAddress
        self.maker = _maker
        self.taker = _taker
        self.makerTokenAddress = _makerTokenAddress
        self.takerTokenAddress = _takerTokenAddress
        self.feeRecipient = _feeRecipient
        self.makerTokenAmount = _makerTokenAmount
        self.takerTokenAmount = _takerTokenAmount
        self.makerFee = _makerFee
        self.takerFee = _takerFee
        self.makerFeeTokenAddress = _makerFeeTokenAddress
        self.takerFeeTokenAddress = _takerFeeTokenAddress
        self.expirationUnixTimestampSec = _expirationUnixTimestampSec
        self.duration = _duration
        self.salt = _salt
        self.senderAddress = _senderAddress
        self.signature = _signature

        self.amountFilled = 0
        self.cancelOrderTxId = None
        self.dateTimeCanceled_unixEpochTime_int = None
        self.hash = None

    def GenerateOrderTupleForTransaction(self, doEncode=False):
        from Exchanges.zrxV2 import GenerateOrderTupleForTransaction, GenerateOrderTupleForTransaction_Encoded
        if doEncode:
            raise Exception("Deprecated, I should not be using this anymore")
            return GenerateOrderTupleForTransaction_Encoded(self)
        else:
            return GenerateOrderTupleForTransaction(self)


class Order_0xv4(Order_0x):
    hash = None
    txOrigin = None
    pool = None
    signatureType = None
    v = None
    r = None
    s = None

    def __init__(self, _id, _exchangeContractAddress, _maker, _taker, _makerTokenAddress,
                 _takerTokenAddress, _makerTokenAmount, _takerTokenAmount,
                 _expirationUnixTimestampSec, _salt, _txOrigin, _pool,
                 _hash, _signatureType, _v, _r, _s):
        self.id = _id
        self.exchangeContractAddress = _exchangeContractAddress
        self.maker = _maker
        self.taker = _taker
        self.makerTokenAddress = _makerTokenAddress
        self.takerTokenAddress = _takerTokenAddress
        self.makerTokenAmount = _makerTokenAmount
        self.takerTokenAmount = _takerTokenAmount
        self.expirationUnixTimestampSec = _expirationUnixTimestampSec
        self.salt = _salt
        self.txOrigin = _txOrigin
        self.pool = _pool
        self.hash = _hash
        self.signatureType = _signatureType
        self.v = _v
        self.r = _r
        self.s = _s

    def GenerateOrderTupleForTransaction(self, doEncode=False):
        from Exchanges.zrxV4 import GenerateOrderTupleForTransaction
        if doEncode:
            raise Exception("Deprecated, I should not be using this anymore")
        else:
            return GenerateOrderTupleForTransaction(self)

    def GenerateSignatureTupleForTransaction(self, doEncode=False):
        from Exchanges.zrxV4 import GenerateSignatureTupleForTransaction
        if doEncode:
            raise Exception("Deprecated, I should not be using this anymore")
        else:
            return GenerateSignatureTupleForTransaction(self)


class MyOrder_0xv4(MyOrder_0x):
    hash = None
    txOrigin = None
    pool = None
    signatureType = None
    v = None
    r = None
    s = None

    def __init__(self, _id, _exchangeContractAddress, _maker, _taker, _makerTokenAddress,
                 _takerTokenAddress, _makerTokenAmount, _takerTokenAmount,
                 _expirationUnixTimestampSec, _duration, _salt,
                 _txOrigin, _pool, _signatureType=None, _v=None, _r=None, _s=None):
        self.id = _id
        self.exchangeContractAddress = _exchangeContractAddress
        self.maker = _maker
        self.taker = _taker
        self.makerTokenAddress = _makerTokenAddress
        self.takerTokenAddress = _takerTokenAddress
        self.makerTokenAmount = _makerTokenAmount
        self.takerTokenAmount = _takerTokenAmount
        self.expirationUnixTimestampSec = _expirationUnixTimestampSec
        self.duration = _duration
        self.salt = _salt
        self.txOrigin = _txOrigin
        self.pool = _pool
        self.signatureType = _signatureType
        self.v = _v
        self.r = _r
        self.s = _s

        self.amountFilled = 0
        self.cancelOrderTxId = None
        self.dateTimeCanceled_unixEpochTime_int = None
        self.hash = None

    def GenerateOrderTupleForTransaction(self, doEncode=False):
        from Exchanges.zrxV4 import GenerateOrderTupleForTransaction
        if doEncode:
            raise Exception("Deprecated, I should not be using this anymore")
        else:
            return GenerateOrderTupleForTransaction(self)

    def GenerateSignatureTupleForTransaction(self, doEncode=False):
        from Exchanges.zrxV4 import GenerateSignatureTupleForTransaction
        if doEncode:
            raise Exception("Deprecated, I should not be using this anymore")
        else:
            return GenerateSignatureTupleForTransaction(self)


class Order_Airswap(Order_DX):
    id = None
    makerAddress = None
    makerAmount = None
    makerToken = None
    takerAddress = None
    takerAmount = None
    takerToken = None
    expirationUnixTimestampSec = None
    nonce = None
    v = None
    r = None
    s = None

    def __init__(self, _id, _makerAddress, _makerAmount, _makerToken, _takerAddress,
                 _takerAmount, _takerToken, _expirationUnixTimestampSec, _nonce, _v, _r, _s):
        self.id = _id
        self.makerAddress = _makerAddress
        self.makerAmount = _makerAmount
        self.makerToken = _makerToken
        self.takerAddress = _takerAddress
        self.takerAmount = _takerAmount
        self.takerToken = _takerToken
        self.expirationUnixTimestampSec = _expirationUnixTimestampSec
        self.nonce = _nonce
        self.v = _v
        self.r = _r
        self.s = _s

    def GetTokenQuantity(self):
        # PrintAndLog("GetTokenQuantity where self.GetTokenBaseAmount() = " + str(self.GetTokenBaseAmount()) +
        #             " self.GetTokenAddress() = " + str(self.GetTokenAddress()) +
        #             " and Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress()) = " +
        #             str(Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress())))
        decimals_token = Libraries.core.GetDecimalsForTokenContract(self.GetTokenAddress())
        return Libraries.core.ConvertBaseAmountToEtherAmount(self.GetTokenBaseAmount(), float("1e" + str(decimals_token)))

    def GetEtherQuantity(self):
        # PrintAndLog("GetEtherQuantity: self.GetEtherBaseAmount() = " + str(self.GetEtherBaseAmount()))
        return Libraries.core.ConvertBaseAmountToEtherAmount(self.GetEtherBaseAmount(), Libraries.core.Ether_Decimals)

    def GetPrice(self):
        # PrintAndLog("GetPrice: self.GetEtherQuantity() = " + str(self.GetEtherQuantity()))
        # PrintAndLog("GetPrice: self.GetTokenQuantity() = " + str(self.GetTokenQuantity()))
        return float(self.GetEtherQuantity() / self.GetTokenQuantity())

    def GetExpireTime(self):
        return int(self.expirationUnixTimestampSec)

    def GetMaker(self):
        return self.makerAddress

    def GetEtherBaseAmount(self):
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.makerAmount
        elif "sell" in self.id:
            return self.takerAmount

    def GetTokenBaseAmount(self):
        # PrintAndLog("GetTokenBaseAmount with amountGet = " + str(self.amountGet) + " self.amountGive = " + str(self.amountGive))
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.takerAmount
        elif "sell" in self.id:
            return self.makerAmount

    def GetTokenAddress(self):
        # I have the check the id to see if we're buying or selling
        # I don't know which is which without knowing whether or not i'm buying or selling
        if "buy" in self.id:
            return self.takerToken
        elif "sell" in self.id:
            return self.makerToken


# The MyOrder_Airswap class differs from Order_Airswap in that it is an order I submitted and is cancelable
class MyOrder_Airswap(Order_Airswap):
    amountFilled = None
    cancelOrderTxId = None
    dateTimeCanceled_unixEpochTime_int = None
    # Units are in seconds
    duration = None
    hash = None

    def __init__(self, _id, _makerAddress, _makerAmount, _makerToken, _takerAddress,
                 _takerAmount, _takerToken, _expirationUnixTimestampSec, _nonce, _duration, _v=None, _r=None, _s=None):
        self.id = _id
        self.makerAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(_makerAddress)
        self.makerAmount = _makerAmount
        self.makerToken = Libraries.nodes.Instance_Web3.toChecksumAddress(_makerToken)
        self.takerAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(_takerAddress)
        self.takerAmount = _takerAmount
        self.takerToken = Libraries.nodes.Instance_Web3.toChecksumAddress(_takerToken)
        self.expirationUnixTimestampSec = _expirationUnixTimestampSec
        self.nonce = _nonce
        self.duration = _duration
        self.v = _v
        self.r = _r
        self.s = _s

        self.amountFilled = 0
        self.cancelOrderTxId = None
        self.dateTimeCanceled_unixEpochTime_int = None
        self.hash = None

    def GetStartTime(self):
        return int(self.GetExpireTime() - int(self.duration))

    # For this to work for Airswap, I need to convert seconds to blocks.
    # To do this correctly i'd need to make a call to a node and ask them for the seconds time of a block number, but I'd need to make a lot of API calls very frequently...
    # So I'm going to estimate this conversion of time to blocks in order to prevent myself from making dozens of API calls per minute.
    def GetExpireTime_blocks(self):
        return Libraries.core.ConvertUnixEpochTimeToApproximateBlockNumber_GivenEpochTimeAndValidUnixEpochTimeBlockNumberPair(
            self.GetExpireTime(),
            Libraries.core.GetThreadSafeCopyOf_LatestUnixEpochTime_int(),
            Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())

    # For this to work for Airswap, I need to convert seconds to blocks.
    # To do this correctly i'd need to make a call to a node and ask them for the seconds time of a block number, but I'd need to make a lot of API calls very frequently...
    # So I'm going to estimate this conversion of time to blocks in order to prevent myself from making dozens of API calls per minute.
    def GetStartTime_blocks(self):
        return Libraries.core.ConvertUnixEpochTimeToApproximateBlockNumber_GivenEpochTimeAndValidUnixEpochTimeBlockNumberPair(
            self.GetStartTime(),
            Libraries.core.GetThreadSafeCopyOf_LatestUnixEpochTime_int(),
            Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int())

    def GetPartialFillIdForTrade(self, trade):
        # Trade is a tuple, get the values I need from below
        trade_txHash = trade[0]
        trade_etherQuantity_ether = trade[7]
        return str(trade_etherQuantity_ether) + "_" + str(trade_txHash)
        # In the past I was combining the fill amount (in ether) with the txHash, but I was running into issues where the numbers weren't matching up
        # because it was comparing things like 0.102628089298 with 0.1026280892981211 which are the same but technically the strings don't match...
        # I've since removed the amount completely from the ID and we'll see how this does. I don't want ot deal with things like 1.0 vs 1.00000 either... So *&*! it.
        # return str(trade_txHash)

    def HasAnyFullOrPartialFill(self):
        if len(self.partialFillIdList) > 0:
            return True
        else:
            return False

    def HasBeenCanceled(self):
        if self.cancelOrderTxId:
            return True
        else:
            return False

    def SetOrderAsCanceled(self, transactionId):
        self.cancelOrderTxId = transactionId
        self.dateTimeCanceled_unixEpochTime_int = int(time.time())

    def GetSecondsSinceCanceled(self):
        # Return current unix epoch time minus the unix epoch time it was when it was canceled
        return int(time.time()) - int(self.dateTimeCanceled_unixEpochTime_int)

    def AddIdToPartialFillIdList(self, trade):
        self.partialFillIdList.append(self.GetPartialFillIdForTrade(trade))

    def DoesPartialFillIdListContainTrade(self, trade, doPrintDebug=False):
        if doPrintDebug:
            PrintAndLog("DoesPartialFillIdListContainTrade, with type(self) = " + str(type(self)) + ", trade = " + str(trade) + ", self.partialFillIdList = " + str(
                self.partialFillIdList))
        return self.GetPartialFillIdForTrade(trade) in self.partialFillIdList

    def GetSumOfAllPreviousPartialFills(self, doPrintDebug=False):
        sum = 0

        # partialFillIdList, will look like this: ['0.0006222_0xtransactionId12345']
        if doPrintDebug:
            PrintAndLog("Iterating through self.partialFillIdList = " + str(self.partialFillIdList))

        for partialFillId in self.partialFillIdList:
            if doPrintDebug:
                PrintAndLog("partialFillId = " + str(partialFillId))

            # Split by the delimiter so we can separate the amount and the date
            splitString = partialFillId.split("_")
            partialFillAmount = float(splitString[0])
            sum += partialFillAmount
            if doPrintDebug:
                PrintAndLog("adding in partialFillAmount = " + str(partialFillAmount))

        if doPrintDebug:
            PrintAndLog("returning sum = " + str(sum))

        return sum


# Ocean is a 0x relayer and uses the Order Matching technique as opposed to Open Orderbook.
class MyOrder_Ocean(MyOrder_0x):
    # If there are any differences, I can add them here.  But so far, I don't see any
    NoDifferences = None


class Order_DXonChainLiquidityPool(Order_DX):
    # This applies to DXs like Bancor, Kyber, Uniswap, etc
    id = None
    effectiveEther = None
    effectiveTokens = None

    def __init__(self, _id, _effectiveEther, _effectiveTokens):
        self.id = _id
        self.effectiveEther = _effectiveEther
        self.effectiveTokens = _effectiveTokens

    def PrintOrderDetails(self):
        return "Order_DXonChainLiquidityPool: id = " + str(self.id) + ", GetTokenQuantity = " + str(self.GetTokenQuantity()) + ", GetEtherQuantity = " + str(
            self.GetEtherQuantity()) + ", GetPrice = " + str(self.GetPrice()) + ", TODO: is this price correct?"

    def GetTokenQuantity(self):
        return self.effectiveTokens

    def GetEtherQuantity(self):
        return self.effectiveEther

    def GetPrice(self):
        return float(self.GetEtherQuantity() / self.GetTokenQuantity())


def GetOrderFromOrderJSON_RadarRelay(orderJSON, id):
    return Order_0xv3(id, orderJSON['signedOrder']['exchangeAddress'], orderJSON['signedOrder']['makerAddress'], orderJSON['signedOrder']['takerAddress'],
                      Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['signedOrder']['makerAssetData']),
                      Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['signedOrder']['takerAssetData']),
                      orderJSON['signedOrder']['feeRecipientAddress'],
                      orderJSON['signedOrder']['makerAssetAmount'], orderJSON['signedOrder']['takerAssetAmount'],
                      orderJSON['signedOrder']['makerFee'], orderJSON['signedOrder']['takerFee'],
                      Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['signedOrder']['makerFeeAssetData']),
                      Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['signedOrder']['takerFeeAssetData']),
                      orderJSON['signedOrder']['expirationTimeSeconds'], orderJSON['signedOrder']['salt'],
                      orderJSON['signedOrder']['senderAddress'], orderJSON['orderHash'], orderJSON['signedOrder']['signature'])


def GetOrderFromOrderJSON_0xMesh(orderJSON, id):
    return Order_0xv3(id, orderJSON['order']['exchangeAddress'], orderJSON['order']['makerAddress'], orderJSON['order']['takerAddress'],
                      Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['order']['makerAssetData']),
                      Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['order']['takerAssetData']),
                      orderJSON['order']['feeRecipientAddress'],
                      orderJSON['order']['makerAssetAmount'], orderJSON['order']['takerAssetAmount'],
                      orderJSON['order']['makerFee'], orderJSON['order']['takerFee'],
                      Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['order']['makerFeeAssetData']),
                      Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['order']['takerFeeAssetData']),
                      orderJSON['order']['expirationTimeSeconds'], orderJSON['order']['salt'],
                      orderJSON['order']['senderAddress'], orderJSON['metaData']['orderHash'], orderJSON['order']['signature'])


def GetOrderFromOrderJSON_HidingBook(orderJSON, id):
    return Order_0xv4(id, orderJSON['order']['verifyingContract'], orderJSON['order']['maker'], orderJSON['order']['taker'],
                      orderJSON['order']['makerToken'], orderJSON['order']['takerToken'],
                      orderJSON['order']['makerAmount'], orderJSON['order']['takerAmount'],
                      orderJSON['order']['expiry'], orderJSON['order']['salt'],
                      orderJSON['order']['txOrigin'], orderJSON['order']['pool'],
                      orderJSON['metaData']['orderHash'],
                      orderJSON['order']['signature']['signatureType'],
                      orderJSON['order']['signature']['v'],
                      orderJSON['order']['signature']['r'],
                      orderJSON['order']['signature']['s'])


def ConvertMakerBasedAmountToEtherAmount_0xor0xv2(amount_weiUnits, order):
    return ConvertMakerOrTakerBasedAmountToEtherAmount_0xor0xv2(Libraries.core.TradingRole.maker, amount_weiUnits, order)


def ConvertTakerBasedAmountToEtherAmount_0xor0xv2(amount_weiUnits, order):
    return ConvertMakerOrTakerBasedAmountToEtherAmount_0xor0xv2(Libraries.core.TradingRole.taker, amount_weiUnits, order)


def ConvertMakerOrTakerBasedAmountToEtherAmount_0xor0xv2(side, amount_weiUnits, order):
    # The amount we're trying to convert could be in either makerToken or takerToken.
    # analyze the order data and see which, then convert it as necessary and return.

    # PrintAndLog("ConvertTakerBasedAmountToEtherAmount_0xor0xv2: order.id = " + str(order.id))
    if (side == Libraries.core.TradingRole.taker and "sell" in order.id) \
            or (side == Libraries.core.TradingRole.maker and "buy" in order.id):
        # PrintAndLog("ConvertTakerBasedAmountToEtherAmount_0xor0xv2 just returning the amount because it's already in ether units: " + str(amount_weiUnits))
        return amount_weiUnits
    elif (side == Libraries.core.TradingRole.taker and "buy" in order.id) \
            or (side == Libraries.core.TradingRole.maker and "sell" in order.id):

        # TODO, is this price calculation correct??
        price = None
        if side == Libraries.core.TradingRole.taker and "buy" in order.id:
            price = float(order.makerTokenAmount) / float(order.takerTokenAmount)
        elif side == Libraries.core.TradingRole.maker and "sell" in order.id:
            price = float(order.takerTokenAmount) / float(order.makerTokenAmount)
        else:
            raise Exception("This should never happen!")

        # amount_weiUnits is tokens, not ether.  So let's convert it
        amount_weiUnits_convertedToEther = float(amount_weiUnits) * price
        # Multiply it by 99.9999% just to be safe, and round to an int
        amount_weiUnits_convertedToEther *= 0.999999
        amount_weiUnits_convertedToEther = int(amount_weiUnits_convertedToEther)
        # Ensure it does not go negative
        if amount_weiUnits_convertedToEther < 0:
            amount_weiUnits_convertedToEther = 0

        # PrintAndLog("ConvertTakerBasedAmountToEtherAmount_0xor0xv2 converting filled amount from tokens to ether before returning: amount_weiUnits = " + str(
        #     amount_weiUnits) + ", amount_weiUnits_convertedToEther = " + str(amount_weiUnits_convertedToEther))
        return amount_weiUnits_convertedToEther


def GetOrdersFillableAmountRemaining_0x(order, exchange, doPrintDebug=False):
    import Libraries.orderAggregator
    # Use the orderAggregator to determine how much of this order is still fillable.

    decimals_quoteToken = Libraries.core.GetDecimalsForTokenContract(order.GetEtherAddress(), True)
    decimals_makerToken = Libraries.core.GetDecimalsForTokenContract(order.makerTokenAddress, True)

    # Get the order's amountFilled
    amountFilled_takerToken_weiUnits = Libraries.orderAggregator.AmountFilledValueDict_0x.GetCopyOfValue(order.hash)
    # The value is sometimes in tokens and not ether, so let's make sure it's in ether
    amountFilled_quoteTokens_weiUnits = ConvertTakerBasedAmountToEtherAmount_0xor0xv2(amountFilled_takerToken_weiUnits, order)
    amountFilled_quoteTokens_etherUnits = Libraries.core.ConvertWeiToEther(amountFilled_quoteTokens_weiUnits, decimals_quoteToken)

    keyToUse = Libraries.orderAggregator.GetKey_GivenUserAndToken(order.maker, order.makerTokenAddress)

    # Get the maker's makerToken balance
    makersTokenBalance_etherUnits = Libraries.orderAggregator.BalanceValueDict_0x.GetCopyOfValue(keyToUse)
    # PrintAndLog("makersTokenBalance_etherUnits = " + str(makersTokenBalance_etherUnits))
    makersTokenBalance_weiUnits = Libraries.core.ConvertEtherToWei(makersTokenBalance_etherUnits, decimals_makerToken)
    # The value is sometimes in tokens and not ether, so let's make sure it's in ether
    makersTokenBalance_quoteTokens_weiUnits = ConvertMakerBasedAmountToEtherAmount_0xor0xv2(makersTokenBalance_weiUnits, order)
    makersTokenBalance_quoteTokens_etherUnits = Libraries.core.ConvertWeiToEther(makersTokenBalance_quoteTokens_weiUnits, decimals_quoteToken)

    # Determine which AllowanceValueDict to use from Libraries.orderAggregator
    allowanceValueDict = Libraries.orderAggregator.GetAllowanceValueDict_GivenExchange(exchange)
    # Get the maker's approved allowance
    makersTokenAllowance_etherUnits = allowanceValueDict.GetCopyOfValue(keyToUse)
    # PrintAndLog("makersTokenAllowance_etherUnits = " + str(makersTokenAllowance_etherUnits))
    makersTokenAllowance_weiUnits = Libraries.core.ConvertEtherToWei(makersTokenAllowance_etherUnits, decimals_makerToken)
    # The value is sometimes in tokens and not ether, so let's make sure it's in ether
    makersTokenAllowance_quoteTokens_weiUnits = ConvertMakerBasedAmountToEtherAmount_0xor0xv2(makersTokenAllowance_weiUnits, order)
    makersTokenAllowance_quoteTokens_etherUnits = Libraries.core.ConvertWeiToEther(makersTokenAllowance_quoteTokens_weiUnits, decimals_quoteToken)

    # Calculate the fillable amount once we factor in all of the above
    fillableAmountRemaining_quoteTokens_etherUnits = order.GetEtherQuantity() - amountFilled_quoteTokens_etherUnits
    # Factor in the maker's balance of makerToken
    fillableAmountRemaining_quoteTokens_etherUnits = min(fillableAmountRemaining_quoteTokens_etherUnits, makersTokenBalance_quoteTokens_etherUnits)
    # Factor in the maker's allowance of makerToken
    fillableAmountRemaining_quoteTokens_etherUnits = min(fillableAmountRemaining_quoteTokens_etherUnits, makersTokenAllowance_quoteTokens_etherUnits)
    # Ensure we don't somehow overfill
    fillableAmountRemaining_quoteTokens_etherUnits *= 0.9999

    if doPrintDebug or Libraries.defaults.VerboseLogging_GetOrdersFillableAmountRemaining_0x:
        PrintAndLog_FuncNameHeader("exchange.exchangeName = " + str(exchange.exchangeName))
        PrintAndLog_FuncNameHeader("order.hash = " + str(order.hash) + ", order.maker = " + str(order.maker))
        PrintAndLog_FuncNameHeader("order.GetEtherQuantity() = " + str(order.GetEtherQuantity()))
        PrintAndLog_FuncNameHeader("amountFilled_quoteTokens_etherUnits = " + str(amountFilled_quoteTokens_etherUnits))
        PrintAndLog_FuncNameHeader("makersTokenBalance_quoteTokens_etherUnits = " + str(makersTokenBalance_quoteTokens_etherUnits))
        PrintAndLog_FuncNameHeader("makersTokenAllowance_quoteTokens_etherUnits = " + str(makersTokenAllowance_quoteTokens_etherUnits))
        PrintAndLog_FuncNameHeader("fillableAmountRemaining_quoteTokens_etherUnits = " + str(fillableAmountRemaining_quoteTokens_etherUnits))

    return fillableAmountRemaining_quoteTokens_etherUnits, amountFilled_quoteTokens_etherUnits, \
           makersTokenBalance_quoteTokens_etherUnits, makersTokenAllowance_quoteTokens_etherUnits
