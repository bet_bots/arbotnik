import datetime

from Libraries.core import API_PostOperatorNotification
from Libraries.loggingConfig import PrintAndLog

ExceptionCountSinceLastDateTime = 0
DateTimeOfLastExceptionPostMessage = None

CountSinceLastFailedToUpdateWalletBalances = 0
DateTimeOfLastFailedToUpdateWalletBalances = None


def ConsiderPostingMessageSayingWeRequireBalancing(market, message):
    PrintAndLog(message)
    if not market.dateTime_OfLastRequireBalancingPostMessage or ((datetime.datetime.now() - market.dateTime_OfLastRequireBalancingPostMessage).total_seconds() > 3600):
        market.dateTime_OfLastRequireBalancingPostMessage = datetime.datetime.now()
        API_PostOperatorNotification(message)


def ConsiderPostingMessageSayingWeHadAnException(stackTraceString):
    global DateTimeOfLastExceptionPostMessage
    global ExceptionCountSinceLastDateTime

    # Increment the number of exceptions that have occurred
    # Also keep track of how many exceptions happened that didn't get messaged to my API_PostOperatorNotification api
    ExceptionCountSinceLastDateTime += 1

    if not DateTimeOfLastExceptionPostMessage or ((datetime.datetime.now() - DateTimeOfLastExceptionPostMessage).total_seconds() > 3600):
        DateTimeOfLastExceptionPostMessage = datetime.datetime.now()

        message = "Exception (" + str(ExceptionCountSinceLastDateTime) + " exceptions since last report): " + stackTraceString
        API_PostOperatorNotification(message)

        # Reset this to zero since we sent the message.
        ExceptionCountSinceLastDateTime = 0


def ConsiderPostingMessageSayingFailedToUpdateWalletBalances(stackTraceString):
    global DateTimeOfLastFailedToUpdateWalletBalances
    global CountSinceLastFailedToUpdateWalletBalances

    # Increment the number of exceptions that have occurred
    # Also keep track of how many exceptions happened that didn't get messaged to my API_PostOperatorNotification api
    CountSinceLastFailedToUpdateWalletBalances += 1

    if not DateTimeOfLastFailedToUpdateWalletBalances or ((datetime.datetime.now() - DateTimeOfLastFailedToUpdateWalletBalances).total_seconds() > 400):
        DateTimeOfLastFailedToUpdateWalletBalances = datetime.datetime.now()

        message = "Failed to update wallet balances (" + str(CountSinceLastFailedToUpdateWalletBalances) + " times since last report): " + stackTraceString
        API_PostOperatorNotification(message)

        # Reset this to zero since we sent the message.
        CountSinceLastFailedToUpdateWalletBalances = 0


def ConsiderPostingMessageSaying_FailedSanityCheckOnPrice(market, message):
    PrintAndLog(message)
    if not market.dateTimeOfLast_PostMessage_FailedSanityCheckOnPrice or ((datetime.datetime.now() - market.dateTimeOfLast_PostMessage_FailedSanityCheckOnPrice).total_seconds() > 3600):
        market.dateTimeOfLast_PostMessage_FailedSanityCheckOnPrice = datetime.datetime.now()
        API_PostOperatorNotification(message)
