import traceback

import Libraries.core
import Libraries.cache
from Exchanges.ninja_tailgating import FunctionSelectorDict_UniswapV2_Router, MakeSureFunctionSelectorDictsAreCached
from Libraries.exceptions import OverheadTokenTransferGasCostNotAvailableForThisTokenException
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError
from Libraries.customDicts import DictWithValueList, DictWithZeroInitializedValue
from Libraries.utils import IsTokenAKnownEtherToken
import Libraries.defaults

TokenTransferGasCostDict = None


class TxHashGasUsedAssociation:
    txHash = None
    gasUsed = None

    def __init__(self):
        self.txHash = None
        self.gasUsed = None

    def SetTxHash(self, txHash):
        self.txHash = txHash

    def SetGasUsed(self, gasUsed):
        self.gasUsed = gasUsed


def API_GetTokenTransferCost(tokensList):
    from Contracts.contracts import Contract_UniswapV2_Router, Contract_UniswapV2_Router_Old
    from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings

    tokensList = ConvertListOfStringsToLowercaseListOfStrings(tokensList)
    # PrintAndLog_FuncNameHeader("tokensList = " + str(tokensList))

    endBlock = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    # This needs to be large enough to get at least 1 or 2 transactions for every token.
    # Hopefully the tokens I'm trading are popular and have transactions broadcasting every few minutes worst case
    # But we should get hours of history just to be certain
    # blockSpan = 300
    blockSpan = 1000

    startBlock = endBlock - blockSpan

    blockList = list(range(startBlock, endBlock + 1))

    # PrintAndLog_FuncNameHeader("startBlock = " + str(startBlock))
    # PrintAndLog_FuncNameHeader("endBlock = " + str(endBlock))
    # PrintAndLog_FuncNameHeader("blockList = " + str(blockList))

    # blockDataList = Libraries.core.API_GetBlock_Batched(blockList, True)
    blockDataList = Libraries.core.API_GetBlock_Batched_Safe(blockList, True)
    # PrintAndLog_FuncNameHeader("blockDataList = " + str(blockDataList))

    # Use a common protocol like UniswapV2 as the gas usage measuring standard
    # Make a list of functionSelectors we're looking to match
    functionSelectorToMatch_swapExactETHForTokens = '7ff36ab5'
    # functionSelectorToMatch_swapExactTokensForETH = '18cbafe5'
    functionSelectorsMatchList = [
        functionSelectorToMatch_swapExactETHForTokens,
        # functionSelectorToMatch_swapExactTokensForETH,
    ]
    # swapExactETHForTokens - 0x7ff36ab5 - token should be dataProperty[6] - data property length = 7
    # swapExactTokensForETH - 0x18cbafe5 - token should be dataProperty[6] - data property length = 8

    uniswapV2RoutersList = [
        Contract_UniswapV2_Router.address.lower(),
        Contract_UniswapV2_Router_Old.address.lower(),
    ]
    tokenAssociationDict = DictWithValueList()
    # Keep track of all txHashes so I can batch call them all at once more easily
    allTxHashList = []
    for blockData in blockDataList:
        txList = blockData['transactions']
        for tx in txList:
            try:
                # Ensure the to address matches one I care about
                if tx['to'].lower() in uniswapV2RoutersList:
                    # Ensure the functionSelector matches one I care about
                    functionSelector, functionInputData = Libraries.core.GetFunctionSelectorFromCallData(tx['input'])
                    if functionSelector.lower() in functionSelectorsMatchList:
                        splitArray_functionInputData = Libraries.core.SplitStringIntoChunks(functionInputData, Libraries.core.LengthOfDataProperty)
                        # Ensure the argument length is correct if it's longer than we want it may be a multi leg trade
                        # which we do not want to consider for this gas usage measurement
                        if len(splitArray_functionInputData) == 7:
                            tokenAddress = Libraries.core.GetAddressFromDataProperty(splitArray_functionInputData[6])
                            # PrintAndLog_FuncNameHeader("Uniswap trade function call found for tokenAddress " + str(tokenAddress))

                            # PrintAndLog_FuncNameHeader("This is a token I care about")
                            # gasLimit = tx['gas']  # This is the gas limit, not the gas used.  I need to make another API call on this txHash to get the gas used
                            txHash = tx['hash']
                            # Create an object that helps us associate the txHash with the gas used
                            txHashGasUsedAssociation = TxHashGasUsedAssociation()
                            txHashGasUsedAssociation.SetTxHash(txHash.lower())
                            # Associate the tx with the tokenAddress so that I can later get the gas used for this transaction
                            tokenAssociationDict.AddItemToDict(tokenAddress.lower(), txHashGasUsedAssociation)
                            if txHash.lower() not in allTxHashList:
                                allTxHashList.append(txHash.lower())

                        # else:
                        #     PrintAndLog_FuncNameHeader("Arguments not exactly what we're looking for")

                    # else:
                    #     PrintAndLog_FuncNameHeader("function selector does not match")

                # else:
                #     PrintAndLog_FuncNameHeader("to address does not match")

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception when parsing tx, " + traceback.format_exc())

    txReceiptList = Libraries.core.API_GetTransactionReceipt_Batched_Safe(allTxHashList)
    # PrintAndLog_FuncNameHeader("txReceiptList = " + str(txReceiptList))

    tokenTransferGasCostListDict = DictWithValueList()
    for txReceipt in txReceiptList:
        txHash = txReceipt['transactionHash'].lower()
        status = Libraries.core.ConvertHexToInt(txReceipt['status'])
        # PrintAndLog_FuncNameHeader("status = " + str(status))
        # Require that this tx mined in successfully
        if status != 1:
            continue

        gasUsed = Libraries.core.ConvertHexToInt(txReceipt['gasUsed'])

        if gasUsed < Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt * 0.7:
            PrintAndLogError("Something went wrong, gasUsed for a token transfer should never be less than GasUsedWhenExecutingEmptyFunctionWithNothingInIt. "
                             "gasUsed = " + str(gasUsed) + ", how is that possible? I'm ignoring this value because it's most likely a bug. txHash = " + str(txHash))
            continue

        # Update the txHashGasUsedAssociation object in tokenAssociationDict
        for tokenAddress in tokenAssociationDict.GetKeys():
            txHashGasUsedAssociationList = tokenAssociationDict.GetItems(tokenAddress)
            for txHashGasUsedAssociation in txHashGasUsedAssociationList:
                if txHashGasUsedAssociation.txHash.lower() == txHash.lower():
                    # Update the txHash gas used association with the gas used
                    txHashGasUsedAssociation.gasUsed = gasUsed
                    # PrintAndLog_FuncNameHeader("gasUsed = " + str(txHashGasUsedAssociation.gasUsed) + " for txHash = " + str(
                    #     txHashGasUsedAssociation.txHash) + " for token " + str(tokenAddress))
                    tokenTransferGasCostListDict.AddItemToDict(tokenAddress.lower(), gasUsed)

    # Iterate over the tokenTransferGasCostListDict and remove all outliers from the data
    for key in tokenTransferGasCostListDict.GetKeys():
        valueList = tokenTransferGasCostListDict.GetItems(key)
        # Remove outliers from list
        newList = Libraries.utils.RemoveOutliersFromList(valueList)
        # Update the list
        tokenTransferGasCostListDict.SetValueList(key, newList)
        PrintAndLog_FuncNameHeader("Removed outliers from list for key " + str(key))
        PrintAndLog_FuncNameHeader("   list before = " + str(valueList))
        PrintAndLog_FuncNameHeader("   list after = " + str(newList))

    # Create tokenTransferGasCostDict from tokenTransferGasCostListDict
    tokenTransferGasCostDict = DictWithZeroInitializedValue()
    for key in tokenTransferGasCostListDict.GetKeys():
        # Get max value from tokenTransferGasCostListDict
        valueList = tokenTransferGasCostListDict.GetItems(key)
        # maxValue = max(valueList)
        maxValue = min(valueList)
        PrintAndLog_FuncNameHeader("Setting the value based on the other dict's value list, for key = " + str(key))
        PrintAndLog_FuncNameHeader("   maxValue = " + str(maxValue))
        PrintAndLog_FuncNameHeader("   valueList = " + str(valueList))
        tokenTransferGasCostDict.SetValue(key, maxValue)

    # Find the min across all values in the dict
    minGasCost = 9999999999999999
    sumGasCost = 0
    for tokenAddress in tokenTransferGasCostListDict.GetKeys():
        gasUsed = tokenTransferGasCostDict.GetItems(tokenAddress)
        sumGasCost += gasUsed
        # Maintain minGasCost
        if gasUsed < minGasCost:
            minGasCost = gasUsed

    averageGasCost = sumGasCost / len(tokenTransferGasCostDict.GetKeys())

    PrintAndLog_FuncNameHeader("minGasCost = " + str(minGasCost))
    PrintAndLog_FuncNameHeader("averageGasCost = " + str(averageGasCost))
    distanceFromMinToAverage = int(averageGasCost - minGasCost)
    PrintAndLog_FuncNameHeader("distanceFromMinToAverage = " + str(distanceFromMinToAverage))

    tokenTransferGasCostDict_toStore = {
        'tokens': {},
        'distanceFromMinToAverage': distanceFromMinToAverage,
    }
    for tokenAddress in tokenTransferGasCostDict.GetKeys():
        gasUsed = tokenTransferGasCostDict.GetItems(tokenAddress)
        # PrintAndLog_FuncNameHeader("tokenAddress " + str(tokenAddress) + " has gasUsed = " + str(gasUsed))
        symbol = ''
        try:
            symbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception " + traceback.format_exc())

        tokenTransferGasCostDict_toStore['tokens'][tokenAddress] = {
            'minGasCost': gasUsed,
            'minGasCost_excludingOverHeadCostOfTx': gasUsed - Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt,
            'minGasCost_differenceFromMinOfAllTokens': gasUsed - minGasCost,
            'symbol': symbol,
        }

    PrintAndLog_FuncNameHeader("tokenTransferGasCostDict_toStore = " + str(tokenTransferGasCostDict_toStore))
    return tokenTransferGasCostDict_toStore


def GetAdditionalOverheadTokenTransferGasCost_DifferenceFromMinOfAllTokens(tokenAddress):
    # If it's Ether, assume there's no additional overhead gas cost
    if IsTokenAKnownEtherToken(tokenAddress):
        return 0
    # Else assume it's a token that may have some overhead gas cost when calling the transfer function
    else:
        # If this token's data is not available
        if tokenAddress not in TokenTransferGasCostDict['tokens']:
            if Libraries.defaults.VerboseLogging_GetExpectedGasUsage_AllTradeActions:
                PrintAndLog_FuncNameHeader("Returning the distanceFromMinToAverage value as a fallback since this tokenAddress " + str(
                    tokenAddress) + " was not found in TokenTransferGasCostDict")
            # just use the distanceFromMinToAverage value as a fallback
            return TokenTransferGasCostDict['distanceFromMinToAverage']
        else:
            # return the value corresponding to this tokenAddress
            return TokenTransferGasCostDict['tokens'][tokenAddress]['minGasCost_differenceFromMinOfAllTokens']
