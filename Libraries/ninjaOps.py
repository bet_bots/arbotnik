from Libraries.accounts import NinjaOpAccountDict
from Libraries.core import API_PostOperatorNotification
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader

NinjaOpConfigList = []


class NinjaOpConfig:
    ninjaOpList = None
    # When a ninja op's ether balance gets below this threshold... it needs refilled!
    minEtherBalanceThreshold = None
    maxEtherBalanceThreshold = None

    def __init__(self, _minEtherBalanceThreshold, _maxEtherBalanceThreshold, _ninjaOpList):
        self.minEtherBalanceThreshold = _minEtherBalanceThreshold
        self.maxEtherBalanceThreshold = _maxEtherBalanceThreshold
        self.ninjaOpList = _ninjaOpList


def CreateNewNinjaOpConfig(minEtherBalanceThreshold, maxEtherBalanceThreshold, ninjaOpList):
    ninjaOpConfig = NinjaOpConfig(minEtherBalanceThreshold, maxEtherBalanceThreshold, ninjaOpList)
    NinjaOpConfigList.append(ninjaOpConfig)
    return ninjaOpConfig


NinjaOpConfig_NinjaGeneric_Wild = CreateNewNinjaOpConfig(
    3.0, 5.0,
    [
        NinjaOpAccountDict["ninja-op-116"],
        NinjaOpAccountDict["ninja-op-117"],
        NinjaOpAccountDict["ninja-op-118"],
        NinjaOpAccountDict["ninja-op-119"],
        NinjaOpAccountDict["ninja-op-120"],
        NinjaOpAccountDict["ninja-op-121"],
        NinjaOpAccountDict["ninja-op-122"],
        NinjaOpAccountDict["ninja-op-123"],
    ])

# I'm defaulting NinjaOpConfig_NinjaGeneric here, but I may want to override it to something else in the config
NinjaOpConfig_NinjaGeneric = NinjaOpConfig_NinjaGeneric_Wild

NinjaOpConfig_NinjaGeneric_Hide = CreateNewNinjaOpConfig(
    3.0, 5.0,
    [
        NinjaOpAccountDict["ninja-op-102"],
        NinjaOpAccountDict["ninja-op-103"],
        NinjaOpAccountDict["ninja-op-104"],
        NinjaOpAccountDict["ninja-op-105"],
        NinjaOpAccountDict["ninja-op-106"],
        NinjaOpAccountDict["ninja-op-107"],
        NinjaOpAccountDict["ninja-op-108"],
    ])

NinjaOpConfig_Tailgating = CreateNewNinjaOpConfig(
    0.4, 1.0,
    [
        NinjaOpAccountDict["ninja-op-102"],
        NinjaOpAccountDict["ninja-op-103"],
        NinjaOpAccountDict["ninja-op-104"],
        NinjaOpAccountDict["ninja-op-105"],
        NinjaOpAccountDict["ninja-op-106"],
        NinjaOpAccountDict["ninja-op-107"],
        NinjaOpAccountDict["ninja-op-108"],
        # NinjaOpAccountDict["ninja-op-109"],
        # NinjaOpAccountDict["ninja-op-110"],
        # NinjaOpAccountDict["ninja-op-111"],
        # NinjaOpAccountDict["ninja-op-112"],
        # NinjaOpAccountDict["ninja-op-113"],
        # NinjaOpAccountDict["ninja-op-114"],
    ])


def RebalanceNinjaOpAccount():
    from Libraries.core import API_GetEtherBalance_Batched_Safe
    for ninjaOpConfig in NinjaOpConfigList:
        addressList = []
        for ninjaOpAccount in ninjaOpConfig.ninjaOpList:
            addressList.append(ninjaOpAccount.publicAddress)

        resultList = API_GetEtherBalance_Batched_Safe(addressList)

        for index, etherBalance in enumerate(resultList):
            ninjaOpAccount = ninjaOpConfig.ninjaOpList[index]
            PrintAndLog_FuncNameHeader("ninjaOpAccount " + str(ninjaOpAccount.marketName) + " " + str(
                ninjaOpAccount.publicAddress) + " has a balance of " + str(etherBalance) + " ETH")

            # If the balance is low
            if etherBalance < ninjaOpConfig.minEtherBalanceThreshold:
                etherAmountItNeeds = ninjaOpConfig.maxEtherBalanceThreshold - etherBalance
                message = str(ninjaOpAccount.marketName) + "'s ether balance is LOW! Has only " + str(
                    round(etherBalance, 2)) + " ETH left! Please send it " + str(round(etherAmountItNeeds, 2)) + " ETH"
                API_PostOperatorNotification(message)
