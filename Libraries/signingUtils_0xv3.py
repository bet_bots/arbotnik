from zero_ex.order_utils import generate_order_hash_hex

from Libraries.loggingConfig import PrintAndLogError, PrintAndLog_FuncNameHeader
import Libraries.network
import Libraries.core
from Libraries.signingUtils import GetSignatureStringFromVRS, SignHash


def SignOrder_0xv3(order, privateKey):
    SignOrder_GivenHash_0xv3(order, privateKey, GenerateOrderHash_0xv3(order))


def GenerateOrderHash_0xv3(order):
    from Exchanges.zrxV2 import EncodeERC20AssetData

    # Radar Relay is setting these as '0x' whenever the fee is not used.
    # Not sure what other relayers will do but let's go with this for now
    makerFeeAssetData = None
    takerFeeAssetData = None

    if order.makerFeeTokenAddress.lower() == '0x':
        makerFeeAssetData = '0x'
    else:
        makerFeeAssetData = EncodeERC20AssetData(order.makerFeeTokenAddress)

    if order.takerFeeTokenAddress.lower() == '0x':
        takerFeeAssetData = '0x'
    else:
        takerFeeAssetData = EncodeERC20AssetData(order.takerFeeTokenAddress)

    orderData_jData = {
        'makerAddress': order.maker,
        'takerAddress': order.taker,
        'feeRecipientAddress': order.feeRecipient,
        'senderAddress': order.senderAddress,
        'makerAssetAmount': order.makerTokenAmount,
        'takerAssetAmount': order.takerTokenAmount,
        'makerFee': order.makerFee,
        'takerFee': order.takerFee,
        'expirationTimeSeconds': order.expirationUnixTimestampSec,
        'salt': order.salt,
        'makerAssetData': EncodeERC20AssetData(order.makerTokenAddress),
        'takerAssetData': EncodeERC20AssetData(order.takerTokenAddress),
        'makerFeeAssetData': makerFeeAssetData,
        'takerFeeAssetData': takerFeeAssetData,
    }
    # PrintAndLog_FuncNameHeader("orderData_jData = " + str(orderData_jData))
    return GenerateOrderHash_GivenOrderDict_0xv3(orderData_jData, order.exchangeContractAddress)


def GenerateOrderHash_GivenOrderDict_0xv3(orderData_jData, exchangeContractAddress):
    chainId = Libraries.network.GetChainId()
    orderHash = generate_order_hash_hex(orderData_jData, exchangeContractAddress, chainId)
    PrintAndLog_FuncNameHeader("orderHash = " + str(orderHash))
    return orderHash


def SignOrder_GivenHash_0xv3(order, privateKey, orderHash):
    from Exchanges.zrxV2 import AppendSignatureTypeToSignatureString, SignatureType

    orderHash_bytesArray = bytes.fromhex(orderHash)
    v, r, s = SignHash(orderHash_bytesArray, privateKey)

    order.hash = '0x' + orderHash
    order.v = v
    order.r = r
    order.s = s
    # print("order.hash = ", order.hash)
    # print("order.v = ", order.v)
    # print("order.r = ", order.r)
    # print("order.s = ", order.s)

    pythonGeneratedSig = GetSignatureStringFromVRS(v, r, s)
    pythonGeneratedSig = AppendSignatureTypeToSignatureString(pythonGeneratedSig, SignatureType.EthSign)
    order.signature = pythonGeneratedSig
    PrintAndLog_FuncNameHeader("order.signature = " + str(order.signature))
