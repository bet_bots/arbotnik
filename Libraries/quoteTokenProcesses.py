import datetime
import json
import multiprocessing
import os
import queue
import time
import traceback
import jsonpickle

import Exchanges.ninja_arbitrage
import Libraries.processCommunication
import Libraries.ratesGraph
import Libraries.nodes
import Libraries.utils
import Libraries.orderAggregator

# Keyed by quoteToken, value is process
from Libraries.exchanges import ExchangeName_0xMesh, EnforceTokenAddressIsERC20Version, GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken, ExchangeName_HidingBook
from Libraries.executeOnInterval import IsTimeToExecute
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError, PrintAndLog_Header
from Libraries.market import GetTokenObjectGivenTokenAddress
import Libraries.defaults

ProcessesDict_QuoteTokens = {}

# Set a ProcessIdString based on the quoteToken that I can add to logging to make reading logs easier
# This will be a variable used by the process only
ProcessIdString = None

Seed = None
SeedLength = None

EventQueueDict_MessagesToQuoteTokenProcesses = {}

MultiprocessingManager = multiprocessing.Manager()


def SignalQuoteTokenProcess_GetAllPrices(resultId, quoteToken, blockNumber, detailed,
                                         quoteTokenExchangeNamesDict, exchangeNameExclusions, quoteTokenBaseTokensDict):
    global ProcessesDict_QuoteTokens
    global EventQueueDict_MessagesToQuoteTokenProcesses
    global MultiprocessingManager

    # ************* Begin memory profiling ************* #
    # # Add to leaky code within python_script_being_profiled.py
    # PrintAndLog_FuncNameHeader("JOEYZMEMPROFILE: Begin")
    # from pympler import muppy, summary
    # all_objects = muppy.get_objects()
    # sum1 = summary.summarize(all_objects)
    # # Prints out a summary of the large objects
    # PrintAndLog_FuncNameHeader("JOEYZMEMPROFILE: Printing a summary???")
    # summary.print_(sum1)
    # # # Get references to certain types of objects such as dataframe
    # # dictObjects = [ao for ao in all_objects if isinstance(ao, dict)]
    # # for d in dictObjects:
    # #     PrintAndLog_FuncNameHeader("JOEYZMEMPROFILE: d of len " + str(len(d)) + " = " + str(d))
    # ************* End memory profiling ************* #

    # The purpose of this function is to store the arguments for the GetAllPrices_ForQuoteToken call
    # in shared memory where the quoteToken process can use them to make the call to get price data on exchanges.
    # Using separate processes here is a performance optimization since making all these massive calls on one python process is inefficient

    # Start the process only if it hasn't been started yet
    if quoteToken.lower() not in ProcessesDict_QuoteTokens:
        PrintAndLog_FuncNameHeader("Creating process for quoteToken " + str(quoteToken))
        EventQueueDict_MessagesToQuoteTokenProcesses[quoteToken.lower()] = MultiprocessingManager.Queue()

        # Start the process for this quoteToken
        ProcessesDict_QuoteTokens[quoteToken.lower()] = multiprocessing.Process(
            target=ProcessLoop_QuoteTokens,
            args=(quoteToken.lower(), EventQueueDict_MessagesToQuoteTokenProcesses[quoteToken.lower()],))
        ProcessesDict_QuoteTokens[quoteToken.lower()].start()

    # Signal that the process should begin
    # Once this signal is set, the processes (that are already running) will begin. This process should wait for their responses
    PrintAndLog_FuncNameHeader("Sending signal_getPrices to sub process with resultId " + str(resultId))
    jData = {
        'method': 'signal_getPrices',
        'payload': {
            'blockNumber': blockNumber,
            'resultId': resultId,
            'detailed': detailed,
            'quoteTokenExchangeNamesDict': quoteTokenExchangeNamesDict,
            'exchangeNameExclusions': exchangeNameExclusions,
            'quoteTokenBaseTokensDict': quoteTokenBaseTokensDict,
        }
    }
    EventQueueDict_MessagesToQuoteTokenProcesses[quoteToken.lower()].put(json.dumps(jData))


def SignalAllQuoteTokenProcesses_Kill():
    global EventQueueDict_MessagesToQuoteTokenProcesses

    # Signal that the process should begin
    # Once this signal is set, the processes (that are already running) will begin. This process should wait for their responses
    PrintAndLog_FuncNameHeader("Sending kill to sub processes")
    jData = {
        'method': 'kill',
    }
    for processKey in EventQueueDict_MessagesToQuoteTokenProcesses:
        EventQueueDict_MessagesToQuoteTokenProcesses[processKey].put(json.dumps(jData))


def ProcessLoop_QuoteTokens(quoteToken, eventQueue_MessagesToQuoteTokenProcesses):
    import Exchanges.keeperDAO
    from Libraries.cache import GetSymbolForTokenContract_UseCachingSystem
    from Exchanges.zrxV2 import Contract_WETH
    from Libraries.core import API_PostOperatorNotification

    global ProcessIdString
    global Seed
    global SeedLength

    ProcessIdString = GetSymbolForTokenContract_UseCachingSystem(quoteToken)
    header_logging = "ProcessLoop_QuoteTokens: " + str(ProcessIdString)
    PrintAndLog_Header(header_logging, "Starting process for quoteToken " + str(quoteToken) + " os.getpid() = " + str(os.getpid()))

    initialized = False
    blockNumber = None

    # Generate a seed based on which index this quoteToken is within all the quoteTokens
    quoteTokenList = Exchanges.keeperDAO.GetBorrowableAssetsList()
    Seed = quoteTokenList.index(quoteToken.lower())
    SeedLength = len(quoteTokenList)
    # PrintAndLog_Header(header_logging, "Setting Seed for this process as " + str(Seed) + ", SeedLength = " + str(SeedLength))

    while True:
        try:
            # PrintAndLog_Header(header_logging, "Checking eventQueue_MessagesToQuoteTokenProcesses")
            try:
                data = eventQueue_MessagesToQuoteTokenProcesses.get(block=True, timeout=0.05)
                # PrintAndLog_Header(header_logging, "Checking eventQueue_MessagesToQuoteTokenProcesses: data = " + str(data))
                jData = json.loads(data)
                # PrintAndLog_Header(header_logging, "Checking eventQueue_MessagesToQuoteTokenProcesses: found method " + str(jData['method']) + ", jData = " + str(jData))

                if jData['method'] == 'kill':
                    # Shut down the process
                    PrintAndLog_Header(header_logging, "Received the kill signal to stop the process. quoteToken " + str(
                        quoteToken) + " os.getpid() = " + str(os.getpid()))
                    return
                elif jData['method'] != 'signal_getPrices':
                    raise Exception("method not supported: " + str(jData['method']))

                # If we've made it this far, we can process the payload

                # Extract the data we need from the payload
                blockNumber = jData['payload']['blockNumber']
                resultId = jData['payload']['resultId']
                detailed = jData['payload']['detailed']
                quoteTokenExchangeNamesDict = jData['payload']['quoteTokenExchangeNamesDict']
                exchangeNameExclusions = jData['payload']['exchangeNameExclusions']
                quoteTokenBaseTokensDict = jData['payload']['quoteTokenBaseTokensDict']

                PrintAndLog_Header(header_logging, "Called with resultId " + str(resultId))
                PrintAndLog_Header(header_logging, "found detailed = " + str(detailed))
                PrintAndLog_Header(header_logging, "found quoteTokenExchangeNamesDict = " + str(quoteTokenExchangeNamesDict))
                PrintAndLog_Header(header_logging, "found exchangeNameExclusions = " + str(exchangeNameExclusions))
                PrintAndLog_Header(header_logging, "found quoteTokenBaseTokensDict = " + str(quoteTokenBaseTokensDict))

                if not initialized:
                    # PrintAndLog_Header(header_logging, "Initializing because this is our first run")
                    # Initialize things that only need run once at the start of this process

                    # Start the processCommunication client for this quoteToken so it can communicate with the main process' server
                    Libraries.processCommunication.StartClient_InBackgroundThread()
                    Libraries.ratesGraph.Set_HighestInitializedRatesGraphBlockNumber_BasedOnLatestBlockNumber__Process_QuoteToken(blockNumber)
                    # We must init the ratesGraphs for the first time before so that when we call GetPrices we have graphs to put the data in
                    Libraries.ratesGraph.InitializeRatesGraphsForNearFutureBlocks__Process_QuoteToken(blockNumber)
                    initialized = True

                # Make sure we have graphs initialized
                if not Libraries.ratesGraph.IsBlockReadyInRatesGraphDicts__Process_QuoteToken(blockNumber):
                    # This is supposed to be done ahead of time,
                    # but if a bunch of blocks get mined very quickly back to back to back, it's possible this isn't initialized yet
                    # Since it's not initialized yet, we have to do it now on this thread before we can proceed with getting price data
                    PrintAndLog_Header(header_logging,
                                       "Found a block that wasn't initialized right when we were about to call GetAllPrices_ForQuoteToken. "
                                       "Manually initializing the rates graphs on this process's main thread now so we can move forward. blockNumber = " + str(blockNumber))
                    Libraries.ratesGraph.InitializeRatesGraphForBlock__Process_QuoteToken(blockNumber)

                # Get an updated RemoteNodeList from the main process
                # This is important because we want each process to know which nodes have the latest block information
                # so that each process can make calls to the appropriate nodes in Ninja's node network
                API_UpdateRemoteNodeList_FromMainProcess()

                before = datetime.datetime.now()
                # Call GetAllPrices_ForQuoteToken with the shared memory variables
                exchangesResultsDict, expectedResultsDict = Exchanges.ninja_arbitrage.GetAllPrices_ForQuoteToken(
                    quoteToken,
                    blockNumber,
                    detailed,
                    quoteTokenExchangeNamesDict,
                    exchangeNameExclusions,
                    quoteTokenBaseTokensDict)

                # PrintAndLog_Header(header_logging, "exchangesResultsDict = " + str(exchangesResultsDict))
                # PrintAndLog_Header(header_logging, "expectedResultsDict = " + str(expectedResultsDict))

                duration_s = (datetime.datetime.now() - before).total_seconds()

                PrintAndLog_Header(header_logging, "GetAllPrices_ForQuoteToken results received with duration_s = " + str(
                    duration_s) + " for resultId = " + str(resultId))

                # Send the results to the main process
                before = datetime.datetime.now()
                # PrintAndLog_Header(header_logging, "Before creating jData for result_getPrices")
                jData = {
                    'method': 'result_getPrices',
                    'payload': {
                        'resultId': resultId,
                        'timestamp': Libraries.core.GetUnixEpochTime_FromDateTime_seconds(datetime.datetime.now()),
                        'exchangesResultsDict': exchangesResultsDict,
                        'expectedResultsDict': expectedResultsDict,
                        'Libraries.ratesGraph.RatesGraph_Dict__Process_QuoteToken': Libraries.ratesGraph.RatesGraph_Dict__Process_QuoteToken[blockNumber],
                        'Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames__Process_QuoteToken': Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber],
                        'Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens__Process_QuoteToken': Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber],
                    }
                }
                # PrintAndLog_Header(header_logging, "After creating jData for result_getPrices")
                # PrintAndLog_Header(header_logging, "Before sending result_getPrices to main process, jData = " + str(jData))
                Libraries.processCommunication.SendData_FromSubProcessToMainProcess(jData)
                # PrintAndLog_Header(header_logging, "After sending result_getPrices to main process, new duration_s = " + str(duration_s))

                # TODO testing memory usage without all of this stuff.
                #  Now that I discovered that ZMQ is using so much memory, i may not have a memory leak after all
                # # Clean up memory to avoid memory leaks
                # jData.clear()
                #
                # Libraries.ratesGraph.RatesGraph_Dict__Process_QuoteToken[blockNumber].clear()
                # del Libraries.ratesGraph.RatesGraph_Dict__Process_QuoteToken[blockNumber]
                #
                # Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber].clear()
                # del Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber]
                #
                # Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber].clear()
                # del Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber]
                #
                # # call garbage collection, do I need this?
                # IsTimeToExecute("Libraries.utils.PerformGarbageCollection", 33, Libraries.utils.PerformGarbageCollection,
                #                 True, True, False, "sub process " + str(ProcessIdString))

            except queue.Empty:
                # PrintAndLog_Header(header_logging, "Checking eventQueue_MessagesToQuoteTokenProcesses: empty")
                pass

            if initialized:
                # Some things we should only do if we're initialized
                if blockNumber:
                    IsTimeToExecute("Libraries.ratesGraph.InitializeRatesGraphsForNearFutureBlocks__Process_QuoteToken__ThenCall__RemoveOldRatesGraphs__Process_QuoteToken",
                                    Libraries.defaults.Interval_seconds_InitializeRatesGraphsForNearFutureBlocks,
                                    Libraries.ratesGraph.InitializeRatesGraphsForNearFutureBlocks__Process_QuoteToken__ThenCall__RemoveOldRatesGraphs__Process_QuoteToken,
                                    True, True, False, blockNumber)

                IsTimeToExecute("API_UpdateQuoteTokensToSpendList_FromMainProcess", 35, API_UpdateQuoteTokensToSpendList_FromMainProcess, True)
                # TODO trying this on on main thread to see if this avoids the ZMQ deadlock???
                # IsTimeToExecute("API_UpdateQuoteTokensToSpendList_FromMainProcess", 35, API_UpdateQuoteTokensToSpendList_FromMainProcess, False)

            else:
                # Things we should do regardless of being initialized
                pass

            # TODO sleep here? How long?
            time.sleep(0.005)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception in ProcessLoop_QuoteTokens with quoteToken = " + str(
                quoteToken) + " and blockNumber = " + str(blockNumber) + ": " + traceback.format_exc())

        # TODO sleep here??
        time.sleep(0.005)


def API_UpdateQuoteTokensToSpendList_FromMainProcess():
    import Exchanges.keeperDAO

    global ProcessIdString

    header_logging = "API_UpdateRemoteNodeList_FromMainProcess: " + str(ProcessIdString)

    payload = {
        'method': 'get_quoteTokensToSpendList',
    }
    # PrintAndLog_Header(header_logging, "Calling SendData_FromSubProcessToMainProcess with payload = " + str(payload))
    response = Libraries.processCommunication.SendData_FromSubProcessToMainProcess(payload)
    # PrintAndLog_Header(header_logging, "Updating Exchanges.keeperDAO.ManuallyAddedTradeAmountArrayDict to: " + str(response))
    Exchanges.keeperDAO.ManuallyAddedTradeAmountArrayDict = response


def API_UpdateRemoteNodeList_FromMainProcess():
    global Seed
    global SeedLength
    global ProcessIdString

    before = datetime.datetime.now()
    header_logging = "API_UpdateRemoteNodeList_FromMainProcess: " + str(ProcessIdString)

    payload = {
        'method': 'get_RemoteNodeList',
        'payload': {
            'seed': Seed,
            'seedLength': SeedLength,
        }
    }
    # PrintAndLog_Header(header_logging, "Calling SendData_FromSubProcessToMainProcess with payload = " + str(payload))
    response = Libraries.processCommunication.SendData_FromSubProcessToMainProcess(payload)

    # Convert the response into a NodeList that has URL_RemoteNode objects in it instead of strings
    # PrintAndLog_Header(header_logging, "response = " + str(response))
    nodeList_strings = response['nodeList']
    # PrintAndLog_Header(header_logging, "nodeList_strings = " + str(nodeList_strings))
    nodeList_objects = []
    for nodeList_string in nodeList_strings:
        nodeList_objects.append(Libraries.nodes.URL_RemoteNode(nodeList_string))

    # PrintAndLog_Header(header_logging, "Updating Libraries.nodes.RemoteNodeList:")
    # PrintAndLog_Header(header_logging, "   from " + str(Libraries.nodes.RemoteNodeList))
    # PrintAndLog_Header(header_logging, "   to: " + str(nodeList_objects))
    duration_s = (datetime.datetime.now() - before).total_seconds()
    # PrintAndLog_Header(header_logging, "   call duration: " + str(duration_s) + " seconds")
    Libraries.nodes.RemoteNodeList = nodeList_objects
    # PrintAndLog_Header(header_logging, "   Updated Libraries.nodes.RemoteNodeList to " + str(Libraries.nodes.RemoteNodeList))


# TODO update this function for the hiding book
def API_Update0xOrderAggregators_FromMainProcess(quoteToken):
    global ProcessIdString

    before = datetime.datetime.now()
    header_logging = "API_UpdateRemoteNodeList_FromMainProcess: " + str(ProcessIdString)

    payload = {
        'method': 'get_0xOrderAggregatorData',
        'payload': {
            'quoteToken': quoteToken,
        }
    }
    # PrintAndLog_Header(header_logging, "Calling SendData_FromSubProcessToMainProcess with payload = " + str(payload))
    response = Libraries.processCommunication.SendData_FromSubProcessToMainProcess(payload)

    duration_s = (datetime.datetime.now() - before).total_seconds()
    # PrintAndLog_Header(header_logging, "   call duration: " + str(duration_s) + " seconds")
    # PrintAndLog_Header(header_logging, "   response for quoteToken " + str(quoteToken) + " = " + str(response))

    # Update my orderAggregator objects based on data from the response
    # Support all 0x orderbook style exchanges
    exchangeNames = []
    if ExchangeName_0xMesh in Libraries.exchanges.ExchangeDict:
        exchangeNames.append(ExchangeName_0xMesh)
    if ExchangeName_HidingBook in Libraries.exchanges.ExchangeDict:
        exchangeNames.append(ExchangeName_HidingBook)

    for exchangeName in exchangeNames:
        # baseTokenList = GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken(exchangeName, quoteToken)
        # PrintAndLog_Header(header_logging, "   comparing response with the following baseTokenList for 0x, baseTokenList = " + str(baseTokenList))
        # The main process is only sending me data for tokens it has orders for.
        # So I should clear all order data first before I update my data cache and assume if I don't have any data for a token then there exists none
        for baseToken in response['orderAggregators'][exchangeName]:
            # Update the OrderAggregators in the token objects in this sub process
            token = GetTokenObjectGivenTokenAddress(baseToken)
            orderAggregator = token.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()]
            orderAggregator.orderDict_Asks = {}
            orderAggregator.orderDict_Bids = {}
            # PrintAndLog_Header(header_logging, "   Cleared orderAggregators to empty dicts")
            # PrintAndLog_Header(header_logging, "   Cleared orderAggregators to empty dicts. Has " + str(len(orderAggregator.orderDict_Bids)) + " bids and " + str(
            #     len(orderAggregator.orderDict_Asks)) + " asks for quoteToken " + str(quoteToken) + " and baseToken " + str(baseToken))

        # Now iterate over the response and update the orderAggregators based on the response
        for baseToken in response['orderAggregators'][exchangeName]:
            orderDict_bids_0x = response['orderAggregators'][exchangeName][baseToken]['orderDict_bids_0x']
            orderDict_asks_0x = response['orderAggregators'][exchangeName][baseToken]['orderDict_asks_0x']
            # PrintAndLog_Header(header_logging, "   found " + str(len(orderDict_bids_0x)) + " bids and " + str(
            #     len(orderDict_asks_0x)) + " asks for quoteToken " + str(quoteToken) + " and baseToken " + str(baseToken))

            # Update the OrderAggregators in the token objects in this sub process
            token = GetTokenObjectGivenTokenAddress(baseToken)
            orderAggregator = token.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()]
            orderAggregator.orderDict_Asks = orderDict_asks_0x
            orderAggregator.orderDict_Bids = orderDict_bids_0x
            # PrintAndLog_Header(header_logging, "   Updated orderAggregators. orderAggregator.GetDetails() = " + str(orderAggregator.GetDetails()))

    # Deserialize the objects we need from Libraries.orderAggregator so we can update this sub process' cache objects
    Libraries.orderAggregator.AmountFilledValueDict_0x = jsonpickle.decode(response['Libraries.orderAggregator.AmountFilledValueDict_0x'])
    Libraries.orderAggregator.BalanceValueDict_0x = jsonpickle.decode(response['Libraries.orderAggregator.BalanceValueDict_0x'])
    Libraries.orderAggregator.AllowanceValueDict_0xv2 = jsonpickle.decode(response['Libraries.orderAggregator.AllowanceValueDict_0xv2'])
    Libraries.orderAggregator.AllowanceValueDict_0xv4 = jsonpickle.decode(response['Libraries.orderAggregator.AllowanceValueDict_0xv4'])
    # PrintAndLog_Header(header_logging, "   Updated Libraries.orderAggregator cache dicts")
