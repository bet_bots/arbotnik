import datetime
import random
import string
import threading
import time
import traceback
from enum import Enum

import Libraries.cache
import Libraries.core
import Libraries.customDicts
import Libraries.defaults
import Libraries.gasStation
import Libraries.nonceUtils
import Libraries.orderIntent
import Libraries.utils
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader

# This keeps track of all nonces for all transactionIds that we submitted. Being able to quickly look up a nonce for a transaction is very convenient.
TransactionNonceAssociationDict = Libraries.customDicts.DictWithValue()


class TxReceiptBlockStatus(Enum):
    Succeeded = 1
    RevertedOrErrored = 0


class Trade:
    market = None
    transactionId = None
    orderDX = None
    exchangeDX = None
    exchangeCX = None
    effectiveEther_toSpendOnTrade = None
    effectiveTokens_toSpendOnTrade = None

    def __init__(self, _market, _transactionId, _orderDX, _exchangeDX, _exchangeCX, _effectiveEther_toSpendOnTrade, _effectiveTokens_toSpendOnTrade):
        self.market = _market
        self.transactionId = _transactionId
        self.orderDX = _orderDX
        self.exchangeDX = _exchangeDX
        self.exchangeCX = _exchangeCX
        self.effectiveEther_toSpendOnTrade = _effectiveEther_toSpendOnTrade
        self.effectiveTokens_toSpendOnTrade = _effectiveTokens_toSpendOnTrade


class Thread_PendingTransactionManager(object):

    def __init__(self, interval=10):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        """ Method that runs forever """
        from Libraries.accounts import NinjaOpAccountDict, ReleaseReservableEthereumAccountPoolAccounts_BasedOnNonces
        from Libraries.trade import HandlePendingTrade_NinjaOpAccountDict
        from Exchanges.ninja import SetLastDateTime_NinjaTradeReverted
        from Libraries.core import API_PostOperatorNotification

        while True:
            # Check the NinjaOpAccountDict for pending transactions
            for name in NinjaOpAccountDict:
                try:
                    copyOfPendingTransactionList = NinjaOpAccountDict[name].GetCopyOfPendingTransactionList()
                    if len(copyOfPendingTransactionList) > 0:
                        PrintAndLog("Thread_PendingTransactionManager NinjaOpAccountDict: len(copyOfPendingTransactionList) = " + str(len(copyOfPendingTransactionList)))
                        for pendingTransaction in copyOfPendingTransactionList:
                            try:
                                PrintAndLog("Thread_PendingTransactionManager NinjaOpAccountDict: analyzing " + str(pendingTransaction.GetDetails()))

                                transactionResult, txReceipt = API_PostCheckPendingTransaction(pendingTransaction.transactionId)
                                PrintAndLog("Thread_PendingTransactionManager NinjaOpAccountDict: transactionResult = " + str(transactionResult))

                                # Always do this first regardless of transactionResult
                                if pendingTransaction.transactionType == TransactionType.trade:
                                    HandlePendingTrade_NinjaOpAccountDict(NinjaOpAccountDict[name], pendingTransaction, transactionResult, txReceipt)

                                # TODO, I'm disabling this for now, not sure if it's needed anymore
                                # # If the transaction has been mined in
                                # if transactionResult == TransactionResult.confirmed or transactionResult == TransactionResult.error_revertOrOutOfGasOrBadJumpDestinationEtc:
                                #     # Set it's nonce as used so we don't attempt to use it again.
                                #     Libraries.nonceUtils.SetNonceUsed(pendingTransaction.publicAddress, pendingTransaction.nonce)

                                if transactionResult == TransactionResult.confirmed:
                                    message = "Thread_PendingTransactionManager NinjaOpAccountDict: Transaction for " + str(name) + " (" + str(
                                        pendingTransaction.transactionType) + ") has confirmed. Nulling out this pendingTransaction. TransactionId was: " + str(
                                        pendingTransaction.transactionId)
                                    PrintAndLog(message)
                                    # API_PostOperatorNotification(message)
                                    NinjaOpAccountDict[name].RemovePendingTransaction(pendingTransaction)

                                elif transactionResult == TransactionResult.error_revertOrOutOfGasOrBadJumpDestinationEtc:
                                    # Call SetLastDateTime_NinjaTradeReverted so we know the last time we had a transaction fail
                                    SetLastDateTime_NinjaTradeReverted()
                                    message = "Thread_PendingTransactionManager NinjaOpAccountDict: Transaction for " + str(name) + " (" + str(
                                        pendingTransaction.transactionType) + ") had an error. Nulling out this pendingTransaction. TransactionId was: " + str(
                                        pendingTransaction.transactionId)
                                    # PrintAndLog(message)
                                    API_PostOperatorNotification(message)
                                    NinjaOpAccountDict[name].RemovePendingTransaction(pendingTransaction)

                                # Consider expiring pending transactions if they've been pending for too long
                                else:
                                    # If this pending transaction has been pending for too long then we'll consider it poofed or timed out.
                                    if pendingTransaction.GetPendingDuration_seconds() > Libraries.defaults.ExpireCurrentPendingTransactionAfterX_Ninja_seconds:
                                        message = "Thread_PendingTransactionManager NinjaOpAccountDict: Failed to create message regarding timed out transaction"
                                        try:
                                            message = str(name) + "'s " + str(
                                                pendingTransaction.transactionType) + " transaction didn't get mined into a block within " + str(
                                                Libraries.defaults.TransactionTimeout_TradeOrder_minutes) + " minutes. Nulling out this pendingTransaction. TransactionId was: " + str(
                                                pendingTransaction.transactionId)

                                        except:
                                            PrintAndLogError("exception: " + traceback.format_exc())
                                            pass

                                        # call RemovePendingTransaction_GivenTransactionId now that we're considering this transaction to be "poofed" or timed out
                                        NinjaOpAccountDict[name].RemovePendingTransaction(pendingTransaction)
                                        API_PostOperatorNotification(message)

                                # If this transaction has been mined into a block, regardless of result
                                if transactionResult != TransactionResult.unconfirmed:
                                    # Consider releasing any accounts that were held as in use in a ReservableEthereumAccountPool
                                    ReleaseReservableEthereumAccountPoolAccounts_BasedOnNonces()

                            except (KeyboardInterrupt, SystemExit):
                                print('\nkeyboard interrupt caught')
                                print('\n...Program Stopped Manually!')
                                raise

                            except:
                                PrintAndLogError("exception in Thread_PendingTransactionManager (for pendingTransaction) = " + traceback.format_exc())
                                pass

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    PrintAndLogError("exception in Thread_PendingTransactionManager (for name) = " + traceback.format_exc())
                    pass

            time.sleep(self.interval)


class PendingTransaction:
    transactionId = None
    transactionType = None
    nonce = None
    publicAddress = None
    marketName = None
    dateTimeSet = None

    def __init__(self, _transactionId, _transactionType, _nonce, _publicAddress, _marketName):
        self.transactionId = _transactionId
        self.transactionType = _transactionType
        self.nonce = _nonce
        self.publicAddress = _publicAddress
        self.marketName = _marketName
        self.dateTimeSet = datetime.datetime.now()

    def GetDetails(self):
        return str(self.marketName) + ", " + str(self.publicAddress) + ", " + str(self.transactionType) + ", nonce = " + str(self.nonce) + ", " + str(self.transactionId)

    def GetPendingDuration_seconds(self):
        # Returns the duration of time this transaction has been pending
        return (datetime.datetime.now() - self.dateTimeSet).total_seconds()


class TransactionResult(Enum):
    confirmed = 1
    unconfirmed = 2
    timedOut = 3
    error_revertOrOutOfGasOrBadJumpDestinationEtc = 4


class TransactionType(Enum):
    trade = 1
    approve = 2
    deposit = 3
    withdraw = 4
    cancelOrder = 5
    other = 6
    deploy = 7


def API_PostCheckPendingTransaction(transactionId, doPrintForDebug=True, extraCommandsDict=None, txReceipt=None,
                                    resultDict=None, resultKey=None, lock_resultDict=None, doUseResultDict=False,
                                    timeout=None, specifiedBlockNumber_int=None):
    from Libraries.core import API_PostOperatorNotification, API_GetTransactionReceipt, \
        DoesTransactionContainValidBlockNumberAndBlockHash, API_PostGetBlockNumberContainingThisTransaction, GetThreadSafeCopyOf_LatestBlockNumber_int, \
        RequestTimeout_seconds

    if not timeout:
        timeout = RequestTimeout_seconds

    transactionResult = TransactionResult.unconfirmed

    # extraCommandsDict is a dictionary containing keys which tell me to do extra things, like calculate block height.
    # They are extra because I don't want to do those things every time since they can cause extra API calls
    # The return value for this command is set in the dictionary's value

    try:
        if not txReceipt:
            txReceipt = API_GetTransactionReceipt(transactionId, resultDict, resultKey, lock_resultDict, timeout, specifiedBlockNumber_int)
            PrintAndLog("API_PostCheckPendingTransaction: txReceipt = " + str(txReceipt))
        else:
            PrintAndLog("API_PostCheckPendingTransaction had txReceipt passed in as an argument: txReceipt = " + str(txReceipt))

        # If it's unconfirmed
        if not txReceipt:
            transactionResult = TransactionResult.unconfirmed
            if doPrintForDebug:
                PrintAndLog("Transaction has not yet confirmed, this transactionId is not valid. Maybe it hasn't yet propagated "
                            "throughout the network yet. Put a timer on this in case it poofs and disappears? tx = " + str(transactionId))

        elif 'status' not in txReceipt:
            transactionResult = TransactionResult.unconfirmed
            if doPrintForDebug:
                PrintAndLog("Transaction has not yet confirmed, no status found in the txReceipt. Put a timer on this in case it poofs and disappears? tx = " + str(
                    transactionId))

        # If there's an error in this transaction, let's handle it
        elif int(txReceipt['status'], 16) != 1:
            transactionResult = TransactionResult.error_revertOrOutOfGasOrBadJumpDestinationEtc
            if doPrintForDebug:
                PrintAndLog("Transaction had an error: bad jump destination, bad instruction, or out of gas. Handle this case? tx = " + str(transactionId))

        elif not DoesTransactionContainValidBlockNumberAndBlockHash(txReceipt):
            transactionResult = TransactionResult.unconfirmed
            if doPrintForDebug:
                PrintAndLog("Transaction has not yet confirmed, the txReceipt does not contain both a valid blockNumber and blockHash!")

        # No errors in this transaction, and status says success so it must be confirmed
        else:
            # I'm having trouble with this API call returning good data 100% of the time API_PostGetBlockNumberContainingThisTransaction
            # I'm cutting this portion of the logic out since I cannot rely on it
            transactionResult = TransactionResult.confirmed
            blockNumber_int = Libraries.core.ConvertHexToInt(txReceipt['blockNumber'])
            PrintAndLog("Transaction confirmed in block " + str(blockNumber_int) + ", transactionId = " + str(transactionId))

            key_blockHeight = "blockHeight"
            if extraCommandsDict and key_blockHeight in extraCommandsDict:
                try:
                    blockNumberThisTransactionIsIn_hex = API_PostGetBlockNumberContainingThisTransaction(transactionId)
                    # Calculate the confirmations right here to avoid null issues,
                    confirmations = None
                    if GetThreadSafeCopyOf_LatestBlockNumber_int() and blockNumberThisTransactionIsIn_hex:
                        blockNumberThisTransactionIsIn_int = int(blockNumberThisTransactionIsIn_hex, 16)
                        confirmations = GetThreadSafeCopyOf_LatestBlockNumber_int() - blockNumberThisTransactionIsIn_int
                        if doPrintForDebug:
                            PrintAndLog("Confirmations = " + str(confirmations) + ", GetThreadSafeCopyOf_LatestBlockNumber_int() = " + str(
                                GetThreadSafeCopyOf_LatestBlockNumber_int()) + ", blockNumberThisTransactionIsIn_int = " + str(
                                blockNumberThisTransactionIsIn_int) + ", transactionId = " + str(transactionId))

                    # PrintAndLog("blockNumberThisTransactionIsIn_hex = " + str(blockNumberThisTransactionIsIn_hex))
                    PrintAndLog("confirmations = " + str(confirmations))
                    extraCommandsDict[key_blockHeight] = confirmations

                except:
                    PrintAndLogError("Exception in API_PostCheckPendingTransaction when trying to calculate block height = " + traceback.format_exc())

    except:
        PrintAndLogError("Exception inside API_PostCheckPendingTransaction, failed to determine the transactions state , " + str(transactionId))
        PrintAndLogError("exception = " + traceback.format_exc())

    if doUseResultDict:
        # TODO, new but not yet tested.  Does it work???
        Libraries.utils.ConsiderSettingResultDict(transactionResult, resultDict, 'transactionResult', lock_resultDict)
        Libraries.utils.ConsiderSettingResultDict(txReceipt, resultDict, 'transactionReceipt', lock_resultDict)

    return transactionResult, txReceipt


def API_SendTokens_ToAddress(fromAddress, fromPrivateKey, amount_ether, tokenAddress, toAddress, decimals, transactionType, gasPrice_int=None):
    if not gasPrice_int:
        gasPrice_int = Libraries.gasStation.GetGasPrice_Cheap()

    amount_wei = Libraries.core.ConvertEtherToWei(amount_ether, decimals)

    # Convert this hex string to a hex byte array
    data_hex = GetDataForSendingTokensToAddress(amount_wei, toAddress)
    # PrintAndLog("API_SendTokens_ToAddress: fromAddress = " + str(fromAddress) + ", amount_ether = " + str(amount_ether) + ", tokenAddress = " + str(
    #     tokenAddress) + ", toAddress = " + str(toAddress) + ", decimals = " + str(decimals))
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, tokenAddress, 0,
                                             Libraries.gasStation.GetGasLimit_Other(), gasPrice_int, data_hex, transactionType)

    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Sending " + str(amount_ether) + " " + str(tokenAddress) + ". transactionId: " + str(transactionId)
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to API_SendTokens_ToAddress, no transactionId")

    return transactionId


def API_DeployContract(fromAddress, fromPrivateKey, gas_int, gasPrice_int, data_hex, nonce=None):
    return API_SendEther_ToContract(fromAddress, fromPrivateKey, "", 0, gas_int, gasPrice_int,
                                    data_hex, TransactionType.deploy, nonce)


def API_CancelTransaction_BaseGasPriceOnSpecificTx(fromAddress, fromPrivateKey, transactionId):
    txInfo = Libraries.core.API_GetTransactionInfo(transactionId)
    PrintAndLog("API_CancelTransaction_BaseGasPriceOnSpecificTx: txInfo = " + str(txInfo))
    gasPrice = Libraries.core.ConvertHexToInt(txInfo['gasPrice'])
    PrintAndLog("API_CancelTransaction_BaseGasPriceOnSpecificTx: transactionId's gasPrice = " + str(gasPrice))
    # In order to override a transaction, we need to use a higher gas price by about 10%
    gasPrice = int(gasPrice * 1.11)
    PrintAndLog("API_CancelTransaction_BaseGasPriceOnSpecificTx: gasPrice after adjusting to override the other tx = " + str(gasPrice))
    # Cancel by sending 0 ether to yourself
    return API_SendEther_ToAddress(fromAddress, fromPrivateKey, fromAddress, 0,
                                   Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt,
                                   gasPrice, TransactionType.other)


def API_CancelTransaction(fromAddress, fromPrivateKey, gasPrice, nonce=None):
    # Cancel by sending 0 ether to yourself
    return API_SendEther_ToAddress(fromAddress, fromPrivateKey, fromAddress, 0,
                                   Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt,
                                   gasPrice, TransactionType.other, nonce)


def API_SendEther_ToContract(fromAddress, fromPrivateKey, sendToAddress, value, gas_int, gasPrice_int, data_hex,
                             transactionType, nonce=None, forceSendRegardlessOfCurrentlyPendingForThisAddress=False,
                             doIncrementAndResubmitIfFailedWithNonceTooLow=False, autoGasPriceIncrementIterations=Libraries.defaults.DefaultAutoGasPriceIncrementIterations):
    from Libraries.marketUtils import GetNinjaOpAccountGivenPublicAddress
    from Libraries.core import API_PostOperatorNotification, SignTransaction, API_SendRawTransaction_ToManyRemoteNodes, \
        TransactionErrors_NonceIsTooLow, TransactionErrors_NonceTooLow

    if not sendToAddress:
        # We can only allow sendToAddress to not be set if we're deploying a contract
        if transactionType == TransactionType.deploy:
            pass
        else:
            PrintAndLog_FuncNameHeader("sendToAddress is None! Failed to API_SendEther_ToContract")
            return None

    if not fromAddress:
        PrintAndLog_FuncNameHeader("fromAddress is None! Failed to API_SendEther_ToContract")
        return None

    ninjaOpAccount = GetNinjaOpAccountGivenPublicAddress(fromAddress)

    # PrintAndLog_FuncNameHeader("API_SendEther_ToContract fromAddress: " + fromAddress + ", sendToAddress: " + sendToAddress)

    transactionCount = None
    # We cannot call API_GetTransactionCount if this fromAddress has not yet made a transaction,
    # because the call will error out because the account hasn't yet been used
    # This makes for some awkward logic, my work around is this, don't call it if nonce is hard coded to zero!
    if nonce != 0:
        # Get the transactionCount for the nonce
        transactionCount, dontCare = Libraries.cache.GetTransactionCountFromCacheIfPossible(fromAddress)

    nonceToUse = None
    # If we're using a zero nonce
    if nonce == 0:
        # Set nonceToUse and do not look at transactionCount at all, since we cannot make the call because it will fail
        nonceToUse = nonce
    # If we're just using the transactionCount as the nonce
    elif not nonce:
        PrintAndLog_FuncNameHeader("using nonce from transactionCount: " + str(transactionCount))
        nonceToUse = transactionCount
    # If we're going to override the nonce to something specific (either future or current)
    else:
        PrintAndLog_FuncNameHeader("using hard coded nonce = " + str(nonce) + ", while transactionCount = " + str(transactionCount))
        nonceToUse = nonce

        if nonceToUse < transactionCount:
            # TODO, I'm not sure if I should throw an exception here.  I want to see if this ever happens before I start throwing exceptions.  This may never happen...
            message = "GetNonceToUseForQueueableTransaction: Nonce invalid.  Overriding to transactionCount. transactionCount = " + str(
                transactionCount) + " yet nonceToUse was calculated to be = " + str(
                nonceToUse) + ". nonceToUse should never be less than transactionCount. I must have a bug."
            API_PostOperatorNotification(message)

            # override the nonce because our nonceToUse was less than the transactionCount and that shouldn't happen.
            # If this did happen, than lots of transactions must be flying around and blocks must be getting mined in real quick within the last few seconds
            nonceToUse = transactionCount

    # Sign and broadcast transaction
    # autoGasPriceIncrementIterations enables me to auto re-send this transaction again at a slightly higher gas price after some period of time
    # This helps prevent front running when people are targeting me
    # This also just helps me get a tx through if it's taking a long time
    # autoGasPriceIncrementIterations may conflict with doIncrementAndResubmitIfFailedWithNonceTooLow
    if autoGasPriceIncrementIterations > Libraries.defaults.DefaultAutoGasPriceIncrementIterations and doIncrementAndResubmitIfFailedWithNonceTooLow:
        message = "both autoGasPriceIncrementIterations and doIncrementAndResubmitIfFailedWithNonceTooLow cannot be set to non default values at the same time " \
                  "because they can conflict and cause undesirable problems. Disabling doIncrementAndResubmitIfFailedWithNonceTooLow for this call."
        API_PostOperatorNotification(message)
        doIncrementAndResubmitIfFailedWithNonceTooLow = False

    # If autoGasPriceIncrementIterations is enabled on the main thread
    if autoGasPriceIncrementIterations > Libraries.defaults.DefaultAutoGasPriceIncrementIterations and threading.current_thread() is threading.main_thread():
        # Warn the user that it wasn't designed to be used that way.
        message = "Warning: autoGasPriceIncrementIterations is currently being used on the main thread. " \
                  "This will cause extreme delays! Do you really want to sleep the main thread??? Please use a background thread"
        API_PostOperatorNotification(message)

    if autoGasPriceIncrementIterations > Libraries.defaults.MaxSafeAutoGasPriceIncrementIterations:
        # This could get dangerous
        message = "Warning: autoGasPriceIncrementIterations was not intended to be very high. " \
                  "This could have very expensive consequences if this number gets too high! Raising exception for safety. " \
                  "Overwriting autoGasPriceIncrementIterations from " + str(autoGasPriceIncrementIterations) + " to " + str(Libraries.defaults.MaxSafeAutoGasPriceIncrementIterations)
        API_PostOperatorNotification(message)
        autoGasPriceIncrementIterations = Libraries.defaults.MaxSafeAutoGasPriceIncrementIterations

    transactionIdList = []
    for index_gasPriceIncrementer in range(autoGasPriceIncrementIterations + 1):
        if index_gasPriceIncrementer > 0:
            # Sleep a bit before I incrementally raise the gas price up
            PrintAndLog_FuncNameHeader("Sleeping a bit before I incrementally raise up the gas price with the same nonce")
            time.sleep(Libraries.defaults.SleepTimeBetweenAutoGasPriceIncrementIterations_seconds)

            # Raise the gas price
            gasPrice_int = int(gasPrice_int * 1.105)
            PrintAndLog_FuncNameHeader("gasPrice_int after raising gas price to " + str(
                gasPrice_int) + " wei, or " + str(Libraries.core.ConvertWeiToGwei(gasPrice_int)) + " gwei")

        transactionId = None
        maxTries = 1
        tryCount = 0
        safetyCount = 0
        if doIncrementAndResubmitIfFailedWithNonceTooLow:
            # If doIncrementAndResubmitIfFailedWithNonceTooLow is True, we can consider trying more than once
            maxTries = 2

        # Send the transaction, consider trying again under certain circumstances
        while tryCount < maxTries:
            tryCount += 1
            PrintAndLog_FuncNameHeader("try " + str(tryCount) + " of " + str(maxTries) + ", nonceToUse = " + str(nonceToUse) + ", gasPrice_int = " + str(gasPrice_int))

            result = SignTransaction(sendToAddress, value, fromPrivateKey, nonceToUse, gas_int, gasPrice_int, data_hex)

            error = result['error']
            PrintAndLog_FuncNameHeader("transactionHash error = " + str(error))
            if error:
                PrintAndLog_FuncNameHeader("transactionHash result = " + str(result))
            signedTransactionHash = result['sign']
            # PrintAndLog_FuncNameHeader("signedTransactionHash = " + signedTransactionHash)

            # Call API_PostSendRawTransaction to send the raw transaction to one single remote node
            # transactionId = API_PostSendRawTransaction(signedTransactionHash)

            transactionId, broadcastError = API_SendRawTransaction_ToManyRemoteNodes(signedTransactionHash)
            PrintAndLog_FuncNameHeader("transactionId: " + str(transactionId) + ", broadcastError: " + str(broadcastError))

            # Here is where I consider breaking or incrementing the nonce and letting the loop try again.

            # If we have a valid transactionId
            if transactionId:
                # Track each transactionId
                transactionIdList.append(transactionId)

                # Update the TransactionNonceAssociationDict
                TransactionNonceAssociationDict.SetValue(transactionId.lower(), nonceToUse)

                PrintAndLog_FuncNameHeader("breaking from the loop because we got a transactionId")
                break
            # If we are allowed to increment the nonce and try again AND we got an error that says our nonce is too low
            elif doIncrementAndResubmitIfFailedWithNonceTooLow and \
                    broadcastError and (broadcastError == TransactionErrors_NonceIsTooLow or broadcastError == TransactionErrors_NonceTooLow):
                # Increment the nonce by one, so we can try again next loop iteration
                nonceToUse += 1
                PrintAndLog_FuncNameHeader("incremented the nonce in case it tries again next time, nonceToUse = " + str(nonceToUse))
            # Else broadcasting the transaction failed, and we got some other error
            else:
                PrintAndLog_FuncNameHeader("breaking from the loop because we got some kind of error when broadcasting the tx and "
                                           "we have not yet handled this error.  So just assume that the tx failed to broadcast and return.")
                break

            # Use another counter to ensure this doesn't loop to many times.  This can be removed if the other counter proves to work.
            safetyCount += 1
            if safetyCount > maxTries or safetyCount > 3:
                message = "API_SendEther_ToContract: safetyCount has exceeded maxTries or my hard coded safety number. " \
                          "This should not have happened! safetyCount = " + str(safetyCount) + ", maxTries = " + str(maxTries) + ". Breaking for safety"
                API_PostOperatorNotification(message)
                break

    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    PrintAndLog_FuncNameHeader("considering calling SetNewPendingTransaction with transactionIdList: " + str(transactionIdList) + ", transactionType = " + str(
        transactionType) + ", nonceToUse = " + str(nonceToUse) + ", ninjaOpAccount = " + str(ninjaOpAccount))

    for transactionId in transactionIdList:
        # Consider calling SetNewPendingTransaction on the ninjaOpAccount
        if transactionId and ninjaOpAccount:
            ninjaOpAccount.SetNewPendingTransaction(transactionId, transactionType, nonceToUse)

    # For backwards compatibility to everything that is used to seeing only one transactionId,
    # I need to return exactly one transactionId instead of a list.
    if autoGasPriceIncrementIterations == Libraries.defaults.DefaultAutoGasPriceIncrementIterations:
        # autoGasPriceIncrementIterations is not being used, so just return one single transactionId
        return transactionIdList[0]
    else:
        # autoGasPriceIncrementIterations is being used and we should have many transactionIds
        return transactionIdList


def API_SendEther_ToAddress(fromAddress, fromPrivateKey, sendToAddress, value_wei_int, gas_int, gasPrice_int, transactionType, nonce=None):
    from Libraries.core import SignTransaction, API_SendRawTransaction_ToManyRemoteNodes

    # PrintAndLog_FuncNameHeader("gas_int = " + str(gas_int))
    # PrintAndLog_FuncNameHeader("gasPrice_int = " + str(gasPrice_int))

    if not sendToAddress:
        PrintAndLog_FuncNameHeader("sendToAddress is None! Failed to API_SendEther_ToAddress")
        return None

    if not fromAddress:
        PrintAndLog_FuncNameHeader("fromAddress is None! Failed to API_SendEther_ToAddress")
        return None

    PrintAndLog_FuncNameHeader("API_SendEther_ToAddress fromAddress: " + fromAddress + ", sendToAddress: " + sendToAddress + ", value_wei_int: " + str(value_wei_int))

    nonceToUse = None
    # 0 is an acceptable value
    if nonce or nonce == 0:
        nonceToUse = nonce
        PrintAndLog_FuncNameHeader("setting nonce from hard coded nonce: " + str(nonceToUse))
    else:
        # nonceToUse = API_GetTransactionCount(fromAddress)
        nonceToUse, dontCare = Libraries.cache.GetTransactionCountFromCacheIfPossible(fromAddress)
        PrintAndLog_FuncNameHeader("setting nonce from transactionCount: " + str(nonceToUse))

    result = SignTransaction(sendToAddress, value_wei_int, fromPrivateKey, nonceToUse, gas_int, gasPrice_int)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    error = result['error']
    PrintAndLog_FuncNameHeader("transactionHash error = " + str(error))
    if error:
        PrintAndLog_FuncNameHeader("transactionHash result = " + str(result))
    signedTransactionHash = result['sign']
    # PrintAndLog_FuncNameHeader("signedTransactionHash = " + signedTransactionHash)

    transactionId, error = API_SendRawTransaction_ToManyRemoteNodes(signedTransactionHash)
    PrintAndLog_FuncNameHeader("transactionId: " + str(transactionId))
    if transactionId:
        # Update the TransactionNonceAssociationDict
        TransactionNonceAssociationDict.SetValue(transactionId.lower(), nonceToUse)

    return transactionId


def GetDataFor_FillOrder_EtherDelta(order, orderAmountToFill_ether, decimals):
    from Libraries.core import PadDataParemeter_LeftFillPadding, Remove0XfromHexString

    # Regardless if you're buying or selling, get and give are the same
    # tokenGet no changes, already in hex
    # amountGet convert from hex to int
    # tokenGive no changes, already in hex
    # amountGive convert from hex to int
    # expires convert from hex to int
    # nonce convert from hex to int
    # requires no changes, already in hex
    # v convert from hex to int
    # r requires no changes, already in hex
    # s requires no changes, already in hex
    # amount convert from hex to int

    # So everything gets copied directly from etherdelta with the exception of the following:
    #       amount is the amount you're buying or selling but it's always in ether
    #       so if you're buying 100 ABC tokens for 1 ether then you enter 1.0 ether in wei units
    #       and if you're selling 100 ABC tokens for 1 ether then you enter 1.0 ether in wei units
    #       amount is never in ABC tokens

    data = ""
    addNewLinesForDebug = False
    printDataForDebug = False
    # Method
    data += "0x0a19b14a"
    if addNewLinesForDebug:
        data += "\n"

    # tokenGet
    tokenGet = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(order.tokenGet))
    if printDataForDebug:
        PrintAndLog("tokenGet: " + str(tokenGet))
    data += tokenGet
    if addNewLinesForDebug:
        data += "\n"

    # amountGet
    amountGet = PadDataParemeter_LeftFillPadding("%x" % int(float(order.amountGet)))
    if printDataForDebug:
        PrintAndLog("amountGet: " + str(amountGet))
    data += amountGet
    if addNewLinesForDebug:
        data += "\n"

    # tokenGive
    tokenGive = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(order.tokenGive))
    if printDataForDebug:
        PrintAndLog("tokenGive: " + str(tokenGive))
    data += tokenGive
    if addNewLinesForDebug:
        data += "\n"

    # amountGive
    amountGive = PadDataParemeter_LeftFillPadding("%x" % int(float(order.amountGive)))
    if printDataForDebug:
        PrintAndLog("amountGive: " + str(amountGive))
    data += amountGive
    if addNewLinesForDebug:
        data += "\n"

    # expires
    expires = PadDataParemeter_LeftFillPadding("%x" % int(float(order.expires)))
    if printDataForDebug:
        PrintAndLog("expires: " + str(expires))
    data += expires
    if addNewLinesForDebug:
        data += "\n"

    # nonce
    nonce = PadDataParemeter_LeftFillPadding("%x" % int(float(order.nonce)))
    if printDataForDebug:
        PrintAndLog("nonce: " + str(nonce))
    data += nonce
    if addNewLinesForDebug:
        data += "\n"

    # user
    user = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(order.user))
    if printDataForDebug:
        PrintAndLog("user: " + str(user))
    data += user
    if addNewLinesForDebug:
        data += "\n"

    # v
    v = PadDataParemeter_LeftFillPadding("%x" % int(float(order.v)))
    if printDataForDebug:
        PrintAndLog("v: " + str(v))
    data += v
    if addNewLinesForDebug:
        data += "\n"

    # r
    r = Remove0XfromHexString(order.r)
    if printDataForDebug:
        PrintAndLog("r: " + str(r))
    data += r
    if addNewLinesForDebug:
        data += "\n"

    # s
    s = Remove0XfromHexString(order.s)
    if printDataForDebug:
        PrintAndLog("s: " + str(s))
    data += s
    if addNewLinesForDebug:
        data += "\n"

    # amount
    amount = PadDataParemeter_LeftFillPadding("%x" % int(float(orderAmountToFill_ether * decimals)))
    if printDataForDebug:
        PrintAndLog("amount: " + str(amount))
    data += amount

    if printDataForDebug:
        PrintAndLog("data: " + str(data))

    return data


def GetData_CancelOrder_EtherDelta(order, decimals):
    from Libraries.core import PadDataParemeter_LeftFillPadding, Remove0XfromHexString

    data = ""
    addNewLinesForDebug = False
    printDataForDebug = False
    # Method
    data += "0x278b8c0e"
    if addNewLinesForDebug:
        data += "\n"

    # tokenGet
    tokenGet = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(order.tokenGet))
    if printDataForDebug:
        PrintAndLog("tokenGet: " + str(tokenGet))
    data += tokenGet
    if addNewLinesForDebug:
        data += "\n"

    # amountGet
    amountGet = PadDataParemeter_LeftFillPadding("%x" % int(float(order.amountGet)))
    if printDataForDebug:
        PrintAndLog("amountGet: " + str(amountGet))
    data += amountGet
    if addNewLinesForDebug:
        data += "\n"

    # tokenGive
    tokenGive = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(order.tokenGive))
    if printDataForDebug:
        PrintAndLog("tokenGive: " + str(tokenGive))
    data += tokenGive
    if addNewLinesForDebug:
        data += "\n"

    # amountGive
    amountGive = PadDataParemeter_LeftFillPadding("%x" % int(float(order.amountGive)))
    if printDataForDebug:
        PrintAndLog("amountGive: " + str(amountGive))
    data += amountGive
    if addNewLinesForDebug:
        data += "\n"

    # expires
    expires = PadDataParemeter_LeftFillPadding("%x" % int(float(order.expires)))
    if printDataForDebug:
        PrintAndLog("expires: " + str(expires))
    data += expires
    if addNewLinesForDebug:
        data += "\n"

    # nonce
    nonce = PadDataParemeter_LeftFillPadding("%x" % int(float(order.nonce)))
    if printDataForDebug:
        PrintAndLog("nonce: " + str(nonce))
    data += nonce
    if addNewLinesForDebug:
        data += "\n"

    # v
    v = PadDataParemeter_LeftFillPadding("%x" % int(float(order.v)))
    if printDataForDebug:
        PrintAndLog("v: " + str(v))
    data += v
    if addNewLinesForDebug:
        data += "\n"

    # r
    r = Remove0XfromHexString(order.r)
    if printDataForDebug:
        PrintAndLog("r: " + str(r))
    data += r
    if addNewLinesForDebug:
        data += "\n"

    # s
    s = Remove0XfromHexString(order.s)
    if printDataForDebug:
        PrintAndLog("s: " + str(s))
    data += s
    if addNewLinesForDebug:
        data += "\n"

    if printDataForDebug:
        PrintAndLog("data: " + str(data))

    return data


def GetDataForApprove_EtherDelta(amountForDepositApproval_wei):
    from Libraries.core import PadDataParemeter_LeftFillPadding, Remove0XfromHexString
    from Exchanges.etherDelta import Contract_Exchange as Contract_Exchange_EtherDelta

    data = ""
    addNewLinesForDebug = False
    printDataForDebug = False
    # Method
    data += "0x095ea7b3"
    if addNewLinesForDebug:
        data += "\n"

    # spender
    spender = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(Contract_Exchange_EtherDelta))
    if printDataForDebug:
        PrintAndLog("spender: " + spender)
    data += spender
    if addNewLinesForDebug:
        data += "\n"

    # value
    value = PadDataParemeter_LeftFillPadding("%x" % int(float(amountForDepositApproval_wei)))
    if printDataForDebug:
        PrintAndLog("value: " + value)
    data += value
    if addNewLinesForDebug:
        data += "\n"

    if printDataForDebug:
        PrintAndLog("data: " + data)

    return data


def GetDataForDepositERC20Token_EtherDelta(amountForDepositApproval_wei, tokenAddress):
    from Libraries.core import PadDataParemeter_LeftFillPadding, Remove0XfromHexString

    data = ""
    addNewLinesForDebug = False
    printDataForDebug = False
    # Method
    data += "0x338b5dea"
    if addNewLinesForDebug:
        data += "\n"

    # token
    token = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(tokenAddress))
    if printDataForDebug:
        PrintAndLog("token: " + token)
    data += token
    if addNewLinesForDebug:
        data += "\n"

    # amount
    amount = PadDataParemeter_LeftFillPadding("%x" % int(float(amountForDepositApproval_wei)))
    if printDataForDebug:
        PrintAndLog("amount: " + amount)
    data += amount
    if addNewLinesForDebug:
        data += "\n"

    if printDataForDebug:
        PrintAndLog("data: " + data)

    return data


def GetDataForDepositEther_EtherDelta():
    data = "0xd0e30db0"
    return data


def GetDataForWithdrawalERC20Token_EtherDelta(amountForWithdrawal_wei, tokenAddress):
    from Libraries.core import PadDataParemeter_LeftFillPadding, Remove0XfromHexString

    data = ""
    addNewLinesForDebug = False
    printDataForDebug = False
    # Method
    data += "0x9e281a98"
    if addNewLinesForDebug:
        data += "\n"

    # token
    token = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(tokenAddress))
    if printDataForDebug:
        PrintAndLog("token: " + token)
    data += token
    if addNewLinesForDebug:
        data += "\n"

    # amount
    amount = PadDataParemeter_LeftFillPadding("%x" % int(float(amountForWithdrawal_wei)))
    if printDataForDebug:
        PrintAndLog("amount: " + amount)
    data += amount
    if addNewLinesForDebug:
        data += "\n"

    if printDataForDebug:
        PrintAndLog("data: " + data)

    return data


def GetDataForSendingTokensToAddress(amount_wei, toAddress):
    from Libraries.core import PadDataParemeter_LeftFillPadding, Remove0XfromHexString

    # PrintAndLog("GetDataForSendingTokensToAddress amount_wei: " + amount_wei)
    # PrintAndLog("GetDataForSendingTokensToAddress toAddress: " + toAddress)
    # MethodID: 0xa9059cbb
    # [0]:000000000000000000000000da79f2b9db7ae9fc5e308e81dc1e1992245c4b41
    # [1]:000000000000000000000000000000000000000000000000002386f26fc10000

    data = ""
    addNewLinesForDebug = False
    printDataForDebug = False
    # Method
    data += "0xa9059cbb"
    if addNewLinesForDebug:
        data += "\n"

    # toAddress
    toAddress = PadDataParemeter_LeftFillPadding(Remove0XfromHexString(toAddress))
    if printDataForDebug:
        PrintAndLog("toAddress: " + toAddress)
    data += toAddress
    if addNewLinesForDebug:
        data += "\n"

    # amount
    amount = PadDataParemeter_LeftFillPadding("%x" % int(float(amount_wei)))
    if printDataForDebug:
        PrintAndLog("amount: " + amount)
    data += amount
    if addNewLinesForDebug:
        data += "\n"

    if printDataForDebug:
        PrintAndLog("data: " + data)

    return data


def GetTxReceiptBlockStatus_GivenStatusFromTxReceipt(status_int):
    if status_int == 1:
        return TxReceiptBlockStatus.Succeeded
    else:
        return TxReceiptBlockStatus.RevertedOrErrored
