from threading import Lock
import datetime


class DictWithValueList:
    Lock_MyDict = None
    MyDict = None

    def __init__(self):
        self.Lock_MyDict = Lock()
        self.MyDict = {}

    def AddItemToDict(self, key, item):
        self.Lock_MyDict.acquire()
        try:
            if key not in self.MyDict:
                # init to empty array
                self.MyDict[key] = []

            # Append the item to the value list
            self.MyDict[key].append(item)

        finally:
            self.Lock_MyDict.release()

    def SetValueList(self, key, valueList):
        self.Lock_MyDict.acquire()
        try:
            self.MyDict[key] = valueList

        finally:
            self.Lock_MyDict.release()

    def RemoveItemFromDict(self, key, item):
        self.Lock_MyDict.acquire()
        try:
            self.MyDict[key].remove(item)

            # If this key no longer has any items in the array
            if len(self.MyDict[key]) <= 0:
                # nuke it
                del self.MyDict[key]

        finally:
            self.Lock_MyDict.release()

    def RemoveItemFromDictsValueArray(self, key, item):
        self.Lock_MyDict.acquire()
        try:
            if key not in self.MyDict:
                raise Exception("key " + str(key) + " not found in dict")

            if item not in self.MyDict[key]:
                raise Exception("item " + str(item) + " not found in value list for key " + str(key))

            # Remove the item to the value list
            self.MyDict[key].remove(item)

        finally:
            self.Lock_MyDict.release()

    def GetItems(self, key):
        returnValues = None

        self.Lock_MyDict.acquire()
        try:
            returnValues = self.MyDict[key]

        finally:
            self.Lock_MyDict.release()

        return returnValues

    def GetKeys(self):
        keysList = None

        self.Lock_MyDict.acquire()
        try:
            keysList = list(self.MyDict.keys())

        finally:
            self.Lock_MyDict.release()

        return keysList

    def GetSumOfTotalValuesInAllKeys(self):
        sum = 0
        for myKey in self.MyDict.keys():
            sum += len(self.MyDict[myKey])

        return sum


class DictWithZeroInitializedValue:
    Lock_MyDict = None
    MyDict = None

    def __init__(self):
        self.Lock_MyDict = Lock()
        self.MyDict = {}

    def IncrementValueInDict(self, key, amountToAdd):
        self.Lock_MyDict.acquire()
        try:
            if key not in self.MyDict:
                # init to zero
                self.MyDict[key] = 0

            # increment value
            self.MyDict[key] += amountToAdd

        finally:
            self.Lock_MyDict.release()

    def DecrementValueInDict(self, key, amountToSubtract):
        self.Lock_MyDict.acquire()
        try:
            if key not in self.MyDict:
                # init to zero
                self.MyDict[key] = 0

            # increment value
            self.MyDict[key] -= amountToSubtract

        finally:
            self.Lock_MyDict.release()

    def RemoveItemFromDict(self, key, item):
        self.Lock_MyDict.acquire()
        try:
            self.MyDict[key].remove(item)

            # If this key no longer has any items in the array
            if len(self.MyDict[key]) <= 0:
                # nuke it
                del self.MyDict[key]

        finally:
            self.Lock_MyDict.release()

    # TODO this should be renamed to GetValue... but it will be very hard to find replace so I may just leave it
    def GetItems(self, key, doReturnZeroIfKeyNotInDict=False):
        if doReturnZeroIfKeyNotInDict and not self.IsKeyInDict(key):
            return 0

        returnValues = None
        self.Lock_MyDict.acquire()
        try:
            returnValues = self.MyDict[key]

        finally:
            self.Lock_MyDict.release()

        return returnValues

    def SetValue(self, key, value):
        self.Lock_MyDict.acquire()
        try:
            self.MyDict[key] = value

        finally:
            self.Lock_MyDict.release()

    def IsKeyInDict(self, key):
        # Don't bother locking just to check for a key
        if key in self.MyDict:
            return True
        else:
            return False

    def GetKeys(self):
        keysList = None

        self.Lock_MyDict.acquire()
        try:
            keysList = list(self.MyDict.keys())

        finally:
            self.Lock_MyDict.release()

        return keysList


class DictWithValue:
    Lock_MyDict = None
    MyDict = None

    def __init__(self):
        self.Lock_MyDict = Lock()
        self.MyDict = {}

    def SetValue(self, key, value):
        self.Lock_MyDict.acquire()
        try:
            self.MyDict[key] = value

        finally:
            self.Lock_MyDict.release()

    def RemoveKeyFromDict(self, key):
        self.Lock_MyDict.acquire()
        try:
            del self.MyDict[key]

        finally:
            self.Lock_MyDict.release()

    def GetValue(self, key):
        if key not in self.MyDict:
            return None

        returnValue = None
        self.Lock_MyDict.acquire()
        try:
            returnValue = self.MyDict[key]

        finally:
            self.Lock_MyDict.release()

        return returnValue


class DictWithValueAndAge:
    Lock_MyDict = None
    MyDict = None

    def __init__(self):
        self.Lock_MyDict = Lock()
        self.MyDict = {}

    def SetValue(self, key, value):
        self.Lock_MyDict.acquire()
        try:
            self.MyDict[key] = (value, datetime.datetime.now())

        finally:
            self.Lock_MyDict.release()

    def RemoveKeyFromDict(self, key):
        self.Lock_MyDict.acquire()
        try:
            del self.MyDict[key]

        finally:
            self.Lock_MyDict.release()

    def GetValue(self, key):
        if key not in self.MyDict:
            return None

        returnValue = None
        self.Lock_MyDict.acquire()
        try:
            valueTuple = self.MyDict[key]
            dataAge_seconds = (datetime.datetime.now() - valueTuple[1]).total_seconds()
            returnValue = (valueTuple[0], dataAge_seconds)

        finally:
            self.Lock_MyDict.release()

        return returnValue

    def GetKeys(self):
        keysList = None

        self.Lock_MyDict.acquire()
        try:
            keysList = list(self.MyDict.keys())

        finally:
            self.Lock_MyDict.release()

        return keysList
