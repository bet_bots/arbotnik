# TODO REMOVEME

# from Libraries.loggingConfig import InitLogging, PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader
# import Libraries.core
# import Libraries.nodes
#
# import websocket
# import requests
# import traceback
# import json
# import threading
# import time
# import datetime
# from random import randint
#
# WebsocketClient = None
# URL_WS_Base = "wss://mainnet.infura.io/ws"
# # URL_WS_Base = "ws://ec2-35-183-152-34.ca-central-1.compute.amazonaws.com:8546"
#
# Filter_eth_newBlockFilter = None
# Datetime_Before = datetime.datetime.now()
# LastBlockNumber = None
# SubscriptionId_NewHeads = None
#
# MemPoolTransaction_UnknowData = '~UNKNOWN_DATA~'
#
#
# class WebsocketClientObject(object):
#     ws = None
#
#     def __init__(self, interval=10):
#         self.interval = interval
#
#         thread = threading.Thread(target=self.run, args=())
#         thread.daemon = True
#         thread.start()
#
#         # thread_keepAlive = threading.Thread(target=self.keepAlive, args=())
#         # thread_keepAlive.daemon = True
#         # thread_keepAlive.start()
#
#     def run(self):
#         """ Method that runs forever """
#
#         websocket.enableTrace(False)
#         websocket.http_proxy_host = URL_WS_Base
#         self.ws = websocket.WebSocketApp(URL_WS_Base,
#                                          on_message=self.on_message,
#                                          on_error=self.on_error,
#                                          on_close=self.on_close)
#         while True:
#             self.ws.run_forever()
#             self.ws.run_forever(ping_interval=70, ping_timeout=10)
#             PrintAndLog("I was disconnected from websockets!  Sleeping a bit then trying again")
#             time.sleep(30)
#
#     # def keepAlive(self):
#     #     while True:
#     #         time.sleep(1)
#     #         try:
#     #             # keep alive for the server
#     #             self.ws.send('ping')
#     #         except:
#     #             PrintAndLogError("exception in WebsocketClientObject: " + traceback.format_exc())
#     #             pass
#     #
#     #         time.sleep(40)
#
#     def on_message(self, ws, message):
#         global Filter_eth_newBlockFilter
#         global Datetime_Before
#         global LastBlockNumber
#         global SubscriptionId_NewHeads
#
#         # PrintAndLog("Websockets Node, data received: " + message[0:90] + "....." + message[-25:])
#         # PrintAndLog("Websockets Node, data received: " + message)
#         try:
#             jData = json.loads(message)
#
#             if 'id' in jData and int(jData['id']) == 1:
#                 Filter_eth_newBlockFilter = jData['result']
#                 PrintAndLog("Subscribed to Filter_eth_newBlockFilter: " + str(Filter_eth_newBlockFilter))
#
#             elif 'id' in jData and int(jData['id']) == 2:
#                 SubscriptionId_NewHeads = jData['result']
#                 PrintAndLog("Subscribed to SubscriptionId_NewHeads: " + str(SubscriptionId_NewHeads))
#
#             else:
#                 if 'params' in jData and 'subscription' in jData['params'] and jData['params']['subscription'] == SubscriptionId_NewHeads:
#                     newBlockNumber = jData['params']['result']['number']
#                     newBlockHash = jData['params']['result']['hash']
#                     PrintAndLog("newBlockNumber = " + str(newBlockNumber) + ", newBlockHash = " + str(newBlockHash))
#
#                     payload = {
#                         "id": randint(0, 99999999999999),
#                         "jsonrpc": "2.0",
#                         "method": "eth_getBalance",
#                         "params": [
#                             "0x755468ba98e5dcf979452044b8278c0858d8ad4a",
#                             newBlockNumber
#                             # Libraries.core.ConvertIntToHex(Libraries.core.ConvertHexToInt(newBlockNumber) - 1)
#                         ]
#                     }
#                     response = requests.post(Libraries.nodes.URL_RemoteNode.joeyz_VA.value, data=json.dumps(payload), headers=Libraries.core.Headers,
#                                              timeout=Libraries.core.RequestTimeout_seconds)
#                     if response.ok:
#                         responseData = response.content
#                         otherjData = json.loads(responseData)
#                         PrintAndLog("GetEtherBalance otherjData = " + str(otherjData))
#                         PrintAndLog("Succeeded!")
#
#                     else:
#                         PrintAndLog("Failed!")
#
#                 elif len(jData['result']) > 0:
#                     PrintAndLog("Got something else, : " + str(jData) + " duration = " + str((datetime.datetime.now() - Datetime_Before).total_seconds()) + " seconds, " + str(
#                         datetime.datetime.now()))
#
#             # currentBlockNumber = jData['result']
#             # # PrintAndLog("currentBlockNumber = " + str(currentBlockNumber))
#             #
#             # if LastBlockNumber and LastBlockNumber != currentBlockNumber:
#             #     PrintAndLog("New block: " + str(currentBlockNumber) + " duration = " + str((datetime.datetime.now() - Datetime_Before).total_seconds()) + " seconds, " + str(datetime.datetime.now()))
#             #
#             # LastBlockNumber = currentBlockNumber
#
#         except:
#             PrintAndLogError("exception in WebsocketClientObject: " + traceback.format_exc())
#             pass
#
#     def on_error(self, ws, error):
#         print(error)
#
#     def on_close(self, ws):
#         message = "Node: Disconnected from websockets."
#         PrintAndLogError(message)
#         Libraries.core.API_PostOperatorNotification(message)
#
#
# def ConnectWebSocketClient():
#     global WebsocketClient
#     global Filter_eth_newBlockFilter
#
#     WebsocketClient = WebsocketClientObject()
#     time.sleep(1.0)
#
#     Filter_eth_newBlockFilter = None
#     WebsocketClient.ws.send('{"jsonrpc":"2.0","method":"eth_newBlockFilter","params":[],"id":1}')
#     # WebsocketClient.ws.send('{"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":73}')
#
#     # Libraries.node.WebsocketClient.ws.send('{"id": 5, "method": "eth_subscribe", "params": ["newPendingTransactions"]}')
#     # Libraries.node.WebsocketClient.ws.send('{"id": 5, "method": "parity_subscribe", "params": ["newPendingTransactions"]}')
#
#     # Libraries.node.WebsocketClient.ws.send('{"id": 5, "method": "eth_subscribe", "params": ["eth_blockNumber"]}')
#     # Libraries.node.WebsocketClient.ws.send('{"id": 5, "method": "parity_subscribe", "params": ["eth_blockNumber"]}')
#
#     Libraries.node.WebsocketClient.ws.send('{"id": 2, "method": "eth_subscribe", "params": ["newHeads"]}')
