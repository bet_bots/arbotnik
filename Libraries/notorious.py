from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings

KnownProxyContractAddresses = [
    '0x8018280076d7fa2caa1147e441352e8a89e1ddbe',
    '0x693c188e40f760ecf00d2946ef45260b84fbc43e',
]
# Make sure it's all lowercase for string comparison purposes
KnownProxyContractAddresses = ConvertListOfStringsToLowercaseListOfStrings(KnownProxyContractAddresses)

KnownOperatorAddresses = [
    '0xff49432a1ea8ac6d12285099ba426d1f16f23c8d',
    '0xff28319a7cd2136ea7283e7cdb0675b50ac29dd2',
    '0xff1b9745f68f84f036e5e92c920038d895fb701a',
    '0xff59364722a4622a8d33623548926375b1b07767',
    '0xff3769cdbd31893ef1b10a01ee0d8bd1f3773899',
    '0xff49432a1ea8ac6d12285099ba426d1f16f23c8d',
    '0xff6d62bc882c2fca5af5cbfe1e6c10b97ba251a4',
    '0xff7baf00edf054e249e9f498aa51d1934b8d3526',
    '0xff6d62bc882c2fca5af5cbfe1e6c10b97ba251a4',
    '0xffcdfd98c455c29818697ab2eeafccbc4e59fd3d',
    '0xff1b9745f68f84f036e5e92c920038d895fb701a',
    '0xffbfdb803d38d794b5785ee0ac09f83b429d11b5',
    '0xffe1c5696d924438fba5274d7b5d8ffa29239a6f',
    '0xffdce1ae835d35bb603c95163e510bb2604a1a41',
]
# Make sure it's all lowercase for string comparison purposes
KnownOperatorAddresses = ConvertListOfStringsToLowercaseListOfStrings(KnownOperatorAddresses)
