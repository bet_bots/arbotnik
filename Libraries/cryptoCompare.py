from Libraries.loggingConfig import PrintAndLog

import Libraries.core
import Libraries.utils

import requests
import datetime
import json
import time

URL_Base = 'https://min-api.cryptocompare.com/'
API_Key = '5e7e4b2db0d9d9f45478fceb5e45abae52683688df95146bea080b7619a18f0f'

# Rate limit is 50 calls per second, but i'll make it lower to be safe
RateLimit_callsPerSecond = 25


def API_GetMarkets(symbolsList):
    # url = 'https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,BAT&tsyms=USD,ETH'
    symbolsString = ''
    for i in range(len(symbolsList)):
        symbol = symbolsList[i]
        symbolsString += symbol.upper()
        if i < (len(symbolsList) - 1):
            symbolsString += ','

    PrintAndLog("symbolsString = " + str(symbolsString))
    url = URL_Base + 'data/pricemulti?fsyms=' + str(symbolsString) + '&tsyms=USD,ETH,BTC'

    PrintAndLog("API_GetMarkets url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("API_GetMarkets jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()

    return None


def API_GetHistory(symbol, currencyOfReturnPrice, limit, earliestTimeStampReceived):
    url = URL_Base + "data/histohour?api_key=" + str(API_Key) + "&fsym=" + str(symbol.upper()) + "&tsym=" + str(currencyOfReturnPrice.upper())

    if limit:
        url += "&limit=" + str(limit)

    if earliestTimeStampReceived:
        url += "&toTs=" + str(earliestTimeStampReceived)

    PrintAndLog("API_GetHistory url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetHistory jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()

    return None


def API_GetMaxHistory(symbol, currencyOfReturnPrice, datetime_begin=None):
    # limit = 10
    limit = 2000

    if not datetime_begin:
        datetime_begin = datetime.datetime(2018, 1, 1)
        PrintAndLog("Overriding datetime_begin to " + str(datetime_begin))
    else:
        PrintAndLog("Overriding datetime_begin = " + str(datetime_begin))

    earliestTimeStampReceived = None

    combinedResults = []
    doLoop = True
    while doLoop:
        # PrintAndLog("API_GetMaxHistory with limit = " + str(limit) + ", earliestTimeStampReceived = " + str(earliestTimeStampReceived))
        jData = API_GetHistory(symbol, currencyOfReturnPrice, limit, earliestTimeStampReceived)
        # PrintAndLog("jData = " + str(jData))
        earliestTimeStampReceived = jData['Data'][0]['time']
        # PrintAndLog("Setting earliestTimeStampReceived to " + str(earliestTimeStampReceived))

        jData['Data'].reverse()
        combinedResults += jData['Data']

        for data in jData['Data']:
            unixEpochTime_seconds = data['time']
            dataDateTime = Libraries.core.ConvertUnixEpochToDateTime_GivenUnixEpochInSeconds(unixEpochTime_seconds)
            # PrintAndLog("dataDateTime = " + str(dataDateTime) + ", unixEpochTime_seconds = " + str(unixEpochTime_seconds))

            if dataDateTime < datetime_begin:
                PrintAndLog("Exiting loop because we found data older than our boundry. dataDateTime = " + str(dataDateTime) + ", datetime_begin = " + str(datetime_begin))
                doLoop = False
                break

        # Sleep to obey rate limit
        time.sleep(1 / float(RateLimit_callsPerSecond))

    return combinedResults


def API_GetPriceDuringRangeInHistory_GivenDateTimeFromHistory(datetime_begin, asset, currencyOfReturnPrice):
    resultDict = {}

    dataList = API_GetMaxHistory(asset, currencyOfReturnPrice, datetime_begin)
    # PrintAndLog("dataList = " + str(dataList))
    for data in dataList:
        unixTimeStamp_seconds = data['time']
        high = float(data['high'])
        low = float(data['low'])
        price = (high + low) / 2

        dateTimeOfPrice = Libraries.core.ConvertUnixEpochToDateTime_GivenUnixEpochInSeconds(unixTimeStamp_seconds)
        # Uses hours, so this will use hourly market prices
        dateTimeOfPrice = dateTimeOfPrice.replace(minute=0, second=0, microsecond=0)

        resultDict[str(dateTimeOfPrice)] = price

    return resultDict
