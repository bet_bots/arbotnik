import datetime
import threading
import traceback
import jsonpickle
import zmq
import json

from Libraries.exchanges import GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken, ExchangeName_0xMesh, ExchangeName_HidingBook
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError
import Libraries.nodes
import Libraries.quoteTokenProcesses
import Libraries.orderAggregator
import Libraries.defaults
from Libraries.market import GetTokenObjectGivenTokenAddress
from Libraries.executeOnInterval import IsTimeToExecute

URL_Server = 'tcp://127.0.0.1:5555'

Server = None
Client = None

ResultIdDict = {}

Lock_ZmqClient = threading.Lock()


def StartServer_InBackgroundThread():
    t = threading.Thread(target=StartServer, args=())
    t.start()


def StartServer():
    global Server

    PrintAndLog_FuncNameHeader("Starting ZMQ server on main process to communicate with sub processes")
    context = zmq.Context()
    Server = context.socket(zmq.REP)
    Server.bind(URL_Server)
    while True:
        method = "Unknown"
        try:
            msg_json = Server.recv_json()

            method = msg_json['method'].lower()
            if Libraries.defaults.VerboseLogging_ZMQ_LogMessages:
                PrintAndLog_FuncNameHeader('message received, msg_json = ' + str(msg_json))

            # if method == 'post_ratesGraph'.lower():
            #     HandlePayload_RatesGraph(Server, msg_json['payload'])
            if method == 'get_quoteTokensToSpendList'.lower():
                HandlePayload_QuoteTokensToSpendList(Server)
            elif method == 'get_RemoteNodeList'.lower():
                HandlePayload_RemoteNodeList(Server, msg_json['payload'])
            elif method == 'result_getPrices'.lower():
                HandlePayload_Result_GetPrices(Server, msg_json['payload'])
            elif method == 'get_0xOrderAggregatorData'.lower():
                HandlePayload_0xOrderAggregatorData(Server, msg_json['payload'])
            else:
                # I have to respond to every message or else ZMQ will break
                Server.send_json({'message': 'Invalid method'})

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            # I have to respond to every message or else ZMQ will break
            errorMessage = "exception when handling payload on ZMQ method " + str(method)
            Server.send_json({'message': errorMessage})
            PrintAndLogError(errorMessage + ": " + traceback.format_exc())


def HandlePayload_Result_GetPrices(server, payload_jData):
    global ResultIdDict

    # PrintAndLog_FuncNameHeader("Handling response for payload_jData of len " + str(len(payload_jData)))
    # Store the data we just received in the ResultIdDict
    ResultIdDict[payload_jData['resultId']] = payload_jData

    # I have to respond or else it crashes
    server.send_json({'message': 'OK'})


def HandlePayload_QuoteTokensToSpendList(server):
    import Exchanges.keeperDAO

    # Send dict in response
    server.send_json(Exchanges.keeperDAO.ManuallyAddedTradeAmountArrayDict)


def HandlePayload_RemoteNodeList(server, payload_jData):
    # make a local copy of list so we can modify it
    nodeList = Libraries.nodes.RemoteNodeList_QuoteTokenProcessSelection.copy()

    seed = int(payload_jData['seed'])
    # TODO add seedLength to the other side of the call
    # seedLength = int(payload_jData['seedLength'])
    maxNumOfNodesPerProcess = Libraries.defaults.MaxNumOfNodesCalledPerRequest_QuoteTokenProcess
    numOfNodesPerProcess = min(maxNumOfNodesPerProcess, len(nodeList))
    # seedLength = 3
    # seed will be either  0, 1, or 2
    # numOfNodesPerProcess let's assume it's 2
    # array length will be numOfNodesPerProcess * seedLength  so in this case 6
    # starting index will be seed * numOfNodesPerProcess
    startingIndex = seed * numOfNodesPerProcess
    # PrintAndLog_FuncNameHeader("Determining seeds to use: seed = " + str(seed) + ", numOfNodesPerProcess = " + str(
    #     numOfNodesPerProcess) + ", startingIndex = " + str(startingIndex) + ", nodeList = " + str(nodeList))
    nodeList_toReturn = []
    for i in range(0, numOfNodesPerProcess):
        indexedSeed = startingIndex + i
        # PrintAndLog_FuncNameHeader("indexedSeed = " + str(indexedSeed))
        moddedSeed = indexedSeed % len(nodeList)
        # PrintAndLog_FuncNameHeader("moddedSeed = " + str(moddedSeed) + " should always be less than len(nodeList) of " + str(len(nodeList)))
        # return the value since the enum object cannot be serialized/deserialized
        # this means on the other side I will need to convert this value to the actual object
        nodeList_toReturn.append(nodeList[moddedSeed].value)

    # PrintAndLog_FuncNameHeader("nodeList_toReturn = " + str(nodeList_toReturn))

    response_jData = {'nodeList': nodeList_toReturn}
    # Send dict in response
    server.send_json(response_jData)


# TODO update for hidingbook
def HandlePayload_0xOrderAggregatorData(server, payload_jData):
    response_jData = {}
    response_jData['orderAggregators'] = {}
    # Serialize the objects we need from Libraries.orderAggregator so we can send them as json
    response_jData['Libraries.orderAggregator.AmountFilledValueDict_0x'] = jsonpickle.encode(Libraries.orderAggregator.AmountFilledValueDict_0x)
    response_jData['Libraries.orderAggregator.BalanceValueDict_0x'] = jsonpickle.encode(Libraries.orderAggregator.BalanceValueDict_0x)
    response_jData['Libraries.orderAggregator.AllowanceValueDict_0xv2'] = jsonpickle.encode(Libraries.orderAggregator.AllowanceValueDict_0xv2)
    response_jData['Libraries.orderAggregator.AllowanceValueDict_0xv4'] = jsonpickle.encode(Libraries.orderAggregator.AllowanceValueDict_0xv4)

    # Support all 0x orderbook style exchanges
    exchangeNames = []
    if ExchangeName_0xMesh in Libraries.exchanges.ExchangeDict:
        exchangeNames.append(ExchangeName_0xMesh)
    if ExchangeName_HidingBook in Libraries.exchanges.ExchangeDict:
        exchangeNames.append(ExchangeName_HidingBook)

    for exchangeName in exchangeNames:
        response_jData['orderAggregators'][exchangeName] = {}

        # Determine which tokens I need to get orderAggregatorData for
        quoteToken = payload_jData['quoteToken']
        # Only send data on tokens we care about
        baseTokenList = GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken(exchangeName, quoteToken)

        for baseToken in baseTokenList:
            # PrintAndLog_FuncNameHeader("baseToken = " + str(baseToken))
            token = GetTokenObjectGivenTokenAddress(baseToken)
            # token should never be None, but it currently can be because i've keyed tokens by symbol instead of address
            # Once I refactor that None should never be possible
            if not token:
                continue

            orderDict_bids_0x = token.GetCopyOfAllOrders_Bids_FromTheseOrderAggregators(quoteToken, False, [exchangeName])
            orderDict_asks_0x = token.GetCopyOfAllOrders_Asks_FromTheseOrderAggregators(quoteToken, False, [exchangeName])
            # Only include it if there's actually order data on the books
            if len(orderDict_bids_0x) > 0 or len(orderDict_asks_0x) > 0:
                response_jData['orderAggregators'][exchangeName][baseToken.lower()] = {}
                response_jData['orderAggregators'][exchangeName][baseToken.lower()]['orderDict_bids_0x'] = orderDict_bids_0x
                response_jData['orderAggregators'][exchangeName][baseToken.lower()]['orderDict_asks_0x'] = orderDict_asks_0x

    # PrintAndLog_FuncNameHeader("response_jData = " + str(response_jData))
    # Send dict in response
    server.send_json(response_jData)


def StartClient_InBackgroundThread():
    t = threading.Thread(target=StartClient, args=())
    t.start()


def StartClient():
    global Client

    PrintAndLog_FuncNameHeader("Starting ZMQ client on subprocess to communicate with main process")
    context = zmq.Context()
    Client = context.socket(zmq.REQ)
    Client.connect(URL_Server)


def SendData_FromSubProcessToMainProcess(jData):
    from Libraries.core import API_PostOperatorNotification

    global Client
    global Lock_ZmqClient

    # I must thread lock before I send a message because ZMQ is not thread safe, it requires 1 request and 1 response in exactly that order.
    response_json = None
    before = datetime.datetime.now()
    Lock_ZmqClient.acquire()
    try:
        duration_s = (datetime.datetime.now() - before).total_seconds()
        message = "I had to wait " + str(round(duration_s, 3)) + " seconds for the Lock_ZmqClient. Now I can finally make my call"
        # If the wait is a long time
        if duration_s > 0.3:
            # I periodically want to know because something may be broken or inefficient
            if IsTimeToExecute("Lock_ZmqClient is broken or inefficient", 300):
                API_PostOperatorNotification(message)
        else:
            PrintAndLog_FuncNameHeader(message)

        if Libraries.defaults.VerboseLogging_ZMQ_LogMessages:
            PrintAndLog_FuncNameHeader("Sending data from client to server, jData = " + str(jData))

        before = datetime.datetime.now()
        Client.send_json(jData)
        # I have to wait for a response or else it crashes
        response_json = Client.recv_json()
        duration_s = (datetime.datetime.now() - before).total_seconds()
        if Libraries.defaults.VerboseLogging_ZMQ_Delays:
            PrintAndLog_FuncNameHeader("Client.send_json and Client.recv_json completed with duration_s = " + str(duration_s) + " seconds")

    finally:
        Lock_ZmqClient.release()

    return response_json


def GetAllPricesResults(resultId):
    global ResultIdDict

    # PrintAndLog_FuncNameHeader("Checking ResultIdDict for resultId " + str(resultId))
    if resultId in ResultIdDict:
        # Found the resultId we're looking for
        return ResultIdDict[resultId]
    else:
        # If we've made it this far, the resultId we're looking for isn't in the ResultIdDict and wasn't found in the EventQueue_MessagesToMainProcess
        return None


def RemoveOldResultsFromResultIdDict():
    # from Libraries.core import API_PostOperatorNotification

    global ResultIdDict

    try:
        keysToDelete = []
        PrintAndLog_FuncNameHeader("Begin with " + str(len(ResultIdDict)) + " keys in ResultIdDict")
        for key in ResultIdDict.copy():
            # jData = {
            #                     'method': 'result_getPrices',
            #                     'payload': {
            #                         'resultId': resultId,
            #                         'timestamp': Libraries.core.GetUnixEpochTime_FromDateTime_seconds(datetime.datetime.now()),
            #                         'exchangesResultsDict': exchangesResultsDict,
            #                         'expectedResultsDict': expectedResultsDict,
            #                         'Libraries.ratesGraph.RatesGraph_Dict__Process_QuoteToken': Libraries.ratesGraph.RatesGraph_Dict__Process_QuoteToken[blockNumber],
            #                         'Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames__Process_QuoteToken': Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber],
            #                         'Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens__Process_QuoteToken': Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber],
            #                     }
            #                 }

            # Extract the timestamp_datetime from the payload
            payload = ResultIdDict[key]
            timestamp_datetime = datetime.datetime.fromtimestamp(payload['timestamp'])
            # Determine how old this data is
            age_s = (datetime.datetime.now() - timestamp_datetime).total_seconds()
            # If this result is old, let's remove it since we don't need it anymore
            # TODO put this time out in default?
            # TODO this should be in terms of blocks, not seconds..
            threshold_s = 200
            if age_s > threshold_s:
                keysToDelete.append(key)
                message = "(processCommunication) Removing an old result that was lingering for over " + str(threshold_s) + " seconds. key = " + str(key)
                PrintAndLog_FuncNameHeader(message)
                # # TODO remove this once I know this is working properly
                # API_PostOperatorNotification(message)

        PrintAndLog_FuncNameHeader("Deleting " + str(len(keysToDelete)) + " keys from ResultIdDict. Is this working properly?")
        for key in keysToDelete:
            del ResultIdDict[key]

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception in RemoveOldResultsFromResultIdDict: " + traceback.format_exc())
