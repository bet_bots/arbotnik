from Libraries.loggingConfig import PrintAndLog

import Libraries.core

import requests
import json

URL_Base = 'https://api.coingecko.com/api/v3/'


def API_GetMarkets_Safe(vs_currency, numOfTopMarketsByRank):
    per_page_maximum = 200
    per_page = min(numOfTopMarketsByRank, per_page_maximum)
    page = 1
    count = 0
    returnList = []
    # Make the call as many time as needed while setting the page based on the per_page rate limit cap which is currently 250.  So stay under 250.
    while count < numOfTopMarketsByRank:
        returnList += API_GetMarkets(vs_currency, per_page, page)
        count += per_page
        page += 1

    # PrintAndLog("API_GetMarkets: returnList = " + str(returnList))
    return returnList


def API_GetMarkets(vs_currency, per_page=100, page=1):
    url = URL_Base + "coins/markets?vs_currency=" + vs_currency + "&per_page=" + str(per_page) + "&page=" + str(page)
    PrintAndLog("API_GetMarkets url = " + str(url))

    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetMarkets jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()

    return None


def API_GetHistory(id, dd_string, mm_string, yyyy_string):
    date_string = dd_string + "-" + mm_string + "-" + yyyy_string
    url = URL_Base + "coins/" + str(id) + "/history?date=" + date_string + "&localization=false"
    # PrintAndLog("API_GetHistory url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetHistory jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetMaxHistory(id, vs_currency):
    # https://api.coingecko.com/api/v3/coins/ethereum/market_chart?vs_currency=usd&days=max
    url = URL_Base + "coins/" + str(id) + "/market_chart?vs_currency=" + vs_currency + "&days=max"
    # PrintAndLog("API_GetMaxHistory url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetMaxHistory jData = " + str(jData))
        return jData['prices']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetPriceAtPointInHistory_GivenDateTime(datetime, asset, currencyOfReturnPrice):
    jData = API_GetHistory(asset, str(datetime.day), str(datetime.month), str(datetime.year))
    return jData['market_data']['current_price'][currencyOfReturnPrice]


def API_GetPriceDuringRangeInHistory_GivenDateTimeRange(datetime_begin, datetime_end, asset, currencyOfReturnPrice):
    resultDict = {}

    priceArray = API_GetMaxHistory(asset, currencyOfReturnPrice)
    for values in priceArray:
        unixTimeStamp_milliseconds = values[0]
        price = values[1]

        dateTimeOfPrice = Libraries.core.ConvertUnixEpochToDateTime_GivenUnixEpochInMilliseconds(unixTimeStamp_milliseconds)
        dateTimeOfPrice = dateTimeOfPrice.replace(hour=0, minute=0, second=0, microsecond=0)

        resultDict[str(dateTimeOfPrice)] = price

    return resultDict
