import json
import traceback
from random import randint
from enum import Enum
import jsonpickle
import websocket
import time
import threading

import Libraries.core
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_Mempool
from Libraries.executeOnInterval import IsTimeToExecute

# ApiKey_Alchemy = 'Oi7mfoPmhMnKK3kSA2Xlqaudsk1RC5_w'  # VolleyFire
# ApiKey_Alchemy = 'of0_8k8qofGv4fpP3I4HhKm4awE8ECLX'  # autista
ApiKey_Alchemy = 'C9fiCgCJuHUswK-Rfs9ctmJk8FH_CYpt'  # eevilpoptart

URL_WS_Base = 'wss://eth-mainnet.ws.alchemyapi.io/ws/' + ApiKey_Alchemy
# wscat -c “wss://eth-mainnet.ws.alchemyapi.io/v2/<YOUR_API_KEY>
# {"jsonrpc": "2.0", "id": 100, "method": "eth_subscribe", "params": ["alchemy_newFullPendingTransactions"]}


WebsocketClient = None

LatestBlockNumberDict = {}

NodeType = "Alchemy"


class SubscriptionType(Enum):
    Mempool = "Mempool"


def ConnectWebSocketClient():
    global WebsocketClient
    WebsocketClient = WebsocketClientObject()


class WebsocketClientObject(object):
    ws = None
    nodeName = None
    # When we subscribe to a feature, we populate this dict with the id used in the subscribe payload
    # When we receive a message via websockets we'll use that id to match with the subscriptionId
    subscriptionPayloadIdDict = None
    # We need a dictionary to associate the subscriptionType with the subscriptionPayloadId
    subscriptionIdDict = None

    def __init__(self):
        self.subscriptionPayloadIdDict = {}
        self.subscriptionIdDict = {}

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        websocket.enableTrace(False)
        websocket.http_proxy_host = URL_WS_Base
        self.ws = websocket.WebSocketApp(URL_WS_Base,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)

        while True:
            try:
                # Subscribe to events
                IsTimeToExecute("SubscribeWebsockets_Mempool", 0, self.SubscribeWebsockets_Mempool, True)

                Libraries.core.API_PostOperatorNotification("Connecting to websockets: " + str(self.GetUniqueNodeName()))
                self.ws.run_forever(ping_interval=30, ping_timeout=10)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in websockets, " + str(self.GetUniqueNodeName()) + " = " + traceback.format_exc())
                pass

            PrintAndLog("Websockets disconnected! Sleeping a bit then trying again. " + str(self.GetUniqueNodeName()))
            time.sleep(30)

    def on_message(self, ws, message):
        from Libraries.mempool import HandleIncomingMempoolTx, MempoolServiceName, Key_MempoolServiceName

        global LatestBlockNumberDict

        jData = json.loads(message)
        # PrintAndLog(str(self.GetUniqueNodeName()) + ": Websockets: jData = " + str(jData))
        # PrintAndLog_Mempool(str(self.GetUniqueNodeName()) + ": Websockets: jData = " + str(jData))

        if 'id' in jData:
            # Assume this is a subscription response
            # {'jsonrpc': '2.0', 'id': 85050627463811, 'result': '0xc624794b6a328d430ab7160e3c2ec38'}

            for subscriptionType in SubscriptionType:
                if jData['id'] == self.subscriptionPayloadIdDict[subscriptionType.value]:
                    # Associate the subscriptionType with the subscriptionPayloadId
                    self.subscriptionIdDict[jData['result']] = subscriptionType.value

            # PrintAndLog("self.subscriptionIdDict = " + str(self.subscriptionIdDict))

        elif 'method' in jData and jData['method'] == 'eth_subscription':
            # Update the data to include the mempoolServiceName
            jData['params']['result'][Key_MempoolServiceName] = MempoolServiceName.Alchemy.value
            HandleIncomingMempoolTx(jData['params']['result'])

    def on_error(self, ws, error):
        PrintAndLogError(str(error))

    def on_close(self, ws):
        message = str(self.GetUniqueNodeName()) + ": Disconnected from websockets."
        PrintAndLogError(message)
        Libraries.core.API_PostOperatorNotification(message)

    def GetUniqueNodeName(self):
        return NodeType

    def SubscribeWebsockets_Mempool(self, doSleepFirst=True):
        # Give websockets time to connect
        if doSleepFirst:
            time.sleep(4)

        subscriptionPayloadId = randint(0, 99999999999999)
        # Associate the subscriptionPayloadId with the SubscriptionTypes
        self.subscriptionPayloadIdDict[SubscriptionType.Mempool.value] = subscriptionPayloadId

        payload = {
            "jsonrpc": "2.0",
            "id": subscriptionPayloadId,
            "method": "eth_subscribe",
            "params": ["alchemy_newFullPendingTransactions"],
        }
        self.SubscribeWebsocketsToEndpoint(payload)

    def SubscribeWebsocketsToEndpoint(self, subscriptionPayload):
        PrintAndLog_Mempool("Subscribing to subscriptionPayload = " + str(subscriptionPayload))
        self.ws.send(jsonpickle.encode(subscriptionPayload))
