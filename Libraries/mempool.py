import copy
import datetime
import threading
import traceback
from enum import Enum
from threading import Lock

from Libraries.executeOnInterval import IsTimeToExecute
from Libraries.loggingConfig import PrintAndLog_Mempool, PrintAndLogError, PrintAndLog_FuncNameHeader
import Libraries.defaults
import Libraries.core
import Libraries.cache
import Libraries.gasStation

MempoolTxDict = {}
Lock_MempoolTxDict = Lock()

Key_MempoolServiceName = 'mempoolServiceName'

# I need to maintain a dict of all villains I want to track in mempool
# it is not efficient to track all possible txs in mempool because there are just too many of them
# here is where I define a hard coded list of villains
# I can also programmatically add addresses to this dict as well at run time
WatchedVillainAddressDict = {}

# Notorious, he's the top villain.
# His most recent address is at the top of this group of addresses.
# The others are older addresses
WatchedVillainAddressDict['0x0000000000007f150bd6f54c40a34d7c3d5e9f56'.lower()] = True
WatchedVillainAddressDict['0xe33c8e3a0d14a81f0dd7e174830089e82f65fc85'.lower()] = True
WatchedVillainAddressDict['0x693c188e40f760ecf00d2946ef45260b84fbc43e'.lower()] = True

# This villain has a very good operation going on
# He's hiding which token he's trading by some means
WatchedVillainAddressDict['0x000000000000006f6502b7f2bbac8c30a3f67e9a'.lower()] = True

# These villains are arbitraging
WatchedVillainAddressDict['0xbbdb082325600967b0d410947ae1c5f120cc105c'.lower()] = True
WatchedVillainAddressDict['0x8a3960472b3d63894b68df3f10f58f11828d6fd9'.lower()] = True
WatchedVillainAddressDict['0xf6fb09a41fa6c18cebc1e7c6f75a8664d69e4d48'.lower()] = True
WatchedVillainAddressDict['0x000000000025d4386f7fb58984cbe110aee3a4c4'.lower()] = True
WatchedVillainAddressDict['0x762ed657b76372f8c08c6f7e0aa4170658c4ca35'.lower()] = True
WatchedVillainAddressDict['0xb958a8f59ac6145851729f73c7a6968311d8b633'.lower()] = True
WatchedVillainAddressDict['0x9799b475dec92bd99bbdd943013325c36157f383'.lower()] = True
WatchedVillainAddressDict['0x7b2ef92fdecdf4a156365eb78c9e92b44588fe84'.lower()] = True
WatchedVillainAddressDict['0xad572bba83cd36902b508e89488b0a038986a9f3'.lower()] = True
WatchedVillainAddressDict['0xcb13bc474bf3039ec61606a763d661e16f91a02d'.lower()] = True
WatchedVillainAddressDict['0x860bd2dba9cd475a61e6d1b45e16c365f6d78f66'.lower()] = True
WatchedVillainAddressDict['0x7c250ea97b92b05fa33588ffc9cf157529fb7e03'.lower()] = True
WatchedVillainAddressDict['0x6780846518290724038e86c98a1e903888338875'.lower()] = True
WatchedVillainAddressDict['0x1c073d5045b1abb6924d5f0f8b2f667b1653a4c3'.lower()] = True
WatchedVillainAddressDict['0x000000002605006ff4ad30ac969f0726821ebd7c'.lower()] = True

# These villains are Sandwiching
WatchedVillainAddressDict['0xeb46faa47a6a52519839a2e52c7b28a2db17651e'.lower()] = True

# These villains are tailgating
WatchedVillainAddressDict['0x78a55b9b3bbeffb36a43d9905f654d2769dc55e8'.lower()] = True
WatchedVillainAddressDict['0xff1b9745f68f84f036e5e92c920038d895fb701a'.lower()] = True
WatchedVillainAddressDict['0x0000007545678230df9599ce5c481022eee34e84'.lower()] = True
WatchedVillainAddressDict['0x7fb160abedb1a996594274b6014c4dc442ddb95b'.lower()] = True
WatchedVillainAddressDict['0x000000f91331681adf90ecf210e59084abbca8ef'.lower()] = True
WatchedVillainAddressDict['0x6d3a20a8c916c4d857e4e44b4c67f470303a8afe'.lower()] = True
WatchedVillainAddressDict['0xf9877131de7928beebd4daf504bd44ca346375c5'.lower()] = True
WatchedVillainAddressDict['0x5392f73b013ea80b4c5eec3ea229388809114537'.lower()] = True
WatchedVillainAddressDict['0x000000000000006f6502b7f2bbac8c30a3f67e9a'.lower()] = True
WatchedVillainAddressDict['0x86debf1adf201883d467cc9b636cc8a8658fb989'.lower()] = True
WatchedVillainAddressDict['0x7ee8ab2a8d890c000acc87bf6e22e2ad383e23ce'.lower()] = True

# Set this to True when you want to compare mempool services latency
# It outputs data comparing the performance of each service
EnableMempoolServiceCompareDict = False
MempoolServiceCompareDict = {}
MempoolServiceCompareVillainAddressList = [
    '0x7a250d5630b4cf539739df2c5dacb4c659f2488d'.lower(),
]

if EnableMempoolServiceCompareDict:
    # Make sure they're all in the watch list
    for address in MempoolServiceCompareVillainAddressList:
        WatchedVillainAddressDict[address.lower()] = True


class TxStatus(Enum):
    Pending = "pending"
    Confirmed = "confirmed"
    Dropped = "dropped"


class MempoolServiceName(Enum):
    BlockNative = "BlockNative"
    Alchemy = "Alchemy"
    Bloxroute = "Bloxroute"


class MempoolTx:
    txHash = None
    # villainAddress can be the fromAddress or the toAddress
    villainAddress = None
    fromAddress = None
    toAddress = None
    gasPrice_wei = None
    gas = None
    value = None
    nonce = None
    data = None
    dateTimeFound = None
    source = None

    def __init__(self, _txHash, _villainAddress, _fromAddress, _toAddress, _gasPrice_wei, _gas, _value, _nonce, _data, _status, _source):
        self.txHash = _txHash.lower()

        self.villainAddress = None
        if _villainAddress:
            self.villainAddress = _villainAddress.lower()

        self.fromAddress = _fromAddress
        self.toAddress = _toAddress
        self.gasPrice_wei = _gasPrice_wei
        self.gas = _gas
        self.value = _value
        self.nonce = _nonce
        self.data = _data.lower()
        self.status = _status
        self.dateTimeFound = datetime.datetime.now()
        self.source = _source

    def GetAge_s(self):
        return (datetime.datetime.now() - self.dateTimeFound).total_seconds()

    def IsPending(self):
        return self.status.lower() == TxStatus.Pending.value.lower()


def AddVillainsTo_WatchedVillainAddressDict(villainAddressList):
    global WatchedVillainAddressDict

    for villainAddress in villainAddressList:
        try:
            WatchedVillainAddressDict[villainAddress.lower()] = True

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when adding watch address to WatchedVillainAddressDict, exception = " + traceback.format_exc())

    PrintAndLog_FuncNameHeader("WatchedVillainAddressDict now has " + str(len(WatchedVillainAddressDict)) + " watched addresses")


def AreAnyOfTheseAddressesAWatchedVillain(addressList):
    global WatchedVillainAddressDict

    for address in addressList:
        if address.lower() in WatchedVillainAddressDict:
            return True

    return False


def HandleIncomingMempoolTx(jData):
    global MempoolTxDict
    global Lock_MempoolTxDict

    try:
        # PrintAndLog_Mempool("HandleIncomingMempoolTx: jData = " + str(jData))
        # PrintAndLog_Mempool("HandleIncomingMempoolTx: jData[Key_MempoolServiceName] = " + str(jData[Key_MempoolServiceName]))
        jDataString = str(jData).lower()
        mempoolTxObject = None
        if Key_MempoolServiceName.lower() in jDataString and jData[Key_MempoolServiceName] == MempoolServiceName.Bloxroute.value:
            mempoolTxObject = HandleIncomingMempoolTx_Bloxroute(jData)
        elif Key_MempoolServiceName.lower() in jDataString and jData[Key_MempoolServiceName] == MempoolServiceName.Alchemy.value:
            mempoolTxObject = HandleIncomingMempoolTx_Alchemy(jData)
        elif Key_MempoolServiceName.lower() in jDataString and jData[Key_MempoolServiceName] == MempoolServiceName.BlockNative.value:
            mempoolTxObject = HandleIncomingMempoolTx_BlockNative(jData)
        # elif 'hash' in jDataString and 'to' in jDataString and 'input' in jDataString:
        #     mempoolTxObject = HandleIncomingMempoolTx_Alchemy(jData)
        else:
            message = "Found a pending mempool tx that doesn't fit any of our mempool services"
            # I don't want to spam the logs with this, I'd rather ignore most of them in case i suddenly get a lot of these
            if IsTimeToExecute(message, 30, None, True, True):
                PrintAndLog_Mempool("exception jData = " + str(jData))
                raise Exception(message)

            # Return in case I got here, I don't care about this tx at this point
            return

        if not mempoolTxObject:
            # We don't care about this tx
            return
        # If the tx has confirmed or canceled or anything other than pending
        # if mempoolTxObject.status != TxStatus.Pending.value:
        elif not mempoolTxObject.IsPending():
            # Remove it from MempoolTxDict
            # PrintAndLog_Mempool(mempoolTxObject.source + " found txHash " + str(mempoolTxObject.txHash) + " by villain " + str(
            #     mempoolTxObject.villainAddress) + " to be confirmed/canceled/dropped, removing it from MempoolTxDict")
            RemoveTxsFromMempoolTxDict([mempoolTxObject.txHash])
        else:
            # Else the tx is pending so consider adding it to MempoolTxDict
            if mempoolTxObject.txHash in MempoolTxDict:
                # PrintAndLog_Mempool("Found a txHash that's already in the MempoolTxDict. Let's hope it's the same as the one we have!")
                # Sanity check to see if the data is the same
                if mempoolTxObject.data != MempoolTxDict[mempoolTxObject.txHash].data:
                    message = "Found a mempoolTx whose data changed! Is one of my mempool tx services giving me bad data? txHash = " + str(mempoolTxObject.txHash)
                    Libraries.core.API_PostOperatorNotification(message)
                    PrintAndLog_Mempool(message)
                    PrintAndLog_Mempool("data 1 = " + str(mempoolTxObject.data) + ", source = " + str(mempoolTxObject.source))
                    PrintAndLog_Mempool("data 2 = " + str(MempoolTxDict[mempoolTxObject.txHash].data) + ", source = " + str(MempoolTxDict[mempoolTxObject.txHash].source))
                else:
                    pass
                    age_ms = 1000 * MempoolTxDict[mempoolTxObject.txHash].GetAge_s()
                    if Libraries.defaults.Mempool_LogLateDiscoveries:
                        PrintAndLog_Mempool("Found late villain tx via " + str(mempoolTxObject.source) + ", it came in " + str(
                            round(age_ms, 0)) + " milliseconds later than originally discovered by " + str(
                            MempoolTxDict[mempoolTxObject.txHash].source) + ", txHash = " + str(mempoolTxObject.txHash))

                    if EnableMempoolServiceCompareDict:
                        AddLatencyToMempoolServiceCompareDict(mempoolTxObject.txHash, mempoolTxObject.villainAddress, mempoolTxObject.source, round(age_ms, 1))

            else:
                # We have a new pending tx, save it
                MempoolTxDict[mempoolTxObject.txHash] = mempoolTxObject
                PrintAndLog_Mempool("Found villain's tx via " + str(mempoolTxObject.source) + ", villain " + str(
                    mempoolTxObject.villainAddress) + ", fromAddress = " + str(mempoolTxObject.fromAddress) + ", txHash " + str(
                    mempoolTxObject.txHash) + ", nonce = " + str(mempoolTxObject.nonce) + ". MempoolTxDict has " + str(len(MempoolTxDict)) + " txs")

                if EnableMempoolServiceCompareDict:
                    AddLatencyToMempoolServiceCompareDict(mempoolTxObject.txHash, mempoolTxObject.villainAddress, mempoolTxObject.source, 0)

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception, " + traceback.format_exc())


def AddTxHashToMempoolServiceCompareDict(txHash, villainAddress):
    global MempoolServiceCompareDict
    global MempoolServiceCompareVillainAddressList

    if villainAddress.lower() not in MempoolServiceCompareVillainAddressList:
        return

    if txHash.lower() not in MempoolServiceCompareDict:
        MempoolServiceCompareDict[txHash.lower()] = {}


def AddLatencyToMempoolServiceCompareDict(txHash, villainAddress, sourceName, latency):
    global MempoolServiceCompareDict
    global MempoolServiceCompareVillainAddressList

    if villainAddress.lower() not in MempoolServiceCompareVillainAddressList:
        return

    AddTxHashToMempoolServiceCompareDict(txHash, villainAddress)
    if sourceName.lower() not in MempoolServiceCompareDict[txHash.lower()]:
        MempoolServiceCompareDict[txHash.lower()][sourceName.lower()] = latency

    # Print this out every so often
    if IsTimeToExecute("AddLatencyToMempoolServiceCompareDict", 30):
        PrintAndLog_FuncNameHeader("MempoolServiceCompareDict = " + str(MempoolServiceCompareDict))


def HandleIncomingMempoolTx_BlockNative(jData):
    # BlockNative maintains its own watchlist,
    # so this function assumes that the blocknative watch list is set and we are only getting a subset of mempool txs so we don't need to filter them here

    # PrintAndLog_Mempool("jData = " + str(jData))
    gasPrice_wei = int(jData['gasPrice'])
    gas = int(jData['gas'])

    # Reject txs with gas prices that are too low, so that our trading logic doesn't even have to process them
    if Libraries.defaults.Mempool_RejectTxsWithLowGasPrices and gasPrice_wei < Libraries.gasStation.GetGasPrice_SafeLow():
        return

    blockchain = jData['system']
    network = jData['network']
    watchedAddress = jData['watchedAddress']
    value = int(jData['value'])
    nonce = int(jData['nonce'])
    input = jData['input']
    fromAddress = jData['from']
    toAddress = jData['to']
    txHash = jData['hash']
    status = jData['status']

    if blockchain.lower() != 'ethereum':
        return

    if network.lower() != 'main':
        return

    # PrintAndLog_Mempool("Found pending tx from " + str(watchedAddress))
    # PrintAndLog_Mempool("   txHash = " + str(txHash))
    # PrintAndLog_Mempool("   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(gasPrice_wei)) + " gwei")
    # PrintAndLog_Mempool("   input = " + str(input))

    return MempoolTx(txHash, watchedAddress, fromAddress, toAddress, gasPrice_wei, gas,
                     value, nonce, input, status, MempoolServiceName.BlockNative.value)


def HandleIncomingMempoolTx_Alchemy(jData):
    # Alchemy gives us all mempool txs, so we need to filter for watchlist here in this function

    #     jData: {
    #       "blockHash": null,
    #       "blockNumber": null,
    #       "from": "0xa790b711173721b248f72f25842071cc84cefcbb",
    #       "gas": "0x82110",
    #       "gasPrice": "0x187e710070",
    #       "hash": "0x3742b795c29bef5688f9b7d878f7bc0eaad4e99ae13d1aa80b0f9b4fa23a82be",
    #       "input": "0x61461954037bfb0de5009d000000000000000000000000000000000000000000000000120e3b5b9ad8aa008cb77ea869def8f7fdeab9e4da6cf02897bbf076c02aaa39b223fe8d0a0e5c4f27ead9083c756cc28ab7404063ec4dbcfd4598215992dc3f8ec853d700000000000000000000000000000000000000000000050816636cf21b9e09ca0000000000000000000000000000000000000000000004ef8d378dc100114bbb637bfb0de5009d000000000000000000000000000000000000000000000676dcf925cdac3bca5398f90bfc702ec548d21b5a566a4df6853e2890d48ab7404063ec4dbcfd4598215992dc3f8ec853d7a0b86991c6218b36c1d19d4a2e9eb0ce3606eb480000000000000000000000000000000000000000000000000000000000002990000000000000000000000000000000000000000000000000000000000000283e639a99d253008a0000000000000000000000000000000000000000000000000000000012fead5b0297dec872013f6b5fb443861090ad931542878126a0b86991c6218b36c1d19d4a2e9eb0ce3606eb480000000000000000000000000000000000000000000000fcaceb35fd4f0c2ef70000000000000000000000000000000000000000000000fc9a6bdaf046af536e6301",
    #       "nonce": "0x10f8",
    #       "to": "0x8a3960472b3d63894b68df3f10f58f11828d6fd9",
    #       "transactionIndex": null,
    #       "value": "0x0",
    #       "v": "0x25",
    #       "r": "0xbafdc32b29ff1c1d549b40bd93be9cb92cd3e3f1611aff6d875771020f12d3fb",
    #       "s": "0x2bb3d974555bda49a17653267e985a3e56348749a9ed66056e3170985070dd3d"
    #     }

    return HandleIncomingMempoolTx_Generic(jData, MempoolServiceName.Alchemy.value)


def HandleIncomingMempoolTx_Bloxroute(jData):
    # Bloxroute gives us all mempool txs, so we need to filter for watchlist here in this function

    # {
    #     'from': '0x7a59cb353863153171c95e5c166241e2d1304f34',
    #     'gas': '0xbf2b',
    #     'gasPrice': '0x156ba09800',
    #     'hash': '0x665817a5f4418ca6594ec266e96896f96ab894f77148f37d815bfc4de9508f10',
    #     'input': '0x095ea7b30000000000000000000000007a250d5630b4cf539739df2c5dacb4c659f2488dffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',
    #     'nonce': '0xb6',
    #     'value': '0x0',
    #     'v': '0x26',
    #     'r': '0xbf548514cb63fb40811dab55dc5385738f208dd73ad0aaf009ef9e139182edf7',
    #     's': '0x6b0bd8bef1889b73fa8cc96c043be1690798627e08eb0184f7b83265b46ad85e',
    #     'to': '0x1da01e84f3d4e6716f274c987ae4bee5dc3c8288'
    # }

    return HandleIncomingMempoolTx_Generic(jData, MempoolServiceName.Bloxroute.value)


def HandleIncomingMempoolTx_Generic(jData, sourceName):
    global WatchedVillainAddressDict
    # PrintAndLog_Mempool("HandleIncomingMempoolTx_Generic: jData = " + str(jData))

    # {
    #     'from': '0x7a59cb353863153171c95e5c166241e2d1304f34',
    #     'gas': '0xbf2b',
    #     'gasPrice': '0x156ba09800',
    #     'hash': '0x665817a5f4418ca6594ec266e96896f96ab894f77148f37d815bfc4de9508f10',
    #     'input': '0x095ea7b30000000000000000000000007a250d5630b4cf539739df2c5dacb4c659f2488dffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',
    #     'nonce': '0xb6',
    #     'value': '0x0',
    #     'to': '0x1da01e84f3d4e6716f274c987ae4bee5dc3c8288'
    # }

    if 'to' not in jData or not jData['to']:
        return

    gasPrice_wei = Libraries.core.ConvertHexToInt(jData['gasPrice'])
    gas = Libraries.core.ConvertHexToInt(jData['gas'])

    # Reject txs with gas prices that are too low, so that our trading logic doesn't even have to process them
    if Libraries.defaults.Mempool_RejectTxsWithLowGasPrices and gasPrice_wei < Libraries.gasStation.GetGasPrice_SafeLow():
        return

    toAddress = jData['to'].lower()
    fromAddress = jData['from'].lower()

    watchedAddress = None
    if AreAnyOfTheseAddressesAWatchedVillain([toAddress]):
        watchedAddress = toAddress
    elif AreAnyOfTheseAddressesAWatchedVillain([fromAddress]):
        watchedAddress = fromAddress
    # If we've made it this far in the if statement and we want to restrict the mempool to watchlist only
    elif Libraries.defaults.RestrictMempoolToWatchListOnly:
        # Return here so we don't fill up the mempool cache with txs we don't care about
        return

    value = Libraries.core.ConvertHexToInt(jData['value'])
    nonce = Libraries.core.ConvertHexToInt(jData['nonce'])
    input = jData['input']
    txHash = jData['hash']
    status = TxStatus.Pending.value

    # TODO testing new feature, does not work on mempool transactions :-(
    # txInfo = Libraries.core.API_GetTransactionInfo(txHash)
    # PrintAndLog_FuncNameHeader("Before calling API_GetTransactionTrace. txInfo = " + str(txInfo))
    # txReceipt = Libraries.core.API_GetTransactionReceipt(txHash)
    # PrintAndLog_FuncNameHeader("Before calling API_GetTransactionTrace. txReceipt = " + str(txReceipt))
    # t = threading.Thread(target=Libraries.core.API_WaitForThisToWork_GetTransactionTrace, args=(txHash,))
    # t.start()

    # PrintAndLog_Mempool("Found pending tx on " + str(sourceName) + " from watchedAddress " + str(watchedAddress))
    # PrintAndLog_Mempool("   txHash = " + str(txHash))
    # PrintAndLog_Mempool("   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(gasPrice_wei)) + " gwei")
    # PrintAndLog_Mempool("   input = " + str(input))

    return MempoolTx(txHash, watchedAddress, fromAddress, toAddress, gasPrice_wei, gas,
                     value, nonce, input, status, sourceName)


def GetThreadSafeCopyOf_MempoolTxDict():
    # global MempoolTxDict
    # global Lock_MempoolTxDict

    # myCopy = None
    # Lock_MempoolTxDict.acquire()
    # try:

    # Optimization, do not need to thread lock here
    myCopy = MempoolTxDict.copy()

    # finally:
    #     Lock_MempoolTxDict.release()

    return myCopy


def RemoveTxsFromMempoolTxDict(listOfTxsToRemoveFrom_MempoolTxDict):
    # global MempoolTxDict
    # global Lock_MempoolTxDict
    #
    # Lock_MempoolTxDict.acquire()
    # try:
    #     for txHash in listOfTxsToRemoveFrom_MempoolTxDict:
    #         if txHash.lower() in MempoolTxDict:
    #             del MempoolTxDict[txHash.lower()]
    #
    # finally:
    #     Lock_MempoolTxDict.release()

    # Optimization, do not need to thread lock here
    for txHash in listOfTxsToRemoveFrom_MempoolTxDict:
        if txHash.lower() in MempoolTxDict:
            try:
                del MempoolTxDict[txHash.lower()]

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception when calculating price, " + traceback.format_exc())


# Periodically call this function to clean up the MempoolTxDict
def CleanUpOldMempoolTxs():
    copy_MempoolTxDict = GetThreadSafeCopyOf_MempoolTxDict()
    if Libraries.defaults.VerboseLogging_Mempool_CleanUpOldMempoolTxs:
        PrintAndLog_FuncNameHeader("Begin with " + str(len(copy_MempoolTxDict)) + " txs in the mempool. copy_MempoolTxDict = " + str(copy_MempoolTxDict))

    # Ensure registration of all from addresses in the mempool to the TransactionCountCache
    addressList = []
    for txHash in copy_MempoolTxDict:
        mempoolTx = copy_MempoolTxDict[txHash]
        addressList.append(mempoolTx.fromAddress)

    newlyRegisteredAddresses = Libraries.cache.EnsureRegistrationOfAddressesInTransactionCountCache(addressList)

    if len(newlyRegisteredAddresses) > 0:
        # Now call UpdateTransactionCountCache_GivenSubSetOfAddresses because we want to batch call API_GetTransactionCount_Batched on only the new addresses that aren't cached
        Libraries.cache.UpdateTransactionCountCache_GivenSubSetOfAddresses(newlyRegisteredAddresses)

    # For the addresses that were already cached, we're going to assume that UpdateTransactionCountCache was called on them already

    # Now all of our transactionCounts should be in the cache and the below call to GetTransactionCountFromCacheIfPossible should hopefully just return from cache every single time

    # Now I want to call GetTransactionCountFromCacheIfPossible with doMakeAPICallIfTxCountWasNotCached set to False
    # so that it doesn't actually make any API calls.
    # I want to keep track of which results did not come from cache and make a list of them
    # Make a batched API call to get transaction counts for those so that we're not making them one at a time.
    listOfAddressesWhoseTransactionCountsAreNotCached = []
    for txHash in copy_MempoolTxDict:
        mempoolTx = copy_MempoolTxDict[txHash]
        dontCare, resultCameFromCache = Libraries.cache.GetTransactionCountFromCacheIfPossible(mempoolTx.fromAddress)
        if not resultCameFromCache:
            listOfAddressesWhoseTransactionCountsAreNotCached.append(mempoolTx.fromAddress)

    if Libraries.defaults.VerboseLogging_Mempool_CleanUpOldMempoolTxs:
        PrintAndLog_FuncNameHeader("listOfAddressesWhoseTransactionCountsAreNotCached of len " + str(
            len(listOfAddressesWhoseTransactionCountsAreNotCached)) + " = " + str(
            listOfAddressesWhoseTransactionCountsAreNotCached) + " which means I need to make a batched API call for all of these transaction counts")

    listOfTxsToRemoveFrom_MempoolTxDict = []
    for txHash in copy_MempoolTxDict:
        mempoolTx = copy_MempoolTxDict[txHash]
        # Find old pending txHashes and remove them

        # If it's old
        if mempoolTx.GetAge_s() > 200:
            listOfTxsToRemoveFrom_MempoolTxDict.append(txHash)
        else:
            # Check to see if the transactionCount for the address that sent the transaction has increased
            currentTransactionCount, resultCameFromCache = Libraries.cache.GetTransactionCountFromCacheIfPossible(mempoolTx.fromAddress)
            if Libraries.defaults.VerboseLogging_Mempool_CleanUpOldMempoolTxs_NonceAnalysisAndRemoves:
                PrintAndLog_FuncNameHeader("Considering breaking if transaction with this nonce got mined in, comparing currentTransactionCount = " + str(
                    currentTransactionCount) + " and nonce = " + str(mempoolTx.nonce) + ". resultCameFromCache = " + str(resultCameFromCache))
            if currentTransactionCount > mempoolTx.nonce:
                listOfTxsToRemoveFrom_MempoolTxDict.append(txHash)
                if Libraries.defaults.VerboseLogging_Mempool_CleanUpOldMempoolTxs_NonceAnalysisAndRemoves:
                    PrintAndLog_FuncNameHeader("removing this tx because currentTransactionCount (" + str(
                        currentTransactionCount) + ") exceeded our target nonce(" + str(
                        mempoolTx.nonce) + "). We're going to assume this means our tx mined in or got dropped")

    RemoveTxsFromMempoolTxDict(listOfTxsToRemoveFrom_MempoolTxDict)

    if Libraries.defaults.VerboseLogging_Mempool_CleanUpOldMempoolTxs:
        copy_MempoolTxDict = GetThreadSafeCopyOf_MempoolTxDict()
        PrintAndLog_FuncNameHeader("End with " + str(len(copy_MempoolTxDict)) + " txs in the mempool. copy_MempoolTxDict = " + str(copy_MempoolTxDict))
