from enum import Enum

import Libraries.defaults
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader
import Libraries.utils
from Libraries.network import NodeInfrastructure
import Libraries.nodes
from Libraries.tradeProperties import TradeProperties

ActiveConfig = None
SetActiveConfigHasBeenCalled = False
# A list of all options the operator ran ninja with
ActiveOptions = []


# Note, values must be the same as the property and must be lowercase
# This enables me to iterate over the Enum and pass in Enum values from command line
class Config(Enum):
    Wild = 'wild'  # Trading in the wild
    Hide = 'hide'  # Trading only as a Hiding Game keeper


# Note, values must be the same as the property and must be lowercase
# This enables me to iterate over the Enum and pass in Enum values from command line
class Option(Enum):
    LiteQuote = 'litequote'  # For local testing with minimal quoteTokens, this reduces the API calls and performance requirements
    LiteBase = 'litebase'  # For local testing with minimal baseTokens, this reduces the API calls and performance requirements
    LiteAmount = 'liteamount'  # For local testing incredibly small quoteToken amounts, helpful for forcing and debugging trades
    DE = 'de'  # Uses nodes from Germany
    US = 'us'  # Uses nodes from US
    Time = 'time'  # TimeMachine for back testing based on some point in time
    Notify = 'notify'  # Send notifications to ninja operator
    Trade = 'trade'  # Trading not execute trades
    NoKeys = 'nokeys'  # Do not execute trades
    ForceArb = 'forcearb'  # Force find arbitrage by lowering the min profit requirement
    ForceFlash = 'forceflash'  # Force a flash loan regardless of whether or not ninja has the tokens needed to facilitate the trade
    LimitTradeCount = 'limittradecount'  # Limit the number of trades that are allowed to execute before we halt trading
    LimitDXs = 'limitdxs'  # Require trades too only include a strict list of Exchanges
    NoSafeTrade = 'nosafetrade'  # Disables pre trade safety such as eth_estimateGas
    GenHideList = 'genhidelist'  # Generate the HidingBook's tokenList on start up, then stop and do not continue running
    Approve = 'approve'  # Issue approvals for new tokens on start up, then stop and do not continue running
    LocalHide = 'localhide'  # Directs HidingBook calls to localhost instead of production


def SetActiveConfig(config):
    import Exchanges.hidingBook
    import Libraries.ninjaOps
    from Libraries.exchanges import ExchangeName_HidingBook, ExchangeName_0xMesh

    global ActiveConfig
    global SetActiveConfigHasBeenCalled

    if SetActiveConfigHasBeenCalled:
        raise Exception("You can only set one Config at a time. ActiveConfig already set to " + str(ActiveConfig))

    SetActiveConfigHasBeenCalled = True

    ActiveConfig = config

    PrintAndLog_FuncNameHeader("Libraries.defaults.EnableMempoolStrategy_FrontRunning before = " + str(Libraries.defaults.EnableMempoolStrategy_FrontRunning))
    if ActiveConfig == Config.Wild:
        Libraries.ninjaOps.NinjaOpConfig_NinjaGeneric = Libraries.ninjaOps.NinjaOpConfig_NinjaGeneric_Wild

        # Lots of competition trading in the wild, we can ban extremely competitive trading paths that are a waste of gas to try and compete
        # Banning ETH based 2 leg trades that are not flash loaned (so they're small in size) and do not include 0x as the most optimal route
        # I'm setting exchangeName_toExclude as 0x because i'm seeing significantly less competition on the 0x path
        Libraries.defaults.FinalStepTradeBanList = [
            TradeProperties('0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'.lower(), 2, False, ExchangeName_0xMesh)
        ]

    elif ActiveConfig == Config.Hide:
        Libraries.ninjaOps.NinjaOpConfig_NinjaGeneric = Libraries.ninjaOps.NinjaOpConfig_NinjaGeneric_Hide

        Libraries.defaults.EnableOrderAggregator_HidingBook = True
        Libraries.defaults.EnableMempoolStrategy_FrontRunning = False
        Libraries.defaults.EnableMempoolStrategy_KeepAhead = False
        Libraries.defaults.CancelTradesThatGetGrimTriggeredByAVillain = False
        # No gas auctions in the Hiding Game, shouldn't need to ban any paths
        Libraries.defaults.FinalStepTradeBanList = []

        # Hiding Game should only be trading with the HidingBook
        Libraries.defaults.RequireTradesToIncludeTheseExchangeNames = []
        Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_HidingBook)

        # Point everything to production
        Exchanges.hidingBook.Port_http = Exchanges.hidingBook.Port_http_Production
        Exchanges.hidingBook.Port_ws = Exchanges.hidingBook.Port_ws_Production
        Exchanges.hidingBook.Url_Base = Exchanges.hidingBook.Host_Production
        Exchanges.hidingBook.Headers = Exchanges.hidingBook.Headers_Production

    else:
        raise Exception("config not yet implemented: " + str(config))

    PrintAndLog_FuncNameHeader("ActiveConfig = " + str(ActiveConfig))


def SetOption(option):
    import Exchanges.keeperDAO
    import Exchanges.hidingBook
    from Libraries.exchanges import ExchangeName_Curve, ExchangeName_UniswapV2, ExchangeName_Sushiswap, ExchangeName_Defiswap, \
        ExchangeName_Balancer, ExchangeName_0xMesh, ExchangeName_HidingBook

    global ActiveOptions

    PrintAndLog_FuncNameHeader("Setting option " + str(option))
    ActiveOptions.append(option)

    if option == Option.LiteQuote:
        Exchanges.keeperDAO.BorrowableQuoteTokensList = []
        # Note, less quoteTokens = better performance and simplified testing scenarios
        # Simply overwrite BorrowableQuoteTokensList this list
        Exchanges.keeperDAO.BorrowableQuoteTokensList.append(Exchanges.keeperDAO.Contract_USDC)
        Exchanges.keeperDAO.BorrowableQuoteTokensList.append(Exchanges.keeperDAO.Contract_DAI)
        # Exchanges.keeperDAO.BorrowableQuoteTokensList.append(Exchanges.keeperDAO.Contract_USDT)

    elif option == Option.LiteBase:
        Libraries.defaults.UseHardCodedTradableTokensList = True

    elif option == Option.LiteAmount:
        Exchanges.keeperDAO.ManuallyAddedTradeAmountArrayDict = {}
        # Simply overwrite ManuallyAddedTradeAmountArrayDict this dict with a couple really small dust values so we can find overlapping spot prices on AMMs
        Exchanges.keeperDAO.ManuallyAddedTradeAmountArrayDict[Exchanges.keeperDAO.EthTokenContract.lower()] = [0.1, 0.01, 0.000001]

    elif option == Option.DE:
        Libraries.defaults.ActiveNodeInfrastructure = NodeInfrastructure.DE
        Libraries.nodes.ConfigureNodes()

    elif option == Option.US:
        Libraries.defaults.ActiveNodeInfrastructure = NodeInfrastructure.US
        Libraries.nodes.ConfigureNodes()

    elif option == Option.Time:
        # ******** Begin hard code time machine properties here ******** #
        timeMachine_borrowableQuoteTokensList = [Exchanges.keeperDAO.Contract_renBTC, Exchanges.keeperDAO.Contract_wBTC]
        blockNumberToInspect = 11498921  # arb - 0xce782ad2351dd4e3e33c726037e84c3b2b5506ea02bfc63069ab46a3d4413600
        # ******** End hard code time machine properties here ******** #

        Exchanges.keeperDAO.BorrowableQuoteTokensList = []
        # Must use an archive node for time machine, but be weary of rate limits on Alchemy
        # you can blow through an entire month's rate limit in just 20 minutes of use...
        Libraries.defaults.ActiveNodeInfrastructure = NodeInfrastructure.AlchemyOnly
        Libraries.nodes.ConfigureNodes()
        Exchanges.keeperDAO.BorrowableQuoteTokensList = timeMachine_borrowableQuoteTokensList
        # We have to initialize the time machine at least 2 blocks before the where we want to go, because it takes 2 whole blocks to initialize properly
        Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug = blockNumberToInspect - 2
        Libraries.defaults.StopIncrementing_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_EndBlock = \
            Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug + \
            Libraries.defaults.StopIncrementing_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_AfterNBlocks

    elif option == Option.Notify:
        Libraries.defaults.EnableSendingNotificationsToOperator = True

    elif option == Option.Trade:
        Libraries.defaults.EnableNinja_TradeExecution = True

    elif option == Option.NoKeys:
        Libraries.defaults.MustSuccessfullyLoadSecurityCredentialsOnStartUp = False

    elif option == Option.ForceArb:
        # This is extremely dangerous!
        Libraries.defaults.UseDebugMinProfitRequirement_NinjaGeneric = True
        Libraries.defaults.FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm_minProfitPercentageRequirement_HardCodeToZero = True

    elif option == Option.LimitTradeCount:
        Libraries.defaults.PreventTradingAfterNTradesAreTriggered_MaxTradesAllowed = 2

    elif option == Option.ForceFlash:
        # You only need to set one of these, they both achieve the same thing. At some point I will deprecate one of them
        # Libraries.defaults.ForceSourcingQuoteTokensFrom_KeeperDAOFlashLoan = True
        Libraries.defaults.ForceNinjaBalanceToBeZero_SourcingQuoteTokensFrom_WillForceAKeeperDAOFlashLoan = True

    elif option == Option.LimitDXs:
        Libraries.defaults.RequireTradesToIncludeTheseExchangeNames = []
        Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_0xMesh)
        # Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_HidingBook)
        # Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_UniswapV2)
        # Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_Sushiswap)
        # Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_Defiswap)
        # Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_Sakeswap)
        # Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_Curve)
        # Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_Uniswap)
        # Libraries.defaults.RequireTradesToIncludeTheseExchangeNames.append(ExchangeName_Balancer)

    elif option == Option.NoSafeTrade:
        # Setting these to False is extremely dangerous!
        Libraries.defaults.CallEstimateGasBeforeTradingForSafety = False
        Libraries.defaults.CallEthCallBeforeTradingForSafety = False

    elif option == Option.GenHideList:
        Libraries.defaults.GenerateHidingBookTokenListOnStartUp = True
        Libraries.defaults.GenerateHidingBookTokenListOnStartUp_ExitAfterApproves = True

    elif option == Option.Approve:
        Libraries.defaults.IssueApprovesOnStartup = True
        Libraries.defaults.IssueApprovesOnStartup_DoExecute = True
        Libraries.defaults.IssueApprovesOnStartup_ExitAfterApproves = True

    elif option == Option.LocalHide:
        # Point everything to production
        Exchanges.hidingBook.Port_http = Exchanges.hidingBook.Port_http_Local
        Exchanges.hidingBook.Port_ws = Exchanges.hidingBook.Port_ws_Local
        Exchanges.hidingBook.Url_Base = Exchanges.hidingBook.Host_Local
        Exchanges.hidingBook.Headers = Exchanges.hidingBook.Headers_Local

    else:
        raise Exception("Option not yet implemented: " + str(option))


def EnforceRules(enumList):
    # Put all rule functions here
    EnforceRule_TimeMachineCannotBeUsedWithLite(enumList)
    EnforceRule_ForceArb_MustHaveOperatorConsentBeforeExecutingTrades_ItsExtremelyDangerous(enumList)
    EnforceRule_NoSafeTrade_MustHaveOperatorConsentBeforeExecutingTrades_ItsExtremelyDangerous(enumList)
    EnforceRule_TradeAndForceArbWithoutLimitTradeCount_MustHaveOperatorConsentBeforeExecutingTrades_ItsExtremelyDangerous(enumList)
    EnforceRule_GenHideList_MustBeCalledInHidingGame(enumList)


def SetConditionalOptions():
    global ActiveOptions

    # This function assumes that both SetActiveConfig and SetOption has already been called on all parameters

    # Time machine + tailgating need some additional parameters set in order to function properly
    if Libraries.utils.ActiveScriptName == Libraries.utils.ScriptName.NinjaTail and IsEnumInEnumList(Option.Time, ActiveOptions):
        # TODO test this to confirm it works
        Libraries.defaults.Tailgating_WithHardCodedTxHash = True
        Libraries.defaults.UseDebugGasStation = True


def EnforceRule_TimeMachineCannotBeUsedWithLite(enumList):
    # These options are not compatible
    if IsEnumInEnumList(Option.Time, enumList) and IsEnumInEnumList(Option.LiteAmount, enumList):
        raise Exception("LiteAmount option cannot be used with TimeMachine option")

    # TODO, i'm not sure if this conflicts with Time... It may have just been LiteAmount
    if IsEnumInEnumList(Option.Time, enumList) and IsEnumInEnumList(Option.LiteQuote, enumList):
        raise Exception("LiteQuote option cannot be used with TimeMachine option")

    # TODO, i'm not sure if this conflicts with Time... It may have just been LiteAmount
    if IsEnumInEnumList(Option.Time, enumList) and IsEnumInEnumList(Option.LiteBase, enumList):
        raise Exception("LiteBase option cannot be used with TimeMachine option")


def EnforceRule_ForceArb_MustHaveOperatorConsentBeforeExecutingTrades_ItsExtremelyDangerous(enumList):
    # These options are extremely dangerous when used together
    message = 'ForceArb is extremely dangerous when used with Trade!'
    if IsEnumInEnumList(Option.Trade, enumList) and IsEnumInEnumList(Option.ForceArb, enumList):
        answer = input(message + " Are you sure you want to continue?  Y/N: ").lower()
        PrintAndLog_FuncNameHeader("Answer = " + str(answer))
        if answer == 'y':
            return
        else:
            raise Exception(str(message) + ". Operator chickened out")


def EnforceRule_NoSafeTrade_MustHaveOperatorConsentBeforeExecutingTrades_ItsExtremelyDangerous(enumList):
    # These options are extremely dangerous when used together
    message = 'NoSafeTrade is extremely dangerous when used with Trade!'
    if IsEnumInEnumList(Option.Trade, enumList) and IsEnumInEnumList(Option.NoSafeTrade, enumList):
        answer = input(message + " Are you sure you want to continue?  Y/N: ").lower()
        PrintAndLog_FuncNameHeader("Answer = " + str(answer))
        if answer == 'y':
            return
        else:
            raise Exception(str(message) + ". Operator chickened out")


def EnforceRule_TradeAndForceArbWithoutLimitTradeCount_MustHaveOperatorConsentBeforeExecutingTrades_ItsExtremelyDangerous(enumList):
    # These options are extremely dangerous when used together
    message = 'Trade and ForceArb are extremely dangerous when used without LimitTradeCount!'
    if IsEnumInEnumList(Option.Trade, enumList) and IsEnumInEnumList(Option.ForceArb, enumList) and not IsEnumInEnumList(Option.LimitTradeCount, enumList):
        answer = input(message + " Are you sure you want to continue?  Y/N: ").lower()
        PrintAndLog_FuncNameHeader("Answer = " + str(answer))
        if answer == 'y':
            return
        else:
            raise Exception(str(message) + ". Operator chickened out")


def EnforceRule_GenHideList_MustBeCalledInHidingGame(enumList):
    if IsEnumInEnumList(Option.GenHideList, enumList) and ActiveConfig != Config.Hide:
        raise Exception('GenHideList must be called in Hide mode with the Hiding game or else bad things can happen!')


def IsEnumInEnumList(enumToMatch, enumList):
    for localEnum in enumList:
        if localEnum == enumToMatch:
            return True
    return False


def ConvertEnumListToEnumValueList(enumList):
    enumValueList = []
    for enumString in enumList:
        enumValueList.append(enumString.split('.')[1])
    return enumValueList


def GetEnumValueFromArgument(arg, enumClass: Enum):
    # Convert the enum to a list of enum values
    enumList_option = list(map(str, enumClass))
    enumList_option = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(enumList_option)
    enumValueList_option = ConvertEnumListToEnumValueList(enumList_option)
    # PrintAndLog_FuncNameHeader("enumValueList_option = " + str(enumValueList_option))
    # If the arg is found in this enum
    if arg in enumValueList_option:
        # Convert this arg to an enum value
        index = enumValueList_option.index(arg)
        # PrintAndLog_FuncNameHeader("index = " + str(index))
        enumString = enumValueList_option[index]
        # PrintAndLog_FuncNameHeader("enumString = " + str(enumString))
        matchedEnum = enumClass(enumString)
        # PrintAndLog_FuncNameHeader("matchedEnum = " + str(matchedEnum))
        return matchedEnum


def GetArgsFromSysArgv(sysArgv):
    arguments = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(sysArgv.copy())
    arguments.pop(0)
    arguments.pop(0)
    return arguments


def IsOptionActive(option):
    global ActiveOptions

    if option in ActiveOptions:
        return True
    else:
        return False
