from Libraries.loggingConfig import PrintAndLog, PrintAndLog_Stats

import datetime
import copy
from threading import Lock

import Libraries.executeOnInterval

RecordingsDict = {}
Lock_RecordingsDict = Lock()

RecordingHistoryDict = {}
Lock_RecordingHistoryDict = Lock()

Interval_seconds_ClearRecordingHistory = 120
Interval_seconds_GetStatsOnHistory_Average = 60

RoundNumOfDecimals = 2


def BeginRecording(key):
    PrintAndLog("BeginRecording key: " + str(key))
    global RecordingsDict
    global Lock_RecordingsDict

    # Record the datetime at when you begin your task
    Lock_RecordingsDict.acquire()
    try:
        RecordingsDict[key] = datetime.datetime.now()

    finally:
        Lock_RecordingsDict.release()


def EndRecording(key):
    PrintAndLog("EndRecording key: " + str(key))
    global RecordingsDict
    global Lock_RecordingsDict
    global RecordingHistoryDict
    global Lock_RecordingHistoryDict

    # Measure the duration of this task
    duration_seconds = None
    Lock_RecordingsDict.acquire()
    try:
        duration_seconds = (datetime.datetime.now() - RecordingsDict[key]).total_seconds()
        RecordingsDict[key] = None

    finally:
        Lock_RecordingsDict.release()

    # PrintAndLog("EndRecording key: " + str(key) + ", duration_seconds = " + str(duration_seconds))

    # Store the measured duration in the RecordingHistoryDict with the same key
    # This will log a history of durations so I can see how it performs over time
    Lock_RecordingHistoryDict.acquire()
    try:
        # Consider initializing the history
        if key not in RecordingHistoryDict:
            RecordingHistoryDict[key] = []

        RecordingHistoryDict[key].append(duration_seconds)
        # PrintAndLog("EndRecording key: " + str(key) + ", duration appended to history. RecordingHistoryDict[key] = " + str(RecordingHistoryDict[key]))

    finally:
        Lock_RecordingHistoryDict.release()

    # Periodically, clear the history every so often so it's not spamming memory usage.
    Libraries.executeOnInterval.IsTimeToExecute("Call ClearRecordingHistory", Interval_seconds_ClearRecordingHistory, ClearRecordingHistory, False, False)


def ClearRecordingHistory():
    global RecordingHistoryDict
    global Lock_RecordingHistoryDict

    # PrintAndLog("ClearRecordingHistory")
    Lock_RecordingHistoryDict.acquire()
    try:
        RecordingHistoryDict.clear()

    finally:
        Lock_RecordingHistoryDict.release()


def GetStatsOnHistory_Average():
    global RecordingHistoryDict
    global Lock_RecordingHistoryDict

    copy_RecordingHistoryDict = None
    Lock_RecordingHistoryDict.acquire()
    try:
        # PrintAndLog("GetStatsOnHistory_Average with RecordingHistoryDict = " + str(RecordingHistoryDict))
        copy_RecordingHistoryDict = copy.deepcopy(RecordingHistoryDict)

    finally:
        Lock_RecordingHistoryDict.release()

    sumDict = {}
    # sum = 0
    for key in copy_RecordingHistoryDict:
        # init the entry in sumDict
        if key not in sumDict:
            sumDict[key] = 0

        # maintain a sum for each entry in the dict
        for item in copy_RecordingHistoryDict[key]:
            sumDict[key] += item

    averageDict = {}
    for key in copy_RecordingHistoryDict:
        averageDict[key] = sumDict[key] / len(copy_RecordingHistoryDict[key])
        PrintAndLog_Stats("GetStatsOnHistory_Average on key " + str(key) + ", average = " + str(round(averageDict[key], RoundNumOfDecimals)))

    return averageDict


def GetStatsOnHistory_Max():
    global RecordingHistoryDict
    global Lock_RecordingHistoryDict

    copy_RecordingHistoryDict = None
    Lock_RecordingHistoryDict.acquire()
    try:
        copy_RecordingHistoryDict = copy.deepcopy(RecordingHistoryDict)

    finally:
        Lock_RecordingHistoryDict.release()

    maxDict = {}
    for key in copy_RecordingHistoryDict:
        maxDict[key] = copy_RecordingHistoryDict[key][0]
        for item in copy_RecordingHistoryDict[key]:
            if item > maxDict[key]:
                maxDict[key] = item

        PrintAndLog_Stats("GetStatsOnHistory_Max on key " + str(key) + ", max = " + str(round(maxDict[key], RoundNumOfDecimals)))

    return maxDict


def GetStatsOnHistory_Min():
    global RecordingHistoryDict
    global Lock_RecordingHistoryDict

    copy_RecordingHistoryDict = None
    Lock_RecordingHistoryDict.acquire()
    try:
        copy_RecordingHistoryDict = copy.deepcopy(RecordingHistoryDict)

    finally:
        Lock_RecordingHistoryDict.release()

    minDict = {}
    for key in copy_RecordingHistoryDict:
        minDict[key] = copy_RecordingHistoryDict[key][0]
        for item in copy_RecordingHistoryDict[key]:
            if item < minDict[key]:
                minDict[key] = item

        PrintAndLog_Stats("GetStatsOnHistory_Min on key " + str(key) + ", min = " + str(round(minDict[key], RoundNumOfDecimals)))

    return minDict
