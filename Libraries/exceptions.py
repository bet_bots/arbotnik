class MarketNotSupportedException(Exception):
    pass


class DeadlineExpiredException(Exception):
    pass


class TradeNotFundedException(Exception):
    pass


class InsufficientTokenAllowanceException(Exception):
    pass


class TradePriceRequirementException(Exception):
    pass


class OutOfAvailableAccountsException(Exception):
    pass


class TradeRecipientDiffersFromTransactionBroadcaster_FeatureNotYetImplementedException(Exception):
    pass


class OverheadTokenTransferGasCostNotAvailableForThisTokenException(Exception):
    pass


class InvalidOrderType(Exception):
    pass


# class ArbitrageOpportunityAbandoned(Exception):
#     pass
