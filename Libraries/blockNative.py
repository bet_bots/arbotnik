import json
import time
import traceback
import requests
import webhook_listener
from os.path import expanduser

from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError
from Libraries.executeOnInterval import IsTimeToExecute

webhooks = None

Port_WebhookServer_Ninja_Arbitrage = 8090
Port_WebhookServer_Ninja_Tailgating = 8091

APIKey_Production_Ninja_Tailgating = "36c46d86-64ea-4f50-98b3-0ae2b547d226"
APIKey_Production_Ninja_Arbitrage = "2423ba0c-6a98-44aa-a4e2-3c850f21a0a4"
APIKey_Develop = "f0db3275-da92-4d86-9f14-192a4515d441"
KnownSafeAPIKeys = [
    APIKey_Production_Ninja_Tailgating,
    APIKey_Production_Ninja_Arbitrage,
    APIKey_Develop,
]

Url_Base = "https://api.blocknative.com/"


def ProcessPostRequest(request, *args, **kwargs):
    from Libraries.mempool import HandleIncomingMempoolTx, MempoolServiceName, Key_MempoolServiceName
    from Libraries.core import API_PostOperatorNotification

    try:
        jData = json.loads(str(request.body.read(int(request.headers["Content-Length"])), 'utf-8'))

        # Reject all messages from invalid API keys
        if not AtLeastOneKnownSafeAPIKeyIsInString(str(jData)):
            key = "BlockNative webhooks found a request from an invalid API key"
            # Post a message to API_PostOperatorNotification every so often to alert me
            if IsTimeToExecute(key, 200):
                message = key + ", " + str(jData)
                API_PostOperatorNotification(message)

            return

        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        # Update the data to include the mempoolServiceName
        jData[Key_MempoolServiceName] = MempoolServiceName.BlockNative.value
        HandleIncomingMempoolTx(jData)

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception in when processing a webhook, " + traceback.format_exc())


def AtLeastOneKnownSafeAPIKeyIsInString(myString):
    global KnownSafeAPIKeys

    for knownSafeAPIKey in KnownSafeAPIKeys:
        if knownSafeAPIKey.lower() in myString.lower():
            # PrintAndLog_FuncNameHeader("AtLeastOneKnownSafeAPIKeyIsInString is returning True for API key " + str(knownSafeAPIKey))
            return True

    # If we've made it this far, none of our API keys were found in this payload
    # PrintAndLog_FuncNameHeader("AtLeastOneKnownSafeAPIKeyIsInString is returning False")
    return False


def GetWebhookPort():
    import Libraries.utils

    # TODO, super hacky
    #  Ghetto hack because I want to use this port when developing at home on my laptop so I only have 1 webhook created for my home ip address
    if 'pako' in expanduser('~').lower():
        return Port_WebhookServer_Ninja_Arbitrage

    # Set the port based on the ActiveScriptName
    # This is because I have different watch lists for Arbitrage and Tailgating
    if Libraries.utils.ActiveScriptName == Libraries.utils.ScriptName.NinjaArb:
        return Port_WebhookServer_Ninja_Arbitrage
    elif Libraries.utils.ActiveScriptName == Libraries.utils.ScriptName.NinjaTail:
        return Port_WebhookServer_Ninja_Tailgating

    raise Exception("No valid port")


def GetAPIKeys():
    import Libraries.utils

    # Set the port based on the ActiveScriptName
    # This is because I have different watch lists for Arbitrage and Tailgating
    if Libraries.utils.ActiveScriptName == Libraries.utils.ScriptName.NinjaArb:
        return [
            APIKey_Production_Ninja_Arbitrage,
            APIKey_Develop,
        ]
    elif Libraries.utils.ActiveScriptName == Libraries.utils.ScriptName.NinjaTail:
        return [
            APIKey_Production_Ninja_Tailgating,
            APIKey_Develop,
        ]

    raise Exception("No valid API keys")


def CreateWebhookListener():
    global webhooks

    webhooks = webhook_listener.Listener(handlers={"POST": ProcessPostRequest}, port=GetWebhookPort())
    webhooks.start()


def API_SendTestWebhook(message, port):
    try:
        # PrintAndLog_FuncNameHeader("message = " + message)
        data = {"content": message}
        # Uses local host when running locally while testing
        url = 'http://0.0.0.0:' + str(port) + '/'
        requests.post(url, data=data, timeout=5)

    except:
        PrintAndLogError("Exception inside API_PostOperatorNotification: " + str(traceback.format_exc()))
        PrintAndLogError(message)
        pass


# Do not call this on the main thread, i'm sleeping between calls
def API_AddWatchAddresses(addresses, sleepTime_s=0.40):
    # Use a rate limit respecting sleepTime_s in here
    successCount = 0
    tryCount = 0
    apiKeys = GetAPIKeys()
    for apiKey in apiKeys:
        for address in addresses:
            try:
                tryCount += 1
                API_AddWatchAddress(address, apiKey)
                successCount += 1

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in API_AddWatchAddresses, " + traceback.format_exc())

            time.sleep(sleepTime_s)

    PrintAndLog_FuncNameHeader("Finished with " + str(successCount) + " successes out of " + str(tryCount) + " request attempts")


def API_AddWatchAddress(address, apiKey):
    from Libraries.core import Headers, RequestTimeout_seconds

    payload = {
        "blockchain": "ethereum",
        "networks": ["main"],
        "address": address,
        "apiKey": apiKey,
    }
    url = Url_Base + "address"
    # PrintAndLog_FuncNameHeader("url = " + str(url))
    # PrintAndLog_FuncNameHeader("payload = " + str(payload))
    response = requests.post(url, data=json.dumps(payload), headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
    else:
        PrintAndLogError("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


# Do not call this on the main thread, i'm sleeping between calls
def API_DeleteWatchAddresses(addresses, sleepTime_s=0.33):
    apiKeys = GetAPIKeys()
    for apiKey in apiKeys:
        for address in addresses:
            API_DeleteWatchAddress(address, apiKey)
            time.sleep(sleepTime_s)


def API_DeleteWatchAddress(address, apiKey):
    from Libraries.core import Headers, RequestTimeout_seconds

    payload = {
        "blockchain": "ethereum",
        "networks": ["main"],
        "address": address,
        "apiKey": apiKey,
    }
    url = Url_Base + "address"
    # PrintAndLog_FuncNameHeader("url = " + str(url))
    # PrintAndLog_FuncNameHeader("payload = " + str(payload))
    response = requests.delete(url, data=json.dumps(payload), headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
    else:
        PrintAndLogError("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()
