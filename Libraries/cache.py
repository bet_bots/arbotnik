# TODO, Implement a caching system where I can take anything and cache it in a file.
#  then when anything accesses it, i'll return it's value.
#  But after x minutes, i'll tell it that it needs to make the API call to get new data and not return the cached value
#  i'll want to store the file in ./Cache and also somehow store it's age so it knows when to expire it. Store the age in the filename?
#  use this to cache files like the kyber list of token addresses or the ERCDEX response for fee recipient and exchange address.
import copy
import datetime
import os
import time
import traceback
from threading import Lock
import jsonpickle

import Libraries.executeOnInterval
from Libraries.core import API_PostOperatorNotification
import Libraries.defaults
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader, PrintAndLog_Detailed

Lock_Cache = Lock()

Lock_EthereumAssetCache = Lock()
EthereumAssetCache = None

Lock_TransactionCountCache = Lock()
TransactionCountCache = {}


def GetFilepath(filename):
    return Libraries.defaults.Directory_Cache + "/" + filename + ".json"


def DoesCacheExistInCache(filename):
    return os.path.exists(GetFilepath(filename))


def StoreDataInCache(filename, data):
    global Lock_Cache

    Lock_Cache.acquire()
    try:
        file = open(GetFilepath(filename), "w")
        file.write(jsonpickle.encode(data))
        file.close()

    finally:
        Lock_Cache.release()


def GetDataFromCache(filename):
    global Lock_Cache

    data = None
    Lock_Cache.acquire()
    try:
        file = open(GetFilepath(filename), "r")
        data = jsonpickle.decode(file.read())
        file.close()

    finally:
        Lock_Cache.release()

    return data


def GetModifiedDateOfFile_UnixEpochTime_Seconds(filename):
    global Lock_Cache

    returnValue = None
    Lock_Cache.acquire()
    try:
        filepath = GetFilepath(filename)
        if not os.path.exists(filepath):
            returnValue = None
        else:
            returnValue = os.path.getmtime(filepath)

    finally:
        Lock_Cache.release()

    return returnValue


def GetFileTimeSinceLastModified_Seconds(filename):
    result = GetModifiedDateOfFile_UnixEpochTime_Seconds(filename)
    if not result:
        return None
    else:
        return time.time() - result


def GetFileTimeSinceLastModified_Minutes(filename):
    result = GetFileTimeSinceLastModified_Seconds(filename)
    if not result:
        return None
    else:
        return result / float(60)


def GetFileTimeSinceLastModified_Hours(filename):
    result = GetFileTimeSinceLastModified_Minutes(filename)
    if not result:
        return None
    else:
        return result / float(60)


def GetFileTimeSinceLastModified_Days(filename):
    result = GetFileTimeSinceLastModified_Hours(filename)
    if not result:
        return None
    else:
        return result / float(24)


def GetAndCache_RadarRelay_API_GetListOfTokenAddresses():
    from Exchanges.radarRelay2 import API_GetListOfTokenAddresses
    try:
        key = "Exchanges.radarRelay2.API_GetListOfTokenAddresses"
        StoreDataInCache(key, API_GetListOfTokenAddresses())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_Kyber_API_GetListOfTokenAddresses():
    from Exchanges.kyber import API_GetListOfTokenAddresses
    try:
        key = "Exchanges.kyber.API_GetListOfTokenAddresses"
        StoreDataInCache(key, API_GetListOfTokenAddresses())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_0xApi_API_GetListOfTokenAddresses():
    from Exchanges.zrxApi import API_GetListOfTokenAddresses
    try:
        key = "Exchanges.zrxApi.API_GetListOfTokenAddresses"
        StoreDataInCache(key, API_GetListOfTokenAddresses())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_HidingBook_API_GetListOfTokenAddresses():
    from Exchanges.hidingBook import API_GetListOfTokenAddresses
    try:
        key = "Exchanges.hidingBook.API_GetListOfTokenAddresses"
        StoreDataInCache(key, API_GetListOfTokenAddresses())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_Bancor_API_GetAllCurrencies():
    from Exchanges.bancor import API_GetAllCurrencies
    try:
        key = "Exchanges.bancor.API_GetAllCurrencies"
        StoreDataInCache(key, API_GetAllCurrencies())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_Uniswap_API_GetExchangeDataDict():
    from Exchanges.uniswap import API_GetExchangeDataDict
    try:
        key = "Exchanges.uniswap.API_GetExchangeDataDict"
        StoreDataInCache(key, API_GetExchangeDataDict())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_UniswapV2_API_GetExchangeDataDict():
    from Exchanges.uniswapV2 import API_GetExchangeDataDict
    try:
        key = "Exchanges.uniswapV2.API_GetExchangeDataDict"
        StoreDataInCache(key, API_GetExchangeDataDict())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_Sushiswap_API_GetExchangeDataDict():
    from Exchanges.sushiswap import API_GetExchangeDataDict
    try:
        key = "Exchanges.sushiswap.API_GetExchangeDataDict"
        StoreDataInCache(key, API_GetExchangeDataDict())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_Defiswap_API_GetExchangeDataDict():
    from Exchanges.defiswap import API_GetExchangeDataDict
    try:
        key = "Exchanges.defiswap.API_GetExchangeDataDict"
        StoreDataInCache(key, API_GetExchangeDataDict())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_Sakeswap_API_GetExchangeDataDict():
    from Exchanges.sakeswap import API_GetExchangeDataDict
    try:
        key = "Exchanges.sakeswap.API_GetExchangeDataDict"
        StoreDataInCache(key, API_GetExchangeDataDict())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_Balancer_API_GetExchangeDataDict():
    from Exchanges.balancer import API_GetExchangeDataDict
    try:
        key = "Exchanges.balancer.API_GetExchangeDataDict"
        StoreDataInCache(key, API_GetExchangeDataDict())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_Curve_API_GetExchangeDataDict():
    from Exchanges.curve import API_GetExchangeDataDict
    try:
        key = "Exchanges.curve.API_GetExchangeDataDict"
        StoreDataInCache(key, API_GetExchangeDataDict())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetAndCache_HistoricalData_SymbolToUsdPrice(symbol, coinMarketCapId):
    from Libraries.core import GetKeyGivenSymbol_HistoricalMarketPriceData_SymbolToUsdPrice
    from Libraries.cryptoCompare import API_GetPriceDuringRangeInHistory_GivenDateTimeFromHistory

    PrintAndLog("GetAndCache_HistoricalData_SymbolToUsdPrice with symbol = " + str(symbol) + ", coinMarketCapId = " + str(coinMarketCapId))
    try:
        key = GetKeyGivenSymbol_HistoricalMarketPriceData_SymbolToUsdPrice(symbol)
        currencyOfReturnPrice = 'usd'

        datetime_begin = datetime.datetime(2017, 1, 1)
        datetime_end = datetime.datetime.now()

        # CoinGecko API
        # resultDict = Libraries.coinGecko.API_GetPriceDuringRangeInHistory_GivenDateTimeRange(datetime_begin, datetime_end, coinMarketCapId, currencyOfReturnPrice)
        # CryptoCompare API
        resultDict = API_GetPriceDuringRangeInHistory_GivenDateTimeFromHistory(datetime_begin, symbol, currencyOfReturnPrice)

        # Store the merged result in the cache
        StoreDataInCache(key, resultDict)
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())
        pass


def GetAndCache_Kyber_GetTradeableReserves():
    from Exchanges.kyber import GetTradeableReserves
    try:
        key = "Exchanges.kyber.GetTradeableReserves"
        StoreDataInCache(key, GetTradeableReserves())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())
        pass


def GetAndCache_GasEstimates_Trades(jData):
    try:
        key = "GasEstimates_Trades"
        StoreDataInCache(key, jData)
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())
        pass


def GetAndCache_Set_RebalancingSets():
    from Exchanges.set import API_GetRebalancingSets

    try:
        key = "Set_RebalancingSets"
        jData_rebalancing_sets = API_GetRebalancingSets()
        # PrintAndLog("jData_rebalancing_sets = " + str(jData_rebalancing_sets))
        StoreDataInCache(key, jData_rebalancing_sets)
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception in GetAndCache_Set_RebalancingSets: " + traceback.format_exc())
        pass


def GetAndCache_API_GetTokenTransferCost():
    from Libraries.tokenTransferCost import API_GetTokenTransferCost
    from Libraries.accounts import TokenDict_Ninja

    try:
        key = "API_GetTokenTransferCost"
        tokenList = []
        for symbol in TokenDict_Ninja:
            tokenList.append(TokenDict_Ninja[symbol].erc20TokenContractAddress.lower())

        PrintAndLog_FuncNameHeader("Calling API_GetTokenTransferCost with tokenList of len " + str(len(tokenList)) + " = " + str(tokenList))
        jData = API_GetTokenTransferCost(tokenList)
        StoreDataInCache(key, jData)
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception in GetAndCache_API_GetTokenTransferCost: " + traceback.format_exc())
        pass


def GetAndCache_HidingBook_GenerateTokenList():
    from Exchanges.hidingBook import GenerateTokenList
    try:
        key = "Exchanges.hidingBook.GenerateTokenList"
        StoreDataInCache(key, GenerateTokenList())
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def Cache_HidingBook_TransactionsDict(rewardsTransactionsDict):
    try:
        key = "Exchanges.hidingBook.RewardsTransactionsDict"
        StoreDataInCache(key, rewardsTransactionsDict)
        PrintAndLog(str(key) + ": Got and cached new data")

    except:
        PrintAndLogError("exception: " + traceback.format_exc())


def GetDecimalsForTokenContract_UseCachingSystem(tokenAddress):
    global Lock_EthereumAssetCache
    global EthereumAssetCache
    from Libraries.core import API_ERC20Contract_GetDecimals, GetEtherContractAddress
    from Exchanges.kyber import EtherToken as KybersEtherToken
    from Exchanges.keeperDAO import EthTokenContract as KeeperDAOsEtherToken

    filename = "EthereumAssetCache"

    cacheExists = DoesCacheExistInCache(filename)
    # PrintAndLog("cacheExists = " + str(cacheExists))

    if not cacheExists:
        EthereumAssetCache = {}
        # Populate it with ether
        address = GetEtherContractAddress().lower()
        EthereumAssetCache[address] = {}
        EthereumAssetCache[address]['decimals'] = 18

        # Populate it with Kyber's ether
        address = KybersEtherToken.lower()
        EthereumAssetCache[address] = {}
        EthereumAssetCache[address]['decimals'] = 18

        # Populate it with KeeperDAO's ether
        address = KeeperDAOsEtherToken.lower()
        EthereumAssetCache[address] = {}
        EthereumAssetCache[address]['decimals'] = 18

        StoreDataInCache(filename, EthereumAssetCache)
        PrintAndLog("GetDecimalsForTokenContract_UseCachingSystem: Initializing")

    else:
        returnValue = None

        # Get the EthereumAssetCache from disk space adn store it in memory adn only access it from memory from now on
        if not EthereumAssetCache:
            EthereumAssetCache = GetDataFromCache(filename)

        needToGetData = False
        needToInsertKey = False

        # PrintAndLog("GetDecimalsForTokenContract_UseCachingSystem: EthereumAssetCache is currently = " + str(EthereumAssetCache))
        # If data in cache
        if tokenAddress.lower() in EthereumAssetCache:
            # PrintAndLog("GetDecimalsForTokenContract_UseCachingSystem: returning data from cache")
            if 'decimals' in EthereumAssetCache[tokenAddress.lower()]:
                returnValue = int(EthereumAssetCache[tokenAddress.lower()]['decimals'])
            else:
                needToGetData = True

        # data not in cache
        else:
            needToGetData = True
            needToInsertKey = True

        if needToGetData:
            PrintAndLog("GetDecimalsForTokenContract_UseCachingSystem: data not in cache, making API call to get it")

            decimals = API_ERC20Contract_GetDecimals(tokenAddress.lower())

            Lock_EthereumAssetCache.acquire()
            try:
                if needToInsertKey:
                    EthereumAssetCache[tokenAddress.lower()] = {}

                EthereumAssetCache[tokenAddress.lower()]['decimals'] = decimals
                # Upon getting new data, always update the cache
                StoreDataInCache(filename, EthereumAssetCache)

            finally:
                Lock_EthereumAssetCache.release()

            returnValue = int(EthereumAssetCache[tokenAddress.lower()]['decimals'])

        return returnValue


def GetSymbolForTokenContract_UseCachingSystem(tokenAddress):
    global Lock_EthereumAssetCache
    global EthereumAssetCache
    from Libraries.core import API_GetERC20Symbol, GetEtherContractAddress, GetKnownHardCodedERC20TokenSymbol
    from Exchanges.kyber import EtherToken as KybersEtherToken
    from Libraries.utils import IsTokenAKnownEtherToken
    from Exchanges.keeperDAO import EthTokenContract as KeeperDAOsEtherToken

    if IsTokenAKnownEtherToken(tokenAddress):
        return "ETH"

    # Handle weird hard coded edge cases where the smart contract developer screwed up
    if GetKnownHardCodedERC20TokenSymbol(tokenAddress):
        return GetKnownHardCodedERC20TokenSymbol(tokenAddress)

    filename = "EthereumAssetCache"

    cacheExists = DoesCacheExistInCache(filename)
    # PrintAndLog("cacheExists = " + str(cacheExists))

    if not cacheExists:
        EthereumAssetCache = {}
        # Populate it with ether
        address = GetEtherContractAddress().lower()
        EthereumAssetCache[address] = {}
        EthereumAssetCache[address]['decimals'] = 18

        # Populate it with Kyber's ether
        address = KybersEtherToken.lower()
        EthereumAssetCache[address] = {}
        EthereumAssetCache[address]['decimals'] = 18

        # Populate it with KeeperDAO's ether
        address = KeeperDAOsEtherToken.lower()
        EthereumAssetCache[address] = {}
        EthereumAssetCache[address]['decimals'] = 18

        StoreDataInCache(filename, EthereumAssetCache)
        PrintAndLog("GetDecimalsForTokenContract_UseCachingSystem: Initializing")

    else:
        returnValue = None

        if not EthereumAssetCache:
            # Get the EthereumAssetCache from disk space and store it in memory and only access it from memory from now on
            EthereumAssetCache = GetDataFromCache(filename)

        needToGetData = False
        needToInsertKey = False

        # PrintAndLog("GetDecimalsForTokenContract_UseCachingSystem: EthereumAssetCache is currently = " + str(EthereumAssetCache))
        # If data in cache
        if tokenAddress.lower() in EthereumAssetCache:
            # PrintAndLog("GetDecimalsForTokenContract_UseCachingSystem: returning data from cache")
            if 'symbol' in EthereumAssetCache[tokenAddress.lower()]:
                returnValue = EthereumAssetCache[tokenAddress.lower()]['symbol']
            else:
                needToGetData = True

        # data not in cache
        else:
            needToGetData = True
            needToInsertKey = True

        if needToGetData:
            PrintAndLog("GetDecimalsForTokenContract_UseCachingSystem: data not in cache, making API call to get it")

            symbol = API_GetERC20Symbol(tokenAddress.lower())
            if not symbol or symbol.isspace():
                # If it's invalid, just use the tokenAddress
                symbol = tokenAddress.lower()

            Lock_EthereumAssetCache.acquire()
            try:
                if needToInsertKey:
                    EthereumAssetCache[tokenAddress.lower()] = {}

                EthereumAssetCache[tokenAddress.lower()]['symbol'] = symbol
                # Upon getting new data, always update the cache
                StoreDataInCache(filename, EthereumAssetCache)

            finally:
                Lock_EthereumAssetCache.release()

            returnValue = EthereumAssetCache[tokenAddress.lower()]['symbol']

        return returnValue


def RegisterAddressesInTransactionCountCache_AllTradingAddresses():
    from Libraries.accounts import NinjaOpAccountDict

    addressList = []

    for name in NinjaOpAccountDict:
        ninjaOpAccount = NinjaOpAccountDict[name]
        addressList.append(ninjaOpAccount.publicAddress.lower())

    PrintAndLog_FuncNameHeader("Calling RegisterAddressesInTransactionCountCache with " + str(len(addressList)) + " addresses, addressList = " + str(addressList))
    RegisterAddressesInTransactionCountCache(addressList)


def RegisterAddressesInTransactionCountCache(addressList):
    for address in addressList:
        RegisterAddressInTransactionCountCache(address)


def RegisterAddressInTransactionCountCache(address):
    global TransactionCountCache
    # global Lock_TransactionCountCache

    if address.lower() not in TransactionCountCache:
        # Lock_TransactionCountCache.acquire()
        # try:
        # Optimization, I should not need to lock here
        TransactionCountCache[address.lower()] = None

        # finally:
        #     Lock_TransactionCountCache.release()


def UpdateTransactionCountCache():
    global TransactionCountCache
    # global Lock_TransactionCountCache

    from Libraries.core import API_GetTransactionCount_Batched

    # I dont think locking here is necessary
    # copy_TransactionCountCache = GetThreadSafeCopyOf_TransactionCountCache()
    # addressList = list(copy_TransactionCountCache.keys())

    addressList = list(TransactionCountCache.keys())

    resultDict = API_GetTransactionCount_Batched(addressList)

    # Lock_TransactionCountCache.acquire()
    # try:
    # Optimization, I should not need to lock here
    TransactionCountCache = resultDict

    # finally:
    #     Lock_TransactionCountCache.release()


def UpdateTransactionCountCache_GivenSubSetOfAddresses(subSetOfAddresses):
    global TransactionCountCache
    # global Lock_TransactionCountCache

    from Libraries.core import API_GetTransactionCount_Batched
    from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings

    PrintAndLog_FuncNameHeader("With subSetOfAddresses of len " + str(len(subSetOfAddresses)) + " = " + str(subSetOfAddresses))
    subSetOfAddresses = ConvertListOfStringsToLowercaseListOfStrings(subSetOfAddresses)
    resultDict = API_GetTransactionCount_Batched(subSetOfAddresses)
    PrintAndLog_FuncNameHeader("resultDict = " + str(resultDict))

    PrintAndLog_FuncNameHeader("TransactionCountCache before has " + str(len(TransactionCountCache)) + " items")

    # Lock_TransactionCountCache.acquire()
    # try:
    # Optimization, I should not need to lock here
    for address in subSetOfAddresses:
        TransactionCountCache[address] = resultDict[address]

    # finally:
    #     Lock_TransactionCountCache.release()

    PrintAndLog_FuncNameHeader("TransactionCountCache after has " + str(len(TransactionCountCache)) + " items")


def UpdateTransactionCountCache_ThenCall_CleanUpOldMempoolTxs():
    from Libraries.mempool import CleanUpOldMempoolTxs

    header_logging = "UpdateTransactionCountCache_ThenCall_CleanUpOldMempoolTxs"
    functionsStartDateTime = datetime.datetime.now()

    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Calling UpdateTransactionCountCache")
    UpdateTransactionCountCache()
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Calling CleanUpOldMempoolTxs")
    CleanUpOldMempoolTxs()
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Finished")


def GetThreadSafeCopyOf_TransactionCountCache():
    global TransactionCountCache
    # global Lock_TransactionCountCache

    # copy_TransactionCountCache = None
    # Lock_TransactionCountCache.acquire()
    # try:
    #     copy_TransactionCountCache = TransactionCountCache.copy()
    #
    # finally:
    #     Lock_TransactionCountCache.release()
    #
    # return copy_TransactionCountCache

    return copy.deepcopy(TransactionCountCache)


def EnsureRegistrationOfAddressesInTransactionCountCache(addressList):
    global TransactionCountCache
    # global Lock_TransactionCountCache

    newlyRegisteredAddresses = []
    for address in addressList:
        # If the address is not registered
        if address.lower() not in TransactionCountCache:
            # PrintAndLog_FuncNameHeader("address was found to not be registered in TransactionCountCache. address = " + str(address))
            # register it
            RegisterAddressInTransactionCountCache(address.lower())
            newlyRegisteredAddresses.append(address)

    PrintAndLog_FuncNameHeader("Registered " + str(len(newlyRegisteredAddresses)) + " new addresses. There are " + str(
        len(TransactionCountCache.keys())) + " total registered addresses")
    return newlyRegisteredAddresses


def GetTransactionCountFromCacheIfPossible(address, doMakeAPICallIfTxCountWasNotCached=True):
    global TransactionCountCache
    # global Lock_TransactionCountCache

    from Libraries.core import API_GetTransactionCount

    # If the address is not registered
    if address.lower() not in TransactionCountCache:
        # PrintAndLog_FuncNameHeader("address was found to not be registered in TransactionCountCache. address = " + str(address))
        # register it
        RegisterAddressInTransactionCountCache(address.lower())
    # Else it's registered
    else:
        # PrintAndLog_FuncNameHeader("address is registered in TransactionCountCache. address = " + str(address))
        # Optimization, I should not need to lock here
        returnValue = TransactionCountCache[address.lower()]

        # returnValue = None
        # Lock_TransactionCountCache.acquire()
        # try:
        #     # Copy the value
        #     returnValue = TransactionCountCache[address.lower()]
        #
        # finally:
        #     Lock_TransactionCountCache.release()

        # PrintAndLog_FuncNameHeader("returnValue = " + str(returnValue))
        # Does it have a proper value that's not None?
        if isinstance(returnValue, int):
            # Return the cached value
            # PrintAndLog_FuncNameHeader("returning value from cache. address = " + str(address) + ", returnValue = " + str(returnValue))
            # Tell the caller that the returnValue came from the cache
            return returnValue, True
        else:
            message = "address is registered in TransactionCountCache but the value was found to be None. " \
                      "Is UpdateTransactionCountCache not getting called? address = " + str(address)
            # Periodically tell me that websockets are working
            if Libraries.executeOnInterval.IsTimeToExecute(message, 7200):
                API_PostOperatorNotification(message)

    if doMakeAPICallIfTxCountWasNotCached:
        # If we've made it this far, the transactionCount value was ether not in cache or not registered, so just make the API call to get the value
        # Tell the caller that the returnValue did not come from the cache
        return API_GetTransactionCount(address), False
    else:
        # Tell the caller that the returnValue did not come from the cache
        return None, False
