from Libraries.loggingConfig import PrintAndLog, PrintAndLogError, PrintAndLog_FuncNameHeader
import Libraries.core

import traceback


class ValueDict:
    valueDict = None
    name = "Unnamed"

    def __init__(self, _name):
        self.name = _name
        self.valueDict = {}

    def DoesKeyExist(self, keyToUse):
        if keyToUse in self.valueDict:
            return True
        else:
            return False

    def GetCopyOfValue(self, keyToUse, doReturnZeroIfKeyNotInDict=True):
        copy_valueToUse = None
        # self.lock_valueDict.acquire()
        # try:
        if keyToUse in self.valueDict:
            # OPTIMIZATION, i'm no longer returning a COPY of the value, just returning the value instead.
            # copy_valueToUse = copy.deepcopy(self.valueDict[keyToUse])
            copy_valueToUse = self.valueDict[keyToUse]

        # finally:
        #     self.lock_valueDict.release()

        if not copy_valueToUse:
            if doReturnZeroIfKeyNotInDict:
                # PrintAndLogError("GetCopyOfValue was called on an keyToUse I do not know about. "
                #                  "I'm returning 0 to not break the caller but this could potentially mask a problem so I'm logging an error to cover my ass.")
                copy_valueToUse = 0
            else:
                raise Exception("Key not in dict! TODO: Handle this error.")

        return copy_valueToUse

    def UpdateValue(self, keyValueTupleArray):
        for keyValuePair in keyValueTupleArray:
            try:
                (keyToUse, valueToUse) = keyValuePair
                # PrintAndLog_FuncNameHeader("self.name = " + str(self.name) + ", keyToUse = " + str(keyToUse) + ", valueToUse = " + str(valueToUse))
                self.valueDict[keyToUse] = valueToUse

            except:
                message = "exception in UpdateValue: " + traceback.format_exc()
                Libraries.core.API_PostOperatorNotification(message)
                PrintAndLogError(message)

    def RemoveValue(self, keyToUseArray):
        removeCount = 0
        # self.lock_valueDict.acquire()
        # try:
        for keyToUse in keyToUseArray:
            try:
                if keyToUse in self.valueDict:
                    del self.valueDict[keyToUse]
                    removeCount += 1

            except:
                message = "exception in RemoveValue: " + traceback.format_exc()
                Libraries.core.API_PostOperatorNotification(message)
                PrintAndLogError(message)
                pass

        # finally:
        #     self.lock_valueDict.release()

        # PrintAndLog("RemoveValue " + str(self.name) + " now has " + str(len(self.valueDict)) + " orders after removing " + str(
        #     removeCount) + " of " + str(len(keyToUseArray)) + " orders. ")
