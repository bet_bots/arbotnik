import threading
import time
import datetime
import traceback
import copy

from Libraries.exchanges import EnforceTokenAddressIsERC20Version
from Libraries.loggingConfig import PrintAndLog, PrintAndLogError, PrintAndLog_FuncNameHeader, PrintAndLog_FuncNameHeaderDetailed, PrintAndLog_OrderAggregator
from Libraries.valueDict import ValueDict
from Libraries.market import GetTokenObjectGivenTokenAddress
import Libraries.core
import Libraries.orders
import Libraries.arbyUtility
import Exchanges.radarRelay2
import Exchanges.zrxV2
import Exchanges.zrxV4
import Exchanges.zrxApi
import Exchanges.hidingBook
import Libraries.exceptions
import Libraries.utils

# Keyed by orderHash
AmountFilledValueDict_0x = ValueDict("AmountFilledValueDict_0x")
# Keyed by GetKey_GivenUserAndToken(user, token)
BalanceValueDict_0x = ValueDict("BalanceValueDict_0x")
# Keyed by GetKey_GivenUserAndToken(user, token)
AllowanceValueDict_0xv2 = ValueDict("AllowanceValueDict_0xv2")
# Keyed by GetKey_GivenUserAndToken(user, token)
AllowanceValueDict_0xv4 = ValueDict("AllowanceValueDict_0xv4")

Delimiter_key = "_"


class OrderAggregator:
    # TODO I should be able to replace deepcopy with copy() on orderDict_Asks and orderDict_Bids and get rid of lock/acquire all together
    # Orders keyed by orderHash
    orderDict_Bids = None
    orderDict_Asks = None

    name = None
    quoteToken = None
    exchangeName = None
    baseTokenObject = None

    def __init__(self, _name, _quoteToken, _exchangeName, _baseTokenObject):
        self.name = _name
        self.quoteToken = _quoteToken
        self.exchangeName = _exchangeName
        self.baseTokenObject = _baseTokenObject

        self.orderDict_Bids = {}
        self.orderDict_Asks = {}

        PrintAndLog("OrderAggregator with " + str(self.GetDetails()))

    def GetDetails(self):
        return "OrderAggregator for " + str(self.exchangeName) + ", quoteToken = " + str(self.quoteToken) + ", baseToken = " + \
               str(self.baseTokenObject.erc20TokenContractAddress) + " with " + \
               str(len(self.orderDict_Bids)) + " bids and " + str(len(self.orderDict_Asks)) + " asks"

    def GetCopyOfAllOrders(self):
        return Libraries.core.MergeDictionaries(self.GetCopyOfAllOrders_Asks(), self.GetCopyOfAllOrders_Bids())

    def GetCopyOfAllOrders_Bids(self, doConvertedFromJSONToOrderObjects=True):
        copy_orderDict_Bids = copy.deepcopy(self.orderDict_Bids)

        if not doConvertedFromJSONToOrderObjects:
            # return the actual value which contains a dict of JSON
            return copy_orderDict_Bids
        else:
            # This dict contains JSON and we need to convert all the items to order objects
            returnDict = {}
            for orderHash in copy_orderDict_Bids:
                orderJSON = copy_orderDict_Bids[orderHash]
                order, dontCare, dontCare2 = Get0xOrderObjectGivenOrderJSON(orderJSON, self.exchangeName, self.quoteToken)
                returnDict[orderHash] = order

            # PrintAndLog_FuncNameHeader("Converted dict of orderJSON to dict of order objects")
            # PrintAndLog_FuncNameHeader("copy_orderDict_Bids = " + str(copy_orderDict_Bids))
            # PrintAndLog_FuncNameHeader("returnDict = " + str(returnDict))
            return returnDict

    def GetCopyOfAllOrders_Asks(self, doConvertedFromJSONToOrderObjects=True):
        copy_orderDict_Asks = copy.deepcopy(self.orderDict_Asks)

        if not doConvertedFromJSONToOrderObjects:
            # return the actual value which contains a dict of JSON
            return copy_orderDict_Asks
        else:
            # This dict contains JSON and we need to convert all the items to order objects
            returnDict = {}
            for orderHash in copy_orderDict_Asks:
                orderJSON = copy_orderDict_Asks[orderHash]
                order, dontCare, dontCare2 = Get0xOrderObjectGivenOrderJSON(orderJSON, self.exchangeName, self.quoteToken)
                returnDict[orderHash] = order

            # PrintAndLog_FuncNameHeader("Converted dict of orderJSON to dict of order objects")
            # PrintAndLog_FuncNameHeader("copy_orderDict_Asks = " + str(copy_orderDict_Asks))
            # PrintAndLog_FuncNameHeader("returnDict = " + str(returnDict))
            return returnDict

    # NOTE, I tried putting order objects in the cache but for some reason that was super slow and had bad performance
    # I noticed that putting json strings in the cache had much better performance, so that's what I've gone with
    def UpdateOrdersToCache(self, ordersArray, exchangeName):
        orderJSONList_asks = []
        orderJSONList_bids = []

        for orderJSON in ordersArray:
            try:
                # Do a sanity check to make sure the tokens in this order are base/quote tokens in this orderAggregator object
                makerToken, takerToken, dontCare = Get0xOrderTokensGivenOrderJSON(orderJSON, exchangeName)
                orderTokens = [makerToken.lower(), takerToken.lower()]
                # Skip the orderJSON if it does not match our base & quote
                # PrintAndLog_FuncNameHeader("Sanity checking this orderJSON to see if it is trading the quoteToken and baseToken that correspond with this OrderAggregator")
                # PrintAndLog_FuncNameHeader("   quoteToken = " + str(self.quoteToken.lower()))
                # PrintAndLog_FuncNameHeader("   baseToken = " + str(self.baseTokenObject.erc20TokenContractAddress.lower()))
                # PrintAndLog_FuncNameHeader("   orderTokens = " + str(orderTokens))
                if not Libraries.utils.FindIntersectingValuesInLists([self.quoteToken.lower()], orderTokens):
                    # PrintAndLog_FuncNameHeader("quoteToken was not found in the orderJSON")
                    continue
                if not Libraries.utils.FindIntersectingValuesInLists([self.baseTokenObject.erc20TokenContractAddress.lower()], orderTokens):
                    # PrintAndLog_FuncNameHeader("baseToken was not found in the orderJSON")
                    continue

                # Perform the same sanity check requiring all the tokens in the order to be in the OrderAggregator
                orderAggregatorsTokens = [self.quoteToken.lower(), self.baseTokenObject.erc20TokenContractAddress.lower()]
                # PrintAndLog_FuncNameHeader("Sanity checking this OrderAggregator's quoteToken and baseToken to see if it is trading the orderJSON's tokens")
                # PrintAndLog_FuncNameHeader("   orderAggregatorsTokens = " + str(orderAggregatorsTokens))
                doContinue = False
                for orderToken in orderTokens:
                    # PrintAndLog_FuncNameHeader("   orderToken = " + str(orderToken))
                    if not Libraries.utils.FindIntersectingValuesInLists([orderToken.lower()], orderAggregatorsTokens):
                        # PrintAndLog_FuncNameHeader("orderToken was not found in the orderAggregatorsTokens")
                        doContinue = True

                if doContinue:
                    continue

                orderHash, orderType = Get0xOrderHashAndTypeGivenOrderJSON(orderJSON, exchangeName, self.quoteToken)
                if Libraries.core.IsSell(orderType):
                    orderJSONList_asks.append((orderHash, orderJSON))
                elif Libraries.core.IsBuy(orderType):
                    orderJSONList_bids.append((orderHash, orderJSON))
                else:
                    raise Exception("Not buy or sell")

            except Libraries.exceptions.InvalidOrderType:
                # This is a valid exception, no need to raise, just skip this one
                pass

            except:
                message = "exception in UpdateOrdersToCache: " + traceback.format_exc()
                # Libraries.core.API_PostOperatorNotification(message)
                PrintAndLogError(message)

        # Update lock_orderDict_Asks using orderJSONList_asks
        for item in orderJSONList_asks:
            orderHash, orderJSON = item
            try:
                self.orderDict_Asks[orderHash.lower()] = orderJSON

            except:
                message = "exception in UpdateOrdersToCache: " + traceback.format_exc()
                # Libraries.core.API_PostOperatorNotification(message)
                PrintAndLogError(message)

        # if len(orderJSONList_asks) > 0:
        #     PrintAndLog("UpdateOrdersToCache now has " + str(len(self.orderDict_Asks)) + " ASK orders after updating " + str(
        #         len(ordersArray)) + " new total orders. " + str(self.GetDetails()))

        # Update lock_orderDict_Bids using orderJSONList_bids
        for item in orderJSONList_bids:
            orderHash, orderJSON = item
            try:
                self.orderDict_Bids[orderHash.lower()] = orderJSON

            except:
                message = "exception in UpdateOrdersToCache: " + traceback.format_exc()
                # Libraries.core.API_PostOperatorNotification(message)
                PrintAndLogError(message)

        # if len(orderJSONList_bids) > 0:
        #     PrintAndLog("UpdateOrdersToCache now has " + str(len(self.orderDict_Bids)) + " BID orders after updating " + str(
        #         len(ordersArray)) + " new total orders. " + str(self.GetDetails()))

    def RemoveOrdersFromCache(self, orderHashesArray):
        if len(orderHashesArray) <= 0:
            return

        # before = datetime.datetime.now()
        self.RemoveOrdersFromCache_Asks(orderHashesArray)
        self.RemoveOrdersFromCache_Bids(orderHashesArray)
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog_FuncNameHeader("duration_s of setting dict values = " + str(duration_s))

    def RemoveOrdersFromCache_Bids(self, orderHashesArray):
        global AmountFilledValueDict_0x

        removeCount = 0
        for orderHash in orderHashesArray:
            try:
                if orderHash in self.orderDict_Bids:
                    del self.orderDict_Bids[orderHash.lower()]
                    removeCount += 1
            except:
                message = "exception in RemoveOrdersFromCache_Bids: " + traceback.format_exc()
                Libraries.core.API_PostOperatorNotification(message)
                PrintAndLogError(message)

        try:
            # Since we're removing these orders from the cache, let's remove them from the amountFilled cache as well
            AmountFilledValueDict_0x.RemoveValue(orderHashesArray)

        except:
            message = "exception in RemoveOrdersFromCache_Bids: " + traceback.format_exc()
            Libraries.core.API_PostOperatorNotification(message)
            PrintAndLogError(message)

        # PrintAndLog("RemoveOrdersFromCache_Bids now has " + str(len(self.orderDict_Asks)) + " orders after removing " + str(
        #     removeCount) + " of " + str(len(orderHashesArray)) + " orders.")

    def RemoveOrdersFromCache_Asks(self, orderHashesArray):
        global AmountFilledValueDict_0x

        removeCount = 0
        for orderHash in orderHashesArray:
            try:
                if orderHash in self.orderDict_Asks:
                    del self.orderDict_Asks[orderHash.lower()]
                    removeCount += 1
            except:
                message = "exception in RemoveOrdersFromCache_Asks: " + traceback.format_exc()
                Libraries.core.API_PostOperatorNotification(message)
                PrintAndLogError(message)

        try:
            # Since we're removing these orders from the cache, let's remove them from the amountFilled cache as well
            AmountFilledValueDict_0x.RemoveValue(orderHashesArray)

        except:
            message = "exception in RemoveOrdersFromCache_Asks: " + traceback.format_exc()
            Libraries.core.API_PostOperatorNotification(message)
            PrintAndLogError(message)

        # PrintAndLog("RemoveOrdersFromCache_Asks now has " + str(len(self.orderDict_Asks)) + " orders after removing " + str(
        #     removeCount) + " of " + str(len(orderHashesArray)) + " orders.")


def GetAllowanceValueDict_GivenExchange(exchange):
    from Libraries.exchanges import Exchange_0xv4, Exchange_0xMesh

    global AllowanceValueDict_0xv2
    global AllowanceValueDict_0xv4

    # Order is important in this if/elif statement
    # Because of the inheritance structure, the beginning of the if statement should be the latest/newest classes in the inheritance structure
    # So it should be 0xv4, 0xv3, 0xv2, 0xv1   and NOT 0xv1, 0xv2, 0xv3, 0xv4
    if isinstance(exchange, Exchange_0xv4):
        # PrintAndLog_FuncNameHeader("Returning AllowanceValueDict_0xv4 because exchange = " + str(exchange.exchangeName))
        return AllowanceValueDict_0xv4
    elif isinstance(exchange, Exchange_0xMesh):
        # PrintAndLog_FuncNameHeader("Returning AllowanceValueDict_0xv2 because exchange = " + str(exchange.exchangeName))
        return AllowanceValueDict_0xv2
    else:
        raise Exception("Not yet implemented. Did I forget to add support for a new 0x version?")


def Get0xOrderObjectGivenOrderJSON(orderJSON, exchangeName, quoteToken):
    from Libraries.exchanges import ExchangeName_RadarRelay2, ExchangeName_0xMesh, ExchangeName_HidingBook

    # TODO, pass as parameter the quote token so I can make orderType with respect to a quote token rather than assuming quote = ETH/WETH
    order = None
    orderHash = None
    orderType = None

    # Determine which exchange/relayer/etc this is so I know how to convert it's orderJSON to an order object
    if exchangeName.lower() == ExchangeName_RadarRelay2.lower():
        orderType = Exchanges.zrxV2.GetOrderType_GivenMakerAndTakerTokenAssetData(
            orderJSON['signedOrder']['makerAssetData'], orderJSON['signedOrder']['takerAssetData'], quoteToken)
        orderHash = orderJSON['orderHash'].lower()
        order = Libraries.orders.GetOrderFromOrderJSON_RadarRelay(orderJSON, orderHash + "_" + orderType)
    elif exchangeName.lower() == ExchangeName_0xMesh.lower():
        orderType = Exchanges.zrxV2.GetOrderType_GivenMakerAndTakerTokenAssetData(
            orderJSON['order']['makerAssetData'], orderJSON['order']['takerAssetData'], quoteToken)
        orderHash = orderJSON['metaData']['orderHash'].lower()
        order = Libraries.orders.GetOrderFromOrderJSON_0xMesh(orderJSON, orderHash + "_" + orderType)
    elif exchangeName.lower() == ExchangeName_HidingBook.lower():
        orderType = Exchanges.zrxV2.GetOrderType_GivenMakerAndTakerTokenAddresses(
            orderJSON['order']['makerToken'], orderJSON['order']['takerToken'], quoteToken)
        orderHash = orderJSON['metaData']['orderHash'].lower()
        order = Libraries.orders.GetOrderFromOrderJSON_HidingBook(orderJSON, orderHash + "_" + orderType)
    else:
        raise Exception("Not yet implemented for exchangeName " + str(exchangeName))

    return order, orderHash, orderType


def Get0xOrderHashAndTypeGivenOrderJSON(orderJSON, exchangeName, quoteToken):
    from Libraries.exchanges import ExchangeName_RadarRelay2, ExchangeName_0xMesh, ExchangeName_HidingBook

    orderHash = None
    orderType = None

    makerToken, takerToken, orderHash = Get0xOrderTokensGivenOrderJSON(orderJSON, exchangeName)
    # Determine which exchange/relayer/etc this is so I know how to convert it's orderJSON to an order object
    if exchangeName.lower() == ExchangeName_RadarRelay2.lower():
        orderType = Exchanges.zrxV2.GetOrderType_GivenMakerAndTakerTokenAssetData(makerToken, takerToken, quoteToken)
    elif exchangeName.lower() == ExchangeName_0xMesh.lower():
        orderType = Exchanges.zrxV2.GetOrderType_GivenMakerAndTakerTokenAssetData(makerToken, takerToken, quoteToken)
    elif exchangeName.lower() == ExchangeName_HidingBook.lower():
        orderType = Exchanges.zrxV2.GetOrderType_GivenMakerAndTakerTokenAddresses(makerToken, takerToken, quoteToken)
    else:
        raise Exception("Not yet implemented for exchangeName " + str(exchangeName))

    return orderHash, orderType


def Get0xOrderTokensGivenOrderJSON(orderJSON, exchangeName):
    from Libraries.exchanges import ExchangeName_RadarRelay2, ExchangeName_0xMesh, ExchangeName_HidingBook

    makerToken = None
    takerToken = None
    orderHash = None

    # Determine which exchange/relayer/etc this is so I know how to convert it's orderJSON to an order object
    if exchangeName.lower() == ExchangeName_RadarRelay2.lower():
        makerToken = Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['signedOrder']['makerAssetData'])
        takerToken = Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['signedOrder']['takerAssetData'])
        orderHash = orderJSON['orderHash'].lower()
    elif exchangeName.lower() == ExchangeName_0xMesh.lower():
        makerToken = Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['order']['makerAssetData'])
        takerToken = Exchanges.zrxV2.ExtractAssetAddressFromAssetData(orderJSON['order']['takerAssetData'])
        orderHash = orderJSON['metaData']['orderHash'].lower()
    elif exchangeName.lower() == ExchangeName_HidingBook.lower():
        makerToken = orderJSON['order']['makerToken']
        takerToken = orderJSON['order']['takerToken']
        orderHash = orderJSON['metaData']['orderHash'].lower()
    else:
        raise Exception("Not yet implemented for exchangeName " + str(exchangeName))

    return makerToken, takerToken, orderHash


def PruneOrders_ManyOrderAggregators_0x(orderAggregatorList_0xv2, orderAggregatorList_0xv4,
                                        doCheck_orderInfos, doCheck_balances, doCheck_allowances):
    global AmountFilledValueDict_0x
    global BalanceValueDict_0x
    global AllowanceValueDict_0xv2
    global AllowanceValueDict_0xv4

    beforeDateTime = datetime.datetime.now()
    # PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "Begin")
    # This is optimizing the GetAllOrders call.
    # I want to minimize the number of GetAllOrders calls I make despite making the call for many orderAggregators in the orderAggregatorList
    # To do this, I must keep track of which orderAggregator the orderHashes belong to so that if I need to remove an order I know which orderAggregator to remove the order from
    # This is a bit in efficient looking code, but the makes a much more efficient use of the network/node API calls

    allOrdersDictForAllOrderAggregators_0xv2 = {}
    allOrdersDictForAllOrderAggregators_0xv4 = {}

    orderAggregatorDict = {}
    makerTokenKeyList = []
    makersList = []
    tokensList = []
    tokenDecimalsList = []

    # I'm going to maintain one set of lists (see above lists) and will populate them based on all 0x protocol versions
    # This keeps my code simpler and allows me to save on some API calls and complexities
    # This also makes sense because a maker's same token balance can be used on 0xv2 as well as 0xv4
    # Allowances however are different, but this will still work I'll just have to make some extra calls for allowances that aren't needed
    # Prepare the data we need to prune by analyzing the orders in the orderAggregator
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "orderAggregatorList_0xv2 has " + str(len(orderAggregatorList_0xv2)) + " items")
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "orderAggregatorList_0xv4 has " + str(len(orderAggregatorList_0xv4)) + " items")

    numOfOrdersBefore = 0

    numOfOrdersBefore += len(allOrdersDictForAllOrderAggregators_0xv2)
    UpdateListsFromOrderAggregatorList(orderAggregatorList_0xv2, orderAggregatorDict, allOrdersDictForAllOrderAggregators_0xv2,
                                       makerTokenKeyList, makersList, tokensList, tokenDecimalsList)

    numOfOrdersBefore += len(allOrdersDictForAllOrderAggregators_0xv4)
    UpdateListsFromOrderAggregatorList(orderAggregatorList_0xv4, orderAggregatorDict, allOrdersDictForAllOrderAggregators_0xv4,
                                       makerTokenKeyList, makersList, tokensList, tokenDecimalsList)

    # PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "orderAggregatorDict = " + str(orderAggregatorDict))
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "makerTokenKeyList has " + str(len(makerTokenKeyList)) + " items")
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "makersList has " + str(len(makersList)) + " items")
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "tokensList has " + str(len(tokensList)) + " items")
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "tokenDecimalsList has " + str(len(tokenDecimalsList)) + " items")

    # I need separate objects for 0xv2 and 0xv4 since the order objects are different
    orderList_0xv2 = list(allOrdersDictForAllOrderAggregators_0xv2.values())
    orderList_0xv4 = list(allOrdersDictForAllOrderAggregators_0xv4.values())
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "orderList_0xv2 has " + str(len(orderList_0xv2)) + " items")
    # PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "orderList_0xv4 has " + str(len(orderList_0xv4)) + " items")
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "orderList_0xv4 has " + str(len(orderList_0xv4)) + " items. orderList_0xv4 = " + str(orderList_0xv4))

    threads = []
    resultsDict = {}

    key_tokenBalances = "tokenBalances"
    key_allowances0xv2 = "allowances0xv2"
    key_allowances0xv4 = "allowances0xv4"
    key_Exchanges_zrxV2_API_GetOrderInfo_Batched_Safe = "Exchanges.zrxV2.API_GetOrderInfo_Batched_Safe"
    key_Exchanges_zrxV4_API_GetOrderInfo_Batched_Safe = "Exchanges.zrxV4.API_GetOrderInfo_Batched_Safe"

    if doCheck_orderInfos:
        if len(orderList_0xv2) <= 0:
            resultsDict[key_Exchanges_zrxV2_API_GetOrderInfo_Batched_Safe] = []
        else:
            t = threading.Thread(
                target=Exchanges.zrxV2.API_GetOrderInfo_Batched_Safe,
                args=(orderList_0xv2, key_Exchanges_zrxV2_API_GetOrderInfo_Batched_Safe, resultsDict))
            threads.append(t)
            t.start()

        if len(orderList_0xv4) <= 0:
            resultsDict[key_Exchanges_zrxV4_API_GetOrderInfo_Batched_Safe] = []
        else:
            t = threading.Thread(
                target=Exchanges.zrxV4.API_GetOrderInfo_Batched_Safe,
                args=(orderList_0xv4, Exchanges.zrxV4.OrderObjectType.Class, key_Exchanges_zrxV4_API_GetOrderInfo_Batched_Safe, resultsDict))
            threads.append(t)
            t.start()

    if doCheck_balances:
        t = threading.Thread(
            target=Libraries.core.API_GetTokenBalance_Batched_Safe,
            args=(makersList, tokensList, tokenDecimalsList, resultsDict, key_tokenBalances))
        threads.append(t)
        t.start()

    if doCheck_allowances:
        t = threading.Thread(
            target=Libraries.arbyUtility.API_GetManyAllowances,
            args=(makersList, tokensList, tokenDecimalsList, Exchanges.zrxV2.Contract_ERC20Proxy, resultsDict, key_allowances0xv2))
        threads.append(t)
        t.start()

        t = threading.Thread(
            target=Libraries.arbyUtility.API_GetManyAllowances,
            args=(makersList, tokensList, tokenDecimalsList, Exchanges.zrxV4.Contract_Exchange, resultsDict, key_allowances0xv4))
        threads.append(t)
        t.start()

    for thread in threads:
        thread.join()

    # Extract results from resultsDict

    # Track the number of orders removed
    ordersRemovedCount = 0
    if doCheck_orderInfos:
        # Handle response for getOrderInfo for all 0x protocols
        # Note, different versions have different order state enums
        if len(resultsDict[key_Exchanges_zrxV2_API_GetOrderInfo_Batched_Safe]) > 0:
            ordersRemovedCount += HandleResponse_PruneOrders_ManyOrderAggregators_0x_GetOrderInfo(
                resultsDict[key_Exchanges_zrxV2_API_GetOrderInfo_Batched_Safe],
                orderAggregatorDict, Exchanges.zrxV2.OrderState.Open.value)
            PrintAndLog_FuncNameHeaderDetailed(beforeDateTime,
                                               "ordersRemovedCount = " + str(ordersRemovedCount) +
                                               " after HandleResponse_PruneOrders_ManyOrderAggregators_0x_GetOrderInfo with 0xv2")
        else:
            PrintAndLog_FuncNameHeaderDetailed(beforeDateTime,
                                               "ordersRemovedCount = 0 after HandleResponse_PruneOrders_ManyOrderAggregators_0x_GetOrderInfo with 0xv2")

        if len(resultsDict[key_Exchanges_zrxV4_API_GetOrderInfo_Batched_Safe]) > 0:
            ordersRemovedCount += HandleResponse_PruneOrders_ManyOrderAggregators_0x_GetOrderInfo(
                resultsDict[key_Exchanges_zrxV4_API_GetOrderInfo_Batched_Safe],
                orderAggregatorDict, Exchanges.zrxV4.OrderState.Open.value)
            PrintAndLog_FuncNameHeaderDetailed(beforeDateTime,
                                               "ordersRemovedCount = " + str(ordersRemovedCount) +
                                               " after HandleResponse_PruneOrders_ManyOrderAggregators_0x_GetOrderInfo with 0xv4")
        else:
            PrintAndLog_FuncNameHeaderDetailed(beforeDateTime,
                                               "ordersRemovedCount = 0 after HandleResponse_PruneOrders_ManyOrderAggregators_0x_GetOrderInfo with 0xv4")

    if doCheck_balances:
        # Get the maker balances
        makerBalancesList_etherUnits = resultsDict[key_tokenBalances]

        # Ensure that these lists are the same length, if not something bad happened
        if not Libraries.core.AreListsTheSameLength(
                [makerBalancesList_etherUnits, makerTokenKeyList, makersList, tokensList, tokenDecimalsList]):
            raise Exception("These lists should be of exactly the same length.  Something must have gone wrong")

        HandleResponse_PruneOrders_ManyOrderAggregators_0x_MakerBalances(makerBalancesList_etherUnits, makerTokenKeyList)

    if doCheck_allowances:
        # Get maker allowances
        makerAllowancesList_0xv2_etherUnits = resultsDict[key_allowances0xv2]
        makerAllowancesList_0xv4_etherUnits = resultsDict[key_allowances0xv4]

        # Ensure that these lists are the same length, if not something bad happened
        if not Libraries.core.AreListsTheSameLength(
                [makerAllowancesList_0xv2_etherUnits, makerAllowancesList_0xv4_etherUnits,
                 makerTokenKeyList, makersList, tokensList, tokenDecimalsList]):
            raise Exception("These lists should be of exactly the same length. Something must have gone wrong")

        HandleResponse_PruneOrders_ManyOrderAggregators_0x_Allowances0xv2(makerAllowancesList_0xv2_etherUnits, makerTokenKeyList)

        HandleResponse_PruneOrders_ManyOrderAggregators_0x_Allowances0xv4(makerAllowancesList_0xv4_etherUnits, makerTokenKeyList)

    # PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, "End")
    PrintAndLog_FuncNameHeaderDetailed(beforeDateTime, str(numOfOrdersBefore) + " total orders found before pruning. " +
                                       str(ordersRemovedCount) + " orders were removed by the pruner. doCheck_orderInfos = " +
                                       str(doCheck_orderInfos) + ", doCheck_balances = " + str(doCheck_balances) +
                                       ", doCheck_allowances = " + str(doCheck_allowances))


def UpdateListsFromOrderAggregatorList(orderAggregatorList, orderAggregatorDict, allOrdersDictForAllOrderAggregators,
                                       makerTokenKeyList, makersList, tokensList, tokenDecimalsList):
    for orderAggregator in orderAggregatorList:
        try:
            orderDict = orderAggregator.GetCopyOfAllOrders()
            for orderHash in orderDict:
                order = orderDict[orderHash]
                # This is where I match the hash with the orderAggregator so I know which one it goes with for later in case I need to remove the order from the orderAggregator
                orderAggregatorDict[orderHash] = orderAggregator

                # Maintain a dict of all orders from all orderAggregators so we can batch calls together
                allOrdersDictForAllOrderAggregators[orderHash] = order

                makerTokenKeyToUse = GetKey_GivenUserAndToken(order.maker, order.makerTokenAddress)
                # Only add these items if they're not already in the list.
                if makerTokenKeyToUse not in makerTokenKeyList:
                    makerTokenKeyList.append(makerTokenKeyToUse)
                    makersList.append(order.maker)
                    tokensList.append(order.makerTokenAddress)
                    tokenDecimalsList.append(float("1e" + str(Libraries.core.GetDecimalsForTokenContract(order.makerTokenAddress))))

        except:
            PrintAndLogError("exception in PruneOrders_ManyOrderAggregators_0x: " + traceback.format_exc())


def HandleResponse_PruneOrders_ManyOrderAggregators_0x_GetOrderInfo(returnList, orderAggregatorDict, orderState_Open_value):
    global AmountFilledValueDict_0x

    PrintAndLog_FuncNameHeader("Called with " + str(len(returnList)) + " orderInfos")

    # Prepare a tuple array we can use to call UpdateValue
    amountFilledTupleArray = []
    for resultTuple in returnList:
        (orderState, orderHash, orderTakerAmountFilled_weiUnits) = resultTuple
        amountFilledTupleArray.append((orderHash, orderTakerAmountFilled_weiUnits))

    # Update the values
    AmountFilledValueDict_0x.UpdateValue(amountFilledTupleArray)

    ordersRemovedCount = 0
    # Prune the orderbook based on returnList
    for resultTuple in returnList:
        try:
            (orderState, orderHash, orderTakerAmountFilled_weiUnits) = resultTuple
            # I need to determine which orderAggregator to remove this order from
            # To do this, access the orderAggregatorDict
            orderAggregator_toRemoveFrom = orderAggregatorDict[orderHash]

            # If the order is not fillable
            # We're comparing the enum's int values here
            if orderState != orderState_Open_value:
                # Remove it from the order cache
                # orderHashes are unique so just remove from both lists here to cover all bases
                orderAggregator_toRemoveFrom.RemoveOrdersFromCache([orderHash])
                ordersRemovedCount += 1

            # else:
            #     PrintAndLog_FuncNameHeader("Not pruning this order because it's still fillable, orderHash = " + str(
            #         orderHash) + ", orderAggregator_toRemoveFrom.name = " + str(
            #         orderAggregator_toRemoveFrom.name) + ", orderAggregator_toRemoveFrom.exchangeName = " + str(orderAggregator_toRemoveFrom.exchangeName))

        except:
            PrintAndLogError("exception: " + traceback.format_exc())

    # For logging purposes, track the number of orders removed
    return ordersRemovedCount


def HandleResponse_PruneOrders_ManyOrderAggregators_0x_MakerBalances(makerBalancesList_etherUnits, makerTokenKeyList):
    global BalanceValueDict_0x

    PrintAndLog_FuncNameHeader("Called with " + str(len(makerBalancesList_etherUnits)) + " balances")

    # Prepare a tuple array we can use to call UpdateValue
    tupleArray = []
    for index in range(len(makerBalancesList_etherUnits)):
        keyToUse = makerTokenKeyList[index]
        valueToUse = makerBalancesList_etherUnits[index]
        tupleArray.append((keyToUse, valueToUse))

    # Cache the maker token balances
    BalanceValueDict_0x.UpdateValue(tupleArray)


def HandleResponse_PruneOrders_ManyOrderAggregators_0x_Allowances0xv2(makerAllowancesList_0xv2_etherUnits, makerTokenKeyList):
    global AllowanceValueDict_0xv2

    PrintAndLog_FuncNameHeader("Called with " + str(len(makerAllowancesList_0xv2_etherUnits)) + " balances")

    # Prepare a tuple array we can use to call UpdateValue on AllowanceValueDict_0xv2
    tupleArray = []
    # for index, makerAllowance in enumerate(makerAllowancesList_0xv2_etherUnits):
    for index in range(len(makerAllowancesList_0xv2_etherUnits)):
        keyToUse = makerTokenKeyList[index]
        valueToUse = makerAllowancesList_0xv2_etherUnits[index]
        tupleArray.append((keyToUse, valueToUse))

    # Cache the maker token allowances
    AllowanceValueDict_0xv2.UpdateValue(tupleArray)


def HandleResponse_PruneOrders_ManyOrderAggregators_0x_Allowances0xv4(makerAllowancesList_0xv4_etherUnits, makerTokenKeyList):
    global AllowanceValueDict_0xv4

    PrintAndLog_FuncNameHeader("Called with " + str(len(makerAllowancesList_0xv4_etherUnits)) + " balances")

    # Prepare a tuple array we can use to call UpdateValue on AllowanceValueDict_0xv4
    tupleArray = []
    # for index, makerAllowance in enumerate(makerAllowancesList_0xv4_etherUnits):
    for index in range(len(makerAllowancesList_0xv4_etherUnits)):
        keyToUse = makerTokenKeyList[index]
        valueToUse = makerAllowancesList_0xv4_etherUnits[index]
        tupleArray.append((keyToUse, valueToUse))

    # Cache the maker token allowances
    AllowanceValueDict_0xv4.UpdateValue(tupleArray)


def GetKey_GivenUserAndToken(user, token):
    return (user + Delimiter_key + token).lower()


def CreateOrderAggregator_BasedOnTokenDict_Ninja(exchangeName, quoteToken):
    from Libraries.accounts import TokenDict_Ninja

    # Create an OrderAggregator for each market
    for tokenName in TokenDict_Ninja:
        if TokenDict_Ninja[tokenName].ShouldUseOrderBookAggregator(exchangeName):
            PrintAndLog_FuncNameHeader("CreateOrderAggregator for " + str(exchangeName) + ", quoteToken " +
                                       str(quoteToken) + ", and baseToken " + str(tokenName))
            TokenDict_Ninja[tokenName].CreateOrderAggregator(exchangeName, quoteToken)


def UpdateAllOrderAggregators_ByPollingOrderbooks_0xMesh():
    from Libraries.accounts import TokenDict_Ninja
    from Libraries.exchanges import ExchangeName_0xMesh
    import Exchanges.keeperDAO

    exchangeName = ExchangeName_0xMesh

    before = datetime.datetime.now()
    for quoteToken in Exchanges.keeperDAO.GetBorrowableQuoteTokensList(True):
        for tokenName in TokenDict_Ninja:
            if TokenDict_Ninja[tokenName].ShouldUseOrderBookAggregator(exchangeName):
                # PrintAndLog_FuncNameHeader("for tokenName = " + str(tokenName))
                try:
                    baseToken = TokenDict_Ninja[tokenName].erc20TokenContractAddress.lower()
                    # Get the entire orderbook
                    orderData_jData = Exchanges.zrxApi.API_GetOrderbook(baseToken, quoteToken)
                    # PrintAndLog_FuncNameHeader("orderData_jData for quoteToken " + str(
                    #     quoteToken) + " and baseToken " + str(baseToken) + " has " + str(
                    #     len(orderData_jData['bids']['records'])) + " bids and " + str(len(orderData_jData['asks']['records'])) + " asks")

                    # orderAggregator = TokenDict_Ninja[tokenName].OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()]
                    # PrintAndLog_FuncNameHeader("Populating orderAggregator for Ninja's " + str(
                    #     tokenName) + " on " + str(exchangeName) + " on orderAggregator: " + str(orderAggregator.GetDetails()))

                    # Add the orders to the orderAggregator
                    # orderAggregator.UpdateOrdersToCache(orderData_jData['bids']['records'], exchangeName)
                    # orderAggregator.UpdateOrdersToCache(orderData_jData['asks']['records'], exchangeName)

                    Libraries.orderAggregator.HandleNewOrderDatas_0xv3(orderData_jData['bids']['records'], exchangeName, False)
                    Libraries.orderAggregator.HandleNewOrderDatas_0xv3(orderData_jData['asks']['records'], exchangeName, False)

                except:
                    PrintAndLogError("exception: " + traceback.format_exc())

                # Sleep a bit to be nice to the rate limit
                time.sleep(0.5)

        duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog_FuncNameHeader("duration_s after updating orders = " + str(duration_s))


def UpdateAllOrderAggregators_ByPollingOrderbooks_HidingBook():
    from Libraries.accounts import TokenDict_Ninja
    from Libraries.exchanges import ExchangeName_HidingBook
    import Exchanges.keeperDAO

    exchangeName = ExchangeName_HidingBook

    before = datetime.datetime.now()
    # This API call gets all open orders regardless of base/quote tokens
    jData = Exchanges.hidingBook.API_GetOrders(True)
    PrintAndLog_FuncNameHeader("Found " + str(len(jData)) + " orders on " + str(exchangeName))
    # PrintAndLog_FuncNameHeader("Found " + str(len(jData['orders'])) + " orders on " + str(exchangeName) + ", jData = " + str(jData))
    # Add the orders to the orderAggregator
    # Setting doAlwaysUpdate_AmountFilledValueDict_0x = False here
    # because we don't want to rely on this to change our AmountFilledValueDict_0x because this can be behind by 15++ seconds
    Libraries.orderAggregator.HandleNewOrderDatas_0xv4(jData['orders'], exchangeName, False)

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("duration_s after updating orders = " + str(duration_s))


def PruneAllOrderAggregators_0x(doCheck_orderInfos=True, doCheck_balances=True, doCheck_allowances=True):
    from Libraries.exchanges import ExchangeName_0xMesh, ExchangeName_HidingBook, ExchangeDict
    import Exchanges.keeperDAO

    exchangeName = ExchangeName_0xMesh
    orderAggregatorList_0xv2 = []
    # Check if exchangeName is in ExchangeDict allows me to disable this exchange if I want to without exceptions happening here
    if exchangeName in ExchangeDict:
        for quoteToken in Exchanges.keeperDAO.GetBorrowableQuoteTokensList(True):
            orderAggregatorList_0xv2 = Libraries.core.MergeLists_RemovingDuplicates(orderAggregatorList_0xv2, GetAllOrderAggregators_Tokens(exchangeName, quoteToken))
    PrintAndLog_FuncNameHeader("orderAggregatorList_0xv2 has a total of " + str(len(orderAggregatorList_0xv2)) + " unique orderbook aggregators")

    exchangeName = ExchangeName_HidingBook
    orderAggregatorList_0xv4 = []
    # Check if exchangeName is in ExchangeDict allows me to disable this exchange if I want to without exceptions happening here
    if exchangeName in ExchangeDict:
        for quoteToken in Exchanges.keeperDAO.GetBorrowableQuoteTokensList(True):
            orderAggregatorList_0xv4 = Libraries.core.MergeLists_RemovingDuplicates(orderAggregatorList_0xv4, GetAllOrderAggregators_Tokens(exchangeName, quoteToken))
    PrintAndLog_FuncNameHeader("orderAggregatorList_0xv4 has a total of " + str(len(orderAggregatorList_0xv4)) + " unique orderbook aggregators")

    before = datetime.datetime.now()
    PruneOrders_ManyOrderAggregators_0x(orderAggregatorList_0xv2, orderAggregatorList_0xv4,
                                        doCheck_orderInfos, doCheck_balances, doCheck_allowances)
    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("duration_s after pruning orders = " + str(duration_s))


def GetOrderAggregator_BasedOnTokenDict_Ninja(exchangeName, quoteToken, baseToken):
    # PrintAndLog_FuncNameHeader("exchangeName = " + str(exchangeName) + ", quoteToken = " + str(
    #     quoteToken) + ", baseToken = " + str(baseToken))

    token = GetTokenObjectGivenTokenAddress(baseToken)
    if not token:
        # raise Exception("Did not find a token in TokenDict_Ninja matching baseToken " + str(baseToken))
        return None

    # PrintAndLog_FuncNameHeader("token = " + str(token.tokenName))
    if token and exchangeName in token.OrderAggregatorDict:
        if quoteToken.lower() not in token.OrderAggregatorDict[exchangeName]:
            # PrintAndLog_FuncNameHeader("Did not find quoteToken in token.OrderAggregatorDict[exchangeName], quoteToken = " + str(
            #     quoteToken) + ". I'm expecting this to happen when I get orders that we do not support because we aren't trading this quote token.  "
            #                   "But this should never happen for a quoteToken we are actually trading!")
            return None

        # PrintAndLog_FuncNameHeader("Returning a valid OrderAggregator for token " + str(token.tokenName) + " and quoteToken " + str(quoteToken))
        return token.OrderAggregatorDict[exchangeName][quoteToken.lower()]
    else:
        return None


def GetAllOrderAggregators_Tokens(exchangeName, quoteToken):
    from Libraries.accounts import TokenDict_Ninja

    orderAggregatorsList = []
    for tokenName in TokenDict_Ninja:
        token = TokenDict_Ninja[tokenName]
        if token.ShouldUseOrderBookAggregator(exchangeName):
            if exchangeName in token.OrderAggregatorDict:
                orderAggregator = token.OrderAggregatorDict[exchangeName][quoteToken.lower()]
                if not orderAggregator:
                    message = "This should not be null! tokenName = " + str(tokenName) + ", exchangeName + " + str(exchangeName)
                    PrintAndLogError(message)
                    Libraries.core.API_PostOperatorNotification(message)

                else:
                    orderAggregatorsList.append(orderAggregator)

    return orderAggregatorsList


def HandleNewOrderDatas_0xv3(orderDataList, exchangeName, doAlwaysUpdate_AmountFilledValueDict_0x):
    for orderData in orderDataList:
        try:
            order = orderData['order']
            metaData = orderData['metaData']
            remainingFillableTakerAssetAmount = int(metaData['remainingFillableTakerAssetAmount'])
            orderHash = metaData['orderHash']
            makerToken = Exchanges.zrxV2.ExtractAssetAddressFromAssetData(order['makerAssetData']).lower()
            takerToken = Exchanges.zrxV2.ExtractAssetAddressFromAssetData(order['takerAssetData']).lower()
            maker = order['makerAddress']

            # I need to try the below code using both tokens as the quoteToken because I could have an orderbook aggregator where each token is the quoteToken
            for i in range(0, 2):
                quoteToken = None
                baseToken = None
                if i == 0:
                    quoteToken = makerToken
                    baseToken = takerToken
                elif i == 1:
                    baseToken = makerToken
                    quoteToken = takerToken
                else:
                    raise Exception("You should never have an index this high")

                # PrintAndLog_OrderAggregator("Populating orderAggregator assuming order: quoteToken = " + str(
                #     quoteToken) + ", baseToken = " + str(baseToken))
                # Try and update the orderbook based on the quoteToken baseToken we decided above
                # This is in a try catch because if this one fails I don't want it to prevent hte other one from succeeding
                try:
                    # Get the orderAggregator matching these order addresses
                    orderAggregator = Libraries.orderAggregator.GetOrderAggregator_BasedOnTokenDict_Ninja(exchangeName, quoteToken, baseToken)

                    if not orderAggregator:
                        # PrintAndLog_OrderAggregator("No orderAggregator was found for exchange " + str(
                        #     exchangeName) + " where quoteToken = " + str(quoteToken) + " and baseToken = " + str(baseToken))
                        continue

                    # If the order is fillable
                    if remainingFillableTakerAssetAmount > 0:
                        # PrintAndLog_OrderAggregator("order update! hash = " + str(orderHash))

                        # Update it
                        orderAggregator.UpdateOrdersToCache([orderData], exchangeName)

                        # Also update the AmountFilledValueDict_0x based on remainingFillableTakerAssetAmount
                        # remainingFillableTakerAssetAmount is not the same as what we store in AmountFilledValueDict_0x so I have to do some conversions
                        orderTakerAmountFilled_weiUnits = int(order['takerAssetAmount']) - remainingFillableTakerAssetAmount
                        # PrintAndLog_OrderAggregator("updating takerFilledAmount to AmountFilledValueDict_0x. orderTakerAmountFilled_weiUnits = " + str(
                        #     orderTakerAmountFilled_weiUnits) + ", remainingFillableTakerAssetAmount = " + str(remainingFillableTakerAssetAmount))

                        # Update the orderAggregator
                        # Prepare a tuple array we can use to call UpdateValue
                        keyToUse = orderHash
                        keyExists_AmountFilledValueDict_0x = Libraries.orderAggregator.AmountFilledValueDict_0x.DoesKeyExist(keyToUse)
                        PrintAndLog_OrderAggregator("keyExists_AmountFilledValueDict_0x = " + str(keyExists_AmountFilledValueDict_0x))
                        amountFilledTupleArray = [(orderHash, orderTakerAmountFilled_weiUnits)]
                        if doAlwaysUpdate_AmountFilledValueDict_0x or not keyExists_AmountFilledValueDict_0x:
                            PrintAndLog_OrderAggregator("Updating AmountFilledValueDict_0x with " + str(amountFilledTupleArray))
                            Libraries.orderAggregator.AmountFilledValueDict_0x.UpdateValue(amountFilledTupleArray)
                        else:
                            PrintAndLog_OrderAggregator("NOT Updating AmountFilledValueDict_0x with " + str(amountFilledTupleArray) +
                                                        ", doAlwaysUpdate_AmountFilledValueDict_0x = " + str(doAlwaysUpdate_AmountFilledValueDict_0x) +
                                                        ", keyExists_AmountFilledValueDict_0x = " + str(keyExists_AmountFilledValueDict_0x))

                        # Update the orderAggregator
                        # Making the API call to get the maker balance here would be inefficient, so we're not doing it
                        # Instead, we can either make assumptions or just set it to zero and lean on the pruner get all the balances later
                        keyToUse = Libraries.orderAggregator.GetKey_GivenUserAndToken(maker, makerToken)
                        if not Libraries.orderAggregator.BalanceValueDict_0x.DoesKeyExist(keyToUse):
                            assumedMakerBalance_etherUnits = None
                            if doAlwaysUpdate_AmountFilledValueDict_0x or not keyExists_AmountFilledValueDict_0x:
                                # Assume the value is the remaining fillable amount
                                # Assuming this could get us into trouble, but is probably advantageous most of the time
                                # I have the remainingFillableAmount_takerToken, and need to convert it to remainingFillableAmount_makerToken
                                # Calculate the order's % filled, and use that to convert
                                remainingFillablePercentage = float(remainingFillableTakerAssetAmount) / float(order['takerAssetAmount'])
                                PrintAndLog_OrderAggregator("remainingFillablePercentage = " + str(remainingFillablePercentage))
                                remainingFillableAmount_makerToken_weiUnits = int(remainingFillablePercentage * float(order['makerAssetAmount']))
                                PrintAndLog_OrderAggregator("remainingFillableAmount_makerToken_weiUnits = " + str(remainingFillableAmount_makerToken_weiUnits))
                                remainingFillableAmount_makerToken_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(
                                    remainingFillableAmount_makerToken_weiUnits, makerToken)
                                PrintAndLog_OrderAggregator("remainingFillableAmount_makerToken_etherUnits = " + str(remainingFillableAmount_makerToken_etherUnits))
                                assumedMakerBalance_etherUnits = remainingFillableAmount_makerToken_etherUnits
                            else:
                                # Set the value as zero and let the pruner set it later
                                assumedMakerBalance_etherUnits = 0

                            PrintAndLog_OrderAggregator("assumedMakerBalance_etherUnits = " + str(assumedMakerBalance_etherUnits))
                            balanceTupleArray = [(keyToUse, assumedMakerBalance_etherUnits)]
                            PrintAndLog_OrderAggregator("Updating BalanceValueDict_0x with " + str(balanceTupleArray))
                            Libraries.orderAggregator.BalanceValueDict_0x.UpdateValue(balanceTupleArray)

                        else:
                            PrintAndLog_OrderAggregator("NOT Updating BalanceValueDict_0x with maker = " +
                                                        str(maker) + " and makerToken = " + str(makerToken))

                        # Update the orderAggregator
                        # Prepare a tuple array we can use to call UpdateValue
                        # It's generally safe to assume they've verified that the maker does have sufficient allowance
                        # so I'm going to assume the allowance is the max value
                        makerAllowance_makerToken_weiUnits = Libraries.core.DataF_Int
                        makerAllowance_makerToken_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(makerAllowance_makerToken_weiUnits, makerToken)
                        allowanceTupleArray = [(
                            Libraries.orderAggregator.GetKey_GivenUserAndToken(maker, makerToken),
                            makerAllowance_makerToken_etherUnits)]
                        # PrintAndLog_OrderAggregator("Updating AllowanceValueDict for " + str(exchangeName) + " with " + str(allowanceTupleArray))
                        # Determine which AllowanceValueDict to use from Libraries.orderAggregator
                        allowanceValueDict = Libraries.orderAggregator.GetAllowanceValueDict_GivenExchange(Libraries.exchanges.ExchangeDict[exchangeName])
                        PrintAndLog_OrderAggregator("Updating allowanceValueDict with " + str(allowanceTupleArray))
                        allowanceValueDict.UpdateValue(allowanceTupleArray)

                    # If this order has been canceled or completely filled
                    else:
                        # PrintAndLog_OrderAggregator("An order has expired/canceled/filled, hash = " + str(orderHash))
                        # Remove it
                        orderAggregator.RemoveOrdersFromCache([orderHash])

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    errorMessage = "exception = " + traceback.format_exc() + ", orderData = " + str(orderData)
                    PrintAndLog_OrderAggregator(errorMessage)
                    PrintAndLogError(errorMessage)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            errorMessage = "exception = " + traceback.format_exc() + ", orderData = " + str(orderData)
            PrintAndLog_OrderAggregator(errorMessage)
            PrintAndLogError(errorMessage)


def HandleNewOrderDatas_0xv4(orderDataList, exchangeName, doAlwaysUpdate_AmountFilledValueDict_0x):
    for orderData in orderDataList:
        order = orderData['order']
        metaData = orderData['metaData']
        orderHash = metaData['orderHash']
        remainingFillableAmount_takerToken = int(metaData['remainingFillableAmount_takerToken'])
        # makerBalance_makerToken_weiUnits = int(metaData['makerBalance_makerToken'])
        # makerBalance_makerToken_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(makerBalance_makerToken_weiUnits, order['makerToken'])
        # makerAllowance_makerToken_weiUnits = int(metaData['makerAllowance_makerToken'])
        # makerAllowance_makerToken_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(makerAllowance_makerToken_weiUnits, order['makerToken'])

        # Get the orderAggregator matching these order addresses

        for i in range(0, 2):
            quoteToken = None
            baseToken = None
            if i == 0:
                quoteToken = order['makerToken'].lower()
                baseToken = order['takerToken'].lower()
            elif i == 1:
                baseToken = order['makerToken'].lower()
                quoteToken = order['takerToken'].lower()
            else:
                raise Exception("You should never have an index this high")

            # Try and update the orderbook based on the quoteToken baseToken we decided above
            # This is in a try catch because if this one fails I don't want it to prevent hte other one from succeeding
            try:
                orderAggregator = Libraries.orderAggregator.GetOrderAggregator_BasedOnTokenDict_Ninja(exchangeName, quoteToken, baseToken)

                if not orderAggregator:
                    PrintAndLog_OrderAggregator("No orderAggregator was found for exchange " + str(
                        exchangeName) + " where quoteToken = " + str(quoteToken) + " and baseToken = " + str(baseToken))
                    continue

                # If the order is fillable
                if remainingFillableAmount_takerToken > 0:
                    PrintAndLog_OrderAggregator("order update! hash = " + str(orderHash))

                    # Update it
                    orderAggregator.UpdateOrdersToCache([orderData], exchangeName)

                    # Also update the AmountFilledValueDict_0x based on remainingFillableAmount_takerToken
                    # remainingFillableAmount_takerToken is not the same as what we store in AmountFilledValueDict_0x so I have to do some conversions
                    orderTakerAmountFilled_weiUnits = int(order['takerAmount']) - remainingFillableAmount_takerToken
                    PrintAndLog_OrderAggregator("updating takerFilledAmount to AmountFilledValueDict_0x. orderTakerAmountFilled_weiUnits = " + str(
                        orderTakerAmountFilled_weiUnits) + ", remainingFillableAmount_takerToken = " + str(remainingFillableAmount_takerToken))

                    # Update the orderAggregator
                    # Prepare a tuple array we can use to call UpdateValue
                    keyToUse = orderHash
                    keyExists_AmountFilledValueDict_0x = Libraries.orderAggregator.AmountFilledValueDict_0x.DoesKeyExist(keyToUse)
                    PrintAndLog_OrderAggregator("keyExists_AmountFilledValueDict_0x = " + str(keyExists_AmountFilledValueDict_0x))
                    amountFilledTupleArray = [(orderHash, orderTakerAmountFilled_weiUnits)]
                    if doAlwaysUpdate_AmountFilledValueDict_0x or not keyExists_AmountFilledValueDict_0x:
                        PrintAndLog_OrderAggregator("Updating AmountFilledValueDict_0x with " + str(amountFilledTupleArray))
                        Libraries.orderAggregator.AmountFilledValueDict_0x.UpdateValue(amountFilledTupleArray)
                    else:
                        PrintAndLog_OrderAggregator("NOT Updating AmountFilledValueDict_0x with " + str(amountFilledTupleArray) +
                                                    ", doAlwaysUpdate_AmountFilledValueDict_0x = " + str(doAlwaysUpdate_AmountFilledValueDict_0x) +
                                                    ", keyExists_AmountFilledValueDict_0x = " + str(keyExists_AmountFilledValueDict_0x))

                    # Update the orderAggregator
                    # Making the API call to get the maker balance here would be inefficient, so we're not doing it
                    # Instead, we can either make assumptions or just set it to zero and lean on the pruner get all the balances later
                    keyToUse = Libraries.orderAggregator.GetKey_GivenUserAndToken(order['maker'], order['makerToken'])
                    if not Libraries.orderAggregator.BalanceValueDict_0x.DoesKeyExist(keyToUse):
                        assumedMakerBalance_etherUnits = None
                        if doAlwaysUpdate_AmountFilledValueDict_0x or not keyExists_AmountFilledValueDict_0x:
                            # Assume the value is the remaining fillable amount
                            # Assuming this could get us into trouble, but is probably advantageous most of the time
                            # I have the remainingFillableAmount_takerToken, and need to convert it to remainingFillableAmount_makerToken
                            # Calculate the order's % filled, and use that to convert
                            remainingFillablePercentage = float(remainingFillableAmount_takerToken) / float(order['takerAmount'])
                            PrintAndLog_OrderAggregator("remainingFillablePercentage = " + str(remainingFillablePercentage))
                            remainingFillableAmount_makerToken_weiUnits = int(remainingFillablePercentage * float(order['makerAmount']))
                            PrintAndLog_OrderAggregator("remainingFillableAmount_makerToken_weiUnits = " + str(remainingFillableAmount_makerToken_weiUnits))
                            remainingFillableAmount_makerToken_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(
                                remainingFillableAmount_makerToken_weiUnits, order['makerToken'])
                            PrintAndLog_OrderAggregator("remainingFillableAmount_makerToken_etherUnits = " + str(remainingFillableAmount_makerToken_etherUnits))
                            assumedMakerBalance_etherUnits = remainingFillableAmount_makerToken_etherUnits
                        else:
                            # Set the value as zero and let the pruner set it later
                            assumedMakerBalance_etherUnits = 0

                        PrintAndLog_OrderAggregator("assumedMakerBalance_etherUnits = " + str(assumedMakerBalance_etherUnits))
                        balanceTupleArray = [(keyToUse, assumedMakerBalance_etherUnits)]
                        PrintAndLog_OrderAggregator("Updating BalanceValueDict_0x with " + str(balanceTupleArray))
                        Libraries.orderAggregator.BalanceValueDict_0x.UpdateValue(balanceTupleArray)

                    else:
                        PrintAndLog_OrderAggregator("NOT Updating BalanceValueDict_0x with maker = " + str(order['maker']) +
                                                    " and makerToken = " + str(order['makerToken']))

                    # Update the orderAggregator
                    # Prepare a tuple array we can use to call UpdateValue
                    # It's generally safe to assume they've verified that the maker does have sufficient allowance
                    # so I'm going to assume the allowance is the max value
                    makerAllowance_makerToken_weiUnits = Libraries.core.DataF_Int
                    makerAllowance_makerToken_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(makerAllowance_makerToken_weiUnits, order['makerToken'])
                    allowanceTupleArray = [(
                        Libraries.orderAggregator.GetKey_GivenUserAndToken(order['maker'], order['makerToken']),
                        makerAllowance_makerToken_etherUnits)]
                    PrintAndLog_OrderAggregator("Updating AllowanceValueDict for " + str(exchangeName) + " with " + str(allowanceTupleArray))
                    # Determine which AllowanceValueDict to use from Libraries.orderAggregator
                    allowanceValueDict = Libraries.orderAggregator.GetAllowanceValueDict_GivenExchange(Libraries.exchanges.ExchangeDict[exchangeName])
                    PrintAndLog_OrderAggregator("Updating allowanceValueDict with " + str(allowanceTupleArray))
                    allowanceValueDict.UpdateValue(allowanceTupleArray)

                # If this order has been canceled or completely filled
                else:
                    PrintAndLog_OrderAggregator("An order has expired/canceled/filled, hash = " + str(orderHash))
                    # Remove it
                    orderAggregator.RemoveOrdersFromCache([orderHash])

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                errorMessage = "exception = " + traceback.format_exc() + ", orderData = " + str(orderData)
                PrintAndLog_OrderAggregator(errorMessage)
                PrintAndLogError(errorMessage)
