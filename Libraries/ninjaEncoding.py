from Libraries.loggingConfig import PrintAndLog

import Libraries.core
import Libraries.byteUtils


# TODO Rename the functions from EncodeXYZ to PackXYZ since encoding is optional but it's always packing

def PackOrderSignature(signature, doEncode=True):
    if not doEncode:
        raise Exception("Not yet implemented")

    # bitwise rotate right when encoding
    # bitwise rotate left when decoding
    rotations = 2

    signature = signature.replace("0x", "")
    splitString = Libraries.core.SplitStringIntoChunks(signature, 50)
    # PrintAndLog("splitString = " + str(splitString))
    returnList = []
    for split in splitString:
        split_int = Libraries.core.ConvertHexToInt(split)
        split_int_rotated = Libraries.byteUtils.BitWiseRotateRight(split_int, rotations)
        # PrintAndLog("split of len " + str(len(split)) + " = " + str(split) + ", split_int = " + str(split_int) + ", split_int_rotated = " + str(split_int_rotated))
        returnList.append(split_int_rotated)

    return returnList


def PackBytes(myBytes, doEncode=True):
    # bitwise rotate right when encoding
    # bitwise rotate left when decoding
    rotations = 11

    myBytes = myBytes.replace("0x", "")
    splitString = Libraries.core.SplitStringIntoChunks(myBytes, 64)
    # PrintAndLog("splitString = " + str(splitString))
    returnList = []
    for split in splitString:
        split_int = Libraries.core.ConvertHexToInt(split)
        if doEncode:
            split_int_rotated = Libraries.byteUtils.BitWiseRotateRight(split_int, rotations)
            # PrintAndLog("split of len " + str(len(split)) + " = " + str(split) + ", split_int = " + str(split_int) + ", split_int_rotated = " + str(
            #     split_int_rotated) + ", doEncode = " + str(doEncode))
            returnList.append(split_int_rotated)
        else:
            # PrintAndLog("split of len " + str(len(split)) + " = " + str(split) + ", split_int = " + str(split_int) + ", doEncode = " + str(doEncode))
            returnList.append(split_int)

    return returnList


def PackOrder0xAssetData(zrxAssetData, doEncode=True):
    if not doEncode:
        raise Exception("Not yet implemented")

    # bitwise rotate right when encoding
    # bitwise rotate left when decoding
    rotations = 7

    zrxAssetData = zrxAssetData.replace("0x", "")
    splitString = Libraries.core.SplitStringIntoChunks(zrxAssetData, 36)
    # PrintAndLog("splitString = " + str(splitString))
    returnList = []
    for split in splitString:
        split_int = Libraries.core.ConvertHexToInt(split)
        split_int_rotated = Libraries.byteUtils.BitWiseRotateRight(split_int, rotations)
        # PrintAndLog("split of len " + str(len(split)) + " = " + str(split) + ", split_int = " + str(split_int) + ", split_int_rotated = " + str(split_int_rotated))
        returnList.append(split_int_rotated)

    return returnList


def PackAddress(address, doEncode=True):
    if not doEncode:
        raise Exception("Not yet implemented")

    # bitwise rotate right when encoding
    # bitwise rotate left when decoding
    rotations = 5

    address = address.replace("0x", "")
    address_int = Libraries.core.ConvertHexToInt(address)
    address_int_rotated = Libraries.byteUtils.BitWiseRotateRight(address_int, rotations)
    # Add some obscure number to help make it look weird
    address_int_rotated_added = address_int_rotated + 891754983589020875164519
    # PrintAndLog("address = " + str(address) + ", address_int  = " + str(address_int) + ", address_int_rotated = " + str(
    #     address_int_rotated) + ", address_int_rotated_added = " + str(address_int_rotated_added))
    return address_int_rotated_added


def PackAddressList(addressList, doEncode=True):
    returnList = []
    for address in addressList:
        returnList.append(PackAddress(address, doEncode))

    return returnList


def PackInt(number_int, doEncode=True):
    if not doEncode:
        raise Exception("Not yet implemented")

    # bitwise rotate right when encoding
    # bitwise rotate left when decoding
    rotations = 16

    number_int_rotated = Libraries.byteUtils.BitWiseRotateRight(number_int, rotations)
    # PrintAndLog("number_int = " + str(number_int) + ", number_int_rotated = " + str(number_int_rotated))
    return number_int_rotated


def PackIntList(intList, doEncode=True):
    returnList = []
    for myInt in intList:
        returnList.append(PackInt(myInt, doEncode))

    return returnList
