from threading import Lock

MarketOrderIntentDict = {}
Lock_MarketOrderIntentDict = Lock()


class MarketOrderIntent:
    transactionId = None
    exchange = None
    tokenQuantity = None
    side = None

    def __init__(self, _transactionId, _exchange, _tokenQuantity, _side):
        self.transactionId = _transactionId
        self.exchange = _exchange
        self.tokenQuantity = _tokenQuantity
        self.side = _side
        
    def GetDetails(self):
        return str(self.exchange.exchangeName) + " MarketOrderIntent to " + str(self.side) + " " + str(self.tokenQuantity)


def GetMarketOrderIntent(transactionId):
    global MarketOrderIntentDict
    global Lock_MarketOrderIntentDict

    Lock_MarketOrderIntentDict.acquire()
    try:
        if transactionId in MarketOrderIntentDict:
            return MarketOrderIntentDict[transactionId]

    finally:
        Lock_MarketOrderIntentDict.release()


def AddMarketOrderIntent(marketOrderIntent):
    global MarketOrderIntentDict
    global Lock_MarketOrderIntentDict

    Lock_MarketOrderIntentDict.acquire()
    try:
        MarketOrderIntentDict[marketOrderIntent.transactionId] = marketOrderIntent

    finally:
        Lock_MarketOrderIntentDict.release()


def RemoveMarketOrderIntent(transactionId):
    global MarketOrderIntentDict
    global Lock_MarketOrderIntentDict

    Lock_MarketOrderIntentDict.acquire()
    try:
        if transactionId in MarketOrderIntentDict:
            del MarketOrderIntentDict[transactionId]

    finally:
        Lock_MarketOrderIntentDict.release()
