import Libraries.core

# Dict keyed by block number, value is a dict keyed by trading path string
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader

ArbitrageOpportunitysPerBlockDict = {}


def Set_ArbitrageOpportunityListTo_ArbitrageOpportunitysPerBlockDict(blockNumber, arbitrageOpportunityList):
    global ArbitrageOpportunitysPerBlockDict

    ArbitrageOpportunitysPerBlockDict[blockNumber] = {}

    for arbitrageOpportunity in arbitrageOpportunityList:
        # Update the dict with the arbitrageOpportunity's trade path
        ArbitrageOpportunitysPerBlockDict[blockNumber][arbitrageOpportunity.GenerateDescriptionString_Exchanges().lower()] = True

    # PrintAndLog_FuncNameHeader("ArbitrageOpportunitysPerBlockDict = " + str(ArbitrageOpportunitysPerBlockDict))


def IsArbitrageOpportunityStillCurrentlyAvailable(arbitrageOpportunity):
    global ArbitrageOpportunitysPerBlockDict

    # Use this function to determine whether or not an arbitrageOpportunity is still available
    # This is helpful when considering whether or not to cancel a trade
    # Example, say you're trading at safe low gas prices and the trade may take 5 blocks to mine in
    # Use this function to tell if an arbitrageOpportunity is still available.
    # If it's not available any more, we should cancel our trade so we don't waste gas and miss the trade.

    copy_keysList = list(ArbitrageOpportunitysPerBlockDict.keys()).copy()
    # get the highest block number we have
    copy_keysList.sort(reverse=True)
    highestKnownBlockNumber = copy_keysList[0]
    PrintAndLog_FuncNameHeader("highestKnownBlockNumber = " + str(highestKnownBlockNumber) +
                               ". copy_keysList of len " + str(len(copy_keysList)) + " = " + str(copy_keysList))
    PrintAndLog_FuncNameHeader("ArbitrageOpportunitysPerBlockDict[highestKnownBlockNumber] = " + str(ArbitrageOpportunitysPerBlockDict[highestKnownBlockNumber]))
    # If the arbitrageOpportunity's trade path is found in the dict under the block number highestKnownBlockNumber
    tradePath = arbitrageOpportunity.GenerateDescriptionString_Exchanges()
    if tradePath.lower() in ArbitrageOpportunitysPerBlockDict[highestKnownBlockNumber]:
        # arbitrage is still available
        PrintAndLog_FuncNameHeader("Returning True with arbitrageOpportunity " + str(tradePath))
        return True
    else:
        PrintAndLog_FuncNameHeader("Returning False with arbitrageOpportunity " + str(tradePath))
        return False


def RemoveOldItemsIn_ArbitrageOpportunitysPerBlockDict():
    global ArbitrageOpportunitysPerBlockDict

    blockThreshold = 50
    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    # Find all old blockNumbers and delete them
    blockNumbersToDelete = []
    for blockNumber in ArbitrageOpportunitysPerBlockDict:
        if blockNumber + blockThreshold < latestBlockNumber:
            blockNumbersToDelete.append(blockNumber)

    PrintAndLog_FuncNameHeader("Removing " + str(len(blockNumbersToDelete)) +
                               " items from blockNumbersToDelete. blockNumbersToDelete = " + str(blockNumbersToDelete))
    for blockNumber in blockNumbersToDelete:
        del ArbitrageOpportunitysPerBlockDict[blockNumber]

    PrintAndLog_FuncNameHeader("ArbitrageOpportunitysPerBlockDict keys = " + str(list(ArbitrageOpportunitysPerBlockDict.keys())))
