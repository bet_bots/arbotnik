import datetime
import traceback

import pandas as pd
import numpy as np
import networkx as nx
import math
import bisect
from collections import defaultdict
from io import StringIO

from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError, PrintAndLog_Detailed


def bellman_ford_negative_cycles(g, s):
    """
    Bellman Ford, modified so that it returns cycles.
    Runtime is O(VE).
    :param g: graph
    :type g: networkx weighted DiGraph
    :param s: source vertex
    :type s: str
    :return: all negative-weight cycles reachable from a source vertex
    :rtype: str list (empty if no neg-weight cyc)
    """
    n = len(g.nodes())
    d = defaultdict(lambda: math.inf)  # distances dict
    p = defaultdict(lambda: -1)  # predecessor dict
    d[s] = 0

    for _ in range(n - 1):
        for u, v in g.edges():
            # Bellman-Ford relaxation
            weight = g[u][v]["weight"]
            if d[u] + weight < d[v]:
                d[v] = d[u] + weight
                p[v] = u  # update pred

    # Find cycles if they exist
    all_cycles = []
    seen = defaultdict(lambda: False)

    for u, v in g.edges():
        weight = g[u][v]["weight"]
        # If we can relax further, there must be a neg-weight cycle
        if seen[v]:
            continue

        if d[u] + weight < d[v]:
            cycle = []
            x = v
            while True:
                # Walk back along predecessors until a cycle is found
                seen[x] = True
                cycle.append(x)
                x = p[x]
                if x == v or x in cycle:
                    break
            # Slice to get the cyclic portion
            idx = cycle.index(x)
            cycle.append(x)
            all_cycles.append(cycle[idx:][::-1])
    return all_cycles


def all_negative_cycles(g):
    """
    Get all negative-weight cycles by calling Bellman-Ford on
    each vertex. O(V^2 E)
    :param g: graph
    :type g: networkx weighted DiGraph
    :return: list of negative-weight cycles
    :rtype: list of str list
    """
    all_paths = []
    for v in g.nodes():
        all_paths.append(bellman_ford_negative_cycles(g, v))
    flatten = lambda l: [item for sublist in l for item in sublist]
    return [list(i) for i in set(tuple(j) for j in flatten(all_paths))]


def calculate_arb(cycle, g, verbose=True):
    """
    For a given negative-weight cycle on the log graph, calculate and
    print the arbitrage
    :param cycle: the negative-weight cycle
    :type cycle: list
    :param g: graph
    :type g: networkx weighted DiGraph
    :param verbose: whether to print path and arb
    :type verbose: bool
    :return: fractional value of the arbitrage
    :rtype: float
    """
    total = 0
    for (p1, p2) in zip(cycle, cycle[1:]):
        total += g[p1][p2]["weight"]
    arb = np.exp(-total) - 1
    if verbose:
        PrintAndLog_FuncNameHeader("Path: " + str(cycle))
        PrintAndLog_FuncNameHeader(f"{arb * 100:.2g}%\n")
    return arb


def find_arbitrage(graphArray, tokenArray, find_all=True, sources=None):
    """
    Looks for arbitrage opportunities within a snapshot, i.e negative-weight cycles
    that include the currencies given in the sources list
    :param filename: filename of snapshot, defaults to "snapshot.csv"
    :type filename: str, optional
    :param find_all: whether to find all paths, defaults to False.
                     If false, sources must be provided.
    :type find_all: bool, optional
    :param sources: list of starting nodes – should choose the 'most connected' pairs,
                    defaults to None.
    :type sources: str list, optional
    :return: list of negative-weight cycles, or None if none exist
    :rtype: str list
    """

    # graphArray = [
    #     [1, 0.00261, 0.00282, 0.051],
    #     [398.40637450199205, 1, 1.0, 20.1],
    #     [367.6470588235294, 1.0, 1, None],
    #     [18.51851851851852, 0.05, None, 1]]
    #
    # tokenArray = [
    #     '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',
    #     '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48',
    #     '0x6b175474e89094c44da98b954eedeac495271d0f',
    #     '0x221657776846890989a759ba2973e427dff5c9bb'
    # ]

    # tokenArray = [
    #     'ETH',
    #     'USDC',
    #     'DAI',
    #     'REP'
    # ]

    # ,ETH,TFUEL,VET
    # ETH,,,2.764e-05
    # TFUEL,,,
    # VET,36088.05485384338,,

    graphArray_CSVFormat = ''
    delimiter = ','

    graphArray_CSVFormat += delimiter
    for index, token in enumerate(tokenArray):
        graphArray_CSVFormat += token
        if index < len(tokenArray) - 1:
            graphArray_CSVFormat += delimiter

    graphArray_CSVFormat += '\n'

    for index_rowList, rowList in enumerate(graphArray):
        # print('index_rowList = ', index_rowList, ', rowList = ', rowList)

        graphArray_CSVFormat += tokenArray[index_rowList] + delimiter
        for index_entry, entry in enumerate(rowList):
            # print('index_entry = ', index_entry, ', entry = ', entry)
            doAddDelimiter = True

            if index_entry >= len(rowList) - 1:
                doAddDelimiter = False

            # print('doAddDelimiter = ', doAddDelimiter)

            if entry is not None:
                graphArray_CSVFormat += str(entry)

            if doAddDelimiter:
                graphArray_CSVFormat += delimiter

        if index_rowList < len(graphArray) - 1:
            graphArray_CSVFormat += '\n'

    print("graphArray_CSVFormat = ", graphArray_CSVFormat)

    TESTDATA = StringIO(graphArray_CSVFormat)

    # Read df and convert to negative logs so we can use Bellman Ford
    # Negative weight cycles thus correspond to arbitrage opps
    # Transpose log_df so that graph has same API as the dataframe
    df = pd.read_csv(TESTDATA, header=0, index_col=0)

    # filename = "snapshot.csv"
    filename = "snapshot_joeyz.csv"

    # df = pd.read_csv(filename, header=0, index_col=0)
    # print('df of type ' + str(type(df)) + ' = ', df)
    # print('stuff = ', -np.log(df).fillna(0).T)

    g = nx.DiGraph(-np.log(df).fillna(0).T)

    if nx.negative_edge_cycle(g):
        print("ARBITRAGE FOUND\n" + "=" * 15 + "\n")

        PrintAndLog_FuncNameHeader("find_all = " + str(find_all))
        PrintAndLog_FuncNameHeader("sources = " + str(sources))

        if find_all:
            unique_cycles = all_negative_cycles(g)
            PrintAndLog_FuncNameHeader("unique_cycles when find_all = True, unique_cycles = " + str(unique_cycles))
        else:
            all_paths = []
            for s in sources:
                all_paths.append(bellman_ford_negative_cycles(g, s))
            flatten = lambda l: [item for sublist in l for item in sublist]
            unique_cycles = [list(i) for i in set(tuple(j) for j in flatten(all_paths))]
            PrintAndLog_FuncNameHeader("sources when find_all = False, sources = " + str(sources))
            PrintAndLog_FuncNameHeader("flatten when find_all = False, flatten = " + str(flatten))
            PrintAndLog_FuncNameHeader("unique_cycles when find_all = False, unique_cycles = " + str(unique_cycles))

        for p in unique_cycles:
            try:
                calculate_arb(p, g)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in find_arbitrage, " + traceback.format_exc())

        return unique_cycles

    else:
        print("No arbitrage opportunities")
        return None


# def BruteForceArbitrage(graph, tokenList, verboseLogging=False):
#     vertices = len(graph)
#
#     if verboseLogging:
#         PrintAndLog_FuncNameHeader("tokenList = " + str(tokenList))
#
#     for i in range(1, vertices):
#         if verboseLogging:
#             PrintAndLog_FuncNameHeader("i = " + str(i))
#         for j in range(0, vertices - 1):
#             if verboseLogging:
#                 PrintAndLog_FuncNameHeader("j = " + str(j))
#
#             if i == j:
#                 continue
#             entry = graph[i][j]
#             if verboseLogging:
#                 PrintAndLog_FuncNameHeader("entry = " + str(entry))
#
#             if not entry:
#                 continue
#
#             k = j
#             if verboseLogging:
#                 PrintAndLog_FuncNameHeader("k = " + str(k))
#             for m in range(vertices - 1, 0, -1):
#                 if verboseLogging:
#                     PrintAndLog_FuncNameHeader("m = " + str(m))
#                 if k == m:
#                     continue
#                 compareEntry = graph[k][m]
#                 if verboseLogging:
#                     PrintAndLog_FuncNameHeader("compareEntry = " + str(compareEntry))
#
#                 if not compareEntry:
#                     continue
#
#                 profitPercentage = entry * compareEntry
#                 if verboseLogging:
#                     PrintAndLog_FuncNameHeader("profitPercentage = " + str(profitPercentage) + " which = " + str(entry) + " * " + str(compareEntry))
#                 arbConfirmed = False
#                 if profitPercentage > 1.0:
#                     if verboseLogging:
#                         PrintAndLog_FuncNameHeader("Arbitrage Possibly Found, need to confirm that it ended where it began, "
#                                                    "i = " + str(i) + ", j = " + str(j) + ", k = " + str(k) + ", m = " + str(m))
#
#                     # I think this is requiring that we start and end on the same token
#                     # TODO confirm this
#                     if i == m:
#                         PrintAndLog_FuncNameHeader("!!!!!!!!!!!!!! Arbitrage Confirmed by i==m !!!!!!!!!!!!!!")
#                         PrintAndLog_FuncNameHeader("Path = " + str(tokenList[i]) + " -> " + str(tokenList[j]) + " -> " + str(tokenList[k]) + " -> " + str(tokenList[m]))
#                         arbConfirmed = True
#
#                 if not arbConfirmed:
#                     # Dig another level deeper to find 3 leg trade arbitrage
#                     # TODO make a loop out of this so I can just specify how many legs I want to check
#
#                     n = m
#                     if verboseLogging:
#                         PrintAndLog_FuncNameHeader("n = " + str(n))
#                     for o in range(vertices - 1, 0, -1):
#                         if verboseLogging:
#                             PrintAndLog_FuncNameHeader("o = " + str(o))
#                         if n == o:
#                             continue
#                         compareEntry2 = graph[n][o]
#                         if verboseLogging:
#                             PrintAndLog_FuncNameHeader("compareEntry2 = " + str(compareEntry2))
#
#                         if not compareEntry2:
#                             continue
#
#                         profitPercentage2 = entry * compareEntry * compareEntry2
#                         if verboseLogging:
#                             PrintAndLog_FuncNameHeader("profitPercentage2 = " + str(profitPercentage2) + " which = " + str(entry) + " * " + str(
#                                 compareEntry) + " * " + str(compareEntry2))
#                         arbConfirmed = False
#                         if profitPercentage2 > 1.0:
#                             if verboseLogging:
#                                 PrintAndLog_FuncNameHeader("Arbitrage Possibly Found, need to confirm that it ended where it began, "
#                                                            "i = " + str(i) + ", j = " + str(j) + ", k = " + str(k) + ", m = " + str(m) + ", n = " + str(n) + ", o = " + str(o))
#                             # I think this is requiring that we start and end on the same token
#                             # TODO confirm this
#                             if i == o:
#                                 PrintAndLog_FuncNameHeader("!!!!!!!!!!!!!! Arbitrage Confirmed by i==o !!!!!!!!!!!!!!")
#                                 PrintAndLog_FuncNameHeader("Path = " + str(tokenList[i]) + " -> " + str(tokenList[j]) + " -> " + str(tokenList[k]) + " -> " + str(
#                                     tokenList[m]) + " -> " + str(tokenList[n]) + " -> " + str(tokenList[o]))
#                                 arbConfirmed = True


# def BruteForceArbitrage(graph_rates, graph_exchangeNames, tokenList, sourceTokensList, id,
#                         verboseLogging_rates=True, verboseLogging_indexes=True, verboseLogging_allLists=True,
#                         verboseLogging_resultLists=True, verboseLogging_tokens=True):
#     from Exchanges.ninja import ConvertRateToPrice
#
#     header_logging = "BruteForceArbitrage: " + str(id)
#     functionsStartDateTime = datetime.datetime.now()
#
#     vertices = len(graph_rates)
#
#     if verboseLogging_tokens:
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "tokenList = " + str(tokenList))
#
#     # Convert sourceTokensList to a dict where the index (based on tokenList) is the key and the value is the token
#     sourceTokensDict = {}
#     for sourceToken in sourceTokensList:
#         sourceTokensDict[tokenList.index(sourceToken)] = sourceToken
#
#     if verboseLogging_tokens:
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "sourceTokensDict = " + str(sourceTokensDict))
#
#     pairList = []
#     ratesList = []
#     indexList = []
#     exchangeNamesList = []
#     profitPercentageList = []
#
#     # First layer to begin finding 2-leg arbitrage
#     # for i in range(0, vertices):
#     # TODO why am I starting the for loop at 1 here??
#     for i in range(1, vertices):
#         # Skip all non sourceTokens since we have to start with a token that we have assets for.
#         # For example, there may be arbitrage by starting off by selling some shitcoin, but if we don't have that shitcoin then we cannot sell it as our first trade
#         # if i not in sourceTokensDict:
#         #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#         #                          "Skipping these possible trades because token " + str(tokenList[i]) + " is not in sourceTokensDict")
#         #     continue
#
#         if verboseLogging_indexes:
#             PrintAndLog_Detailed(header_logging, functionsStartDateTime, "i = " + str(i))
#         for j in range(0, vertices - 1):
#             if verboseLogging_indexes:
#                 PrintAndLog_Detailed(header_logging, functionsStartDateTime, "j = " + str(j))
#
#             if i == j:
#                 continue
#             entry = graph_rates[i][j]
#             if verboseLogging_rates:
#                 PrintAndLog_Detailed(header_logging, functionsStartDateTime, "entry = " + str(entry))
#
#             if not entry:
#                 continue
#
#             # Next layer to find 2-leg arbitrage
#             # Optimization, I would normally make this a function and recursively call it to save on code
#             # but functions in python are not optimal in this case since i'm looping and would be calling it thousands or tens of thousands of times
#             k = j
#             if verboseLogging_indexes:
#                 PrintAndLog_Detailed(header_logging, functionsStartDateTime, "k = " + str(k))
#             for m in range(vertices - 1, 0, -1):
#                 if verboseLogging_indexes:
#                     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "m = " + str(m))
#                 if k == m:
#                     continue
#                 compareEntry2 = graph_rates[k][m]
#                 if verboseLogging_rates:
#                     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "compareEntry2 = " + str(compareEntry2))
#
#                 if not compareEntry2:
#                     continue
#
#                 profitPercentage2 = (entry * compareEntry2) - 1
#                 arbConfirmed = False
#                 if profitPercentage2 > 0:
#
#                     # Arbitrage has been found if I start and end at the same token, otherwise I'm still in the middle of chasing down an arbitragable path
#                     if i == m:
#                         arbConfirmed = True
#                         PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                              "!!!!!!!!!!!!!! Arbitrage Confirmed vai 2-leg trade !!!!!!!!!!!!!!")
#                         PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                              "indexes, i = " + str(i) + ", j = " + str(j) + ", k = " + str(k) + ", m = " + str(m))
#                         PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                              "profitPercentage2 = " + str(round(profitPercentage2 * 100, 2)) + " % which = " + str(entry) + " * " + str(compareEntry2))
#                         PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                              "Path = " + str(tokenList[i]) + " -> " + str(tokenList[j]) + " -> " + str(tokenList[k]) + " -> " + str(tokenList[m]))
#                         # Insert sorted
#                         bisect.insort(profitPercentageList, profitPercentage2)
#                         # Get index so we can insert the others
#                         index = profitPercentageList.index(profitPercentage2)
#                         pairList.insert(index, [(tokenList[i], tokenList[j]), (tokenList[k], tokenList[m])])
#                         indexList.insert(index, [(i, j), (k, m)])
#                         ratesList.insert(index, [entry, compareEntry2])
#                         exchangeNamesList.insert(index, [(graph_exchangeNames[i][j], graph_exchangeNames[k][m])])
#
#                 if not arbConfirmed:
#
#                     # Next layer to find 3-leg arbitrage
#                     # Optimization, I would normally make this a function and recursively call it to save on code
#                     # but functions in python are not optimal in this case since i'm looping and would be calling it thousands or tens of thousands of times
#                     n = m
#                     if verboseLogging_indexes:
#                         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "n = " + str(n))
#                     for o in range(vertices - 1, 0, -1):
#                         if verboseLogging_indexes:
#                             PrintAndLog_Detailed(header_logging, functionsStartDateTime, "o = " + str(o))
#                         if n == o:
#                             continue
#                         compareEntry3 = graph_rates[n][o]
#                         if verboseLogging_rates:
#                             PrintAndLog_Detailed(header_logging, functionsStartDateTime, "compareEntry3 = " + str(compareEntry3))
#
#                         if not compareEntry3:
#                             continue
#
#                         profitPercentage3 = (entry * compareEntry2 * compareEntry3) - 1
#                         arbConfirmed = False
#                         if profitPercentage3 > 0:
#                             # Arbitrage has been found if I start and end at the same token, otherwise I'm still in the middle of chasing down an arbitragable path
#                             if i == o:
#                                 arbConfirmed = True
#                                 PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                                      "!!!!!!!!!!!!!! Arbitrage Confirmed vai 3-leg trade !!!!!!!!!!!!!!")
#                                 PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                                      "indexes, "
#                                                      "i = " + str(i) + ", j = " + str(j) + ", k = " + str(k) + ", m = " + str(m) + ", n = " + str(n) + ", o = " + str(o))
#                                 PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                                      "profitPercentage3 = " + str(round(profitPercentage3 * 100, 2)) + " % which = " + str(
#                                                          entry) + " * " + str(compareEntry2) + " * " + str(compareEntry3))
#                                 PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                                      "Path = " + str(tokenList[i]) + " -> " + str(tokenList[j]) + " -> " + str(tokenList[k]) + " -> " + str(
#                                                          tokenList[m]) + " -> " + str(tokenList[n]) + " -> " + str(tokenList[o]))
#                                 # Insert sorted
#                                 bisect.insort(profitPercentageList, profitPercentage3)
#                                 # Get index so we can insert the others
#                                 index = profitPercentageList.index(profitPercentage3)
#                                 pairList.insert(index, [(tokenList[i], tokenList[j]), (tokenList[k], tokenList[m]), (tokenList[n], tokenList[o])])
#                                 indexList.insert(index, [(i, j), (k, m), (n, o)])
#                                 ratesList.insert(index, [entry, compareEntry2, compareEntry3])
#                                 exchangeNamesList.insert(index, [(graph_exchangeNames[i][j], graph_exchangeNames[k][m], graph_exchangeNames[n][o])])
#
#                         if not arbConfirmed:
#                             # Next layer to find 4-leg arbitrage
#                             # Optimization, I would normally make this a function and recursively call it to save on code
#                             # but functions in python are not optimal in this case since i'm looping and would be calling it thousands or tens of thousands of times
#                             p = o
#                             if verboseLogging_indexes:
#                                 PrintAndLog_Detailed(header_logging, functionsStartDateTime, "p = " + str(p))
#                             for q in range(vertices - 1, 0, -1):
#                                 if verboseLogging_indexes:
#                                     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "q = " + str(q))
#                                 if p == q:
#                                     continue
#                                 compareEntry4 = graph_rates[p][q]
#                                 if verboseLogging_rates:
#                                     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "compareEntry4 = " + str(compareEntry4))
#
#                                 if not compareEntry4:
#                                     continue
#
#                                 profitPercentage4 = (entry * compareEntry2 * compareEntry3 * compareEntry4) - 1
#                                 arbConfirmed = False
#                                 if profitPercentage4 > 0:
#
#                                     # Arbitrage has been found if I start and end at the same token, otherwise I'm still in the middle of chasing down an arbitragable path
#                                     if i == q:
#                                         arbConfirmed = True
#                                         PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                                              "!!!!!!!!!!!!!! Arbitrage Confirmed vai 4-leg trade !!!!!!!!!!!!!!")
#                                         PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                                              "indexes, i = " + str(
#                                                                  i) + ", j = " + str(j) + ", k = " + str(k) + ", m = " + str(m) + ", n = " + str(n) + ", o = " + str(
#                                                                  o) + ", p = " + str(p) + ", q = " + str(q))
#                                         PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                                              "profitPercentage4 = " + str(round(profitPercentage4 * 100, 2)) + " % which = " + str(
#                                                                  entry) + " * " + str(compareEntry2) + " * " + str(compareEntry3) + " * " + str(compareEntry3) + " * " + str(compareEntry4))
#                                         PrintAndLog_Detailed(header_logging, functionsStartDateTime,
#                                                              "Path = " + str(tokenList[i]) + " -> " + str(tokenList[j]) + " -> " + str(tokenList[k]) + " -> " + str(
#                                                                  tokenList[m]) + " -> " + str(tokenList[n]) + " -> " + str(tokenList[o]) + " -> " + str(tokenList[p]) + " -> " + str(
#                                                                  tokenList[q]))
#                                         # Insert sorted
#                                         bisect.insort(profitPercentageList, profitPercentage4)
#                                         # Get index so we can insert the others
#                                         index = profitPercentageList.index(profitPercentage4)
#                                         pairList.insert(index, [(tokenList[i], tokenList[j]), (tokenList[k], tokenList[m]), (tokenList[n], tokenList[o]), (tokenList[p], tokenList[q])])
#                                         indexList.insert(index, [(i, j), (k, m), (n, o), (p, q)])
#                                         ratesList.insert(index, [entry, compareEntry2, compareEntry3, compareEntry4])
#                                         exchangeNamesList.insert(
#                                             index,
#                                             [(graph_exchangeNames[i][j], graph_exchangeNames[k][m], graph_exchangeNames[n][o], graph_exchangeNames[p][q])])
#
#     # I must reverse all lists
#     profitPercentageList.reverse()
#     pairList.reverse()
#     ratesList.reverse()
#     indexList.reverse()
#     exchangeNamesList.reverse()
#
#     if verboseLogging_allLists:
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "indexList = " + str(indexList))
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesList = " + str(ratesList))
#
#     if verboseLogging_allLists or verboseLogging_resultLists:
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "profitPercentageList = " + str(profitPercentageList))
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pairList = " + str(pairList))
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "exchangeNamesList = " + str(exchangeNamesList))
#
#     # From the indexList, I can build a sidesList
#     sidesList = []
#     for subIndexList in indexList:
#         # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "subIndexList = " + str(subIndexList))
#         sidesSubList = []
#         for indexPair in subIndexList:
#             # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "indexPair = " + str(indexPair))
#             # Determine if this is to the left or right of the row's 1, and determine if it's above or below the col's 1
#             # Flip this to [1] if this is backwards
#             rowPosition = indexPair[0]
#             colPosition = indexPair[1]
#             onesRowPosition = colPosition
#             onesColPosition = rowPosition
#             # Find 1 position based on this rowPosition
#             # Flip the buy and sell if it's backwards
#             if rowPosition > onesRowPosition or colPosition < onesColPosition:
#                 side = 'sell'
#             elif rowPosition < onesRowPosition or colPosition > onesColPosition:
#                 side = 'buy'
#             else:
#                 raise Exception("Logic is flawed, is the data wrong or is this logic wrong?")
#
#             sidesSubList.append(side)
#
#         # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "sidesSubList = " + str(sidesSubList))
#         sidesList.append(sidesSubList)
#
#     if verboseLogging_allLists or verboseLogging_resultLists:
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "sidesList = " + str(sidesList))
#
#     # Use sidesList and ratesList to generate a pricesList
#     # The difference between a rate and a price is that a rate is inverted based on the side
#     pricesList = []
#     for index_tradingPathsRates, tradingPathsRates in enumerate(ratesList):
#         subPricesList = []
#         for index_rate, rate in enumerate(tradingPathsRates):
#             side = sidesList[index_tradingPathsRates][index_rate]
#             price = ConvertRateToPrice(rate, side)
#             subPricesList.append(price)
#
#         pricesList.append(subPricesList)
#
#     if verboseLogging_allLists or verboseLogging_resultLists:
#         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pricesList = " + str(pricesList))
#
#     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Completed")
#
#     return profitPercentageList, pairList, pricesList, sidesList, exchangeNamesList


def BruteForceArbitrage(graph_rates, graph_exchangeNames, graph_quoteTokens, tokenList, sourceTokensList, id,
                        minProfitPercentageRequirement=0.0, enable4LegTrades=False, verboseLogging_rates=False, verboseLogging_indexes=False,
                        verboseLogging_allLists=False, verboseLogging_resultLists=False, verboseLogging_tokens=False,
                        verboseLogging_arbitrageFindings=False):
    # minProfitPercentageRequirement can be 0 or any number greater than 0.  If you want all possible profitable paths then go with 0
    # but that can lead to suggesting way to many paths that are not actually profitable after gas and other trading fees.
    # so you can do some math and make a slightly above zero minProfitPercentageRequirement and pass it into this function
    # to help reduce the number of results by filtering out paths that will ultimately not be profitable
    # for non zero minProfitPercentageRequirement, it should be based on the source quantity being traded
    # so say you're trading 100 ETH and the gas fee is 0.1 ETH.  minProfitPercentageRequirement should not be zero, but instead be 0.001
    # But this is tricky because it means you'll want to know the gas fee ahead of knowing the path.  But the path dictates the gas fee.
    # So you'll have to make a min estimate of the gas fee ahead of calling this to help you filter out at least some of the trades
    # Just don't be overly aggressive with the estimated gas fee when setting minProfitPercentageRequirement or else you'll miss out on arbitragable paths!

    header_logging = "BruteForceArbitrage: " + str(id)
    functionsStartDateTime = datetime.datetime.now()

    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "graph_rates = " + str(graph_rates))
    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "graph_exchangeNames = " + str(graph_exchangeNames))
    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "graph_quoteTokens = " + str(graph_quoteTokens))
    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "tokenList = " + str(tokenList))
    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "sourceTokensList = " + str(sourceTokensList))
    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "minProfitPercentageRequirement = " + str(minProfitPercentageRequirement))

    vertices = len(graph_rates)

    if verboseLogging_tokens:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "tokenList = " + str(tokenList))

    # Convert sourceTokensList to a dict where the index (based on tokenList) is the key and the value is the token
    sourceTokensDict = {}
    for sourceToken in sourceTokensList:
        sourceTokensDict[tokenList.index(sourceToken)] = sourceToken

    if verboseLogging_tokens:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "sourceTokensDict = " + str(sourceTokensDict))

    pair2dList = []
    rates2dList = []
    index2dList = []
    exchangeNames2dList = []
    quoteTokens2dList = []
    profitPercentageList = []

    # First layer to begin finding 2-leg arbitrage
    for i in range(0, vertices):
        # Skip all non sourceTokens since we have to start with a token that we have assets for.
        # For example, there may be arbitrage by starting off by selling some shitcoin, but if we don't have that shitcoin then we cannot sell it as our first trade
        if i not in sourceTokensDict:
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime,
            #                      "Skipping these possible trades because token " + str(tokenList[i]) + " is not in sourceTokensDict")
            continue

        # if verboseLogging_indexes:
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "i = " + str(i))
        for j in range(0, vertices):
            # if verboseLogging_indexes:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "j = " + str(j))

            if i == j:
                continue
            entry = graph_rates[i][j]
            # if verboseLogging_rates:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "entry = " + str(entry))

            if not entry:
                continue

            # Next layer to find 2-leg arbitrage
            # Optimization, I would normally make this a function and recursively call it to save on code
            # but functions in python are not optimal in this case since i'm looping and would be calling it thousands or tens of thousands of times
            k = j
            # if verboseLogging_indexes:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "k = " + str(k))
            for m in range(vertices - 1, -1, -1):
                # if verboseLogging_indexes:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "m = " + str(m))
                if k == m:
                    continue
                compareEntry2 = graph_rates[k][m]
                # if verboseLogging_rates:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "compareEntry2 = " + str(compareEntry2))

                if not compareEntry2:
                    continue

                profitPercentage2 = (entry * compareEntry2) - 1
                arbConfirmed = False
                if profitPercentage2 > minProfitPercentageRequirement:

                    # Arbitrage has been found if I start and end at the same token, otherwise I'm still in the middle of chasing down an arbitragable path
                    if i == m:
                        arbConfirmed = True
                        # if verboseLogging_arbitrageFindings:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "!!!!!!!!!!!!!! Arbitrage Confirmed vai 2-leg trade !!!!!!!!!!!!!!")
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "indexes, i = " + str(i) + ", j = " + str(j) + ", k = " + str(k) + ", m = " + str(m))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "profitPercentage2 = " + str(round(profitPercentage2 * 100, 2)) + " % which = " + str(entry) + " * " + str(compareEntry2))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "Path = " + str(tokenList[i]) + " -> " + str(tokenList[j]) + " -> " + str(tokenList[k]) + " -> " + str(tokenList[m]))
                        # Insert sorted
                        bisect.insort(profitPercentageList, profitPercentage2)
                        # Get index so we can insert the others
                        index = profitPercentageList.index(profitPercentage2)
                        pair2dList.insert(index, [(tokenList[i], tokenList[j]), (tokenList[k], tokenList[m])])
                        index2dList.insert(index, [(i, j), (k, m)])
                        rates2dList.insert(index, [entry, compareEntry2])
                        exchangeNames2dList.insert(index, [graph_exchangeNames[i][j], graph_exchangeNames[k][m]])
                        quoteTokens2dList.insert(index, [graph_quoteTokens[i][j], graph_quoteTokens[k][m]])

                if not arbConfirmed:

                    # Next layer to find 3-leg arbitrage
                    # Optimization, I would normally make this a function and recursively call it to save on code
                    # but functions in python are not optimal in this case since i'm looping and would be calling it thousands or tens of thousands of times
                    n = m
                    # if verboseLogging_indexes:
                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "n = " + str(n))
                    for o in range(vertices - 1, -1, -1):
                        # if verboseLogging_indexes:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "o = " + str(o))
                        if n == o:
                            continue
                        compareEntry3 = graph_rates[n][o]
                        # if verboseLogging_rates:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "compareEntry3 = " + str(compareEntry3))

                        if not compareEntry3:
                            continue

                        profitPercentage3 = (entry * compareEntry2 * compareEntry3) - 1
                        arbConfirmed = False
                        if profitPercentage3 > minProfitPercentageRequirement:
                            # Arbitrage has been found if I start and end at the same token, otherwise I'm still in the middle of chasing down an arbitragable path
                            if i == o:
                                arbConfirmed = True
                                # if verboseLogging_arbitrageFindings:
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                #                          "!!!!!!!!!!!!!! Arbitrage Confirmed vai 3-leg trade !!!!!!!!!!!!!!")
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                #                          "indexes, "
                                #                          "i = " + str(i) + ", j = " + str(j) + ", k = " + str(k) + ", m = " + str(m) + ", n = " + str(n) + ", o = " + str(o))
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                #                          "profitPercentage3 = " + str(round(profitPercentage3 * 100, 2)) + " % which = " + str(
                                #                              entry) + " * " + str(compareEntry2) + " * " + str(compareEntry3))
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                #                          "Path = " + str(tokenList[i]) + " -> " + str(tokenList[j]) + " -> " + str(tokenList[k]) + " -> " + str(
                                #                              tokenList[m]) + " -> " + str(tokenList[n]) + " -> " + str(tokenList[o]))
                                # Insert sorted
                                bisect.insort(profitPercentageList, profitPercentage3)
                                # Get index so we can insert the others
                                index = profitPercentageList.index(profitPercentage3)
                                pair2dList.insert(index, [(tokenList[i], tokenList[j]), (tokenList[k], tokenList[m]), (tokenList[n], tokenList[o])])
                                index2dList.insert(index, [(i, j), (k, m), (n, o)])
                                rates2dList.insert(index, [entry, compareEntry2, compareEntry3])
                                exchangeNames2dList.insert(index, [graph_exchangeNames[i][j], graph_exchangeNames[k][m], graph_exchangeNames[n][o]])
                                quoteTokens2dList.insert(index, [graph_quoteTokens[i][j], graph_quoteTokens[k][m], graph_quoteTokens[n][o]])

                        # Only consider 4 leg trades if I've enabled it, it hurts performance
                        if enable4LegTrades:
                            if not arbConfirmed:
                                # Next layer to find 4-leg arbitrage
                                # Optimization, I would normally make this a function and recursively call it to save on code
                                # but functions in python are not optimal in this case since i'm looping and would be calling it thousands or tens of thousands of times
                                p = o
                                # if verboseLogging_indexes:
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "p = " + str(p))
                                for q in range(vertices - 1, -1, -1):
                                    # if verboseLogging_indexes:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "q = " + str(q))
                                    if p == q:
                                        continue
                                    compareEntry4 = graph_rates[p][q]
                                    # if verboseLogging_rates:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "compareEntry4 = " + str(compareEntry4))

                                    if not compareEntry4:
                                        continue

                                    profitPercentage4 = (entry * compareEntry2 * compareEntry3 * compareEntry4) - 1
                                    arbConfirmed = False
                                    if profitPercentage4 > minProfitPercentageRequirement:

                                        # Arbitrage has been found if I start and end at the same token, otherwise I'm still in the middle of chasing down an arbitragable path
                                        if i == q:
                                            arbConfirmed = True
                                            # if verboseLogging_arbitrageFindings:
                                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                            #                          "!!!!!!!!!!!!!! Arbitrage Confirmed vai 4-leg trade !!!!!!!!!!!!!!")
                                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                            #                          "indexes, i = " + str(
                                            #                              i) + ", j = " + str(j) + ", k = " + str(k) + ", m = " + str(m) + ", n = " + str(n) + ", o = " + str(
                                            #                              o) + ", p = " + str(p) + ", q = " + str(q))
                                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                            #                          "profitPercentage4 = " + str(round(profitPercentage4 * 100, 2)) + " % which = " + str(
                                            #                              entry) + " * " + str(compareEntry2) + " * " + str(compareEntry3) + " * " + str(
                                            #                              compareEntry3) + " * " + str(compareEntry4))
                                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                            #                          "Path = " + str(tokenList[i]) + " -> " + str(tokenList[j]) + " -> " + str(tokenList[k]) + " -> " + str(
                                            #                              tokenList[m]) + " -> " + str(tokenList[n]) + " -> " + str(tokenList[o]) + " -> " + str(
                                            #                              tokenList[p]) + " -> " + str(tokenList[q]))
                                            # Insert sorted
                                            bisect.insort(profitPercentageList, profitPercentage4)
                                            # Get index so we can insert the others
                                            index = profitPercentageList.index(profitPercentage4)
                                            pair2dList.insert(
                                                index,
                                                [(tokenList[i], tokenList[j]), (tokenList[k], tokenList[m]), (tokenList[n], tokenList[o]), (tokenList[p], tokenList[q])])
                                            index2dList.insert(index, [(i, j), (k, m), (n, o), (p, q)])
                                            rates2dList.insert(index, [entry, compareEntry2, compareEntry3, compareEntry4])
                                            exchangeNames2dList.insert(
                                                index,
                                                [graph_exchangeNames[i][j], graph_exchangeNames[k][m], graph_exchangeNames[n][o], graph_exchangeNames[p][q]])
                                            quoteTokens2dList.insert(
                                                index,
                                                [graph_quoteTokens[i][j], graph_quoteTokens[k][m], graph_quoteTokens[n][o], graph_quoteTokens[p][q]])

    # I must reverse all lists
    profitPercentageList.reverse()
    pair2dList.reverse()
    rates2dList.reverse()
    index2dList.reverse()
    exchangeNames2dList.reverse()
    quoteTokens2dList.reverse()

    if verboseLogging_allLists:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "index2dList = " + str(index2dList))
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "rates2dList = " + str(rates2dList))

    if verboseLogging_allLists or verboseLogging_resultLists:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "profitPercentageList = " + str(profitPercentageList))
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pair2dList = " + str(pair2dList))
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "exchangeNames2dList = " + str(exchangeNames2dList))
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokens2dList = " + str(quoteTokens2dList))

    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Completed")

    if len(profitPercentageList) != len(pair2dList) != len(exchangeNames2dList) != len(quoteTokens2dList):
        raise Exception("Results are invalid because lists are not of the same length")

    return profitPercentageList, pair2dList, rates2dList, exchangeNames2dList, quoteTokens2dList
