import Libraries.core

Topic_Ninja_ProfitFromTrade = '0x4643b65b54e79c3bf1066dbe4300ca46e42ee7ddeaa0ca4c1c43ff74ab241f72'
Topic_0xv4_Fill_RFQ = '0x829fa99d94dc4636925b38632e625736a614c154d55006b7ab6bea979c210c32'


def BuildTopicsArray_OasisDex_LogTake(pair=None, maker=None, taker=None):
    if maker:
        maker = '0x' + maker.replace("0x", "").zfill(Libraries.core.LengthOfDataProperty)

    if taker:
        taker = '0x' + taker.replace("0x", "").zfill(Libraries.core.LengthOfDataProperty)

    topics = [
        '0x3383e3357c77fd2e3a4b30deea81179bc70a795d053d14d5b7f2f01d0fd4596f',
        pair,
        maker,
        taker,
    ]
    # PrintAndLog("BuildTopicsArray_OasisDex_LogTake topics = " + str(topics))
    return topics


def BuildTopicsArray_0xv1_LogFill(maker=None):
    if maker:
        maker = '0x' + maker.replace("0x", "").zfill(Libraries.core.LengthOfDataProperty)

    topics = [
        '0x0d0b9391970d9a25552f37d436d2aae2925e2bfe1b2a923754bada030c498cb3',
        maker,
    ]
    # PrintAndLog("BuildTopicsArray_0xv1_LogFill topics = " + str(topics))
    return topics


def BuildTopicsArray_0xv2_Fill(maker=None):
    if maker:
        maker = '0x' + maker.replace("0x", "").zfill(Libraries.core.LengthOfDataProperty)

    topics = [
        '0x0bcc4c97732e47d9946f229edb95f5b6323f601300e4690de719993f3c371129',
        maker,
    ]
    # PrintAndLog("BuildTopicsArray_0xv2_Fill topics = " + str(topics))
    return topics


def BuildTopicsArray_Kyber_Trade():
    topics = [
        '0x1c8399ecc5c956b9cb18c820248b10b634cca4af308755e07cd467655e8ec3c7'
    ]
    # PrintAndLog("BuildTopicsArray_Kyber_Trade topics = " + str(topics))
    return topics


def BuildTopicsArray_EtherDelta_Trade():
    topics = [
        '0x6effdda786735d5033bfad5f53e5131abcced9e52be6c507b62d639685fbed6d'
    ]
    # PrintAndLog("BuildTopicsArray_EtherDelta_Trade topics = " + str(topics))
    return topics


def BuildTopicsArray_Airswap_Filled():
    topics = [
        '0xe59c5e56d85b2124f5e7f82cb5fcc6d28a4a241a9bdd732704ac9d3b6bfc98ab'
    ]
    # PrintAndLog("BuildTopicsArray_Airswap_Filled topics = " + str(topics))
    return topics


def BuildTopicsArray_Bancor_Conversion():
    topics = [
        '0x276856b36cbc45526a0ba64f44611557a2a8b68662c5388e9fe6d72e86e1c8cb'
    ]
    # PrintAndLog("BuildTopicsArray_Bancor_Convert topics = " + str(topics))
    return topics


def BuildTopicsArray_EtherDelta_Cancel():
    topics = [
        '0x1e0b760c386003e9cb9bcf4fcf3997886042859d9b6ed6320e804597fcdb28b0'
    ]
    # PrintAndLog("BuildTopicsArray_EtherDelta_Cancel topics = " + str(topics))
    return topics


def BuildTopicsArray_0xv1_LogCancel(maker=None):
    if maker:
        maker = '0x' + maker.replace("0x", "").zfill(Libraries.core.LengthOfDataProperty)

    topics = [
        '0x67d66f160bc93d925d05dae1794c90d2d6d6688b29b84ff069398a9b04587131',
        maker,
    ]
    # PrintAndLog("BuildTopicsArray_0xv1_LogCancel topics = " + str(topics))
    return topics


def BuildTopicsArray_0xv2_CancelUpTo(maker=None):
    if maker:
        maker = '0x' + maker.replace("0x", "").zfill(Libraries.core.LengthOfDataProperty)

    topics = [
        '0x82af639571738f4ebd4268fb0363d8957ebe1bbb9e78dba5ebd69eed39b154f0',
        maker,
    ]
    # PrintAndLog("BuildTopicsArray_0xv2_CancelUpTo topics = " + str(topics))
    return topics


def BuildTopicsArray_EtherDelta_Deposit():
    topics = [
        '0xdcbc1c05240f31ff3ad067ef1ee35ce4997762752e3a095284754544f4c709d7'
    ]
    # PrintAndLog("BuildTopicsArray_EtherDelta_Deposit topics = " + str(topics))
    return topics


def BuildTopicsArray_EtherDelta_Withdraw():
    topics = [
        '0xf341246adaac6f497bc2a656f546ab9e182111d630394f0c57c710a59a2cb567'
    ]
    # PrintAndLog("BuildTopicsArray_EtherDelta_Withdraw topics = " + str(topics))
    return topics


def BuildTopicsArray_WETH_WrapEther():
    topics = [
        '0xe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c'
    ]
    # PrintAndLog("BuildTopicsArray_WETH_WrapEther topics = " + str(topics))
    return topics


def BuildTopicsArray_WETH_UnwrapEther():
    topics = [
        '0x7fcf532c15f0a6db0bd6d0e038bea71d30d808c7d98cb3bf7268a95bf5081b65'
    ]
    # PrintAndLog("BuildTopicsArray_WETH_UnwrapEther topics = " + str(topics))
    return topics


def BuildTopicsArray_Set_BidPlaced():
    topics = [
        '0x7964b0abc937d19ea4a78ec85a1dab6930da8281799c700deee860945a8a6c21'
    ]
    # PrintAndLog("BuildTopicsArray_Set_BidPlaced topics = " + str(topics))
    return topics


def BuildTopicsArray_Uniswap_TokenToEthSwapInput():
    topics = [
        '0x7f4091b46c33e918a0f3aa42307641d17bb67029427a5369e54b353984238705'
    ]
    # PrintAndLog("BuildTopicsArray_Set_BidPlaced topics = " + str(topics))
    return topics


def BuildTopicsArray_Uniswap_EthToTokenSwapInput():
    topics = [
        '0xcd60aa75dea3072fbc07ae6d7d856b5dc5f4eee88854f5b4abf7b680ef8bc50f'
    ]
    # PrintAndLog("BuildTopicsArray_Set_BidPlaced topics = " + str(topics))
    return topics


def BuildTopicsArray_UniswapV2_Swap():
    topics = [
        '0xd78ad95fa46c994b6551d0da85fc275fe613ce37657fb8d5e3d130840159d822'
    ]
    # PrintAndLog(" topics = " + str(topics))
    return topics


def BuildTopicsArray_Balancer_NewPool():
    topics = [
        '0x8ccec77b0cb63ac2cafd0f5de8cdfadab91ce656d262240ba8a6343bccc5f945'
    ]
    # PrintAndLog(" topics = " + str(topics))
    return topics


def BuildTopicsArray_Balancer_Swap():
    topics = [
        '0x908fb5ee8f16c6bc9bc3690973819f32a4d4b10188134543c88706e0e1d43378'
    ]
    # PrintAndLog(" topics = " + str(topics))
    return topics


def BuildTopicsArray_KeeperDAO_BorrowProfitShare():
    topics = [
        '0xc14e9e6b4d98a542b05a0f6b64acd5f4cbdb914be88432d87486b664e97071a7'
    ]
    # PrintAndLog(" topics = " + str(topics))
    return topics


def BuildTopicsArray_Curve_Swap():
    topics = [
        '0x8b3e96f2b889fa771c53c981b40daf005f63f637f1869f707052d15a3dd97140'
    ]
    # PrintAndLog(" topics = " + str(topics))
    return topics


def BuildTopicsArray_Ninja_ProfitFromTrade():
    topics = [
        Topic_Ninja_ProfitFromTrade
    ]
    # PrintAndLog(" topics = " + str(topics))
    return topics


def BuildTopicsArray_0xv4_Fill_RFQ():
    topics = [
        Topic_0xv4_Fill_RFQ
    ]
    # PrintAndLog(" topics = " + str(topics))
    return topics
