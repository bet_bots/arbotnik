import datetime
import inspect
import logging
import threading
import time
import traceback
from random import randint
from threading import Lock
import jsonpickle
import pytz

import Libraries.defaults

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

logger_global = None
logger_errors = None
logger_orderbooks_cx = None
logger_websockets = None
logger_mempool = None
logger_stats = None
logger_pricesDicts = None
logger_blocks = None
logger_orderAggregator = None

# I'm putting some z's in front of the file names because it forces them to the bottom of A-Z sort.
CustomThreadNameHeader = 'logfile_zz_'

# Keyed by loggerName
MarketLoggersDict = {}
# Optimization, removing lock
# Lock_MarketLoggersDict = Lock()

LogfileExtension = '.log'


def setup_logger(name, level=logging.INFO, filepath=Libraries.defaults.Directory_Logs):
    filename = name + LogfileExtension
    handler = logging.FileHandler(filepath + "/" + filename)
    handler.setFormatter(formatter)

    logger = logging.getLogger(filename)
    logger.setLevel(level)
    logger.addHandler(handler)
    logger.converter = GetCustomTime

    return logger


def GetCustomTime(*args):
    utc_dt = pytz.utc.localize(datetime.datetime.utcnow())
    my_tz = pytz.timezone("US/Eastern")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


def GenerateHeaderForPrintAndLog(customString=None, blockNumber=True, doAddRandomId=True):
    from Libraries.core import GetThreadSafeCopyOf_LatestBlockNumber_int

    # Use the stack too get the function name of the caller of this function
    returnString = ''

    if customString:
        returnString += customString + " "

    if blockNumber is True:
        returnString += str(GetThreadSafeCopyOf_LatestBlockNumber_int())
        if doAddRandomId:
            returnString += '-'
    elif isinstance(blockNumber, int):
        returnString += str(blockNumber)
        if doAddRandomId:
            returnString += '-'

    if doAddRandomId:
        returnString += str(randint(0, 999999))

    PrintAndLog_FuncNameHeader("generating header with blockNumber = " + str(blockNumber) + ", returnString = " + str(returnString))
    return returnString


def PrintAndLog_FuncNameHeader(message, doShowThreadName=True):
    # Use this logger for when you have a header and a message
    # Use the stack too get the function name of the caller of this function
    stackLocation = inspect.stack()[1][3]
    functionName = stackLocation
    # If the functionName is an init of a class, we don't want ot just print __init__ because that's not very helpful or informative
    # Instead print ClassName:__init__
    if '__init__' in functionName.lower():
        try:
            className = inspect.stack()[1][0].f_locals["self"].__class__.__name__
            # className = stackLocation.f_locals["self"].__class__.__name__
            functionName = className + ':' + functionName

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception in PrintAndLog_FuncNameHeader, " + traceback.format_exc())

    PrintAndLog(
        functionName + ": " + message,
        doShowThreadName)


def PrintAndLog_Header(header, message, doShowThreadName=True):
    # Use this logger for when you have a header and a message
    PrintAndLog(
        header + ": " + message,
        doShowThreadName)


def PrintAndLog_Detailed(header, functionsStartDateTime, message, doShowThreadName=True):
    # Use this logger for when you have a header, a message, and you also want the duration since the beginning of the function to be displayed
    functionsCallDuration_s = round((datetime.datetime.now() - functionsStartDateTime).total_seconds(), 1)
    PrintAndLog(
        header + " " + str(functionsCallDuration_s) + "s: " + message,
        doShowThreadName)


def PrintAndLog_FuncNameHeaderDetailed(functionsStartDateTime, message, doShowThreadName=True):
    # Use this logger for when you have a header and a message
    # Use the stack too get the function name of the caller of this function
    functionName = inspect.stack()[1][3]
    # Use this logger for when you have a header, a message, and you also want the duration since the beginning of the function to be displayed
    functionsCallDuration_s = round((datetime.datetime.now() - functionsStartDateTime).total_seconds(), 1)
    PrintAndLog(
        functionName + " " + str(functionsCallDuration_s) + "s: " + message,
        doShowThreadName)


def LogPricesDict(filename, jData):
    t = threading.Thread(
        target=Private_LogPricesDict,
        args=(filename, jData,))
    t.start()


def Private_LogPricesDict(filename, jData):
    # Sleep a bit so it doesnt interfere with whatever's being processed at this point since this is a critial processing time for Ninja
    time.sleep(1)

    filepath = Libraries.defaults.Directory_Logs_PricesDicts + "/" + filename + ".json"
    file = open(filepath, "w")
    file.write(jsonpickle.encode(jData))
    file.close()


def PrintAndLog(message, doShowThreadName=True):
    global logger_global
    global CustomThreadNameHeader

    if CustomThreadNameHeader in threading.currentThread().getName():
        spitString = threading.currentThread().getName().split(CustomThreadNameHeader)
        marketName = spitString[1]
        logger_toUse = GetLogger(GetLoggerName(marketName))

        if doShowThreadName:
            # Add the marketName to the message
            message = "(" + marketName + ") " + message
    else:
        logger_toUse = logger_global

        if doShowThreadName:
            # Add the thread name to the message
            message = "(" + threading.currentThread().getName() + ") " + message

    print(message)

    logger_toUse.info(message)


def PrintAndLogError(message):
    global logger_errors
    global logger_global

    print(message)
    logger_errors.error(message)
    logger_global.error(message)


def PrintAndLog_Orderbooks_CX(message):
    global logger_orderbooks_cx

    # print(message)
    logger_orderbooks_cx.info(message)


def PrintAndLog_Websockets(message):
    global logger_websockets

    # print(message)
    logger_websockets.info(message)


def PrintAndLog_Mempool(message):
    global logger_mempool

    # print(message)
    logger_mempool.info(message)


def PrintAndLog_PricesDicts(message):
    global logger_pricesDicts

    # print(message)
    logger_pricesDicts.info(message)


def PrintAndLog_Blocks(message):
    global logger_blocks

    # print(message)
    logger_blocks.info(message)


def PrintAndLog_Stats(message):
    global logger_stats

    # print(message)
    logger_stats.info(message)


def PrintAndLog_OrderAggregator(message):
    global logger_orderAggregator

    # print(message)
    logger_orderAggregator.info(message)


def GetLoggerName(subName):
    global CustomThreadNameHeader

    # NOTE: Do not make a PrintAndLog in this function
    return CustomThreadNameHeader + subName


def RegisterLogger(loggerName, filepath=Libraries.defaults.Directory_Logs):
    global MarketLoggersDict
    # Optimization, removing lock
    # global Lock_MarketLoggersDict

    # NOTE: Do not make a PrintAndLog in this function
    print("RegisterLogger with loggerName = " + str(loggerName))
    print("RegisterLogger with filepath = " + str(filepath))
    # loggerFilename = 'logfile_zz_' + loggerName

    # Lock_MarketLoggersDict.acquire()
    # try:
    if loggerName not in MarketLoggersDict:
        MarketLoggersDict[loggerName] = setup_logger(loggerName, logging.DEBUG, filepath)
        print("RegisterLogger: successfully registered " + str(loggerName))
    # else:
    #     print("RegisterLogger: previously registered " + str(loggerName))

    # finally:
    #     Lock_MarketLoggersDict.release()


def GetLogger(loggerName):
    global MarketLoggersDict
    # Optimization, removing lock
    # global Lock_MarketLoggersDict

    returnLogger = None
    # Lock_MarketLoggersDict.acquire()
    # try:
    returnLogger = MarketLoggersDict[loggerName]

    # finally:
    #     Lock_MarketLoggersDict.release()

    return returnLogger


def InitLogging():
    global logger_global
    global logger_errors
    global logger_orderbooks_cx
    global logger_websockets
    global logger_mempool
    global logger_stats
    global logger_pricesDicts
    global logger_blocks
    global logger_orderAggregator

    # NOTE: Do not make a PrintAndLog in this function
    logger_global = setup_logger('logfile_general', logging.DEBUG)
    logger_errors = setup_logger('logfile_errors', logging.DEBUG)
    logger_orderbooks_cx = setup_logger('logfile_orderbooks_cx', logging.DEBUG)
    logger_websockets = setup_logger('logfile_websockets', logging.DEBUG)
    logger_mempool = setup_logger('logfile_mempool', logging.DEBUG)
    logger_stats = setup_logger('logfile_stats', logging.DEBUG)
    logger_pricesDicts = setup_logger('logfile_pricesDicts', logging.DEBUG)
    logger_blocks = setup_logger('logfile_blocks', logging.DEBUG)
    logger_orderAggregator = setup_logger('logfile_orderAggregator', logging.DEBUG)

    # requests spams the logging module with stuff I don't need, so don't show them unless it's a warning
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("websocket").setLevel(logging.WARNING)


def GetDateTimeStringFromLogFileLine(line):
    splitString = line.split(' ')
    return splitString[0] + ' ' + splitString[1]
