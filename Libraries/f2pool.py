# F2Pool tx accelerator api
#
# POST http://www.f2pool.com/pushtx?currency=ethereum
# application/json
# {
#   "sender": "KeeperDAO",
#   "secret": "4a638db83bedb37fdcb9fed3c6ecff15597d2092c465709d2363882b9d736aea",
#   "txid": "hex",
#   "timeout": 30,
# }
#
# {
#   "sender": "KeeperDAO",
#   "secret": "4a638db83bedb37fdcb9fed3c6ecff15597d2092c465709d2363882b9d736aea",
#   "txpair": ["tx1", "tx2"],
#   "timeout": 30
# }
#
# txpair is ordered, for tx1 to get mined in first and tx2 to get mined in second
import json

import requests

from Libraries.core import Headers, RequestTimeout_seconds
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader

Url_Base = "http://www.f2pool.com/"
Url_PushTx = Url_Base + "pushtx?currency=ethereum"
Sender = "KeeperDAO"
Secret = "4a638db83bedb37fdcb9fed3c6ecff15597d2092c465709d2363882b9d736aea"


def API_PushTx_Single(txHash, timeout_s):
    payload = {
        "sender": Sender,
        "secret": Secret,
        "txid": txHash,
        "timeout": int(timeout_s),
    }
    PrintAndLog_FuncNameHeader("payload = " + str(payload))
    url = Url_PushTx
    PrintAndLog_FuncNameHeader("url = " + str(url))

    response = requests.post(url, data=json.dumps(payload), headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_PushTx_OrderedPair(txHash_1, txHash_2, timeout_s):
    payload = {
        "sender": Sender,
        "secret": Secret,
        "txpair": [txHash_1, txHash_2],
        "timeout": int(timeout_s),
    }
    PrintAndLog_FuncNameHeader("payload = " + str(payload))
    url = Url_PushTx
    PrintAndLog_FuncNameHeader("url = " + str(url))

    response = requests.post(url, data=json.dumps(payload), headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def GetTimeoutForTrade():
    return 40


def GetTimeoutForMintGasTokens():
    return 3600
