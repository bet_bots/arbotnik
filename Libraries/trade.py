from enum import Enum
from threading import Lock
import numpy
import datetime
import traceback
import copy

from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader
from Libraries.core import API_PostOperatorNotification
import Libraries.gasStation
import Libraries.executeOnInterval
import Libraries.utils
import Libraries.defaults
import Exchanges.zrxV4
import Exchanges.keeperDAO

# This list will keep track of order cancels so I can see how much i'm spending on cancels
OrderCancelTrackingList = []
Lock_OrderCancelTrackingList = Lock()

DateTimeOfLast_ConsiderAnalyzingOrderCancelTrackingList = None
Interval_seconds_ConsiderAnalyzingOrderCancelTrackingList = 1800

# Ninja needs to keep track of which transactionId was filling which orderHash. In case a transaction reverted I need to know which orderHash to blacklist
# Key is a txHash, value is a list of orderHashes associated.  So in case a transaction is reverted we can do things like blacklist all the orderhashes associated with it
TxHashOrderHashListDict = {}


class GetMarketPriceTechnique_WithinOneSingleExchange(Enum):
    consideringBuyOrdersOnly = 1
    consideringSellOrdersOnly = 2
    consideringBothBuyAndSellOrders = 3


class OrderCancel_ForTracking:
    market = None
    orderPropertySet = None
    exchangeDX = None
    estimatedGasCost_eth = None

    def __init__(self, _market, _orderPropertySet, _exchangeDX, _estimatedGasCost_eth):
        self.market = _market
        self.orderPropertySet = _orderPropertySet
        self.exchangeDX = _exchangeDX
        self.estimatedGasCost_eth = _estimatedGasCost_eth


class ExchangePrice:
    price = None
    exchange = None

    def __init__(self, _price, _exchange):
        self.price = _price
        self.exchange = _exchange

    def GetDetails(self):
        return "ExchangePrice: " + str(self.exchange.exchangeName) + " at price = " + str(self.price)


def ConsiderAnalyzingOrderCancelTrackingList():
    global DateTimeOfLast_ConsiderAnalyzingOrderCancelTrackingList
    global Interval_seconds_ConsiderAnalyzingOrderCancelTrackingList
    global OrderCancelTrackingList
    global Lock_OrderCancelTrackingList

    if not DateTimeOfLast_ConsiderAnalyzingOrderCancelTrackingList or (
            (datetime.datetime.now() - DateTimeOfLast_ConsiderAnalyzingOrderCancelTrackingList).total_seconds() > Interval_seconds_ConsiderAnalyzingOrderCancelTrackingList):
        DateTimeOfLast_ConsiderAnalyzingOrderCancelTrackingList = datetime.datetime.now()

        try:
            cancelCount = None
            sum_estimatedGasCost_eth = 0
            # These cancelOccurrenceDict service the purpose of counting occurrences of cancel properties
            # The key is the property, the value is the count
            cancelOccurrenceDict_minimumProfitRequirement = {}
            cancelOccurrenceDict_duration = {}
            cancelOccurrenceDict_marketName = {}

            Lock_OrderCancelTrackingList.acquire()
            try:
                cancelCount = len(OrderCancelTrackingList)
                for cancel in OrderCancelTrackingList:
                    PrintAndLog("Iterating over cancel: " + str(cancel.market.marketName) + " cancel.estimatedGasCost_eth = " + str(cancel.estimatedGasCost_eth))
                    sum_estimatedGasCost_eth += cancel.estimatedGasCost_eth

                    # Init the key/value in the dict if it doesn't already exist
                    if cancel.orderPropertySet.minimumProfitRequirement not in cancelOccurrenceDict_minimumProfitRequirement:
                        cancelOccurrenceDict_minimumProfitRequirement[cancel.orderPropertySet.minimumProfitRequirement] = 0

                    # Init the key/value in the dict if it doesn't already exist
                    if cancel.orderPropertySet.duration not in cancelOccurrenceDict_duration:
                        cancelOccurrenceDict_duration[cancel.orderPropertySet.duration] = 0

                    # Init the key/value in the dict if it doesn't already exist
                    if cancel.market.marketName not in cancelOccurrenceDict_marketName:
                        cancelOccurrenceDict_marketName[cancel.market.marketName] = 0

                    cancelOccurrenceDict_minimumProfitRequirement[cancel.orderPropertySet.minimumProfitRequirement] += cancel.estimatedGasCost_eth
                    cancelOccurrenceDict_duration[cancel.orderPropertySet.duration] += cancel.estimatedGasCost_eth
                    cancelOccurrenceDict_marketName[cancel.market.marketName] += cancel.estimatedGasCost_eth

            finally:
                Lock_OrderCancelTrackingList.release()

            scriptRunningDuration_seconds = Libraries.utils.GetTimeSinceNinjaLaunched_seconds()
            scriptRunningDuration_minutes = scriptRunningDuration_seconds / float(60)
            scriptRunningDuration_hours = scriptRunningDuration_minutes / float(60)
            scriptRunningDuration_days = scriptRunningDuration_hours / float(24)
            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: scriptRunningDuration_seconds = " + str(scriptRunningDuration_seconds))
            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: scriptRunningDuration_minutes = " + str(scriptRunningDuration_minutes))
            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: scriptRunningDuration_hours = " + str(scriptRunningDuration_hours))
            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: scriptRunningDuration_days = " + str(scriptRunningDuration_days))

            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: cancelOccurrenceDict_minimumProfitRequirement = " + str(cancelOccurrenceDict_minimumProfitRequirement))
            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: cancelOccurrenceDict_duration = " + str(cancelOccurrenceDict_duration))
            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: cancelOccurrenceDict_marketName = " + str(cancelOccurrenceDict_marketName))

            cancelOccurrenceDict_averagePerDay_minimumProfitRequirement = {}
            cancelOccurrenceDict_averagePerDay_duration = {}
            cancelOccurrenceDict_averagePerDay_marketName = {}
            # Take the value and average it over the number of days this script has been running
            for key in cancelOccurrenceDict_minimumProfitRequirement:
                cancelOccurrenceDict_averagePerDay_minimumProfitRequirement[key] = cancelOccurrenceDict_minimumProfitRequirement[key] / scriptRunningDuration_days

            # Take the value and average it over the number of days this script has been running
            for key in cancelOccurrenceDict_duration:
                cancelOccurrenceDict_averagePerDay_duration[key] = cancelOccurrenceDict_duration[key] / scriptRunningDuration_days

            # Take the value and average it over the number of days this script has been running
            for key in cancelOccurrenceDict_marketName:
                cancelOccurrenceDict_averagePerDay_marketName[key] = cancelOccurrenceDict_marketName[key] / scriptRunningDuration_days

            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: cancelOccurrenceDict_averagePerDay_minimumProfitRequirement = " + str(
                cancelOccurrenceDict_averagePerDay_minimumProfitRequirement))
            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: cancelOccurrenceDict_averagePerDay_duration = " + str(cancelOccurrenceDict_averagePerDay_duration))
            PrintAndLog("ConsiderAnalyzingOrderCancelTrackingList: cancelOccurrenceDict_averagePerDay_marketName = " + str(cancelOccurrenceDict_averagePerDay_marketName))

            estimatedGasCost_eth_perDay = sum_estimatedGasCost_eth / scriptRunningDuration_days

            message = "Analysis of Order Cancels: "
            message += "Spending average of " + str(round(estimatedGasCost_eth_perDay, 1)) + " ETH every 24 hrs over " + str(
                round(scriptRunningDuration_days, 2)) + " days, "
            message += "minProfitReqAvg = " + str(cancelOccurrenceDict_averagePerDay_minimumProfitRequirement) + ", "
            message += "durationAvg = " + str(cancelOccurrenceDict_averagePerDay_duration) + ", "
            message += "marketAvg = " + str(cancelOccurrenceDict_averagePerDay_marketName) + ", "
            API_PostOperatorNotification(message)

        except:
            PrintAndLogError("exception in ConsiderAnalyzingOrderCancelTrackingList: " + str(traceback.format_exc()))


def CalculateBestEtherToSpendOnTrade(x, y):
    # x should be the variable you have control over, y should be the variable you want to optimize
    # Flip the x and y
    # x = [1.5079014074408472, 1.3540339168856588, 1.2001664263304703, 1.0462989357752819, 0.8924314452200932,
    #      0.7385639546649048, 0.5846964641097163, 0.4308289735545278, 0.27696148299933926]
    # y = [0.0195, 0.019, 0.0225, 0.0207, 0.0212, 0.0218, 0.0179, 0.0137, 0.0091]
    # yo = numpy.polyfit(x, y, 2)
    # print("yo = ", yo)
    # p = numpy.poly1d(numpy.polyfit(x, y, 2))
    # print("p = ", p)
    z = numpy.polyfit(x, y, 2)
    # print("z = ", z)
    result = -1 * z[1] / (2 * z[0])
    # print("result ", result)
    return result


def GetEffectiveEtherGivenTokensAndPrice(tokens, price):
    effectiveEther = tokens * price
    return effectiveEther


def GetEffectiveTokensGivenEtherAndPrice(ether, price):
    effectiveTokens = ether / price
    return effectiveTokens


def AssociateAnOrderHashWithATxHash(txHash, orderHash):
    global TxHashOrderHashListDict

    if txHash in TxHashOrderHashListDict:
        # Array already created, just append
        TxHashOrderHashListDict[txHash].append(orderHash)
    else:
        # Create the array
        TxHashOrderHashListDict[txHash] = [orderHash]


def GetListOfOrderHashesAssociatedWithTxHash(txHash):
    global TxHashOrderHashListDict

    if txHash in TxHashOrderHashListDict:
        return copy.deepcopy(TxHashOrderHashListDict[txHash])
    else:
        # Return an empty list since there are none
        return []


def HandlePendingTrade_NinjaOpAccountDict(ninjaAccount, pendingTransaction, transactionResult, txReceipt):
    from Libraries.exchanges import BlacklistedOrderManager
    global TxHashOrderHashListDict
    from Libraries.transactions import TransactionResult

    # if the transaction has been mined into a block and we DO need to enforce the 0x fill order success event logs BUT the fill failed either due to a cancel/expire/etc
    if transactionResult == TransactionResult.confirmed:
        PrintAndLog("HandlePendingTrade_NinjaOpAccountDict: Order was mined in, so it either succeeded or failed gracefully. "
                    "TODO, determine which based on txReceipt. pendingTransaction.transactionId = " + str(pendingTransaction.transactionId))

    # if the transaction has mined into a block and contains an error
    elif transactionResult == TransactionResult.error_revertOrOutOfGasOrBadJumpDestinationEtc:
        if Libraries.executeOnInterval.IsTimeToExecute("transactionError " + ninjaAccount.marketName, 600):
            message = str(ninjaAccount.marketName) + " Bad jump, out of gas, or revert. trade.transactionId = " + str(pendingTransaction.transactionId)
            API_PostOperatorNotification(message)

        orderHashList = GetListOfOrderHashesAssociatedWithTxHash(pendingTransaction.transactionId)
        PrintAndLog(
            "HandlePendingTrade_NinjaOpAccountDict: orderHashList contains a list of orderhashes to blacklist since this transaction errored. orderHashList = " + str(
                orderHashList))
        if orderHashList:
            for orderHash in orderHashList:
                BlacklistedOrderManager.BlacklistThisOrderId(orderHash)
                PrintAndLog("HandlePendingTrade_NinjaOpAccountDict: Blacklisted this order id because it resulted in an error, orderHash = " + str(orderHash))

    # else the transaction is still pending
    else:
        PrintAndLog("HandlePendingTrade_NinjaOpAccountDict: Transaction is still pending, let's wait for it")


def ApplyMultiplierToEffectiveValues(effectiveEther, effectiveTokens, multiplier):
    if effectiveEther and effectiveTokens:
        if multiplier != 1.0:
            # For debug, this can be set to something really small like 0.0001 so i'm trading pennies while I test
            effectiveEther *= multiplier
            effectiveTokens *= multiplier

            PrintAndLog("ApplyMultiplierToEffectiveValues: multiplier (" + str(
                multiplier) + ") was applied to effectiveEther and effectiveTokens.  effectiveEther = " + str(
                effectiveEther) + " and effectiveTokens = " + str(effectiveTokens))

    return effectiveEther, effectiveTokens


def GetArbitrageProfitPercentageMultiplier_GivenMarket(market):
    if "wbtc" in market.GetSecondaryTokenName().lower():
        # Because we're using Coinlist to convert between WBTC and BTC, we must require more profit since they take a big fee
        return 1.45
    else:
        return 1.0


def IsValidPrice(price):
    # Make sure it's not null and that it's a proper type
    if price and (isinstance(price, int) or isinstance(price, float)):
        return True
    else:
        return False


def GetArbitrageOpportunityConflicts(arbitrageOpportunity1, arbitrageOpportunity2):
    # exchangeIdentifier (exchange contract address or 0x order hash)
    # tokenIn
    # tokenOut
    #    if trade1.exchangeIdentifier == trade2.exchangeIdentifier and token1.tokenIn == token1.tokenIn and token2.tokenIn == token2.tokenIn:

    tradePathIdentifierStrings1 = arbitrageOpportunity1.GetTradePathIdentifiers_ReturnStrings()
    tradePathIdentifierStrings2 = arbitrageOpportunity2.GetTradePathIdentifiers_ReturnStrings()
    PrintAndLog_FuncNameHeader("Checking for conflicts:")
    # PrintAndLog_FuncNameHeader("   tradePathIdentifierStrings1 = " + str(tradePathIdentifierStrings1))
    # PrintAndLog_FuncNameHeader("   tradePathIdentifierStrings2 = " + str(tradePathIdentifierStrings2))

    # There's a problem that occurs with several exchanges where the generic logic will look at conflicts incorrectly because
    # it sees one arbitrageOpportunity trading ETH while the other is trading WETH when in fact they're the same exact arbitrageOpportunity
    # Fixing this the right way is going to be very complicated and require a LOT of time debugging to make sure I don't break anything
    # Fixing this the cheap way is just assuming WETH and ETH are the same during a conflict check
    # This short term solution should be fine for most exchanges since UniswapV2 only trades WETH and not ETH, 0x only trades WETh and not ETH, etc
    # But for exchanges like 1inch, they actually trade both WETH and ETH it will cause problems there.
    # The problem is most likely that it will see conflicts where there are not actually conflicts. I think that's okay for now.
    # This short term work around is critical because it prevents double trades from occurring! Double trades are costly to profit
    if Libraries.defaults.AssumeEthAndWethAreTheSameThing_WhenCheckingForArbitrageOpportunityConflicts:
        # PrintAndLog_FuncNameHeader("tradePathIdentifierStrings1 before = " + str(tradePathIdentifierStrings1))
        tradePathIdentifierStrings1 = MergeAllOccurrencesOfEthAndWethInListOfStrings(tradePathIdentifierStrings1)
        # PrintAndLog_FuncNameHeader("tradePathIdentifierStrings1 after = " + str(tradePathIdentifierStrings1))

        # PrintAndLog_FuncNameHeader("tradePathIdentifierStrings2 before = " + str(tradePathIdentifierStrings2))
        tradePathIdentifierStrings2 = MergeAllOccurrencesOfEthAndWethInListOfStrings(tradePathIdentifierStrings2)
        # PrintAndLog_FuncNameHeader("tradePathIdentifierStrings2 after = " + str(tradePathIdentifierStrings2))

    # Check to see if any of these conflict with each other
    conflictingTradePathIdentifierStrings = Libraries.utils.FindIntersectingValuesInLists(tradePathIdentifierStrings1, tradePathIdentifierStrings2)
    return conflictingTradePathIdentifierStrings


def MergeAllOccurrencesOfEthAndWethInListOfStrings(listOfStrings):
    # PrintAndLog_FuncNameHeader("listOfStrings before = " + str(listOfStrings))
    for index, tradePathIdentifierString in enumerate(listOfStrings.copy()):
        listOfStrings[index] = listOfStrings[index].replace(Exchanges.zrxV4.Contract_WETH.lower(), Exchanges.keeperDAO.EthTokenContract.lower())
    # PrintAndLog_FuncNameHeader("listOfStrings after = " + str(listOfStrings))
    return listOfStrings
