from enum import Enum


class ReadySetGoState(Enum):
    Ready = 'ready'
    Set = 'set'
    Go = 'go'
    Fail = 'fail'
