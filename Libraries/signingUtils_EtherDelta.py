from sha3 import keccak_256

from Libraries.loggingConfig import PrintAndLogError, PrintAndLog_FuncNameHeader
from Libraries.signingUtils import SignHash, address, uint


def SignOrder_EtherDelta(exchangeContractAddress, tokenGet, amountGet, tokenGive, amountGive, expires, orderNonce, fromPrivateKey):
    hash_keccak_256, hashString = HashOrder_EtherDelta(exchangeContractAddress, tokenGet, amountGet, tokenGive, amountGive, expires, orderNonce)
    v, r, s = SignHash(hash_keccak_256.digest(), fromPrivateKey)

    hash = '0x' + hash_keccak_256.hexdigest()
    v = v
    r = r
    s = s
    print("hash = ", hash)
    print("v = ", v)
    print("r = ", r)
    print("s = ", s)
    return hash, v, r, s


def HashOrder_EtherDelta(exchangeContractAddress, tokenGet, amountGet, tokenGive, amountGive, expires, orderNonce):
    hashed_order = keccak_256(
        address(exchangeContractAddress) +
        address(tokenGet) +
        uint(amountGet) +
        address(tokenGive) +
        uint(amountGive) +
        uint(expires) +
        uint(orderNonce))

    PrintAndLog_FuncNameHeader('orderHash.hexdigest = ' + str(hashed_order.hexdigest()))
    PrintAndLog_FuncNameHeader('hashed_order of type ' + str(type(hashed_order)) + ' = ' + str(hashed_order))
    return hashed_order, "0x" + str(hashed_order.hexdigest())
