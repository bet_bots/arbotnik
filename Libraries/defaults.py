from Libraries.network import Network, NodeInfrastructure
from Libraries.tradeProperties import TradeProperties

# This directory is for operator specific data, like private keys or personal information that is specific to the operator of this bot

Directory_OperatorData = "../ArbyOperatorData"

# Directories that live in the Directory_OperatorData
Directory_Snapshots = Directory_OperatorData + "/" + "Snapshots"
Directory_SnapshotsCSV = Directory_OperatorData + "/" + "SnapshotsCSV"
Directory_Snapshots_Test = Directory_OperatorData + "/" + "Snapshots_Test"
Directory_SnapshotsCSV_Test = Directory_OperatorData + "/" + "SnapshotsCSV_Test"
Directory_PKs = Directory_OperatorData + "/" + "PKs"
Directory_Forms = Directory_OperatorData + "/" + "Forms"
Directory_RedisDB = Directory_OperatorData + "/" + "RedisDB"

# Directories that live in the root project directory
Directory_Logs = "Logs"
Directory_Logs_Set = Directory_Logs + "/" + "Set"
Directory_Logs_PricesDicts = Directory_Logs + "/" + "PricesDicts"
Directory_Cache = "Cache"
Directory_Exchanges = "Exchanges"
Directory_Media = "Media"

DateTimeFormat_Snapshots = '%Y-%m-%d %X.%f'

CoinMarketCapId_Ethereum = "ethereum"  # Works for CoinMarketCap and CoinGecko
# CoinMarketCapId_Ethereum = "ETH"    # Works for CryptoCompare

DecimalsForPrintStatement_Prices = 8
DecimalsForPrintStatement_Ether = 4
DecimalsForPrintStatement_Tokens = 1

# Sometimes when I withdraw from a CX, the withdraw will confirm but my remote node won't show my balance as updated.
# So I'm requiring several confirmations before I assume the withdraw has hit and remove it from my pending withdrawals
ConfirmationsRequiredToAssumePendingWithdrawFromCXhasHitMyWallet = 6

Interval_seconds_LatestBlockNumberCheck = 1.2
Interval_seconds_UpdateTransactionCountCache_Arby = 10
Interval_seconds_GetAndCacheGeneralExchangeData = 60 * 30
Interval_seconds_NinjaBetweenSetAnd0x = 2.0
Interval_seconds_GetOasisDexOrderbook = 6
Interval_seconds_TrackExchangePriceVolatility = 15
Interval_seconds_RemoveOldOrderbookCacheDataFromList = 101
Interval_seconds_InitializeRatesGraphsForNearFutureBlocks = 15
Interval_seconds_ManuallyPoleOrderbooks_0xMesh = 500  # I have to make one API call per pair, so do this rarely
Interval_seconds_ManuallyPoleOrderbooks_HidingBook = 15
Interval_seconds_PruneAllOrderAggregators_0x_Slow = 25  # This takes a really long time because the 0xmesh order book is massive, do not do this frequently
Interval_seconds_PruneAllOrderAggregators_0x_Fast = 12  # This should be done at least once every block or even more frequently

# Some smart contracts have very large decimals. It's clearly bad programming so don't allow it.  Put a max on it
MaxAllowedTokenDecimals = 50

# Sometimes transactions can poof and fail. This will allow me to move on from a transaction after x minutes if it's poofing
TransactionTimeout_TradeOrder_minutes = 6

# These prevent the ninja's non-trading functions to be called when the trading functions are being called.
# We don't want to spam the nodes too hard with low priority calls when we're also spamming it with high priority trading calls
NinjaThreshold_TradingFunctionCalls_seconds = 5
NinjaThreshold_NonTradingFunctionCalls_seconds = 10
# Note, FrequencyToConsiderTradingOnaNinjaMarket_seconds should always be greater than NinjaThreshold_TradingFunctionCalls_seconds
# This Safeguard prevents the ninja from trading too frequently
# FrequencyToConsiderTradingOnaNinjaMarket_seconds = NinjaThreshold_TradingFunctionCalls_seconds + 1
FrequencyToConsiderTradingOnaNinjaMarket_seconds = 0  # I've set this to zero because it was preventing me from making successful trades! Should it be removed all together?

# I will null out a currentPendingTransaction after a while, sometimes they poof or get overwritten
ExpireCurrentPendingTransactionAfterX_Arby_seconds = 300
ExpireCurrentPendingTransactionAfterX_Ninja_seconds = 360

DXAssetSourcingAmountValues_NumOfDataValues_Arby = 28
DXAssetSourcingAmountValues_QuantityMultiplier_Arby = 2.5
# DXAssetSourcingAmountValues_QuantityMultiplier_Arby = 7.0  # Was in use for over a year.  It's overkill though right??

NumOfDataValues_Arby = 25
NumOfDataValues_Ninja = 6
NumOfDataValues_Ninja_Set = 20
# Using the QuadraticFormula doesn't seem to be perfect, instead of trying to perfect the formula i'm adding some hard coded quantities to the array
ManuallyAddedTradeAmountArray_Ninja = [0.1, 0.35, 0.65, 1.0, 2.0, 3.0, 4.0, 5.0]

# Arbitrage trade profit minimums, gas is expensive so I have to put in minimum profit requirements to ensure i'm not losing money on any trades
# These should always be greater than zero but can be decimals...
ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly_ShallowLiquidity = 0.0035  # Use for production
# ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly_ShallowLiquidity = 0.000000001       # TODO, DEBUG purposes only, UNSET THIS BEFORE GOING PRODUCTION
ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly = 0.0039  # Use for production
# ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly = 0.000000001       # TODO, DEBUG purposes only, UNSET THIS BEFORE GOING PRODUCTION
ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly_DeepLiquidity = 0.0049  # Use for production
# ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly_DeepLiquidity = 0.000000001       # TODO, DEBUG purposes only, UNSET THIS BEFORE GOING PRODUCTION

# Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_BancorAndKyber = 0.85  # Succeeding somewhat consistently for a while, now it has a poor track record
# Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_UniswapAndKyber = 0.85  # Succeeding somewhat consistently for a while, now it has a poor track record
# Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_BancorAndKyber = 0.50  # Does not really work at all
# Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_UniswapAndKyber = 0.50  # Does not really work at all
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_BancorAndKyber = 0.80  # Testing
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_UniswapAndKyber = 0.95  # Testing

Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_SetAndKyber = 0.75
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_SetAndUniswap = 0.75  # Seems to be conflicting with Kyber because they share liquidity...
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_SetAnd0x = 0.79  # I was using 0.81 with 3 auto gas price increments but that was rarely successful

# TODO, I'm trying this, raising my MaxKeepAhead values higher than I really want them to be
#  My goal is to see if I am more consistently winning auctions. If I am, then I know my MaxKeepAhead technique is working
#  If i'm not winning more consistently than I know MaxKeepAhead isn't as effective as I was hoping OR I need to tweak that sleep between the rapid fire gas incrementer
# NOTES:
# Lower OpeningBids have not been working lately
# Higher OpeningBids have been more successful but I've also been getting grim triggered a lot
# This could mean my MaxKeepAhead is too slow or ineffective
# TODO, high numbers 1.00 and above only make sense when considering gas tokens
# Opening bids to lead of trading
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_OpeningBid_BaseValue = 0.90
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_OpeningBid_0x = 0.80
# Point at which I stop rapid fire raising gas prices
# Making MaxKeepAhead lower than OpeningBid effectively disables it
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxKeepAhead_BaseValue = 0.64
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxKeepAhead_0x = 0.52
# TODO, MaxKeepAhead must be ~15% less than MaxGrimTrigger or else we'll just rapid fire increase gas until we hit our MaxGrimTrigger
#  Do not let this happen or else we'll pre-maturely break from the trade loop and then I won't be able to monitor the trade to see if someone front runs me
# Grim trigger max will lose money, but the purpose is to block others front getting any money at all
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxGrimTrigger_BaseValue = 0.90
Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxGrimTrigger_0x = 0.80
# When Ninja is trading at high gas prices, it will burn gas tokens resulting in less gas usage
# When over that BurnGasTokenThreshold_wei, add this much to my PercentageOfExpectedProfit
# So if PercentageOfExpectedProfit = 0.8 (or 80%), that means when I burn gas token it will be 0.8 + 0.3 (or 110%)
Ninja_ProfitPercentageAdder_WhenGasPriceOverBurnGasTokenThreshold = 0.50

# Do not attempt to burn gas tokens if our contract gas token quantity gets lower than this
MinGasTokenThreshold = 50

# Ninja_SimpleRequirements_WiggleRoomPercentage = 0.002  # Tried this but was getting a lot of Shit the bed code 5's
Ninja_SimpleRequirements_WiggleRoomPercentage = 0.001

Multiplier_0x_fillableAmountRemaining_quoteTokens_etherUnits = 1.6

# FrontRunningMaxDurationToWaitBetweenEachGasPriceIncrement_seconds = 2.0  # Was beating people consistently, but overpaying a good portion of the time
# This seems to hurt me because I'm increasing my gas price for no good reason.
# So I'm setting this to a really high number to basically turn it off without removing the feature
FrontRunningMaxDurationToWaitBetweenEachGasPriceIncrement_seconds = 99999.0

# This is used to determine when to pass or fail the check on AMM (automated market maker) protocols like Uniswap/UniswapV2/Balancer etc
# You can use this on any protocol (not just AMM) that provides you with a min/max price acceptable
# Decrease this number slightly if you're seeing a lot of code 5's because the trade reverted after passing the checks despite no profit actually being available
# Increase this number slightly if you're seeing a lot of trade opportunities where Ninja is failing checks when profit is actually there
# I like to set priceMovementPercentageAllowed based on expected profit percentage. This helps me do that. So if profit percentage is 1%,
# and we want to reject the trade if profit percentage gets to 0.5%, then set this value to 0.5 so it draws the line at half.
# But i've found that setting this value to 0.5 causes a lot of Code 5 reverts so in practice this value needs to be much lower than 0.5 but not too low...
AMMProfitPercentagePriceMovementMultiplier = 0.15

# Multiplier applied to balancer exchange balance to act as a safety buffer
BalancerExchangeBalanceSafetyBufferPercentageMultiplier = 0.9
CurveExchangeBalanceSafetyBufferPercentageMultiplier = 0.9

# Ban some tokens form the ninja trading them. It's possible I only need to ban them on one or two exchanges,
# but it's not worth spending the time to research which exchanges need which token banned...
# I have to ban these because approving allowances fails for some reason.
# I don't want to get a revert every time i attempt to approve something that will never approve
# TODO, ban them based on exchange protocol instead of globally banning
Ninja_BannedTokens = []
# Ninja_BannedTokens.append("0xd26114cd6EE289AccF82350c8d8487fedB8A0C07".lower())  # omg
# Ninja_BannedTokens.append("0xB97048628DB6B661D4C2aA833e95Dbe1A905B280".lower())  # pay
# Ninja_BannedTokens.append("0xF433089366899D83a9f26A773D59ec7eCF30355e".lower())  # mtl
# Ninja_BannedTokens.append("0x4f3afec4e5a3f2a6a1a411def7d7dfe50ee057bf".lower())  # dgx
# Ninja_BannedTokens.append("0xb5a5f22694352c15b00323844ad545abb2b11028".lower())  # icx
# Ninja_BannedTokens.append("0xdac17f958d2ee523a2206206994597c13d831ec7".lower())  # usdt
Ninja_BannedTokens.append("0xe3818504c1b32bf1557b16c238b2e01fd3149c17".lower())  # plr
Ninja_BannedTokens.append("0x177d39ac676ed1c67a2b268ad7f1e58826e5b0af".lower())  # cdt
Ninja_BannedTokens.append("0x0f4ca92660efad97a9a70cb0fe969c755439772c".lower())  # levn
Ninja_BannedTokens.append("0xc3761eb917cd790b30dad99f6cc5b4ff93c4f9ea".lower())  # erc20
Ninja_BannedTokens.append("0x85eba557c06c348395fd49e35d860f58a4f7c95a".lower())  # h3x
Ninja_BannedTokens.append("0x3a9fff453d50d4ac52a6890647b823379ba36b9e".lower())  # shuf
Ninja_BannedTokens.append("0x5adc961D6AC3f7062D2eA45FEFB8D8167d44b190".lower())  # dth
Ninja_BannedTokens.append("0xa44e5137293e855b1b7bc7e2c6f8cd796ffcb037".lower())  # sent
Ninja_BannedTokens.append("0x55296f69f40ea6d20e478533c15a6b08b654e758".lower())  # xyo
Ninja_BannedTokens.append("0xc0e47007e084eef3ee58eb33d777b3b4ca98622f".lower())  # xstar
Ninja_BannedTokens.append("0x985dd3d42de1e256d09e1c10f112bccb8015ad41".lower())  # ocean old token before fork
Ninja_BannedTokens.append("0x7afebbb46fdb47ed17b22ed075cde2447694fb9e".lower())  # new ocean still not passing eth_estimateGas
Ninja_BannedTokens.append("0x5a98fcbea516cf06857215779fd812ca3bef1b32".lower())  # ldo out of gas error
Ninja_BannedTokens.append("0x87d73E916D7057945c9BcD8cdd94e42A6F47f776".lower())  # nftx out of gas error

# I have to initialize graphs for various things, per ethereum block.
# It's computationally expensive, so I do them in a background thread on each process ahead of time so that they're ready when I need them
# I do a certain number of blocks into the future
# Set the initial lower than the target so that we make sure the bot load successfully before we demand that it stays ahead to the target
InitialNumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks = 1
TargetNumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks = 3

ReleaseAnInUse_ReservableEthereumAccountPool_AfterItsBeenInuseForTooLong_ExpirationDuration_minutes = 1.0

# Send the operator a phone call notification alert when a set rebalance begins whose marketcap exceeds this threshold
SetPhoneCallAlertMarketCapThreshold_usd = 400000

# TODO, I need to get this from a config or something or maybe based on gas price
#  fast gas price can be shorter duration. Slower gas price, must be longer duration
#  I used to set this to the block time in seconds but that's fundamentally flawed
#  because an order isn't expired until the block timestamp surpasses the order expiry.
#  As a short term work around, i'm setting this MinFillableOrderDuration_0x_seconds to a really really small number
MinFillableOrderDuration_0x_seconds = 2

Ninja_StopTradingAfterReverts_minutes = 0.5
Ninja_StopTradingAfterReverts_seconds = Ninja_StopTradingAfterReverts_minutes * 60

# This is an anti-front running mechanism that enables me to fight off front runners who are finding my gas, then adding 1 to it.
# This feature will auto-resubmit my transaction with a slightly higher gas price every x seconds to keep 1 upping the other ninjas
AutoGasPriceIncrementIterations_NinjaBetweenSetAnd0x = 2
AutoGasPriceIncrementIterations_Ninja_Generic = 3
# Never exceed this value, for safety reasons
MaxSafeAutoGasPriceIncrementIterations = 4
# Default number of times we will auto increment gas price on a tx
DefaultAutoGasPriceIncrementIterations = 0
# I want this time to be long enough such that a Villain can see my gas price adn front run it, but then I later re-submit at a better gas price and leap frog him
# If it's too long, I wont leap frog him in time
# If it's too short, he may never see the original one and he'll just front run my leap frog attempt
SleepTimeBetweenAutoGasPriceIncrementIterations_seconds = 1.5  # Notorious could wait until my highest gas price tx was submitted and then he just front ran me...

MinProfitRequirement_quoteToken_NinjaDebug = 0.000000000001  # Use for debugging only
# MinProfitRequirement_quoteToken_NinjaDebug = 0.000001  # Use for debugging only

# For debugging purposes I can make this lower while testing, like 0.00001
NinjaTakerAmountMultiplier = 1.0  # Use for production
# NinjaTakerAmountMultiplier = 0.0001  # TODO, DEBUG purposes only, UNSET THIS BEFORE GOING PRODUCTION

# This is a strategy i'm considering because there's a chance in some cases I actually don't want to be the first bidder
# I've found that one of the best traders is never the first bidder...
# I could be that his nodes and algorithms are slow or it could be that he's waiting on purpose,
# because block times are ~14 seconds so why rush for that first bid? Worth trying...
# This could also just save me money because if someone's going to grim trigger me or if two traders are grim triggering each other,
# just let them do it and sit back and watch rather than bid only to get guaranteed grim trigger.
DelayTheTradeLoopUntilBlockDiscoveryTimeHasReachedThisElapsedDuration_seconds = 0

BlockAge_blocks_WhenABlockIsConsideredOldForDataCleanUpPurposes = 5

# MaxNumOfNodesCalledPerRequest will be enforced on all calls
MaxNumOfNodesCalledPerRequest = 2
# where as MaxNumOfNodesCalledPerRequest_QuoteTokenProcess is only enforced on sub process calls for QuoteTokenProcesses
MaxNumOfNodesCalledPerRequest_QuoteTokenProcess = 2
if MaxNumOfNodesCalledPerRequest_QuoteTokenProcess > MaxNumOfNodesCalledPerRequest:
    raise Exception("MaxNumOfNodesCalledPerRequest_QuoteTokenProcess must be <= MaxNumOfNodesCalledPerRequest in order to make sense")

# This is the min exchange balance required to consider trading a token
# Measured in terms of ETH, but works for any quoteToken because i'm converting it
# Increasing this number increases performance by reducing the number of tokens we trade to only ones with higher liquidity
# Decreasing this number hurts performance because we are enabling trade on more tokens since we are allowing a lower level of liquidity
# This will directly effect how many tokens end up in our ratesGraph. The larger the ratesGraph the worse performance.
# ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth = 2  # Bad performance, includes way too many shit coins that fill our ratesGraph with useless junk
ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth = 35
# ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth = 7  # Bad performance?

# This is a multiplier that determines how much ether i'm willing to overpay on gas for trades before my sanity check safety feature kicks in
# and prevents the trade from happening. The goal is to have the sanity check feature prevent bugs from spending a shit load of gas on a trade on accident
# But also keep in mind for testing purposes some times I like to force trades through
# Also keep in mind calculations may not be perfect
# Also keep in mind burning gas tokens can save you up to 50% of the gas cost on the tx
# so this number should not go lower than say 1.5 or 1.6 or even 1.75 to be safe
# The goal is to prevent Ninja from accidentally spending 1 ETH of gas to make 0.001 ETH. This feature will prevent that even if my trade logic is bugged
GasPriceSanityCheckBufferMultiplier = 2.0
# GasPriceSanityCheckBufferMultiplier = 5.0  # TODO, DEBUG purposes only, UNSET THIS BEFORE GOING PRODUCTION
# This is used for debugging sending test trades
# when I trade a super small amount the sanity check will end up failing
# so I'm enabling a minimum with an or condition to make sure I can test through the sanity check
GasPriceSanityCheckBuffer_AlternativeMinimumCost_ether = 0.1

ActiveNetwork = Network.mainnet  # Use for production
# ActiveNetwork = Network.kovanTestnet  # TODO, DEBUG purposes only, UNSET THIS BEFORE GOING PRODUCTION

# This hard codes a block number for network calls so i can simulate being back in time, at a certain block number, when making calls
timeMachine_TravelToBlockNumber = None
# timeMachine_TravelToBlockNumber = 11539124
# timeMachine_TravelToBlockNumber = 11498921
# We have to initialize the time machine at least 2 blocks before the where we want to go, because it takes 2 whole blocks to initialize properly
TimeMachine_EnabledForHistoricBlockDebug = None
if timeMachine_TravelToBlockNumber:
    TimeMachine_EnabledForHistoricBlockDebug = timeMachine_TravelToBlockNumber - 2
# This really needs to always be set to True. I have only been using this set to True
Increment_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_ToSimulateNewBlocksBeingMined = True
StopIncrementing_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_AfterNBlocks = 3
StopIncrementing_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_EndBlock = None

# This is where I can ban specific types of trades based on TradeProperties class
# I may want to do this for debug purposes or because for whatever reason I'm not profitable on that specific path because of other trader tendencies
# Set this in the config
FinalStepTradeBanList = []

# Run time flags that enable/disable critical features
# TODO, these should all be True when deploying to production, Setting to False will disable that feature
EnterMainLoop = True
ActiveNodeInfrastructure = NodeInfrastructure.DE
MustSuccessfullyLoadSecurityCredentialsOnStartUp = True
EnableSendingNotificationsToOperator = False
EnableOrderAggregator_0xMesh = True
EnableOrderAggregator_HidingBook = False
EnableSet = False
SetAllowedToPredictFuturePrice = True
EnableBalancersCacheToOptimizeGetPrices = True
EnableCurvesCacheToOptimizeGetPrices = True  # TODO, do I need this????? Probably not. But still debugging and developing curve
EnableMempoolStrategy_FrontRunning = True
EnableMempoolStrategy_KeepAhead = True
ConsiderUpdatingCachesBeforeBotLoop = False  # Takes up to ~20 minutes to fully update all caches, set to False if you don't care about new tokens and want to save on load time
CallEthCallBeforeTradingForSafety = True  # Setting this to False is dangerous unless you know what you're doing!
CallEstimateGasBeforeTradingForSafety = True  # Setting this to False is dangerous unless you know what you're doing!
# MinGasRequired_ToAssumeTradeWillSucceed = 150000  # I found cases where estimatedGas = ~55k and the arbitrage was clearly there when the block mined in
MinGasRequired_ToAssumeTradeWillSucceed = 20000  # TODO, testing with this effectively disabled now that I have the eth_call working
MinGasRequired_UsedToDetermine_MinProfitPercentageRequirementForCalculatingArbitrage = 40000
ActualProfitMinPercentageOfExpectedProfit = 0.80  # if this is 0.90, it means ethCall must return at least a 90% actual profit based on what we're expecting
EnableSourcingQuoteTokensFrom_NinjaContract = True
EnableSourcingQuoteTokensFrom_KeeperDAOFlashLoan = True
RestrictMempoolToWatchListOnly = True  # When on, improved performance. When off we risk huge performance hits because of how many txs are in mempool
FrontRunningTargetsRequireAWatchListVillain = True  # When on, improved performance. When off, we front run ALL TRANSACTIONS in mempool which can hurt performance and be costly
RequireMempoolAnalysisStringsToMatchInOrderToFrontRunVillains = True
AssumeEthAndWethAreTheSameThing_WhenCheckingForArbitrageOpportunityConflicts = True
BroadcastTxsToEtherScan = False
EnableBloxrouteWebsockets = True
BroadcastTxsDirectlyToBloxrouteGateways = True
EnableMempool = True
EnableMempool_Bloxroute = True
EnableMempool_BlockNative = False
EnableMempool_Alchemy = False
CallBlockNativesAddWatchAddresses = True
Mempool_RejectTxsWithLowGasPrices = True
Mempool_LogLateDiscoveries = True
EnableNinja_Loops = True
EnableNinja_Events = True
EnableNinja_Arbitrage = True
EnableNinja_Tailgating = True
EnableNinja_TradeExecution = False  # Setting this to True means it will literally trade when it sees an opportunity
EnableNinja_TradeUsingRelayProxy = False  # I've blacklisted my relay proxy's, since they didnt' seem to help combat front running
CancelTradesThatGetGrimTriggeredByAVillain = True
LimitNumOfProcessesToNumOfCPUCores = False
IssueApprovesOnStartup = False
IssueApprovesOnStartup_DoExecute = False
IssueApprovesOnStartup_ExitAfterApproves = False
GenerateHidingBookTokenListOnStartUp = False
GenerateHidingBookTokenListOnStartUp_ExitAfterApproves = False

# DEBUG FLAGS
# TODO, these should all be False when deploying to production
QuitAfterTradeExecutionWhileIDebug = False
UniswapV2_UseOnlyTheFirstFewExchanges = False
LongSleepForDebug_Ninja = False
UseDebugTradeArraySizes = False
Test0xWebsocketsAndOrderAggregatorsOnly = False
Ninja_DeleteAllTokensExceptForThoseInThisList = False
Ninja_VerifyThatTokenDictIsSetCorrectly = False
Ninja_SleepAfterPursueTradeOpportunityWhileIDebug = False
Ninja_UseCheapGasPricesForTrades = False
Ninja_ForcePassSimpleRequirements = False
Ninja_BanASubsetOfExchangeNames = False
Ninja_ForceTradingFeeToBeZero = False
Tailgating_CapGasPriceForTesting = False
Tailgating_WithHardCodedTxHash = False
UseDebugGasStation = False
RequireTradesToIncludeTheseExchangeNames = False
RequireThisNumberOfLegsInTrade = False
RequireThisAsTradeQuoteToken = False
# RequireThisAsTradeQuoteToken = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'.lower()
# RequireThisAsTradeQuoteToken = '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'.lower()
# RequireThisAsTradeQuoteToken = '0x6b175474e89094c44da98b954eedeac495271d0f'.lower()
# RequireThisAsTradeQuoteToken = '0xeb4c2781e4eba804ce9a9803c67d0893436bb27d'.lower()
# RequireThisAsTradeQuoteToken = '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2'.lower()
PreventTradingAfterNTradesAreTriggered_MaxTradesAllowed = False
PreventTradingAfterNTradesAreTriggered_CurrentTradeCount = 0
ForceSourcingQuoteTokensFrom_KeeperDAOFlashLoan = False  # Deprecated, do not use because it can lead to forcing me to flash loan tokens KeeperDAO does not have
ForceNinjaBalanceToBeZero_SourcingQuoteTokensFrom_WillForceAKeeperDAOFlashLoan = False
UseHardCodedTradableTokensList = False
Set_ForceTradeRegardlessOfPrice = False
Set_DeleteAllSetTokensExceptForOne = False
Set_UseMainnetStagingContracts = False
Set_AddMyHardCodedStagingContractsToSetData = False
UseDebugMinProfitRequirement_Arbitrage_SetOnly = False
UseDebugMinProfitRequirement_NinjaBetweenSetAnd0x = False
UseDebugMinProfitRequirement_NinjaBetweenSetAndKyber = False
UseDebugMinProfitRequirement_NinjaBetweenSetAndKyber_TokenForToken = False
UseDebugMinProfitRequirement_NinjaBetweenSetAndUniswap = False
UseDebugMinProfitRequirement_NinjaBetweenUniswapAndKyber_ForToken = False
UseDebugMinProfitRequirement_NinjaGeneric = False
FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm_minProfitPercentageRequirement_HardCodeToZero = False
UseUnitTestData_NinjaGeneric_Quick = False
UseUnitTestData_NinjaGeneric_Detailed = False
VerboseLogging_Tailgating_PricesDictAndRatesGraphs = False
VerboseLogging_FindArbitrageOpportunities = False
VerboseLogging_FindArbitrageOpportunities_Found = False
VerboseLogging_ArbitrageGeneric = False
VerboseLogging_ArbitrageGeneric_ArbitrageNo = False
VerboseLogging_IsOrderFillable = False
VerboseLogging_GetOrdersFillableAmountRemaining_0x = False
VerboseLogging_CalculateGasPrice = False
VerboseLogging_CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens = False
VerboseLogging_GetExpectedGasUsage_AllTradeActions = False
VerboseLogging_GetMinProfitRequirement_GivenGasProperties = False
VerboseLogging_GetAllPrices = False
VerboseLogging_IsProfitable = False
VerboseLogging_CalculateTradingFee_quoteTokens = False
VerboseLogging_GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken = False
VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug = False
VerboseLogging_Mempool_CleanUpOldMempoolTxs = False
VerboseLogging_Mempool_CleanUpOldMempoolTxs_NonceAnalysisAndRemoves = False
VerboseLogging_SettingReturnValueForResultKey = False
VerboseLogging_GetPrices_Balancer = False
VerboseLogging_GetPrices_UniswapV2_Generic = False
VerboseLogging_GetPrices_Curve = False
VerboseLogging_GetPrices_Uniswap = False
VerboseLogging_GetPrices_0xMesh = False
VerboseLogging_PursueTradingOpportunity_CallData = False
VerboseLogging_PursueTradingOpportunity_TradeLoop = False
VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm = False
VerboseLogging_PricesDictToFile = False
VerboseLogging_PricesDict = False
VerboseLogging_ZMQ_Delays = True
VerboseLogging_ZMQ_LogMessages = False  # NOTE, this is really bad for performance, use litequote and litebase when testing with this
