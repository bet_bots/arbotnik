import datetime
import inspect
import json
import sys
import traceback
from enum import Enum
import multiprocessing
import numpy as np
from numpy import ones, vstack
from numpy.linalg import lstsq

import Libraries.core
import Libraries.arbyUtility
import Libraries.signingUtils
import Libraries.nodes
from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader, PrintAndLogError
import Libraries.defaults

BaseNum = 1000000

# Use this to see how long the script has been running
DateTimeThatNinjaStarted = datetime.datetime.now()
DateTimeThatNinjaLoopLoadingCompleted = None

CPUCount = None


class ScriptName(Enum):
    Unnamed = "Unnamed"
    Arby = "Arby"
    NinjaArb = "Ninja-Arb"
    NinjaTail = "Ninja-Tail"


ActiveScriptName = ScriptName.Unnamed


class MathSymbol(Enum):
    GreaterThan = "Greater than"
    LessThan = "Less than"
    Equals = "Equals"


def SetActiveScriptName(scriptName):
    global ActiveScriptName

    ActiveScriptName = scriptName


def CreateArrayOfTradeAmountValues(value, numOfMiddleValues):
    array = []
    minPercentage = 0.02
    maxPercentage = 0.98
    diffPercentageRange = maxPercentage - minPercentage
    percentageStep = diffPercentageRange / (numOfMiddleValues + 1)
    # PrintAndLog("diffPercentageRange = " + str(diffPercentageRange))
    # PrintAndLog("percentageStep = " + str(percentageStep))

    # append the first value
    array.append(value * maxPercentage)

    for i in range(numOfMiddleValues):
        indexToUse = i + 1
        # PrintAndLog("indexToUse = " + str(indexToUse))
        percentageMultiplier = maxPercentage - (percentageStep * indexToUse)
        # PrintAndLog("percentageMultiplier = " + str(percentageMultiplier))
        array.append(value * percentageMultiplier)
        # PrintAndLog("Set array[" + str(indexToUse) + "] to " + str(array[indexToUse]))

    # append the last value
    array.append(value * minPercentage)

    return array


def CreateArrayOfTradeAmountValues_UsingCustomQuadraticFormula(value, numOfDataValues, manuallyAddedTradeAmountArray=None):
    # I'm using the quadratic equation to create a curve to populate an array of numbers based on one single value.
    # The goal is to populate an array of numbers where value is the max and it goes from 0 to value based on the curve defined by the quadratic equation.

    # PrintAndLog_FuncNameHeader("CreateArrayOfTradeAmountValues_UsingCustomQuadraticFormula with value = " + str(value) + ", numOfDataValues = " + str(numOfDataValues))
    array = []
    if numOfDataValues == 0:
        # Leave the array empty
        if not manuallyAddedTradeAmountArray:
            raise Exception("CreateArrayOfTradeAmountValues_UsingCustomQuadraticFormula: manuallyAddedTradeAmountArray must be set when numOfDataValues is zero")

        return manuallyAddedTradeAmountArray

    else:
        # TODO, Rename thingy
        thingy = 10 / float(numOfDataValues)
        # PrintAndLog("thingy = " + str(thingy))

        # Works well for Arby, but not for Ninja
        # pointsList_x = [0, 7, 10]
        # pointsList_y = [0, 5, 10]

        # Works well for Ninja and Arby
        # (MainThread) dataArray of len 15 = [
        #     109.9999999999996, 95.75308641975262, 82.49382716049332, 70.22222222222172, 58.93827160493774,
        #     48.641975308641435, 39.33333333333281, 31.012345679011837, 23.679012345678526, 17.333333333332888,
        #     11.975308641974909, 7.604938271604599, 4.222222222221951, 1.8271604938269692, 0.41975308641965214]
        pointsList_x = [0, 9.90, 10]
        pointsList_y = [0, 9.80, 10]

        quadraticFormulaValues_a, quadraticFormulaValues_b, quadraticFormulaValues_c = GetCoefficientForQuandraticEquation(pointsList_x, pointsList_y)
        # PrintAndLog_FuncNameHeader("quadraticFormulaValues_a = " + str(quadraticFormulaValues_a))
        # PrintAndLog_FuncNameHeader("quadraticFormulaValues_b = " + str(quadraticFormulaValues_b))
        # PrintAndLog_FuncNameHeader("quadraticFormulaValues_c = " + str(quadraticFormulaValues_c))

        maxRange = numOfDataValues
        for i in range(maxRange):
            indexToUse = maxRange - i
            # PrintAndLog_FuncNameHeader("indexToUse = " + str(indexToUse))
            x = indexToUse * thingy
            # PrintAndLog_FuncNameHeader("x = " + str(x))
            y = SolveQuadraticFormula_GivenX(x, quadraticFormulaValues_a, quadraticFormulaValues_b, quadraticFormulaValues_c)
            # PrintAndLog_FuncNameHeader("y = " + str(y))
            if y < 0:
                y = 0
                # PrintAndLog_FuncNameHeader("overriding y to be zero: y = " + str(y))

            percentageMultiplier = y / float(10)
            # PrintAndLog_FuncNameHeader("percentageMultiplier = " + str(percentageMultiplier))
            array.append(value * percentageMultiplier)
            # PrintAndLog_FuncNameHeader("Set array[" + str(indexToUse) + "] to " + str(array[indexToUse]))

        # Consider adding manual amounts
        if manuallyAddedTradeAmountArray:
            largestValueInArray = array[0]
            # PrintAndLog_FuncNameHeader("manuallyAddedTradeAmountArray = " + str(manuallyAddedTradeAmountArray))
            # PrintAndLog_FuncNameHeader("array = " + str(array))
            # PrintAndLog_FuncNameHeader("largestValueInArray = " + str(largestValueInArray))
            # Only add values from manuallyAddedTradeAmountArray if they are smaller than largestValueInArray
            for value in manuallyAddedTradeAmountArray:
                if value < largestValueInArray:
                    array.append(value)
                # else:
                #     PrintAndLog_FuncNameHeader("Not adding " + str(value) + " because it's larger than the largest value in our array")

            # PrintAndLog_FuncNameHeader("array combined = " + str(array))
            array.sort(reverse=True)
            # PrintAndLog_FuncNameHeader("array after sorted = " + str(array))

        return array


def GetCoefficientForQuandraticEquation(x, y):
    if len(x) != 3 or len(y) != 3:
        raise Exception("This function was designed to handle exactly 3 arguments per input list")

    x_1 = x[0]
    x_2 = x[1]
    x_3 = x[2]
    y_1 = y[0]
    y_2 = y[1]
    y_3 = y[2]

    a = y_1 / ((x_1 - x_2) * (x_1 - x_3)) + y_2 / ((x_2 - x_1) * (x_2 - x_3)) + y_3 / ((x_3 - x_1) * (x_3 - x_2))

    b = (-y_1 * (x_2 + x_3) / ((x_1 - x_2) * (x_1 - x_3))
         - y_2 * (x_1 + x_3) / ((x_2 - x_1) * (x_2 - x_3))
         - y_3 * (x_1 + x_2) / ((x_3 - x_1) * (x_3 - x_2)))

    c = (y_1 * x_2 * x_3 / ((x_1 - x_2) * (x_1 - x_3))
         + y_2 * x_1 * x_3 / ((x_2 - x_1) * (x_2 - x_3))
         + y_3 * x_1 * x_2 / ((x_3 - x_1) * (x_3 - x_2)))

    return a, b, c


def SolveQuadraticFormula_GivenX(x, a, b, c):
    y = (a * (x * x)) + (b * x) + (c)
    # PrintAndLog("SolveQuadraticFormula_GivenX, y = " + str(y))
    return y


def GetMaxNumber():
    return sys.maxsize


def GetMinNumber():
    return -sys.maxsize - 1


def ConsiderSettingResultDict(result, resultDict=None, resultKey=None, lock_resultDict=None):
    # This is a common pattern i'm using where I want to run a function in a background thread
    # When running in a background thread, I cannot just return a result from a function
    # Instead I must pass in a dictionary and set the result in the dictionary
    # I made this function to do it because this pattern is used in so many places

    if resultKey and lock_resultDict:
        # PrintAndLog("ConsiderSettingResultDict Set the resultDict with the return value, make it thread safe")
        # Set the resultDict with the return value, make it thread safe
        lock_resultDict.acquire()
        try:
            resultDict[resultKey] = result
        finally:
            lock_resultDict.release()
            # PrintAndLog("ConsiderSettingResultDict resultDict = " + str(resultDict))

    elif resultKey and not lock_resultDict:
        # Don't bother locking since no lock was passed in
        # Since we have a key, we can still set the dict
        resultDict[resultKey] = result


def GetDurationSinceDateTime_seconds(myDateTime):
    return (datetime.datetime.now() - myDateTime).total_seconds()


def ConvertEtherListToTokensList(etherToSpendList_weiUnits, price, decimals_token):
    return ConvertQuoteListToBaseList(etherToSpendList_weiUnits, price, decimals_token)


def ConvertQuoteListToBaseList(quoteTokenQuantityToSpendList_weiUnits, price, decimals_baseToken):
    baseTokenQuantityToSpendList_weiUnits = []
    for quoteTokenQuantity_weiUnits in quoteTokenQuantityToSpendList_weiUnits:
        quoteTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(quoteTokenQuantity_weiUnits, Libraries.core.Ether_Decimals)
        baseTokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(quoteTokenQuantity_etherUnits, price)
        baseTokenQuantity_weiUnits = Libraries.core.ConvertEtherToWei(baseTokenQuantity_etherUnits, decimals_baseToken)
        baseTokenQuantityToSpendList_weiUnits.append(baseTokenQuantity_weiUnits)

    return baseTokenQuantityToSpendList_weiUnits


def ConvertQuoteListToBaseList_GivenPriceList(quoteTokenQuantityToSpendList_weiUnits, priceList, decimals_baseToken):
    # This function assumes the priceList and quoteTokenQuantityToSpendList_weiUnits list have corresponding indexes
    baseTokenQuantityToSpendList_weiUnits = []
    for index, quoteTokenQuantity_weiUnits in enumerate(quoteTokenQuantityToSpendList_weiUnits):
        quoteTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(quoteTokenQuantity_weiUnits, Libraries.core.Ether_Decimals)
        baseTokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(quoteTokenQuantity_etherUnits, priceList[index])
        baseTokenQuantity_weiUnits = Libraries.core.ConvertEtherToWei(baseTokenQuantity_etherUnits, decimals_baseToken)
        baseTokenQuantityToSpendList_weiUnits.append(baseTokenQuantity_weiUnits)

    return baseTokenQuantityToSpendList_weiUnits


def GetLinearFunctionGivenDataPoints(points, x):
    # HACK
    # For some reason these functions i'm going to use below do not like it when you use very large numbers.
    # For some reason numbers around this size 1,579,813,555 break it!
    # So i'm putting in a hack to make sure I do not break it because I need to use numbers that large...

    # If we only have one single point
    if len(points) == 0:
        raise Exception("points must have at least one item in an array")

    # This function detects if this array has any large x values and makes them smaller... Pretty hacky but it works
    breakingNumber = 19813555
    needToHackFix = False
    for point in points:
        if point[0] > breakingNumber:
            needToHackFix = True

    hackProofPoints = []
    if needToHackFix:
        safeNumber = 113555
        divider = breakingNumber - safeNumber
        x = x / divider
        for point in points:
            hackProofPoints.append((point[0] / divider, point[1]))

        # PrintAndLog("Had to convert points = " + str(points) + " to hackProofPoints = " + str(hackProofPoints))
        points = hackProofPoints

    # this creates a line formula so I can calculate y
    x_coords, y_coords = zip(*points)
    A = vstack([x_coords, ones(len(x_coords))]).T
    m, c = lstsq(A, y_coords)[0]
    # resultString = "Line Solution is y = {m}x + {c}".format(m=m, c=c)
    # PrintAndLog(resultString)
    # x = 60
    y = (m * x) + c
    # PrintAndLog("GetLinearFunctionGivenDataPoints: If x = " + str(x) + " then y = " + str(y))
    return y


def GetAverageOfTwoNumbers(num1, num2):
    return (float(num1) + float(num2)) / float(2)


def GetIdBasedOnIndexNumber(index, id):
    global BaseNum

    # The purpose of this function is to give me a way to make ids scale based on index.
    # So if we want to have id 1 = fruit and id 2 = veggies, we can also say Tom's fruit and Joey's fruit and Bob's fruit and differentiate them
    # by saying Tom's index 0, Joey's index 1, Bob's index 2
    # so Tom's fruit are actually 0000001 and Joey's fruit are actually 1000001 and Bob's fruit are actually 2000001
    if index > BaseNum:
        raise Exception(
            "GetIdBasedOnIndexNumber is failing because index exceeds BaseNum. Increase the size of BaseNum or use smaller indexes! Is your list really that big???")

    returnValue = (index * BaseNum) + id
    # PrintAndLog_FuncNameHeader("index = " + str(index) + ", id = " + str(id) + ". returnValue = " + str(returnValue))
    return returnValue


def GetAllPossibleUniqueCombinationsOfItemsInList(myList, n):
    from itertools import combinations
    return list(combinations(myList, n))


def ConvertListOfStringsToLowercaseListOfStrings(myList):
    return [x.lower() for x in myList]


def GetValueBetweenTwoValuesBasedOnPercentage(value1, value2, percentageTowardsValue1):
    # Set percentageTowardsValue1 to 0.5 if you want half way between value1 and value2
    # This was intended to be used with any percentageTowardsValue1 from 0 through 1.0
    return (value1 * percentageTowardsValue1) + (value2 * (1.0 - percentageTowardsValue1))


def SafetifyThisNumber_int(number, factor):
    return int(number * factor / (float(factor + 1)))


def SafetifyThisNumber_float(number, factor):
    return float(number) * float(factor) / (float(factor) + 1)


def GetMethodSignaturesOfAllContractFunctions(contractList, functionNameRequirement_startsWith=None):
    functionSelectorList = []
    for contract in contractList:
        for function in contract.all_functions():
            # PrintAndLog_FuncNameHeader("function = " + str(function))
            # PrintAndLog_FuncNameHeader("function.abi = " + str(function.abi))
            # PrintAndLog_FuncNameHeader("function.abi['name'] = " + str(function.abi['name']))
            methodName = function.abi['name']

            # If the functionNameRequirement_startsWith is NOT set, or if it's set and the function name matches
            if not functionNameRequirement_startsWith or methodName.lower().startswith(functionNameRequirement_startsWith.lower()):
                # Calculate the methodSignature
                methodSignature = Libraries.signingUtils.GetMethodSignature_GivenAbi(methodName, function.abi)
                # PrintAndLog_FuncNameHeader("methodSignature = " + str(methodSignature) + " for method " + str(methodName) + " in contract " + str(contract.address))
                functionSelectorList.append(methodSignature)

    return functionSelectorList


def GetTimeSinceNinjaLaunched_seconds():
    global DateTimeThatNinjaStarted

    return (datetime.datetime.now() - DateTimeThatNinjaStarted).total_seconds()


def GetTimeSinceNinjaLoopLoadingCompleted_seconds():
    global DateTimeThatNinjaLoopLoadingCompleted

    if not DateTimeThatNinjaLoopLoadingCompleted:
        return 0
    else:
        return (datetime.datetime.now() - DateTimeThatNinjaLoopLoadingCompleted).total_seconds()


def GetPercentageProximityBetweenTwoNumbers(num1, num2):
    return min(num1, num2) / max(num1, num2)


def DeterminePrintAndSumOverlappedMarket(coinMarketCaps_MarketDict, CoinMarketCaps_DXMarketDict, marketTuple,
                                         exchangeId_DX, marketId, exchangeId_Source, sumDict_eth):
    # PrintAndLog("DeterminePrintAndSumOverlappedMarket: exchangeId_Source = " + str(exchangeId_Source) + ", sumDict_eth = " + str(sumDict_eth))
    if exchangeId_DX in coinMarketCaps_MarketDict and exchangeId_Source in coinMarketCaps_MarketDict:
        message = "{0:15}{1:30}{2:40}{3:20}".format(exchangeId_Source, marketId, "Overlaps with DX, Daily volume (ETH) ", str(CoinMarketCaps_DXMarketDict[marketId][1]))
        # PrintAndLog(exchangeId_Source + ", " + marketId + ", both DX and this exchange. Daily volume in terms of ETH = " + str(CoinMarketCaps_DXMarketDict[marketId][1]))
        PrintAndLog(message)

        if exchangeId_Source not in sumDict_eth:
            sumDict_eth[exchangeId_Source] = 0
        # Sum the ETH for this exchange
        sumDict_eth[exchangeId_Source] += marketTuple[2]


def CalculateExpectedProfitForTrade_Bancor(etherArray, tokensArray, index, side, profitPercentage, price):
    return CalculateExpectedProfitForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, profitPercentage, price)


def CalculateExpectedProfitForTrade_Kyber(etherArray, tokensArray, index, side, profitPercentage, price):
    return CalculateExpectedProfitForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, profitPercentage, price)


def CalculateExpectedProfitForTrade_Uniswap(etherArray, tokensArray, index, side, profitPercentage, price):
    return CalculateExpectedProfitForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, profitPercentage, price)


def CalculateExpectedProfitForTrade_Set(etherArray, tokensArray, index, side, profitPercentage, price):
    return CalculateExpectedProfitForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, profitPercentage, price)


# def CalculateExpectedProfitForTrade_Uniswap(etherArray, index, profitPercentage):
#     return CalculateExpectedProfitForTrade_EtherAlwaysDictates(etherArray, index, profitPercentage)


def CalculateExpectedProfitForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, profitPercentage, price):
    expectedProfit_eth = None

    # When buying tokens, etherArray dictates
    if Libraries.core.IsBuy(side):
        expectedProfit_eth = profitPercentage * etherArray[index]
    # When selling tokens, tokensArray dictates
    elif Libraries.core.IsSell(side):
        expectedProfit_tokens = profitPercentage * tokensArray[index]
        # Convert the expectedProfit to ether because that's what the return value is
        expectedProfit_eth = Libraries.core.ConvertTokensToEther(expectedProfit_tokens, price)
    else:
        raise Exception("Not buy or sell")

    return expectedProfit_eth


def CalculateExpectedProfitForTrade_EtherAlwaysDictates(etherArray, index, profitPercentage):
    # Ether always dictates price
    expectedProfit_eth = profitPercentage * etherArray[index]
    return expectedProfit_eth


def CalculateEffectiveEtherAndTokensForTrade_Bancor(etherArray, tokensArray, index, side, price):
    return CalculateEffectiveEtherAndTokensForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, price)


def CalculateEffectiveEtherAndTokensForTrade_Kyber(etherArray, tokensArray, index, side, price):
    return CalculateEffectiveEtherAndTokensForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, price)


def CalculateEffectiveEtherAndTokensForTrade_Uniswap(etherArray, tokensArray, index, side, price):
    return CalculateEffectiveEtherAndTokensForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, price)


def CalculateEffectiveEtherAndTokensForTrade_Set(etherArray, tokensArray, index, side, price):
    return CalculateEffectiveEtherAndTokensForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, price)


def CalculateEffectiveEtherAndTokensForTrade_SourceTokenDictates(etherArray, tokensArray, index, side, price):
    effectiveEther = None
    effectiveTokens = None

    # When buying tokens, etherArray dictates
    if Libraries.core.IsBuy(side):
        effectiveEther = etherArray[index]
        # Set the other one based on the one that dictates
        effectiveTokens = Libraries.core.ConvertEtherToTokens(effectiveEther, price)
    # When selling tokens, tokensArray dictates
    elif Libraries.core.IsSell(side):
        effectiveTokens = tokensArray[index]
        # Set the other one based on the one that dictates
        effectiveEther = Libraries.core.ConvertTokensToEther(effectiveTokens, price)
    else:
        raise Exception("Not buy or sell")

    return effectiveEther, effectiveTokens


def CalculateEffectiveEtherAndTokensForTrade_EtherAlwaysDictates(etherArray, index, price):
    # Ether always dictates price
    effectiveEther = etherArray[index]
    # Set the other one based on the one that dictates
    effectiveTokens = Libraries.core.ConvertEtherToTokens(effectiveEther, price)

    return effectiveEther, effectiveTokens


def API_GetMyActiveOrders_DX_AssumesDataInMarketDictCache(exchangeDX, publicAddress):
    # This assumes data is in the MarketDict data cache
    # PrintAndLog("API_GetMyActiveOrders_DX_AssumesDataInMarketDictCache for " + publicAddress)
    matchingOrdersList = []

    bigNumber = 10000
    orders = exchangeDX.GetTopNBuyOrders(bigNumber) + exchangeDX.GetTopNSellOrders(bigNumber)

    for order in orders:
        # print "order.user = ", order.user, ", price = ", order.price
        if order.GetMaker().lower() == publicAddress.lower():
            matchingOrdersList.append(order)
            PrintAndLog("Found active " + exchangeDX.exchangeName + " for " + order.GetMaker() + ", price = " + str(order.GetPrice()))

    if len(matchingOrdersList) <= 0:
        PrintAndLog("No active " + exchangeDX.exchangeName + " orders were found matching this address: " + publicAddress)

    return matchingOrdersList


def SweepEtherFromAccount(publicAddress_toSweep, privateKey_toSweep, destinationAddress, gasPrice):
    from Libraries.transactions import API_SendEther_ToAddress, TransactionType

    # Sweep ether and send to destinationAddress
    gas = Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt
    balance_ether = Libraries.core.API_GetEtherBalance(publicAddress_toSweep)
    PrintAndLog("balance_ether " + str(publicAddress_toSweep) + " = " + str(balance_ether) + " ETH")
    if balance_ether > 0.001:
        gasCost_ether = 1.1 * Libraries.core.GetGasCost(gasPrice, gas)
        PrintAndLog("gasCost_ether = " + str(gasCost_ether))
        value_wei_int = Libraries.core.ConvertEtherToWei(balance_ether - gasCost_ether, Libraries.core.Ether_Decimals)
        return API_SendEther_ToAddress(publicAddress_toSweep, privateKey_toSweep, destinationAddress,
                                       value_wei_int, gas, gasPrice, TransactionType.other)


def SweepTokensFromAccount(publicAddress_toSweep, privateKey_toSweep, destinationAddress, tokenContractAddress, tokenDecimals):
    # Sweep tokens and send to destinationAddress
    tokenBalance_weiUnits = Libraries.core.API_GetTokenBalance(
        publicAddress_toSweep, tokenContractAddress, tokenDecimals,
        None, None, None, Libraries.core.RequestTimeout_seconds, None, Libraries.core.Units.Wei)
    PrintAndLog("tokenBalance_weiUnits " + str(publicAddress_toSweep) + " = " + str(tokenBalance_weiUnits) + " Tokens (" + str(tokenContractAddress) + ")")
    # TODO, implement


def ReadJsonFromFile(filepath):
    file = open(filepath, "r")
    data = file.read()
    file.close()
    # return json.loads(json.dumps(data))
    return json.loads(str(data))


def IsTokenAKnownEtherToken(tokenAddress):
    from Libraries.core import GetEtherContractAddress
    from Exchanges.keeperDAO import EthTokenContract as KeeperDAO_EthTokenContract
    from Exchanges.kyber import EtherToken as Kyber_EtherToken

    if tokenAddress.lower() == GetEtherContractAddress().lower() or \
            tokenAddress.lower() == KeeperDAO_EthTokenContract.lower() or \
            tokenAddress.lower() == Kyber_EtherToken.lower():
        return True
    else:
        return False


def DoAnyItemsInTheseListsIntersect(list1, list2):
    return any(x in list1 for x in list2)


def FindIntersectingValuesInLists(list1, list2):
    set1 = set(list1)
    set2 = set(list2)
    if set1 & set2:
        return list(set1 & set2)
    else:
        return []


def DoesStringContainAllSubStrings(stringToCheck, subStringList):
    for subString in subStringList:
        if subString not in stringToCheck:
            return False

    return True


def RemoveListOfItemsFromList(itemsList, itemsToRemoveList):
    new_list = []
    for fruit in itemsList:
        if fruit not in itemsToRemoveList:
            new_list.append(fruit)

    return new_list


def RemoveItemsFromThisListThatAreNotFoundInThatList(thisList, thatList):
    newThisList = []
    for item in thisList:
        if item in thatList:
            newThisList.append(item)

    return newThisList


def ConvertRateToPrice(rate, side):
    # When buying
    if Libraries.core.IsBuy(side):
        # rate is inverted of the price
        return 1 / float(rate)
    # When selling
    elif Libraries.core.IsSell(side):
        # rate is the price
        return rate
    else:
        raise Exception("side must be buy or sell!")


def ConvertPriceToRate(price, side):
    # It's the same function!
    return ConvertRateToPrice(price, side)


def GetProfitPercentageFromRatesList(ratesList):
    ratesMultiplied = 1.0
    if len(ratesList) <= 1:
        raise Exception("ratesList must have at least two items")

    for rate in ratesList:
        ratesMultiplied *= rate

    return ratesMultiplied - 1.0


def RemoveOutliersFromList(data):
    if len(data) <= 2:
        return data
    else:
        elements = np.array(data)

        mean = np.mean(elements, axis=0)
        sd = np.std(elements, axis=0)
        # If the standard deviation is zero, just return the original list because all elements are assumed to be the same
        if sd == 0:
            return data
        else:
            final_list = [x for x in data if (x > mean - 2 * sd)]
            final_list = [x for x in final_list if (x < mean + 2 * sd)]
            return final_list


def AverageListContents(numberList):
    return sum(numberList) / len(numberList)


def GetClassNameAndFunctionName():
    stackLocation = inspect.stack()[1][3]
    functionName = stackLocation
    className = "UnknownClassName"
    # If the functionName is an init of a class, we don't want ot just print __init__ because that's not very helpful or informative
    # Instead print ClassName:__init__
    if '__init__' in functionName.lower():
        try:
            className = inspect.stack()[1][0].f_locals["self"].__class__.__name__
            # className = stackLocation.f_locals["self"].__class__.__name__
            functionName = className + ':' + functionName

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception in PrintAndLog_FuncNameHeader, " + traceback.format_exc())

    return className, functionName


def GetCurrent_NumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks():
    # I'm seeing an issue where the bot has a "false start" where it will frequently fail to get data because
    # initializing empty rates graphs is so computationally expensive such that it cannot load in time before new blocks arrive
    # To alleviate this issue I'm starting currentNumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks at a very small number
    # Just to get the bot loading quicker, then later it an up currentNumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks to an ideal number
    if GetTimeSinceNinjaLoopLoadingCompleted_seconds() < 60:
        return Libraries.defaults.InitialNumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks
    else:
        return Libraries.defaults.TargetNumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks


def SetCPUCount():
    global CPUCount

    # You must call this on the main process
    CPUCount = multiprocessing.cpu_count()


def RemoveLastNItemsFromList(myList, n):
    return myList[:-n or None]


def UpdateList_KeepOnlyTopNItemsInList(myList, n):
    if len(myList) > n:
        amountWeAreOverMaxAllowed = len(myList) - n
        return Libraries.utils.RemoveLastNItemsFromList(myList, amountWeAreOverMaxAllowed)
    else:
        return myList


def GenerateHashOfFunctionCallAndArguments(functionCall, functionArguments):
    # functionArguments must be a tuple
    str_functionCall = str(functionCall)
    str_functionArguments = ''
    for functionArgument in functionArguments:
        str_functionArguments += str(functionArgument)
        str_functionArguments += ','

    # Is there any point in hashing it here? Or should we just leave it as a string so it's readable?
    result = (str_functionCall + str_functionArguments).lower()
    PrintAndLog_FuncNameHeader("result = " + str(result))
    return result


def ExecuteGenericFunctionCall(resultsDict, key, functionCall, functionArguments):
    result = functionCall(*functionArguments)
    ConsiderSettingResultDict(result, resultsDict, key)
    return result
