from enum import Enum
from web3 import Web3, HTTPProvider

from Libraries.alchemy import ApiKey_Alchemy
from Libraries.spark import ApiKey_Spark
import Libraries.defaults

Joeyz_NYC3_geth_23 = "167.172.128.48"
Joeyz_FRA1_geth_16 = "207.154.232.113"
Joeyz_FRA1_geth_17 = "207.154.221.92"
Joeyz_FRA1_geth_18 = "207.154.230.50"
Joeyz_FRA1_geth_19 = "207.154.232.145"
Joeyz_FRA1_geth_20 = "142.93.102.23"

Bloxroute_FRA1_geth_17 = "34.244.70.235"
Bloxroute_FRA1_geth_16 = "34.244.70.235"

Port_http_Node = 8545
Port_ws_Node = 8546
Port_ws_Bloxroute = 28353

RemoteNodeList = []
RemoteNodeList_All = []
RemoteNodeList_QuoteTokenProcessSelection = []
RemoteNodeList_BroadcastingTxsAndPendingTxsOnly = []
RemoteNodeList_Backup = []
RemoteNodeWebsocketsList = []
BloxrouteList = []
Instance_Web3 = None


def GetHttpUrlGivenIpAddress(ipAddress, port):
    return "http://" + ipAddress + ":" + str(port)


def GetWsUrlGivenIpAddress(ipAddress, port):
    return "ws://" + ipAddress + ":" + str(port)


class URL_RemoteNode(Enum):
    infura_highPriority = "https://mainnet.infura.io/v3/868256b838e145f2b01b4d1672cbad98"
    infura_lowPriority = "https://mainnet.infura.io/v3/868256b838e145f2b01b4d1672cbad98"
    infura_1 = "https://mainnet.infura.io/v3/868256b838e145f2b01b4d1672cbad98"
    alchemy = "https://eth-mainnet.alchemyapi.io/v2/" + ApiKey_Alchemy  # Works, but performance isn't as fast as they're claiming, is archive though!
    spark = 'http://api.taichi.network:10000/rpc/' + ApiKey_Spark
    spark_eu = 'http://api-eu.taichi.network:10000/rpc/' + ApiKey_Spark

    etherCattle = "https://mainnet.ethercattle.openrelay.xyz/"
    cloudflare = "https://cloudflare-eth.com"
    radar = "https://shared-geth-mainnet.nodes.deploy.radar.tech/?apikey="  # TODO, needs API key
    linkPool = "https://main-rpc.linkpool.io"  # Found this one here: https://medium.com/linkpool/release-of-public-ethereum-rpcs-f5dd57455d2e

    rinkeby = "https://rinkeby.infura.io/gHfUvVz0MiKVLnGLxTjX"
    kovan = "https://kovan.infura.io/gHfUvVz0MiKVLnGLxTjX"

    Joeyz_NYC3_geth_23 = GetHttpUrlGivenIpAddress(Joeyz_NYC3_geth_23, Port_http_Node)
    joeyz_FRA1_geth_16 = GetHttpUrlGivenIpAddress(Joeyz_FRA1_geth_16, Port_http_Node)
    joeyz_FRA1_geth_17 = GetHttpUrlGivenIpAddress(Joeyz_FRA1_geth_17, Port_http_Node)
    joeyz_FRA1_geth_18 = GetHttpUrlGivenIpAddress(Joeyz_FRA1_geth_18, Port_http_Node)
    joeyz_FRA1_geth_19 = GetHttpUrlGivenIpAddress(Joeyz_FRA1_geth_19, Port_http_Node)
    joeyz_FRA1_geth_20 = GetHttpUrlGivenIpAddress(Joeyz_FRA1_geth_20, Port_http_Node)

    bloxroute_FRA1_geth_17 = GetWsUrlGivenIpAddress(Bloxroute_FRA1_geth_17, Port_ws_Bloxroute)
    bloxroute_FRA1_geth_16 = GetWsUrlGivenIpAddress(Bloxroute_FRA1_geth_16, Port_ws_Bloxroute)

    joey_VA_parity_1 = "http://ec2-3-218-58-128.compute-1.amazonaws.com:8545"


class URL_RemoteNode_Websockets(Enum):
    Joeyz_NYC3_geth_23 = GetWsUrlGivenIpAddress(Joeyz_NYC3_geth_23, Port_ws_Node)
    joeyz_FRA1_geth_16 = GetWsUrlGivenIpAddress(Joeyz_FRA1_geth_16, Port_ws_Node)
    joeyz_FRA1_geth_17 = GetWsUrlGivenIpAddress(Joeyz_FRA1_geth_17, Port_ws_Node)
    joeyz_FRA1_geth_18 = GetWsUrlGivenIpAddress(Joeyz_FRA1_geth_18, Port_ws_Node)
    joeyz_FRA1_geth_19 = GetWsUrlGivenIpAddress(Joeyz_FRA1_geth_19, Port_ws_Node)
    joeyz_FRA1_geth_20 = GetWsUrlGivenIpAddress(Joeyz_FRA1_geth_20, Port_ws_Node)


def ConfigureNodes():
    global RemoteNodeList
    global RemoteNodeList_All
    global RemoteNodeList_QuoteTokenProcessSelection
    global RemoteNodeList_BroadcastingTxsAndPendingTxsOnly
    global RemoteNodeList_Backup
    global RemoteNodeWebsocketsList
    global BloxrouteList
    global Instance_Web3

    # Nodes that are available and have the most recent block data
    RemoteNodeList = []
    # Nodes that are available to use, but may not have the most recent block data
    RemoteNodeList_All = []
    # Nodes that are available and have the most recent block data, including Backup options
    # I'm putting this in a separate list because the RemoteNodeList removes backup options,
    # but the RemoteNodeList_QuoteTokenProcessSelection wants to consider all options even backups
    RemoteNodeList_QuoteTokenProcessSelection = []
    # Nodes that we can broadcast txs to, this can include extra nodes we don't normally make calls to
    RemoteNodeList_BroadcastingTxsAndPendingTxsOnly = []
    # Nodes that we want to ONLY make normal calls to if our nodes in RemoteNodeList_All do not have the latest block information
    RemoteNodeList_Backup = []

    # List of nodes that support websockets
    # TODO refactor and rename all of these lists to something more meaningful
    RemoteNodeWebsocketsList = []

    BloxrouteList = []

    if Libraries.defaults.ActiveNetwork == Libraries.defaults.Network.mainnet:
        # All nodes websocket connections regardless of whether or not they're primary or backup
        if Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.DE:
            RemoteNodeWebsocketsList.append(URL_RemoteNode_Websockets.joeyz_FRA1_geth_16)
            RemoteNodeWebsocketsList.append(URL_RemoteNode_Websockets.joeyz_FRA1_geth_17)
            RemoteNodeWebsocketsList.append(URL_RemoteNode_Websockets.joeyz_FRA1_geth_18)
            RemoteNodeWebsocketsList.append(URL_RemoteNode_Websockets.joeyz_FRA1_geth_19)
            RemoteNodeWebsocketsList.append(URL_RemoteNode_Websockets.joeyz_FRA1_geth_20)
        elif Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.US:
            RemoteNodeWebsocketsList.append(URL_RemoteNode_Websockets.Joeyz_NYC3_geth_23)
        else:
            pass

        # Bloxroute gateway websocket connections
        if Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.DE:
            BloxrouteList.append(URL_RemoteNode.bloxroute_FRA1_geth_16)
        elif Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.US:
            BloxrouteList.append(URL_RemoteNode.bloxroute_FRA1_geth_17)
        else:
            pass

        # # All nodes http connections regardless of whether or not they're primary or backup
        # if TimeMachine_EnabledForHistoricBlockDebug:
        #     # We require a full archive node when using this feature
        #     RemoteNodeList_All.append(URL_RemoteNode.alchemy)
        #     # RemoteNodeList_All.append(URL_RemoteNode.joeyz_FRA1_geth_16)
        if Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.DE:
            RemoteNodeList_All.append(URL_RemoteNode.joeyz_FRA1_geth_16)
            RemoteNodeList_All.append(URL_RemoteNode.joeyz_FRA1_geth_17)
            RemoteNodeList_All.append(URL_RemoteNode.joeyz_FRA1_geth_18)
            RemoteNodeList_All.append(URL_RemoteNode.joeyz_FRA1_geth_19)
            RemoteNodeList_All.append(URL_RemoteNode.joeyz_FRA1_geth_20)
        elif Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.US:
            RemoteNodeList_All.append(URL_RemoteNode.Joeyz_NYC3_geth_23)
        elif Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.AlchemyOnly:
            RemoteNodeList_All.append(URL_RemoteNode.alchemy)

        # TODO, these are now used for not only backup to primary but also for spreading the load of quoteToken process calls across multiple nodes
        #  Should I rename this?
        #  Should I break this into 2 separate groups?
        #    One group as primary backups and another as quoteToken process alternatives?
        #    I probably should, but i'm just happen it's working as intended right now and don't want to break them up just yet
        # This list dictates whether or not a node is a backup or primary
        # All nodes should be in RemoteNodeList_All, but only backups should be included here
        # So if a node is not included in this list and it's in RemoteNodeList_All, then it is assumed to be a primary
        # Use these as backups only in the event my normal nodes do not have the latest block information
        if Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.DE:
            pass
            # RemoteNodeList_Backup.append(URL_RemoteNode.alchemy)
        elif Libraries.defaults.ActiveNodeInfrastructure == Libraries.defaults.NodeInfrastructure.US:
            pass

        # Use these nodes for data gathering and tax purposes only
        # RemoteNodeList_All.append(URL_RemoteNode.joey_VA_parity_1)

        # Node services
        # RemoteNodeList_All.append(URL_RemoteNode.alchemy)
        # RemoteNodeList_All.append(URL_RemoteNode.spark_eu)
        # RemoteNodeList_All.append(URL_RemoteNode.spark)
        # RemoteNodeList_All.append(URL_RemoteNode.infura_1)  # Infura is rate limiting me, so don't use them unless I have to
        # RemoteNodeList_All.append(URL_RemoteNode.radar)  # TODO, this one costs $0.0000125 per call, be careful using it only occasionally when needing another node for testing

        # Add some nodes to RemoteNodeList_BroadcastingTxsAndPendingTxsOnly if they aren't already in RemoteNodeList_All
        # TODO Leaving alchemy turned off for now.
        # remoteNode = URL_RemoteNode.alchemy
        # if remoteNode not in RemoteNodeList_All:
        #     RemoteNodeList_BroadcastingTxsAndPendingTxsOnly.append(remoteNode)

        # remoteNode = URL_RemoteNode.spark_eu
        # if remoteNode not in RemoteNodeList_All:
        #     RemoteNodeList_BroadcastingTxsAndPendingTxsOnly.append(remoteNode)

        # remoteNode = URL_RemoteNode.spark
        # if remoteNode not in RemoteNodeList_All:
        #     RemoteNodeList_BroadcastingTxsAndPendingTxsOnly.append(remoteNode)

    elif Libraries.defaults.ActiveNetwork == Libraries.defaults.Network.kovanTestnet:
        RemoteNodeList_All.append(URL_RemoteNode.kovan)

    else:
        raise Exception("Network " + str(Libraries.defaults.ActiveNetwork) + " has not yet been assigned a remote node.")

    # Initialize RemoteNodeList
    for item in RemoteNodeList_All:
        RemoteNodeList.append(item)

    # TODO, this is ugly. Find a better way to get a hold of a dedicated Instance_Web3
    Instance_Web3 = Web3(HTTPProvider(RemoteNodeList[0].value))


# Call ConfigureNodes with the default setup
# It can be called again later if the settings are changed
ConfigureNodes()
