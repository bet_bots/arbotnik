import datetime
import threading
from threading import Lock
import time
import traceback

from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader

import Libraries.node

PendingTransactionGatheringService = None

NodeTxMemPoolDict = {}
Lock_NodeTxMemPoolDict = Lock()

RunningServicesList = []


class Thread_PendingTransactionGathering(object):
    nodeKey = None

    def __init__(self, _nodeKey):
        global RunningServicesList

        self.nodeKey = _nodeKey

        if self.nodeKey.lower() in RunningServicesList:
            raise Exception("Service with nodeKey " + str(self.nodeKey.lower()) + " already running")

        PrintAndLog("Thread_PendingTransactionGathering: starting pending tx gathering service for node: " + str(self.nodeKey.lower()))
        RunningServicesList.append(self.nodeKey.lower())

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        global NodeTxMemPoolDict
        global Lock_NodeTxMemPoolDict

        """ Method that runs forever """
        while True:
            try:
                # make the API call OUTSIDE of the lock because it can take a very long time
                before = datetime.datetime.now()

                # Determine which node to get pending transactions from
                txMemPoolDict = None
                if self.nodeKey.lower() == 'etherscan':
                    txMemPoolDict = Libraries.node.API_GetMemPoolTransactions_EtherScan({})
                else:
                    url = self.nodeKey.lower()
                    txMemPoolDict = Libraries.node.API_GetMemPoolTransactions_ParitySpecificNode(url, {})

                duration_s = (datetime.datetime.now() - before).total_seconds()
                PrintAndLog("Thread_PendingTransactionGathering: txMemPoolDict len = " + str(len(txMemPoolDict)) + ", duration_s = " + str(
                    duration_s) + ", self.nodeKey = " + str(self.nodeKey))

                # Only set the result inside the lock
                Lock_NodeTxMemPoolDict.acquire()
                try:
                    NodeTxMemPoolDict[self.nodeKey.lower()] = txMemPoolDict

                finally:
                    Lock_NodeTxMemPoolDict.release()

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception = " + traceback.format_exc())
                pass

            time.sleep(1.0)


def StartService(nodeKey):
    global PendingTransactionGatheringService
    # Start thread that periodically updates the on chain price for each token we're interested in
    PendingTransactionGatheringService = Thread_PendingTransactionGathering(nodeKey)


def GetThreadSafeCopyOf_TxMemPoolDict():
    global NodeTxMemPoolDict
    global Lock_NodeTxMemPoolDict

    from Libraries.core import MergeDictionaries

    # Get a copy of the TxMemPoolDict in a thread safe way
    # Each node has it's own dict, I need to obtain each node's dict, then merge them together
    returnDict = {}
    Lock_NodeTxMemPoolDict.acquire()
    try:
        for key in NodeTxMemPoolDict:
            txMemPoolDict = NodeTxMemPoolDict[key]
            returnDict = MergeDictionaries(returnDict, txMemPoolDict)
            PrintAndLog_FuncNameHeader("txMemPoolDict for " + str(key) + " has " + str(len(txMemPoolDict)) + " items")

    finally:
        Lock_NodeTxMemPoolDict.release()

    PrintAndLog_FuncNameHeader("returnDict has " + str(len(returnDict)) + " items")

    return returnDict


def GetNodesWhichAreAwareOfThisTxsExistence(txHash):
    global NodeTxMemPoolDict
    global Lock_NodeTxMemPoolDict

    listOfNodes_awareOfThisTxHash = []
    Lock_NodeTxMemPoolDict.acquire()
    try:
        for key in NodeTxMemPoolDict:
            txMemPoolDict = NodeTxMemPoolDict[key]
            if txHash.lower() in txMemPoolDict.keys():
                listOfNodes_awareOfThisTxHash.append(key)

    finally:
        Lock_NodeTxMemPoolDict.release()

    PrintAndLog_FuncNameHeader("txHash has " + str(len(listOfNodes_awareOfThisTxHash)) + " nodes aware of its existence, listOfNodes_awareOfThisTxHash = " + str(
        listOfNodes_awareOfThisTxHash) + ", txHash = " + str(txHash))
    return listOfNodes_awareOfThisTxHash
