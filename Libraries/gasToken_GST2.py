import Libraries.nodes

MaxSafeNumberOfGasTokensToMint = 53
Contract_GST2 = Libraries.nodes.Instance_Web3.toChecksumAddress("0x0000000000b3f879cb30fe243b4dfee438691c04")


# def GetGas_GivenQuantityOfGasTokensToMint(quantity):
#     from Libraries.core import GasUsedWhenExecutingEmptyFunctionWithNothingInIt
#     return GasUsedWhenExecutingEmptyFunctionWithNothingInIt + (39400 * quantity)


def EnforceSafeQuantityOfGasTokensToMint(quantity):
    return min(quantity, MaxSafeNumberOfGasTokensToMint)
