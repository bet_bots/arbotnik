import traceback
import datetime

from Libraries.loggingConfig import PrintAndLogError, PrintAndLog
from threading import Lock
from Libraries.core import API_PostOperatorNotification
import Libraries.coinGecko
import Libraries.utils
import Libraries.cryptoCompare
import Libraries.defaults

# Dictionary containing ETH-USD prices from various exchanges so my stable coins can verify prices
PriceDict_ETHUSD = {}
Lock_PriceDict_ETHUSD = Lock()
# Current price based on the PriceDict
CurrentPrice_ETHUSD = None
Lock_CurrentPrice_ETHUSD = Lock()

# Dictionary containing price data from CoinMarketCap/CoinGecko, or any similar service
CoinMarketCapMarketsDict = {}

DateTimeOfLast_UpdateCoinMarketCapMarkets = None
Interval_seconds_UpdateCoinMarketCapMarkets = 10


class CoinMarketCapMarket:
    id = None
    # name = None
    symbol = None
    # rank = None
    price_usd = None
    price_btc = None
    # twentyFourHourVolume_usd = None
    dateTimeDataWasSet = None

    def __init__(self, _id, _symbol, _price_usd, _price_btc):
        self.id = _id
        # self.name = _name
        self.symbol = _symbol
        # self.rank = _rank
        self.price_usd = _price_usd
        self.price_btc = _price_btc
        # self.twentyFourHourVolume_usd = _twentyFourHourVolume_usd
        self.dateTimeDataWasSet = datetime.datetime.now()

    def GetPriceEth(self, coinMarketCapId, CoinMarketCapMarketsDict_toUse):
        price_BTCtoETH = CoinMarketCapMarketsDict_toUse[Libraries.defaults.CoinMarketCapId_Ethereum].price_btc
        price_BTCtoSYMBOL = CoinMarketCapMarketsDict_toUse[coinMarketCapId].price_btc
        # PrintAndLog("price_BTCtoETH = " + str(price_BTCtoETH))
        # PrintAndLog("price_BTCtoSYMBOL = " + str(price_BTCtoSYMBOL))
        price_ETHtoSYMBOL = float(price_BTCtoSYMBOL) / float(price_BTCtoETH)
        # PrintAndLog("price_ETHtoSYMBOL = " + str(price_ETHtoSYMBOL))
        return price_ETHtoSYMBOL

    def GetDataAge_s(self):
        if not self.dateTimeDataWasSet:
            return None
        else:
            return (datetime.datetime.now() - self.dateTimeDataWasSet).total_seconds()


# This works for Coingecko only
def UpdateCoinMarketCapMarkets(numberOfTopMarkets, keyToUseForData="symbol"):
    global CoinMarketCapMarketsDict
    # global CrashOnPurposeFlagWeeeeee
    # PrintAndLog("UpdateCoinMarketCapMarkets with numberOfTopMarkets = " + str(numberOfTopMarkets) + ", and keyToUseForData = " + str(keyToUseForData))

    jData = Libraries.coinGecko.API_GetMarkets_Safe("usd", numberOfTopMarkets)

    # Find Bitcoin as a price reference
    data_Bitcoin = None
    for data in jData:
        if data['id'].lower() == 'bitcoin':
            data_Bitcoin = data

    if not data_Bitcoin:
        message = "UpdateCoinMarketCapMarkets could not find Bitcoin. WTF?"
        API_PostOperatorNotification(message)
        raise Exception(message)

    bitcoinsPrice_usd = float(data_Bitcoin['current_price'])  # CoinGecko

    for data in jData:
        try:
            # PrintAndLog("Iterating over data " + str(data))
            symbol = data[keyToUseForData].lower()

            if not data['current_price']:
                continue

            price_usd = float(data['current_price'])
            price_btc = price_usd / bitcoinsPrice_usd
            # volume24h_usd = float(data['total_volume'])

            coinMarketCapMarket = CoinMarketCapMarket(data['id'], symbol, price_usd, price_btc)
            CoinMarketCapMarketsDict[symbol] = coinMarketCapMarket

        except:
            PrintAndLogError("exception in UpdateCoinMarketCapMarkets: " + traceback.format_exc())
            pass

    return True


# # This works for CryptoCompare only
# def UpdateCoinMarketCapMarkets(numberOfTopMarkets, keyToUseForData="symbol"):
#     from Libraries.marketObserver import CoinMarketCapMarketsDict, CoinMarketCapMarket
#     from Libraries.accounts import MarketDict
#     # PrintAndLog("UpdateCoinMarketCapMarkets with numberOfTopMarkets = " + str(numberOfTopMarkets) + ", and keyToUseForData = " + str(keyToUseForData))
#
#     jData = Libraries.cryptoCompare.API_GetMarkets(Libraries.cryptoCompare.GetListOfSymbolsToUseWhenGettingMarkets())
#     PrintAndLog("UpdateCoinMarketCapMarkets jData = " + str(jData))
#
#     # # Find Bitcoin as a price reference
#     # data_Bitcoin = jData['btc']
#     #
#     # if not data_Bitcoin:
#     #     message = "UpdateCoinMarketCapMarkets could not find Bitcoin. WTF?"
#     #     API_PostOperatorNotification(message)
#     #     raise Exception(message)
#     #
#     # bitcoinsPrice_usd = float(data_Bitcoin['USD'])
#
#     # for data in jData:
#     #     try:
#     #         # PrintAndLog("Iterating over data " + str(data))
#     #         symbol = data[keyToUseForData].lower()
#     #
#     #         if not data['current_price']:
#     #             continue
#     #
#     #         price_usd = float(data['current_price'])
#     #         price_btc = price_usd / bitcoinsPrice_usd
#     #         # volume24h_usd = float(data['total_volume'])
#
#     # Set all tokens from MarketDict
#     for marketName in MarketDict:
#         try:
#             if MarketDict[marketName].marketIsEnabled:
#                 symbol = MarketDict[marketName].GetSecondaryTokenName().upper()
#                 PrintAndLog("Creating CoinMarketCapMarket object for " + str(symbol))
#                 price_usd = jData[symbol]['USD']
#                 price_btc = jData[symbol]['BTC']
#
#                 CoinMarketCapMarketsDict[symbol] = CoinMarketCapMarket(symbol, symbol, price_usd, price_btc)
#
#         except (KeyboardInterrupt, SystemExit):
#             print('\nkeyboard interrupt caught')
#             print('\n...Program Stopped Manually!')
#             raise
#
#         except:
#             PrintAndLogError("exception in UpdateCoinMarketCapMarkets: " + traceback.format_exc())
#             pass
#
#     # Set ETH
#     try:
#         symbol_eth = 'ETH'
#         price_usd = jData[symbol_eth]['USD']
#         price_btc = jData[symbol_eth]['BTC']
#         CoinMarketCapMarketsDict[CoinMarketCapId_Ethereum] = CoinMarketCapMarket(CoinMarketCapId_Ethereum, symbol_eth, price_usd, price_btc)
#
#     except (KeyboardInterrupt, SystemExit):
#         print('\nkeyboard interrupt caught')
#         print('\n...Program Stopped Manually!')
#         raise
#
#     except:
#         PrintAndLogError("exception in UpdateCoinMarketCapMarkets: " + traceback.format_exc())
#         pass
#
#     return True


def ConsiderUpdatingCoinMarketCapMarkets():
    global DateTimeOfLast_UpdateCoinMarketCapMarkets
    global Interval_seconds_UpdateCoinMarketCapMarkets

    if not DateTimeOfLast_UpdateCoinMarketCapMarkets or \
            ((datetime.datetime.now() - DateTimeOfLast_UpdateCoinMarketCapMarkets).total_seconds() > Interval_seconds_UpdateCoinMarketCapMarkets):
        PrintAndLog("ConsiderUpdatingCoinMarketCapMarkets interval has passed.  Time to call UpdateCoinMarketCapMarkets")
        DateTimeOfLast_UpdateCoinMarketCapMarkets = datetime.datetime.now()

        successfullyGotData = UpdateCoinMarketCapMarkets(1000, "id")
        if not successfullyGotData:
            API_PostOperatorNotification("failed to get data from UpdateCoinMarketCapMarkets")
