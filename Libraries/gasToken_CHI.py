import Libraries.nodes

# This should be approx a hair under 1/2 the block limit, I could make ak formulate to ensure this is exactly tha tas the block limit size changes...
MaxSafeNumberOfGasTokensToMint = 140
Contract_CHI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x0000000000004946c0e9F43F4Dee607b0eF1fA1c")


def EnforceSafeQuantityOfGasTokensToMint(quantity):
    return min(quantity, MaxSafeNumberOfGasTokensToMint)
