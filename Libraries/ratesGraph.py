import datetime
import os
import traceback

from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError, PrintAndLog_Detailed
import Libraries.defaults
import Libraries.core
import Libraries.utils
import Exchanges.keeperDAO
import Exchanges.ninja

# List of tokens used in the PricesGraph
# PricesGraph should always be a 2d array where each inner array is of len RatesGraphTokenList
RatesGraphTokenList = []
# Optimization, a dict that is keyed by the token and valued by the index of the token in the list RatesGraphTokenList
RatesGraphTokenListIndexDict = {}
# Optimization, a dict that is keyed by the exchangeName and valued by the index of the exchangeName in the list ExchangeNamesList_Alphabetized
ExchangeNamesListIndexDict = {}

# The RatesGraphs Dicts below are keyed by block number, and valued by another dict which is keyed by index_string valued by the graph
# The RatesGraphs Empty below is an empty graph. When you want to create a new blank graph, just copy the empty
# 2d array used with arbitrage algorithm
RatesGraph_Dict = {}
# Exchange name of each order in the graph
RatesGraph_Dict_ExchangeNames = {}
# QuoteToken of each order in the graph
RatesGraph_Dict_QuoteTokens = {}

RatesGraph_Dict__Process_QuoteToken = {}
RatesGraph_Dict_ExchangeNames__Process_QuoteToken = {}
RatesGraph_Dict_QuoteTokens__Process_QuoteToken = {}

# I have to initialize a series of RatesGraphs for each block
# I'm doing it a head of time so it doesn't have to be done during a high priority heavy processing load time
# This number keeps track of which block numbers have already been initialized
# This needs initialized to the block number
HighestInitializedRatesGraphBlockNumber = 0
HighestInitializedRatesGraphBlockNumber__Process_QuoteToken = 0


class ExchangeRate:
    rate = None
    quoteToken = None
    exchange = None

    def __init__(self, _rate, _quoteToken, _exchange):
        self.rate = _rate
        self.quoteToken = _quoteToken
        self.exchange = _exchange

    # this method allows sorting, comparison, etc.
    def __ge__(self, other):
        return self.rate >= other.rate


def CreateNewRatesGraphTokenList():
    global RatesGraphTokenList
    RatesGraphTokenList = []


def AddTokenToRatesGraph(tokenAddress):
    global RatesGraphTokenList

    # TODO, this is only used for debugging and unit testing and NOT FOR PRODUCTION
    if tokenAddress.lower() not in RatesGraphTokenList:
        RatesGraphTokenList.append(tokenAddress.lower())

    PrintAndLog_FuncNameHeader("RatesGraphTokenList = " + str(RatesGraphTokenList))


def PopulateRatesGraphTokenList():
    from Libraries.accounts import TokenDict_Ninja
    import Libraries.exchanges

    global RatesGraphTokenList
    global RatesGraphTokenListIndexDict
    global ExchangeNamesListIndexDict

    tokenList = []
    tokenList.append(Exchanges.keeperDAO.EthTokenContract.lower())

    for tokenName in TokenDict_Ninja:
        token = TokenDict_Ninja[tokenName].erc20TokenContractAddress
        if token.lower() not in tokenList:
            tokenList.append(token.lower())

    # It's critical that we never modify RatesGraphTokenList again after this or else it breaks our RatesGraphTokenListIndexDict
    RatesGraphTokenList = tokenList

    # Optimization, a dict that is keyed by the token and valued by the index of the token in this list RatesGraphTokenList
    for token in tokenList:
        # Store the index of the token in the dict so we can easily access that later without having to call .index again
        RatesGraphTokenListIndexDict[token.lower()] = RatesGraphTokenList.index(token.lower())

    # Also populate ExchangeNamesListIndexDict
    for index, exchangeName in enumerate(Libraries.exchanges.ExchangeNamesList_Alphabetized):
        ExchangeNamesListIndexDict[exchangeName] = index

    PrintAndLog_FuncNameHeader("Populated RatesGraphTokenList: " + str(RatesGraphTokenList))


def ConvertListOfTokenIndexesToTokens(tokenIndexList):
    global RatesGraphTokenList

    tokenList = []
    for tokenIndex in tokenIndexList:
        tokenList.append(RatesGraphTokenList[tokenIndex])

    return tokenList


def ConvertListOfExchangeNameIndexesToExchangeNames(exchangeNameIndexList):
    import Libraries.exchanges

    exchangeNameList = []
    for exchangeNameIndex in exchangeNameIndexList:
        exchangeNameList.append(Libraries.exchanges.ExchangeNamesList_Alphabetized[exchangeNameIndex])

    return exchangeNameList


def CreateASetOfEmptyRatesGraphs():
    before = datetime.datetime.now()

    # This function assumes RatesGraphTokenList is already set
    # Initialize RatesGraph using RatesGraphTokenList
    length = len(RatesGraphTokenList)

    ratesGraph_Empty_ExchangeNames = []
    ratesGraph_Empty_QuoteToken = []
    ratesGraph_Empty = []

    for r in range(0, length):
        ratesGraph_Empty_ExchangeNames.append([])
        ratesGraph_Empty_QuoteToken.append([])
        ratesGraph_Empty.append([])

        for c in range(0, length):
            ratesGraph_Empty_ExchangeNames[r].append(0)
            ratesGraph_Empty_QuoteToken[r].append(0)
            if r == c:
                ratesGraph_Empty[r].append(1)
            else:
                ratesGraph_Empty[r].append(0)

    # PrintAndLog_FuncNameHeader("ratesGraph_Empty of size " + str(length) + " x " + str(length) + " = " + str(ratesGraph_Empty))
    duration_s = (datetime.datetime.now() - before).total_seconds()
    # PrintAndLog_FuncNameHeader("Completed with duration_s = " + str(duration_s) + " seconds")
    return ratesGraph_Empty, ratesGraph_Empty_ExchangeNames, ratesGraph_Empty_QuoteToken


def GetSharedGiantStringKey(index_string, row, column):
    return str(index_string) + "_" + str(row) + "_" + str(column)


def InitializeRatesGraphForBlock(blockNumber):
    global RatesGraph_Dict
    global RatesGraph_Dict_ExchangeNames
    global RatesGraph_Dict_QuoteTokens
    global HighestInitializedRatesGraphBlockNumber

    if HighestInitializedRatesGraphBlockNumber >= blockNumber:
        PrintAndLog_FuncNameHeader("we're already initialized for blockNumber " + str(blockNumber) + ", returning")
        return

    before = datetime.datetime.now()

    RatesGraph_Dict[blockNumber] = {}
    RatesGraph_Dict_ExchangeNames[blockNumber] = {}
    RatesGraph_Dict_QuoteTokens[blockNumber] = {}

    quoteTokensToSpendList_etherUnits = Exchanges.keeperDAO.GetQuoteTokensToSpendList_etherUnits(Exchanges.keeperDAO.EthTokenContract)
    for index in range(len(quoteTokensToSpendList_etherUnits) - 1, -1, -1):
        # PrintAndLog_FuncNameHeader("Initializing graphs based on empties for index = " + str(index))
        # Trying creating new graphs every time instead of deepcopy since deepcopy has been slow as hell
        RatesGraph_Dict[blockNumber][str(index)], RatesGraph_Dict_ExchangeNames[blockNumber][str(index)], RatesGraph_Dict_QuoteTokens[blockNumber][str(index)] = \
            CreateASetOfEmptyRatesGraphs()

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Completed with duration_s = " + str(duration_s) + " seconds")

    # Set the new HighestInitializedRatesGraphBlockNumber
    HighestInitializedRatesGraphBlockNumber = blockNumber


def InitializeRatesGraphForBlock__Process_QuoteToken(blockNumber):
    global RatesGraph_Dict__Process_QuoteToken
    global RatesGraph_Dict_ExchangeNames__Process_QuoteToken
    global RatesGraph_Dict_QuoteTokens__Process_QuoteToken
    global HighestInitializedRatesGraphBlockNumber__Process_QuoteToken

    if HighestInitializedRatesGraphBlockNumber__Process_QuoteToken >= blockNumber:
        PrintAndLog_FuncNameHeader("we're already initialized for blockNumber " + str(blockNumber) + ", returning")
        return

    before = datetime.datetime.now()

    RatesGraph_Dict__Process_QuoteToken[blockNumber] = {}
    RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber] = {}
    RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber] = {}

    quoteTokensToSpendList_etherUnits = Exchanges.keeperDAO.GetQuoteTokensToSpendList_etherUnits(Exchanges.keeperDAO.EthTokenContract)
    for index in range(len(quoteTokensToSpendList_etherUnits) - 1, -1, -1):
        # PrintAndLog_FuncNameHeader("Initializing graphs based on empties for index = " + str(index))
        # Trying creating new graphs every time instead of deepcopy since deepcopy has been slow as hell
        RatesGraph_Dict__Process_QuoteToken[blockNumber][str(index)], \
        RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber][str(index)], \
        RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber][str(index)] = \
            CreateASetOfEmptyRatesGraphs()

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Completed with duration_s = " + str(duration_s) + " seconds")

    # Set the new HighestInitializedRatesGraphBlockNumber__Process_QuoteToken
    HighestInitializedRatesGraphBlockNumber__Process_QuoteToken = blockNumber


def IsDataAvailableForBlockNumber(blockNumber):
    global RatesGraph_Dict

    if blockNumber in RatesGraph_Dict:
        return True
    else:
        return False


def GetRatesGraphDicts(blockNumber, index_string):
    global RatesGraph_Dict
    global RatesGraph_Dict_ExchangeNames
    global RatesGraph_Dict_QuoteTokens

    # PrintAndLog_FuncNameHeader("RatesGraph_Dict[blockNumber].keys() = " + str(RatesGraph_Dict[blockNumber].keys()))
    # PrintAndLog_FuncNameHeader("RatesGraph_Dict_ExchangeNames[blockNumber].keys() = " + str(RatesGraph_Dict_ExchangeNames[blockNumber].keys()))
    # PrintAndLog_FuncNameHeader("RatesGraph_Dict_QuoteTokens[blockNumber].keys() = " + str(RatesGraph_Dict_QuoteTokens[blockNumber].keys()))
    if index_string:
        return RatesGraph_Dict[blockNumber][index_string], \
               RatesGraph_Dict_ExchangeNames[blockNumber][index_string], \
               RatesGraph_Dict_QuoteTokens[blockNumber][index_string]
    else:
        return RatesGraph_Dict[blockNumber], \
               RatesGraph_Dict_ExchangeNames[blockNumber], \
               RatesGraph_Dict_QuoteTokens[blockNumber]


def GetRatesGraphDicts__Process_QuoteToken(blockNumber, index_string):
    global RatesGraph_Dict__Process_QuoteToken
    global RatesGraph_Dict_ExchangeNames__Process_QuoteToken
    global RatesGraph_Dict_QuoteTokens__Process_QuoteToken

    # PrintAndLog_FuncNameHeader("RatesGraph_Dict[blockNumber].keys() = " + str(RatesGraph_Dict[blockNumber].keys()))
    # PrintAndLog_FuncNameHeader("RatesGraph_Dict_ExchangeNames[blockNumber].keys() = " + str(RatesGraph_Dict_ExchangeNames[blockNumber].keys()))
    # PrintAndLog_FuncNameHeader("RatesGraph_Dict_QuoteTokens[blockNumber].keys() = " + str(RatesGraph_Dict_QuoteTokens[blockNumber].keys()))
    if index_string:
        return RatesGraph_Dict__Process_QuoteToken[blockNumber][index_string], \
               RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber][index_string], \
               RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber][index_string]
    else:
        return RatesGraph_Dict__Process_QuoteToken[blockNumber], \
               RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber], \
               RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber]


def IsBlockReadyInRatesGraphDicts__Process_QuoteToken(blockNumber):
    global RatesGraph_Dict__Process_QuoteToken
    global RatesGraph_Dict_ExchangeNames__Process_QuoteToken
    global RatesGraph_Dict_QuoteTokens__Process_QuoteToken

    if blockNumber in RatesGraph_Dict__Process_QuoteToken and \
            blockNumber in RatesGraph_Dict_ExchangeNames__Process_QuoteToken and \
            blockNumber in RatesGraph_Dict_QuoteTokens__Process_QuoteToken:
        return True
    else:
        return False


def InitializeRatesGraphsForNearFutureBlocks_ThenCall_RemoveOldRatesGraphs():
    InitializeRatesGraphsForNearFutureBlocks()
    RemoveOldRatesGraphs()


def InitializeRatesGraphsForNearFutureBlocks__Process_QuoteToken__ThenCall__RemoveOldRatesGraphs__Process_QuoteToken(latestBlockNumber):
    # header_logging = "InitializeRatesGraphsForNearFutureBlocks__Process_QuoteToken__ThenCall__RemoveOldRatesGraphs__Process_QuoteToken: pid " + str(os.getpid())
    # functionsStartDateTime = datetime.datetime.now()

    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Begin")
    InitializeRatesGraphsForNearFutureBlocks__Process_QuoteToken(latestBlockNumber)
    RemoveOldRatesGraphs__Process_QuoteToken(latestBlockNumber)
    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Finished")


def RemoveOldRatesGraphs():
    global RatesGraph_Dict
    global RatesGraph_Dict_ExchangeNames
    global RatesGraph_Dict_QuoteTokens

    # Call this periodically

    # Remove old data within the RatesGraphs dicts
    # If the blockNumber is old, delete the data
    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    # PrintAndLog_FuncNameHeader("Begin with latestBlockNumber = " + str(latestBlockNumber))
    blockNumberExpirationThreshold = Libraries.defaults.BlockAge_blocks_WhenABlockIsConsideredOldForDataCleanUpPurposes
    blockNumbersToDelete = []
    for blockNumber in RatesGraph_Dict:
        if blockNumber < latestBlockNumber - blockNumberExpirationThreshold:
            blockNumbersToDelete.append(blockNumber)

    for blockNumber in blockNumbersToDelete:
        try:
            before = datetime.datetime.now()
            PrintAndLog_FuncNameHeader("Removing RatesGraph_Dict for blockNumber = " + str(blockNumber))
            # PrintAndLog_FuncNameHeader("Reference count: RatesGraph_Dict[blockNumber] = " + str(sys.getrefcount(RatesGraph_Dict[blockNumber])))
            # PrintAndLog_FuncNameHeader("Reference count: RatesGraph_Dict = " + str(sys.getrefcount(RatesGraph_Dict)))
            del RatesGraph_Dict[blockNumber]
            del RatesGraph_Dict_ExchangeNames[blockNumber]
            del RatesGraph_Dict_QuoteTokens[blockNumber]

            duration_s = (datetime.datetime.now() - before).total_seconds()
            PrintAndLog_FuncNameHeader("Finished Removing RatesGraph_Dict for blockNumber = " + str(
                blockNumber) + ", duration_s = " + str(duration_s) + " seconds")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when trying to clean up an old RatesGraph_Dict: " + traceback.format_exc())
            pass


def RemoveOldRatesGraphs__Process_QuoteToken(latestBlockNumber):
    global RatesGraph_Dict__Process_QuoteToken
    global RatesGraph_Dict_ExchangeNames__Process_QuoteToken
    global RatesGraph_Dict_QuoteTokens__Process_QuoteToken

    # Call this periodically
    # PrintAndLog_FuncNameHeader("Begin with latestBlockNumber = " + str(latestBlockNumber))

    # Remove old data within the RatesGraphs dicts
    # If the blockNumber is old, delete the data
    blockNumberExpirationThreshold = Libraries.defaults.BlockAge_blocks_WhenABlockIsConsideredOldForDataCleanUpPurposes
    blockNumbersToDelete = []
    for blockNumber in RatesGraph_Dict__Process_QuoteToken:
        if blockNumber < latestBlockNumber - blockNumberExpirationThreshold:
            blockNumbersToDelete.append(blockNumber)

    for blockNumber in blockNumbersToDelete:
        try:
            before = datetime.datetime.now()
            PrintAndLog_FuncNameHeader("Removing RatesGraph_Dict__Process_QuoteToken for blockNumber = " + str(blockNumber))
            # PrintAndLog_FuncNameHeader("Reference count: RatesGraph_Dict__Process_QuoteToken[blockNumber] = " + str(sys.getrefcount(RatesGraph_Dict__Process_QuoteToken[blockNumber])))
            # PrintAndLog_FuncNameHeader("Reference count: RatesGraph_Dict__Process_QuoteToken = " + str(sys.getrefcount(RatesGraph_Dict__Process_QuoteToken)))
            del RatesGraph_Dict__Process_QuoteToken[blockNumber]
            del RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber]
            del RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber]

            # # TODO testing memory usage without all of this stuff.
            # #  Now that I discovered that ZMQ is using so much memory, i may not have a memory leak after all
            #
            # # Example of what the object looks like
            # # RatesGraph_Dict__Process_QuoteToken[blockNumber][index_string][r][c]
            #
            # for index_string in RatesGraph_Dict__Process_QuoteToken[blockNumber]:
            #     for subList in RatesGraph_Dict__Process_QuoteToken[blockNumber][index_string]:
            #         # Clear the contents of the list
            #         del subList[:]
            #         del subList
            #         # subList.clear()  # Clear might not work anymore....
            #     # Clear the contents of the list
            #     del RatesGraph_Dict__Process_QuoteToken[blockNumber][index_string][:]
            #     # del RatesGraph_Dict__Process_QuoteToken[blockNumber][index_string]
            #     # RatesGraph_Dict__Process_QuoteToken[blockNumber][index_string].clear()
            # # Clear all keys in the dict
            # RatesGraph_Dict__Process_QuoteToken[blockNumber].clear()
            # # Clear the key out of the dict
            # del RatesGraph_Dict__Process_QuoteToken[blockNumber]
            #
            # for index_string in RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber]:
            #     for subList in RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber][index_string]:
            #         # Clear the contents of the list
            #         del subList[:]
            #         del subList
            #         # subList.clear()  # Clear might not work anymore....
            #     # Clear the contents of the list
            #     del RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber][index_string][:]
            #     # del RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber][index_string]
            #     # RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber][index_string].clear()
            # # Clear all keys in the dict
            # RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber].clear()
            # # Clear the key out of the dict
            # del RatesGraph_Dict_ExchangeNames__Process_QuoteToken[blockNumber]
            #
            # for index_string in RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber]:
            #     for subList in RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber][index_string]:
            #         # Clear the contents of the list
            #         del subList[:]
            #         del subList
            #         # subList.clear()  # Clear might not work anymore....
            #     # Clear the contents of the list
            #     del RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber][index_string][:]
            #     # del RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber][index_string]
            #     # RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber][index_string].clear()
            # # Clear all keys in the dict
            # RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber].clear()
            # # Clear the key out of the dict
            # del RatesGraph_Dict_QuoteTokens__Process_QuoteToken[blockNumber]

            duration_s = (datetime.datetime.now() - before).total_seconds()
            PrintAndLog_FuncNameHeader("Finished Removing RatesGraph_Dict__Process_QuoteToken for blockNumber = " + str(
                blockNumber) + ", duration_s = " + str(duration_s) + " seconds")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when trying to clean up an old RatesGraph_Dict__Process_QuoteToken: " + traceback.format_exc())


def Set_HighestInitializedRatesGraphBlockNumber_BasedOnLatestBlockNumber():
    global HighestInitializedRatesGraphBlockNumber

    # Set as one block older since we want it to start the this block number
    HighestInitializedRatesGraphBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() - 1


def Set_HighestInitializedRatesGraphBlockNumber_BasedOnLatestBlockNumber__Process_QuoteToken(blockNumber):
    global HighestInitializedRatesGraphBlockNumber__Process_QuoteToken

    # Set as one block older since we want it to start the this block number
    HighestInitializedRatesGraphBlockNumber__Process_QuoteToken = blockNumber - 1


def InitializeRatesGraphsForNearFutureBlocks():
    global HighestInitializedRatesGraphBlockNumber

    before = datetime.datetime.now()
    # If we set this to 10, we should always be 9/10 ahead which should prevent this from bering processed during a high priority processing/trading time
    numOfFutureBlocksToInit = Libraries.utils.GetCurrent_NumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks()
    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    # PrintAndLog_FuncNameHeader("Begin with latestBlockNumber = " + str(latestBlockNumber) + ", numOfFutureBlocksToInit = " + str(numOfFutureBlocksToInit))
    for blockNumber in range(HighestInitializedRatesGraphBlockNumber, latestBlockNumber + numOfFutureBlocksToInit):
        PrintAndLog_FuncNameHeader("Calling on block " + str(blockNumber) + " with main process = " + str(os.getpid()))
        # Initialize RatesGraphs for this blockNumber
        InitializeRatesGraphForBlock(blockNumber)

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Completed with duration_s = " + str(duration_s) + " seconds. numOfFutureBlocksToInit = " + str(numOfFutureBlocksToInit))


def InitializeRatesGraphsForNearFutureBlocks__Process_QuoteToken(latestBlockNumber):
    global HighestInitializedRatesGraphBlockNumber__Process_QuoteToken

    before = datetime.datetime.now()
    # If we set this to ~10 or more, we should always be 9/10 ahead which should prevent this from being processed during a high priority processing/trading time
    numOfFutureBlocksToInit = Libraries.utils.GetCurrent_NumOfFutureBlocksWorthOfRatesGraphsToInitialize_blocks()
    # PrintAndLog_FuncNameHeader("Begin with latestBlockNumber = " + str(latestBlockNumber) + ", numOfFutureBlocksToInit = " + str(numOfFutureBlocksToInit))
    for blockNumber in range(HighestInitializedRatesGraphBlockNumber__Process_QuoteToken, latestBlockNumber + numOfFutureBlocksToInit):
        PrintAndLog_FuncNameHeader("Calling on block " + str(blockNumber) + " with sub process os.getpid() = " + str(os.getpid()))
        # Initialize RatesGraphs for this blockNumber
        InitializeRatesGraphForBlock__Process_QuoteToken(blockNumber)

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Completed with duration_s = " + str(duration_s) + " seconds. numOfFutureBlocksToInit = " + str(numOfFutureBlocksToInit))


def UpdateNodeInRatesGraph(quoteToken, baseToken, price_bid, price_ask,
                           exchangeName_bid, exchangeName_ask, blockNumber, index_string):
    global RatesGraphTokenListIndexDict

    # debugToken = '0xc00e94cb662c3520282e6f5717214004a7f26888'
    # # debugToken = None
    # doPrintForDebug = False
    # if debugToken and (quoteToken.lower() == debugToken.lower() or baseToken.lower() == debugToken.lower()):
    #     doPrintForDebug = True

    # header_logging = "UpdateNodeInRatesGraph: pid " + str(os.getpid()) + ", quoteToken " + str(quoteToken)
    # functionsStartDateTime = datetime.datetime.now()

    if not price_bid and not price_ask:
        # if doPrintForDebug:
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "returning because prices were None")
        return

    ratesGraph_Dict, ratesGraph_Dict_ExchangeNames, ratesGraph_Dict_QuoteTokens = \
        GetRatesGraphDicts__Process_QuoteToken(blockNumber, index_string)

    # Get the index from the RatesGraphTokenListIndexDict since it's much faster than calling .index here
    indexOf_quoteToken = RatesGraphTokenListIndexDict[quoteToken.lower()]
    indexOf_baseToken = RatesGraphTokenListIndexDict[baseToken.lower()]

    # if doPrintForDebug:
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "indexOf_quoteToken = " + str(indexOf_quoteToken))
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "indexOf_baseToken = " + str(indexOf_baseToken))
    #
    # if doPrintForDebug:
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Examining nodes " + str(indexOf_quoteToken) + ", " + str(
    #         indexOf_baseToken) + " and " + str(indexOf_baseToken) + ", " + str(indexOf_quoteToken) + " for index_string = " + str(index_string))

    if price_bid:
        rate_bid = Libraries.utils.ConvertPriceToRate(price_bid, 'sell')
        # if doPrintForDebug:
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
        #                          "ratesGraph_Dict[indexOf_baseToken][indexOf_quoteToken] = " + str(ratesGraph_Dict[indexOf_baseToken][indexOf_quoteToken]))
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
        #                          "rate_bid of type " + str(type(rate_bid)) + " = " + str(rate_bid) + ", price_bid = " + str(price_bid))
        if rate_bid > ratesGraph_Dict[indexOf_baseToken][indexOf_quoteToken]:
            # if doPrintForDebug:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
            #                          "ratesGraph_Dict[indexOf_baseToken] = " + str(ratesGraph_Dict[indexOf_baseToken]))
            ratesGraph_Dict[indexOf_baseToken][indexOf_quoteToken] = rate_bid
            # ratesGraph_Dict_ExchangeNames[indexOf_baseToken][indexOf_quoteToken] = exchangeName_bid
            # ratesGraph_Dict_QuoteTokens[indexOf_baseToken][indexOf_quoteToken] = quoteToken
            # Optimization: Store the index of the exchangeName, not the exchangeName itself
            ratesGraph_Dict_ExchangeNames[indexOf_baseToken][indexOf_quoteToken] = ExchangeNamesListIndexDict[exchangeName_bid]
            # Optimization: Store the index of the token, not the token itself
            ratesGraph_Dict_QuoteTokens[indexOf_baseToken][indexOf_quoteToken] = RatesGraphTokenListIndexDict[quoteToken.lower()]
            # if doPrintForDebug:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
            #                          "Updating ask at position " + str(indexOf_baseToken) + ", " + str(
            #                              indexOf_quoteToken) + ", ratesGraph_Dict[indexOf_baseToken][indexOf_quoteToken] = " + str(
            #                              ratesGraph_Dict[indexOf_baseToken][indexOf_quoteToken]))

    if price_ask:
        rate_ask = Libraries.utils.ConvertPriceToRate(price_ask, 'buy')
        # if doPrintForDebug:
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
        #                          "ratesGraph_Dict[indexOf_quoteToken][indexOf_baseToken] = " + str(ratesGraph_Dict[indexOf_quoteToken][indexOf_baseToken]))
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
        #                          "rate_ask of type " + str(type(rate_ask)) + " = " + str(rate_ask) + ", price_ask = " + str(price_ask))
        if rate_ask > ratesGraph_Dict[indexOf_quoteToken][indexOf_baseToken]:
            # if doPrintForDebug:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
            #                          "ratesGraph_Dict[indexOf_quoteToken] = " + str(ratesGraph_Dict[indexOf_quoteToken]))
            ratesGraph_Dict[indexOf_quoteToken][indexOf_baseToken] = rate_ask
            # ratesGraph_Dict_ExchangeNames[indexOf_quoteToken][indexOf_baseToken] = exchangeName_ask
            # ratesGraph_Dict_QuoteTokens[indexOf_quoteToken][indexOf_baseToken] = quoteToken
            # Optimization: Store the index of the exchangeName, not the exchangeName itself
            ratesGraph_Dict_ExchangeNames[indexOf_quoteToken][indexOf_baseToken] = ExchangeNamesListIndexDict[exchangeName_ask]
            # Optimization: Store the index of the token, not the token itself
            ratesGraph_Dict_QuoteTokens[indexOf_quoteToken][indexOf_baseToken] = RatesGraphTokenListIndexDict[quoteToken.lower()]
            # if doPrintForDebug:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
            #                          "Updating ask at position " + str(indexOf_quoteToken) + ", " + str(
            #                              indexOf_baseToken) + ", ratesGraph_Dict[indexOf_quoteToken][indexOf_baseToken] = " + str(
            #                              ratesGraph_Dict[indexOf_quoteToken][indexOf_baseToken]))

    # if doPrintForDebug:
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict = " + str(ratesGraph_Dict))
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict_ExchangeNames = " + str(ratesGraph_Dict_ExchangeNames))
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict_QuoteTokens = " + str(ratesGraph_Dict_QuoteTokens))


def ForceUpdateNodeInRatesGraph(quoteToken, baseToken, price_bid, price_ask, exchangeName_bid, exchangeName_ask,
                                ratesGraph_Dict, ratesGraph_Dict_ExchangeNames, ratesGraph_Dict_QuoteTokens):
    global RatesGraphTokenListIndexDict

    # Get the index from the RatesGraphTokenListIndexDict since it's much faster than calling .index here
    indexOf_quoteToken = RatesGraphTokenListIndexDict[quoteToken.lower()]
    indexOf_baseToken = RatesGraphTokenListIndexDict[baseToken.lower()]

    # PrintAndLog_FuncNameHeader("price_bid = " + str(price_bid))
    if price_bid:
        rate_bid = Libraries.utils.ConvertPriceToRate(price_bid, 'sell')
        ratesGraph_Dict[indexOf_baseToken][indexOf_quoteToken] = rate_bid
        ratesGraph_Dict_ExchangeNames[indexOf_baseToken][indexOf_quoteToken] = ExchangeNamesListIndexDict[exchangeName_bid]

    # PrintAndLog_FuncNameHeader("price_ask = " + str(price_ask))
    if price_ask:
        rate_ask = Libraries.utils.ConvertPriceToRate(price_ask, 'buy')
        ratesGraph_Dict[indexOf_quoteToken][indexOf_baseToken] = rate_ask
        ratesGraph_Dict_ExchangeNames[indexOf_quoteToken][indexOf_baseToken] = ExchangeNamesListIndexDict[exchangeName_ask]
