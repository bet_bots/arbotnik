from Libraries.aesCipher import AESCipher
import Libraries.defaults

P4s5wo0dKey = None
P4s5wo0dG4rby = None


def LoadSecurityCredentialsIntoMem(skipCheck_override_MustSuccessfullyLoadSecurityCredentialsOnStartUp=False):
    global P4s5wo0dKey
    global P4s5wo0dG4rby

    # Load the security credentials into memory from Directory_PKs (the location that's not committed to the repo)
    # NOTE: DO NOT USE PrintAndLog here, we do not want keys, private keys, or encrypted content getting logged in log files

    try:
        f = open(Libraries.defaults.Directory_PKs + "/P4s5wo0dKey", "r")
        P4s5wo0dKey = f.readline()
        f.close()
    except:
        message = 'failed to load P4s5wo0dKey credentials'
        print(message)
        if not skipCheck_override_MustSuccessfullyLoadSecurityCredentialsOnStartUp and Libraries.defaults.MustSuccessfullyLoadSecurityCredentialsOnStartUp:
            raise Exception(message)

    try:
        f = open(Libraries.defaults.Directory_PKs + "/P4s5wo0dG4rby", "r")
        P4s5wo0dG4rby = f.readline()
        f.close()
    except:
        message = 'failed to load P4s5wo0dG4rby credentials'
        print(message)
        if not skipCheck_override_MustSuccessfullyLoadSecurityCredentialsOnStartUp and Libraries.defaults.MustSuccessfullyLoadSecurityCredentialsOnStartUp:
            raise Exception(message)


def EncryptContentWithKey(content):
    global P4s5wo0dKey

    # NOTE: DO NOT USE PrintAndLog here, we do not want keys, private keys, or encrypted content getting logged in log files
    # print("EncryptContent P4s5wo0dKey = ", P4s5wo0dKey)
    aesCipher = AESCipher(P4s5wo0dKey)
    encryptedContent = aesCipher.encrypt(content)
    # print("EncryptContent encryptedContent = ", encryptedContent)


def GetP4s5wo0dFromMem():
    global P4s5wo0dKey
    global P4s5wo0dG4rby

    # NOTE: DO NOT USE PrintAndLog here, we do not want keys, private keys, or encrypted content getting logged in log files
    aesCipher = AESCipher(P4s5wo0dKey)
    returnValue = aesCipher.decrypt(P4s5wo0dG4rby)
    # print("GetP4s5wo0dFromMem returnValue = ", returnValue)
    return returnValue
