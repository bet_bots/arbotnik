import redis
import jsonpickle

Host = 'localhost'
Port = 6379
RedisDB = redis.StrictRedis(host=Host, port=Port, db=0)

Delimiter_key = "_"
PrefixDelimiter_key = ":"


def GetKeyPrefix_TradeHistoryMainnet():
    return "tradehistory" + Delimiter_key + "mainnet"


def GetKeyPrefix_TradeHistoryUnitTest():
    return "tradehistory" + Delimiter_key + "unittest"


def GetValueInRedis(key, useJsonPickle=False):
    global RedisDB

    returnValue_bytes = RedisDB.get(key)
    if returnValue_bytes:
        returnValue = DecodeBytesToString(returnValue_bytes)

        if useJsonPickle:
            # Decode it
            returnValue = jsonpickle.decode(returnValue)
            # PrintAndLog("StoreValueInRedis: decoded returnValue and it's now = " + str(returnValue))

        # PrintAndLog("GetValueInRedis: get value for key '" + str(key) + "' = " + str(returnValue))
        return returnValue
    else:
        # PrintAndLog("GetValueInRedis: get value for key '" + str(key) + "' = None")
        return None


def StoreValueInRedis(key, value, useJsonPickle=False):
    global RedisDB

    if useJsonPickle:
        # Encode it
        value = jsonpickle.encode(value)
        # PrintAndLog("StoreValueInRedis: encoded value and it's now = " + str(value))

    RedisDB.set(key, value)
    returnValue = DecodeBytesToString(RedisDB.get(key))
    # PrintAndLog("StoreValueInRedis: stored value = " + str(returnValue))
    return returnValue


def DecodeBytesToString(bytesValue):
    return bytesValue.decode("utf-8")


def DeleteAllKeys():
    global RedisDB
    keyList = list(RedisDB.scan_iter("*"))
    for key in keyList:
        RedisDB.delete(key)
