import json
import traceback
from random import randint
from enum import Enum

import jsonpickle
import websocket
import time
import threading
import copy

import Libraries.core

from Libraries.loggingConfig import PrintAndLog_Websockets, PrintAndLogError, PrintAndLog, PrintAndLog_Blocks
from Libraries.executeOnInterval import IsTimeToExecute

WebsocketClientDict = {}

LatestBlockNumberDict = {}

NodeType = "Geth"


class SubscriptionType(Enum):
    NewHead = "NewHead"


def ConnectWebSocketClient(remoteNode):
    global WebsocketClientDict
    WebsocketClientDict[remoteNode.value] = WebsocketClientObject(remoteNode)


class WebsocketClientObject(object):
    ws = None
    nodeName = None
    url_ws = None
    # When we subscribe to a feature, we populate this dict with the id used in the subscribe payload
    # When we receive a message via websockets we'll use that id to match with the subscriptionId
    subscriptionPayloadIdDict = None
    # We need a dictionary to associate the subscriptionType with the subscriptionPayloadId
    subscriptionIdDict = None

    def __init__(self, remoteNode):
        self.nodeName = remoteNode.name
        self.url_ws = remoteNode.value
        self.subscriptionPayloadIdDict = {}
        self.subscriptionIdDict = {}

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        websocket.enableTrace(False)
        websocket.http_proxy_host = self.url_ws
        self.ws = websocket.WebSocketApp(self.url_ws,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)

        while True:
            try:
                # Subscribe to events
                IsTimeToExecute("SubscribeWebsockets_NewHeads", 0, self.SubscribeWebsockets_NewHeads, True)

                Libraries.core.API_PostOperatorNotification("Connecting to websockets: " + str(self.GetUniqueNodeName()))
                self.ws.run_forever(ping_interval=30, ping_timeout=10)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in websockets, " + str(self.GetUniqueNodeName()) + " = " + traceback.format_exc())
                pass

            PrintAndLog("Websockets disconnected! Sleeping a bit then trying again. " + str(self.GetUniqueNodeName()))
            time.sleep(30)

    def on_message(self, ws, message):
        global LatestBlockNumberDict

        try:
            jData = json.loads(message)
            # PrintAndLog(str(self.GetUniqueNodeName()) + ": Websockets: jData = " + str(jData))
            # PrintAndLog_Websockets(str(self.GetUniqueNodeName()) + ": Websockets: jData = " + str(jData))

            if 'id' in jData:
                # Assume this is a subscription response
                # {'jsonrpc': '2.0', 'id': 85050627463811, 'result': '0xc624794b6a328d430ab7160e3c2ec38'}

                for subscriptionType in SubscriptionType:
                    if jData['id'] == self.subscriptionPayloadIdDict[subscriptionType.value]:
                        # Associate the subscriptionType with the subscriptionPayloadId
                        self.subscriptionIdDict[jData['result']] = subscriptionType.value

                # PrintAndLog("self.subscriptionIdDict = " + str(self.subscriptionIdDict))

            elif 'method' in jData and jData['method'] == 'eth_subscription':
                params = jData['params']
                subscriptionId = params['subscription']
                subscriptionTypeValue = self.subscriptionIdDict[subscriptionId]
                content = params['result']
                blockNumber = Libraries.core.ConvertHexToInt(content['number'])
                blockTimestamp = Libraries.core.ConvertHexToInt(content['timestamp'])
                PrintAndLog_Blocks(str(self.nodeName) + " found " + str(subscriptionTypeValue) + " with blockNumber = " + str(
                    blockNumber) + ", blockTimestamp = " + str(blockTimestamp) + ", " + str(time.time()))

                LatestBlockNumberDict[self.nodeName] = blockNumber
                # PrintAndLog("Calling ResultHandlerDelegate_DetermineHighestResult_int_LatestBlockNumberOnly_Generic with "
                #             "LatestBlockNumberDict = " + str(LatestBlockNumberDict))

                Libraries.core.ResultHandlerDelegate_DetermineHighestResult_int_LatestBlockNumberOnly_Generic(
                    copy.deepcopy(LatestBlockNumberDict), None, True)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when parsing message in WebsocketClientObject: " + traceback.format_exc() + ", message = " + str(message))
            pass

    def on_error(self, ws, error):
        PrintAndLogError(str(error))

    def on_close(self, ws):
        message = str(self.GetUniqueNodeName()) + ": Disconnected from websockets."
        PrintAndLogError(message)
        Libraries.core.API_PostOperatorNotification(message)

    def GetUniqueNodeName(self):
        return NodeType + " " + self.url_ws

    def SubscribeWebsockets_NewHeads(self, doSleepFirst=True):
        # Give websockets time to connect
        if doSleepFirst:
            time.sleep(4)

        subscriptionPayloadId = randint(0, 99999999999999)
        # Associate the subscriptionPayloadId with the SubscriptionTypes
        self.subscriptionPayloadIdDict[SubscriptionType.NewHead.value] = subscriptionPayloadId
        payload = {
            "id": subscriptionPayloadId,
            "method": "eth_subscribe",
            "params": ["newHeads"],
        }
        self.SubscribeWebsocketsToEndpoint(payload)

    def SubscribeWebsocketsToEndpoint(self, subscriptionPayload):
        PrintAndLog_Websockets("Subscribing to subscriptionPayload = " + str(subscriptionPayload))
        self.ws.send(jsonpickle.encode(subscriptionPayload))
