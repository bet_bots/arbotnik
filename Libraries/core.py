import copy
import datetime
import json
import math
import os
import random
import string
import threading
import time
import traceback
from decimal import *
from enum import Enum
from random import randint, uniform
from threading import Lock
import requests
import rlp
from ethereum import transactions
from web3 import Web3

from Libraries.executeOnInterval import IsTimeToExecute
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader, PrintAndLog_Header
from Libraries.nonceUtils import GetHighestKnownNonceUsedAndConfirmed
import Libraries.defaults

UseOnlyLatest_NotBlockNumber = False

SecondsInADay = 60 * 60 * 24

RequestExceptionString_ConnectTimeout = "requests.exceptions.connecttimeout"
RequestExceptionString_ReadTimeout = "requests.exceptions.readtimeout"
RequestExceptionString_ConnectionError = "requests.exceptions.connectionerror"

# Dict of datetimes of last time the Sentinel validated this node for responsiveness and sync.
# Keyed by remoteNode
SentinelDict = {}
# Dict of last block numbers for each remote node.  This way I can see if they are stuck or syncing.  These numbers should always increase with time
# Keyed by remoteNode
SentinelLastBlockNumberDict = {}

DateTimeOfLast_SentinelDictParse = None
# Interval_hours_SentinelDictParse = 1
# Interval_seconds_SentinelDictParse = Interval_hours_SentinelDictParse * 60 * 60
Interval_seconds_SentinelDictParse = 5
DateTimeOfLast_SentinelDictEnforcingBadNodes = None
Interval_seconds_SentinelDictEnforcingBadNodes = 30
DateTimeOfLast_DidNotFindValidResponseOutOfAllNodes = None
Interval_seconds_DidNotFindValidResponseOutOfAllNodes = 10000
Interval_minutes_DidNotFindValidResponseOutOfAllNodes = round(float(Interval_seconds_DidNotFindValidResponseOutOfAllNodes / 60), 1)
OccurrenceCount_DidNotFindValidResponseOutOfAllNodes = 0

# Interval_seconds_SentinelLoopSleepTime = 50
Interval_seconds_SentinelLoopSleepTime = 1

URL_EtherScan = "https://api.etherscan.io/api"
# the real app uses the cache endpoint, but there is also an api endpoint
# URL_EtherDelta_Cache = "https://cache.etherdelta.com"
# URL_EtherDelta = "https://api.etherdelta.com"
URL_ForkDelta = "https://api.forkdelta.com"

Headers = {'content-type': 'application/json'}

ApiKey_EtherScan_HighPriority = "RAEW4IPNPZU2N21A65YTU473D4TWVI6QDC"
ApiKey_EtherScan_LowPriority = "MKSFSDAKRYDDKFJGRNMZ1YHSKSINJFQSNN"

# This is the event ID for the EtherDelta contract's trade function. It's logging info that I need to determine if my orders have been filled
EventId_EtherDeltaTradeHash = "0x6effdda786735d5033bfad5f53e5131abcced9e52be6c507b62d639685fbed6d"
EventId_EtherDeltasOrderHash = "0x3f7f2eda73683c21a15f9435af1028c93185b5f1fa38270762dc32be606b3e85"
EventId_callAvailableVolume_eventLogsUint_localClass = "0x0d0585b6d9267e43861ac4ed35b34a20d743067469c003bf097a40d4ccba68f3"

OrderCanceledBy_OceanAPI = "Successfully canceled by Ocean API"
OrderCanceledBy_ErcDex = "ErcDex canceled this order"
OrderNotFoundOn_Ocean = "Order not found on Ocean, assume canceled or filled"

TransactionErrors_InsufficientFundsForGasPrice = "insufficient funds for gas * price + value"
TransactionErrors_NonceTooLow = "nonce too low"
TransactionErrors_NonceIsTooLow = "nonce is too low"

Ether_Decimals = 1000000000000000000

LengthOfDataProperty = 64
LengthOfDataProperty_Including0x = 66
LengthOfPrivateKey = 64
LengthOfPublicAddress_Including0x = 42
LengthOfPublicAddress_Excludes0x = 40
LengthOfTransactionHash_Including0x = 66
LengthOfTransactionHash_Excludes0x = 64
LengthOfFunctionHash_Including0x = 10
LengthOfFunctionHash_Excluding0x = 8

RequestTimeout_broadcastTx_seconds = 0.7  # Use this for production
RequestTimeout_wickedShort_seconds = 0.9  # Use this for production
RequestTimeout_short_seconds = 2  # Use this for production
# RequestTimeout_wickedShort_seconds = 6  # Use this for debug when node is in Europe
# RequestTimeout_short_seconds = 6  # Use this for debug when node is in Europe

RequestTimeout_seconds = 5
RequestTimeout_long_seconds = 17
RequestTimeout_veryLong_seconds = 40
RequestTimeout_wickedLong_seconds = 75
RequestTimeout_NotificationsToOperator_seconds = 2

# Etherscan's API is slow, can take a long time especially when you have a large block range you're searching over
RequestTimeout_TradeHistoryEtherScan_seconds = 25
ItemsPerPage_GetPendingTransactionsInMemPool_EtherScan = 100

AverageBlockTime_seconds = 13

IsCloseDecimalPrecision = 8

Lock_LatestBlockNumber_int = Lock()
LatestBlockNumber_int = None
LatestUnixEpochTime_int = None
# This keeps track of the unix epoch time of when I discover each block
# Block number keyed by time discovered in unix epoch time
BlockDiscoveryTimeStampDict = {}

# Dict of all latest block numbers for all nodes keyed by node
Lock_LatestBlockNumberDict_int = Lock()
LatestBlockNumberDict_int = {}

SleepTime_WaitingForWebsocketsToConnect_seconds = 1.0

# This gives me the option to query the nodes for future blocks instead of just current block.
# This way I have the ability to check a block or two in the future in case there's a chance a new block was quickly mined in
# NumOfFutureBlocksToQuery = 0

DataF = "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
DataF_Hex = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
DataF_Int = 115792089237316195423570985008687907853269984665640564039457584007913129639935
# Older tokens would ideally approve uint256.maxValue, but newer tokens are having you approve uint96.maxValue which is 79228162514264337593543950335
# So I'm including the value for uint96 here as well
DataF_Int_uint96 = 79228162514264337593543950335
Data0 = "0000000000000000000000000000000000000000000000000000000000000000"

FunctionHash_Approve = "0x095ea7b3"
FunctionHash_TokenTransfer = "0xa9059cbb"

FunctionTopic_ERC20_Transfer = "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef"
FunctionTopic_ZRX_FillOrder = "0x0d0b9391970d9a25552f37d436d2aae2925e2bfe1b2a923754bada030c498cb3"
FunctionTopic_ZRXv2_FillOrder = "0x0bcc4c97732e47d9946f229edb95f5b6323f601300e4690de719993f3c371129"
FunctionTopic_ZRXv3_FillOrder = "0x6869791f0a34781b29882982cc39e882768cf2c96995c2a110c577c53bc932d5"

DateTimeFormat = '%Y-%m-%d %H:%M:%S'
DateTimeFormat_includingMilliseconds = '%Y-%m-%d %H:%M:%S.%f'
DateTimeFormat_TaxForm = '%m-%d-%Y'

# This is how much gas is used when calling a smart contract function with no code in it.
# Modifying this could have consequences and break things!  check it's usage before you modify it
GasUsedWhenExecutingEmptyFunctionWithNothingInIt = 21483

EthereumBlockGasLimit = 10000000

FunctionHashExample = '0x9b44d556'

DoSuppressPrintAndLog_SendRequestToSpecifiedNodes = False


class TradingRole(Enum):
    none = "none"
    maker = "maker"
    taker = "taker"


class Units(Enum):
    Ether = "ether"
    # TODO, rename Wei units to Base units everywhere in the entire project
    Wei = "wei"


class BlockScanningStrategy(Enum):
    SubtractDiff = "SubtractDiff"
    OneAtATime = "OneAtATime"


class OrderPrepDetails:
    exchangeDX = None
    makerTokenAddress = None
    takerTokenAddress = None
    makerTokenAmount_etherUnits = None
    takerTokenAmount_etherUnits = None

    orderType = None
    price = None
    quoteTokenAddress = None
    baseTokenAddress = None
    makerTokenDecimals = None
    takerTokenDecimals = None
    quoteTokenDecimals = None
    baseTokenDecimals = None
    quoteTokenAmount_etherUnits = None
    baseTokenAmount_etherUnits = None
    makerTokenAmount_weiUnits = None
    takerTokenAmount_weiUnits = None
    quoteTokenAmount_weiUnits = None
    baseTokenAmount_weiUnits = None

    orderType_forExchangeDX = None
    quoteTokenSymbol = None
    baseTokenSymbol = None

    def __init__(self, _exchangeDX, _makerTokenAddress, _takerTokenAddress, _makerTokenAmount_etherUnits, _takerTokenAmount_etherUnits, _orderType):
        self.exchangeDX = _exchangeDX
        self.makerTokenAddress = _makerTokenAddress
        self.takerTokenAddress = _takerTokenAddress
        self.makerTokenAmount_etherUnits = _makerTokenAmount_etherUnits
        self.takerTokenAmount_etherUnits = _takerTokenAmount_etherUnits
        self.orderType = _orderType

        self.DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType()

    def DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType(self):
        if IsBuy(self.orderType):
            self.price = self.makerTokenAmount_etherUnits / self.takerTokenAmount_etherUnits
            self.quoteTokenAddress = self.makerTokenAddress
            self.baseTokenAddress = self.takerTokenAddress
            self.quoteTokenAmount_etherUnits = self.makerTokenAmount_etherUnits
            self.baseTokenAmount_etherUnits = self.takerTokenAmount_etherUnits
        elif IsSell(self.orderType):
            self.price = self.takerTokenAmount_etherUnits / self.makerTokenAmount_etherUnits
            self.quoteTokenAddress = self.takerTokenAddress
            self.baseTokenAddress = self.makerTokenAddress
            self.quoteTokenAmount_etherUnits = self.takerTokenAmount_etherUnits
            self.baseTokenAmount_etherUnits = self.makerTokenAmount_etherUnits

        if self.exchangeDX.DoIFlipBaseAndQuote():
            # Switch the symbols
            self.quoteTokenSymbol = self.exchangeDX.market.GetSecondaryTokenName()
            self.baseTokenSymbol = self.exchangeDX.market.GetPrimaryTokenName()
            # Invert the orderType
            self.orderType_forExchangeDX = InvertOrderType(self.orderType)
            # Switch the base/quote amounts
            old_quoteTokenAmount_etherUnits = self.quoteTokenAmount_etherUnits
            old_baseTokenAmount_etherUnits = self.baseTokenAmount_etherUnits
            self.quoteTokenAmount_etherUnits = old_baseTokenAmount_etherUnits
            self.baseTokenAmount_etherUnits = old_quoteTokenAmount_etherUnits
            # Switch the base/quote addresses
            old_quoteTokenAddress = self.quoteTokenAddress
            old_baseTokenAddress = self.baseTokenAddress
            self.quoteTokenAddress = old_baseTokenAddress
            self.baseTokenAddress = old_quoteTokenAddress

            price_old = self.price
            self.price = 1 / self.price
            PrintAndLog("DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType " + str(self.exchangeDX.exchangeName) + ", flipped price from " + str(
                price_old) + " to " + str(self.price) + " because self.DoIFlipBaseAndQuote() was True")
            PrintAndLog("DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType " + str(self.exchangeDX.exchangeName) + ", flipped orderType from " + str(
                self.orderType) + " to " + str(self.orderType_forExchangeDX) + " because self.DoIFlipBaseAndQuote() was True")
            PrintAndLog("DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType " + str(
                self.exchangeDX.exchangeName) + ", switched quoteTokenAmount_etherUnits and baseTokenAmount_etherUnits because self.DoIFlipBaseAndQuote() was True")
        else:
            self.quoteTokenSymbol = self.exchangeDX.market.GetPrimaryTokenName()
            self.baseTokenSymbol = self.exchangeDX.market.GetSecondaryTokenName()
            self.orderType_forExchangeDX = self.orderType
            PrintAndLog("DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType " + str(
                self.exchangeDX.exchangeName) + ", leaving price alone because self.DoIFlipBaseAndQuote() was False")
            PrintAndLog("DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType " + str(
                self.exchangeDX.exchangeName) + ", leaving orderType alone because self.DoIFlipBaseAndQuote() was False")
            PrintAndLog("DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType " + str(
                self.exchangeDX.exchangeName) + ", leaving quoteTokenAmount_etherUnits and baseTokenAmount_etherUnits alone because self.DoIFlipBaseAndQuote() was False")

        self.makerTokenDecimals = float("1e" + str(GetDecimalsForTokenContract(self.makerTokenAddress)))
        self.takerTokenDecimals = float("1e" + str(GetDecimalsForTokenContract(self.takerTokenAddress)))
        self.quoteTokenDecimals = float("1e" + str(GetDecimalsForTokenContract(self.quoteTokenAddress)))
        self.baseTokenDecimals = float("1e" + str(GetDecimalsForTokenContract(self.baseTokenAddress)))
        self.makerTokenAmount_weiUnits = ConvertEtherAmountToBaseAmount(self.makerTokenAmount_etherUnits, self.makerTokenDecimals)
        self.takerTokenAmount_weiUnits = ConvertEtherAmountToBaseAmount(self.takerTokenAmount_etherUnits, self.takerTokenDecimals)
        self.quoteTokenAmount_weiUnits = ConvertEtherAmountToBaseAmount(self.quoteTokenAmount_etherUnits, self.quoteTokenDecimals)
        self.baseTokenAmount_weiUnits = ConvertEtherAmountToBaseAmount(self.baseTokenAmount_etherUnits, self.baseTokenDecimals)

        PrintAndLog("DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType quoteTokenAddress = " + str(self.quoteTokenAddress))
        PrintAndLog("DeterminePriceQuoteAndBase_GivenMakerStuffTakerStuffAndOrderType baseTokenAddress = " + str(self.baseTokenAddress))


class OperatorNotificationKey(Enum):
    ArbyCallTelegram = "ArbyCallTelegram"
    ArbyCallUsPhone = "ArbyCallUsPhone"
    Arby = "Arby"


def ConvertHexStringToDataByteArray(data_hex):
    # Remove the 0x from the beginning
    data_hex_with0xRemoved = data_hex.replace("0x", "")
    # PrintAndLog("data_hex_with0xRemoved:", data_hex_with0xRemoved)
    dataByteArray = bytearray.fromhex(data_hex_with0xRemoved)
    # PrintAndLog("dataByteArray = " + dataByteArray)
    return dataByteArray


def IsClose(a, b, significantDigits=None):
    global IsCloseDecimalPrecision
    # PrintAndLog("a = " + str(a) + ", type = " + str(type(a)))
    # PrintAndLog("b = " + str(b) + ", type = " + str(type(b)))
    # PrintAndLog("significantDigits = " + str(significantDigits))

    if not significantDigits:
        significantDigits = IsCloseDecimalPrecision
    # PrintAndLog("significantDigits to use = " + str(significantDigits))

    yo = abs(a - b)
    rotated = yo * float("1e" + str(significantDigits))
    # PrintAndLog("abs = " + str(yo) + ", rotated = " + str(rotated))
    if rotated > 1:
        # PrintAndLog("IsClose Returning False")
        return False
    else:
        # PrintAndLog("IsClose Returning True")
        return True


def CreateDirectoryIfDoesNotAlreadyExist(directoryPath):
    if not os.path.exists(directoryPath):
        os.makedirs(directoryPath)


# Referenced from: https://stackoverflow.com/questions/41383787/round-down-to-2-decimal-in-python
# import math
# a = 28.266
# print((math.floor(a * 100)) / 100.0)
# Output:
# 28.26
def RoundDownWithDecimalPlaces(num, decimalPlaces):
    thingy = float("1e" + str(decimalPlaces))
    # PrintAndLog("thingy = " + str(thingy))
    return math.floor(float(num) * thingy) / float(thingy)


def RoundUpWithDecimalPlaces(num, decimalPlaces):
    thingy = float("1e" + str(decimalPlaces))
    return math.ceil(float(num) * thingy) / float(thingy)


def ForceAllValuesInDictLowercase(dict):
    for k, v in dict.items():
        dict[k] = v.lower()


def EnforceMinMax(num, minimum=1, maximum=255):
    return max(min(num, maximum), minimum)


def SplitListIntoChunks_OfSizeNoGreaterThanThis(list, maxChunkSize):
    return [list[i * maxChunkSize:(i + 1) * maxChunkSize] for i in range((len(list) + maxChunkSize - 1) // maxChunkSize)]


def SplitListIntoChunks_ExactlyThisManyChunks(list, numOfChunks):
    returnListOfLists = []
    # PrintAndLog("Splitting list of length " + str(len(list)) + " into " + str(numOfChunks) + " chunks")
    itemsPerList_wholeNumber = int(len(list) / numOfChunks)
    itemsPerList_remainder = int(len(list) % numOfChunks)
    # PrintAndLog("itemsPerList_wholeNumber = " + str(itemsPerList_wholeNumber))
    # PrintAndLog("itemsPerList_remainder = " + str(itemsPerList_remainder))
    remaindersUsed = 0
    for i in range(numOfChunks):
        listToAppend = []
        for j in range(itemsPerList_wholeNumber):
            indexToAppend = (i * itemsPerList_wholeNumber) + j + remaindersUsed
            # PrintAndLog("Appending indexToAppend (whole number) " + str(indexToAppend) + " - " + str(list[indexToAppend]))
            listToAppend.append(list[indexToAppend])

        if itemsPerList_remainder > 0:
            remaindersUsed += 1
            itemsPerList_remainder -= 1
            indexToAppend = (i * itemsPerList_wholeNumber) + j + remaindersUsed
            listToAppend.append(list[indexToAppend])
            # PrintAndLog("Appending indexToAppend ( remainder  ) " + str(indexToAppend) + " - " + str(list[indexToAppend]))

        returnListOfLists.append(listToAppend)

    # PrintAndLog("returnListOfLists = " + str(returnListOfLists))
    return returnListOfLists


def SplitStringIntoChunks(myStringIWantToSplit, splitLength):
    splitArray = [myStringIWantToSplit[i:i + splitLength] for i in range(0, len(myStringIWantToSplit), splitLength)]
    # for thingy in splitArray:
    #     PrintAndLog("SplitStringIntoChunks: item = " + str(thingy))
    return splitArray


def GetGasCost(gasPrice_wei, gas_wei):
    estimatedGasCost_ether = gasPrice_wei * gas_wei / float(Ether_Decimals)
    return estimatedGasCost_ether


def ConvertEtherAmountToBaseAmount(etherAmount, decimals):
    # PrintAndLog("ConvertEtherAmountToBaseAmount with etherAmount = " + str(etherAmount) + " decimals = " + str(decimals))
    # Always return ether amount as a long so it has no decimal.
    return int(etherAmount * decimals)


def ConvertBaseAmountToEtherAmount(baseAmount, decimals):
    # PrintAndLog("ConvertBaseAmountToEtherAmount with baseAmount = " + str(baseAmount) + " decimals = " + str(decimals))
    # Always return base amount as a float so it has a decimal.
    return float(baseAmount) / float(decimals)


def ConvertMinutesToBlocks(minutes):
    global AverageBlockTime_seconds

    averageBlockTime_minutes = 60 / float(AverageBlockTime_seconds)
    blocks = minutes * averageBlockTime_minutes
    return int(blocks)


def ConvertBlocksToMinutes(blocks):
    global AverageBlockTime_seconds

    averageBlockTime_minutes = 60 / float(AverageBlockTime_seconds)
    minutes = float(blocks) / float(averageBlockTime_minutes)
    return minutes


def ConvertSecondsToBlocks(seconds):
    global AverageBlockTime_seconds

    blocks = seconds / AverageBlockTime_seconds
    return int(blocks)


def ConvertBlocksToSeconds(blocks):
    global AverageBlockTime_seconds

    seconds = float(blocks) * float(AverageBlockTime_seconds)
    return seconds


def ConvertUnixEpochTimeToApproximateBlockNumber_GivenEpochTimeAndValidUnixEpochTimeBlockNumberPair(epochTimeToConvert, latestUnixEpochTime_int, latestBlockNumber_int):
    # I know that it was latestUnixEpochTime_int when block latestBlockNumber_int was the latest block
    # So to get approximate block number at a given unix epoch time i'm using that as my measuring point
    # Find distance between the epochTimeToConvert and latestUnixEpochTime_int then convert that to blocks using AverageBlockTime_seconds and latestBlockNumber_int
    # PrintAndLog("epochTimeToConvert = " + str(epochTimeToConvert))
    # PrintAndLog("latestUnixEpochTime_int = " + str(latestUnixEpochTime_int))
    distance_unixEpochTime = latestUnixEpochTime_int - epochTimeToConvert
    # PrintAndLog("distance_unixEpochTime = " + str(distance_unixEpochTime))
    distance_blocks = int(round(distance_unixEpochTime / AverageBlockTime_seconds))
    # PrintAndLog("distance_blocks = " + str(distance_blocks))
    result_blocks = latestBlockNumber_int - distance_blocks
    # PrintAndLog("result_blocks = " + str(result_blocks))
    return result_blocks


def ConvertWeiToEther(wei, decimals):
    # This float is critical in the division because of how small some values can be
    return float(wei) / float(decimals)


def ConvertEtherToWei(ether, decimals, roundUsingDecimalType=False):
    # This float is critical in the division because of how small some values can be
    # But then it needs to be returned as an int
    if roundUsingDecimalType:
        # DO NOT USE THIS UNLESS YOU ABSOLUTELY NEED IT.
        # Decimal types requires a string and I do not trust math when decimals are converted to strings.
        # It's burned me before and cost me money due to bugs.
        return int(Decimal(str(ether)) * int(decimals))
    else:
        return int(round(float(ether) * float(decimals)))


def ConvertWeiToEther_GivenTokenAddress(wei, tokenAddress):
    decimals = GetDecimalsForTokenContract(tokenAddress, True)
    return ConvertWeiToEther(wei, decimals)


def ConvertEtherToWei_GivenTokenAddress(ether, tokenAddress, roundUsingDecimalType=False):
    decimals = GetDecimalsForTokenContract(tokenAddress, True)
    return ConvertEtherToWei(ether, decimals, roundUsingDecimalType)


def ConvertListOfWeisToEthers(listOfWeis, decimals):
    # This assumes they all have the same decimals
    returnList = []
    for wei in listOfWeis:
        returnList.append(ConvertWeiToEther(wei, decimals))

    return returnList


def ConvertListOfEthersToWeis(listOfEthers, decimals):
    # This assumes they all have the same decimals
    returnList = []
    for ether in listOfEthers:
        returnList.append(ConvertEtherToWei(ether, decimals))

    return returnList


def ConvertListOfQuoteTokensToBaseTokens_weiUnits(quoteTokenList_weiUnits, marketPrice, decimals_quoteToken, decimals_baseToken):
    # Convert quoteTokenList_weiUnits from quote tokens to base tokens
    baseTokenList_weiUnits = []
    for quoteTokenQuantity_weiUnits in quoteTokenList_weiUnits:
        # First convert from weiUnits to etherUnits
        quoteTokenQuantity_etherUnits = ConvertWeiToEther(quoteTokenQuantity_weiUnits, decimals_quoteToken)
        # PrintAndLog("quoteTokenQuantity_etherUnits = " + str(quoteTokenQuantity_etherUnits))
        # Then convert from ether to tokens using price
        baseTokenQuantity_etherUnits = ConvertEtherToTokens(quoteTokenQuantity_etherUnits, marketPrice)
        # PrintAndLog("baseTokenQuantity_etherUnits = " + str(baseTokenQuantity_etherUnits))
        # Then convert tokens from etherUnits to weiUnits
        baseTokenQuantity_weiUnits = ConvertEtherToWei(baseTokenQuantity_etherUnits, decimals_baseToken)
        # PrintAndLog("baseTokenQuantity_weiUnits = " + str(baseTokenQuantity_weiUnits))
        baseTokenList_weiUnits.append(baseTokenQuantity_weiUnits)

    return baseTokenList_weiUnits


def ConvertGweiToWei(gwei):
    return int(gwei * 1000000000)


def ConvertWeiToGwei(wei):
    return wei / float(1000000000)


def ConvertIntToHex(number_int):
    return "%x" % int(number_int)


def ConvertHexToInt(hex):
    return int(hex, 16)


def ConvertHexToBool(hex):
    intValue = ConvertHexToInt(hex)
    if intValue == 0:
        return False
    elif intValue == 1:
        return True
    else:
        raise Exception("Invalid data: " + str(hex))


def Remove0XfromHexString(myString):
    returnString = myString.replace("0x", "")
    return returnString


def EnforceMinAndMaxValues_GivenValue(value, min, max):
    constraint = None
    if value < min:
        constraint = "min"
        value = min

    if value > max:
        constraint = "max"
        value = max

    return value, constraint


def IsNumberWithinPercentOfAnotherNumber(num1, num2, within):
    if abs(num1 - int(num2)) <= within:
        return True
    else:
        return False


def DivideListIntoNsmallerLists(l, n):
    n = max(1, n)
    # return list((l[i:i+n] for i in xrange(0, len(l), n)))     # Python2
    return list((l[i:i + n] for i in range(0, len(l), n)))  # Python3


def MergeDictionaries(x, y):
    z = copy.deepcopy(x)  # start with x's keys and values
    z.update(y)  # modifies z with y's keys and values & returns None
    return z


def GetArbitraryItemInDictionary(dict):
    return next(iter(dict.values()))


def FindValueInDict(dict, stringToSearch):
    # PrintAndLog("FindValueInDict stringToSearch = " + str(stringToSearch))
    return stringToSearch in dict.values()


def FindOutlier(nums):
    parities = [n & 1 for n in nums]
    return nums[parities.index((parities[0] in parities[1:3]) ^ parities[0])]


def ToLowerContentsOfArray(array):
    return [x.lower() for x in array]


def IsTransactionDataPrefixedWithApproveHash(hexString):
    global FunctionHash_Approve

    isDataAnApprove = FunctionHash_Approve.lower() in str(hexString).lower()
    return isDataAnApprove


def DoesThisTransactionReceiptContain_0xFillOrderSuccess(txReceipt):
    global FunctionTopic_ERC20_Transfer
    global FunctionTopic_ZRX_FillOrder

    # PrintAndLog("DoesThisTransactionReceiptContain_0xFillOrderSuccess")
    # The event logs emitted from this transaction must have at least 2 transfers and one fillOrder
    # These events can come in any order
    if len(txReceipt['logs']) >= 1 and \
            GetEventLogsOccuranceCount_ForSpecificTopicHash(FunctionTopic_ERC20_Transfer, txReceipt) >= 2 and \
            GetEventLogsOccuranceCount_ForSpecificTopicHash(FunctionTopic_ZRX_FillOrder, txReceipt) >= 1:
        return True
    else:
        return False


def DoesThisTransactionReceiptContain_0xv2or0xv3FillOrderSuccess(txReceipt):
    global FunctionTopic_ERC20_Transfer
    global FunctionTopic_ZRXv2_FillOrder
    global FunctionTopic_ZRXv3_FillOrder

    # PrintAndLog("DoesThisTransactionReceiptContain_0xFillOrderSuccess")
    # The event logs emitted from this transaction must have at least 2 transfers and one fillOrder
    # These events can come in any order
    if len(txReceipt['logs']) >= 1 and \
            GetEventLogsOccuranceCount_ForSpecificTopicHash(FunctionTopic_ERC20_Transfer, txReceipt) >= 2 and \
            (
                    GetEventLogsOccuranceCount_ForSpecificTopicHash(FunctionTopic_ZRXv2_FillOrder, txReceipt) >= 1
                    or
                    GetEventLogsOccuranceCount_ForSpecificTopicHash(FunctionTopic_ZRXv3_FillOrder, txReceipt) >= 1
            ):
        return True
    else:
        return False


def DoesTransactionContainValidBlockNumberAndBlockHash(txReceipt):
    if 'blockNumber' not in txReceipt or 'blockHash' not in txReceipt:
        return False
    elif not txReceipt['blockNumber'] or not txReceipt['blockHash']:
        return False
    elif '0x' not in txReceipt['blockNumber'].lower() or '0x' not in txReceipt['blockHash'].lower():
        return False
    else:
        return True


def GetEventLogsOccuranceCount_ForSpecificTopicHash(topicHashToMatch, txReceipt):
    count = 0
    for log in txReceipt['logs']:
        if DoesTxReceiptContainTopicHash(topicHashToMatch, log['topics'][0]):
            count += 1

    return count


def DoesTxReceiptContainTopicHash(topicHashToMatch, topicHash):
    if topicHashToMatch.lower() == topicHash.lower():
        returnValue = True
    else:
        returnValue = False

    # PrintAndLog("DoesTxReceiptContainTopicHash returning " + str(returnValue))
    return returnValue


def GetSumOfTokensTransferredInTransaction(tx_receipt, orderType, publicAddress, tokenAddress, tokenDecimals):
    # PrintAndLog("GetSumOfTokensTransferredInTransaction: orderType = " + str(orderType) + ", publicAddress = " + str(publicAddress) + ", tokenAddress = " + str(
    #     tokenAddress) + ", tokenDecimals = " + str(tokenDecimals))
    # PrintAndLog("GetSumOfTokensTransferredInTransaction: tx_receipt = " + str(tx_receipt))
    # Find the tokens sent or received by publicAddress.  Sum them up (because there could have been multiple transfers) and then return it.
    sumOfTokensInTrade_etherUnits = 0

    if tx_receipt and 'logs' in tx_receipt:
        for log in tx_receipt['logs']:
            # PrintAndLog("---------------------------")
            # PrintAndLog("log = " + str(log))
            # PrintAndLog("Comparing topics: log['topics'][0].lower() = " + str(log['topics'][0].lower()) + " with FunctionTopic_ERC20_Transfer.lower() = " + str(FunctionTopic_ERC20_Transfer.lower()))
            # PrintAndLog("Comparing topics: log['address'].lower() = " + str(log['address'].lower()) + " with tokenAddress.lower() = " + str(tokenAddress.lower()))
            if IsBuy(orderType):
                # if len(log['topics']) >= 3:
                #     PrintAndLog("Checking for Remove0XfromHexString(publicAddress.lower()) = " + str(Remove0XfromHexString(publicAddress.lower())) + " in log['topics'][2].lower() = " + str(log['topics'][2].lower()))

                # Match the token address, and the Transfer function topic hash, and my address as the receiver of the transfer
                if log['address'].lower() == tokenAddress.lower() and \
                        log['topics'][0].lower() == FunctionTopic_ERC20_Transfer.lower() and \
                        len(log['topics']) >= 3 and \
                        Remove0XfromHexString(publicAddress.lower()) in log['topics'][2].lower():
                    actualAmountOfTokensInTrade_weiUnits = ConvertHexToInt(log['data'])
                    actualAmountOfTokensInTrade_etherUnits = ConvertWeiToEther(actualAmountOfTokensInTrade_weiUnits, tokenDecimals)
                    sumOfTokensInTrade_etherUnits += actualAmountOfTokensInTrade_etherUnits
                    PrintAndLog("GetSumOfTokensTransferredInTransaction: Found it! Buying tokens on DX actualAmountOfTokensInTrade_etherUnits = " + str(
                        actualAmountOfTokensInTrade_etherUnits))

            elif IsSell(orderType):
                # if len(log['topics']) >= 3:
                #     PrintAndLog("Checking for Remove0XfromHexString(publicAddress.lower()) = " + str(Remove0XfromHexString(publicAddress.lower())) + " in log['topics'][1].lower() = " + str(log['topics'][1].lower()))

                # Match the token address, and the Transfer function topic hash, and my address as the sender of the transfer
                if log['address'].lower() == tokenAddress.lower() and \
                        log['topics'][0].lower() == FunctionTopic_ERC20_Transfer.lower() and \
                        len(log['topics']) >= 3 and \
                        Remove0XfromHexString(publicAddress.lower()) in log['topics'][1].lower():
                    actualAmountOfTokensInTrade_weiUnits = ConvertHexToInt(log['data'])
                    actualAmountOfTokensInTrade_etherUnits = ConvertWeiToEther(actualAmountOfTokensInTrade_weiUnits, tokenDecimals)
                    sumOfTokensInTrade_etherUnits += actualAmountOfTokensInTrade_etherUnits
                    PrintAndLog("GetSumOfTokensTransferredInTransaction: Found it! Selling tokens on DX actualAmountOfTokensInTrade_etherUnits = " + str(
                        actualAmountOfTokensInTrade_etherUnits))

    PrintAndLog("GetSumOfTokensTransferredInTransaction: sumOfTokensInTrade_etherUnits = " + str(sumOfTokensInTrade_etherUnits))
    return sumOfTokensInTrade_etherUnits


def GetDataFor_Approve(amount, contractToApprove):
    data = "0x095ea7b3"
    # data += "0000000000000000000000008da0d80f5007ef1e431dd2127178d224e32c2ef4"
    data += PadDataParemeter_LeftFillPadding(Remove0XfromHexString(contractToApprove))
    # Max number
    # data += "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
    data += amount
    return data


def GetDecimalsForTokenContract(erc20TokenContractAddress, doConvertResultToUsableDecimals=False):
    # I used to assume 18 unless otherwise noted, but now i'm relying on my caching system.
    # decimals = 18

    # I have to hard code some in here because some smart contracts in the early days of ethereum were programmed incorrectly
    # For example, some people used uint8 for decimals when they shouldn't have!

    # DGD
    if erc20TokenContractAddress.lower() == "0xe0b7927c4af23765cb51314a0e0521a9645f0e2a".lower():
        decimals = 9
    # Civic
    elif erc20TokenContractAddress.lower() == "0x41e5560054824ea6b0732e656e3ad64e20e94e45".lower():
        decimals = 8
    # LRC
    elif erc20TokenContractAddress.lower() == "0xef68e7c694f40c8202821edf525de3782458639f".lower():
        decimals = 18
    # FunFair
    elif erc20TokenContractAddress.lower() == "0x419d0d8bdd9af5e606ae2232ed285aff190e711b".lower():
        decimals = 8
    # Monaco
    elif erc20TokenContractAddress.lower() == "0xb63b606ac810a52cca15e44bb630fd42d8d1d83d".lower():
        decimals = 8
    # SNGLS
    elif erc20TokenContractAddress.lower() == "0xaec2e87e0a235266d9c5adc9deb4b2e29b54d009".lower():
        decimals = 0
    # Storj
    elif erc20TokenContractAddress.lower() == "0xb64ef51c888972c908cfacf59b47c1afbc0ab8ac".lower():
        decimals = 8
    # Edgeless
    elif erc20TokenContractAddress.lower() == "0x08711d3b02c8758f2fb3ab4e80228418a7f8e39c".lower():
        decimals = 0
    # RLC
    elif erc20TokenContractAddress.lower() == "0x607F4C5BB672230e8672085532f7e901544a7375".lower():
        decimals = 9
    # TokenCard
    elif erc20TokenContractAddress.lower() == "0xaaaf91d9b90df800df4f55c205fd6989c977e73a".lower():
        decimals = 8
    # TAAS
    elif erc20TokenContractAddress.lower() == "0xe7775a6e9bcf904eb39da2b68c5efb4f9360e08c".lower():
        decimals = 6
    # AdToken
    elif erc20TokenContractAddress.lower() == "0xd0d6d6c5fe4a677d343cc433536bb717bae167dd".lower():
        decimals = 9
    # DICE
    elif erc20TokenContractAddress.lower() == "0x2e071D2966Aa7D8dECB1005885bA1977D6038A65".lower():
        decimals = 16
    # Trustcoin
    elif erc20TokenContractAddress.lower() == "0xcb94be6f13a1182e4a4b6140cb7bf2025d28e41b".lower():
        decimals = 6
    # Xaurum
    elif erc20TokenContractAddress.lower() == "0x4df812f6064def1e5e029f1ca858777cc98d2d81".lower():
        decimals = 8
    # Humaniq
    elif erc20TokenContractAddress.lower() == "0xcbcc0f036ed4788f63fc0fee32873d6a7487b908".lower():
        decimals = 8
    # TIME
    elif erc20TokenContractAddress.lower() == "0x6531f133e6deebe7f2dce5a0441aa7ef330b4e53".lower():
        decimals = 8
    # Guppy
    elif erc20TokenContractAddress.lower() == "0xf7b098298f7c69fc14610bf71d5e02c60792894c".lower():
        decimals = 3
    # BCAP
    elif erc20TokenContractAddress.lower() == "0xff3519eeeea3e76f1f699ccce5e23ee0bdda41ac".lower():
        decimals = 0
    # TNT
    elif erc20TokenContractAddress.lower() == "0x08f5a9235b08173b7569f83645d2c7fb55e8ccd8".lower():
        decimals = 8
    # MTH
    elif erc20TokenContractAddress.lower() == "0xaf4dce16da2877f8c9e00544c93b62ac40631f16".lower():
        decimals = 5
    # MTL
    elif erc20TokenContractAddress.lower() == "0xF433089366899D83a9f26A773D59ec7eCF30355e".lower():
        decimals = 8
    # XRL
    elif erc20TokenContractAddress.lower() == "0xb24754be79281553dc1adc160ddf5cd9b74361a4".lower():
        decimals = 9
    # SKIN
    elif erc20TokenContractAddress.lower() == "0x2bdc0d42996017fce214b21607a515da41a9e0c5".lower():
        decimals = 6
    # PPT
    elif erc20TokenContractAddress.lower() == "0xd4fa1460f537bb9085d22c7bccb5dd450ef28e3a".lower():
        decimals = 8
    # ADX
    elif erc20TokenContractAddress.lower() == "0x4470BB87d77b963A013DB939BE332f927f2b992e".lower():
        decimals = 4
    # SALT
    elif erc20TokenContractAddress.lower() == "0x4156d3342d5c385a87d264f90653733592000581".lower():
        decimals = 8
    # BQX
    elif erc20TokenContractAddress.lower() == "0x5af2be193a6abca9c8817001f45744777db30756".lower():
        decimals = 8
    # AST
    elif erc20TokenContractAddress.lower() == "0x27054b13b1b798b345b591a4d22e6562d47ea75a".lower():
        decimals = 4
    # AST Rinkeby
    elif erc20TokenContractAddress.lower() == "0xcc1cbd4f67cceb7c001bd4adf98451237a193ff8".lower():
        decimals = 4
    # ENG
    elif erc20TokenContractAddress.lower() == "0xf0ee6b27b759c9893ce4f094b49ad28fd15a23e4".lower():
        decimals = 8
    # POWR
    elif erc20TokenContractAddress.lower() == "0x595832f8fc6bf59c85c527fec3740a1b7a361269".lower():
        decimals = 6
    # MOD
    elif erc20TokenContractAddress.lower() == "0x957c30ab0426e0c93cd8241e2c60392d08c6ac8e".lower():
        decimals = 0
    # SUB
    elif erc20TokenContractAddress.lower() == "0x12480e24eb5bec1a9d4369cab6a80cad3c0a377a".lower():
        decimals = 2
    # DGD
    elif erc20TokenContractAddress.lower() == "0xe0b7927c4af23765cb51314a0e0521a9645f0e2a".lower():
        decimals = 9
    # POE
    elif erc20TokenContractAddress.lower() == "0x0e0989b1f9b8a38983c2ba8053269ca62ec9b195".lower():
        decimals = 8
    # TRX
    elif erc20TokenContractAddress.lower() == "0xf230b790e05390fc8295f4d3f60332c93bed42e2".lower():
        decimals = 6
    # AION
    elif erc20TokenContractAddress.lower() == "0x4CEdA7906a5Ed2179785Cd3A40A69ee8bc99C466".lower():
        decimals = 8
    # EVX
    elif erc20TokenContractAddress.lower() == "0xf3db5fa2c66b7af3eb0c0b782510816cbe4813b8".lower():
        decimals = 4
    # INS
    elif erc20TokenContractAddress.lower() == "0x5b2e4a700dfbc560061e957edec8f6eeeb74a320".lower():
        decimals = 10
    # AGI
    elif erc20TokenContractAddress.lower() == "0x8eb24319393716668d768dcec29356ae9cffe285".lower():
        decimals = 8
    # ZIL
    elif erc20TokenContractAddress.lower() == "0x05f4a42e251f2d52b8ed15e9fedaacfcef1fad27".lower():
        decimals = 12
    # GTO
    elif erc20TokenContractAddress.lower() == "0xc5bbae50781be1669306b9e001eff57a2957b09d".lower():
        decimals = 5
    # UP
    elif erc20TokenContractAddress.lower() == "0x6ba460ab75cd2c56343b3517ffeba60748654d26".lower():
        decimals = 8
    # RLC
    elif erc20TokenContractAddress.lower() == "0x607F4C5BB672230e8672085532f7e901544a7375".lower():
        decimals = 9
    # WAX
    elif erc20TokenContractAddress.lower() == "0x39bb259f66e1c59d5abef88375979b4d20d98022".lower():
        decimals = 8
    # USDC
    elif erc20TokenContractAddress.lower() == "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48".lower():
        decimals = 6
    else:
        from Libraries.cache import GetDecimalsForTokenContract_UseCachingSystem
        decimals = GetDecimalsForTokenContract_UseCachingSystem(erc20TokenContractAddress.lower())

    if doConvertResultToUsableDecimals:
        return ConvertDecimalsToUsableDecimals(decimals)
    else:
        return decimals


def ConvertDecimalsToUsableDecimals(decimals):
    return float("1e" + str(decimals))


def ConvertUsableDecimalsToDecimals(usableDecimals):
    return CountDigits(usableDecimals) - 1


def CountDigits(num):
    return math.floor(math.log10(abs(num))) + 1


def ConvertEtherToTokens(ether, price):
    tokens = float(ether) / float(price)
    return tokens


def ConvertTokensToEther(tokens, price):
    ether = float(tokens) * float(price)
    return ether


def GetPriceGivenEtherAndTokens(ether, tokens):
    price = float(ether) / float(tokens)
    return price


def ConvertQuoteTokensToBaseTokens(quoteTokenQuantity, price):
    baseTokenQuantity = float(quoteTokenQuantity) / float(price)
    return baseTokenQuantity


def ConvertBaseTokensToQuoteTokens(baseTokenQuantity, price):
    quoteTokenQuantity = float(baseTokenQuantity) * float(price)
    return quoteTokenQuantity


def ConvertQuoteTokensToBaseTokens_weiUnits(quoteTokenQuantity_weiUnits, priceForConversion, quoteToken, baseToken):
    # Convert quoteTokenQuantity_weiUnits to baseTokensToSpend_weiUnits
    # To do this properly, you have to first convert the weiUnits to etherUnits, then convert, then convert back from etherUnits to weiUnits
    quoteTokensToSpend_etherUnits = ConvertWeiToEther_GivenTokenAddress(quoteTokenQuantity_weiUnits, quoteToken)
    # PrintAndLog_FuncNameHeader("quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
    # PrintAndLog_FuncNameHeader("priceForConversion = " + str(priceForConversion))
    baseTokensToSpend_etherUnits = ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, priceForConversion)
    # PrintAndLog_FuncNameHeader("baseTokensToSpend_etherUnits = " + str(baseTokensToSpend_etherUnits))
    baseTokensToSpend_weiUnits = ConvertEtherToWei_GivenTokenAddress(baseTokensToSpend_etherUnits, baseToken)
    return baseTokensToSpend_weiUnits


def GetPriceGivenQuantities(quoteTokenQuantity, baseTokenQuantity):
    price = float(quoteTokenQuantity) / float(baseTokenQuantity)
    return price


def SafeRoundPriceGivenOrderType(price, orderType, decimalPlaces):
    if IsBuy(orderType):
        return RoundDownWithDecimalPlaces(price, decimalPlaces)
    elif IsSell(orderType):
        return RoundUpWithDecimalPlaces(price, decimalPlaces)
    else:
        raise Exception("orderType not a buy or a sell! orderType = " + str(orderType))


def IsBuy(orderType):
    if orderType.lower() == "buy":
        return True
    else:
        return False


def IsSell(orderType):
    if orderType.lower() == "sell":
        return True
    else:
        return False


def ConvertOrderTypeToPastTense(orderType):
    if orderType.lower() == "sell":
        return "sold"
    elif orderType.lower() == "buy":
        return "bought"


def AreAddressesEqual(address1, address2):
    if address1.lower() == address2.lower():
        return True
    else:
        return False


def CalculateEffectiveEtherAndTokenValues_BasedOnPrice(ether, tokens, price):
    # Compare ether and tokens and if one limits the other, lower it to the limit
    tokens_convertedToEther = tokens * price
    # PrintAndLog("comparing tokens_convertedToEther " + str(tokens_convertedToEther) + ", with ether " + str(ether))
    if tokens_convertedToEther < ether:
        # PrintAndLog("tokens_convertedToEther was lower, converting it so that we meet the new limiting factor")
        ether = tokens_convertedToEther
        # Also convert this back to tokens so we can update that one as well
        tokens = ether / price
        # PrintAndLog("ether now = " + str(ether))
        # PrintAndLog("tokens now = " + str(tokens))

    elif tokens_convertedToEther > ether:
        # PrintAndLog("tokens_convertedToEther was higher, converting it so that we meet the new limiting factor")
        # Leave ether alone
        # Also convert this back to tokens so we can update that one as well
        tokens = ether / price
        # PrintAndLog("ether now = " + str(ether))
        # PrintAndLog("tokens now = " + str(tokens))

        # else:
        # PrintAndLog("tokens_convertedToEther was equal, no conversion required")

    return ether, tokens


def GetBetterOrderPrice_GivenPercentageToBeBetter(oldPrice, orderType, percentageToBeBetter):
    if orderType.lower() == "buy":
        # For buys, a better price is a lower price
        newPrice = float(oldPrice) * (1.0 - float(percentageToBeBetter))

    elif orderType.lower() == "sell":
        # For sells, a better price is a higher price
        newPrice = float(oldPrice) * (1.0 + float(percentageToBeBetter))

    return newPrice


def Get_RemoteEthereumNodeToUse(url_RemoteNode=None):
    from Libraries.nodes import URL_RemoteNode

    if not url_RemoteNode:
        url_RemoteNode = URL_RemoteNode.infura_lowPriority

    return url_RemoteNode.value


def Get_EtherScanAPIKeyToUse(lowPriority=False):
    global ApiKey_EtherScan_HighPriority
    global ApiKey_EtherScan_LowPriority

    if lowPriority:
        return ApiKey_EtherScan_LowPriority
    else:
        return ApiKey_EtherScan_HighPriority


def API_PostGetBalance_EtherScan(address, lowPriority=False):
    # PrintAndLog("API_PostGetBalance_EtherScan address: " + address)

    url = URL_EtherScan + "?module=account&action=balance&address=" + address + "&tag=latest&apikey=" + Get_EtherScanAPIKeyToUse(lowPriority)
    response = requests.post(url, headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_PostGetBalance_EtherScan responseData = " + responseData)
        jData = json.loads(responseData)
        # PrintAndLog("jData = " + str(jData))
        # PrintAndLog("jData['result'] = " + str(jData['result']))
        # Etherscan returns the value in wei as a long, not in hex
        wei = int(jData['result'])
        # PrintAndLog("wei = " + str(wei))
        ether = ConvertWeiToEther(wei, Ether_Decimals)
        # PrintAndLog("ether = " + str(ether))
        return float(ether)

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def Locations(city, *other_cities):
    PrintAndLog("city = " + str(city))
    PrintAndLog("other_cities = " + str(other_cities))


def CallFunctionWithBuiltInRetry(functionToCall, sleepTimeIfFails_seconds, maxNumOfRetries, *args):
    # PrintAndLog("CallFunctionWithBuiltInRetry with args = " + str(args))
    tryCount = 0
    returnValue = None
    while tryCount < maxNumOfRetries:
        tryCount += 1
        try:
            # Make the function call
            returnValue = functionToCall(*args)
            # If we've made it this far, the call succeeded so let's return
            return returnValue

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            # If it has an exception then we'll sleep a while and let it try again
            PrintAndLogError("exception in CallFunctionWithBuiltInRetry for functionToCall = " + str(
                functionToCall) + " with args = " + str(args) + ": " + traceback.format_exc())
            time.sleep(sleepTimeIfFails_seconds)
            pass

    raise Exception("Call failed repeatedly in CallFunctionWithBuiltInRetry for functionToCall = " + str(
        functionToCall) + " with args = " + str(args) + ": " + traceback.format_exc())


def API_GetTransactions_EtherScan(address, startblock_int, endblock_int):
    # PrintAndLog("API_GetTransactions_EtherScan address: " + address)
    url = URL_EtherScan + "?module=account&action=txlist&address=" + str(address) + "&startblock=" + str(
        startblock_int) + "&endblock=" + str(endblock_int) + "&sort=asc&apikey=" + Get_EtherScanAPIKeyToUse()
    PrintAndLog("API_GetTransactions_EtherScan url: " + url)
    response = requests.post(url, headers=Headers, timeout=RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetInternalTransactions_EtherScan(address, startblock_int, endblock_int):
    # PrintAndLog("API_GetInternalTransactions_EtherScan address: " + address)
    url = URL_EtherScan + "?module=account&action=txlistinternal&address=" + str(address) + "&startblock=" + str(
        startblock_int) + "&endblock=" + str(endblock_int) + "&sort=asc&apikey=" + Get_EtherScanAPIKeyToUse()
    PrintAndLog("API_GetInternalTransactions_EtherScan url: " + url)
    response = requests.post(url, headers=Headers, timeout=RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetErc20TokenTransactions_EtherScan(address, startblock_int, endblock_int):
    # PrintAndLog("API_GetErc20TokenTransactions_EtherScan address: " + address)
    url = URL_EtherScan + "?module=account&action=tokentx&address=" + str(address) + "&startblock=" + str(
        startblock_int) + "&endblock=" + str(endblock_int) + "&sort=asc&apikey=" + Get_EtherScanAPIKeyToUse()
    PrintAndLog("API_GetErc20TokenTransactions_EtherScan url: " + url)
    response = requests.post(url, headers=Headers, timeout=RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetPendingTransactions_EtherScan_Batched(pages=1, itemsPerPage=ItemsPerPage_GetPendingTransactionsInMemPool_EtherScan):
    resultDict = {}
    threads = []
    for i in range(pages):
        page = i + 1
        resultKey = 'API_GetPendingTransactions_EtherScan_Batched_' + str(page)
        t = threading.Thread(target=API_GetPendingTransactions_EtherScan, args=(page, itemsPerPage, resultKey, resultDict))
        threads.append(t)
        t.start()

    # Wait for all threads to finish
    for thread in threads:
        thread.join()

    # All results should now be in resultDict
    # Combine them into one array
    return resultDict


def API_GetPendingTransactions_EtherScan(page=1, itemsPerPage=ItemsPerPage_GetPendingTransactionsInMemPool_EtherScan, resultKey=None, resultDict=None):
    PrintAndLog("API_GetPendingTransactions_EtherScan, page = " + str(page) + ", itemsPerPage = " + str(itemsPerPage))
    # Page number starts at 1
    url = 'https://etherscan.io/txsPending?ps=' + str(itemsPerPage) + '&&m=&p=' + str(page)

    if itemsPerPage != ItemsPerPage_GetPendingTransactionsInMemPool_EtherScan:
        raise Exception("Etherscan may not support itemsPerPage = " + str(itemsPerPage))

    PrintAndLog("API_GetPendingTransactions_EtherScan url = " + str(url))
    headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'}

    response = requests.get(url, headers=headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseContent = response.content
        # responseText = response.text

        # If we're using the resultKey and resultDict
        if resultKey:
            resultDict[resultKey] = responseContent

        return responseContent

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_EthCall(toAddress, fromAddress, data, value=0, timeout=RequestTimeout_short_seconds,
                resultDict=None, resultKey=None, lock_resultDict=None):
    from Libraries.utils import ConsiderSettingResultDict

    value_hex = '0x' + ConvertIntToHex(int(value))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": data,
                "to": toAddress,
                "from": fromAddress,
                "value": value_hex,
            },
        ]
    }
    # PrintAndLog_FuncNameHeader("payload = " + str(payload))
    # before = datetime.datetime.now()
    # Do not set requiredToWaitForAllNodesResponsesBeforeYouConsiderResults to True here because it makes this call take a really long time
    response = SendRequestToAllNodes(payload, Headers, timeout)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog_FuncNameHeader("eth_call succeeded in " + str(duration_s) + " seconds")
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        result = jData['result']
        # PrintAndLog_FuncNameHeader("result = " + str(result))
        ConsiderSettingResultDict(result, resultDict, resultKey, lock_resultDict)
        return result

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_ERC20Contract_GetDecimals(contractAddress):
    PrintAndLog("API_ERC20Contract_GetDecimals for contractAddress = " + str(contractAddress))
    # Remove the 0x from the tokenAddress
    contractAddress_With0xRemoved = contractAddress.replace("0x", "")

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": "0x313ce567000000000000000000000000" + contractAddress_With0xRemoved,
                "to": contractAddress
            },
        ]
    }
    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_short_seconds, None, False)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_ERC20Contract_GetDecimals responseData = " + responseData)
        jData = json.loads(responseData)
        # PrintAndLog("API_ERC20Contract_GetDecimals jData = " + str(jData))
        decimals_hex = jData['result']
        decimals_int = ConvertHexToInt(decimals_hex)
        # PrintAndLog("API_ERC20Contract_GetDecimals decimals_int = " + str(decimals_int))
        return decimals_int

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


# def API_GetTransactionReceipt_Batched(txHashList):
#     # PrintAndLog("API_GetTransactionReceipt_Batched with txHashList of len " + str(len(txHashList)) + " = " + str(txHashList))
#
#     payload_total = []
#     txHashPayloadIdDict = {}
#     payloadId = randint(0, 99999999999999)
#     for txHash in txHashList:
#         # Increment the id each time around
#         payloadId += 1
#
#         # Pair the txHash with the Id so I can know which is which in the response
#         txHashPayloadIdDict[payloadId] = txHash
#
#         payload = {
#             "id": payloadId,
#             "jsonrpc": "2.0",
#             "method": "eth_getTransactionReceipt",
#             "params": [str(txHash)]
#         }
#         payload_total.append(payload)
#
#     # PrintAndLog("payload_total = " + str(payload_total))
#
#     response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_veryLong_seconds, None, False, False, True)
#     if response.ok:
#         responseData = response.content
#         jData = json.loads(responseData)
#         # PrintAndLog("API_GetTransactionReceipt_Batched jData = " + str(jData))
#
#         # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
#         resultList = []
#         for id in txHashPayloadIdDict:
#             for batchedResult in jData:
#                 if int(batchedResult['id']) == id:
#                     id = txHashPayloadIdDict[id]
#                     result = batchedResult['result']
#                     # PrintAndLog("found Id " + str(id) + " in the response. result = " + str(result))
#                     resultList.append(result)
#                     break
#
#         if len(resultList) != len(txHashList):
#             raise Exception("Length of lists did not match, response data is bad?")
#
#         return resultList
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetTransactionReceipt_Batched response was not ok response = " + str(response))
#         response.raise_for_status()


def API_ERC20Contract_GetDecimals_Batched(contractAddressList):
    PrintAndLog("API_ERC20Contract_GetDecimals_Batched for contractAddressList of len " + str(len(contractAddressList)))
    payload_total = []
    contractAddressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for contractAddress in contractAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the contractAddress with the Id so I can know which is which in the response
        contractAddressPayloadIdDict[payloadId] = contractAddress

        # Remove the 0x from the tokenAddress
        contractAddress_With0xRemoved = contractAddress.replace("0x", "")

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": "0x313ce567000000000000000000000000" + contractAddress_With0xRemoved,
                    "to": contractAddress
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("payload_total = " + str(payload_total))

    doRejectBatchedPayloadIfAnyOneHasError = False
    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_veryLong_seconds, None, False, False, True, doRejectBatchedPayloadIfAnyOneHasError)
    # PrintAndLog("response = " + str(response))
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_ERC20Contract_GetDecimals_Batched jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in contractAddressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = contractAddressPayloadIdDict[id]
                    if 'result' in batchedResult:
                        try:
                            result = ConvertHexToInt(batchedResult['result'])
                            # Some super sketchy token contracts have massive decimal numbers
                            # If we see this, raise an exception
                            if result > Libraries.defaults.MaxAllowedTokenDecimals:
                                raise Exception("We found a token decimals that's way too high!")
                            # PrintAndLog("found Id " + str(id) + " in the response. result = " + str(result))
                            resultDict[id] = result
                            break
                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(contractAddressList):
            message = "Length of lists did not match"

            if doRejectBatchedPayloadIfAnyOneHasError:
                raise Exception(message)
            else:
                PrintAndLog(message)

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_ERC20Contract_GetDecimals_Batched response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetERC20Symbol(contractAddress):
    PrintAndLog_FuncNameHeader("contractAddress = " + str(contractAddress))
    # Remove the 0x from the tokenAddress
    contractAddress_With0xRemoved = contractAddress.replace("0x", "")

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": "0x95d89b41000000000000000000000000" + contractAddress_With0xRemoved,
                "to": contractAddress
            },
        ]
    }
    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + responseData)
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return ConvertResponseDataToReadableString(jData['result'])

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def ConvertResponseDataToReadableString(result):
    # This is complex because all ERC20 tokens don't handle this function exactly the same.
    # So this checks for the known types and handles them.
    # PrintAndLog_FuncNameHeader("result = " + str(result))

    hexData = result.replace("0x", "")
    splitArray = SplitStringIntoChunks(hexData, LengthOfDataProperty)
    # PrintAndLog_FuncNameHeader("splitArray = " + str(splitArray))
    symbol_hex = splitArray[len(splitArray) - 1]
    # PrintAndLog_FuncNameHeader("symbol_hex = " + str(symbol_hex))
    symbol_string = bytes.fromhex(symbol_hex).decode('utf-8')
    # # Strip invisible characters
    symbol_string = StripInvisibleCharactersFromString(symbol_string)
    # PrintAndLog_FuncNameHeader("symbol_string = " + str(symbol_string))
    return symbol_string


def API_GetERC20Symbol_Batched(addressList):
    PrintAndLog_FuncNameHeader("addressList = " + str(addressList))

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for address in addressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        # Remove the 0x from the tokenAddress
        contractAddress_With0xRemoved = address.replace("0x", "")

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": "0x95d89b41000000000000000000000000" + contractAddress_With0xRemoved,
                    "to": address
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    # FIXME, for some reason infura is no longer letting me send them batched requests for this call.  https://community.infura.io/t/batched-calls-for-getting-token-symbol-dont-seem-to-be-working-after-switching-to-new-endpoint/451/3
    # FIXME, To fix this, send "latest" to infura.  Parity doesn't require it for some reason
    # response = SendRequestToMyNodesOnly(payload_total, Headers, RequestTimeout_veryLong_seconds, None, False, False, True, False)
    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_veryLong_seconds, None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = addressPayloadIdDict[id]
                    # if 'result' in batchedResult and batchedResult['result'] != '0x':
                    if 'result' in batchedResult:
                        try:
                            result = ConvertResponseDataToReadableString(batchedResult['result'])
                            # PrintAndLog_FuncNameHeader("found Id " + str(id) + " in the response. result = " + str(result))
                            # I'm using a resultDict instead of a list because not all ERC20 contracts are standard and some will fail when I make this call
                            # Since i'm expecting some to fail, I will be returning a dict keyed by the address
                            # PrintAndLog_FuncNameHeader("Setting resultDict keyed by " + str(id) + ": result = " + str(result))
                            resultDict[id] = result
                            break
                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(addressList):
            PrintAndLog_FuncNameHeader("Length of resultDict did not match length of addressList.")

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


# def SendRequestToMyNodesOnly(payload, headers, requestTimeout_seconds, resultHandlerDelegate=None,
#                              doSetBlockNumber_BasedOnLatestBlockNumber_int=True,
#                              requiredToWaitForAllNodesResponsesBeforeYouConsiderResults=False,
#                              payloadIsBatched=False, doRejectBatchedPayloadIfAnyOneHasError=True, specifiedBlockNumber_int=None):
#     from Libraries.nodes import RemoteNodeList_MyParityNodesOnly
#
#     return SendRequestToSpecifiedNodes(RemoteNodeList_MyParityNodesOnly, payload, headers, requestTimeout_seconds, resultHandlerDelegate,
#                                        doSetBlockNumber_BasedOnLatestBlockNumber_int,
#                                        requiredToWaitForAllNodesResponsesBeforeYouConsiderResults,
#                                        payloadIsBatched, doRejectBatchedPayloadIfAnyOneHasError, specifiedBlockNumber_int)


def SendRequestToAllNodes(payload, headers, requestTimeout_seconds, resultHandlerDelegate=None,
                          doSetBlockNumber_BasedOnLatestBlockNumber_int=True,
                          requiredToWaitForAllNodesResponsesBeforeYouConsiderResults=False,
                          payloadIsBatched=False, doRejectBatchedPayloadIfAnyOneHasError=True, specifiedBlockNumber_int=None):
    from Libraries.nodes import RemoteNodeList

    # We can safely assume that RemoteNodeList has a list of nodes that have the latest block information
    # RemoteNodeList may have many many nodes, but if RemoteNodeList has 10 nodes there's no reason to send our request to all 10 nodes
    # so let's cap a max number of nodes we'll send to and also load balance which nodes we're sending requests to
    # use a random number to determine which nodes we select
    statIndex = randint(0, 9999)
    numOfNodes = min(len(RemoteNodeList), Libraries.defaults.MaxNumOfNodesCalledPerRequest)
    remoteNodeList_toUse = []
    for i in range(numOfNodes):
        indexToUse = statIndex + i
        # Grab an index based on i and use mod (%) to make sure I do not exceed the size of RemoteNodeList
        remoteNodeList_toUse.append(RemoteNodeList[indexToUse % len(RemoteNodeList)])

    # PrintAndLog_FuncNameHeader("remoteNodeList_toUse has " + str(len(remoteNodeList_toUse)) + " of the original " + str(
    #     len(RemoteNodeList)) + " items. remoteNodeList_toUse= " + str(remoteNodeList_toUse) + " ")

    return SendRequestToSpecifiedNodes(RemoteNodeList, payload, headers, requestTimeout_seconds, resultHandlerDelegate,
                                       doSetBlockNumber_BasedOnLatestBlockNumber_int,
                                       requiredToWaitForAllNodesResponsesBeforeYouConsiderResults,
                                       payloadIsBatched, doRejectBatchedPayloadIfAnyOneHasError, specifiedBlockNumber_int)


def SendRequestToSpecifiedNodes(remoteNodeList_toUse, payload, headers, requestTimeout_seconds, resultHandlerDelegate=None,
                                doSetBlockNumber_BasedOnLatestBlockNumber_int=True,
                                requiredToWaitForAllNodesResponsesBeforeYouConsiderResults=False,
                                payloadIsBatched=False, doRejectBatchedPayloadIfAnyOneHasError=True, specifiedBlockNumber_int=None):
    global LatestBlockNumber_int

    # PrintAndLog_FuncNameHeader("payload = " + str(payload))
    # PrintAndLog_FuncNameHeader("remoteNodeList_toUse = " + str(remoteNodeList_toUse))
    if Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        PrintAndLog_FuncNameHeader(
            "Using hard coded historic block number for debug, TimeMachine_EnabledForHistoricBlockDebug = " + str(Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug))
        specifiedBlockNumber_int = Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug

    if doSetBlockNumber_BasedOnLatestBlockNumber_int and specifiedBlockNumber_int:
        PrintAndLog_FuncNameHeader("Setting both of these is not valid. Only one can be set: doSetBlockNumber_BasedOnLatestBlockNumber_int "
                                   "and specifiedBlockNumber_int. Overriding doSetBlockNumber_BasedOnLatestBlockNumber_int to False.")
        doSetBlockNumber_BasedOnLatestBlockNumber_int = False

    numOfBatchedRequests = 0
    if payloadIsBatched:
        numOfBatchedRequests = len(payload)

    # PrintAndLog_FuncNameHeader("numOfBatchedRequests = " + str(numOfBatchedRequests))
    threads = []
    resultDict = {}
    lock_resultDict = Lock()
    blockNumberDict = {}
    numOfExpectedResults = len(remoteNodeList_toUse)
    for remoteNode in remoteNodeList_toUse:
        # Reverse the order, instead of going for 0, 1, 2, 3... Go 3, 2, 1, 0

        resultKey = str(remoteNode.name)

        # If set block number based on GetThreadSafeCopyOf_LatestBlockNumber_int
        if doSetBlockNumber_BasedOnLatestBlockNumber_int:
            # PrintAndLog_FuncNameHeader("If statement 1")
            # blockNumber_toUse = "latest"
            blockNumber_toUse = '0x' + str(ConvertIntToHex(GetThreadSafeCopyOf_LatestBlockNumber_int()))
            # Maintain a pairing between the blockNumber_toUse we're making the call with and the resultKey
            blockNumberDict[resultKey] = ConvertHexToInt(blockNumber_toUse)
        elif specifiedBlockNumber_int:
            # PrintAndLog_FuncNameHeader("If statement 2")
            blockNumber_toUse = '0x' + str(ConvertIntToHex(specifiedBlockNumber_int))
            blockNumberDict[resultKey] = ConvertHexToInt(blockNumber_toUse)
        else:
            # PrintAndLog_FuncNameHeader("If statement 3")
            blockNumber_toUse = None

        # Execute the call with this blockNumber_toUse
        # PrintAndLog_FuncNameHeader("node = " + str(remoteNode.name) + ", payload = " + str(payload))
        t = threading.Thread(target=SubmitRequest, args=(Get_RemoteEthereumNodeToUse(remoteNode), payload, payloadIsBatched, headers, requestTimeout_seconds,
                                                         blockNumber_toUse, resultDict, resultKey, lock_resultDict,))
        t.start()
        threads.append(t)

    # TODO, Instead of doing a .join and waiting for all to finish.
    # Some may timeout, so I want to proceed as soon as at least one valid response comes in.
    # Don't wait for many valid responses or else i'll lag everything
    # for thread in threads:
    #     thread.join()

    elapsedTime_ms = 0
    timeout_ms = requestTimeout_seconds * 1000
    # Keep this low, as long as we don't have a million print/log statements.  The more print/log statements the more it will slow everything down
    sleepTime_ms = 5
    sleepTime_s = sleepTime_ms / 1000
    foundValidRecentResponse = False
    currentResultDictLength = 0
    previousResultDictLength = 0
    while elapsedTime_ms < timeout_ms:
        # Create a copy so it's threadsafe, we need to do this because new data could come in at any moment since i'm making so many requests
        lock_resultDict.acquire()
        try:
            resultDict_copy = copy.deepcopy(resultDict)

        finally:
            lock_resultDict.release()

        currentResultDictLength = len(resultDict_copy)
        # PrintAndLog("resultDict_copy, waiting for responses. We have " + str(currentResultDictLength) + " of " + str(numOfExpectedResults) + " = " + str(resultDict_copy))

        if currentResultDictLength == numOfExpectedResults:
            # PrintAndLog("Received all responses, breaking from loop")
            break

        # Consider breaking if i've found a result that satisfies me.
        # Tricky thing is, some calls will want to wait for all results to come in before it analyzes results and some calls are willing to go with the first good looking result.
        # So check the requiredToWaitForAllNodesResponsesBeforeYouConsiderResults flag
        elif not requiredToWaitForAllNodesResponsesBeforeYouConsiderResults:
            if currentResultDictLength > 0:
                # if not resultHandlerDelegate and currentResultDictLength > 0:
                # PrintAndLog("currentResultDictLength = " + str(currentResultDictLength) + ", previousResultDictLength = " + str(previousResultDictLength))
                # Only call DetermineMostRecentValidResponseFromResultDict when we have new data
                if currentResultDictLength == previousResultDictLength:
                    # PrintAndLog("Not calling DetermineMostRecentValidResponseFromResultDict because we don't have any new data since last time through the loop")
                    pass
                else:
                    # Set the previous now that we have new data
                    previousResultDictLength = currentResultDictLength
                    # PrintAndLog("Calling DetermineMostRecentValidResponseFromResultDict because we have have new data since last time through the loop")
                    # PrintAndLog("We haven't yet received all responses but let's check to see if the responses we received are valid enough")
                    # Call DetermineMostRecentValidResponseFromResultDict but do not yet raise an exception if it's not valid because we're still waiting for some API calls to return
                    if DetermineMostRecentValidResponseFromResultDict(resultDict_copy, blockNumberDict, remoteNodeList_toUse,
                                                                      doSetBlockNumber_BasedOnLatestBlockNumber_int,
                                                                      payloadIsBatched, numOfBatchedRequests, doRejectBatchedPayloadIfAnyOneHasError,
                                                                      False):
                        # PrintAndLog("We found a valid recent response! Breaking despite not yet receiving all responses")
                        foundValidRecentResponse = True
                        break

        time.sleep(sleepTime_s)
        elapsedTime_ms += sleepTime_ms

    # if not foundValidRecentResponse and currentResultDictLength != numOfExpectedResults:
    #     PrintAndLog("Timed out before we received all responses")

    # Create a copy so it's threadsafe, we need to do this because new data could come in at any moment since i'm making so many requests
    lock_resultDict.acquire()
    try:
        resultDict_copy = copy.deepcopy(resultDict)

    finally:
        lock_resultDict.release()

    firstNcharacters = 500

    # This print statement will show me which nodes got success responses and which got timeouts or known errors we're catching
    if not payloadIsBatched:
        if not DoSuppressPrintAndLog_SendRequestToSpecifiedNodes:
            PrintAndLog("SendRequestToSpecifiedNodes: resultDict for " + str(payload['method']) + " = " + str(resultDict_copy))

        # # This below loop with print statements will show me the content in each response for each node
        # for resultKey in resultDict_copy:
        #     if hasattr(resultDict_copy[resultKey], 'content'):
        #         PrintAndLog("SendRequestToSpecifiedNodes: resultDict w/ content = " + str(resultDict_copy[resultKey].content)[0:firstNcharacters] + "...")
        #     else:
        #         PrintAndLog("SendRequestToSpecifiedNodes: resultDict w/ string value = " + str(resultDict_copy[resultKey])[0:firstNcharacters] + "...")

    else:
        listOfMethods = []
        for batchedPayload in payload:
            listOfMethods.append(batchedPayload['method'])

        if not DoSuppressPrintAndLog_SendRequestToSpecifiedNodes:
            PrintAndLog("SendRequestToSpecifiedNodes: resultDict for " + str(listOfMethods) + " = " + str(resultDict_copy))

        # # This below loop with print statements will show me the content in each response for each node
        # for resultKey in resultDict_copy:
        #     if hasattr(resultDict_copy[resultKey], 'content'):
        #         PrintAndLog("SendRequestToSpecifiedNodes: resultDict w/ content = " + str(resultDict_copy[resultKey].content)[0:firstNcharacters] + "...")
        #     else:
        #         PrintAndLog("SendRequestToSpecifiedNodes: resultDict w/ string value = " + str(resultDict_copy[resultKey])[0:firstNcharacters] + "...")

    if resultHandlerDelegate:
        # PrintAndLog("SendRequestToSpecifiedNodes: calling resultHandlerDelegate")
        return resultHandlerDelegate(resultDict_copy)

    else:
        # Create a copy so it's threadsafe, we need to do this because new data could come in at any moment since i'm making so many requests
        lock_resultDict.acquire()
        try:
            resultDict_copy = copy.deepcopy(resultDict)

        finally:
            lock_resultDict.release()

        # PrintAndLog("SendRequestToSpecifiedNodes: returning DetermineMostRecentValidResponseFromResultDict")
        return DetermineMostRecentValidResponseFromResultDict(resultDict_copy, blockNumberDict, remoteNodeList_toUse,
                                                              doSetBlockNumber_BasedOnLatestBlockNumber_int,
                                                              payloadIsBatched, numOfBatchedRequests, doRejectBatchedPayloadIfAnyOneHasError)


def DetermineMostRecentValidResponseFromResultDict(resultDict, blockNumberDict, remoteNodeList,
                                                   doSetBlockNumber_BasedOnLatestBlockNumber_int,
                                                   payloadIsBatched, numOfBatchedRequests, doRejectBatchedPayloadIfAnyOneHasError,
                                                   doRaiseExceptionIfNotValid=True):
    # So here the resultDict contains a bunch of results.  Some are valid and some are not.  Of the valid ones, some are for different block numbers.
    # Find the most recent valid one
    doPrintDebug = False

    # If we have many batched responses per node call
    if payloadIsBatched:
        mostRecentValidResponse = None
        resultKeyForMostRecentValidResponse = None

        for resultKey in resultDict:
            resultString = None
            if not hasattr(resultDict[resultKey], 'content'):
                resultString = "ERROR, Could not parse response into JSON"
            else:
                responseData = resultDict[resultKey].content
                jData = json.loads(responseData)
                # Count how many times a response failed
                count_failed = 0
                # Increment the fails for each time a response didn't come in per a request
                numOfBatchedResponses = len(jData)
                count_failed += numOfBatchedRequests - numOfBatchedResponses

                for batchedJData in jData:
                    if doPrintDebug:
                        PrintAndLog("DetermineMostRecentValidResponseFromResultDict: Found result batchedJData: " + str(batchedJData))

                    if 'error' in batchedJData:
                        resultString = str(batchedJData['error'])
                        count_failed += 1

                    elif 'result' in batchedJData and batchedJData['result'] and str(batchedJData['result']).lower() != "null" and str(
                            batchedJData['result']).lower() != "0x":
                        resultString = str(batchedJData['result'])

                    else:
                        resultString = "ERROR, Could not find result in response"
                        count_failed += 1

                # Once it's all done iterating over the batched responses, if count_failed is still zero
                if count_failed == 0 or not doRejectBatchedPayloadIfAnyOneHasError:
                    # Then assume we have good data
                    mostRecentValidResponse = resultDict[resultKey]
                    resultKeyForMostRecentValidResponse = resultKey

            if doPrintDebug:
                PrintAndLog("DetermineMostRecentValidResponseFromResultDict: numOfBatchedRequests = " + str(numOfBatchedRequests) + ", numOfBatchedResponses = " + str(
                    numOfBatchedResponses) + ", count_failed = " + str(count_failed) + " after iterating over all batched responses")
                PrintAndLog("DetermineMostRecentValidResponseFromResultDict: Found result: " + str(resultString))

        if mostRecentValidResponse:
            return mostRecentValidResponse
        else:
            # I cannot return anything since this is not a valid response
            return None

    # Else we have only one response to deal with per node call
    else:
        mostRecentValidResponse = None
        highestBlockNumber = 0
        resultKeyForMostRecentValidResponse = None

        if doPrintDebug:
            PrintAndLog("DetermineMostRecentValidResponseFromResultDict: resultDict = " + str(resultDict))
            PrintAndLog("DetermineMostRecentValidResponseFromResultDict: blockNumberDict = " + str(blockNumberDict))
            PrintAndLog("DetermineMostRecentValidResponseFromResultDict: remoteNodeList = " + str(remoteNodeList))

        for resultKey in resultDict:
            resultString = None
            if not hasattr(resultDict[resultKey], 'content'):
                resultString = "ERROR, Could not parse response into JSON"
            else:
                responseData = resultDict[resultKey].content
                jData = json.loads(responseData)
                if doPrintDebug:
                    PrintAndLog("DetermineMostRecentValidResponseFromResultDict: Found result jData: " + str(jData))

                if 'error' in jData:
                    resultString = str(jData['error'])

                elif 'result' in jData and jData['result'] and str(jData['result']).lower() != "null" and str(jData['result']).lower() != "0x":
                    resultString = str(jData['result'])

                    # If we're making the call based on a specific block number then I should verify the block number before I say it's valid
                    if doSetBlockNumber_BasedOnLatestBlockNumber_int:
                        if doPrintDebug:
                            PrintAndLog("DetermineMostRecentValidResponseFromResultDict: Analyzing mostRecentValidResponse = " + str(
                                mostRecentValidResponse) + ", blockNumberDict[resultKey] = " + str(
                                blockNumberDict[resultKey]) + ", highestBlockNumber = " + str(highestBlockNumber) + ", resultKey = " + str(resultKey))
                        # Verify the block number before I assume it's valid
                        if not mostRecentValidResponse or blockNumberDict[resultKey] > highestBlockNumber:
                            mostRecentValidResponse = resultDict[resultKey]
                            highestBlockNumber = blockNumberDict[resultKey]
                            resultKeyForMostRecentValidResponse = resultKey

                    # Else we're making the call based on the 'latest' instead of a specific block number, so we don't have a blockNumberDict
                    else:
                        if doPrintDebug:
                            PrintAndLog("DetermineMostRecentValidResponseFromResultDict: Analyzing mostRecentValidResponse = " + str(
                                mostRecentValidResponse) + ", highestBlockNumber = " + str(highestBlockNumber) + ", resultKey = " + str(resultKey))
                        mostRecentValidResponse = resultDict[resultKey]
                        highestBlockNumber = None
                        resultKeyForMostRecentValidResponse = resultKey

                else:
                    resultString = "ERROR, Could not find result in response"

            if doPrintDebug:
                PrintAndLog("DetermineMostRecentValidResponseFromResultDict: Found result: " + str(resultString))

        if mostRecentValidResponse:
            # If we're making the call based on a specific block number then I should verify the block number before I say it's valid
            if doSetBlockNumber_BasedOnLatestBlockNumber_int:
                maxDictValue = max(blockNumberDict.values())
                if doPrintDebug:
                    PrintAndLog("maxDictValue = " + str(maxDictValue))

                minBlockValueAcceptableForMostRecentValidResponse = maxDictValue

                if doPrintDebug:
                    PrintAndLog("Make sure that the mostRecentValidResponse has a highestBlockNumber of greater than or equal to " + str(
                        minBlockValueAcceptableForMostRecentValidResponse))
                    if highestBlockNumber >= minBlockValueAcceptableForMostRecentValidResponse:
                        PrintAndLog("Valid")
                    else:
                        PrintAndLog("Not valid")

                if highestBlockNumber >= minBlockValueAcceptableForMostRecentValidResponse:
                    if doPrintDebug:
                        PrintAndLog("DetermineMostRecentValidResponseFromResultDict: met min requirements! highestBlockNumber = " + str(
                            highestBlockNumber) + ", mostRecentValidResponse = " + str(
                            mostRecentValidResponse) + ", resultKeyForMostRecentValidResponse = " + str(resultKeyForMostRecentValidResponse))
                    return mostRecentValidResponse

                else:
                    if doPrintDebug:
                        PrintAndLog("DetermineMostRecentValidResponseFromResultDict: this highestBlockNumber does not meet min requirement. "
                                    "Not considered valid. highestBlockNumber = " + str(
                            highestBlockNumber) + ", mostRecentValidResponse = " + str(
                            mostRecentValidResponse) + ", resultKeyForMostRecentValidResponse = " + str(resultKeyForMostRecentValidResponse))

            # Else we're making the call based on the 'latest' instead of a specific block number, so we don't have a blockNumberDict
            else:
                return mostRecentValidResponse

        else:
            if doRaiseExceptionIfNotValid:
                message = "DetermineMostRecentValidResponseFromResultDict: Did not find a valid response in SendRequestToSpecifiedNodes out of " + str(
                    len(remoteNodeList)) + " nodes"
                postedMessage = ConsiderPostingMessageSayingDidNotFindValidResponseOutOfAllNodes(message)
                if postedMessage:
                    raise Exception(message)
                else:
                    PrintAndLog(message)
                    pass

                # I cannot return anything since this is not a valid response
                return None


def ConsiderPostingMessageSayingDidNotFindValidResponseOutOfAllNodes(message):
    global DateTimeOfLast_DidNotFindValidResponseOutOfAllNodes
    global Interval_seconds_DidNotFindValidResponseOutOfAllNodes
    global Interval_minutes_DidNotFindValidResponseOutOfAllNodes
    global OccurrenceCount_DidNotFindValidResponseOutOfAllNodes

    OccurrenceCount_DidNotFindValidResponseOutOfAllNodes += 1

    if not DateTimeOfLast_DidNotFindValidResponseOutOfAllNodes or (
            (datetime.datetime.now() - DateTimeOfLast_DidNotFindValidResponseOutOfAllNodes).total_seconds() > Interval_seconds_DidNotFindValidResponseOutOfAllNodes):
        message += ", occurred " + str(OccurrenceCount_DidNotFindValidResponseOutOfAllNodes) + " times in last " + str(
            Interval_minutes_DidNotFindValidResponseOutOfAllNodes) + " minutes"
        # Reset the datetime
        DateTimeOfLast_DidNotFindValidResponseOutOfAllNodes = datetime.datetime.now()
        # Reset the counter
        OccurrenceCount_DidNotFindValidResponseOutOfAllNodes = 0
        # Post message
        API_PostOperatorNotification(message)
        return True

    else:
        return False


def ResultHandlerDelegate_DetermineHighestResult_int(resultDict):
    highestResult = 0

    # This function is great because it's going to look at the resultDict which contains many responses and iterate over them and look for the response with the highest result
    # This works well for the getting the latest block number as well as getting the latest nonce.  In both cases we want the highest value

    responseOf_highestResult = None
    for resultKey in resultDict:
        # PrintAndLog_FuncNameHeader("resultKey = " + str(resultKey) + ": resultDict[resultKey] = " + str(resultDict[resultKey]))
        if hasattr(resultDict[resultKey], 'content'):
            responseData = resultDict[resultKey].content
            jData = json.loads(responseData)
            # PrintAndLog_FuncNameHeader("jData = " + str(jData))
            # If the result is valid
            if 'result' in jData and jData['result'] and str(jData['result']).lower() != "null" and str(jData['result']).lower() != "none":
                result = ConvertHexToInt(jData['result'])
                # PrintAndLog_FuncNameHeader("jData['result'] = " + str(jData['result']) + ", " + str(result) + " " + str(resultKey))
                if result > highestResult:
                    # PrintAndLog_FuncNameHeader("found a new highest result " + str(result) + ", " + str(resultKey))
                    highestResult = result
                    responseOf_highestResult = resultDict[resultKey]
                # else:
                #     PrintAndLog_FuncNameHeader("found a result " + str(
                #         result) + ", but it didn't exceed our current highest of " + str(highestResult) + " " + str(resultKey))

    return responseOf_highestResult


def ResultHandlerDelegate_DetermineHighestResult_int_LatestBlockNumberOnly_Http(resultDict):
    # Create a new dict of the latest block numbers
    nodeBlockNumberDict = {}
    for resultKey in resultDict:
        # PrintAndLog("ResultHandlerDelegate_DetermineHighestResult_int: resultKey = " + str(resultKey) + ": resultDict[resultKey] = " + str(resultDict[resultKey]))
        if hasattr(resultDict[resultKey], 'content'):
            responseData = resultDict[resultKey].content
            jData = json.loads(responseData)
            # PrintAndLog("ResultHandlerDelegate_DetermineHighestResult_int: jData = " + str(jData))
            # If the result is valid
            if 'result' in jData and jData['result'] and str(jData['result']).lower() != "null" and str(jData['result']).lower() != "none":
                blockNumber_int = ConvertHexToInt(jData['result'])
                nodeBlockNumberDict[resultKey] = blockNumber_int

    return ResultHandlerDelegate_DetermineHighestResult_int_LatestBlockNumberOnly_Generic(nodeBlockNumberDict, resultDict)


def ResultHandlerDelegate_DetermineHighestResult_int_LatestBlockNumberOnly_Generic(
        nodeBlockNumberDict, resultDict, doCall_NinjaEvent_NewBlock=False):
    from BotEvents.ninjaEvents import NinjaEvent_NewBlock
    import Libraries.nodes

    global Lock_LatestBlockNumberDict_int
    global LatestBlockNumberDict_int
    global LatestBlockNumber_int
    global Lock_LatestBlockNumber_int
    global LatestUnixEpochTime_int
    global BlockDiscoveryTimeStampDict

    # This function has two purposes:
    # 1.) maintain a dict of each node's latest block number
    # 2.) call and return ResultHandlerDelegate_DetermineHighestResult_int

    newHighestBlockNumber = None
    # Update the LatestBlockNumberDict_int with new values ONLY IF there is a new value to update.
    # This should NOT update the value if the node API call timed out, for example
    Lock_LatestBlockNumberDict_int.acquire()
    try:
        for key in nodeBlockNumberDict:
            LatestBlockNumberDict_int[key] = nodeBlockNumberDict[key]
            if not newHighestBlockNumber or nodeBlockNumberDict[key] > newHighestBlockNumber:
                newHighestBlockNumber = nodeBlockNumberDict[key]

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception when setting LatestBlockNumberDict_int = " + traceback.format_exc())
        pass

    finally:
        Lock_LatestBlockNumberDict_int.release()

    PrintAndLog_FuncNameHeader("newHighestBlockNumber = " + str(newHighestBlockNumber) + " " + str(LatestBlockNumberDict_int))
    # PrintAndLog_FuncNameHeader("RemoteNodeList = " + str(Libraries.nodes.RemoteNodeList))
    # PrintAndLog_FuncNameHeader("RemoteNodeList_All = " + str(Libraries.nodes.RemoteNodeList_All))
    # PrintAndLog_FuncNameHeader("RemoteNodeList_Backup = " + str(Libraries.nodes.RemoteNodeList_Backup))

    newList = []
    for item in Libraries.nodes.RemoteNodeList_All:
        if item.name in LatestBlockNumberDict_int:
            latestBlockNumber_forThisNode = LatestBlockNumberDict_int[item.name]

            if not newHighestBlockNumber:
                PrintAndLog_FuncNameHeader("Including " + str(
                    item.name) + " in RemoteNodeList because no valid newHighestBlockNumber was found! newHighestBlockNumber = " + str(
                    newHighestBlockNumber) + ". Just include all nodes. Do we not have internet or are all of our nodes down at the same time?")
                newList.append(item)
            elif latestBlockNumber_forThisNode >= newHighestBlockNumber:
                # PrintAndLog_FuncNameHeader("Including " + str(item.name) + " in RemoteNodeList. latestBlockNumber_forThisNode = " + str(latestBlockNumber_forThisNode))
                newList.append(item)
            else:
                numOfBlocksBehind = newHighestBlockNumber - latestBlockNumber_forThisNode
                # PrintAndLog_FuncNameHeader("EXCLUDING " + str(item.name) + " from RemoteNodeList because it is behind by " + str(
                #     numOfBlocksBehind) + " blocks. latestBlockNumber_forThisNode = " + str(latestBlockNumber_forThisNode))

        else:
            PrintAndLog_FuncNameHeader(str(item.name) + " not found in in LatestBlockNumberDict_int")

    # PrintAndLog_FuncNameHeader("newList = " + str(newList))

    # Go through newList and remove all nodes that are in the RemoteNodeList_Backup list if we have at least one normal node in newList
    # To do that, I first need to identify if we have at least one normal or non-backup node in the list
    atLeastOneNonBackupNodeIsInList = False
    for item in newList:
        # PrintAndLog_FuncNameHeader(str(item.name) + " found in newList")
        if item not in Libraries.nodes.RemoteNodeList_Backup:
            atLeastOneNonBackupNodeIsInList = True
            break

    # Set RemoteNodeList_QuoteTokenProcessSelection based on newList BEFORE we remove backup nodes because this list wants to include backup options
    Libraries.nodes.RemoteNodeList_QuoteTokenProcessSelection = copy.deepcopy(newList)
    PrintAndLog_FuncNameHeader("RemoteNodeList_QuoteTokenProcessSelection updated with " +
                               str(len(Libraries.nodes.RemoteNodeList_QuoteTokenProcessSelection)) + " nodes " +
                               str(Libraries.nodes.RemoteNodeList_QuoteTokenProcessSelection))

    # PrintAndLog_FuncNameHeader("atLeastOneNonBackupNodeIsInList = " + str(atLeastOneNonBackupNodeIsInList))
    if atLeastOneNonBackupNodeIsInList:
        # Remove all Backup nodes from the list since we don't want to hammer them when our primary nodes are up and have the latest block number
        for item in copy.deepcopy(newList):
            if item in Libraries.nodes.RemoteNodeList_Backup:
                newList.remove(item)

    if atLeastOneNonBackupNodeIsInList:
        PrintAndLog_FuncNameHeader("newList - Removed backup options because they're not needed. Normal nodes are working. newList = " + str(newList))
    else:
        message = "newList - Did not remove backup options because they are needed! Normal nodes must be down or behind by a few blocks. newList = " + str(newList)
        PrintAndLog_FuncNameHeader(message)
        # Periodically send me notifications about this
        if Libraries.executeOnInterval.IsTimeToExecute("newList - Did not remove backup options because they are needed!", 600):
            API_PostOperatorNotification(message)

    # I should not need to do thread locking here if I'm just setting the list with a new list
    Libraries.nodes.RemoteNodeList = newList
    PrintAndLog_FuncNameHeader("RemoteNodeList updated with " + str(len(Libraries.nodes.RemoteNodeList)) + " nodes " + str(Libraries.nodes.RemoteNodeList))

    newHighestBlockNumberWasFound = False
    printMessage = None
    # Set the LatestBlockNumber_int in a threadsafe way so it can be globally accessed
    Lock_LatestBlockNumber_int.acquire()
    try:
        # If the newHighestBlockNumber is lower than the previous LatestBlockNumber_int
        # TODO, this may be unnecessary of a check... But i'll leave it in here for now in case it's relevant to a problem in the future
        if LatestBlockNumber_int and newHighestBlockNumber < LatestBlockNumber_int:
            # Something bad happened!
            message = "API_GetLatestBlockNumber found an OLDER block number, this should never happen unless there's a bug " \
                      "or somethings out of sync between http calls and websocket events. " \
                      "Check on this. Could be a node problem, software bug, or maybe some sort of blockchain re-org? " \
                      "LatestBlockNumber_int = " + str(LatestBlockNumber_int) + ", newHighestBlockNumber = " + str(newHighestBlockNumber)
            API_PostOperatorNotification(message)
            raise Exception(message)

        # If we have a new block number
        if not LatestBlockNumber_int or (newHighestBlockNumber > LatestBlockNumber_int):
            # Keep track of the latest block number
            LatestBlockNumber_int = newHighestBlockNumber
            LatestUnixEpochTime_int = int(time.time())
            newHighestBlockNumberWasFound = True

            # Keep track of when I discover blocks
            BlockDiscoveryTimeStampDict[LatestBlockNumber_int] = LatestUnixEpochTime_int

            # I want to PrintAndLog, but not inside of a lock
            printMessage = "API_GetLatestBlockNumber: new block number found = " + str(
                LatestBlockNumber_int) + ", LatestUnixEpochTime_int = " + str(LatestUnixEpochTime_int)

        # else we found the same block number we had before

    finally:
        Lock_LatestBlockNumber_int.release()

    if printMessage:
        PrintAndLog_FuncNameHeader(printMessage)

    # If we found a new highest block number
    # TODO, this if statement should also be true if there's a re-org and the block number is still the same...
    #  aka new head but same block number... I need to handle this edge case
    if newHighestBlockNumberWasFound and doCall_NinjaEvent_NewBlock:
        if Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
            PrintAndLog_FuncNameHeader("Not calling NinjaEvent_NewBlock because TimeMachine_EnabledForHistoricBlockDebug was set "
                                       "so we're ignoring real world new blocks since we're simulating with historic blocks")
        else:
            # Call NinjaEvent_NewBlock since we have a new block
            t = threading.Thread(target=NinjaEvent_NewBlock, args=(newHighestBlockNumber,))
            t.start()

    # if resultDict is set, the function caller is still expecting a valid response
    if resultDict:
        return ResultHandlerDelegate_DetermineHighestResult_int(resultDict)


def SimulateLatestBlockNumberChange(newBlockNumber):
    global Lock_LatestBlockNumberDict_int
    global LatestBlockNumberDict_int
    global LatestBlockNumber_int
    global Lock_LatestBlockNumber_int
    global LatestUnixEpochTime_int
    global BlockDiscoveryTimeStampDict

    if not Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        raise Exception("You should not be calling this if TimeMachine_EnabledForHistoricBlockDebug is not set!")

    # Not bothering to thread lock when i'm simulating block changes, this is a controlled environment
    LatestBlockNumber_int = newBlockNumber
    LatestUnixEpochTime_int = int(time.time())
    BlockDiscoveryTimeStampDict[LatestBlockNumber_int] = LatestUnixEpochTime_int


def GetTimeSinceBlockWasDiscovered(blockNumber):
    global BlockDiscoveryTimeStampDict
    return time.time() - BlockDiscoveryTimeStampDict[blockNumber]


def ResultHandlerDelegate_DetermineTransactionReceipt(resultDict):
    return ResultHandlerDelegate_JoeyZTest(resultDict)


def ResultHandlerDelegate_DetermineTransactionByHash(resultDict):
    return ResultHandlerDelegate_JoeyZTest(resultDict)


def ResultHandlerDelegate_JoeyZTest(resultDict):
    # PrintAndLog("ResultHandlerDelegate_JoeyZTest")
    found = False
    for resultKey in resultDict:
        if hasattr(resultDict[resultKey], 'content'):
            responseData = resultDict[resultKey].content
            jData = json.loads(responseData)
            # PrintAndLog("ResultHandlerDelegate_JoeyZTest: jData = " + str(jData))
            if 'result' in jData and jData['result']:
                # PrintAndLog("ResultHandlerDelegate_JoeyZTest returning result for key " + str(resultKey) + ", and jData = " + str(jData))
                return resultDict[resultKey]


def RemovePropertyFromPayloadParams(property, payload):
    if property in payload['params']:
        payload['params'].remove(property)


def SubmitRequest(url, payload, payloadIsBatched, headers, requestTimeout_seconds, blockNumber_toUse,
                  resultDict, resultKey, lock_resultDict=None):
    # Make a copy of the payload object and modify the copy
    payload_copyToUse = copy.deepcopy(payload)

    if not payloadIsBatched:
        # Set the id if it's not already set
        if 'id' not in payload_copyToUse:
            # Set the id to the payload_copyToUse
            payload_copyToUse['id'] = (randint(0, 999999999999999))

        # Clear these block number properties.  Make sure block properties like 'latest', 'pending', etc are not in the params
        RemovePropertyFromPayloadParams('latest', payload_copyToUse)
        RemovePropertyFromPayloadParams('pending', payload_copyToUse)
        RemovePropertyFromPayloadParams('earliest', payload_copyToUse)
    else:
        for batchedPayload in payload_copyToUse:
            # PrintAndLog_FuncNameHeader("batchedPayload before setting id = " + str(batchedPayload))
            # Set the id if it's not already set
            if 'id' not in batchedPayload:
                # Set the id to the payload_copyToUse
                batchedPayload['id'] = (randint(0, 999999999999999))

            # PrintAndLog_FuncNameHeader("batchedPayload after setting id = " + str(batchedPayload))

            # Clear these block number properties.  Make sure block properties like 'latest', 'pending', etc are not in the params
            RemovePropertyFromPayloadParams('latest', batchedPayload)
            RemovePropertyFromPayloadParams('pending', batchedPayload)
            RemovePropertyFromPayloadParams('earliest', batchedPayload)

    blockNumberParamIsRequired = True
    if not payloadIsBatched:
        method = payload_copyToUse['method']
        if not RpcMethodRequiresBlockNumberSpecification(method):
            # PrintAndLog_FuncNameHeader("not specifying block number for this call because it will throw an error if we do")
            blockNumberParamIsRequired = False
    else:
        for batchedPayload in payload_copyToUse:
            method = batchedPayload['method']
            # If anyone one of these methods in this batched call behave this way, treat the entire thing this way
            if not RpcMethodRequiresBlockNumberSpecification(method):
                blockNumberParamIsRequired = False
                break

    if blockNumberParamIsRequired:
        if not blockNumber_toUse:
            if not payloadIsBatched:
                # We're using only the latest
                payload_copyToUse['params'].append('latest')
            else:
                for batchedPayload in payload_copyToUse:
                    # We're using only the latest
                    batchedPayload['params'].append('latest')
        else:
            if not payloadIsBatched:
                # Append the block number to the payload_copyToUse
                # PrintAndLog_FuncNameHeader("Before payload_copyToUse = " + str(payload_copyToUse))
                payload_copyToUse['params'].append(blockNumber_toUse)
                # PrintAndLog_FuncNameHeader("After payload_copyToUse = " + str(payload_copyToUse))
            else:
                for batchedPayload in payload_copyToUse:
                    # Append the block number to the batchedPayload
                    # PrintAndLog_FuncNameHeader("Before batchedPayload = " + str(batchedPayload))
                    batchedPayload['params'].append(blockNumber_toUse)
                    # PrintAndLog_FuncNameHeader("After batchedPayload = " + str(batchedPayload))

    callSucceeded = False
    try:
        # PrintAndLog_FuncNameHeader("Making call to " + str(url) + " with payload " + str(payload_copyToUse))
        # datetimeBefore = datetime.datetime.now()
        response = requests.post(url, data=json.dumps(payload_copyToUse), headers=headers, timeout=requestTimeout_seconds)
        callSucceeded = True
        # duration_s = (datetime.datetime.now() - datetimeBefore).total_seconds()
        # PrintAndLog_FuncNameHeader("to " + str(url) + " duration_s = " + str(duration_s))

    except requests.exceptions.ConnectTimeout as ex:
        PrintAndLogError("exception (ConnectTimeout) in SubmitRequest: ex = " + str(type(ex)))
        # Do not print the stacktrace to logs or i'll get spammed and performance will suffer
        response = RequestExceptionString_ConnectTimeout
        pass
    except requests.exceptions.ConnectionError as ex:
        PrintAndLogError("exception (ConnectionError) in SubmitRequest: ex = " + str(type(ex)))
        # Do not print the stacktrace to logs or i'll get spammed and performance will suffer
        response = RequestExceptionString_ConnectionError
        pass
    except requests.exceptions.ReadTimeout as ex:
        PrintAndLogError("exception (ReadTimeout) in SubmitRequest: ex = " + str(type(ex)))
        # Do not print the stacktrace to logs or i'll get spammed and performance will suffer
        response = RequestExceptionString_ReadTimeout
        pass
    except Exception as ex:
        PrintAndLogError("exception (unknown) in SubmitRequest: ex = " + str(type(ex)) + ", stacktrace = " + str(traceback.format_exc()))
        # # Do not print the stacktrace to logs or i'll get spammed and performance will suffer
        response = "wtf"
        pass

    # if not callSucceeded:
    #     PrintAndLogError("Call failed to " + str(url) + " with payload = " + str(payload_copyToUse))

    # response = requests.post(url, data=json.dumps(payload_copyToUse), headers=headers, timeout=requestTimeout_seconds)
    # PrintAndLog_FuncNameHeader("response = " + str(response))
    # PrintAndLog_FuncNameHeader("response.content = " + str(response.content) + ", this was for url = " + str(url))

    # if there's no lock, just update the resultDict
    if not lock_resultDict:
        resultDict[resultKey] = response
    # Else we have a lock, so let's update the resultDict in a threadsafe way
    else:
        lock_resultDict.acquire()
        try:
            resultDict[resultKey] = response

        finally:
            lock_resultDict.release()


def RpcMethodRequiresBlockNumberSpecification(method):
    # Some RPC calls requires a block number specification (aka no block number, no "latest", etc)
    if method == 'eth_blockNumber' or \
            method == 'eth_estimateGas' or \
            method == 'eth_getBlockTransactionCountByHash' or \
            method == 'eth_sendTransaction' or \
            method == 'eth_sendrawtransaction' or \
            method == 'eth_getBlockByHash' or \
            method == 'eth_getTransactionByHash' or \
            method == 'eth_getTransactionByBlockHashAndIndex' or \
            method == 'eth_getTransactionReceipt' or \
            method == 'eth_pendingTransactions' or \
            method == 'eth_getBlockByNumber' or \
            method == 'eth_pendingTransactions' or \
            method == 'eth_gasPrice' or \
            method == 'eth_getLogs' or \
            method == 'debug_traceTransaction' or \
            method == 'eth_sendRawTransaction':
        return False
    else:
        return True


def API_GetLatestBlockNumber():
    global LatestBlockNumber_int
    global Lock_LatestBlockNumber_int
    global LatestUnixEpochTime_int

    from Libraries.nodes import RemoteNodeList_All

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_blockNumber",
        "params": []
    }
    # Send requests to RemoteNodeList_All here because this call is the one that dictates which nodes the other calls will use
    # This call's ResultHandlerDelegate will decide whether or not a node is behind and worth making calls to
    response = SendRequestToSpecifiedNodes(RemoteNodeList_All, payload, Headers, RequestTimeout_wickedShort_seconds,
                                           ResultHandlerDelegate_DetermineHighestResult_int_LatestBlockNumberOnly_Http, False, True)
    # PrintAndLog("response = " + str(response))
    if response.ok:
        # result is handled in the ResultHandlerDelegate
        pass

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def GetAgeOfLatestBlockNumber_int():
    global LatestUnixEpochTime_int
    return int(time.time()) - LatestUnixEpochTime_int


def API_GetBlock(blockNumber_int, returnFullTransactionObjects=False, resultDict=None, resultKey=None, lock_resultDict=None):
    from Libraries.utils import ConsiderSettingResultDict

    # {"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["0x1b4", true],"id":1}
    blockNumber_hex = '0x' + ConvertIntToHex(blockNumber_int)
    # PrintAndLog("API_GetBlock blockNumber_int = " + str(blockNumber_int) + ", blockNumber_hex = " + str(blockNumber_hex) + ", returnFullTransactionObjects = " + str(returnFullTransactionObjects))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_getBlockByNumber",
        "params": [
            blockNumber_hex,
            returnFullTransactionObjects
        ]
    }
    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_long_seconds, None, False)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetBlock responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetBlock jData = " + str(jData))
        ConsiderSettingResultDict(jData['result'], resultDict, resultKey, lock_resultDict)
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetBlock_Batched_Safe(blockNumberList_int, returnFullTransactionObjects=False, doRejectBatchedPayloadIfAnyOneHasError=False):
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 50

    list_chunked = SplitListIntoChunks_OfSizeNoGreaterThanThis(blockNumberList_int, chunkSize)

    concatenatedList = []
    for i in range(len(list_chunked)):
        concatenatedList += API_GetBlock_Batched(list_chunked[i], returnFullTransactionObjects, doRejectBatchedPayloadIfAnyOneHasError)
        # Sleep a bit between calls.  To give the nodes a break
        time.sleep(0.2)

    return concatenatedList


def API_GetBlock_Batched(blockNumberList_int, returnFullTransactionObjects=False, doRejectBatchedPayloadIfAnyOneHasError=False):
    # PrintAndLog("API_GetBlock_Batched with blockNumberList_int of len " + str(len(blockNumberList_int)) + " = " + str(blockNumberList_intblockNumberList_int))

    payload_total = []
    blockNumberPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for blockNumber in blockNumberList_int:
        # Increment the id each time around
        payloadId += 1

        # Pair the blockNumber with the Id so I can know which is which in the response
        blockNumberPayloadIdDict[payloadId] = blockNumber

        blockNumber_hex = '0x' + ConvertIntToHex(blockNumber)

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_getBlockByNumber",
            "params": [
                blockNumber_hex,
                returnFullTransactionObjects
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("payload_total = " + str(payload_total))

    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_veryLong_seconds, None, False, False, True, doRejectBatchedPayloadIfAnyOneHasError)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetBlock_Batched jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in blockNumberPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = blockNumberPayloadIdDict[id]
                    result = batchedResult['result']
                    # PrintAndLog("found Id " + str(id) + " in the response. result = " + str(result))
                    resultList.append(result)
                    break

        if len(resultList) != len(blockNumberList_int):
            raise Exception("Length of lists did not match, response data is bad?")

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetBlock_Batched response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetGasPrice():
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_gasPrice",
        "params": [],
    }
    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_long_seconds, None, False)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        gasPrice_int_weiUnits = ConvertHexToInt(jData['result'])
        gasPrice_int_gweiUnits = ConvertWeiToGwei(gasPrice_int_weiUnits)
        return gasPrice_int_weiUnits, gasPrice_int_gweiUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetLogs(fromBlock_int, toBlock_int, topics, contractAddress=None):
    fromBlock_hex = '0x' + ConvertIntToHex(fromBlock_int)
    toBlock_hex = '0x' + ConvertIntToHex(toBlock_int)
    PrintAndLog("API_GetLogs fromBlock_int = " + str(fromBlock_int) + ", toBlock_int = " + str(toBlock_int))
    payload = {
        "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_getLogs",
        "params": [
            {
                "topics": topics,
                "address": contractAddress,
                "fromBlock": fromBlock_hex,
                "toBlock": toBlock_hex
            }
        ]
    }
    try:
        response = SendRequestToAllNodes(payload, Headers, RequestTimeout_wickedLong_seconds, None, False)
        if response.ok:
            responseData = response.content
            # PrintAndLog("API_GetLogs responseData = " + str(responseData))
            jData = json.loads(responseData)
            # PrintAndLog("API_GetLogs jData = " + str(jData))
            return jData['result']

        else:
            # If response code is not ok (200), print the resulting http error code with description
            response.raise_for_status()

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception in API_GetLogs: " + traceback.format_exc())
        # TODO this could either be an error/exception or no data matched our filter
        # For now let's assume that no data matched our filter.
        return []


def API_GetLogs_Safe(contractAddress, topics, fromBlock_int, toBlock_int):
    PrintAndLog_FuncNameHeader("fromBlock_int = " + str(fromBlock_int) + ", toBlock_int = " + str(toBlock_int))

    # This is an optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    # Do note, this assumes that we're batching within the call as well
    # chunkSize = 1000
    chunkSize = None  # Use None for Dynamic
    dynamicChunkSizingEnabled = True
    jData_logs_combined = []
    interval = toBlock_int - fromBlock_int
    sum = 0
    fromBlock_toUse = fromBlock_int
    count = 0
    apiCallDuration_seconds = None
    while sum < interval:
        if dynamicChunkSizingEnabled:
            chunkSize = GetChunkSize_EventLogs_GivenDataDict(apiCallDuration_seconds, chunkSize)

        toBlock_toUse = min(toBlock_int, fromBlock_toUse + chunkSize)
        before = datetime.datetime.now()
        PrintAndLog_FuncNameHeader("Calling API_GetLogs with chunkSize = " + str(chunkSize) + ", fromBlock_toUse = " + str(
            fromBlock_toUse) + " and toBlock_toUse = " + str(toBlock_toUse) + " interval = " + str(toBlock_toUse - fromBlock_toUse))
        jData_logs_combined += API_GetLogs(fromBlock_toUse, toBlock_toUse, topics, contractAddress)
        # jData_logs_combined += GetTradeHistoryLogs(exchangeName, contractAddress, fromBlock_toUse, toBlock_toUse, traderAddress)
        apiCallDuration_seconds = (datetime.datetime.now() - before).total_seconds()
        PrintAndLog_FuncNameHeader("Done, apiCallDuration_seconds = " + str(apiCallDuration_seconds) + " seconds. Sleeping a bit to give the node a rest")

        sum += chunkSize
        fromBlock_toUse += chunkSize
        count += 1

        # Give the node a break
        # time.sleep(1.8)  # Use this when using a hard coded chunkSize
        time.sleep(0.05)  # Use this when using the dynamically adjusting chunkSize

    PrintAndLog_FuncNameHeader("Called API_GetLogs " + str(count) + " times. Found " + str(len(jData_logs_combined)) + " results")
    # PrintAndLog_FuncNameHeader("jData_logs_combined = " + str(jData_logs_combined))
    return jData_logs_combined


def API_GetLogs_Batched(fromBlock_int, toBlock_int, topics, contractAddress=None):
    PrintAndLog("API_GetLogs_Batched fromBlock_int = " + str(fromBlock_int) + ", toBlock_int = " + str(toBlock_int))

    # This is an optimization
    # I'm splitting up the arguments to multiple batches within one single call because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 4000
    # chunkSize = 40000

    payload_total = []
    interval = toBlock_int - fromBlock_int
    sum = 0
    fromBlock_toUse = fromBlock_int
    count = 0
    while sum < interval:
        toBlock_toUse = min(toBlock_int, fromBlock_toUse + chunkSize)
        PrintAndLog("API_GetLogs_Batched with fromBlock_toUse = " + str(fromBlock_toUse) + " and toBlock_toUse = " + str(toBlock_toUse) + " interval = " + str(
            toBlock_toUse - fromBlock_toUse))
        fromBlock_toUse_hex = '0x' + ConvertIntToHex(fromBlock_toUse)
        toBlock_toUse_hex = '0x' + ConvertIntToHex(toBlock_toUse)
        # PrintAndLog("API_GetLogs_Batched with fromBlock_toUse_hex = " + str(fromBlock_toUse_hex) + " and toBlock_toUse_hex = " + str(toBlock_toUse_hex) + " interval = " + str(toBlock_toUse - fromBlock_toUse))
        subPayload = {
            "id": randint(0, 9999999999999999),
            "jsonrpc": "2.0",
            "method": "eth_getLogs",
            "params": [
                {
                    "topics": topics,
                    "address": contractAddress,
                    "fromBlock": fromBlock_toUse_hex,
                    "toBlock": toBlock_toUse_hex
                }
            ]
        }
        payload_total.append(subPayload)

        sum += chunkSize
        fromBlock_toUse += chunkSize
        count += 1

    # PrintAndLog("API_GetLogs_Batched payload_total = " + str(payload_total))
    try:
        # url = RemoteNodeList[1].value
        # PrintAndLog("API_GetLogs_Batched  url = " + str(url))
        # response = requests.post(url, data=json.dumps(payload_total), headers=Headers, timeout=RequestTimeout_seconds)
        response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_wickedLong_seconds, None, False, False, True)
        if response.ok:
            responseData = response.content
            # PrintAndLog("API_GetLogs_Batched responseData = " + str(responseData))
            jData = json.loads(responseData)
            # PrintAndLog("API_GetLogs_Batched jData = " + str(jData))

            # Parse the batched response of all the response lists combined
            resultsList = []
            for batchedResult in jData:
                resultsList += batchedResult['result']

            return resultsList

        else:
            # If response code is not ok (200), print the resulting http error code with description
            response.raise_for_status()

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception in API_GetLogs: " + traceback.format_exc())
        # TODO this could either be an error/exception or no data matched our filter
        # For now let's assume that no data matched our filter.
        return []


def GetChunkSize_EventLogs_GivenDataDict(apiCallDuration_seconds, chunkSizeUsed_previous):
    max = 20000
    min = 1000

    if not apiCallDuration_seconds or not chunkSizeUsed_previous:
        # This is the chunksize we'll start with
        chunkSizeUsed_new = min
        # PrintAndLog("GetChunkSize_EventLogs_GivenDataDict: chunkSizeUsed_new is defaulted to the min " + str(chunkSizeUsed_new))
        return int(chunkSizeUsed_new)

    durationThreshold_decrease_seconds = RequestTimeout_wickedLong_seconds * 0.30
    durationThreshold_increase_seconds = RequestTimeout_wickedLong_seconds * 0.10
    # if the call took too long (getting close to the length of the timeout used for the API call)
    if apiCallDuration_seconds > durationThreshold_decrease_seconds:
        # Reduce the chunk size
        chunkSizeUsed_new = chunkSizeUsed_previous * 0.66
        # PrintAndLog("GetChunkSize_EventLogs_GivenDataDict: call took too long (" + str(
        #     round(apiCallDuration_seconds, 1)) + " seconds), so let's decrease the chunk size. chunkSizeUsed_new = " + str(chunkSizeUsed_new))

    elif apiCallDuration_seconds > durationThreshold_increase_seconds:
        # Slightly increase the chunk size
        chunkSizeUsed_new = chunkSizeUsed_previous * 1.10
        # PrintAndLog("GetChunkSize_EventLogs_GivenDataDict: call didn't take too long, so let's increase the chunk size slightly. chunkSizeUsed_new = " + str(chunkSizeUsed_new))

    else:
        # Greatly increase the chunk size
        chunkSizeUsed_new = chunkSizeUsed_previous * 1.25
        # PrintAndLog("GetChunkSize_EventLogs_GivenDataDict: call didn't take too long, so let's increase the chunk size slightly. chunkSizeUsed_new = " + str(chunkSizeUsed_new))

    # Round to nearest 10
    chunkSizeUsed_new = RoundUp_NearestTen(chunkSizeUsed_new)
    # PrintAndLog("GetChunkSize_EventLogs_GivenDataDict: chunkSizeUsed_new rounded up to nearest ten = " + str(chunkSizeUsed_new))

    # Enforce mins and maxes
    if chunkSizeUsed_new > max:
        chunkSizeUsed_new = max
        # PrintAndLog("GetChunkSize_EventLogs_GivenDataDict: chunkSizeUsed_new is the max " + str(chunkSizeUsed_new))
        return int(chunkSizeUsed_new)

    if chunkSizeUsed_new < min:
        chunkSizeUsed_new = min
        # PrintAndLog("GetChunkSize_EventLogs_GivenDataDict: chunkSizeUsed_new is the min " + str(chunkSizeUsed_new))
        return int(chunkSizeUsed_new)

    # PrintAndLog("GetChunkSize_EventLogs_GivenDataDict: chunkSizeUsed_new is " + str(chunkSizeUsed_new) + ", chunkSizeUsed_previous was " + str(chunkSizeUsed_previous))
    return int(chunkSizeUsed_new)


def API_GetBlockByBlockHash(blockHash, returnFullTransactionObjects):
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_getBlockByHash",
        "params": [
            blockHash,
            returnFullTransactionObjects
        ]
    }
    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_wickedLong_seconds, None, False)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetBlockByBlockHash responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetBlockByBlockHash jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetBlockByBlockHash_Batched_Safe(blockHashList, returnFullTransactionObjects):
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 20  # It doesn't work at 4000, works at 1000, so somewhere less than 1000 is safe
    # I had a problem at chunkSize 200 when getting data from 2017... I killed my node

    orderList_chunked = SplitListIntoChunks_OfSizeNoGreaterThanThis(blockHashList, chunkSize)

    concatenatedList = []
    for i in range(len(orderList_chunked)):
        before = datetime.datetime.now()
        concatenatedList += API_GetBlockByBlockHash_Batched(orderList_chunked[i], returnFullTransactionObjects)
        apiCallDuration_seconds = (datetime.datetime.now() - before).total_seconds()
        PrintAndLog(
            "API_GetBlockByBlockHash_Batched_Safe: Done, apiCallDuration_seconds = " + str(apiCallDuration_seconds) + " seconds. Sleeping a bit to give the node a rest")
        # Sleep a bit between calls.  To give the nodes a break
        time.sleep(0.2)

    return concatenatedList


def API_GetBlockByBlockHash_Batched(blockHashList, returnFullTransactionObjects):
    payload_total = []
    blockHashPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for blockHash in blockHashList:
        # Increment the id each time around
        payloadId += 1

        # Pair the blockHashList with the Id so I can know which is which in the response
        blockHashPayloadIdDict[payloadId] = blockHash

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_getBlockByHash",
            "params": [
                blockHash,
                returnFullTransactionObjects
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("payload_total = " + str(payload_total))
    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_wickedLong_seconds, None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetBlockByBlockHash_Batched jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in blockHashPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = blockHashPayloadIdDict[id]
                    result = batchedResult['result']
                    # PrintAndLog("found Id " + str(id) + " in the response. result = " + str(result))
                    resultList.append(result)
                    break

        if len(resultList) != len(blockHashList):
            raise Exception("Length of lists did not match, response data is bad?")

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetBlockByBlockHash_Batched response was not ok response = " + str(response))
        response.raise_for_status()


def GetKnownHardCodedERC20TokenSymbol(tokenAddress):
    if tokenAddress.lower() == '0xe0b7927c4af23765cb51314a0e0521a9645f0e2a'.lower():
        return "DGD"

    if tokenAddress.lower() == '0xb5a5f22694352c15b00323844ad545abb2b11028'.lower():
        return 'ICX'


def Sentinel_ConsiderCheckingOnAllRemoteNodes():
    from Libraries.nodes import RemoteNodeList
    global SentinelDict
    global DateTimeOfLast_SentinelDictParse
    global Interval_seconds_SentinelDictParse
    global DateTimeOfLast_SentinelDictEnforcingBadNodes
    global Interval_seconds_SentinelDictEnforcingBadNodes

    # Consider updating the SentinelDict
    if not DateTimeOfLast_SentinelDictParse or ((datetime.datetime.now() - DateTimeOfLast_SentinelDictParse).total_seconds() > Interval_seconds_SentinelDictParse):
        DateTimeOfLast_SentinelDictParse = datetime.datetime.now()

        for remoteNode in RemoteNodeList:
            # init the SentinelDict if not already
            if remoteNode not in SentinelDict:
                SentinelDict[remoteNode] = None

            nodeIsResponsive, nodeIsFullySynced, nodeIsSyncing, blocksBehind, syncSpeed, estimatedTimeRemaining_minutes = Sentinel_CheckOnRemoteNode(remoteNode)
            if nodeIsFullySynced and nodeIsResponsive:
                SentinelDict[remoteNode] = datetime.datetime.now()

                # Alert me if nodes are out of sync or unresponsive for a long period of time
                # if not DateTimeOfLast_SentinelDictEnforcingBadNodes or ((datetime.datetime.now() - DateTimeOfLast_SentinelDictEnforcingBadNodes).total_seconds() > Interval_seconds_SentinelDictEnforcingBadNodes):
                #     DateTimeOfLast_SentinelDictEnforcingBadNodes = datetime.datetime.now()


def Sentinel_CheckOnRemoteNode(remoteNode):
    global SentinelLastBlockNumberDict
    global Interval_seconds_SentinelDictParse
    # WARNING, Do not call this too often or else it'll tell you that nodes are not syncing. If blocktime = 14 seconds then don't call it every 1 second...
    # Call it every minute or so at the fastest

    # Make some test calls to the node to see if I get responses.
    nodeIsResponsive = False
    nodeIsFullySynced = False
    nodeIsSyncing = False
    blocksBehind = 0
    try:
        latestBlockNumber_fromAllNodes = ConvertHexToInt(API_GetLatestBlockNumber())
        thisNodesLatestBlockNumber = API_Sentinel_GetLatestBlockNumber(remoteNode.value)
        PrintAndLog(str(thisNodesLatestBlockNumber) + ": " + str(remoteNode.name))

        # Check to see if this node is synced with respect to other nodes, given some wiggle room.
        wiggleRoom_blocks = 1

        nodeIsFullySynced = (thisNodesLatestBlockNumber + wiggleRoom_blocks) >= latestBlockNumber_fromAllNodes
        blocksBehind = latestBlockNumber_fromAllNodes - thisNodesLatestBlockNumber
        nodeIsResponsive = thisNodesLatestBlockNumber and API_Sentinel_GetEtherBalance(remoteNode.value)
        # Determine if it's syncing by getting the previous block number from SentinelLastBlockNumberDict
        nodeIsSyncing = remoteNode not in SentinelLastBlockNumberDict or thisNodesLatestBlockNumber > SentinelLastBlockNumberDict[remoteNode]
        syncSpeed = None
        blocksRemaining = latestBlockNumber_fromAllNodes - thisNodesLatestBlockNumber
        estimatedTimeRemaining_minutes = None
        if remoteNode in SentinelLastBlockNumberDict:
            blockNumberDifference = thisNodesLatestBlockNumber - SentinelLastBlockNumberDict[remoteNode]
            time_s = Interval_seconds_SentinelLoopSleepTime
            syncSpeed = blockNumberDifference / time_s
            if syncSpeed > 0:
                estimatedTimeRemaining_seconds = blocksRemaining / syncSpeed
                estimatedTimeRemaining_minutes = round(estimatedTimeRemaining_seconds / 60, 0)
            else:
                estimatedTimeRemaining_minutes = None

        # Set the new previous block number for next time around
        SentinelLastBlockNumberDict[remoteNode] = thisNodesLatestBlockNumber

    except:
        PrintAndLogError("exception in API_Sentinel_GetLatestBlockNumber.  Assume that the node is down: " + traceback.format_exc())
        # PrintAndLogError("exception in API_Sentinel_GetLatestBlockNumber.  Assume that the node is down.")
        pass

    timeRemainingString = None
    if estimatedTimeRemaining_minutes:
        timeRemainingString = str(estimatedTimeRemaining_minutes) + " minutes or " + str(round(estimatedTimeRemaining_minutes / 60, 1)) + " hours"
    if nodeIsResponsive and nodeIsFullySynced and nodeIsSyncing:
        PrintAndLog("Sentinel has validated " + str(remoteNode.name) + ". It's online, responsive, and fully synced.")
    elif not nodeIsResponsive:
        PrintAndLog("Sentinel has found " + str(remoteNode.name) + " as offline or not responsive")
    elif nodeIsSyncing and not nodeIsFullySynced:
        PrintAndLog("Sentinel has found " + str(remoteNode.name) + " to be syncing but not yet fully synced. It's behind by " + str(
            blocksBehind) + " blocks. syncSpeed = " + str(syncSpeed) + " blocks/second, sync time remaining = " + str(timeRemainingString))

    elif not nodeIsSyncing:
        PrintAndLog("Sentinel has found " + str(remoteNode.name) + " to not be syncing at all. It's stuck and behind by " + str(
            blocksBehind) + " blocks. syncSpeed = " + str(syncSpeed) + " blocks/second, sync time remaining = " + str(timeRemainingString))
    else:
        PrintAndLog("Sentinel has found " + str(remoteNode.name) + " to be broken in some incomprehensible way. nodeIsResponsive = " + str(
            nodeIsResponsive) + ", nodeIsFullySynced = " + str(nodeIsFullySynced) + ", nodeIsSyncing = " + str(nodeIsSyncing))

    return nodeIsResponsive, nodeIsFullySynced, nodeIsSyncing, blocksBehind, syncSpeed, estimatedTimeRemaining_minutes


def API_Sentinel_GetLatestBlockNumber(url):
    # Make a test call to the node to get the current block number
    payload = {
        "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_blockNumber",
        "params": []
    }
    beforeDateTime = datetime.datetime.now()
    response = requests.post(url, data=json.dumps(payload), headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_Sentinel_GetLatestBlockNumber responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_Sentinel_GetLatestBlockNumber jData = " + str(jData) + ", url = " + str(url))
        blockNumber_hex = jData['result']
        blockNumber_int = ConvertHexToInt(blockNumber_hex)
        duration_s = (datetime.datetime.now() - beforeDateTime).total_seconds()
        duration_ms = round(duration_s * 1000)
        PrintAndLog(str(datetime.datetime.now()) + " API_GetLatestBlockNumber: block " + str(blockNumber_int) + ", duration_ms = " + str(
            duration_ms) + ", for url = " + str(url))
        return blockNumber_int

    else:
        return False


def API_Sentinel_GetEtherBalance(url):
    # Make a test call to the node to get the ether balance of an address
    payload = {
        "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_getBalance",
        "params": [
            "0x755468ba98e5dcf979452044b8278c0858d8ad4a",
            "latest"
        ]
    }
    response = requests.post(url, data=json.dumps(payload), headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_Sentinel_GetEtherBalance responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_Sentinel_GetEtherBalance jData = " + str(jData) + ", url = " + str(url))
        return True

    else:
        return False


# Optimization, do not need to thread lock this
def GetThreadSafeCopyOf_LatestBlockNumber_int():
    global LatestBlockNumber_int

    if Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        return Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug

    return LatestBlockNumber_int


# Optimization, do not need to thread lock this
def GetThreadSafeCopyOf_LatestUnixEpochTime_int():
    global LatestUnixEpochTime_int

    return LatestUnixEpochTime_int


def ConsiderUpdatingLatestBlockNumber():
    if IsTimeToExecute("API_GetLatestBlockNumber()", Libraries.defaults.Interval_seconds_LatestBlockNumberCheck, API_GetLatestBlockNumber()):
        API_GetLatestBlockNumber()


def API_AllNodes_GetBalance():
    PrintAndLog("API_AllNodes_GetBalance")
    payload = {
        # Do not put 'id' property here, it gets appended inside SendRequestToSpecifiedNodes
        "jsonrpc": "2.0",
        "method": "eth_getBalance",
        "params": [
            "0x0015cb2187299d43b6313ae138a966899c72630c",
            # Do not put block property here (like 'latest'), it gets appended inside SendRequestToSpecifiedNodes
        ]
    }
    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_PostGetBalance_Infura responseData = " + responseData)
        jData = json.loads(responseData)
        # PrintAndLog("jData = " + str(jData))
        # PrintAndLog("jData['result'] = " + str(jData['result']))
        # Infura returns the value in wei as a hex, not a long
        wei = ConvertHexToInt(jData['result'])
        # PrintAndLog("API_AllNodes_GetBalance: wei = " + str(wei))
        ether = ConvertWeiToEther(wei, Ether_Decimals)
        PrintAndLog("API_AllNodes_GetBalance: ether = " + str(ether))
        return float(ether)

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetEtherBalance(address, resultDict=None, resultKey=None, lock_resultDict=None, timeout=RequestTimeout_seconds, specifiedBlockNumber_int=None):
    from Libraries.utils import ConsiderSettingResultDict

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_getBalance",
        "params": [
            address,
        ]
    }
    response = SendRequestToAllNodes(payload, Headers, timeout, None, False, False, False, True, specifiedBlockNumber_int)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetEtherBalance responseData = " + responseData)
        jData = json.loads(responseData)
        # PrintAndLog("jData = " + str(jData))
        result = jData['result']
        wei = ConvertHexToInt(result)
        # PrintAndLog("wei = " + str(wei))
        ether = float(ConvertWeiToEther(wei, Ether_Decimals))
        # PrintAndLog("ether = " + str(ether))
        ConsiderSettingResultDict(ether, resultDict, resultKey, lock_resultDict)
        return ether

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetEtherBalance_Batched(addressList, resultsDict=None, key=None, blockNumber=None):
    if key:
        resultsDict[key] = None

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for address in addressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the address with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_getBalance",
            "params": [address]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader(("payload_total = " + str(payload_total))
    # response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_veryLong_seconds, None, False, False, True, True, blockNumber)
    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_veryLong_seconds,
                                     None, False, False, True, False, blockNumber)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader(("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    # PrintAndLog_FuncNameHeader("batchedResult = " + str(batchedResult))
                    if 'result' not in batchedResult:
                        resultList.append(None)
                    else:
                        # address = addressPayloadIdDict[id]
                        result = batchedResult['result']
                        # Handle invalid balances
                        if not result or result == '0x':
                            resultList.append(None)
                        else:
                            balance_wei = ConvertHexToInt(result)
                            # PrintAndLog_FuncNameHeader(("balance_wei = " + balance_wei)
                            balance_ether = ConvertWeiToEther(balance_wei, Ether_Decimals)
                            # PrintAndLog_FuncNameHeader(("found Id " + str(id) + " in the response. result = " + str(result))
                            resultList.append(balance_ether)

                    # Break since we've matched the id
                    break

        if len(resultList) != len(addressList):
            raise Exception("Length of lists did not match, response data is bad?")

        if key:
            resultsDict[key] = resultList

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetEtherBalance_Batched_Safe(addressList, resultsDict=None, key=None, blockNumber=None):
    # Make the function call with a large number of items,
    # chunk it into multiple calls so we don't over load the node
    chunkSize = 700

    addressList_chunked = SplitListIntoChunks_OfSizeNoGreaterThanThis(addressList, chunkSize)

    resultList = []
    for i in range(len(addressList_chunked)):
        resultList += API_GetEtherBalance_Batched(addressList_chunked[i], None, None, blockNumber)

    if key:
        resultsDict[key] = resultList

    return resultList


def API_GetTokenBalance(walletAddress, tokenAddress, decimals,
                        resultDict=None, resultKey=None, lock_resultDict=None,
                        timeout=RequestTimeout_seconds, specifiedBlockNumber_int=None, units=Units.Ether):
    from Libraries.utils import ConsiderSettingResultDict

    # PrintAndLog_FuncNameHeader("tokenAddress: " + tokenAddress + ", walletAddress: " + walletAddress + ", decimals = " + str(decimals))

    # Remove the 0x from the tokenAddress
    walletAddress_With0xRemoved = walletAddress.replace("0x", "")
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": "0x70a08231000000000000000000000000" + walletAddress_With0xRemoved,
                "to": tokenAddress
            },
        ]
    }
    response = SendRequestToAllNodes(payload, Headers, timeout, None, False, False, False, True, specifiedBlockNumber_int)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        # Some contracts return padding extra zeros at the end of the uint256, this will strip that off...
        result = GetFirstXCharsInString(jData['result'], LengthOfDataProperty_Including0x)
        balance_weiUnits = ConvertHexToInt(result)
        # PrintAndLog_FuncNameHeader("balance_weiUnits = " + str(balance_weiUnits))

        if units == Units.Wei:
            ConsiderSettingResultDict(balance_weiUnits, resultDict, resultKey, lock_resultDict)
            return balance_weiUnits
        else:
            balance_etherUnits = ConvertWeiToEther(balance_weiUnits, decimals)
            # PrintAndLog_FuncNameHeader("balance_etherUnits = " + str(balance_etherUnits))
            ConsiderSettingResultDict(balance_etherUnits, resultDict, resultKey, lock_resultDict)
            return balance_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetTokenBalance_Batched(walletAddressList, tokenAddressList, decimalsList=None, resultsDict=None, key=None, blockNumber=None):
    # PrintAndLog_FuncNameHeader("walletAddressList = " + str(walletAddressList))
    # PrintAndLog_FuncNameHeader("tokenAddressList = " + str(tokenAddressList))
    # PrintAndLog_FuncNameHeader("decimalsList = " + str(decimalsList))
    # PrintAndLog_FuncNameHeader("key = " + str(key))
    # PrintAndLog_FuncNameHeader("blockNumber = " + str(blockNumber))

    if not decimalsList:
        decimalsList = []
        for tokenAddress in tokenAddressList:
            decimalsList.append(GetDecimalsForTokenContract(tokenAddress, True))

    if key:
        resultsDict[key] = None

    if len(walletAddressList) == len(tokenAddressList) == len(decimalsList):
        pass
    else:
        raise Exception("All parameter lists must have equal lengths")

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for index, walletAddress in enumerate(walletAddressList):
        # Increment the id each time around
        payloadId += 1

        tokenAddress = tokenAddressList[index]
        walletAddress_With0xRemoved = None
        if walletAddress:
            # Remove the 0x from the tokenAddress
            walletAddress_With0xRemoved = walletAddress.replace("0x", "")
        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = tokenAddress

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": "0x70a08231000000000000000000000000" + str(walletAddress_With0xRemoved),
                    "to": tokenAddress
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_veryLong_seconds,
                                     None, False, False, True, False, blockNumber)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("duration_s " + str(response.elapsed.total_seconds()) + ", jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    # Handle invalid balances
                    if 'result' not in batchedResult:
                        resultList.append(None)
                    else:
                        # Some contracts return padding extra zeros at the end of the uint256, this will strip that off...
                        # PrintAndLog_FuncNameHeader("batchedResult['result'] = " + str(batchedResult['result']))
                        result = GetFirstXCharsInString(batchedResult['result'], LengthOfDataProperty_Including0x)
                        # PrintAndLog_FuncNameHeader("result = " + str(result))
                        # Handle invalid balances
                        if not result or result == '0x':
                            resultList.append(None)
                        else:
                            balance_wei = ConvertHexToInt(result)
                            # PrintAndLog_FuncNameHeader("balance_wei = " + str(balance_wei))

                            # Handle invalid balances
                            if not IsTokenBalanceValid(balance_wei):
                                resultList.append(None)
                            else:
                                # We need the decimals, so let's match the decimals with the tokenAddress from the params passed into this function
                                tokenAddress = addressPayloadIdDict[id]
                                # Use indexes to match, for example the 5th entry in the decimalsList goes with the 5th entry in the tokenAddressList
                                indexOfTokenAddress = tokenAddressList.index(tokenAddress)
                                decimals = decimalsList[indexOfTokenAddress]
                                # PrintAndLog_FuncNameHeader("decimals = " + str(decimals))
                                balance_ether = ConvertWeiToEther(balance_wei, decimals)
                                # PrintAndLog_FuncNameHeader("found Id " + str(id) + " in the response. balance_ether = " + str(balance_ether) + ", goes with tokenAddress = " + str(
                                #     tokenAddress) + " and decimals = " + str(decimals))
                                resultList.append(balance_ether)

                    # Break since we've matched the id
                    break

        if len(resultList) != len(walletAddressList):
            raise Exception("Length of lists did not match, response data is bad?")

        if key:
            resultsDict[key] = resultList

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetTokenBalance_Batched_Safe(walletAddressList, tokenAddressList, decimalsList=None, resultsDict=None, key=None, blockNumber=None):
    # Make the function call with a large number of items,
    # chunk it into multiple calls so we don't over load the node
    chunkSize = 700

    walletAddressList_chunked = SplitListIntoChunks_OfSizeNoGreaterThanThis(walletAddressList, chunkSize)
    tokenAddressList_chunked = SplitListIntoChunks_OfSizeNoGreaterThanThis(tokenAddressList, chunkSize)
    if decimalsList:
        decimalsList_chunked = SplitListIntoChunks_OfSizeNoGreaterThanThis(decimalsList, chunkSize)

    resultList = []
    for i in range(len(walletAddressList_chunked)):
        decimalsList_toUse = None
        if decimalsList:
            decimalsList_toUse = decimalsList_chunked[i]

        resultList += API_GetTokenBalance_Batched(walletAddressList_chunked[i], tokenAddressList_chunked[i], decimalsList_toUse, None, None, blockNumber)

    if key:
        resultsDict[key] = resultList

    return resultList


def IsTokenBalanceValid(balanceWeiUnits):
    if balanceWeiUnits > DataF_Int:
        return False
    else:
        return True


def API_GetTransactionReceipt(transactionId, resultDict=None, resultKey=None, lock_resultDict=None,
                              timeout=RequestTimeout_seconds, specifiedBlockNumber_int=None):
    from Libraries.utils import ConsiderSettingResultDict

    # PrintAndLog("API_GetTransactionReceipt transactionId = " + str(transactionId))

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_getTransactionReceipt",
        "params": [
            transactionId
        ]
    }

    # PrintAndLog("API_GetTransactionReceipt: payload = " + str(payload))
    response = SendRequestToAllNodes(payload, Headers, timeout, ResultHandlerDelegate_DetermineTransactionReceipt,
                                     False, False, False, True, specifiedBlockNumber_int)
    if response.ok:
        try:
            responseData = response.content
            # PrintAndLog("responseData = " + str(responseData))
            jData = json.loads(responseData)
            # PrintAndLog("API_GetTransactionReceipt jData = " + str(jData))

            ConsiderSettingResultDict(jData['result'], resultDict, resultKey, lock_resultDict)
            return jData['result']

        except:
            PrintAndLogError("exception in API_GetTransactionReceipt: " + traceback.format_exc())
            pass

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetTransactionReceipt_Batched(txHashList):
    # PrintAndLog("API_GetTransactionReceipt_Batched with txHashList of len " + str(len(txHashList)) + " = " + str(txHashList))

    payload_total = []
    txHashPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for txHash in txHashList:
        # Increment the id each time around
        payloadId += 1

        # Pair the txHash with the Id so I can know which is which in the response
        txHashPayloadIdDict[payloadId] = txHash

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_getTransactionReceipt",
            "params": [str(txHash)]
        }
        payload_total.append(payload)

    # PrintAndLog("payload_total = " + str(payload_total))

    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_veryLong_seconds, None, False, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTransactionReceipt_Batched jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in txHashPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = txHashPayloadIdDict[id]
                    result = batchedResult['result']
                    # PrintAndLog("found Id " + str(id) + " in the response. result = " + str(result))
                    resultList.append(result)
                    break

        if len(resultList) != len(txHashList):
            raise Exception("Length of lists did not match, response data is bad?")

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetTransactionReceipt_Batched response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetTransactionReceipt_Batched_Safe(txHashList):
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 200  # 500 works quickly, 2000 works but noticeably slower

    txHashList_chunked = SplitListIntoChunks_OfSizeNoGreaterThanThis(txHashList, chunkSize)

    concatenatedList = []
    for i in range(len(txHashList_chunked)):
        concatenatedList += API_GetTransactionReceipt_Batched(txHashList_chunked[i])
        # Sleep a bit between calls.  To give the nodes a break
        time.sleep(0.2)

    return concatenatedList


def API_GetTransactionInfo(transactionId, resultDict=None, resultKey=None, lock_resultDict=None,
                           timeout=RequestTimeout_seconds, specifiedBlockNumber_int=None):
    from Libraries.utils import ConsiderSettingResultDict

    # PrintAndLog("API_GetTransactionInfo transactionId: " + str(transactionId))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_getTransactionByHash",
        "params": [
            transactionId
        ]
    }
    # PrintAndLog("API_GetTransactionInfo payload: " + str(payload))
    response = SendRequestToAllNodes(payload, Headers, timeout, None, False, False, False, True, specifiedBlockNumber_int)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)

        ConsiderSettingResultDict(jData['result'], resultDict, resultKey, lock_resultDict)
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetTransactionInfo_Batched(txHashList):
    # PrintAndLog("API_GetTransactionInfo_Batched with txHashList of len " + str(len(txHashList)) + " = " + str(txHashList))

    payload_total = []
    txHashPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for txHash in txHashList:
        # Increment the id each time around
        payloadId += 1

        # Pair the txHash with the Id so I can know which is which in the response
        txHashPayloadIdDict[payloadId] = txHash

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_getTransactionByHash",
            "params": [str(txHash)]
        }
        payload_total.append(payload)

    # PrintAndLog("payload_total = " + str(payload_total))

    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_wickedLong_seconds, None, False, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTransactionInfo_Batched jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in txHashPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = txHashPayloadIdDict[id]
                    result = batchedResult['result']
                    # PrintAndLog("found Id " + str(id) + " in the response. result = " + str(result))
                    resultList.append(result)
                    break

        if len(resultList) != len(txHashList):
            raise Exception("Length of lists did not match, response data is bad?")

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetTransactionInfo_Batched response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetTransactionInfo_Batched_Safe(txHashList):
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 200  # 500 works quickly, 2000 works but noticeably slower

    txHashList_chunked = SplitListIntoChunks_OfSizeNoGreaterThanThis(txHashList, chunkSize)

    concatenatedList = []
    for i in range(len(txHashList_chunked)):
        concatenatedList += API_GetTransactionInfo_Batched(txHashList_chunked[i])
        # Sleep a bit between calls.  To give the nodes a break
        time.sleep(0.2)

    return concatenatedList


def IsAddressAContract(address):
    try:
        return API_GetCode(address)

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        pass
        # PrintAndLogError("API_GetCode failed with exception, so we're assuming this means that address"
        #                  " " + str(address) + " is not a contract. Exception = " + traceback.format_exc())

    # If we've made it this far than an exception occurred and we're assuming this is not a contract
    return False


def API_GetCode(address):
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_getCode",
        "params": [
            address,
        ]
    }

    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_seconds)
    if response.ok:
        try:
            responseData = response.content
            # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
            jData = json.loads(responseData)
            # PrintAndLog_FuncNameHeader("jData = " + str(jData))
            return jData['result']

        except:
            PrintAndLogError("exception: " + traceback.format_exc())
            pass

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_PostGetBlockNumberContainingThisTransaction(transactionId):
    # PrintAndLog("API_PostGetBlockNumberContainingThisTransaction transactionId: " + str(transactionId))

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_getTransactionByHash",
        "params": [
            transactionId
        ]
    }
    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_seconds, ResultHandlerDelegate_DetermineTransactionByHash, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        try:
            blockNumber_hex = jData['result']['blockNumber']
            PrintAndLog("blockNumber containing transaction: " + str(blockNumber_hex) + ", transactionId = " + str(transactionId))
            return blockNumber_hex

        except:
            PrintAndLogError("API_PostGetBlockNumberContainingThisTransaction could not get the block number for this transaction. str(jData) = " + str(jData))
            return None

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetTransactionCount(address, doCrossCheckWithNonceUsedDict=True,
                            resultDict=None, resultKey=None, lock_resultDict=None,
                            timeout=RequestTimeout_wickedShort_seconds):
    from Libraries.utils import ConsiderSettingResultDict

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_getTransactionCount",
        "params": [
            address,
        ]
    }
    # PrintAndLog("payload = " + str(payload))
    # None, True, False, False, True, specifiedBlockNumber_int)
    response = SendRequestToAllNodes(payload, Headers, timeout,
                                     ResultHandlerDelegate_DetermineHighestResult_int,
                                     True, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTransactionCount jData = " + str(jData))
        transactionCount = ConvertHexToInt(jData['result'])

        valueToReturn = None
        if not doCrossCheckWithNonceUsedDict:
            PrintAndLog("API_GetTransactionCount transactionCount = " + str(transactionCount) + ", just returning transactionCount")
            valueToReturn = transactionCount

        else:
            # This is really hacky and I shouldn't have to do this but i've found that sometimes my nodes and even infura will
            # return an old transactionCount despite it already being used and mined in. Could be a race condition or something.
            # This hack will protect against that.
            # So rather than just returning transactionCount here I check my own local NonceUsedDict which contains the highest known used nonce for all addresses
            highestKnownNonceUsedAndConfirmed = GetHighestKnownNonceUsedAndConfirmed(address)
            PrintAndLog(
                "API_GetTransactionCount transactionCount = " + str(transactionCount) + ", highestKnownNonceUsedAndConfirmed = " + str(highestKnownNonceUsedAndConfirmed))

            if transactionCount > highestKnownNonceUsedAndConfirmed:
                valueToReturn = transactionCount
            else:
                PrintAndLog("API_GetTransactionCount safe guarded against the node issue where transactionCount "
                            "that I got from my nodes and infura actually wasn't the highest transactionCount! "
                            "TODO, check to make sure this is working properly.")
                valueToReturn = highestKnownNonceUsedAndConfirmed + 1

        ConsiderSettingResultDict(valueToReturn, resultDict, resultKey, lock_resultDict)
        return valueToReturn

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetTransactionCount_Batched(addressList):
    # PrintAndLog_FuncNameHeader("for addressList of len " + str(len(addressList)))
    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for address in addressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the address with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        # # Remove the 0x from the tokenAddress
        # address_With0xRemoved = address.replace("0x", "")

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_getTransactionCount",
            "params": [
                address,
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))

    doRejectBatchedPayloadIfAnyOneHasError = False
    # TODO, Is this right?  Can I use ResultHandlerDelegate_DetermineHighestResult_int here??
    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_seconds,
                                     None, False, False, True, doRejectBatchedPayloadIfAnyOneHasError)
    # PrintAndLog_FuncNameHeader("response = " + str(response))
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = addressPayloadIdDict[id]
                    if 'result' in batchedResult:
                        try:
                            result = ConvertHexToInt(batchedResult['result'])
                            # PrintAndLog_FuncNameHeader("found Id " + str(id) + " in the response. result = " + str(result))
                            resultDict[id] = result
                            break
                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(addressList):
            message = "Length of lists did not match"

            if doRejectBatchedPayloadIfAnyOneHasError:
                raise Exception(message)
            else:
                PrintAndLog_FuncNameHeader(message)

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_SendRawTransaction_ToManyRemoteNodes(transactionData):
    from Libraries.nodes import RemoteNodeList_All, RemoteNodeList_BroadcastingTxsAndPendingTxsOnly
    from Libraries.bloxroute import BroadcastTxToAllBloxrouteGateways_Synchronous
    from Libraries.network import Network

    threads = []
    resultsDict = {}

    if Libraries.defaults.EnableBloxrouteWebsockets and Libraries.defaults.BroadcastTxsDirectlyToBloxrouteGateways:
        # Send to all bloxroute gateways
        key = 'bloxroute'
        t = threading.Thread(target=BroadcastTxToAllBloxrouteGateways_Synchronous, args=(transactionData, resultsDict, key,))
        threads.append(t)
        t.start()

    # Send to all standard RPC nodes
    for remoteNode in RemoteNodeList_All:
        key = str(remoteNode.name)
        t = threading.Thread(target=API_PostSendRawTransaction_StandardRPC, args=(transactionData, Get_RemoteEthereumNodeToUse(remoteNode), resultsDict, key))
        threads.append(t)
        t.start()

    # Also send to RemoteNodeList_BroadcastingTxsAndPendingTxsOnly
    for remoteNode in RemoteNodeList_BroadcastingTxsAndPendingTxsOnly:
        key = str(remoteNode.name)
        t = threading.Thread(target=API_PostSendRawTransaction_StandardRPC, args=(transactionData, Get_RemoteEthereumNodeToUse(remoteNode), resultsDict, key))
        threads.append(t)
        t.start()

    if Libraries.defaults.BroadcastTxsToEtherScan:
        # Only broadcast transactions to Etherscan on mainnet
        if Libraries.defaults.ActiveNetwork == Network.mainnet:
            # Send to Etherscan
            key_EtherScan = "etherscan"
            t = threading.Thread(target=API_PostSendRawTransaction_EtherScan, args=(transactionData, resultsDict, key_EtherScan))
            threads.append(t)
            t.start()

    for thread in threads:
        thread.join()

    # PrintAndLog_FuncNameHeader("resultsDict = " + str(resultsDict))
    # PrintAndLog_FuncNameHeader("resultsDict.values() = " + str(resultsDict.values()))

    numOfTransactionIdsInResultsDict = 0
    for key, txId in list(resultsDict.items()):
        if IsStringATransactionId(txId):
            numOfTransactionIdsInResultsDict += 1

    # PrintAndLog_FuncNameHeader("numOfTransactionIdsInResultsDict = " + str(numOfTransactionIdsInResultsDict))

    # Check for errors, only if a transaction was NOT broadcasted.
    # Sometimes some nodes will broadcast the transaction and other nodes will return an error,
    # an example of this is when a node already heard about your transaction from another node before your API call to broadcast the transaction made it.
    if numOfTransactionIdsInResultsDict == 0:
        errorToCheck = TransactionErrors_InsufficientFundsForGasPrice
        if errorToCheck.lower() in str(resultsDict.values()).lower():
            API_PostOperatorNotification(errorToCheck)
            return None, errorToCheck

        errorToCheck = TransactionErrors_NonceTooLow
        if errorToCheck.lower() in str(resultsDict.values()).lower():
            API_PostOperatorNotification(errorToCheck)
            return None, errorToCheck

        errorToCheck = TransactionErrors_NonceIsTooLow
        if errorToCheck.lower() in str(resultsDict.values()).lower():
            API_PostOperatorNotification(errorToCheck)
            return None, errorToCheck

    # If all of these responses are the same, then we can just reference the first one
    if AllListItemsAreTheSame(list(resultsDict.values())):
        PrintAndLog_FuncNameHeader("All items are the same")
        txId = list(resultsDict.values())[0]

        if IsStringATransactionId(txId):
            PrintAndLog_FuncNameHeader("Using txId = " + txId)
            return txId, None
        else:
            message = "API_PostSendRawTransaction_ToManyRemoteNodes invalid txId (A) " + str(txId)
            API_PostOperatorNotification(message)

    # All responses are not the same
    else:
        PrintAndLog_FuncNameHeader("All items are NOT the same")
        for key, txId in list(resultsDict.items()):
            if IsStringATransactionId(txId):
                PrintAndLog_FuncNameHeader("Using txId = " + txId)
                return txId, None
            else:
                message = "API_PostSendRawTransaction_ToManyRemoteNodes invalid txId (B) " + str(txId)
                API_PostOperatorNotification(message)

    return None, None


def AllListItemsAreTheSame(items):
    return all(x == items[0] for x in items)


def IsStringATransactionId(myString):
    if not myString:
        return False

    if "0x" in str(myString) and len(str(myString)) != LengthOfTransactionHash_Including0x:
        return False

    if "0x" not in str(myString) and len(str(myString)) != LengthOfTransactionHash_Excludes0x:
        return False

    return True


def API_PostSendRawTransaction_Infura(transactionData, result, key):
    from Libraries.nodes import URL_RemoteNode

    return API_PostSendRawTransaction_StandardRPC(transactionData, Get_RemoteEthereumNodeToUse(URL_RemoteNode.infura_highPriority), result, key)


def API_PostSendRawTransaction_StandardRPC(transactionData, url, result, key):
    shortName = url[0:20]
    result[key] = None
    try:
        payload = {
            "id": randint(0, 99999999999999),
            "jsonrpc": "2.0",
            "method": "eth_sendRawTransaction",
            "params": [
                "0x" + transactionData
            ]
        }
        # PrintAndLog_FuncNameHeader("shortName" + str(shortName) + ", payload = " + str(payload))
        timeout_toUse = RequestTimeout_broadcastTx_seconds
        # timeout_toUse = RequestTimeout_wickedShort_seconds
        response = requests.post(url, data=json.dumps(payload), headers=Headers, timeout=timeout_toUse)
        if response.ok:
            responseData = response.content
            jData = json.loads(responseData)
            PrintAndLog_FuncNameHeader(str(shortName) + " jData = " + str(jData))

            if 'error' in jData:
                errorMessage = jData['error']['message']
                errorCode = jData['error']['code']
                # PrintAndLog_FuncNameHeader(str(shortName) + " errorMessage: " + errorMessage + ". errorCode: " + str(errorCode))
                result[key] = jData['error']

            elif 'result' in jData:
                # PrintAndLog_FuncNameHeader(str(shortName) + " jData = " + str(jData))
                transactionId = str(jData['result'])
                result[key] = transactionId

            else:
                PrintAndLog_FuncNameHeader("No error or result in the response!")

        else:
            # If response code is not ok (200), print the resulting http error code with description
            response.raise_for_status()

    except:
        PrintAndLogError("exception = " + traceback.format_exc())
        pass

    return result


def API_PostSendRawTransaction_EtherScan(transactionData, result, key):
    global URL_EtherScan
    # PrintAndLog("API_PostSendRawTransaction_EtherScan")

    result[key] = None
    try:
        # https://api.etherscan.io/api?module=proxy&action=eth_sendRawTransaction&hex=0xf904808000831cfde080&apikey=YourApiKeyToken
        url = URL_EtherScan + "?module=proxy&action=eth_sendRawTransaction&hex=0x" + transactionData + "&apikey=" + Get_EtherScanAPIKeyToUse(True)
        response = requests.post(url, headers=Headers, timeout=RequestTimeout_broadcastTx_seconds)
        if response.ok:
            responseData = response.content
            jData = json.loads(responseData)
            PrintAndLog("API_PostSendRawTransaction_EtherScan jData = " + str(jData))

            if 'error' in jData:
                errorMessage = jData['error']['message']
                errorCode = jData['error']['code']
                # PrintAndLog("API_PostSendRawTransaction_EtherScan errorMessage: " + errorMessage + ". errorCode: " + str(errorCode))
                result[key] = jData['error']

            elif 'result' in jData:
                # PrintAndLog("API_PostSendRawTransaction_EtherScan jData = " + str(jData))
                transactionId = str(jData['result'])
                result[key] = transactionId

            else:
                PrintAndLog("No error or result in the response!")

        else:
            # If response code is not ok (200), print the resulting http error code with description
            response.raise_for_status()

    except:
        PrintAndLogError("exception = " + traceback.format_exc())
        pass

    return result


def API_GetTokenAllowance(ownerAddress, tokenAddress, spenderContractAddress, decimals,
                          resultDict=None, resultKey=None, lock_resultDict=None,
                          timeout=RequestTimeout_seconds, specifiedBlockNumber_int=None):
    from Libraries.utils import ConsiderSettingResultDict

    # PrintAndLog_FuncNameHeader("tokenAddress: " + tokenAddress + ", ownerAddress: " + ownerAddress)

    # Remove the 0x from the tokenAddress
    spenderContractAddress_With0xRemoved = spenderContractAddress.replace("0x", "")
    ownerAddress_With0xRemoved = ownerAddress.replace("0x", "")
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": "0xdd62ed3e000000000000000000000000" + ownerAddress_With0xRemoved + "000000000000000000000000" + spenderContractAddress_With0xRemoved,
                "from": ownerAddress,
                "to": tokenAddress
            },
        ]
    }
    # PrintAndLog_FuncNameHeader("payload = " + str(payload))
    response = SendRequestToAllNodes(payload, Headers, timeout, None, False, False, False, True, specifiedBlockNumber_int)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        result = jData['result']
        approvedTokenAmount_int_weiUnits = ConvertHexToInt(result)
        approvedTokenAmount_int_etherUnits = ConvertWeiToEther(approvedTokenAmount_int_weiUnits, decimals)
        # PrintAndLog_FuncNameHeader("approvedTokenAmount_int_etherUnits = " + str(approvedTokenAmount_int_etherUnits))
        ConsiderSettingResultDict(approvedTokenAmount_int_etherUnits, resultDict, resultKey, lock_resultDict)
        return approvedTokenAmount_int_etherUnits, approvedTokenAmount_int_weiUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_EstimateGas(toAddress, fromAddress, data, value=0, doAddExtraGasPadding=True,
                    multiplier=1.03, timeout=RequestTimeout_short_seconds, nonce=None,
                    resultDict=None, resultKey=None, lock_resultDict=None):
    from Libraries.utils import ConsiderSettingResultDict

    # PrintAndLog_FuncNameHeader("toAddress = " + str(toAddress))
    # PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress))
    # PrintAndLog_FuncNameHeader("data = " + str(data))
    # PrintAndLog_FuncNameHeader("value = " + str(value))

    value_hex = '0x' + ConvertIntToHex(int(value))
    # gasLimit_hex = '0x' + ConvertIntToHex(int(EthereumBlockGasLimit))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_estimateGas",
        "params": [
            {
                "data": data,
                "from": fromAddress,
                "value": value_hex,
                # "gasLimit": gasLimit_hex,  # TODO, this causes the call to fail sometimes and i'm not sure why only sometimes...
            },
        ]
    }

    if toAddress:
        payload['params'][0]['to'] = toAddress

    if nonce:
        payload['params'][0]['nonce'] = nonce

    # PrintAndLog_FuncNameHeader("payload = " + str(payload))
    # before = datetime.datetime.now()
    # Do not set requiredToWaitForAllNodesResponsesBeforeYouConsiderResults to True here because it makes this call take a really long time
    response = SendRequestToAllNodes(payload, Headers, timeout)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog_FuncNameHeader("eth_estimateGas succeeded in " + str(duration_s) + " seconds")
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        estimatedGasUsage_wei = ConvertHexToInt(jData['result'])
        # PrintAndLog_FuncNameHeader("estimatedGasUsage_wei = " + str(estimatedGasUsage_wei))
        if doAddExtraGasPadding:
            estimatedGasUsage_wei = int(estimatedGasUsage_wei * multiplier)
            # PrintAndLog_FuncNameHeader("adding some extra gas as padding, estimatedGasUsage_wei = " + str(
            #     estimatedGasUsage_wei) + " after a multiplier of " + str(multiplier) + " was used")

        ConsiderSettingResultDict(estimatedGasUsage_wei, resultDict, resultKey, lock_resultDict)
        return estimatedGasUsage_wei

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_EstimateGas_ContractDeployOnly(fromAddress, data, nonce, doAddExtraGasPadding=True, multiplier=1.05):
    from Libraries.nodes import Instance_Web3

    estimatedGasUsage_wei = Instance_Web3.eth.estimateGas({"from": fromAddress, "nonce": nonce, "data": data})
    if doAddExtraGasPadding:
        estimatedGasUsage_wei = int(estimatedGasUsage_wei * multiplier)
        # PrintAndLog_FuncNameHeader("adding some extra gas as padding, estimatedGasUsage_wei = " + str(
        #     estimatedGasUsage_wei) + " after a multiplier of " + str(multiplier) + " was used")

    return estimatedGasUsage_wei


def API_GetEtherDeltaTradeHistoryFromEtherScan_LimitedTo1000Results(fromBlock_int, toBlock_int):
    import Exchanges.etherDelta

    url = URL_EtherScan + "?module=logs&action=getLogs&address=" + Exchanges.etherDelta.Contract_Exchange + "&topic0=" + EventId_EtherDeltaTradeHash + "&fromBlock=" + str(
        fromBlock_int) + "&toBlock=" + str(toBlock_int)
    # PrintAndLog("API_GetEtherDeltaTradeHistoryFromEtherScan_LimitedTo1000Results url = " + url)

    response = requests.get(url, headers=Headers, timeout=RequestTimeout_TradeHistoryEtherScan_seconds, verify=True)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetEtherDeltaTradeHistoryFromEtherScan_LimitedTo1000Results responseData = " + responseData)
        jData = json.loads(responseData)
        # PrintAndLog("API_GetEtherDeltaTradeHistoryFromEtherScan_LimitedTo1000Results jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetEtherDeltaOrderHistoryFromEtherScan_LimitedTo1000Results(fromBlock_int, toBlock_int):
    import Exchanges.etherDelta

    url = URL_EtherScan + "?module=logs&action=getLogs&address=" + Exchanges.etherDelta.Contract_Exchange + "&topic0=" + EventId_EtherDeltasOrderHash + "&fromBlock=" + str(
        fromBlock_int) + "&toBlock=" + str(toBlock_int)
    PrintAndLog("API_GetEtherDeltaOrderHistoryFromEtherScan_LimitedTo1000Results url = " + url)

    response = requests.get(url, headers=Headers, timeout=RequestTimeout_TradeHistoryEtherScan_seconds, verify=True)
    if response.ok:
        responseData = response.content
        PrintAndLog("API_GetEtherDeltaOrderHistoryFromEtherScan_LimitedTo1000Results responseData = " + responseData)
        jData = json.loads(responseData)
        PrintAndLog("API_GetEtherDeltaOrderHistoryFromEtherScan_LimitedTo1000Results jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetEventLogs_Passthrough_callAvailableVolume_eventLogsUint_localClass_LimitedTo1000Results(fromBlock_int, toBlock_int):
    PassthroughContractAddress = "0xb38d37EFafb5B9813Ff0ba0e993D257bEa358B80"
    url = URL_EtherScan + "?module=logs&action=getLogs&address=" + PassthroughContractAddress + "&topic0=" + EventId_callAvailableVolume_eventLogsUint_localClass + "&fromBlock=" + str(
        fromBlock_int) + "&toBlock=" + str(toBlock_int)
    PrintAndLog("API_GetEventLogs_Passthrough_callAvailableVolume_eventLogsUint_localClass_LimitedTo1000Results url = " + url)

    response = requests.get(url, headers=Headers, timeout=RequestTimeout_TradeHistoryEtherScan_seconds, verify=True)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetEventLogs_Passthrough_callAvailableVolume_eventLogsUint_localClass_LimitedTo1000Results responseData = " + responseData)
        jData = json.loads(responseData)
        PrintAndLog("API_GetEventLogs_Passthrough_callAvailableVolume_eventLogsUint_localClass_LimitedTo1000Results jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def GetNonce_EtherDelta():
    import os, binascii
    nonce = binascii.b2a_hex(os.urandom(32))
    # make it 63 chars in length...
    nonce = nonce[1:]
    # nonce = "418" + str(randint(0, 9)) + "d" + str(randint(0, 9)) + "9" + str(randint(0, 9)) + "c" + \
    #         str(randint(0, 9)) + "9" + str(randint(0, 9)) + "788" + str(randint(0, 9)) + "e4b2b53c" + \
    #         str(randint(0, 9)) + "a4" + str(randint(0, 9)) + "cd7" + str(randint(0, 9)) + "f96" + \
    #         str(randint(0, 9)) + "b4facf" + str(randint(0, 9)) + "f" + str(randint(0, 9)) + "ce" + \
    #         str(randint(0, 9)) + "28" + str(randint(0, 9)) + "1d0f2" + str(randint(0, 9)) + "4e" + \
    #         str(randint(0, 9)) + "ecb"

    return nonce


def API_GetCoinMarketCap_ExchangeData(exchangeName, ethPriceUSD, doPrintDebug=False):
    url = "https://coinmarketcap.com/exchanges/" + exchangeName.lower() + "/"

    response = requests.get(url, headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        # responseData = response.content
        responseData = response.text
        # PrintAndLog("API_GetCoinMarketCap_ExchangeData responseData = " + str(responseData))

        responseData = responseData.lower()
        # indexOf_ActiveMarkets = responseData.index("active markets")
        # PrintAndLog("indexOf_ActiveMarkets = " + str(indexOf_ActiveMarkets))
        # splitString = responseData.split("https://files.coinmarketcap.com/static/img/coins")
        splitString = responseData.split("https://s2.coinmarketcap.com/static/img/coins")

        returnDict = {}
        # There should be one market's data set for each item
        for index, marketDataSet in enumerate(splitString):
            try:
                key_hrefCurrencies = 'href="/currencies/'
                indexOf_marketId = marketDataSet.find(key_hrefCurrencies)
                # PrintAndLog("indexOf_marketId = " + str(indexOf_marketId))
                # Get enough data that includes all the info we need
                approxDataLength = 600
                contents = GetSubString(marketDataSet, indexOf_marketId, approxDataLength)
                # PrintAndLog("contents = " + str(contents))
                if "24-hour" in contents.lower() and "volume" in contents.lower():
                    # Ignore this one
                    pass
                else:
                    # Extract the marketId
                    tempSplit = contents.split(key_hrefCurrencies)
                    # PrintAndLog("tempSplit = " + str(tempSplit))
                    marketId = tempSplit[1].split('/')[0]
                    # PrintAndLog("marketId = " + str(marketId))

                    # Extract the usd volume
                    key_volumeUSD = 'span class="volume" data-usd="'
                    tempSplit = contents.split(key_volumeUSD)
                    # PrintAndLog("tempSplit = " + str(tempSplit))
                    # PrintAndLog("tempSplit[0] = " + str(tempSplit[0]))
                    # PrintAndLog("tempSplit[1] = " + str(tempSplit[1]))
                    # PrintAndLog("tempSplit[2] = " + str(tempSplit[2]))
                    volumeUSD = float(tempSplit[1].split('"')[0])
                    # PrintAndLog("volumeUSD = " + str(volumeUSD))
                    volumeETH = float(volumeUSD / ethPriceUSD)
                    # PrintAndLog("volumeETH = " + str(volumeETH))

                    returnDict[marketId] = volumeUSD, volumeETH
                    if doPrintDebug:
                        message = "{0:30}{1:25}{2:15}{3:10}".format(str(marketId), "Volume last 24 hours: ", "$ " + str(volumeUSD), str(round(volumeETH, 1)) + " ETH")
                        PrintAndLog(message)

            except:
                # PrintAndLogError("exception in API_GetCoinMarketCap_ExchangeData = " + traceback.format_exc())
                pass

        return returnDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetCoinMarketCap_MarketData(currencyId, ethPriceUSD, doPrintDebug=False):
    url = "https://coinmarketcap.com/currencies/" + currencyId + "/#markets"
    # "https://coinmarketcap.com/currencies/basic-attention-token/#markets"

    response = requests.get(url, headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        # responseData = response.content
        responseData = response.text
        # PrintAndLog("API_GetCoinMarketCap_MarketData responseData = " + str(responseData))
        key_hrefCurrencies = 'href="/exchanges/'
        splitString = responseData.split(key_hrefCurrencies)
        # PrintAndLog("splitString len = " + str(len(splitString)))

        returnDict = {}
        # There should be one market's data set for each item
        for index, marketDataSet in enumerate(splitString):
            try:
                # PrintAndLog("marketDataSet = " + str(marketDataSet))
                exchangeId = marketDataSet.split("/")[0]
                # PrintAndLog("exchangeId = " + str(exchangeId))
                # thing1 = marketDataSet.split('target="_blank">')[1]
                # PrintAndLog("JOEYZ 1: " + str(thing1))
                # thing2 = thing1.split('</a></td>')
                # PrintAndLog("JOEYZ 2: " + str(thing2))
                # PrintAndLog("JOEYZ 3 len: " + str(len(thing2)))
                # pair = marketDataSet.split('target="_blank">')[1].split('<')[0]
                pair = marketDataSet.split('target="_blank">')[1].split('</a></td>')[0]
                # PrintAndLog("pair = " + str(pair))
                key_volumeUSD = '<span class="volume" data-usd="'
                tempSplit = marketDataSet.split(key_volumeUSD)
                volumeUSD = float(tempSplit[1].split('"')[0])
                # PrintAndLog("volumeUSD = " + str(volumeUSD))
                volumeETH = float(volumeUSD / ethPriceUSD)
                # PrintAndLog("volumeETH = " + str(volumeETH))

                returnDict[exchangeId] = pair, volumeUSD, volumeETH
                if doPrintDebug:
                    message = "{0:17}{1:12}{2:25}{3:15}{4:10}".format(str(exchangeId), str(pair), "Volume last 24 hours: ", "$ " + str(volumeUSD),
                                                                      str(round(volumeETH, 1)) + " ETH")
                    PrintAndLog(message)

            except:
                # PrintAndLogError("exception in API_GetCoinMarketCap_MarketData = " + traceback.format_exc())
                pass

        return returnDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetETHtoUSDPrice(lowPriority=False):
    url = URL_EtherScan + "?module=stats&action=ethprice&apikey=" + Get_EtherScanAPIKeyToUse(lowPriority)

    response = requests.post(url, headers=Headers, timeout=RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetETHtoUSDPrice jData = " + jData)
        ethUSDprice = jData['result']['ethusd']
        # PrintAndLog("API_GetETHtoUSDPrice ethUSDprice = " + ethUSDprice)
        return ethUSDprice

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetTokenTransactionsForAddress_EtherScan(address, fromBlock_int, toBlock_int):
    url = URL_EtherScan + "?module=account&action=tokentx&address=" + address + "&startblock=" + str(fromBlock_int) + "&endblock=" + str(
        toBlock_int) + "&sort=asc&apikey=" + Get_EtherScanAPIKeyToUse()
    # PrintAndLog("url = " + url)
    response = requests.get(url, headers=Headers, timeout=RequestTimeout_TradeHistoryEtherScan_seconds, verify=True)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetTokenTransactionsForAddress_EtherScan responseData = " + responseData)
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTokenTransactionsForAddress_EtherScan jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetTokenTransactionsForAddress_EtherScan(txHash):
    # URL_EtherScan = "https://api.etherscan.io/api"
    # url = 'https://etherscan.io/vmtrace?txhash=0x285a6f0df0c7e1f889ec71c7b34452d072fbc36824b7458242d95edb3f6d23b5&type=parity#raw'
    url = 'https://etherscan.io/vmtrace?txhash=' + txHash + '&type=parity#raw'
    PrintAndLog_FuncNameHeader("url = " + url)

    # Lie to etherscan to make it think we're a human
    headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'}

    response = requests.get(url, headers=headers, timeout=RequestTimeout_seconds)
    if response.ok:
        # responseText = response.text
        responseData = response.content
        PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        # jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_WaitForThisToWork_GetTransactionTrace(txId):
    header = "API_WaitForThisToWork_GetTransactionTrace: txId " + str(txId)
    before = datetime.datetime.now()
    doLoop = True
    while doLoop:
        try:
            API_GetTransactionTrace(txId)
            elapsedTime_s = (datetime.datetime.now() - before).total_seconds()
            PrintAndLog_Header(header, "Got a response! elapsedTime_s = " + str(elapsedTime_s) + " seconds")
            # If we get here, it must have worked
            doLoop = False
            txInfo = API_GetTransactionInfo(txId)
            PrintAndLog_Header(header, "txInfo = " + str(txInfo))
            txReceipt = API_GetTransactionReceipt(txId)
            PrintAndLog_Header(header, "txReceipt = " + str(txReceipt))

        except:
            PrintAndLog_Header(header, "API_GetTransactionTrace failed")

        if not doLoop:
            PrintAndLog_Header(header, "Exiting loop")
            break

        time.sleep(0.1)


def API_GetTransactionTrace(txId):
    # {"id": 1, "method": "debug_traceTransaction", "params": ["0x90d8528969192f88a3be830c17fb2cbee77cac67ec87189c0c725ebe9db96cfd", {"tracer": "callTracer", "timeout": "20m"}]}
    payload = {
        "jsonrpc": "2.0",
        "method": "debug_traceTransaction",
        "params": [
            txId,
            # {"tracer": "callTracer"},
        ]
    }
    PrintAndLog_FuncNameHeader("payload = " + str(payload))
    response = SendRequestToAllNodes(payload, Headers, RequestTimeout_wickedShort_seconds, None, False, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))
        traceDataDict = jData['result']
        PrintAndLog_FuncNameHeader("traceDataDict = " + str(traceDataDict))
        # for internalTxCall in traceDataDict['calls']:

        return traceDataDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_PostOperatorNotification(message, doRunInBackgroundThread=True, operatorNotificationKey=OperatorNotificationKey.Arby):
    if not Libraries.defaults.EnableSendingNotificationsToOperator:
        return

    if doRunInBackgroundThread:
        t = threading.Thread(
            name=threading.currentThread().getName(),  # Keep the thread name the same to help with logging consistency
            target=API_PostOperatorNotification_CurrentThread,
            args=(message, operatorNotificationKey,))
        t.start()
    else:
        API_PostOperatorNotification_CurrentThread(message, operatorNotificationKey)


def API_PostOperatorNotification_CurrentThread(message, operatorNotificationKey=OperatorNotificationKey.Arby):
    if not Libraries.defaults.EnableSendingNotificationsToOperator:
        return

    # PrintAndLog("operatorNotificationKey = " + str(operatorNotificationKey.value))
    try:
        PrintAndLog_FuncNameHeader("message = " + message)
        # data = { "value1" : "", "value2" : "", "value3" : ""}
        data = {"value1": message}
        url = 'https://maker.ifttt.com/trigger/' + str(operatorNotificationKey.value) + '/with/key/c18NHpBI3NZT5DqfOWkG1efl0QAB3VkYN-Bg9z9K4TM'
        # PrintAndLog_FuncNameHeader("JOEYZ DEBUGGING:")
        # PrintAndLog_FuncNameHeader("   data = " + str(data))
        # PrintAndLog_FuncNameHeader("   url = " + str(url))
        requests.post(url, data=data, timeout=RequestTimeout_NotificationsToOperator_seconds)

    except:
        PrintAndLogError("Exception inside API_PostOperatorNotification: " + str(traceback.format_exc()))
        PrintAndLogError(message)


def SignTransaction(to, value, privkey, nonce=0, gas_int=21000, gasPrice_int=1010000000, data_hex=""):
    from Libraries.gasStation import GasPrice_GlobalMax
    # https://ethereum.stackexchange.com/questions/40849/serializing-byte-array-for-transaction-data

    # PrintAndLog("SignTransaction comparing gasPrice_int = " + str(ConvertWeiToGwei(gasPrice_int)) + " gwei with " + str(ConvertWeiToGwei(GasPrice_GlobalMax)) + " gwei")
    if gasPrice_int > GasPrice_GlobalMax:
        API_PostOperatorNotification("SignTransaction was asked to sign a gas price that exceeds our global max. Overriding that gas price "
                                     "to be equal to the max. gasPrice_int = " + str(gasPrice_int) + ", GasPrice_GlobalMax = " + str(GasPrice_GlobalMax))
        # Enforce the max gas price
        gasPrice_int = min(gasPrice_int, GasPrice_GlobalMax)

    # PrintAndLog("SignTransaction gasPrice_int = " + str(ConvertWeiToGwei(gasPrice_int)) + " gwei")

    if not isinstance(gasPrice_int, int):
        raise Exception("gasPrice_int must be of type int")

    if gasPrice_int <= 0:
        raise Exception("gasPrice_int cannot be negative or zero")

    if not isinstance(gas_int, int):
        raise Exception("gas_int must be of type int")

    if gas_int <= 0:
        raise Exception("gas_int cannot be negative or zero")

    try:
        data_hex_with0xRemoved = data_hex.replace("0x", "")
        data = bytes.fromhex(data_hex_with0xRemoved)
        # Results in  {'error': True, 'message': ObjectSerializationError('Serialization failed because of field data ("Object is not a serializable (<class \'bytearray\'>)")',)}
        # data = bytearray.fromhex('deadbeef')
        # PrintAndLog("SignTransaction data = " + str(data))
        unsigned_transaction = transactions.Transaction(nonce, gasPrice_int, gas_int, to, value, data)
        # PrintAndLog("unsigned_transaction = " + str(unsigned_transaction))
        raw_transaction_bytes = rlp.encode(unsigned_transaction.sign(privkey))
        # PrintAndLog("raw_transaction_bytes = " + str(raw_transaction_bytes))
        raw_transaction_hex = Web3.toHex(raw_transaction_bytes)
        # PrintAndLog("raw_transaction_hex = " + str(raw_transaction_hex))
        raw_transaction_hex_0xRemoved = raw_transaction_hex.replace("0x", "")
        # PrintAndLog("raw_transaction_hex_0xRemoved = " + raw_transaction_hex_0xRemoved)
        return {'error': False, 'sign': raw_transaction_hex_0xRemoved}
    except Exception as msg:
        return {'error': True, 'message': msg}


def PadDataParemeter_LeftFillPadding(dataParameter):
    lenthPerEntry = 64
    charsToAdd = lenthPerEntry - len(dataParameter)
    padding = "0" * charsToAdd
    returnData = padding + dataParameter
    return returnData


def PadDataParemeter_RightFillPadding(dataParameter):
    lenthPerEntry = 64
    charsToAdd = lenthPerEntry - len(dataParameter)
    padding = "0" * charsToAdd
    returnData = dataParameter + padding
    return returnData


def GetExpandedScientificNotation(flt):
    str_vals = str(flt).split('e')
    coef = float(str_vals[0])
    exp = int(str_vals[1])
    return_val = ''
    if int(exp) > 0:
        return_val += str(coef).replace('.', '')
        return_val += ''.join(['0' for _ in range(0, abs(exp - len(str(coef).split('.')[1])))])
    elif int(exp) < 0:
        return_val += '0.'
        return_val += ''.join(['0' for _ in range(0, abs(exp) - 1)])
        return_val += str(coef).replace('.', '')
    return return_val


def WriteToFileAndPrintAndLog(file, text):
    file.write(text + "\n")
    PrintAndLog(text)


def GetSubString(string, startIndex, length):
    return string[startIndex:startIndex + length]


def BreakStringIntoSubStrings_GivenEqualLength(bigString, equalLength):
    return [bigString[i:i + equalLength] for i in range(0, len(bigString), equalLength)]


def InvertOrderType(action):
    # Invert, flip, reverse an action
    if action.lower() == "buy":
        return "sell"
    elif action.lower() == "sell":
        return "buy"
    else:
        raise Exception("Not buy or sell?")


def InvertTradingRole(tradingRole):
    # Invert, flip, reverse a trading role
    # Handle both the TradingRole type and the string type
    if isinstance(tradingRole, TradingRole):
        if tradingRole == TradingRole.maker:
            return TradingRole.taker
        elif tradingRole == TradingRole.taker:
            return TradingRole.maker
    elif isinstance(tradingRole, str):
        if tradingRole.lower() == TradingRole.maker.value.lower():
            return TradingRole.taker.value
        elif tradingRole.lower() == TradingRole.taker.value.lower():
            return TradingRole.maker.value
    else:
        raise Exception("Not valid tradingRole type")


def MakePriceLessCompetitiveByPercent(priceToAdjust, percentageToMove, orderType):
    return MakePriceMoreCompetitiveByPercent(priceToAdjust, -1 * percentageToMove, orderType)


def MakePriceMoreCompetitiveByPercent(priceToAdjust, percentageToMove, orderType):
    adjustedPrice = None
    priceOffset = priceToAdjust * percentageToMove
    if IsSell(orderType):
        adjustedPrice = priceToAdjust - priceOffset
    elif IsBuy(orderType):
        adjustedPrice = priceToAdjust + priceOffset

    # PrintAndLog("priceToAdjust = " + str(priceToAdjust) + ", priceOffset = " + str(priceOffset) + ", adjustedPrice = " + str(adjustedPrice))
    return adjustedPrice


def GetMostCompetitivePriceInList(pricesList, orderType):
    priceToReturn = pricesList[0]
    for price in pricesList:
        if IsBuy(orderType):
            if price > priceToReturn:
                priceToReturn = price

        elif IsSell(orderType):
            if price < priceToReturn:
                priceToReturn = price

    return priceToReturn


def GetLeastCompetitivePriceInList(pricesList, orderType):
    priceToReturn = pricesList[0]
    for price in pricesList:
        if IsBuy(orderType):
            if price < priceToReturn:
                priceToReturn = price

        elif IsSell(orderType):
            if price > priceToReturn:
                priceToReturn = price

    return priceToReturn


def ReplaceETHwithWETH(tokenName):
    if tokenName.lower() == "eth":
        return "weth"
    else:
        return tokenName


def SleepRandomTimeBetween_Seconds(min, max, doPrint=False):
    sleepTime = randint(min, max)
    if doPrint:
        PrintAndLog("Sleeping " + str(round(sleepTime, 0)) + " seconds to simulate human...")

    time.sleep(sleepTime)
    return


def SleepRandomTimeBetween_Float(min, max, doPrint=False):
    sleepTime = uniform(min, max)
    if doPrint:
        PrintAndLog("Sleeping " + str(round(sleepTime, 2)) + " seconds to simulate human...")

    time.sleep(sleepTime)
    return


def StripInvisibleCharactersFromString(myStr):
    return ''.join([x for x in myStr if x in string.printable])


def AreListsTheSameLength(listOfLists):
    runningLen = len(listOfLists[0])
    for list in listOfLists:
        if len(list) != runningLen:
            return False

    return True


def DoesDataPropertyLookLikeAnAddress(dataProperty):
    occurrenceCount_zero = dataProperty.count('0')
    # PrintAndLog("occurrenceCount_zero = " + str(occurrenceCount_zero))
    # Sometimes I see data properties from a Villain that look like this:
    # 010000000000000000000000a107dfa919c3f084a7893a260b99586981beb528
    # I'm not sure why they are encoding it this way, but it's a legit address with a 01 in the front
    # Wiggle room will help account for this header to the address
    wiggleRoom = 7
    if occurrenceCount_zero > (LengthOfDataProperty - LengthOfPublicAddress_Excludes0x + wiggleRoom):
        return False
    else:
        return True


def ConvertAddressToDataProperty(address, doLeadWith0x=False):
    returnValue = address.replace("0x", "").zfill(LengthOfDataProperty)
    if doLeadWith0x:
        returnValue = '0x' + returnValue

    return returnValue


def GetAddressFromDataProperty(dataProperty):
    return '0x' + dataProperty[-LengthOfPublicAddress_Excludes0x:]


def GetIntFromDataProperty_WeiUnits(dataProperty):
    return ConvertHexToInt(dataProperty)


def GetIntFromDataProperty_EtherUnits(dataProperty, decimals):
    return ConvertWeiToEther(GetIntFromDataProperty_WeiUnits(dataProperty), decimals)


def GetStringContentsOfDataProperty(dataProperty, assumedDecimals_forConversionToEtherUnits=None):
    if DoesDataPropertyLookLikeAnAddress(dataProperty):
        return GetAddressFromDataProperty(dataProperty)
    else:
        # We may or may not know what decimals to use to convert the units
        if assumedDecimals_forConversionToEtherUnits:
            return GetIntFromDataProperty_EtherUnits(dataProperty, assumedDecimals_forConversionToEtherUnits), GetIntFromDataProperty_WeiUnits(dataProperty)
        else:
            return GetIntFromDataProperty_WeiUnits(dataProperty)


def GetOrderType_GivenMakerTokenAndTakerToken(makerToken, takerToken):
    # This assumes that ETH is always the quote
    # OrderType perspective is based on the maker
    import Exchanges.zrx
    import Exchanges.zrxV2
    import Exchanges.kyber

    etherTokenPossibilities = [
        GetEtherContractAddress().lower(),
        Exchanges.zrx.Contract_WETH.lower(),
        Exchanges.zrxV2.Contract_WETH.lower(),
        Exchanges.kyber.EtherToken.lower()
    ]

    if makerToken.lower() in etherTokenPossibilities:
        return "buy"
    elif takerToken.lower() in etherTokenPossibilities:
        return "sell"
    else:
        raise Exception("Ether was not found as a token. This should never happen")


def ConvertDateTimeToBlockNumber(dateTime_utc, doPredictFutureBlocks=False):
    API_GetLatestBlockNumber()
    currentBlockNumber = GetThreadSafeCopyOf_LatestBlockNumber_int()

    # Edge case, if the dateTime_utc is in the future
    if dateTime_utc > datetime.datetime.utcnow():
        if doPredictFutureBlocks:
            raise Exception("Not yet implemented - doPredictFutureBlocks")
        else:
            # We cannot predict future blocks, so just return the current
            return currentBlockNumber

    threshold_return_blocks = 1
    threshold_switchStrategy_blocks = 12
    maxTries = threshold_switchStrategy_blocks + 40  # this must be greater than threshold_switchStrategy_blocks

    # Convert the datetime to block number.
    # First convert the datetime to unixEpoch time
    targetDateTime_unixEpoch = GetUnixEpochTime_FromDateTime_seconds(dateTime_utc)  # Use this value to accept input to the function
    # targetDateTime_unixEpoch = 1527203759.6099  # Use this value for debug, put in something hard coded that reproduces the result you want
    # PrintAndLog("ConvertDateTimeToBlockNumber: targetDateTime_unixEpoch = " + str(targetDateTime_unixEpoch))

    previousDiff_blocks = None
    previousDiff_blocks_float = None
    previousBlockNumber = None

    # Start using one strategy, then switch when you get close
    blockScanningStrategyToUse = BlockScanningStrategy.SubtractDiff

    # Try again with our new block number with the diff factored in until we get close enough to be happy
    index = 0
    while index < maxTries:
        if index > 0:
            # PrintAndLog("ConvertDateTimeToBlockNumber: searching blockchain to match datetime with a block number. index = " + str(
            #     index) + ", currentBlockNumber = " + str(currentBlockNumber) + ", diff_blocks = " + str(diff_blocks))

            # Set the previous before we update the current
            previousBlockNumber = currentBlockNumber

            # Update currentBlockNumber based on which strategy we're using
            if blockScanningStrategyToUse == BlockScanningStrategy.SubtractDiff:
                # There are two approaches i've tried here.  The simplest is to subtract diff_blocks from currentBlockNumber.
                # That works for most cases, but for some reason for blockchain data in 2017 that doesn't always work
                # So I came up with an alternative way that seems to work for both 2017 and 2018 and 2019.
                # I can also multiply diff_blocks by a random number.  You'll make less progress and this entire loop will have to iterate many more times than the previous strategy
                # But the randomness prevents it from getting stuck like the first strategy was getting stuck for some 2017 dates.
                # TLDR: Randomness prevents it from getting stuck, it's just a matter of what randomness

                # Approach 1.)
                # currentBlockNumber = currentBlockNumber - diff_blocks

                # Approach 2.)
                randomMultiplier = random.uniform(0.4, 0.5)
                # PrintAndLog("ConvertDateTimeToBlockNumber: multiplying diff_blocks by randomMultiplier = " + str(randomMultiplier) + " to add randomness to it so it doesn't get stuck")
                currentBlockNumber = int(currentBlockNumber - (diff_blocks * randomMultiplier))

            elif blockScanningStrategyToUse == BlockScanningStrategy.OneAtATime:
                if diff_blocks > 0:
                    currentBlockNumber -= 1
                elif diff_blocks < 0:
                    currentBlockNumber += 1
                else:
                    currentBlockNumber = currentBlockNumber

            else:
                raise Exception("Not yet implemented")

            # Set the previous before we update the current
            previousDiff_blocks = diff_blocks
            previousDiff_blocks_float = diff_blocks_float

        # PrintAndLog("ConvertDateTimeToBlockNumber: set currentBlockNumber to " + str(currentBlockNumber))
        jData = API_GetBlock(currentBlockNumber)
        blockTimestamp_unixEpoch = ConvertHexToInt(jData['timestamp'])
        # PrintAndLog("ConvertDateTimeToBlockNumber: blockTimestamp_unixEpoch = " + str(blockTimestamp_unixEpoch))
        diff_seconds = blockTimestamp_unixEpoch - targetDateTime_unixEpoch

        diff_blocks = int(round(diff_seconds / AverageBlockTime_seconds))
        diff_blocks_float = round(diff_seconds / float(AverageBlockTime_seconds))
        # PrintAndLog("ConvertDateTimeToBlockNumber: diff_blocks should be rounded = " + str(diff_blocks))
        # diff_blocks represents how many blocks we are ahead of target
        # Use the Ethereum blocktime to calculate an estimated time to jump and see if we can get closer
        # PrintAndLog("ConvertDateTimeToBlockNumber: diff_blocks = " + str(diff_blocks) + ", diff_seconds = " + str(diff_seconds))

        # Only consider exiting the loop after the first iteration
        if index > 0:
            # If we're still using the first strategy and we've met our first threshold
            if blockScanningStrategyToUse == BlockScanningStrategy.SubtractDiff and abs(diff_blocks) < threshold_switchStrategy_blocks:
                # Switch over to scanning blocks one at a time rather than this formula
                blockScanningStrategyToUse = BlockScanningStrategy.OneAtATime
                # PrintAndLog("ConvertDateTimeToBlockNumber: diff_blocks fell within our threshold_switchStrategy_blocks. Switching strategies. diff_blocks = " + str(
                #     diff_blocks) + ", threshold_switchStrategy_blocks = " + str(threshold_switchStrategy_blocks) + ", currentBlockNumber = " + str(currentBlockNumber))

            elif blockScanningStrategyToUse == BlockScanningStrategy.OneAtATime and abs(diff_blocks) < threshold_return_blocks:
                # PrintAndLog("ConvertDateTimeToBlockNumber: diff_blocks fell within our threshold_return_blocks. Returning. diff_blocks = " + str(
                #     diff_blocks) + ", threshold_return_blocks = " + str(threshold_return_blocks) + ", currentBlockNumber = " + str(currentBlockNumber))
                return currentBlockNumber

            else:
                # PrintAndLog("ConvertDateTimeToBlockNumber: diff_blocks has not yet fallen within our threshold. Keep looping")
                # Check for the stuck moving back and forth between negative and positive diff_blocks
                # To detect this, the current and previous diff_blocks will have different signs, ex: 1 positive and 1 negative
                # And the blockScanningStrategyToUse == BlockScanningStrategy.OneAtATime
                # And the previousBlockNumber and currentBlockNumber must be within exactly 1 of each other
                if ((diff_blocks > 0 and previousDiff_blocks < 0) or (diff_blocks < 0 and previousDiff_blocks > 0)) \
                        and blockScanningStrategyToUse == BlockScanningStrategy.OneAtATime \
                        and abs(previousBlockNumber - currentBlockNumber) == 1:
                    # PrintAndLog("Detected stuck position!")
                    # When this stuck position is detected, this means that no block was within exactly AverageBlockTime_seconds of this period.
                    # And there is one block ahead of it and one block behind it that are the closest matches
                    # We just need to choose the closest match between those two.
                    # note that we need to refer to diff_blocks_float here becuase it wasn't rounded to an int.  We need that precision to make the determination
                    # PrintAndLog("See which is closer to zero between diff_blocks_float = " + str(diff_blocks_float) + ", and previousDiff_blocks_float = " + str(previousDiff_blocks_float))
                    if abs(diff_blocks_float) < abs(previousDiff_blocks_float):
                        # PrintAndLog("Returning currentBlockNumber because diff_blocks_float was closer to zero")
                        return currentBlockNumber
                    else:
                        # PrintAndLog("Returning previousBlockNumber because previousDiff_blocks_float was closer to zero")
                        return previousBlockNumber

        index += 1


def GetUnixEpochTime_FromDateTime_seconds(dateTime):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dateTime - epoch).total_seconds()


def SendNotificationIfThisIsFalse(value, name):
    message = name + " is set to " + str(value) + " when Arby launched, I hope this was intentional"
    if not value:
        API_PostOperatorNotification(message)


def SendNotificationIfThisIsTrue(value, name):
    message = name + " is set to " + str(value) + " when Arby launched, I hope this was intentional"
    if value:
        API_PostOperatorNotification(message)


def MergeLists_RemovingDuplicates(first_list, second_list):
    return first_list + list(set(second_list) - set(first_list))


def RandomizeFloat(number, value_plusOrMinus, randomUniformsLow=-1, randomUniformsHigh=1):
    if randomUniformsLow < -1 or randomUniformsLow > 1:
        raise Exception("randomUniformsLow is not valid.  Must be between -1 and 1")

    if randomUniformsHigh < -1 or randomUniformsHigh > 1:
        raise Exception("randomUniformsHigh is not valid.  Must be between -1 and 1")

    valueToAdd = value_plusOrMinus * random.uniform(randomUniformsLow, randomUniformsHigh)
    returnValue = number + valueToAdd
    if returnValue < (number - value_plusOrMinus) or returnValue > (number + value_plusOrMinus):
        raise Exception("RandomizeFloat detected that returnValue was outside of valid range.  Is this function broken? returnValue = " + str(
            returnValue) + ", number = " + str(number) + ", value_plusOrMinus = " + str(value_plusOrMinus))

    return returnValue


def GetFirstXCharsInString(str, x):
    return str[:x].lower()


def GetLastXCharsInString(str, x):
    return str[-x:].lower()


def RemoveFirstXCharsInString(str, x):
    return str[x:]


def RemoveLastXCharsInString(str, x):
    return str[:-x]


def GetFunctionSelectorFromCallData(callData):
    functionSelector = GetFirstXCharsInString(callData.replace("0x", ""), 8)
    functionInputData = GetLastXCharsInString(callData, len(callData) - 10)
    return functionSelector, functionInputData


def Calculate_GasUsedOnTransaction_Ether_GivenGasPriceGweiAndGasUsedGwei(gasPrice_wei, gasUsed_wei):
    # This assumes that:
    #   gasPrice_wei comes from API_GetTransactionInfo 'gasPrice'
    #   gasUsed_wei comes from API_GetTransactionReceipt 'gasUsed'

    # PrintAndLog("Calculate_GasUsedOnTransaction_Ether_GivenGasPriceGweiAndGasUsedGwei: gasPrice_wei = " + str(gasPrice_wei))
    # PrintAndLog("Calculate_GasUsedOnTransaction_Ether_GivenGasPriceGweiAndGasUsedGwei: gasUsed_wei = " + str(gasUsed_wei))
    # Convert the gasPrice_wei to etherUnits
    # Leave the gasUsed_wei in weiUnits
    gasPrice_etherUnits = ConvertWeiToEther(gasPrice_wei, Ether_Decimals)
    gasUsedOnTransaction_Ether = gasUsed_wei * gasPrice_etherUnits

    # PrintAndLog("gasUsedOnTransaction_Ether = " + str(gasUsedOnTransaction_Ether))
    return gasUsedOnTransaction_Ether


def ConvertDateTimeToString(myDateTime):
    return str(myDateTime)


def ConvertStringToDateTime(myString):
    return datetime.datetime.strptime(myString, DateTimeFormat)


def ConvertStringToDateTime_includingMilliseconds(myString):
    return datetime.datetime.strptime(myString, DateTimeFormat_includingMilliseconds)


def ConvertUnixEpochToDateTime_GivenUnixEpochInMilliseconds(unixEpoch_milliseconds):
    return datetime.datetime.utcfromtimestamp(unixEpoch_milliseconds / 1000)


def ConvertUnixEpochToDateTime_GivenUnixEpochInSeconds(unixEpoch_seconds):
    return datetime.datetime.utcfromtimestamp(unixEpoch_seconds)


def ConvertDateTimeToUnixEpoch_Seconds(myDateTime):
    # from dateutil.tz import tzutc
    # dt_1970 = datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
    dt_1970 = datetime.datetime(1970, 1, 1, 0, 0, 0)

    return int((myDateTime - dt_1970).total_seconds())


def RoundOnlyIfNotNone(number, decimals):
    # Round the number.  But if it's none, just return it as none
    if not number:
        return number
    else:
        return round(number, decimals)


def RoundUp_NearestTen(x):
    return int(math.ceil(x / 10.0)) * 10


def GetKeyGivenSymbol_HistoricalMarketPriceData_SymbolToUsdPrice(symbol):
    return "HistoricalMarketPriceData." + symbol.upper() + "-USD"


def FormateNumberToCurrencyMoney(number):
    return '${:,.2f}'.format(number)


def FormateNumberToCurrencyMoney_WithoutDecimal(number):
    return '${:.2f}'.format(number)


def ConvertDateTimeTo_DateTimeFormat_TaxForm(myDateTime):
    return datetime.datetime.strftime(myDateTime, DateTimeFormat_TaxForm)


def DifferenceBetweenDateTimes_hours_usingABS(datetime1, datetime2):
    # PrintAndLog("DifferenceBetweenDateTimes_hours_usingABS: datetime1 = " + str(datetime1))
    # PrintAndLog("DifferenceBetweenDateTimes_hours_usingABS: datetime2 = " + str(datetime2))
    difference = datetime1 - datetime2
    difference_days, difference_seconds = difference.days, difference.seconds
    difference_hours = difference_days * 24 + (difference_seconds / 60 / 60)
    # PrintAndLog("DifferenceBetweenDateTimes_hours_usingABS: difference_hours = " + str(difference_hours))
    return abs(difference_hours)


def RemoveDuplicatesFromList(myList):
    return list(dict.fromkeys(myList))


def CountNumOfDecimalPlacesInNumber(number):
    if number < 0:
        raise Exception("This was not meant for negative numbers.")

    absoluteValue = int(number)
    decimalsOnly = number - absoluteValue
    # PrintAndLog("absoluteValue = " + str(absoluteValue))
    # PrintAndLog("decimalsOnly = " + str(decimalsOnly))
    count = 0
    while True:
        # Multiply by 10 until it's greater than 1
        if decimalsOnly == 0:
            # PrintAndLog("Breaking because decimalsOnly is == 0, decimalsOnly = " + str(decimalsOnly))
            break
        elif decimalsOnly >= 1:
            # PrintAndLog("Breaking because decimalsOnly is > 0, decimalsOnly = " + str(decimalsOnly))
            break
        else:
            # PrintAndLog("Looping decimalsOnly = " + str(decimalsOnly))
            decimalsOnly *= 10
            count += 1

    return count


def GetDictKeyGivenValue(dict, valueToMatch):
    for key, value in dict.items():
        if value == valueToMatch:
            return key


def Truncate(number, decimalPlaces):
    # Old method
    # return math.floor(number * 10 ** decimalPlace) / 10 ** decimalPlace

    # New method
    stepper = 10.0 ** decimalPlaces
    return math.trunc(stepper * number) / stepper


def GetNullAddress():
    from Libraries.nodes import Instance_Web3
    return Instance_Web3.toChecksumAddress("0x0000000000000000000000000000000000000000")


def GetEtherContractAddress():
    return GetNullAddress()


def FormatNumberWithCommas(number):
    return f'{number:,}'


def GetReturnAmountGivenTwoTradePrices(quoteAmount_etherUnits, buyPrice, sellPrice):
    # This assumes quoteAmount_etherUnits is the quoteAmount we're spending to buy the baseAmount (not needed in this formula) at the buyPrice
    # and then we turn around and immediately sell the base amount at the sellPrice
    # This function then returns the amount in quoteAmount that we receive from the sale
    # This does not factor in any trading fees!
    return float(sellPrice) / float(buyPrice) * quoteAmount_etherUnits


def CalculateTradeProfitPercentage(buyPrice, sellPrice):
    profitPercentage = 1.0 - (float(buyPrice) / float(sellPrice))
    return profitPercentage


def CalculateTradeProfit(quoteAmount, buyPrice, sellPrice):
    profitPercentage = CalculateTradeProfitPercentage(buyPrice, sellPrice)
    profit_quoteToken = profitPercentage * quoteAmount
    return profit_quoteToken


def GetFromAddressFromTxInfo(txInfo):
    return txInfo['from']


def GetToAddressFromTxInfo(txInfo):
    return txInfo['to']


def GetTxHashFromTxInfo(txInfo):
    return txInfo['hash']


def GetBlockNumberFromTxReceipt(txReceipt):
    return ConvertHexToInt(txReceipt['blockNumber'])


def GetTxIndexInBlockFromTxReceipt(txReceipt):
    return ConvertHexToInt(txReceipt['transactionIndex'])


def GetEventLogsFromTxReceipt(txReceipt):
    return txReceipt['logs']


def RemoveFirstParameterFromCallDataString(data_hex):
    return data_hex[:LengthOfFunctionHash_Including0x] + data_hex[LengthOfFunctionHash_Including0x + LengthOfTransactionHash_Excludes0x:]


def IsValidAddress(address):
    if address and isinstance(address, str) and len(address) == LengthOfPublicAddress_Including0x:
        try:
            int(address, 16)
            return True
        except ValueError:
            return False
    else:
        return False
