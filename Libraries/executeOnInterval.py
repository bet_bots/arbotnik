from Libraries.loggingConfig import RegisterLogger, GetLoggerName, PrintAndLog

import datetime
import threading

DateTimeOfLastOccurrenceDict = {}


class IntervalObject:
    key = None
    interval_seconds = None
    dateTimeOfLastOccurrence = None

    def __init__(self, _key, _interval_seconds):
        self.key = _key
        self.interval_seconds = _interval_seconds

    def MarkOccurrence(self):
        self.dateTimeOfLastOccurrence = datetime.datetime.now()

    def HasTimeSurpassedInterval(self):
        if not self.dateTimeOfLastOccurrence or ((datetime.datetime.now() - self.dateTimeOfLastOccurrence).total_seconds() > self.interval_seconds):
            return True
        else:
            return False


def RegisterKey(key, interval_seconds, doLock=True):
    global DateTimeOfLastOccurrenceDict

    DateTimeOfLastOccurrenceDict[key] = IntervalObject(key, interval_seconds)


def RemoveKey(key):
    global DateTimeOfLastOccurrenceDict

    del DateTimeOfLastOccurrenceDict[key]


def IsTimeToExecute_ReturnThread(key, interval_seconds_provideOnlyWhenRegisteringNew, functionToCallOnExecute=None,
                                 doExecuteOnFirstKeyRegister=True, doCreateUniqueLogFileForThisThreadsContents=False, *functionArguments):
    global DateTimeOfLastOccurrenceDict

    # PrintAndLog("IsTimeToExecute: DateTimeOfLastOccurrenceDict = " + str(DateTimeOfLastOccurrenceDict))
    # PrintAndLog("IsTimeToExecute: key = " + str(key))

    returnValue = False
    # Returning True means that it's time to execute on this time interval
    # Returning False means that it's not time to execute on this time interval because we've executed recently within that time interval

    if key not in DateTimeOfLastOccurrenceDict:
        RegisterKey(key, interval_seconds_provideOnlyWhenRegisteringNew, False)
        # If we want to trigger on the first time the new key is registered, return True here, else return False here
        if doExecuteOnFirstKeyRegister:
            returnValue = True
        else:
            DateTimeOfLastOccurrenceDict[key].MarkOccurrence()
            returnValue = False

    else:
        if DateTimeOfLastOccurrenceDict[key].interval_seconds != interval_seconds_provideOnlyWhenRegisteringNew:
            raise Exception("DateTimeOfLastOccurrenceDict's key " + str(key) + " has changed from " + str(
                DateTimeOfLastOccurrenceDict[key].interval_seconds) + " to " + str(
                interval_seconds_provideOnlyWhenRegisteringNew) + " and changing the interval is not supported. "
                                                                  "This was probably a typo or a bug on your part. Raising an exception for awareness.")

        if not DateTimeOfLastOccurrenceDict[key].HasTimeSurpassedInterval():
            returnValue = False
        else:
            returnValue = True

    # PrintAndLog("returnValue = " + str(returnValue))
    # PrintAndLog("functionToCallOnExecute = " + str(functionToCallOnExecute))
    # PrintAndLog("callFunctionInBackgroundThread = " + str(callFunctionInBackgroundThread))
    t = None
    if returnValue:
        DateTimeOfLastOccurrenceDict[key].MarkOccurrence()
        if functionToCallOnExecute:
            # This supports passing in arguments to the functionToCallOnExecute
            argumentsToPassToThread = ()
            if functionArguments:
                argumentsToPassToThread = functionArguments

            if doCreateUniqueLogFileForThisThreadsContents:
                threadName = GetLoggerName(key)
                # RegisterLogger so we can categorize each logged item based on threadName
                RegisterLogger(threadName)
                # pass the threadName to the thread so it can write contents to the correct log
                t = threading.Thread(name=threadName, target=functionToCallOnExecute, args=argumentsToPassToThread)
                t.start()
            else:
                t = threading.Thread(target=functionToCallOnExecute, args=argumentsToPassToThread)
                t.start()

    return returnValue, t


def IsTimeToExecute(key, interval_seconds_provideOnlyWhenRegisteringNew, functionToCallOnExecute=None,
                    callFunctionInBackgroundThread=False, doExecuteOnFirstKeyRegister=True,
                    doCreateUniqueLogFileForThisThreadsContents=False, *functionArguments):
    global DateTimeOfLastOccurrenceDict

    # PrintAndLog("IsTimeToExecute: DateTimeOfLastOccurrenceDict = " + str(DateTimeOfLastOccurrenceDict))
    # PrintAndLog("IsTimeToExecute: key = " + str(key))

    returnValue = False
    # Returning True means that it's time to execute on this time interval
    # Returning False means that it's not time to execute on this time interval because we've executed recently within that time interval

    if key not in DateTimeOfLastOccurrenceDict:
        RegisterKey(key, interval_seconds_provideOnlyWhenRegisteringNew, False)
        # If we want to trigger on the first time the new key is registered, return True here, else return False here
        if doExecuteOnFirstKeyRegister:
            returnValue = True
        else:
            DateTimeOfLastOccurrenceDict[key].MarkOccurrence()
            returnValue = False

    else:
        if DateTimeOfLastOccurrenceDict[key].interval_seconds != interval_seconds_provideOnlyWhenRegisteringNew:
            raise Exception("DateTimeOfLastOccurrenceDict's key " + str(key) + " has changed from " + str(
                DateTimeOfLastOccurrenceDict[key].interval_seconds) + " to " + str(
                interval_seconds_provideOnlyWhenRegisteringNew) + " and changing the interval is not supported. "
                                                                  "This was probably a typo or a bug on your part. Raising an exception for awareness.")

        if not DateTimeOfLastOccurrenceDict[key].HasTimeSurpassedInterval():
            returnValue = False
        else:
            returnValue = True

    # PrintAndLog("returnValue = " + str(returnValue))
    # PrintAndLog("functionToCallOnExecute = " + str(functionToCallOnExecute))
    # PrintAndLog("callFunctionInBackgroundThread = " + str(callFunctionInBackgroundThread))
    if returnValue:
        DateTimeOfLastOccurrenceDict[key].MarkOccurrence()
        if functionToCallOnExecute:
            # Have the option to call in a background thread or in the foreground
            if callFunctionInBackgroundThread:
                # This supports passing in arguments to the functionToCallOnExecute
                argumentsToPassToThread = ()
                if functionArguments:
                    argumentsToPassToThread = functionArguments

                if doCreateUniqueLogFileForThisThreadsContents:
                    threadName = GetLoggerName(key)
                    # RegisterLogger so we can categorize each logged item based on threadName
                    RegisterLogger(threadName)
                    # pass the threadName to the thread so it can write contents to the correct log
                    t = threading.Thread(name=threadName, target=functionToCallOnExecute, args=argumentsToPassToThread)
                    t.start()
                else:
                    t = threading.Thread(target=functionToCallOnExecute, args=argumentsToPassToThread)
                    t.start()

            else:
                if functionArguments:
                    functionToCallOnExecute(*functionArguments)
                else:
                    functionToCallOnExecute()

    return returnValue
