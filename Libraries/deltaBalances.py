from Libraries.loggingConfig import PrintAndLog
import Libraries.core

import json

ContractAddress_DeltaBalances = "0x39d6a3612148E5D652c4C4c56128F6305da5fdcB"


# "f851a440": "admin()",
# "cac7fc71": "allBalances(address,address,address[])",
# "6fb7fc8b": "deltaBalances(address,address,address[])",
# "2b68b9c6": "destruct()",
# "64422f3c": "multiDeltaBalances(address[],address,address[])",
# "1049334f": "tokenBalance(address,address)",
# "77a7d968": "walletBalances(address,address[])",
# "3ccfd60b": "withdraw()",
# "9e281a98": "withdrawToken(address,uint256)"


def API_GetDeltaBalances_AllBalances(exchange, user, tokensList):
    # PrintAndLog("API_GetDeltaBalances_AllBalances id = " + str(id))
    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": GetDataFor_DeltaBalances_AllBalances(exchange, user, tokensList),
                "to": ContractAddress_DeltaBalances
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetDeltaBalances responseData = " + str(responseData))
        jData = json.loads(responseData)
        data = jData['result']
        # PrintAndLog("len(data) = " + str(len(data)))

        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("dataList = " + str(dataList))
        dataList_number = []
        startingIndexOfArrayResults = 2
        indexCorrespondingWith_tokensList = 0
        maxCount_indexCorrespondingWith_tokensList = 2
        counter_indexCorrespondingWith_tokensList = 0
        for x in range(startingIndexOfArrayResults, len(dataList)):
            data = dataList[x]
            # PrintAndLog("x = " + str(x) + ", indexCorrespondingWith_tokensList = " + str(indexCorrespondingWith_tokensList) + ", where data = " + str(data))
            balance_base = Libraries.core.ConvertHexToInt(data)
            # PrintAndLog("balance_base = " + str(balance_base))
            # Convert the base to ether
            balance_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(balance_base),
                                                                          float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokensList[indexCorrespondingWith_tokensList]))))
            # PrintAndLog("balance_ether = " + str(balance_ether))
            dataList_number.append(balance_ether)

            # only increment indexCorrespondingWith_tokensList every other time since we see each token from the tokensList twice (since it's checking balances in two places)
            counter_indexCorrespondingWith_tokensList += 1
            if counter_indexCorrespondingWith_tokensList >= maxCount_indexCorrespondingWith_tokensList:
                indexCorrespondingWith_tokensList += 1
                counter_indexCorrespondingWith_tokensList = 0

        # PrintAndLog("dataList_number = " + str(dataList_number))
        return tuple(dataList_number)

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetDeltaBalances response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def GetDataFor_DeltaBalances_AllBalances(exchange, user, tokensList):
    # function allBalances(address exchange, address user,  address[] tokens) public view returns (uint[]) {
    # "cac7fc71": "allBalances(address,address,address[])",

    data = ""
    addNewLinesForDebug = False
    printDataForDebug = False

    # Method
    data += "0xcac7fc71"
    if addNewLinesForDebug:
        data += "\n"

    # exchange
    exchange = Libraries.core.PadDataParemeter_LeftFillPadding(Libraries.core.Remove0XfromHexString(exchange))
    if printDataForDebug:
        PrintAndLog("exchange: " + str(exchange))
    data += exchange
    if addNewLinesForDebug:
        data += "\n"

    # user
    user = Libraries.core.PadDataParemeter_LeftFillPadding(Libraries.core.Remove0XfromHexString(user))
    if printDataForDebug:
        PrintAndLog("user: " + str(user))
    data += user
    if addNewLinesForDebug:
        data += "\n"

    # Because the next item is an array
    # TODO, refactor using the web3 logic that handles this for me...  This number is a special case
    arrayDeclarationData = "0000000000000000000000000000000000000000000000000000000000000060"
    data += arrayDeclarationData
    if addNewLinesForDebug:
        data += "\n"

    # Declare length of array
    arrayLengthData = Libraries.core.PadDataParemeter_LeftFillPadding("%x" % int(len(tokensList)))
    data += arrayLengthData
    if addNewLinesForDebug:
        data += "\n"

    # Handle array
    for token in tokensList:
        # token
        token = Libraries.core.PadDataParemeter_LeftFillPadding(Libraries.core.Remove0XfromHexString(token))
        if printDataForDebug:
            PrintAndLog("token from tokensList: " + str(token))
        data += token
        if addNewLinesForDebug:
            data += "\n"

    if printDataForDebug:
        PrintAndLog("data: " + str(data))

    return data


def API_GetDeltaBalances_WalletBalances(user, tokensList):
    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": GetDataFor_DeltaBalances_WalletBalances(user, tokensList),
                "to": ContractAddress_DeltaBalances
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetDeltaBalances_WalletBalances responseData = " + str(responseData))
        jData = json.loads(responseData)
        data = jData['result']
        # PrintAndLog("len(data) = " + str(len(data)))

        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("dataList = " + str(dataList))
        dataList_number = []
        startingIndexOfArrayResults = 2
        indexCorrespondingWith_tokensList = 0
        for x in range(startingIndexOfArrayResults, len(dataList)):
            data = dataList[x]
            balance_base = Libraries.core.ConvertHexToInt(data)
            # Convert the base to ether
            balance_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(balance_base),
                                                                          float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokensList[indexCorrespondingWith_tokensList]))))
            dataList_number.append(balance_ether)
            indexCorrespondingWith_tokensList += 1

        # PrintAndLog("dataList_number = " + str(dataList_number))
        return dataList_number

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetDeltaBalances_WalletBalances response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def GetDataFor_DeltaBalances_WalletBalances(user, tokensList):
    # "77a7d968": "walletBalances(address,address[])",

    data = ""
    addNewLinesForDebug = False
    printDataForDebug = False

    # Method
    data += "0x77a7d968"
    if addNewLinesForDebug:
        data += "\n"

    # user
    user = Libraries.core.PadDataParemeter_LeftFillPadding(Libraries.core.Remove0XfromHexString(user))
    if printDataForDebug:
        PrintAndLog("user: " + str(user))
    data += user
    if addNewLinesForDebug:
        data += "\n"

    # Because the next item is an array
    # TODO, refactor using the web3 logic that handles this for me...  This number is a special case
    arrayDeclarationData = "0000000000000000000000000000000000000000000000000000000000000040"
    data += arrayDeclarationData
    if addNewLinesForDebug:
        data += "\n"

    # Declare length of array
    arrayLengthData = Libraries.core.PadDataParemeter_LeftFillPadding("%x" % int(len(tokensList)))
    data += arrayLengthData
    if addNewLinesForDebug:
        data += "\n"

    # Handle array
    for token in tokensList:
        # token
        token = Libraries.core.PadDataParemeter_LeftFillPadding(Libraries.core.Remove0XfromHexString(token))
        if printDataForDebug:
            PrintAndLog("token from tokensList: " + str(token))
        data += token
        if addNewLinesForDebug:
            data += "\n"

    if printDataForDebug:
        PrintAndLog("data: " + str(data))

    return data
