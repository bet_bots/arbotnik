import copy
import datetime
import json
import random
import secrets
import string
import threading
import time
import traceback
from enum import Enum
from threading import Lock
from web3 import Web3

import Exchanges.bancor
import Exchanges.ercdex
import Exchanges.etherDelta
import Exchanges.kyber
import Exchanges.oasisDex
import Exchanges.ocean
import Exchanges.set
import Exchanges.uniswap
import Exchanges.uniswapV2
import Exchanges.sushiswap
import Exchanges.defiswap
import Exchanges.sakeswap
import Exchanges.balancer
import Exchanges.zrx
import Exchanges.zrxV2
import Exchanges.zrxV4
import Exchanges.keeperDAO
import Exchanges.ninja
import Exchanges.curve
import Libraries.blackListManager
import Libraries.core
import Libraries.utils
import Libraries.arbyUtility
import Libraries.executeOnInterval
import Libraries.orderIntent
import Libraries.cache
import Libraries.nodes
import Libraries.signingUtils
import Libraries.gasStation
import Libraries.node
import Libraries.exceptions
import Libraries.ratesGraph
import Libraries.priceOracle
import Libraries.config
import Libraries.defaults
from Exchanges.ninja_tailgating import PendingTradeTx_Uniswap, PendingTradeTx_UniswapV2
from Libraries.pricesDict import *
from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader, PrintAndLogError, PrintAndLog_Detailed, PrintAndLog_Header
from Libraries.trade import GetMarketPriceTechnique_WithinOneSingleExchange, IsValidPrice
from Libraries.transactions import API_SendEther_ToContract, TransactionType, API_SendTokens_ToAddress, API_SendEther_ToAddress
from Libraries.executeOnInterval import IsTimeToExecute

ExchangeName_0x = "0x"
ExchangeName_0xv1 = "0xv1"
ExchangeName_0xv2 = "0xv2"
ExchangeName_0xMesh = "0xMesh"
ExchangeName_HidingBook = "HidingBook"
# ExchangeName_0xv4OpenOrderbook = "0xv4OpenOrderbook"
ExchangeName_RadarRelay = "RadarRelay"
ExchangeName_RadarRelay2 = "RadarRelay2"
ExchangeName_Bancor = "Bancor"
ExchangeName_OasisDex = "OasisDex"
ExchangeName_OasisDex_Old1 = "OasisDex_Old1"
ExchangeName_OasisDex_Old2 = "OasisDex_Old2"
ExchangeName_Kyber = "Kyber"
ExchangeName_Uniswap = "Uniswap"
ExchangeName_UniswapV2 = "UniswapV2"
ExchangeName_Sushiswap = "Sushiswap"
ExchangeName_Defiswap = "Defiswap"
ExchangeName_Sakeswap = "Sakeswap"
ExchangeName_Set = "Set"
ExchangeName_Balancer = "Balancer"
ExchangeName_Curve = "Curve"

BlacklistedOrderManager = Libraries.blackListManager.BlacklistedOrderManagerObject()

# Dicts contain pricing information for various smart contract based DXs
ArbyContractPricesDict_Bancor = {}
Lock_ArbyContractPricesDict_Bancor = Lock()
ArbyContractPricesDict_Kyber = {}
Lock_ArbyContractPricesDict_Kyber = Lock()
ArbyContractPricesDict_Uniswap = {}
Lock_ArbyContractPricesDict_Uniswap = Lock()

# Keyed by exchangeName, this is a list of tokens the exchange trade
ExchangeTokensDictWithValueList = {}

# This gets set below near where ExchangeDict is declared
ExchangeNamesList_Alphabetized = None


class Exchange_Decentralized:
    exchangeName = None
    # Some exchanges can be made more efficient by caching data to avoid reprocessing data repeatedly
    pricesCache = None

    def __init__(self, _exchangeName):
        self.exchangeName = _exchangeName
        self.pricesCache = {}

    def GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_OpeningBid(self):
        return Libraries.defaults.Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_OpeningBid_BaseValue

    def GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxKeepAhead(self):
        return Libraries.defaults.Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxKeepAhead_BaseValue

    def GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxGrimTrigger(self):
        return Libraries.defaults.Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxGrimTrigger_BaseValue

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        raise Exception("Not yet implemented for " + str(self.exchangeName))

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        raise Exception("Not yet implemented for " + str(self.exchangeName))

    # Some protocols are only available to trade under certain circumstances (Set for example, must be in rebalance mode)
    def IsAvailableToTrade(self):
        return True

    # This is True for things like 0x where there is an offchain orderbook and new orders can get added at any time.
    def PriceDataCanChangeAtAnyTimeEvenBetweenBlocks(self):
        return False

    # This is True for things like Uniswap where the price is a ratio of two tokens in the exchange.
    # This is False for things like 0x where there is an offchain orderbook and one order's price does not effect another order's price
    def IncomingTradesAffectThePriceOfTheExchange(self):
        return True

    def CanQueueMultipleOrdersWithoutAffectingPrice(self):
        return not self.IncomingTradesAffectThePriceOfTheExchange()

    def IsOffchainOrderbookBased(self):
        return False

    def SetMemoryCacheFromFileCache(self):
        return

    # Delete all exchanges from memory cache that we do not care about
    # This improves performance
    def RestrictMemoryCacheToOnlyTheseTokens(self, quoteTokensList, allTokensList):
        return

    def GetPrices(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber,
                  resultDict, resultKey, lock_resultDict, detailed=False):
        returnValue = "TODO " + str(self.exchangeName) + " " + str(quoteToken) + " " + str(quoteTokensToSpendList_etherUnits)
        Libraries.utils.ConsiderSettingResultDict(returnValue, resultDict, resultKey, lock_resultDict)

    def GenerateCallData_Check(self, arbitrageOpportunity, pathIndex, tradingTechnique):
        raise Exception("Not yet implemented for " + str(self.exchangeName))

    def GenerateCallData_Trade(self, arbitrageOpportunity, pathIndex):
        raise Exception("Not yet implemented for " + str(self.exchangeName))

    @staticmethod
    def GenerateCallData_Trades_Generic(arbitrageOpportunity):
        from Contracts.contracts import Contract_Ninja_Trade_3
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        exchangeSpecificTradeCallDataArrays = []
        for pathIndex, local_exchangeName in enumerate(arbitrageOpportunity.exchangeNamesList):
            exchangeSpecificTradeCallData2dArray = ExchangeDict[local_exchangeName].GenerateCallData_Trade(arbitrageOpportunity, pathIndex)
            # Using the first index hard coded for now, in the future I could use more but that logic is a bit complicated and will require a refactor
            exchangeSpecificTradeCallDataArrays.append(exchangeSpecificTradeCallData2dArray[0])

        PrintAndLog_FuncNameHeader("Generating tradeWithinFlashLoan calldata based on len(exchangeSpecificTradeCallDataArrays) = " + str(
            len(exchangeSpecificTradeCallDataArrays)))

        doWrapEthBeforeFirstTrade, doUnwrapEthAfterLastTrade, quoteTokenAddressToUse, dontCare = arbitrageOpportunity.Prepare_Trade()

        functionName = None
        kwargs = None
        if arbitrageOpportunity.GetBool_DoTradeWithinFlashLoan():
            #     function tradeWithinFlashLoan(
            #         uint256 borrowedQuoteTokens,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
            #         address quoteToken,
            #         uint256[][] memory exchangeSpecificTradeCallDataArrays,
            #         bool doWrapEthBeforeFirstTrade,
            #         bool doUnwrapEthAfterLastTrade
            #         ) public onlyWhitelistedKeeperDAOTransferProxys
            functionName = 'tradeWithinFlashLoan'
            kwargs = {
                'borrowedQuoteTokens': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
                'quoteToken': Libraries.nodes.Instance_Web3.toChecksumAddress(quoteTokenAddressToUse),
                'exchangeSpecificTradeCallDataArrays': exchangeSpecificTradeCallDataArrays,
                'doWrapEthBeforeFirstTrade': doWrapEthBeforeFirstTrade,
                'doUnwrapEthAfterLastTrade': doUnwrapEthAfterLastTrade,
            }
        else:
            #     function tradeWithoutFlashLoan(
            #         uint256 quoteTokenQuantity,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
            #         address quoteToken,
            #         uint256[][] memory exchangeSpecificTradeCallDataArrays,
            #         bool doWrapEthBeforeFirstTrade,
            #         bool doUnwrapEthAfterLastTrade
            #         ) public onlyNinjaProxy
            functionName = 'tradeWithoutFlashLoan'
            kwargs = {
                'quoteTokenQuantity': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
                'quoteToken': Libraries.nodes.Instance_Web3.toChecksumAddress(quoteTokenAddressToUse),
                'exchangeSpecificTradeCallDataArrays': exchangeSpecificTradeCallDataArrays,
                'doWrapEthBeforeFirstTrade': doWrapEthBeforeFirstTrade,
                'doUnwrapEthAfterLastTrade': doUnwrapEthAfterLastTrade,
            }

        tradeCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Trade_3, functionName, True, True)
        return [tradeCallDataArray_encoded]

    def FinalizeMetaDataBeforeTrade(self, arbitrageOpportunity, pathIndex):
        # Most exchanges will not want to use this, so default to this
        return 1

    # Some exchanges require ETH as a quote token,
    # which means that if you want to trade token for token you must make two trades (sell one token for ETH, then use that ETH to buy another token)
    # This flag is True when ETH is always the quote token on this exchange
    def DirectQuoteTokenIsRestrictedToEthOnly(self):
        return False

    def EnforceEligibleQuoteToken(self, quoteToken, doRaiseExceptionWhenExchangeNotSupported=True):
        if self.DirectQuoteTokenIsRestrictedToEthOnly():
            return self.RequireThatEthIsTheOnlyDirectlySupportedQuoteToken(quoteToken, doRaiseExceptionWhenExchangeNotSupported)

        # If we've made it this far, this is an eligible quoteToken
        return True

    def RequireThatEthIsTheOnlyDirectlySupportedQuoteToken(self, quoteToken, doRaiseExceptionWhenExchangeNotSupported=True):
        if quoteToken.lower() == Exchanges.keeperDAO.EthTokenContract.lower():
            # If we've made it this far, this is an eligible quoteToken
            return True
        else:
            if doRaiseExceptionWhenExchangeNotSupported:
                # Raise an exception if we're trading an invalid quote token
                raise Exception("This exchange(" + str(self.exchangeName) + ") only works with ETH as the quote token! "
                                                                            "quoteToken provided as " + str(quoteToken))
            else:
                # Return false here because ETH is not the quoteToken
                return False

    def SendMessageRegarding_Ninja_ForcePassSimpleRequirements(self):
        if IsTimeToExecute(self.exchangeName + "_" + "SendMessageRegarding_Ninja_ForcePassSimpleRequirements", 60, None, True, True):
            Libraries.core.API_PostOperatorNotification("Ninja_ForcePassSimpleRequirements is enabled and " + str(
                self.exchangeName) + " just used it to force simple requirements to pass. I hope this was intentional!")

    def GetTradableTokens(self):
        raise Exception("Not yet implemented for " + str(self.exchangeName))

    # Some protocols enforce a max gasPrice. Most of them; however, do not
    def EnforceMaxGasPrice(self, gasPrice):
        return gasPrice

    def PrintAndLogError_ACustomGetPricesException(self, stackTraceFromTraceback, quoteToken, baseToken, quoteTokensToSpend_etherUnits=None):
        string_quoteTokensToSpend_etherUnits = ''
        if quoteTokensToSpend_etherUnits:
            string_quoteTokensToSpend_etherUnits = str(quoteTokensToSpend_etherUnits)

        PrintAndLogError("exception when getting exchange prices for " + str(self.exchangeName) + ", quoteToken = " + str(
            quoteToken) + ", baseToken = " + str(baseToken) + ", quoteTokensToSpend_etherUnits = " + str(
            string_quoteTokensToSpend_etherUnits) + ", " + str(stackTraceFromTraceback))

    # True if it's possible to arbitrage within this exchange.  Example: 0x, yes because it's orderbook based.  Uniswap, no.
    def CanArbitrageWithinThisOneSingleExchange(self):
        return False

    # Uniswap supports ETH, UniswapV2 does not, Kyber supports ETH, 0x does not, etc
    def SupportTradingEthDirectlyWithoutBeingWrapped(self):
        return True

    # Return a list of spender proxy addresses we need to approve token transfers for this exchange
    def GetTokenSpenderProxys(self, tokenToApprove):
        raise Exception("Not yet implemented")

    # Some exchange trades result in a loss based on quoteTokens, but will make profit in baseTokens due to dust
    # For those exchanges, we want to allow for some loss because we expect to make profits in baseTokens
    # In a perfect world, the smart contract would either calculate that baseToken profit and convert to quoteTokens or
    # just liquidate those baseTokens but that's a lot of wasted gas that makes the trade inefficient
    # So the simplest way is to just keep the profit in baseTokens and accept a small loss in quoteTokens
    # Most exchanges should not have dust at all, and this function can simply return 0
    def GetMaxQuoteTokenTradeLossAcceptable_etherUnits(self, quoteToken):
        return 0

    def GetExpectedGasUsage_Check(self):
        raise Exception("Not yet implemented")

    def GetExpectedGasUsage_Trade(self):
        raise Exception("Not yet implemented")

    # Some protocols charge trading fees, like 0x's protocol fee
    def CalculateTradeFee_eth(self, gasPrice):
        return 0

    def GetEventLogsTradeTopics(self):
        raise Exception("Not yet implemented")

    def DoEventLogsContainInteractionWithThisExchange(self, eventLogsList):
        for topic in self.GetEventLogsTradeTopics():
            if topic.lower() in str(eventLogsList).lower():
                return True

        # If we've made it this far, we can safely assume False
        return False

    # Get general exchange info at this block, given txReceipt
    def GetInfoAtBlockNumber(self, local_blockNumber, tokenAddressesToTraceList,
                             txReceipt, txReceiptList_forThisBlockNumber, textToPrepend):
        raise Exception("Not yet implemented")

    # Some exchanges are extremely efficient in getting mass trade prices for many trade quantities
    # Some are not. For ones that are not, we want to minimize the number of calls eth_calls we make to the nodes
    def IsEfficientToGetMassPricesForManyTradeQuantities(self):
        return True

    # Return an array of strings where the strings are things I should look for in tx calldata when front running villains
    # For example, with Uniswap I may want to look up the uniswap exchange address based on the baseToken
    def GetMempoolAnalysisStrings(self, baseToken):
        return []

    # # Stringify the metadata
    # def GetUniqueMetaDataString(self, arbitrageOpportunity, pathIndex):
    #     return None

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def PrepareRateFormulaMetaData(self, arbitrageOpportunity, pathIndex):
        raise Exception("Not yet implemented")

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def GetRateFormulaMetaData(self, resultForApiCall, arbitrageOpportunity, pathIndex, metaDataResultDict, key):
        raise Exception("Not yet implemented")

    # Assume the same metaData structure as set by GetRateFormulaMetaData for this exchange
    # calculate the rate (not price) when trading according to this metaData on this exchange
    def GetRate_FinalizeQuoteTokenAmount(self, arbitrageOpportunity, pathIndex, inFlowAmount, metaData,
                                         tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        raise Exception("Not yet implemented")

    # Return True if this exchange is a candidate for Tailgating
    def CanTailgate(self):
        return False

    # These are addresses I want to monitor in the mempool for txs
    def GetTailgatingWatchAddresses(self):
        return []

    def CreatePendingTradeTx(self, mempoolTx):
        raise Exception("Not yet implemented")


class Exchange_0x_LowerAbstraction(Exchange_Decentralized):

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        # PrintAndLog_Header(header_logging, "returning metaDataIdentifier = " + str(metaDataIdentifier))
        return metaDataIdentifier

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        header_logging = "GetMetaDataIdentifier(" + str(self.exchangeName) + "): "

        arbitragableOrder = arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex]['arbitragableOrder']
        orderHash = arbitragableOrder.hash.lower()
        PrintAndLog_Header(header_logging, "Returning orderHash " + str(orderHash))
        return orderHash

    # This is True for things like Uniswap where the price is a ratio of two tokens in the exchange.
    # This is False for things like 0x where there is an offchain orderbook and one order's price does not effect another order's price
    def IncomingTradesAffectThePriceOfTheExchange(self):
        return False

    # Keep in mind, this means that orders are signed and listed offchain, which means new arbitrage can become available between blocks
    def IsOffchainOrderbookBased(self):
        return True

    # True if it's possible to arbitrage within this exchange.  Example: 0x, yes because it's orderbook based.  Uniswap, no.
    def CanArbitrageWithinThisOneSingleExchange(self):
        return True

    # TODO I have to leave this as true here because of the 0x protocol fee...
    #  For now just let it trade with ETH so that the profit offsets the protocol fee
    # Uniswap supports ETH, UniswapV2 does not, Kyber supports ETH, 0x does not, etc
    def SupportTradingEthDirectlyWithoutBeingWrapped(self):
        return False

    # Return a list of spender proxy addresses we need to approve token transfers for this exchange
    def GetTokenSpenderProxys(self, tokenToApprove):
        return [Exchanges.zrxV2.Contract_ERC20Proxy]

    @staticmethod
    def GetProper0xTokenAddress(tokenAddress):
        # For 0x based exchanges, ETH = WETH, so treat it as such
        if tokenAddress.lower() == Exchanges.keeperDAO.EthTokenContract.lower():
            return Exchanges.zrxV2.Contract_WETH
        else:
            return tokenAddress

    def GetExpectedGasUsage_Check(self):
        return 23500

    def GetExpectedGasUsage_Trade(self):
        return 158569


class Exchange_0x(Exchange_0x_LowerAbstraction):
    todo = None


class Exchange_0xv2(Exchange_0x_LowerAbstraction):

    def API_VerifyValidityOfOrders(self, arbitragableOrdersList):
        raise Exception("Not yet implemented")

    # TODO, move this somewhere else??
    @staticmethod
    def IsValidHashSignature(orderHash, signerAddress, signature):
        import Contracts.contracts

        PrintAndLog("IsValidHashSignature: orderHash = " + str(orderHash))
        PrintAndLog("IsValidHashSignature: signerAddress = " + str(signerAddress))
        PrintAndLog("IsValidHashSignature: signature = " + str(signature))
        kwargs = {
            'hash': Web3.toBytes(hexstr=str(orderHash)),
            'signerAddress': Libraries.nodes.Instance_Web3.toChecksumAddress(signerAddress),
            'signature': Web3.toBytes(hexstr=str(signature)),
        }

        payload = {
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_0xv3.encodeABI('isValidHashSignature', kwargs=kwargs),
                    "to": Exchanges.zrxV2.Contract_Exchange
                },
            ]
        }
        response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
        if response.ok:
            responseData = response.content
            PrintAndLog("IsValidHashSignature Exchange_0xv2 responseData = " + str(responseData))
            jData = json.loads(responseData)
            # PrintAndLog("IsValidHashSignature Exchange_0xv2 jData = " + str(jData))
            return Libraries.core.ConvertHexToBool(jData['result'])

        else:
            # If response code is not ok (200), print the resulting http error code with description
            PrintAndLog("IsValidHashSignature Exchange_0xv2 response was not ok response = " + str(response))
            response.raise_for_status()


class Exchange_0xMesh(Exchange_0xv2):

    def API_VerifyValidityOfOrders(self, arbitragableOrdersList):
        return Exchanges.zrxV2.API_VerifyValidityOfOrders(arbitragableOrdersList)

    def GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_OpeningBid(self):
        return Libraries.defaults.Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_OpeningBid_0x

    def GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxKeepAhead(self):
        return Libraries.defaults.Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxKeepAhead_0x

    def GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxGrimTrigger(self):
        return Libraries.defaults.Ninja_PercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxGrimTrigger_0x

    def GetTradableTokens(self):
        # Get the data from the cache, but if the cache is old then make the API call first
        key = 'Exchanges.zrxApi.API_GetListOfTokenAddresses'
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        tradeableTokensList = Libraries.cache.GetDataFromCache(key)
        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)

    def GetPrices(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber,
                  resultDict, resultKey, lock_resultDict, detailed=False):
        from Libraries.market import GetTokenObjectGivenTokenAddress
        from Libraries.quoteTokenProcesses import API_Update0xOrderAggregators_FromMainProcess

        self.EnforceEligibleQuoteToken(quoteToken)

        header_logging = Libraries.loggingConfig.GenerateHeaderForPrintAndLog(self.exchangeName, blockNumber)
        functionsStartDateTime = datetime.datetime.now()

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 1, Begin on with quoteToken " + str(quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        # Since the orderAggregators are on the main process, we have to make a call to the main process to get orderAggregator data
        API_Update0xOrderAggregators_FromMainProcess(quoteToken)

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 2, iterating over all 0x orders")

        returnValue = {}
        returnValue[pdk_baseTokens] = {}
        for index_baseToken, baseToken in enumerate(baseTokenList):
            try:
                returnValue[pdk_baseTokens][baseToken] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_prices] = {}

                # Get all orders from the OrderAggregators
                token = GetTokenObjectGivenTokenAddress(baseToken)
                # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "getting orderbook data for quoteToken " + str(
                #         quoteToken) + ", baseToken " + str(baseToken) + ", token object = " + str(
                #         token.tokenName) + ", self.exchangeName = " + str(self.exchangeName))

                # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                #     before = datetime.datetime.now()
                orderDict_bids_0x = token.GetCopyOfAllOrders_Bids_FromTheseOrderAggregators(quoteToken, True, [self.exchangeName])
                orderDict_asks_0x = token.GetCopyOfAllOrders_Asks_FromTheseOrderAggregators(quoteToken, True, [self.exchangeName])
                # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                #     duration_s = (datetime.datetime.now() - before).total_seconds()
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "orderDict_bids_0x of len " + str(len(orderDict_bids_0x)) + " = " + str(orderDict_bids_0x))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "orderDict_asks_0x of len " + str(len(orderDict_asks_0x)) + " = " + str(orderDict_asks_0x))

                # Convert the dicts to lists that are sorted by price, most competitive price first
                orderList_bids_0x = list(orderDict_bids_0x.values())
                orderList_asks_0x = list(orderDict_asks_0x.values())
                orderList_bids_0x.sort(key=lambda order: order.GetPrice(), reverse=True)
                orderList_asks_0x.sort(key=lambda order: order.GetPrice(), reverse=False)
                # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Sorted list of orderList_bids_0x")
                #     for order in orderList_bids_0x:
                #         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   bids order.price = " + str(order.GetPrice()))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Sorted list of orderList_asks_0x")
                #     for order in orderList_asks_0x:
                #         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   asks order.price = " + str(order.GetPrice()))
                #
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "orderDict_bids_0x has " + str(len(orderDict_bids_0x)) + " orders for baseToken " + str(
                #         baseToken) + ", duration_s = " + str(duration_s))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "orderDict_asks_0x has " + str(len(orderDict_asks_0x)) + " orders for baseToken " + str(
                #         baseToken) + ", duration_s = " + str(duration_s))

                # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                #     before = datetime.datetime.now()
                for index, quoteTokensToSpend_etherUnits in enumerate(quoteTokensToSpendList_etherUnits):
                    try:
                        # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                        # iterate over all the orders and find the best buy and sell prices that facilitate that trade size of quoteTokensToSpend_etherUnits

                        # Because the order lists are sorted by price, we can iterate over the order list and
                        # the first one we find that meets our requirements, we can break from the loop
                        bestBidPrice = ''
                        for order in orderList_bids_0x:
                            # TODO I will need to say order.GetTokenQuantity(quoteToken) here
                            # If the order wasn't signed with enough quantity
                            # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "order.GetEtherQuantity() bid = " + str(order.GetEtherQuantity()))
                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokensToSpend_etherUnits bid = " + str(quoteTokensToSpend_etherUnits))
                            if order.GetEtherQuantity() < quoteTokensToSpend_etherUnits:
                                continue

                            # Require that the order is fillable
                            if not self.IsOrderFillable(order, quoteTokensToSpend_etherUnits):
                                continue

                            price = order.GetPrice()
                            # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Examining bid " + str(price) + " timeRemaining = " + str(
                            #         order.GetDurationRemaining_seconds()) + ", order.hash = " + str(order.hash))
                            # TODO enforce no dust orders, but I don't want to prevent myself from seeing typo orders that appear dusty... So this may be a bit tricky to do properly
                            if not bestBidPrice or price > bestBidPrice:
                                bestBidPrice = price
                                # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Setting new best price bestBidPrice = " + str(
                                #         bestBidPrice) + ", order.GetEtherQuantity() = " + str(
                                #         order.GetEtherQuantity()) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits) + ", order.hash = " + str(order.hash))
                                # I can break here because I've sorted the list by price
                                break

                        # Because the order lists are sorted by price, we can iterate over the order list and
                        # the first one we find that meets our requirements, we can break from the loop
                        bestAskPrice = ''
                        for order in orderList_asks_0x:
                            # TODO I will need to say order.GetTokenQuantity(quoteToken) here
                            # If the order wasn't signed with enough quantity
                            # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "order.GetEtherQuantity() ask = " + str(order.GetEtherQuantity()))
                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokensToSpend_etherUnits ask = " + str(quoteTokensToSpend_etherUnits))
                            if order.GetEtherQuantity() < quoteTokensToSpend_etherUnits:
                                continue

                            # Require that the order is fillable
                            if not self.IsOrderFillable(order, quoteTokensToSpend_etherUnits):
                                continue

                            price = order.GetPrice()
                            # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Examining ask " + str(price) + " timeRemaining = " + str(
                            #         order.GetDurationRemaining_seconds()) + ", order.hash = " + str(order.hash))
                            # TODO enforce no dust orders, but I don't want to prevent myself from seeing typo orders that appear dusty... So this may be a bit tricky to do properly
                            if not bestAskPrice or price < bestAskPrice:
                                bestAskPrice = price
                                # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Setting new best price bestAskPrice = " + str(
                                #         bestAskPrice) + ", order.GetEtherQuantity() = " + str(
                                #         order.GetEtherQuantity()) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits) + ", order.hash = " + str(order.hash))
                                # I can break here because I've sorted the list by price
                                break

                        returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)] = {
                            pdk_quoteTokensToSpend_etherUnits: quoteTokensToSpend_etherUnits,
                            pdk_bid: bestBidPrice,
                            pdk_ask: bestAskPrice,
                        }

                        Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bestBidPrice, bestAskPrice,
                                                                    self.exchangeName, self.exchangeName, blockNumber, str(index))

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        # Generically show the error so I can easily search for it in the logs
                        self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken, quoteTokensToSpend_etherUnits)

                # if Libraries.defaults.VerboseLogging_GetPrices_0xMesh:
                #     duration_s = (datetime.datetime.now() - before).total_seconds()
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "iterated over " + str(
                #         len(quoteTokensToSpendList_etherUnits)) + " quoteTokensToSpendList_etherUnits in duration_s = " + str(
                #         duration_s) + " seconds for baseToken " + str(baseToken))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                # Generically show the error so I can easily search for it in the logs
                self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken)

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 3, finished parsing 0x orders")

        if Libraries.defaults.VerboseLogging_SettingReturnValueForResultKey:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Setting returnValue for resultKey " + str(
                resultKey) + ", quoteToken = " + str(quoteToken) + ", detailed = " + str(detailed) + ", returnValue = " + str(returnValue))
        Libraries.utils.ConsiderSettingResultDict(returnValue, resultDict, resultKey, lock_resultDict)

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Successfully ended with quoteToken " + str(
            quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 4, returning")

    def FinalizeMetaDataBeforeTrade(self, arbitrageOpportunity, pathIndex):
        from Libraries.market import GetTokenObjectGivenTokenAddress

        maxOrdersToQueue = 6

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        PrintAndLog_FuncNameHeader("Finalizing orderbook data for quoteToken = " + str(quoteToken) + " and baseToken = " + str(baseToken))
        # quoteTokenTradeAmount, baseTokenTradeAmount = arbitrageOpportunity.GetExpectedTokenQuantitiesTraded(pathIndex)
        # PrintAndLog_FuncNameHeader("quoteTokenTradeAmount = " + str(quoteTokenTradeAmount) + ", baseTokenTradeAmount = " + str(baseTokenTradeAmount))

        # Get all orders from the OrderAggregators
        token = GetTokenObjectGivenTokenAddress(baseToken)

        orderDict_bids_0x = token.GetCopyOfAllOrders_Bids_FromTheseOrderAggregators(quoteToken, True, [self.exchangeName])
        orderDict_asks_0x = token.GetCopyOfAllOrders_Asks_FromTheseOrderAggregators(quoteToken, True, [self.exchangeName])
        PrintAndLog_FuncNameHeader("orderDict_bids_0x has " + str(len(orderDict_bids_0x)) + " orders for baseToken " + str(baseToken))
        PrintAndLog_FuncNameHeader("orderDict_asks_0x has " + str(len(orderDict_asks_0x)) + " orders for baseToken " + str(baseToken))

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # PrintAndLog_FuncNameHeader("side = " + str(side))

        # PrintAndLog_FuncNameHeader("arbitrageOpportunity.metaDataDict_beforeTrade before = " + str(arbitrageOpportunity.metaDataDict_beforeTrade))

        #  So i need to uses rate instead of price and just see if this path is still profitable when you consider
        #  all the rates and plug this rate into this index and run them all to see if profit percentage > 1.0
        #  So say rates are, 387.493737984980439 * 0.051282051282051 * 0.0654159889702764 and the middle one is 0x.
        #  Below I'm iterating over all orderHashes in orderDict_bids_0x and orderDict_asks_0x
        #  Keep that same loop, but instead of comparing two prices
        #  I need to replace the middle 0x rate with the rate of the order i'm examining.
        #  Trick thing here is that I only have the order price with respect to pricesDicts quoteToken
        #  Rate could be inverted. So I'll need to call ConvertPriceToRate
        #  Then instead of my logic being
        #       if price_0x > price_other:
        #  it becomes
        #       if 387.493737984980439 * newRate * 0.0654159889702764 > 1.0:

        # TODO, determine what new param gets passed into FinalizeMetaDataBeforeTrade, is it an index for pathList and exchangeNamesList?  I think so...

        # Determine the target profit percentage we're expecting so we can find the best order(s) to select for this trade
        targetProfitPercentage = Libraries.utils.GetProfitPercentageFromRatesList(arbitrageOpportunity.ratesList)
        # PrintAndLog_FuncNameHeader("targetProfitPercentage = " + str(targetProfitPercentage))
        # Make the targetProfitPercentage slightly worse just to avoid a rounding error
        targetProfitPercentage -= 0.000001
        PrintAndLog_FuncNameHeader("targetProfitPercentage made slightly worse to avoid a rounding error, targetProfitPercentage = " + str(targetProfitPercentage))

        if Libraries.core.IsSell(side):
            # Selling on 0x means selling to the bids
            arbitragableOrdersList_bids = []
            for orderHash in orderDict_bids_0x:
                order = orderDict_bids_0x[orderHash]

                # PrintAndLog_FuncNameHeader("Examining bid " + str(order.GetPrice()) + ", timeRemaining = " + str(
                #     order.GetDurationRemaining_seconds()) + ", exchangeName " + str(self.exchangeName) + ", order.hash = " + str(order.hash))
                # Require that the order is fillable
                # NOTE: I'm passing in arbitrageOpportunity.quoteTokenAmount here
                # instead of the value from arbitrageOpportunity.GetExpectedTokenQuantitiesTraded
                # Is this right? Jury is still out
                if not self.IsOrderFillable(order, arbitrageOpportunity.quoteTokenAmount):
                    continue

                # copy the ratesList since we are going to modify it
                copy_ratesList = copy.deepcopy(arbitrageOpportunity.ratesList)
                # PrintAndLog_FuncNameHeader("Before modifying: copy_ratesList = " + str(copy_ratesList))
                # Modify copy_ratesList so that this trade's rate is inserted
                rate_0x = Libraries.utils.ConvertPriceToRate(order.GetPrice(), side)
                # PrintAndLog_FuncNameHeader("rate_0x = " + str(rate_0x))
                copy_ratesList[pathIndex] = rate_0x
                # PrintAndLog_FuncNameHeader("After modifying: copy_ratesList = " + str(copy_ratesList))
                profitPercentage = Libraries.utils.GetProfitPercentageFromRatesList(copy_ratesList)
                # PrintAndLog_FuncNameHeader("profitPercentage = " + str(profitPercentage))

                # Ensure this trade will result in a profit percentage that meets our requirement
                if profitPercentage >= targetProfitPercentage:
                    PrintAndLog_FuncNameHeader("Found a bid arbitragable order: order.GetPrice() = " + str(order.GetPrice()) + ", order.GetEtherQuantity() = " + str(
                        order.GetEtherQuantity()) + ", arbitrageOpportunity.quoteTokenAmount = " + str(arbitrageOpportunity.quoteTokenAmount) + ", order.hash = " + str(
                        order.hash))
                    arbitragableOrdersList_bids.append(order)

            # Sort the arbitragableOrdersList_bids based on highest price first
            arbitragableOrdersList_bids.sort(key=lambda x: x.GetPrice(), reverse=True)
            # PrintAndLog_FuncNameHeader("arbitragableOrdersList_bids after sorting = " + str(arbitragableOrdersList_bids))
            arbitragableOrdersList_bids = arbitragableOrdersList_bids[:maxOrdersToQueue]
            # PrintAndLog_FuncNameHeader("arbitragableOrdersList_bids after taking only max " + str(
            #     maxOrdersToQueue) + " orders = " + str(arbitragableOrdersList_bids))

            # Validate the orders to make sure they're still fillable
            originalCount = len(arbitragableOrdersList_bids)
            arbitragableOrdersList_bids = self.API_VerifyValidityOfOrders(arbitragableOrdersList_bids)
            PrintAndLog_FuncNameHeader("Found that " + str(len(arbitragableOrdersList_bids)) + " orders of the original " + str(
                originalCount) + " are still valid, fillable, and meet our price requirements")

            # Set the metaDataDict_beforeTrade
            # Which order do I give the metaData???
            # I've found a potential blocking issue where if I always return the best item in the list, the others will never get considered when they may also be valid choices
            # In a perfect world, here is where I would actually spawn this arbitrageOpportunity into multiple arbitrageOpportunities
            # But this will be a big refactor that could cause issues that I dont' want to deal with right now
            # Do I deep copy? Where do I draw the line? How many deep copys do I make?
            # If I start with 10 but each one spans 10 I end with 100. That could be crazy bad for performance.
            # THis needs thought through
            # For now, a simple short term solution is to either take the best index, or a random index
            # Or even a random index from the top n choices....

            if len(arbitragableOrdersList_bids) <= 0:
                raise Exception("None of the " + str(self.exchangeName) + " order bids passed the final checks. Did the orders get canceled/filled or do I have a bug??")

            # arbitragableOrder = arbitragableOrdersList_bids[0]
            arbitragableOrder = secrets.choice(arbitragableOrdersList_bids)
            arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex] = {
                'arbitragableOrder': arbitragableOrder,
            }
            PrintAndLog_FuncNameHeader("arbitrageOpportunity.metaDataDict_beforeTrade after = " + str(arbitrageOpportunity.metaDataDict_beforeTrade))

        elif Libraries.core.IsBuy(side):
            # Buying on 0x means buying up the asks
            arbitragableOrdersList_asks = []
            for orderHash in orderDict_asks_0x:
                order = orderDict_asks_0x[orderHash]

                # PrintAndLog_FuncNameHeader("Examining ask " + str(order.GetPrice()) + " timeRemaining = " + str(
                #     order.GetDurationRemaining_seconds()) + ", order.hash = " + str(order.hash))
                # Require that the order is fillable
                # NOTE: I'm passing in arbitrageOpportunity.quoteTokenAmount here
                # instead of the value from arbitrageOpportunity.GetExpectedTokenQuantitiesTraded
                # Is this right? Jury is still out
                if not self.IsOrderFillable(order, arbitrageOpportunity.quoteTokenAmount):
                    continue

                # copy the ratesList since we are going to modify it
                copy_ratesList = copy.deepcopy(arbitrageOpportunity.ratesList)
                # PrintAndLog_FuncNameHeader("Before modifying: copy_ratesList = " + str(copy_ratesList))
                # Modify copy_ratesList so that this trade's rate is inserted
                rate_0x = Libraries.utils.ConvertPriceToRate(order.GetPrice(), side)
                # PrintAndLog_FuncNameHeader("rate_0x = " + str(rate_0x))
                copy_ratesList[pathIndex] = rate_0x
                # PrintAndLog_FuncNameHeader("After modifying: copy_ratesList = " + str(copy_ratesList))
                profitPercentage = Libraries.utils.GetProfitPercentageFromRatesList(copy_ratesList)
                # PrintAndLog_FuncNameHeader("profitPercentage = " + str(profitPercentage))

                # Ensure this trade will result in a profit percentage that meets our requirement
                if profitPercentage >= targetProfitPercentage:
                    PrintAndLog_FuncNameHeader("Found an ask arbitragable order: order.GetPrice() = " + str(order.GetPrice()) + ", order.GetEtherQuantity() = " + str(
                        order.GetEtherQuantity()) + ", arbitrageOpportunity.quoteTokenAmount = " + str(arbitrageOpportunity.quoteTokenAmount) + ", order.hash = " + str(
                        order.hash))
                    arbitragableOrdersList_asks.append(order)

            # Sort the arbitragableOrdersList_asks based on lowest price first
            arbitragableOrdersList_asks.sort(key=lambda x: x.GetPrice(), reverse=False)
            # PrintAndLog_FuncNameHeader("arbitragableOrdersList_asks after sorting = " + str(arbitragableOrdersList_asks))
            arbitragableOrdersList_asks = arbitragableOrdersList_asks[:maxOrdersToQueue]
            # PrintAndLog_FuncNameHeader("arbitragableOrdersList_asks after taking only max " + str(
            #     maxOrdersToQueue) + " orders = " + str(arbitragableOrdersList_asks))

            # Validate the orders to make sure they're still fillable
            originalCount = len(arbitragableOrdersList_asks)
            arbitragableOrdersList_asks = self.API_VerifyValidityOfOrders(arbitragableOrdersList_asks)
            PrintAndLog_FuncNameHeader("Found that " + str(len(arbitragableOrdersList_asks)) + " orders of the original " + str(
                originalCount) + " are still valid, fillable, and meet our price requirements")

            # Set the metaDataDict_beforeTrade
            # Which order do I give the metaData???
            # I've found a potential blocking issue where if I always return the best item in the list, the others will never get considered when they may also be valid choices
            # In a perfect world, here is where I would actually spawn this arbitrageOpportunity into multiple arbitrageOpportunities
            # But this will be a big refactor that could cause issues that I dont' want to deal with right now
            # Do I deep copy? Where do I draw the line? How many deep copys do I make?
            # If I start with 10 but each one spans 10 I end with 100. That could be crazy bad for performance.
            # THis needs thought through
            # For now, a simple short term solution is to either take the best index, or a random index
            # Or even a random index from the top n choices....

            if len(arbitragableOrdersList_asks) <= 0:
                raise Exception("None of the " + str(self.exchangeName) + " order asks passed the final checks. Did the orders get canceled/filled or do I have a bug??")

            # arbitragableOrder = arbitragableOrdersList_asks[0]
            arbitragableOrder = secrets.choice(arbitragableOrdersList_asks)
            arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex] = {
                'arbitragableOrder': arbitragableOrder,
            }
            PrintAndLog_FuncNameHeader("arbitrageOpportunity.metaDataDict_beforeTrade after = " + str(arbitrageOpportunity.metaDataDict_beforeTrade))

        else:
            raise Exception("Not buy or sell?")

    def IsOrderFillable(self, order, effectiveQuoteTokensToSpend, doEnforce_fillableAmountRemainingAgainstEffectiveQuoteTokensToSpend=True):
        from Libraries.orders import GetOrdersFillableAmountRemaining_0x

        # Ensure this orderHash is not blacklisted
        if BlacklistedOrderManager.IsIDBlacklisted(order.hash):
            if Libraries.defaults.VerboseLogging_IsOrderFillable:
                PrintAndLog_FuncNameHeader("Not considering order.hash " + str(order.hash) + " because it's been blacklisted")
            return False

        # TODO, remove this hack once i've addressed the issue
        blackListedOrderHashes = []
        if order.hash in blackListedOrderHashes:
            return False

        fillableAmountRemaining_quoteTokens_etherUnits, amountFilled_quoteTokens_etherUnits, makersTokenBalance_quoteTokens_etherUnits, makersTokenAllowance_quoteTokens_etherUnits = \
            GetOrdersFillableAmountRemaining_0x(order, self, False)

        if Libraries.defaults.VerboseLogging_IsOrderFillable:
            PrintAndLog_FuncNameHeader("Setting effectiveEther based on order.GetEtherQuantity() = " + str(
                order.GetEtherQuantity()) + ", effectiveQuoteTokensToSpend = " + str(
                effectiveQuoteTokensToSpend) + ", and amountFilled_quoteTokens_etherUnits = " + str(
                amountFilled_quoteTokens_etherUnits) + ", makersTokenBalance_quoteTokens_etherUnits = " + str(
                makersTokenBalance_quoteTokens_etherUnits) + ", makersTokenAllowance_quoteTokens_etherUnits = " + str(
                makersTokenAllowance_quoteTokens_etherUnits) + ", fillableAmountRemaining_quoteTokens_etherUnits = " + str(
                fillableAmountRemaining_quoteTokens_etherUnits) + ", order.maker = " + str(order.maker) + ", order.makerTokenAddress = " + str(
                order.makerTokenAddress) + ", order.hash = " + str(order.hash))

        if doEnforce_fillableAmountRemainingAgainstEffectiveQuoteTokensToSpend:
            # TODO FIXME HACKY SHIT
            #  My 0x logic is slightly flawed and it's not handling the effectiveQuoteTokensToSpend validation well
            #  My temporary work around is that I make it less sensitive so that it's more likely to pass this IsOrderFillable effectiveQuoteTokensToSpend check
            # TODO I think I don't need this shitty hack anymore but i'm going to leave it in
            #  because I think it's not hurting anything and I don't have time to debug both with and without it
            fillableAmountRemaining_quoteTokens_etherUnits_withMultiplierApplied = \
                fillableAmountRemaining_quoteTokens_etherUnits * Libraries.defaults.Multiplier_0x_fillableAmountRemaining_quoteTokens_etherUnits
            if Libraries.defaults.VerboseLogging_IsOrderFillable:
                PrintAndLog_FuncNameHeader("Libraries.defaults.Multiplier_0x_fillableAmountRemaining_quoteTokens_etherUnits = " +
                                           str(Libraries.defaults.Multiplier_0x_fillableAmountRemaining_quoteTokens_etherUnits))
                PrintAndLog_FuncNameHeader("fillableAmountRemaining_quoteTokens_etherUnits_withMultiplierApplied = " +
                                           str(fillableAmountRemaining_quoteTokens_etherUnits_withMultiplierApplied))

            if fillableAmountRemaining_quoteTokens_etherUnits_withMultiplierApplied < effectiveQuoteTokensToSpend:
                if Libraries.defaults.VerboseLogging_IsOrderFillable:
                    PrintAndLog_FuncNameHeader("effectiveQuoteTokensToSpend failed validation. effectiveQuoteTokensToSpend = " + str(effectiveQuoteTokensToSpend))
                return False

        if fillableAmountRemaining_quoteTokens_etherUnits <= 0:
            if Libraries.defaults.VerboseLogging_IsOrderFillable:
                PrintAndLog_FuncNameHeader("fillableAmountRemaining_quoteTokens_etherUnits was " + str(
                    fillableAmountRemaining_quoteTokens_etherUnits) + " which means this order is not fillable. Continuing on. order.hash = " + str(order.hash))
            return False

        # TODO, I need to get this from a config or something or maybe based on gas price
        #  fast gas price can be shorter duration. Slower gas price, must be longer duration
        minOrderDuration_seconds = Libraries.defaults.MinFillableOrderDuration_0x_seconds
        # Make sure the order isn't expired
        # If it's almost expired, make sure we use high enough gas price to get it mined in quickly
        if order.GetDurationRemaining_seconds() < minOrderDuration_seconds:
            if Libraries.defaults.VerboseLogging_IsOrderFillable:
                PrintAndLog_FuncNameHeader("Ignoring this order because it's expiring soon compared to our config. order.GetDurationRemaining_seconds() = " + str(
                    order.GetDurationRemaining_seconds()) + ", minOrderDuration_seconds = " + str(minOrderDuration_seconds))
            return False

        # If we've made it this far, this order is fillable based on the requirements passed in
        return True

    def GenerateCallData_Check(self, arbitrageOpportunity, pathIndex, tradingTechnique):
        from Exchanges.ninja import MatchProfitPercentageToSafetiplier, PreparePackedTradeCallData_GivenCallData_Hex
        import Libraries.orderAggregator

        # Generate the encoded callData

        # PrintAndLog_FuncNameHeader("Sanity check, arbitrageOpportunity.priceDict[" + Libraries.core.InvertOrderType(side) + "] = " + str(
        #     arbitrageOpportunity.priceDict[Libraries.core.InvertOrderType(side)]))
        # PrintAndLog_FuncNameHeader("Sanity check, arbitrageOpportunity.priceDict[" + side + "] = " + str(
        #     arbitrageOpportunity.priceDict[side]))

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        quoteTokenTradeAmount, baseTokenTradeAmount = arbitrageOpportunity.GetExpectedTokenQuantitiesTraded(pathIndex)
        quoteTokenTradeAmount_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(quoteTokenTradeAmount, quoteToken)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # quoteTokensToSpendSafetiplier_weiUnits = MatchProfitPercentageToSafetiplier(arbitrageOpportunity.profitPercent)

        # Determine the order from the metaData
        arbitragableOrder = arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex]['arbitragableOrder']

        # create an checkCallDataArray_encoded
        data_hex = self.PrepCallData_Check(side, arbitragableOrder, quoteTokenTradeAmount_weiUnits, quoteToken, baseToken)
        # PrintAndLog_FuncNameHeader("checkCallDataArray data_hex for " + self.exchangeName + " = " + str(data_hex))
        checkCallDataArray_encoded = PreparePackedTradeCallData_GivenCallData_Hex(data_hex)

        # Determine if we're eligible to skip the check to save gas, or if we should mandate a check
        eligibleForSkippingTheCheck = False
        # PrintAndLog_FuncNameHeader("self.exchangeName = " + str(self.exchangeName))
        # Only consider skipping a check if there's no chance of a gas auction (aka Hiding Game)
        if isinstance(self, Exchange_HidingBook):
            # I may not need to check amount filled for partial fills because i'm already doing an eth_estimateGas and eth_call anyways
            # amountFilled_takerToken_weiUnits = Libraries.orderAggregator.AmountFilledValueDict_0x.GetCopyOfValue(arbitragableOrder.hash)
            # PrintAndLog_FuncNameHeader("amountFilled_takerToken_weiUnits = " + str(amountFilled_takerToken_weiUnits))
            # Consider skipping check as long as the order won't expire soon
            # PrintAndLog_FuncNameHeader("arbitragableOrder.GetDurationRemaining_seconds() = " + str(arbitragableOrder.GetDurationRemaining_seconds()))
            if arbitragableOrder.GetDurationRemaining_seconds() > 60:
                eligibleForSkippingTheCheck = True
                
        PrintAndLog_FuncNameHeader("eligibleForSkippingTheCheck = " + str(eligibleForSkippingTheCheck) + ", " + str(self.exchangeName) +
                                   " arbitragableOrder has " + str(arbitragableOrder.GetDurationRemaining_seconds()) + " seconds remaining")
        if eligibleForSkippingTheCheck:
            return []
        else:
            return [checkCallDataArray_encoded]

    def PrepCallData_Check(self, side, order, quoteTokenTradeAmount_weiUnits, quoteToken, baseToken):
        data_hex = None
        if Libraries.core.IsBuy(side):
            #     function checkSimpleRequirements_buy_0xv3(
            #         uint256 quoteTokensToSpend,
            #         address baseToken,
            #         Order calldata order
            #         ) external returns (bool)
            data_hex = Libraries.signingUtils.EncodeABI(
                'checkSimpleRequirements_buy_0xv3',
                'uint256,address,' + Exchanges.zrxV2.OrderTupleString,
                [
                    int(quoteTokenTradeAmount_weiUnits),
                    # We must call GetProper0xTokenAddress because 0x expects WETH, not 0xEEEE...EEEE
                    str(Libraries.nodes.Instance_Web3.toChecksumAddress(Exchange_0x_LowerAbstraction.GetProper0xTokenAddress(baseToken))),
                    order.GenerateOrderTupleForTransaction(),
                ])

        elif Libraries.core.IsSell(side):
            #     function checkSimpleRequirements_sell_0xv3(
            #         uint256 quoteTokensToSpend,
            #         address quoteToken,
            #         Order calldata order
            #         ) external returns (bool)
            data_hex = Libraries.signingUtils.EncodeABI(
                'checkSimpleRequirements_sell_0xv3',
                'uint256,address,' + Exchanges.zrxV2.OrderTupleString,
                [
                    int(quoteTokenTradeAmount_weiUnits),
                    # We must call GetProper0xTokenAddress because 0x expects WETH, not 0xEEEE...EEEE
                    str(Libraries.nodes.Instance_Web3.toChecksumAddress(Exchange_0x_LowerAbstraction.GetProper0xTokenAddress(quoteToken))),
                    order.GenerateOrderTupleForTransaction(),
                ])

        else:
            raise Exception("Not buy or sell?")

        return data_hex

    def GenerateCallData_Trade(self, arbitrageOpportunity, pathIndex):
        from Exchanges.ninja import PreparePackedTradeCallData_GivenCallData_Hex

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        # Determine the order from the metaData
        arbitragableOrder = arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex]['arbitragableOrder']

        # create an tradeCallDataArray_encoded
        data_hex = self.PrepCallData_Trade(arbitragableOrder)
        # PrintAndLog_FuncNameHeader("tradeCallDataArray data_hex for " + self.exchangeName + " = " + str(data_hex))
        tradeCallDataArray_encoded = PreparePackedTradeCallData_GivenCallData_Hex(data_hex, False, True)
        return [tradeCallDataArray_encoded]

    def PrepCallData_Trade(self, order):
        # function trade_0xv3(
        #         uint256 takerTokensToSpend,
        #         Order memory order,
        #         bytes memory signature
        #         ) public returns (uint256 makerTokensReceived)
        data_hex = Libraries.signingUtils.EncodeABI(
            'trade_0xv3',
            'uint256,' + Exchanges.zrxV2.OrderTupleString + ',bytes',
            [
                int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
                order.GenerateOrderTupleForTransaction(),
                Web3.toBytes(hexstr=str(order.signature)),
            ])
        return data_hex

    # # Stringify the metadata
    # def GetUniqueMetaDataString(self, arbitrageOpportunity, pathIndex):
    #     try:
    #         # Get orders from the metaData
    #         arbitragableOrdersList = arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex]['arbitragableOrdersList']
    #         if len(arbitragableOrdersList) <= 0:
    #             return None
    #         else:
    #             # Return a comma separated list of orderHashes
    #             returnString = ''
    #             for index, order in enumerate(arbitragableOrdersList):
    #                 if index > 0:
    #                     # Comma between orders
    #                     returnString += ","
    #
    #                 returnString += order.hash
    #
    #             return returnString
    #
    #     except (KeyboardInterrupt, SystemExit):
    #         print('\nkeyboard interrupt caught')
    #         print('\n...Program Stopped Manually!')
    #         raise
    #
    #     except:
    #         PrintAndLogError("exception when getting unique metadata string, " + traceback.format_exc())
    #
    #     return "ExceptionWhenGettingUniqueMetaDataString"

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def PrepareRateFormulaMetaData(self, arbitrageOpportunity, pathIndex):
        return None, None, None

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def GetRateFormulaMetaData(self, resultForApiCall, arbitrageOpportunity, pathIndex, metaDataResultDict, key):
        from Libraries.orders import GetOrdersFillableAmountRemaining_0x

        header = "GetRateFormulaMetaData: " + str(self.exchangeName)

        # Determine the order from the metaData
        order = arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex]['arbitragableOrder']

        fillableAmountRemaining_quoteTokens_etherUnits, amountFilled_quoteTokens_etherUnits, makersTokenBalance_quoteTokens_etherUnits, makersTokenAllowance_quoteTokens_etherUnits = \
            GetOrdersFillableAmountRemaining_0x(order, self, False)
        PrintAndLog_Header(header, "Using order with hash = " + str(order.hash) +
                           ", fillableAmountRemaining_quoteTokens_etherUnits = " + str(fillableAmountRemaining_quoteTokens_etherUnits))

        # Convert price to rate...
        mySide = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # I want to invert the side because if the exchange is selling, that means I'm buying
        # mySide = Libraries.core.InvertOrderType(order.GetSide())  # This should be the same as above, but let's be consistent and use the same method everywhere
        rate = Libraries.utils.ConvertPriceToRate(order.GetPrice(), mySide)
        PrintAndLog_Header(header, "Converted " + str(self.exchangeName) + " order price = " + str(
            order.GetPrice()) + " to a rate = " + str(rate) + ", order side = " + str(order.GetSide()) + ", mySide = " + str(mySide))

        metaDataResultDict[key] = {
            'rate': rate,
            'maxAmount_quoteTokens': fillableAmountRemaining_quoteTokens_etherUnits,
        }
        PrintAndLog_Header(header, "Set metaDataResultDict = " + str(metaDataResultDict))

    # Assume the same metaData structure as set by GetRateFormulaMetaData for this exchange
    # calculate the rate (not price) when trading according to this metaData on this exchange
    def GetRate_FinalizeQuoteTokenAmount(self, arbitrageOpportunity, pathIndex, inFlowAmount, metaData,
                                         tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        # header = "GetRate_FinalizeQuoteTokenAmount: " + str(self.exchangeName)
        # PrintAndLog_Header(header, "Returning rate from metaData: rate = " + str(metaData['rate']))
        return metaData['rate']

    # Some protocols charge trading fees, like 0x's protocol fee
    def CalculateTradeFee_eth(self, gasPrice):
        # Calculate the fee based on gas price
        protocolFee_eth_weiUnits = Exchanges.zrxV2.GetProtocolFee(gasPrice, 1)
        return protocolFee_eth_weiUnits

    def DoEventLogsContainInteractionWithThisExchange(self, eventLogsList):
        # https://etherscan.io/tx/0x251b629bf5af71bb235040bade5c77b404e23db3200d988c25c9f53c411d4184#eventlog
        if Exchanges.zrxV2.Contract_Exchange.lower() in str(eventLogsList).lower():
            return True
        else:
            return False

    # Get general exchange info at this block, given txReceipt
    def GetInfoAtBlockNumber(self, local_blockNumber, tokenAddressesToTraceList,
                             txReceipt, txReceiptList_forThisBlockNumber, textToPrepend):
        PrintAndLog('TODO, search for 0x cancels and fills')


class Exchange_0xv4(Exchange_0xMesh):
    todo = None


class Exchange_0xv4_RFQ(Exchange_0xv4):

    def GetExpectedGasUsage_Check(self):
        return 20000

    def GetExpectedGasUsage_Trade(self):
        # 105000 is entire cost to trade from a private key based account
        return 65000

    def API_VerifyValidityOfOrders(self, arbitragableOrdersList):
        return Exchanges.zrxV4.API_VerifyValidityOfOrders(arbitragableOrdersList)

    # Some protocols charge trading fees, like 0x's protocol fee
    def CalculateTradeFee_eth(self, gasPrice):
        return 0

    def PrepCallData_Check(self, side, order, quoteTokenTradeAmount_weiUnits, quoteToken, baseToken):
        data_hex = None
        if Libraries.core.IsBuy(side):
            #     function checkSimpleRequirements_buy_0xv4_rfq(
            #         uint256 quoteTokensToSpend,
            #         address baseToken,
            #         RfqOrder calldata order
            #         ) external returns (bool)
            data_hex = Libraries.signingUtils.EncodeABI(
                'checkSimpleRequirements_buy_0xv4_rfq',
                'uint256,address,' + Exchanges.zrxV4.OrderTupleString_RFQ,
                [
                    int(quoteTokenTradeAmount_weiUnits),
                    # We must call GetProper0xTokenAddress because 0x expects WETH, not 0xEEEE...EEEE
                    str(Libraries.nodes.Instance_Web3.toChecksumAddress(Exchange_0x_LowerAbstraction.GetProper0xTokenAddress(baseToken))),
                    order.GenerateOrderTupleForTransaction(),
                ])

        elif Libraries.core.IsSell(side):
            #     function checkSimpleRequirements_sell_0xv4_rfq(
            #         uint256 quoteTokensToSpend,
            #         address quoteToken,
            #         RfqOrder calldata order
            #         ) external returns (bool)
            data_hex = Libraries.signingUtils.EncodeABI(
                'checkSimpleRequirements_sell_0xv4_rfq',
                'uint256,address,' + Exchanges.zrxV4.OrderTupleString_RFQ,
                [
                    int(quoteTokenTradeAmount_weiUnits),
                    # We must call GetProper0xTokenAddress because 0x expects WETH, not 0xEEEE...EEEE
                    str(Libraries.nodes.Instance_Web3.toChecksumAddress(Exchange_0x_LowerAbstraction.GetProper0xTokenAddress(quoteToken))),
                    order.GenerateOrderTupleForTransaction(),
                ])

        else:
            raise Exception("Not buy or sell?")

        return data_hex

    def PrepCallData_Trade(self, order):
        #     function trade_0xv4_rfq(
        #         uint256 takerTokensToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        #         RfqOrder memory order,
        #         Signature memory signature
        #         ) public onlySafeTraders returns (uint256 makerTokensReceived)
        data_hex = Libraries.signingUtils.EncodeABI(
            'trade_0xv4_rfq',
            'uint256,' + Exchanges.zrxV4.OrderTupleString_RFQ + ',' + Exchanges.zrxV4.SignatureTupleString_RFQ,
            [
                int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
                order.GenerateOrderTupleForTransaction(),
                order.GenerateSignatureTupleForTransaction(),
            ])
        return data_hex

    def DoEventLogsContainInteractionWithThisExchange(self, eventLogsList):
        # https://etherscan.io/tx/0x57247fbc2207d298bd161ccf4bdd37545c0baf3f92cb65ad82f25083577a07b1#eventlog
        if Exchanges.zrxV4.Contract_Exchange.lower() in str(eventLogsList).lower():
            return True
        else:
            return False


class Exchange_0xv4_OpenOrderbook(Exchange_0xv4):

    def GetExpectedGasUsage_Check(self):
        return 20000

    def GetExpectedGasUsage_Trade(self):
        # 192000 is entire cost to trade from a private key based account
        return 152000


class Exchange_HidingBook(Exchange_0xv4_RFQ):

    def GetTradableTokens(self):
        # Get the data from the cache, but if the cache is old then make the API call first
        key = 'Exchanges.hidingBook.API_GetListOfTokenAddresses'
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        tradeableTokensList = Libraries.cache.GetDataFromCache(key)

        # Assume the data is cached, caches should be populated ahead of time, not in this function
        # We don't want to just return all the exchange's tokens because there's so many that are absolute junk and don't have any value in the exchange anyways

        # TODO Implement this properly, I tried using UniswapV2's tokens but the rates graph ended up including over 1000 tokens! That would murder performance.
        #  Do not use UniswapV2's token list right now... Very very bad idea
        # tradeableTokensList = Exchanges.uniswapV2.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(
        #     list(Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict().keys()))

        tokensToManuallyEnforce = []
        # ROOK absolutely must be here :-D not much DX liquidity yet so let's force it
        tokensToManuallyEnforce.append('0xfa5047c9c78b8877af97bdcb85db743fd7313d4a'.lower())  # ROOK
        tokensToManuallyEnforce.append('0x111111111117dc0aa78b770fa6a738034120c302'.lower())  # 1inch
        tokensToManuallyEnforce.append('0x221657776846890989a759ba2973e427dff5c9bb'.lower())  # REPv2
        tokensToManuallyEnforce.append('0xEfc1C73A3D8728Dc4Cf2A18ac5705FE93E5914AC'.lower())  # METRIC
        tokensToManuallyEnforce.append('0x6e36556b3ee5aa28def2a8ec3dae30ec2b208739'.lower())  # BUILD
        tokensToManuallyEnforce.append('0x7866E48C74CbFB8183cd1a929cd9b95a7a5CB4F4'.lower())  # KIT
        tokensToManuallyEnforce.append('0xA8b12Cc90AbF65191532a12bb5394A714A46d358'.lower())  # pBTC35A
        tokensToManuallyEnforce.append('0x66c0dded8433c9ea86c8cf91237b14e10b4d70b7'.lower())  # MARS
        tokensToManuallyEnforce.append('0x3449fc1cd036255ba1eb19d65ff4ba2b8903a69a'.lower())  # BAC
        for tokenToManuallyEnforce in tokensToManuallyEnforce:
            if tokenToManuallyEnforce not in tradeableTokensList:
                tradeableTokensList.append(tokenToManuallyEnforce)
                PrintAndLog_FuncNameHeader("Had to manually enforce a token into tradeableTokensList for " + str(self.exchangeName)
                                           + ": tokenToManuallyEnforce = " + str(tokenToManuallyEnforce))

        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)


class Exchange_OasisDex(Exchange_0x_LowerAbstraction):
    todo = None


class Exchange_Bancor(Exchange_Decentralized):
    todo = None


class Exchange_Kyber(Exchange_Decentralized):

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        # PrintAndLog_Header(header_logging, "returning metaDataIdentifier = " + str(metaDataIdentifier))
        return metaDataIdentifier

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        return "TODO-Return-Kyber-Reserve-From-MetaData-GetMetaDataIdentifier"

    # Some exchanges require ETH as a quote token,
    # which means that if you want to trade token for token you must make two trades (sell one token for ETH, then use that ETH to buy another token)
    # This flag is True when ETH is always the quote token on this exchange
    def DirectQuoteTokenIsRestrictedToEthOnly(self):
        return True

    # This is True for things like Uniswap where the price is a ratio of two tokens in the exchange.
    # This is False for things like 0x where there is an offchain orderbook and one order's price does not effect another order's price
    # TODO, for kyber this will be pretty unique. Technically Kyber has many reserves I could arb against.
    #  But i'm not sure that's the same as this.
    #  Either I can make it use this same flag somehow, or I need a separate flag from this
    def IncomingTradesAffectThePriceOfTheExchange(self):
        return True

    # Some protocols enforce a max gasPrice. Most of them; however, do nots
    def EnforceMaxGasPrice(self, gasPrice):
        gasPrice = min(gasPrice, Libraries.gasStation.KyberMaxGasPrice_wei)
        PrintAndLog_FuncNameHeader("Libraries.gasStation.KyberMaxGasPrice_wei of " + str(
            Libraries.core.ConvertWeiToGwei(Libraries.gasStation.KyberMaxGasPrice_wei)) + " gwei, gasPrice now = " + str(
            Libraries.core.ConvertWeiToGwei(gasPrice)) + " gwei")

        return gasPrice

    def GetTradableTokens(self):
        # Get the data from the cache, but if the cache is old then make the API call first
        key = 'Exchanges.kyber.API_GetListOfTokenAddresses'
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        tradeableTokensList = Libraries.cache.GetDataFromCache(key)
        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)

    def GetPrices(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber,
                  resultDict, resultKey, lock_resultDict, detailed=False):
        self.EnforceEligibleQuoteToken(quoteToken)

        header_logging = Libraries.loggingConfig.GenerateHeaderForPrintAndLog(self.exchangeName)
        functionsStartDateTime = datetime.datetime.now()

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 1, Begin with quoteToken " + str(quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        returnValue = {}
        returnValue[pdk_baseTokens] = {}

        # Prepare the structure of the returnValue
        for index_token, baseToken in enumerate(baseTokenList):
            returnValue[pdk_baseTokens][baseToken] = {}
            returnValue[pdk_baseTokens][baseToken][pdk_prices] = {}

            for index, quoteTokensToSpend_etherUnits in enumerate(quoteTokensToSpendList_etherUnits):
                returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)] = {
                    pdk_quoteTokensToSpend_etherUnits: quoteTokensToSpend_etherUnits,
                    pdk_bid: '',
                    pdk_ask: '',
                }

        # Get prices from the kyber proxy because it's the quickest method
        # later if I find a tradingOpportunity I can check that tokens' kyberReserves for more detail
        # Right now the way I implemented it, the sell function depends on data obtained by the buy function, so I have to call them in series instead of parallel
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 2, calling GetProxyPrices_BuyOnly")
        self.GetProxyPrices_BuyOnly(quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber, returnValue)
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 3, calling GetProxyPrices_SellOnly")
        self.GetProxyPrices_SellOnly(quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber, returnValue)
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 4, done making calls")

        if Libraries.defaults.VerboseLogging_SettingReturnValueForResultKey:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Setting returnValue for resultKey " + str(
                resultKey) + ", quoteToken = " + str(quoteToken) + ", detailed = " + str(detailed) + ", returnValue = " + str(returnValue))
        Libraries.utils.ConsiderSettingResultDict(returnValue, resultDict, resultKey, lock_resultDict)

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Successfully ended with quoteToken " + str(
            quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance step 5, returning")

    def GetProxyPrices_BuyOnly(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber, returnValue):
        decimals_quoteTokens = Libraries.core.GetDecimalsForTokenContract(quoteToken, True)
        quoteTokensToSpendList_weiUnits = Libraries.core.ConvertListOfEthersToWeis(quoteTokensToSpendList_etherUnits, decimals_quoteTokens)

        payload_total = []
        payloadIdDict = {}
        payloadId = random.randint(0, 99999999999999)

        for index_token, baseToken in enumerate(baseTokenList):
            # PrintAndLog_FuncNameHeader("Creating payload_total for " + str(baseToken) + " " + str(index_token))

            side_kyber = 'buy'
            src_kyber = quoteToken
            dest_kyber = baseToken

            # Get the expectedRate for kyber itself
            payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)] = payloadId
            for index, quoteTokensToSpend_weiUnits in enumerate(quoteTokensToSpendList_weiUnits):
                try:
                    payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, quoteTokensToSpend_weiUnits, payloadId))

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    PrintAndLogError("exception GetProxyPrices_BuyOnly for index_token " + str(index_token) + " and index " + str(
                        index) + " = " + traceback.format_exc())

                # payloadId must get incremented even if this fails or else bad things may happen with data mismatches!
                payloadId += 1

        # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
        response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                        None, False, False, True, False, blockNumber)
        if response.ok:
            responseData = response.content
            jDataList = json.loads(responseData)
            PrintAndLog_FuncNameHeader("duration_s = " + str(response.elapsed.total_seconds()))
            # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

            # parse the results based on the ids in payloadIdDict
            payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, quoteTokensToSpendList_weiUnits)
            # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

            for index_token, baseToken in enumerate(baseTokenList):
                # PrintAndLog_FuncNameHeader("Parsing payloadResultDict for " + str(baseToken) + " " + str(index_token))

                # Determine results
                for index, quoteTokensToSpend_etherUnits in enumerate(quoteTokensToSpendList_etherUnits):
                    try:
                        # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
                        # PrintAndLog_FuncNameHeader("quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))

                        # PrintAndLog_FuncNameHeader("payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)] "
                        #                            "has len of " + str(
                        #     len(payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)])))
                        # Get the rate from the KyberProxy
                        result = payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)][index]
                        result = result.replace("0x", "")
                        splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
                        # PrintAndLog_FuncNameHeader("splitArray = " + str(splitArray))
                        # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
                        # PrintAndLog_FuncNameHeader("Libraries.core.ConvertHexToInt(splitArray[0]) + " + str(Libraries.core.ConvertHexToInt(splitArray[0])))
                        rate_kyberProxy = Libraries.core.ConvertWeiToEther(Libraries.core.ConvertHexToInt(splitArray[0]), Libraries.core.Ether_Decimals)
                        # PrintAndLog_FuncNameHeader("rate_kyberProxy = " + str(rate_kyberProxy))

                        price_kyber = ''
                        if rate_kyberProxy == 0:
                            # Price invalid
                            pass
                        else:
                            price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberProxy, side_kyber)
                            # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberProxy = " + str(rate_kyberProxy))

                        # Set the price in the returnValue
                        returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)][pdk_ask] = price_kyber

                        Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, None, price_kyber,
                                                                    self.exchangeName, self.exchangeName, blockNumber, str(index))

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        # This occurs a lot, so only show it every once in a while
                        if random.randint(0, 100) >= 98:
                            # Generically show the error so I can easily search for it in the logs
                            self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken, quoteTokensToSpend_etherUnits)

        else:
            # If response code is not ok (200), print the resulting http error code with description
            PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
            response.raise_for_status()

    def GetProxyPrices_SellOnly(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber, returnValue):
        decimals_quoteTokens = Libraries.core.GetDecimalsForTokenContract(quoteToken, True)
        quoteTokensToSpendList_weiUnits = Libraries.core.ConvertListOfEthersToWeis(quoteTokensToSpendList_etherUnits, decimals_quoteTokens)

        payload_total = []
        payloadIdDict = {}
        payloadId = random.randint(0, 99999999999999)

        for index_token, baseToken in enumerate(baseTokenList):
            # PrintAndLog_FuncNameHeader("Creating payload_total for " + str(baseToken) + " " + str(index_token))

            side_kyber = 'sell'
            src_kyber = baseToken
            dest_kyber = quoteToken

            # Get the expectedRate for kyber itself
            payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)] = payloadId
            for index, quoteTokensToSpend_weiUnits in enumerate(quoteTokensToSpendList_weiUnits):
                try:
                    # Convert quoteTokensToSpend_weiUnits to baseTokensToSpend_weiUnits
                    priceToUseForConversion = returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)][pdk_ask]
                    if not IsValidPrice(priceToUseForConversion):
                        raise Exception("priceToUseForConversion is not valid, cannot continue. priceToUseForConversion = " + str(priceToUseForConversion))

                    baseTokensToSpend_weiUnits = Libraries.core.ConvertQuoteTokensToBaseTokens_weiUnits(
                        quoteTokensToSpend_weiUnits, priceToUseForConversion, quoteToken, baseToken)

                    payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, baseTokensToSpend_weiUnits, payloadId))

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    # This occurs a lot, so only show it every once in a while
                    if random.randint(0, 100) >= 98:
                        PrintAndLogError("exception GetProxyPrices_SellOnly for index_token " + str(index_token) + " and index " + str(
                            index) + ". This exception happens often so I'm only printing this to logs occasionally. exception = " + traceback.format_exc())

                # payloadId must get incremented even if this fails or else bad things may happen with data mismatches!
                payloadId += 1

        # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
        response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                        None, False, False, True, False, blockNumber)
        if response.ok:
            responseData = response.content
            jDataList = json.loads(responseData)
            PrintAndLog_FuncNameHeader("duration_s = " + str(response.elapsed.total_seconds()))
            # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

            # parse the results based on the ids in payloadIdDict
            payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, quoteTokensToSpendList_weiUnits)
            # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

            for index_token, baseToken in enumerate(baseTokenList):
                # PrintAndLog_FuncNameHeader("Parsing payloadResultDict for " + str(baseToken) + " " + str(index_token))

                # Determine results
                for index, quoteTokensToSpend_etherUnits in enumerate(quoteTokensToSpendList_etherUnits):
                    try:
                        # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
                        # PrintAndLog_FuncNameHeader("quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))

                        # PrintAndLog_FuncNameHeader("payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)] "
                        #                            "has len of " + str(
                        #     len(payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)])))
                        # Get the rate from the KyberProxy
                        result = payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)][index]
                        result = result.replace("0x", "")
                        splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
                        # PrintAndLog_FuncNameHeader("splitArray = " + str(splitArray))
                        # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
                        # PrintAndLog_FuncNameHeader("Libraries.core.ConvertHexToInt(splitArray[0]) + " + str(Libraries.core.ConvertHexToInt(splitArray[0])))
                        rate_kyberProxy = Libraries.core.ConvertWeiToEther(Libraries.core.ConvertHexToInt(splitArray[0]), Libraries.core.Ether_Decimals)
                        # PrintAndLog_FuncNameHeader("rate_kyberProxy = " + str(rate_kyberProxy))

                        price_kyber = ''
                        if rate_kyberProxy == 0:
                            # Price invalid
                            pass
                        else:
                            price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberProxy, side_kyber)
                            # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberProxy = " + str(rate_kyberProxy))

                        # Set the price in the returnValue
                        returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)][pdk_bid] = price_kyber

                        Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, price_kyber, None,
                                                                    self.exchangeName, self.exchangeName, blockNumber, str(index))

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        # This occurs a lot, so only show it every once in a while
                        if random.randint(0, 100) >= 98:
                            # Generically show the error so I can easily search for it in the logs
                            self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken, quoteTokensToSpend_etherUnits)

        else:
            # If response code is not ok (200), print the resulting http error code with description
            PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
            response.raise_for_status()

    def GenerateCallData_Check(self, arbitrageOpportunity, pathIndex, tradingTechnique):
        from Contracts.contracts import Contract_Ninja_Exchange_Kyber
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData
        from Exchanges.kyber import ConvertPriceToRate

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        quoteTokenTradeAmount, baseTokenTradeAmount = arbitrageOpportunity.GetExpectedTokenQuantitiesTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        PrintAndLog_FuncNameHeader("Sanity check, arbitrageOpportunity.pricesList_wrtPricesDict[" + str(pathIndex) + "] = " + str(
            arbitrageOpportunity.pricesList_wrtPricesDict[pathIndex]))

        # Determine the maxKyberPriceAcceptable/minKyberPriceAcceptable so I can convert that to minKyberRateAcceptable
        minKyberRateAcceptable = None
        # priceMovementPercentageAllowed is how much we're willing to allow the price % to move before we reject it.
        priceMovementPercentageAllowed = arbitrageOpportunity.profitPercent * Libraries.defaults.AMMProfitPercentagePriceMovementMultiplier
        # priceMovementPercentageAllowed = Ninja_SimpleRequirements_WiggleRoomPercentage  # This was my old hard coded number
        if Libraries.core.IsBuy(side):
            multiplier = (1.0 + priceMovementPercentageAllowed)
            # maxKyberPriceAcceptable = arbitrageOpportunity.priceDict[side] * multiplier
            maxKyberPriceAcceptable = arbitrageOpportunity.pricesList_wrtPricesDict[pathIndex] * multiplier
            PrintAndLog_FuncNameHeader("maxKyberPriceAcceptable = " + str(
                maxKyberPriceAcceptable) + " based on priceMovementPercentageAllowed = " + str(priceMovementPercentageAllowed))
            minKyberRateAcceptable = ConvertPriceToRate(maxKyberPriceAcceptable, side)
        elif Libraries.core.IsSell(side):
            multiplier = (1.0 - priceMovementPercentageAllowed)
            # minKyberPriceAcceptable = arbitrageOpportunity.priceDict[side] * multiplier
            minKyberPriceAcceptable = arbitrageOpportunity.pricesList_wrtPricesDict[pathIndex] * multiplier
            PrintAndLog_FuncNameHeader("minKyberPriceAcceptable = " + str(
                minKyberPriceAcceptable) + " based on priceMovementPercentageAllowed = " + str(priceMovementPercentageAllowed))
            minKyberRateAcceptable = ConvertPriceToRate(minKyberPriceAcceptable, side)
        else:
            raise Exception("Not buy or sell?")

        minKyberRateAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minKyberRateAcceptable, Libraries.core.Ether_Decimals)

        # If we want to force the simple requirements to pass for debug purposes
        if Libraries.defaults.Ninja_ForcePassSimpleRequirements:
            minKyberRateAcceptable_weiUnits = 1
            self.SendMessageRegarding_Ninja_ForcePassSimpleRequirements()

        PrintAndLog_FuncNameHeader("side = " + str(side))
        PrintAndLog_FuncNameHeader("arbitrageOpportunity.metaDataDict_beforeTrade = " + str(arbitrageOpportunity.metaDataDict_beforeTrade))
        # TODO, support multiple kyber reserves, for now hard coding the first item in the list
        kyberReserve = str(arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex]['kyberReserveList'][0])
        PrintAndLog_FuncNameHeader("kyberReserve = " + str(kyberReserve))

        baseTokenAmount_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(baseTokenTradeAmount, baseToken)
        quoteTokenTradeAmount_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(quoteTokenTradeAmount, quoteToken)

        kwargs = None
        functionName = None
        if Libraries.core.IsBuy(side):
            #     function checkSimpleRequirements_buy_kyber(
            #         uint256 etherToSpend,
            #         address baseTokenAddress,
            #         address kyberReserve,
            #         uint256 minKyberRateAcceptable
            #         ) external returns (bool)
            functionName = 'checkSimpleRequirements_buy_kyber'
            kwargs = {
                'etherToSpend': int(quoteTokenTradeAmount_weiUnits),
                'baseTokenAddress': str(Libraries.nodes.Instance_Web3.toChecksumAddress(baseToken)),
                'kyberReserve': str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve)),
                'minKyberRateAcceptable': int(minKyberRateAcceptable_weiUnits),
            }

        elif Libraries.core.IsSell(side):
            #     function checkSimpleRequirements_sell_kyber(
            #         address baseTokenAddress,
            #         address kyberReserve,
            #         uint256 expectedBaseTokensToTrade,
            #         uint256 minKyberRateAcceptable
            #         ) external returns (bool)
            functionName = 'checkSimpleRequirements_sell_kyber'
            kwargs = {
                'baseTokenAddress': str(Libraries.nodes.Instance_Web3.toChecksumAddress(baseToken)),
                'kyberReserve': str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve)),
                'expectedBaseTokensToTrade': int(baseTokenAmount_weiUnits),
                'minKyberRateAcceptable': int(minKyberRateAcceptable_weiUnits),
            }

        else:
            raise Exception("Not buy or sell?")

        checkCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Exchange_Kyber, functionName)
        PrintAndLog_FuncNameHeader("checkCallDataArray_encoded for " + self.exchangeName + " = " + str(checkCallDataArray_encoded))
        # TODO, support multiple kyber reserves and return a list of results
        return [checkCallDataArray_encoded]

    def GenerateCallData_Trade(self, arbitrageOpportunity, pathIndex):
        from Contracts.contracts import Contract_Ninja_Exchange_Kyber
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        kwargs = None
        functionName = None
        if Libraries.core.IsBuy(side):
            #     function trade_buy_kyber(
            #         uint256 etherToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
            #         address tokenAddress
            #         ) external onlyNinjaProxy returns (uint256 tokensReceived)
            functionName = 'trade_buy_kyber'
            kwargs = {
                'etherToSpend': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
                'tokenAddress': str(Libraries.nodes.Instance_Web3.toChecksumAddress(baseToken)),
            }

        elif Libraries.core.IsSell(side):
            #     function trade_sell_kyber(
            #         uint256 tokensToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
            #         address tokenAddress
            #         ) external onlyNinjaProxy returns (uint256 etherReceived)
            functionName = 'trade_sell_kyber'
            kwargs = {
                'tokensToSpend': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
                'tokenAddress': str(Libraries.nodes.Instance_Web3.toChecksumAddress(baseToken)),
            }

        else:
            raise Exception("Not buy or sell?")

        tradeCallDataArray = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Exchange_Kyber, functionName, False, True)
        PrintAndLog_FuncNameHeader("tradeCallDataArray for " + self.exchangeName + " = " + str(tradeCallDataArray))
        # TODO, support multiple kyber reserves and return a list of results
        return [tradeCallDataArray]

    def FinalizeMetaDataBeforeTrade(self, arbitrageOpportunity, pathIndex):
        # Find the best kyberReserve that is offering the price we expect from the proxy's price function
        # To do this, make a call to get conversion rate given the quantity we're trading in arbitrageOpportunity

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        quoteTokenTradeAmount, baseTokenTradeAmount = arbitrageOpportunity.GetExpectedTokenQuantitiesTraded(pathIndex)

        kyberReserveList = Exchanges.kyber.GetAllKyberReservesTradingThisToken(baseToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        price_kyberProxy = arbitrageOpportunity.pricesList_wrtPricesDict[pathIndex]

        src = None
        dest = None
        srcQty_weiUnits = None
        if Libraries.core.IsBuy(side):
            src = quoteToken
            dest = baseToken
            srcQty_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(quoteTokenTradeAmount, quoteToken)
        elif Libraries.core.IsSell(side):
            src = baseToken
            dest = quoteToken
            baseTokenAmount = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokenTradeAmount, price_kyberProxy)
            srcQty_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(baseTokenAmount, baseToken)
        else:
            raise Exception("Not buy or sell)")

        resultDict = Exchanges.kyber.API_GetConversionRate_Batched_GivenOneList(kyberReserveList, src, dest, srcQty_weiUnits)
        PrintAndLog_FuncNameHeader("API_GetConversionRate_Batched_GivenOneList resultDict = " + str(resultDict))

        # Find the best kyberReserve
        best_kyberReserve = None
        best_price_kyberReserve = None
        for kyberReserve in resultDict:
            rate = Libraries.core.ConvertWeiToEther(resultDict[kyberReserve], Libraries.core.Ether_Decimals)
            price_kyberReserve = Exchanges.kyber.ConvertRateToPrice(rate, side)
            PrintAndLog_FuncNameHeader("kyberReserve = " + str(kyberReserve))
            PrintAndLog_FuncNameHeader("   price_kyberProxy = " + str(price_kyberProxy))
            PrintAndLog_FuncNameHeader("   price_kyberReserve = " + str(price_kyberReserve))
            if rate and rate > 0:
                # Move the kyberReserve price just a hair to avoid a rounding error
                # In theory, the best reserve price should be exactly the same as the best proxy price, but i'm doing this in case there's a rounding difference/error somewhere
                adjusted_price_kyberReserve = Libraries.core.MakePriceMoreCompetitiveByPercent(price_kyberReserve, 0.00001, Libraries.core.InvertOrderType(side))
                PrintAndLog_FuncNameHeader("   adjusted_price_kyberReserve = " + str(adjusted_price_kyberReserve))
                # if the slightly improved kyberReserve price is better compared to price_kyberProxy
                mostCompetitivePrice_proxyVsReserve = Libraries.core.GetMostCompetitivePriceInList(
                    [price_kyberProxy, adjusted_price_kyberReserve], Libraries.core.InvertOrderType(side))
                # if we have a new reserve price that's approx equal to the proxy price
                if mostCompetitivePrice_proxyVsReserve == adjusted_price_kyberReserve:
                    # If we have a new best reserve price
                    if not best_kyberReserve or Libraries.core.GetMostCompetitivePriceInList(
                            [best_price_kyberReserve, price_kyberReserve], Libraries.core.InvertOrderType(side)) == price_kyberReserve:
                        best_kyberReserve = kyberReserve
                        best_price_kyberReserve = price_kyberReserve
                        PrintAndLog_FuncNameHeader("Found a new best_kyberReserve " + str(best_kyberReserve) + " and best_price_kyberReserve " + str(best_price_kyberReserve))

            else:
                PrintAndLog_FuncNameHeader("   reserve has an invalid rate, ignoring it")

        # Set the metaDataDict_beforeTrade
        PrintAndLog_FuncNameHeader("arbitrageOpportunity.metaDataDict_beforeTrade before = " + str(arbitrageOpportunity.metaDataDict_beforeTrade))
        arbitrageOpportunity.metaDataDict_beforeTrade[pathIndex] = {
            'kyberReserveList': [best_kyberReserve],
        }
        PrintAndLog_FuncNameHeader("arbitrageOpportunity.metaDataDict_beforeTrade after = " + str(arbitrageOpportunity.metaDataDict_beforeTrade))

        return 1

    @staticmethod
    def GetEstimatedGasForTrade(fromAddress, token, sourceQuantity_etherUnits):
        import Contracts.contracts

        # I'm buying tokens, so the worstPriceIllAccept will be higher
        sourceTokenAddress = Exchanges.kyber.EtherToken
        destinationTokenAddress = token.erc20TokenContractAddress
        decimals_sourceQuantity = Libraries.core.Ether_Decimals

        sourceQuantity_weiUnits = Libraries.core.ConvertEtherToWei(sourceQuantity_etherUnits, decimals_sourceQuantity)

        kwargs = {
            'src': str(Libraries.nodes.Instance_Web3.toChecksumAddress(sourceTokenAddress)),
            'srcAmount': int(sourceQuantity_weiUnits),
            'dest': str(Libraries.nodes.Instance_Web3.toChecksumAddress(destinationTokenAddress)),
            'destAddress': str(Libraries.nodes.Instance_Web3.toChecksumAddress(fromAddress)),
            'maxDestAmount': int(Exchanges.kyber.MaxDestinationAmount),
            'minConversionRate': int(1),
            'walletId': str(Libraries.nodes.Instance_Web3.toChecksumAddress(Libraries.core.GetNullAddress())),
        }

        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = Contracts.contracts.Contract_KyberNetworkProxy.encodeABI('trade', kwargs=kwargs)
        value_wei_int = sourceQuantity_weiUnits
        toAddress = Exchanges.kyber.Contract_KyberNetworkProxy

        return Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex, value_wei_int,
                                              False, 1.10, Libraries.core.RequestTimeout_seconds)

    # Return a list of spender proxy addresses we need to approve token transfers for this exchange
    def GetTokenSpenderProxys(self, tokenToApprove):
        return [Exchanges.kyber.Contract_KyberNetworkProxy]

    def GetExpectedGasUsage_Check(self):
        return 50000

    def GetExpectedGasUsage_Trade(self):
        # TODO incorrect
        return 480000

    def DoEventLogsContainInteractionWithThisExchange(self, eventLogsList):
        # https://etherscan.io/tx/0x5db3ef0da174c9aba833c68df9b1f2c9c5db7a3ab5a0a4cc890bc09083435376#eventlog
        if Exchanges.kyber.Contract_KyberNetwork.lower() in str(eventLogsList).lower():
            return True
        else:
            return False

    # Get general exchange info at this block, given txReceipt
    def GetInfoAtBlockNumber(self, local_blockNumber, tokenAddressesToTraceList,
                             txReceipt, txReceiptList_forThisBlockNumber, textToPrepend):
        PrintAndLog('TODO, search for kyber cancels or price changes')

    # Some exchanges are extremely efficient in getting mass trade prices for many trade quantities
    # Some are not. For ones that are not, we want to minimize the number of calls eth_calls we make to the nodes
    def IsEfficientToGetMassPricesForManyTradeQuantities(self):
        return False


class Exchange_Uniswap(Exchange_Decentralized):

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        if quoteToken.lower() == Exchanges.keeperDAO.EthTokenContract.lower():
            exchange = Exchanges.uniswap.ContractDict_Exchange[baseToken.lower()]['exchange']
            # PrintAndLog_Header(header_logging, "returning exchange = " + str(exchange))
            return exchange
        else:
            raise Exception("Uniswap does not support this quoteToken " + str(quoteToken))

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        # Not needed
        return None

    # Some exchanges require ETH as a quote token,
    # which means that if you want to trade token for token you must make two trades (sell one token for ETH, then use that ETH to buy another token)
    # This flag is True when ETH is always the quote token on this exchange
    def DirectQuoteTokenIsRestrictedToEthOnly(self):
        return True

    def GetTradableTokens(self):
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        # We don't want to just return all the exchange's tokens because there's so many that are absolute junk and don't have any value in the exchange anyways
        tradeableTokensList = Exchanges.uniswap.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance()
        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)

    def SetMemoryCacheFromFileCache(self):
        Exchanges.uniswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswap.API_GetExchangeDataDict")

    # Delete all exchanges from memory cache that we do not care about
    # This improves performance
    def RestrictMemoryCacheToOnlyTheseTokens(self, quoteTokensList, allTokensList):
        # Delete all exchanges from memory cache that we do not care about
        # This improves performance
        PrintAndLog_FuncNameHeader("len(Exchanges.uniswap.ContractDict_Exchange) before = " + str(len(Exchanges.uniswap.ContractDict_Exchange)))
        for token in Exchanges.uniswap.ContractDict_Exchange.copy():
            if token.lower() not in allTokensList:
                del Exchanges.uniswap.ContractDict_Exchange[token]

        PrintAndLog_FuncNameHeader("len(Exchanges.uniswap.ContractDict_Exchange) after = " + str(len(Exchanges.uniswap.ContractDict_Exchange)))

    def GetPrices(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber,
                  resultDict, resultKey, lock_resultDict, detailed=False):
        self.EnforceEligibleQuoteToken(quoteToken)

        header_logging = Libraries.loggingConfig.GenerateHeaderForPrintAndLog(self.exchangeName)
        functionsStartDateTime = datetime.datetime.now()

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 1, Begin with quoteToken " + str(
            quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 2, calling API_GetExchangeBalances_Batched")
        balanceDict_ether, balanceDict_tokens = Exchanges.uniswap.API_GetExchangeBalances_Batched(baseTokenList, blockNumber)
        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceDict_ether = " + str(balanceDict_ether))
        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceDict_tokens = " + str(balanceDict_tokens))
        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "baseTokenList = " + str(baseTokenList))

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 3, parsing results")
        returnValue = {}
        returnValue[pdk_baseTokens] = {}

        for index_baseToken, baseToken in enumerate(baseTokenList):
            try:
                returnValue[pdk_baseTokens][baseToken] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_prices] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_metaData] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_metaData][pdk_exchangeQuoteTokenBalance_etherUnits] = balanceDict_ether[index_baseToken]

                for index, quoteTokensToSpend_etherUnits in enumerate(quoteTokensToSpendList_etherUnits):
                    try:
                        # Here is where I need to enforce token balances
                        # If the exchange does not have enough balance, don't provide prices for that trade quantity
                        # pretend the exchange has less balance to give use a safety buffer
                        exchangeBalanceQuoteTokens_withSafetyBuffer = balanceDict_ether[index_baseToken] * Libraries.defaults.BalancerExchangeBalanceSafetyBufferPercentageMultiplier
                        if exchangeBalanceQuoteTokens_withSafetyBuffer < quoteTokensToSpend_etherUnits:
                            if Libraries.defaults.VerboseLogging_GetPrices_Uniswap:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "Not able to trade on this exchange because it doesn't have enough quoteTokenBalance. "
                                                     "quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken) + ", exchangeBalanceQuoteTokens_withSafetyBuffer = " +
                                                     str(exchangeBalanceQuoteTokens_withSafetyBuffer) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                            # Update the cache prices to None and do not consider updating best_askPrice/best_bidPrice since this exchange cannot trade at these quantities
                            returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)] = {
                                pdk_quoteTokensToSpend_etherUnits: quoteTokensToSpend_etherUnits,
                                pdk_bid: None,
                                pdk_ask: None,
                            }
                            # We're done with this loop iteration
                            continue

                        askPrice = Exchanges.uniswap.CalculatePriceGivenBalances(
                            'buy', quoteTokensToSpend_etherUnits, balanceDict_ether[index_baseToken], balanceDict_tokens[index_baseToken])

                        # Now to calculate the bidPrice we have a chicken and egg problem here.
                        # In order to get the bidPrice, we need the source amount which is in baseTokens
                        # But in order to get that we need to get the equivalent based on quoteTokens which requires a price to convert
                        # But if we plug in the askPrice to convert quoteTokens to baseTokens, we'll end up with a baseTokens amount that's slightly off
                        # What we can do though is take that baseTokens amount that's slightly off, and plug it into our price formula to get a bidPrice
                        # Then re-do that previous step with the new price information we just calculated
                        # But there's a trick, don't just plug in that 2nd price we calculated, average both of them together because that will get you a "spot price"
                        # The reason we go with the "spot price" for this conversion here is because we want the baseToken equivalent of quoteTokens
                        baseTokensToSpend_toCalculateSpotPrice = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, askPrice)
                        bidPrice_toCalculateSpotPrice = Exchanges.uniswap.CalculatePriceGivenBalances(
                            'sell', baseTokensToSpend_toCalculateSpotPrice, balanceDict_ether[index_baseToken], balanceDict_tokens[index_baseToken])

                        # Now we can derive a spot price to convert quoteTokens to baseTokens
                        spotPrice = askPrice + bidPrice_toCalculateSpotPrice / 2.0

                        # Finally we can do the actual conversion using the spot price which is way more accurate
                        baseTokensToSpend = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, spotPrice)
                        bidPrice = Exchanges.uniswap.CalculatePriceGivenBalances(
                            'sell', baseTokensToSpend, balanceDict_ether[index_baseToken], balanceDict_tokens[index_baseToken])

                        returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)] = {
                            pdk_quoteTokensToSpend_etherUnits: quoteTokensToSpend_etherUnits,
                            pdk_bid: bidPrice,
                            pdk_ask: askPrice,
                        }

                        Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice,
                                                                    self.exchangeName, self.exchangeName, blockNumber, str(index))

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        # Generically show the error so I can easily search for it in the logs
                        self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken, quoteTokensToSpend_etherUnits)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                # Generically show the error so I can easily search for it in the logs
                self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken)

        if Libraries.defaults.VerboseLogging_SettingReturnValueForResultKey:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Setting returnValue for resultKey " + str(
                resultKey) + ", quoteToken = " + str(quoteToken) + ", detailed = " + str(detailed) + ", returnValue = " + str(returnValue))
        Libraries.utils.ConsiderSettingResultDict(returnValue, resultDict, resultKey, lock_resultDict)

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Successfully ended with quoteToken " + str(
            quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 4, returning")

    def GenerateCallData_Check(self, arbitrageOpportunity, pathIndex, tradingTechnique):
        from Contracts.contracts import Contract_Ninja_Exchange_Uniswap
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        exchangeQuoteTokenQuantity_etherUnits = arbitrageOpportunity.metaDataPath[pathIndex][pdk_exchangeQuoteTokenBalance_etherUnits]
        # Get the uniswap exchange given the baseToken
        uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[baseToken.lower()]['exchange'])

        # Determine minUniswapEthQuantityAcceptable/maxUniswapEthQuantityAcceptable based on exchangeQuoteTokenQuantity_etherUnits
        # priceMovementPercentageAllowed is how much we're willing to allow the price % to move before we reject it.
        # I like to set this based on expected profit percentage. So if profit percentage is 1%,
        #   and we want to reject the trade if profit percentage gets to 0.5%, then set this value to 0.5 so it draws the line at half
        priceMovementPercentageAllowed = arbitrageOpportunity.profitPercent * Libraries.defaults.AMMProfitPercentagePriceMovementMultiplier
        # priceMovementPercentageAllowed = Ninja_SimpleRequirements_WiggleRoomPercentage  # This was my old hard coded number
        maxUniswapEthQuantityAcceptable_weiUnits = None
        minUniswapEthQuantityAcceptable_weiUnits = None
        if Libraries.core.IsBuy(side):
            multiplier = (1.0 + priceMovementPercentageAllowed)
            maxUniswapEthQuantityAcceptable_etherUnits = exchangeQuoteTokenQuantity_etherUnits * multiplier
            maxUniswapEthQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxUniswapEthQuantityAcceptable_etherUnits, Libraries.core.Ether_Decimals)
        elif Libraries.core.IsSell(side):
            multiplier = (1.0 - priceMovementPercentageAllowed)
            minUniswapEthQuantityAcceptable_etherUnits = exchangeQuoteTokenQuantity_etherUnits * multiplier
            minUniswapEthQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minUniswapEthQuantityAcceptable_etherUnits, Libraries.core.Ether_Decimals)
        else:
            raise Exception("Not buy or sell?")

        # If we want to force the simple requirements to pass for debug purposes
        if Libraries.defaults.Ninja_ForcePassSimpleRequirements:
            maxUniswapEthQuantityAcceptable_weiUnits = int(Libraries.core.DataF_Int),
            minUniswapEthQuantityAcceptable_weiUnits = 1
            self.SendMessageRegarding_Ninja_ForcePassSimpleRequirements()

        kwargs = None
        functionName = None
        if Libraries.core.IsBuy(side):
            #     function checkSimpleRequirements_buy_uniswap(
            #         uint256 maxUniswapEthQuantityAcceptable,
            #         address uniswapContract
            #         ) external returns (bool)
            functionName = 'checkSimpleRequirements_buy_uniswap'
            kwargs = {
                'maxUniswapEthQuantityAcceptable': int(maxUniswapEthQuantityAcceptable_weiUnits),
                'uniswapContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
            }

        elif Libraries.core.IsSell(side):
            #     function checkSimpleRequirements_sell_uniswap(
            #         uint256 minUniswapEthQuantityAcceptable,
            #         address uniswapContract
            #         ) external returns (bool)
            functionName = 'checkSimpleRequirements_sell_uniswap'
            kwargs = {
                'minUniswapEthQuantityAcceptable': int(minUniswapEthQuantityAcceptable_weiUnits),
                'uniswapContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
            }

        else:
            raise Exception("Not buy or sell?")

        checkCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Exchange_Uniswap, functionName)
        PrintAndLog_FuncNameHeader("checkCallDataArray_encoded for " + self.exchangeName + " = " + str(checkCallDataArray_encoded))
        return [checkCallDataArray_encoded]

    def GenerateCallData_Trade(self, arbitrageOpportunity, pathIndex):
        from Contracts.contracts import Contract_Ninja_Exchange_Uniswap
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        # Get the uniswap exchange given the baseToken
        uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[baseToken.lower()]['exchange'])

        kwargs = None
        functionName = None
        if Libraries.core.IsBuy(side):
            #     function trade_buy_uniswap(
            #         uint256 etherToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
            #         address uniswapContract
            #         ) external onlyNinjaProxy returns (uint256 tokensReceived)
            functionName = 'trade_buy_uniswap'
            kwargs = {
                'etherToSpend': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
                'uniswapContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
            }

        elif Libraries.core.IsSell(side):
            #     function trade_sell_uniswap(
            #         uint256 tokensToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
            #         address uniswapContract
            #         ) external onlyNinjaProxy returns (uint256 etherReceived)
            functionName = 'trade_sell_uniswap'
            kwargs = {
                'tokensToSpend': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
                'uniswapContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
            }

        else:
            raise Exception("Not buy or sell?")

        tradeCallDataArray = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Exchange_Uniswap, functionName, False, True)
        PrintAndLog_FuncNameHeader("tradeCallDataArray for " + self.exchangeName + " = " + str(tradeCallDataArray))
        return [tradeCallDataArray]

    @staticmethod
    def GetEstimatedGasForTrade(publicAddress_Ninja, token, sourceQuantity_etherUnits):
        import Contracts.contracts

        uniswapContract = Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange']
        deadline_unixEpoch_seconds = int(time.time()) + 100000
        minReturnTokenAmount_weiUnits = 1

        kwargs = {
            'min_tokens': int(minReturnTokenAmount_weiUnits),
            'deadline': int(deadline_unixEpoch_seconds),
        }

        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = Contracts.contracts.Contract_UniswapExchange_dai.encodeABI('ethToTokenSwapInput', kwargs=kwargs)
        value_weiUnits = Libraries.core.ConvertEtherToWei(sourceQuantity_etherUnits, Libraries.core.Ether_Decimals)
        return Libraries.core.API_EstimateGas(uniswapContract, publicAddress_Ninja, data_hex, value_weiUnits,
                                              False, 1.10, Libraries.core.RequestTimeout_seconds)

    # Return a list of spender proxy addresses we need to approve token transfers for this exchange
    def GetTokenSpenderProxys(self, tokenToApprove):
        if tokenToApprove.lower() in Exchanges.uniswap.ContractDict_Exchange:
            return [Exchanges.uniswap.ContractDict_Exchange[tokenToApprove.lower()]['exchange']]
        else:
            return []

    def GetExpectedGasUsage_Check(self):
        return 3000

    def GetExpectedGasUsage_Trade(self):
        return 50000

    def GetEventLogsTradeTopics(self):
        from Libraries.topics import BuildTopicsArray_Uniswap_EthToTokenSwapInput, BuildTopicsArray_Uniswap_TokenToEthSwapInput
        # https://etherscan.io/tx/0x94a7b94b85f9263e50392048b39d070ad2a9ee0eae39b27d49610813daf11b0a
        # See if any topics for this exchange are found in eventLogsList

        topicsList = []
        for topic in BuildTopicsArray_Uniswap_EthToTokenSwapInput():
            topicsList.append(topic.lower())

        for topic in BuildTopicsArray_Uniswap_TokenToEthSwapInput():
            topicsList.append(topic.lower())

        # PrintAndLog_FuncNameHeader("topicsList = " + str(topicsList))
        return topicsList

    # Get general exchange info at this block, given txReceipt
    def GetInfoAtBlockNumber(self, local_blockNumber, tokenAddressesToTraceList,
                             txReceipt, txReceiptList_forThisBlockNumber, textToPrepend):
        eventLogs = Libraries.core.GetEventLogsFromTxReceipt(txReceipt)
        topicsList = self.GetEventLogsTradeTopics()
        # PrintAndLog("eventLogs = " + str(eventLogs))

        # We want to loop for block local_blockNumber as well as the one before it so we can compare before and after of the transaction
        for blockNumberToUse in range(local_blockNumber - 1, local_blockNumber + 1):
            PrintAndLog(textToPrepend + "blockNumberToUse = " + str(blockNumberToUse))
            for eventLog in eventLogs:
                for topic in topicsList:
                    if topic.lower() in str(eventLog).lower():
                        # We found the exchangeAddress, now get the asset balances
                        exchangeAddress = eventLog['address']
                        balance_ether = Libraries.core.API_GetEtherBalance(
                            exchangeAddress, None, None, None, Libraries.core.RequestTimeout_seconds, blockNumberToUse)
                        PrintAndLog(textToPrepend + "balance_ether = " + str(balance_ether) + " ETH")

                        walletAddressList = [exchangeAddress] * len(tokenAddressesToTraceList)
                        # PrintAndLog("walletAddressList = " + str(walletAddressList))
                        balanceList_token = Libraries.core.API_GetTokenBalance_Batched_Safe(
                            walletAddressList, tokenAddressesToTraceList, None, None, None, blockNumberToUse)
                        # Get all the token balances
                        for index, tokenAddress in enumerate(tokenAddressesToTraceList):
                            tokenSymbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)
                            PrintAndLog(textToPrepend + "balance_token = " + str(balanceList_token[index]) + " " + str(
                                tokenSymbol) + " (" + str(tokenAddress) + ")")

    # Return an array of strings where the strings are things I should look for in tx calldata when front running villains
    # For example, with Uniswap I may want to look up the uniswap exchange address based on the baseToken
    def GetMempoolAnalysisStrings(self, baseToken):
        # TODO test this out and see if it makes sense to include
        # uniswapContract = Exchanges.uniswap.ContractDict_Exchange[baseToken.lower()]['exchange'].lower()
        # return [uniswapContract]
        return []

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def PrepareRateFormulaMetaData(self, arbitrageOpportunity, pathIndex):
        header = "PrepareRateFormulaMetaData: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        # functionArguments must be a tuple, so if it only has one item it must be followed by a comma to make it a tuple
        functionArguments = baseToken, arbitrageOpportunity.blockNumberDiscovered
        functionCall = Exchanges.uniswap.API_GetExchangeBalances
        hash = Libraries.utils.GenerateHashOfFunctionCallAndArguments(functionCall, functionArguments)
        PrintAndLog_Header(header, "hash = " + str(hash))
        return functionCall, functionArguments, hash

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def GetRateFormulaMetaData(self, resultForApiCall, arbitrageOpportunity, pathIndex, metaDataResultDict, key):
        header = "GetRateFormulaMetaData: " + str(self.exchangeName)

        balance_ether, balance_tokens = resultForApiCall
        PrintAndLog_Header(header, "balance_ether = " + str(balance_ether))
        PrintAndLog_Header(header, "balance_tokens = " + str(balance_tokens))

        metaDataResultDict[key] = {
            'balance_ether': balance_ether,
            'balance_tokens': balance_tokens,
            'maxAmount_quoteTokens': balance_ether,
        }
        PrintAndLog_Header(header, "Set metaDataResultDict = " + str(metaDataResultDict))

    # Assume the same metaData structure as set by GetRateFormulaMetaData for this exchange
    # calculate the rate (not price) when trading according to this metaData on this exchange
    def GetRate_FinalizeQuoteTokenAmount(self, arbitrageOpportunity, pathIndex, inFlowAmount, metaData,
                                         tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        header = "GetRate_FinalizeQuoteTokenAmount: " + str(self.exchangeName)

        mySide = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        price = Exchanges.uniswap.CalculatePriceGivenBalances(
            mySide, inFlowAmount, metaData['balance_ether'], metaData['balance_tokens'])
        rate = Libraries.utils.ConvertPriceToRate(price, mySide)
        # PrintAndLog_Header(header, "Converted " + str(self.exchangeName) + " price = " + str(
        #     price) + " to a rate = " + str(rate) + ", mySide = " + str(mySide))
        return rate

    # Return True if this exchange is a candidate for Tailgating
    def CanTailgate(self):
        return True

    # These are addresses I want to monitor in the mempool for txs
    def GetTailgatingWatchAddresses(self):
        watchAddresses = []
        tokenAddressList = Exchanges.uniswap.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance()
        for tokenAddress in tokenAddressList:
            exchangeData = Exchanges.uniswap.ContractDict_Exchange[tokenAddress]
            watchAddresses.append(exchangeData['exchange'].lower())

        return watchAddresses

    def CreatePendingTradeTx(self, mempoolTx):
        # Find the uniswapAddress based on the mempoolTx.toAddress
        uniswapAddress = mempoolTx.toAddress
        tokenAddress = None
        tokenSymbol = None
        decimals_token = None
        for local_tokenAddress in Exchanges.uniswap.ContractDict_Exchange:
            exchangeData = Exchanges.uniswap.ContractDict_Exchange[local_tokenAddress]
            exchangeAddress = exchangeData['exchange'].lower()
            if exchangeAddress == mempoolTx.toAddress.lower():
                tokenAddress = local_tokenAddress
                tokenSymbol = exchangeData['symbol']
                decimals_token = float("1e" + str(exchangeData['decimals']))
                PrintAndLog_FuncNameHeader("Found Uniswap exchangeAddress, uniswapAddress = " + str(uniswapAddress))

        PrintAndLog_FuncNameHeader("uniswapAddress = " + str(uniswapAddress))
        PrintAndLog_FuncNameHeader("tokenAddress = " + str(tokenAddress))
        PrintAndLog_FuncNameHeader("tokenSymbol = " + str(tokenSymbol))
        PrintAndLog_FuncNameHeader("decimals_token = " + str(decimals_token))

        gasPrice_wei = mempoolTx.gasPrice_wei
        gasPrice_gwei = Libraries.core.ConvertWeiToGwei(gasPrice_wei)

        toAddress = mempoolTx.toAddress
        fromAddress = mempoolTx.fromAddress
        data = mempoolTx.data
        nonce = mempoolTx.nonce
        value = mempoolTx.value

        PrintAndLog_FuncNameHeader("mempoolTx.txHash = " + str(mempoolTx.txHash))
        PrintAndLog_FuncNameHeader("toAddress = " + str(toAddress))
        PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress))
        PrintAndLog_FuncNameHeader("gasPrice_gwei = " + str(gasPrice_gwei))
        PrintAndLog_FuncNameHeader("data = " + str(data))

        resultDict_transactionCount = {}
        resultDict_balances = {}

        key_dontCareWhatKeyIUse = "dontCareWhatKeyIUse"
        key_exchangeBalances_ether = "exchangeBalances_ether"
        key_exchangeBalances_token = "exchangeBalances_token"
        key_balance_ether = "balance_ether"
        key_balance_tokens = "balance_tokens"
        key_balance_tokenAllowance = "balance_tokenAllowance"

        lock_transactionCount = Lock()
        lock_balances = Lock()

        threads = []
        # We want to use a very short timeout here because this is time sensitive
        timeout_s = 1.3

        # Set a specifiedBlockNumber_int so we're consistent when we're making API calls over the next few seconds
        # Because we're analyzing pending transactions that could get mined in at any second
        specifiedBlockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

        # TODO, add an eth_estimateGas to see if this tx will succeed
        # Check the nonce of the transaction sender to make sure this transaction is eligible to be mined in
        t = threading.Thread(
            target=Libraries.core.API_GetTransactionCount,
            args=(fromAddress, False, resultDict_transactionCount, key_dontCareWhatKeyIUse,
                  lock_transactionCount, timeout_s,))
        threads.append(t)
        t.start()

        # Get exchange ether balance
        t = threading.Thread(
            target=Libraries.core.API_GetEtherBalance,
            args=(uniswapAddress, resultDict_balances, key_exchangeBalances_ether,
                  lock_balances, timeout_s, specifiedBlockNumber_int,))
        threads.append(t)
        t.start()

        # Get exchange token balance
        t = threading.Thread(
            target=Libraries.core.API_GetTokenBalance,
            args=(uniswapAddress, tokenAddress, decimals_token,
                  resultDict_balances, key_exchangeBalances_token, lock_balances,
                  timeout_s, specifiedBlockNumber_int,))
        threads.append(t)
        t.start()

        # Get fromAddress' ether balance
        t = threading.Thread(
            target=Libraries.core.API_GetEtherBalance,
            args=(fromAddress, resultDict_balances, key_balance_ether, lock_balances,
                  timeout_s, specifiedBlockNumber_int,))
        threads.append(t)
        t.start()

        # Get fromAddress' token balance
        t = threading.Thread(
            target=Libraries.core.API_GetTokenBalance,
            args=(fromAddress, tokenAddress, decimals_token,
                  resultDict_balances, key_balance_tokens, lock_balances,
                  timeout_s, specifiedBlockNumber_int,))
        threads.append(t)
        t.start()

        # Get fromAddress' token allowance
        t = threading.Thread(
            target=Libraries.core.API_GetTokenAllowance,
            args=(fromAddress, tokenAddress, uniswapAddress, decimals_token,
                  resultDict_balances, key_balance_tokenAllowance, lock_balances,
                  timeout_s, specifiedBlockNumber_int,))
        threads.append(t)
        t.start()

        for thread in threads:
            thread.join()

        # Check results
        transactionCount = resultDict_transactionCount[key_dontCareWhatKeyIUse]
        uniswapBalance_ether = resultDict_balances[key_exchangeBalances_ether]
        uniswapBalance_tokens = resultDict_balances[key_exchangeBalances_token]
        fromAddressBalance_ether = resultDict_balances[key_balance_ether]
        fromAddressBalance_tokens = resultDict_balances[key_balance_tokens]
        fromAddressTokenAllowance = resultDict_balances[key_balance_tokenAllowance]

        PrintAndLog_FuncNameHeader("transactionCount = " + str(transactionCount))
        PrintAndLog_FuncNameHeader("specifiedBlockNumber_int = " + str(specifiedBlockNumber_int))
        PrintAndLog_FuncNameHeader("uniswapBalance_ether = " + str(uniswapBalance_ether) + " ETH")
        PrintAndLog_FuncNameHeader("uniswapBalance_tokens = " + str(uniswapBalance_tokens) + " " + str(tokenSymbol))
        PrintAndLog_FuncNameHeader("fromAddressBalance_ether = " + str(fromAddressBalance_ether) + " ETH")
        PrintAndLog_FuncNameHeader("fromAddressBalance_tokens = " + str(fromAddressBalance_tokens) + " " + str(tokenSymbol))
        PrintAndLog_FuncNameHeader("fromAddressTokenAllowance = " + str(fromAddressTokenAllowance) + " " + str(tokenSymbol))

        passedValidation = True
        # If this transaction is next to be mined in
        PrintAndLog_FuncNameHeader("Nonce validation: nonce = " + str(nonce) + ", transactionCount = " + str(
            transactionCount) + ", for user address " + str(fromAddress))
        if nonce == transactionCount:
            PrintAndLog_FuncNameHeader("Nonce has passed validation. "
                                       "This transaction can be mined into a block at any moment")
        elif nonce > transactionCount:
            PrintAndLog_FuncNameHeader("Nonce has failed validation. "
                                       "This transaction is set to a future nonce and is awaiting another "
                                       "transaction to be mined in first before it's eligible")
            passedValidation = False
        else:
            PrintAndLog_FuncNameHeader("Nonce has failed validation. Is this nonce old? Was this transaction already mined in?")
            passedValidation = False

        if not passedValidation:
            PrintAndLog_FuncNameHeader("Failed validation, ignoring this transaction")
            # continue
            return None

        else:
            pendingTradeTx = None
            try:
                # Parse the pendingTradeTx, extract trading data, and predict exchange prices after this tx mines in
                pendingTradeTx = PendingTradeTx_Uniswap(
                    mempoolTx.txHash, self.exchangeName, data, value, specifiedBlockNumber_int, fromAddress,
                    uniswapAddress, uniswapBalance_ether, uniswapBalance_tokens,
                    gasPrice_wei, tokenAddress, tokenSymbol, decimals_token,
                    fromAddressBalance_ether, fromAddressBalance_tokens, fromAddressTokenAllowance)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except Libraries.exceptions.DeadlineExpiredException:
                PrintAndLog_FuncNameHeader("Found DeadlineExpiredException, gracefully skip this trade because it will revert")
                # continue
                return None

            except Libraries.exceptions.TradePriceRequirementException:
                PrintAndLog_FuncNameHeader("Found TradePriceRequirementException, gracefully skip this trade because it will revert")
                # continue
                return None

            except Libraries.exceptions.TradeNotFundedException:
                PrintAndLog_FuncNameHeader("Found TradeNotFundedException, gracefully skip this trade because it will revert")
                # continue
                return None

            except Libraries.exceptions.InsufficientTokenAllowanceException:
                PrintAndLog_FuncNameHeader("Found InsufficientTokenAllowanceException, gracefully skip this trade because it will revert")
                # continue
                return None

            except:
                PrintAndLogError("exception: " + traceback.format_exc())
                raise

            if not pendingTradeTx.tradeFunctionName:
                PrintAndLog_FuncNameHeader("Ignoring function " + str(
                    pendingTradeTx.tradeFunctionName) + " because either we aren't handling it yet or we aren't interested in it")
                pass

            else:
                PrintAndLog_FuncNameHeader("TailgateMemPool_Uniswap: MemPool sniffer found Uniswap trade mempoolTx.txHash = " + str(mempoolTx.txHash))
                return [pendingTradeTx]


class Exchange_UniswapV2_Generic(Exchange_Decentralized):

    def GetExchangeClass(self):
        raise Exception("Not yet implemented")

    # Delete all exchanges from memory cache that we do not care about
    # This improves performance by reducing the size of the memory cache
    def RestrictMemoryCacheToOnlyTheseTokens(self, quoteTokensList, allTokensList):
        contractDict = self.GetExchangeClass().ContractDict_Exchange
        PrintAndLog_FuncNameHeader("len(contractDict) " + str(self.exchangeName) + " before " + str(len(contractDict)))
        for key in contractDict.copy():
            atLeastOneTokenMatches = False
            for quoteToken in quoteTokensList:
                if quoteToken.lower() in key.lower():
                    # Enforce that the quoteTokenBalance is greater than ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth
                    # I will need to convert between quoteToken and ETH
                    quoteTokenBalance = self.GetExchangeClass().DetermineContractDictBalanceGivenTokenAndKey(key, quoteToken.lower())
                    priceForConversion = Libraries.priceOracle.GetOnChainPrice_FromCache(quoteToken)
                    effectiveEther_quoteTokenBalance = quoteTokenBalance * priceForConversion
                    if effectiveEther_quoteTokenBalance < Libraries.defaults.ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth:
                        # PrintAndLog_FuncNameHeader("Continuing because the effectiveEther_quoteTokenBalance was below our threshold.")
                        # PrintAndLog_FuncNameHeader("key = " + str(key))
                        # PrintAndLog_FuncNameHeader("quoteToken = " + str(quoteToken))
                        # PrintAndLog_FuncNameHeader("effectiveEther_quoteTokenBalance = " + str(effectiveEther_quoteTokenBalance))
                        # PrintAndLog_FuncNameHeader("quoteTokenBalance = " + str(quoteTokenBalance))
                        # PrintAndLog_FuncNameHeader("priceForConversion = " + str(priceForConversion))
                        continue

                    for token in allTokensList:
                        if token.lower() in key.lower():
                            atLeastOneTokenMatches = True
                            break
                    if atLeastOneTokenMatches:
                        break

            if not atLeastOneTokenMatches:
                # PrintAndLog_FuncNameHeader("Deleting key for " + str(self.exchangeName) + " " + str(key))
                del contractDict[key]

        PrintAndLog_FuncNameHeader("len(contractDict) " + str(self.exchangeName) + " after " + str(len(contractDict)))
        # PrintAndLog_FuncNameHeader("contractDict = " + str(contractDict))

    # Uniswap supports ETH, UniswapV2 does not, Kyber supports ETH, 0x does not, etc
    def SupportTradingEthDirectlyWithoutBeingWrapped(self):
        return False

    def GetTradableTokens(self):
        raise Exception("Not yet implemented")

    def GetPrices(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber,
                  resultDict, resultKey, lock_resultDict, detailed=False):
        self.EnforceEligibleQuoteToken(quoteToken)

        header_logging = Libraries.loggingConfig.GenerateHeaderForPrintAndLog(str(self.exchangeName) + "_" + str(quoteToken))
        functionsStartDateTime = datetime.datetime.now()

        # Filter down the list based on exchange balances for this quote token
        if Libraries.defaults.VerboseLogging_GetPrices_UniswapV2_Generic:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "baseTokenList before updating of len " + str(len(baseTokenList)) + " = " + str(baseTokenList))
        tokensThatHaveSomeDecentBalance = self.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance([quoteToken])
        if Libraries.defaults.VerboseLogging_GetPrices_UniswapV2_Generic:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "tokensThatHaveSomeDecentBalance of len " + str(len(tokensThatHaveSomeDecentBalance)) + " = " + str(tokensThatHaveSomeDecentBalance))
        # Remove tokens from baseTokenList that are not in tokensThatHaveSomeDecentBalance
        baseTokenList = Libraries.utils.RemoveItemsFromThisListThatAreNotFoundInThatList(baseTokenList, tokensThatHaveSomeDecentBalance)
        if Libraries.defaults.VerboseLogging_GetPrices_UniswapV2_Generic:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "baseTokenList after updating of len " + str(len(baseTokenList)) + " = " + str(baseTokenList))

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 1, Begin with quoteToken " + str(
            quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        decimals_quoteTokens = Libraries.core.GetDecimalsForTokenContract(quoteToken, True)
        exchangeContractList = []
        tokensList_quoteToken = []
        tokensList_baseToken = []
        decimalsList_quoteTokens = []
        decimalsList_baseTokens = []
        # Populate the lists we need to make the call
        for baseToken in baseTokenList:
            # Get the uniswap exchange given the tokens
            uniswapV2Contract = self.GetExchangeGivenTokenAddresses(quoteToken, baseToken)
            # It's possible that two tokens are listed on uniswap but not paired together therefor note tradable directly without multiple hops
            if not uniswapV2Contract:
                # Skip this one
                continue

            quoteToken_toAppendToList = EnforceTokenAddressIsERC20Version(quoteToken)
            baseToken_toAppendToList = EnforceTokenAddressIsERC20Version(baseToken)
            exchangeContractList.append(uniswapV2Contract)
            tokensList_quoteToken.append(quoteToken_toAppendToList)
            decimalsList_quoteTokens.append(decimals_quoteTokens)
            tokensList_baseToken.append(baseToken_toAppendToList)
            decimals_baseTokens = Libraries.core.GetDecimalsForTokenContract(baseToken, True)
            decimalsList_baseTokens.append(decimals_baseTokens)

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 2, quoteToken " + str(
            quoteToken) + " and matched exactly " + str(len(tokensList_baseToken)) + " baseTokens. Calling API_GetManyExchangeTokenBalances")

        balanceList_quoteTokens, balanceList_baseTokens = self.API_GetManyExchangeTokenBalances(
            exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens, tokensList_baseToken, decimalsList_baseTokens, blockNumber)

        if Libraries.defaults.VerboseLogging_GetPrices_UniswapV2_Generic:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "exchangeContractList of len " + str(len(exchangeContractList)) + " = " + str(exchangeContractList))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "tokensList_quoteToken of len " + str(len(tokensList_quoteToken)) + " = " + str(tokensList_quoteToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "tokensList_baseToken of len " + str(len(tokensList_baseToken)) + " = " + str(tokensList_baseToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceList_quoteTokens of len " + str(len(balanceList_quoteTokens)) + " = " + str(balanceList_quoteTokens))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceList_baseTokens of len " + str(len(balanceList_baseTokens)) + " = " + str(balanceList_baseTokens))

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 4, parsing results")
        if Libraries.defaults.VerboseLogging_GetPrices_UniswapV2_Generic:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokensToSpendList_etherUnits = " + str(quoteTokensToSpendList_etherUnits))

        returnValue = {}
        returnValue[pdk_baseTokens] = {}

        for index_baseToken, baseToken in enumerate(tokensList_baseToken):
            try:
                # if VerboseLogging_GetPrices_UniswapV2_Generic:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "iterating with index_baseToken = " + str(index_baseToken) + " and baseToken = " + str(baseToken))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "balanceList_quoteTokens[index_baseToken] = " + str(balanceList_quoteTokens[index_baseToken]))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "balanceList_baseTokens[index_baseToken] = " + str(balanceList_baseTokens[index_baseToken]))

                returnValue[pdk_baseTokens][baseToken] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_prices] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_metaData] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_metaData][pdk_exchangeQuoteTokenBalance_etherUnits] = balanceList_quoteTokens[index_baseToken]
                # if VerboseLogging_GetPrices_UniswapV2_Generic:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceList_quoteTokens[index_baseToken] = " + str(balanceList_quoteTokens[index_baseToken]))

                for index, quoteTokensToSpend_etherUnits in enumerate(quoteTokensToSpendList_etherUnits):
                    try:
                        # if VerboseLogging_GetPrices_UniswapV2_Generic:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "iterating with index = " + str(index) + " and quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))

                        # Here is where I need to enforce token balances
                        # If the exchange does not have enough balance, don't provide prices for that trade quantity
                        # pretend the exchange has less balance to give use a safety buffer
                        exchangeBalanceQuoteTokens_withSafetyBuffer = balanceList_quoteTokens[index_baseToken] * Libraries.defaults.BalancerExchangeBalanceSafetyBufferPercentageMultiplier
                        if exchangeBalanceQuoteTokens_withSafetyBuffer < quoteTokensToSpend_etherUnits:
                            if Libraries.defaults.VerboseLogging_GetPrices_UniswapV2_Generic:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "Not able to trade on this exchange because it doesn't have enough quoteTokenBalance. "
                                                     "quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken) + ", exchangeBalanceQuoteTokens_withSafetyBuffer = " +
                                                     str(exchangeBalanceQuoteTokens_withSafetyBuffer) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                            # Update the cache prices to None and do not consider updating best_askPrice/best_bidPrice since this exchange cannot trade at these quantities
                            returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)] = {
                                pdk_quoteTokensToSpend_etherUnits: quoteTokensToSpend_etherUnits,
                                pdk_bid: None,
                                pdk_ask: None,
                            }
                            # We're done with this loop iteration
                            continue

                        askPrice = Exchanges.uniswap.CalculatePriceGivenBalances(
                            'buy', quoteTokensToSpend_etherUnits, balanceList_quoteTokens[index_baseToken], balanceList_baseTokens[index_baseToken])
                        # if VerboseLogging_GetPrices_UniswapV2_Generic:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "askPrice = " + str(askPrice))

                        # Now to calculate the bidPrice we have a chicken and egg problem here.
                        # In order to get the bidPrice, we need the source amount which is in baseTokens
                        # But in order to get that we need to get the equivalent based on quoteTokens which requires a price to convert
                        # But if we plug in the askPrice to convert quoteTokens to baseTokens, we'll end up with a baseTokens amount that's slightly off
                        # What we can do though is take that baseTokens amount that's slightly off, and plug it into our price formula to get a bidPrice
                        # Then re-do that previous step with the new price information we just calculated
                        # But there's a trick, don't just plug in that 2nd price we calculated, average both of them together because that will get you a "spot price"
                        # The reason we go with the "spot price" for this conversion here is because we want the baseToken equivalent of quoteTokens
                        baseTokensToSpend_toCalculateSpotPrice = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, askPrice)
                        bidPrice_toCalculateSpotPrice = Exchanges.uniswap.CalculatePriceGivenBalances(
                            'sell', baseTokensToSpend_toCalculateSpotPrice, balanceList_quoteTokens[index_baseToken], balanceList_baseTokens[index_baseToken])

                        # Now we can derive a spot price to convert quoteTokens to baseTokens
                        spotPrice = (askPrice + bidPrice_toCalculateSpotPrice) / 2.0

                        # Finally we can do the actual conversion using the spot price which is way more accurate
                        baseTokensToSpend = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, spotPrice)
                        bidPrice = Exchanges.uniswap.CalculatePriceGivenBalances(
                            'sell', baseTokensToSpend, balanceList_quoteTokens[index_baseToken], balanceList_baseTokens[index_baseToken])
                        # if VerboseLogging_GetPrices_UniswapV2_Generic:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "bidPrice = " + str(bidPrice))

                        returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)] = {
                            pdk_quoteTokensToSpend_etherUnits: quoteTokensToSpend_etherUnits,
                            pdk_bid: bidPrice,
                            pdk_ask: askPrice,
                        }

                        Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, bidPrice, askPrice,
                                                                    self.exchangeName, self.exchangeName, blockNumber, str(index))

                        # ratesGraph_Dict, ratesGraph_Dict_ExchangeNames, ratesGraph_Dict_QuoteTokens = \
                        #     Libraries.ratesGraph.GetRatesGraphDicts(blockNumber, str(index))
                        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "JOEYZDEBUG Uniswapv2Generic GetPrices ratesGraph_Dict of len " + str(
                        #     len(ratesGraph_Dict)) + " = " + str(ratesGraph_Dict))

                        # if VerboseLogging_GetPrices_UniswapV2_Generic:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "Setting returnValue for baseToken " + str(baseToken) + ", index " + str(
                        #                              index) + ", uniswapV2Contract = " + str(
                        #                              exchangeContractList[index_baseToken] + ", returnValue = " + str(returnValue)))

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        # Generically show the error so I can easily search for it in the logs
                        self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken, quoteTokensToSpend_etherUnits)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                # Generically show the error so I can easily search for it in the logs
                self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken)

        if Libraries.defaults.VerboseLogging_SettingReturnValueForResultKey:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Setting returnValue for resultKey " + str(
                resultKey) + ", quoteToken = " + str(quoteToken) + ", detailed = " + str(detailed) + ", returnValue = " + str(returnValue))
        Libraries.utils.ConsiderSettingResultDict(returnValue, resultDict, resultKey, lock_resultDict)

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Successfully ended with quoteToken " + str(
            quoteToken) + " and " + str(len(tokensList_baseToken)) + " baseTokens")

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 5, returning")

    def GenerateCallData_Check(self, arbitrageOpportunity, pathIndex, tradingTechnique):
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData
        from Exchanges.ninja_arbitrage import TradingTechnique

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        # Extract any metadata I need
        exchangeQuoteTokenQuantity_etherUnits = arbitrageOpportunity.metaDataPath[pathIndex][pdk_exchangeQuoteTokenBalance_etherUnits]

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        # Get the exchange given the tokens
        exchangeContract = self.GetExchangeGivenTokenAddresses(
            arbitrageOpportunity.tradePath[pathIndex], arbitrageOpportunity.tradePath[pathIndex + 1])

        kwargs = None
        functionName = None

        # Checks will be different based tradingTechnique and ActiveConfig
        # TODO I'm having issues with checks failing a lot even in eth_estimateGas when they should succeed.
        #  I'm disabling time stamp checks for now
        # if tradingTechnique == TradingTechnique.Tailgate or \
        #         Libraries.config.ActiveConfig == Libraries.config.Config.Hide:

        if True:
            PrintAndLog_FuncNameHeader("tradingTechnique = " + str(tradingTechnique) + ", calling GetKwargs_Check_QuoteTokenQuantity")
            # Determine minQuoteTokenQuantityAcceptable/maxQuoteTokenQuantityAcceptable based on exchangeQuoteTokenQuantity_etherUnits
            # priceMovementPercentageAllowed is how much we're willing to allow the price % to move before we reject it.
            # I like to set this based on expected profit percentage. So if profit percentage is 1%,
            #   and we want to reject the trade if profit percentage gets to 0.5%, then set this value to 0.5 so it draws the line at half
            priceMovementPercentageAllowed = arbitrageOpportunity.profitPercent * Libraries.defaults.AMMProfitPercentagePriceMovementMultiplier
            # priceMovementPercentageAllowed = Ninja_SimpleRequirements_WiggleRoomPercentage  # This was my old hard coded number
            decimals_quoteToken = Libraries.core.GetDecimalsForTokenContract(quoteToken, True)

            if Libraries.core.IsBuy(side):
                multiplier = (1.0 + priceMovementPercentageAllowed)
                maxQuoteTokenQuantityAcceptable_etherUnits = exchangeQuoteTokenQuantity_etherUnits * multiplier
                maxQuoteTokenQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxQuoteTokenQuantityAcceptable_etherUnits, decimals_quoteToken)

                # If we want to force the simple requirements to pass for debug purposes
                if Libraries.defaults.Ninja_ForcePassSimpleRequirements:
                    maxQuoteTokenQuantityAcceptable_weiUnits = int(Libraries.core.DataF_Int),
                    self.SendMessageRegarding_Ninja_ForcePassSimpleRequirements()

                kwargs, functionName = self.GetKwargs_Check_QuoteTokenQuantity(side, quoteToken, maxQuoteTokenQuantityAcceptable_weiUnits, exchangeContract)
            elif Libraries.core.IsSell(side):
                multiplier = (1.0 - priceMovementPercentageAllowed)
                minQuoteTokenQuantityAcceptable_etherUnits = exchangeQuoteTokenQuantity_etherUnits * multiplier
                minQuoteTokenQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minQuoteTokenQuantityAcceptable_etherUnits, decimals_quoteToken)

                # If we want to force the simple requirements to pass for debug purposes
                if Libraries.defaults.Ninja_ForcePassSimpleRequirements:
                    minQuoteTokenQuantityAcceptable_weiUnits = 1
                    self.SendMessageRegarding_Ninja_ForcePassSimpleRequirements()

                kwargs, functionName = self.GetKwargs_Check_QuoteTokenQuantity(side, quoteToken, minQuoteTokenQuantityAcceptable_weiUnits, exchangeContract)
            else:
                raise Exception("Not buy or sell?")

        # elif tradingTechnique == TradingTechnique.Arbitrage:
        #     PrintAndLog_FuncNameHeader("tradingTechnique = " + str(tradingTechnique) + ", calling GetKwargs_Check_Timestamp")
        #     kwargs, functionName = self.GetKwargs_Check_Timestamp(exchangeContract)

        else:
            raise Exception("New TradingTechnique not yet implemented: " + str(tradingTechnique))

        checkCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, self.GetContract_Ninja_Exchange(), functionName)
        PrintAndLog_FuncNameHeader("checkCallDataArray_encoded for " + self.exchangeName + " = " + str(checkCallDataArray_encoded))
        return [checkCallDataArray_encoded]

    def GenerateCallData_Trade(self, arbitrageOpportunity, pathIndex):
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData
        from Contracts.contracts import Contract_Ninja_DiamondProxy

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        spendToken = Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(arbitrageOpportunity.tradePath[pathIndex]))
        receiveToken = Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(arbitrageOpportunity.tradePath[pathIndex + 1]))
        PrintAndLog_FuncNameHeader("pathIndex " + str(pathIndex) + "'s spendToken = " + str(
            spendToken) + ", receiveToken = " + str(receiveToken))

        # We want to send the trade's outToken to another UniswapV2 pool only if there is another UniswapV2 trade immediately following this trade
        # Let's determine that now

        # Assume the tokenOutRecipient is the ninja unless we determine we can forward the token we receive to the next UniswapV2 pool
        tokenOutRecipient = Contract_Ninja_DiamondProxy.address
        nextTradesPathIndex = pathIndex + 1
        # PrintAndLog_FuncNameHeader("comparing nextTradesPathIndex = " + str(nextTradesPathIndex) + " with len(arbitrageOpportunity.exchangeNamesList) " + str(
        #     len(arbitrageOpportunity.exchangeNamesList)))
        # If the nextTradesPathIndex is valid
        if nextTradesPathIndex < len(arbitrageOpportunity.exchangeNamesList):
            nextTradesExchangeName = arbitrageOpportunity.exchangeNamesList[nextTradesPathIndex]
            nextTradesExchange = Libraries.exchanges.ExchangeDict[nextTradesExchangeName]
            # If the next trade is UniswapV2
            if isinstance(nextTradesExchange, Exchange_UniswapV2_Generic):
                # Get nextTradesExchange's tokens and exchange pool we will be trading on
                nextTradesSpendToken = Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(arbitrageOpportunity.tradePath[nextTradesPathIndex]))
                nextTradesReceiveToken = Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(arbitrageOpportunity.tradePath[nextTradesPathIndex + 1]))
                tokenOutRecipient = nextTradesExchange.GetExchangeGivenTokenAddresses(nextTradesSpendToken, nextTradesReceiveToken)
                PrintAndLog_FuncNameHeader("nextTradesExchange " + str(nextTradesExchangeName) + " and nextTradesPathIndex " +
                                           str(nextTradesPathIndex) + " was found to be a Exchange_UniswapV2_Generic exchange. nextTradesSpendToken = " +
                                           str(nextTradesSpendToken) + ", nextTradesReceiveToken = " + str(nextTradesReceiveToken) +
                                           ", tokenOutRecipient = " + str(tokenOutRecipient))
            else:
                PrintAndLog_FuncNameHeader("nextTradesExchange " + str(nextTradesExchangeName) + " was found to NOT be a Exchange_UniswapV2_Generic")

        # TODO, if the previous trade is UniswapV2, doSendTokensToPool = False
        #  because we're assuming we've forwarded the output from the previous trade to this trade's UniswapV2 pool

        # Assume we are sending tokens unless we determine that the previous trade has already forwarded them to this UniswapV2 pool for us
        doSendTokensToPool = True
        previousTradesIndex = pathIndex - 1
        # PrintAndLog_FuncNameHeader("comparing previousTradesIndex = " + str(previousTradesIndex) + " with 0")
        # If the previousTradesIndex is valid
        if previousTradesIndex >= 0:
            previousTradesExchangeName = arbitrageOpportunity.exchangeNamesList[previousTradesIndex]
            previousTradesExchange = Libraries.exchanges.ExchangeDict[previousTradesExchangeName]
            # If the previous trade is UniswapV2
            if isinstance(previousTradesExchange, Exchange_UniswapV2_Generic):
                doSendTokensToPool = False
                PrintAndLog_FuncNameHeader("previousTradesExchange " + str(previousTradesExchangeName) +
                                           " was found to be a Exchange_UniswapV2_Generic exchange. "
                                           "We do not need to have Ninja send the tokens to the UniswapV2 pool because "
                                           "they will have already been forwarded to the pool for us. spendToken = "
                                           + str(spendToken) + ", receiveToken = " + str(receiveToken))
            else:
                PrintAndLog_FuncNameHeader("previousTradesExchange " + str(previousTradesExchangeName) + " was found to NOT be a Exchange_UniswapV2_Generic")

        PrintAndLog_FuncNameHeader("tokenOutRecipient = " + str(tokenOutRecipient) + ", doSendTokensToPool = " + str(doSendTokensToPool))

        #     function trade_uniswapV2_pool_spendToken0(
        #         uint256 amountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        #         bool doSendTokensToPool,
        #         address poolContract,
        #         address tokenOutRecipient  // Based on doSendTokensToPool, either another UniswapV2 pool or this proxy contract
        #         ) external onlyNinjaProxy returns (uint256 tokensReceived)
        #     function trade_uniswapV2_pool_spendToken1(
        #         uint256 amountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        #         bool doSendTokensToPool,
        #         address poolContract,
        #         address tokenOutRecipient  // Based on doSendTokensToPool, either another UniswapV2 pool or this proxy contract
        #         ) external onlyNinjaProxy returns (uint256 tokensReceived)
        poolContract = self.GetExchangeGivenTokenAddresses(spendToken, receiveToken)
        kwargs = {
            'amountIn': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
            'doSendTokensToPool': doSendTokensToPool,
            'poolContract': Libraries.nodes.Instance_Web3.toChecksumAddress(poolContract),
            'tokenOutRecipient': Libraries.nodes.Instance_Web3.toChecksumAddress(tokenOutRecipient),
        }
        functionName = None
        if self.IsSpendTokenExchangesToken0(spendToken, receiveToken):
            functionName = 'trade_uniswapV2_pool_spendToken0'
        elif self.IsSpendTokenExchangesToken1(spendToken, receiveToken):
            functionName = 'trade_uniswapV2_pool_spendToken1'
        else:
            raise Exception("spendToken is not token0 or token1, I must have a bug. spendToken = " + str(
                spendToken) + ", poolContract " + str(poolContract))

        PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs) + ", functionName = " + str(functionName))
        tradeCallDataArray = PreparePackedTradeCallData_GivenContractData(kwargs, self.GetContract_Ninja_Exchange(), functionName, False, True)
        PrintAndLog_FuncNameHeader("tradeCallDataArray for " + self.exchangeName + " = " + str(tradeCallDataArray))
        return [tradeCallDataArray]

    # Return a list of spender proxy addresses we need to approve token transfers for this exchange
    def GetTokenSpenderProxys(self, tokenToApprove):
        # I'm trading directly through each exchange pool instead of the router
        # And to avoid having to approve them ahead of time I built the approve directly into the smart contract
        # In the future if I want to trade through the proxy/router i'll have to re-enable this and issue approves again
        return []

    def GetExpectedGasUsage_Check(self):
        return 1600

    def GetExpectedGasUsage_Trade(self):
        return 78376

    def GetEventLogsTradeTopics(self):
        from Libraries.topics import BuildTopicsArray_UniswapV2_Swap
        # https://etherscan.io/tx/0x34d25c65a06528914a8900eaed3b918c4d1b7b309b78012b43be96f5357c3330#eventlog
        # See if any topics for this exchange are found in eventLogsList

        topicsList = []
        for topic in BuildTopicsArray_UniswapV2_Swap():
            topicsList.append(topic.lower())

        # PrintAndLog_FuncNameHeader("topicsList = " + str(topicsList))
        return topicsList

    def GetTokenAddressesGivenExchange(self, exchangeAddress):
        raise Exception("Not yet implemented")

    def GetExchangeGivenTokenAddresses(self, tokenAddress0, tokenAddress1):
        raise Exception("Not yet implemented")

    def API_GetManyExchangeTokenBalances(self, exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens,
                                         tokensList_baseToken, decimalsList_baseTokens, blockNumber):
        raise Exception("Not yet implemented")

    def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(self, quoteTokenList):
        raise Exception("Not yet implemented")

    def IsSpendTokenExchangesToken0(self, spendToken, receiveToken):
        raise Exception("Not yet implemented")

    def IsSpendTokenExchangesToken1(self, spendToken, receiveToken):
        raise Exception("Not yet implemented")

    def GetContract_Router(self):
        raise Exception("Not yet implemented")

    def GetContract_Ninja_Exchange(self):
        from Contracts.contracts import Contract_Ninja_Exchange_UniswapV2
        return Contract_Ninja_Exchange_UniswapV2

    def GetKwargs_Check_Timestamp(self, exchangeContract):
        #     function checkSimpleRequirements_timestamp_uniswapV2(
        #         address exchange
        #         ) external returns (bool)
        kwargs = {
            'exchange': Libraries.nodes.Instance_Web3.toChecksumAddress(exchangeContract),
        }
        return kwargs, 'checkSimpleRequirements_timestamp_uniswapV2'

    def GetKwargs_Check_QuoteTokenQuantity(self, side, quoteToken, quoteTokenQuantityThreshold_weiUnits, exchangeContract):
        if Libraries.core.IsBuy(side):
            #     function checkSimpleRequirements_buy_uniswapV2(
            #         address quoteToken,
            #         uint256 maxUniswapQuoteTokenQuantityAcceptable,
            #         address exchange
            #         ) external returns (bool)
            kwargs = {
                'quoteToken': str(Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(quoteToken))),
                'maxUniswapQuoteTokenQuantityAcceptable': int(quoteTokenQuantityThreshold_weiUnits),
                'exchange': Libraries.nodes.Instance_Web3.toChecksumAddress(exchangeContract),
            }
            return kwargs, 'checkSimpleRequirements_buy_uniswapV2'

        elif Libraries.core.IsSell(side):
            #     function checkSimpleRequirements_sell_uniswapV2(
            #         address quoteToken,
            #         uint256 minUniswapQuoteTokenQuantityAcceptable,
            #         address exchange
            #         ) external returns (bool)
            kwargs = {
                'quoteToken': str(Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(quoteToken))),
                'minUniswapQuoteTokenQuantityAcceptable': int(quoteTokenQuantityThreshold_weiUnits),
                'exchange': Libraries.nodes.Instance_Web3.toChecksumAddress(exchangeContract),
            }
            return kwargs, 'checkSimpleRequirements_sell_uniswapV2'

        else:
            raise Exception("Not buy or sell?")

    # Get general exchange info at this block, given txReceipt
    def GetInfoAtBlockNumber(self, local_blockNumber, tokenAddressesToTraceList,
                             txReceipt, txReceiptList_forThisBlockNumber, textToPrepend):
        eventLogs = Libraries.core.GetEventLogsFromTxReceipt(txReceipt)
        topicsList = self.GetEventLogsTradeTopics()
        # PrintAndLog("eventLogs = " + str(eventLogs))

        # We want to loop for block local_blockNumber as well as the one before it so we can compare before and after of the transaction
        for blockNumberToUse in range(local_blockNumber - 1, local_blockNumber + 1):
            PrintAndLog(textToPrepend + "blockNumberToUse = " + str(blockNumberToUse))
            for eventLog in eventLogs:
                for topic in topicsList:
                    if topic.lower() in str(eventLog).lower():
                        # We found the exchangeAddress, now get the asset balances
                        exchangeAddress = eventLog['address']

                        # Create a list of tokens whose balances we want based on the tokens in the exchange and tokenAddressesToTraceList
                        tokenAddress0, tokenAddress1 = self.GetTokenAddressesGivenExchange(exchangeAddress)
                        tokenAddressList_toUse = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(copy.deepcopy(tokenAddressesToTraceList))
                        if tokenAddress0 not in tokenAddressList_toUse:
                            tokenAddressList_toUse.append(tokenAddress0)
                        if tokenAddress1 not in tokenAddressList_toUse:
                            tokenAddressList_toUse.append(tokenAddress1)

                        walletAddressList = [exchangeAddress] * len(tokenAddressList_toUse)
                        # PrintAndLog("walletAddressList = " + str(walletAddressList))
                        balanceList_token = Libraries.core.API_GetTokenBalance_Batched_Safe(
                            walletAddressList, tokenAddressList_toUse, None, None, None, blockNumberToUse)
                        # Get all the token balances
                        for index, tokenAddress in enumerate(tokenAddressList_toUse):
                            tokenSymbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)
                            PrintAndLog(textToPrepend + "balance_token = " + str(balanceList_token[index]) + " " + str(
                                tokenSymbol) + " (" + str(tokenAddress) + ")")

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def PrepareRateFormulaMetaData(self, arbitrageOpportunity, pathIndex):
        header = "PrepareRateFormulaMetaData: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        exchangeContract = self.GetExchangeGivenTokenAddresses(quoteToken, baseToken)
        PrintAndLog_Header(header, "exchangeContract = " + str(exchangeContract))

        # functionArguments must be a tuple, so if it only has one item it must be followed by a comma to make it a tuple
        functionArguments = [exchangeContract], \
                            [EnforceTokenAddressIsERC20Version(quoteToken)], [Libraries.core.GetDecimalsForTokenContract(quoteToken, True)], \
                            [EnforceTokenAddressIsERC20Version(baseToken)], [Libraries.core.GetDecimalsForTokenContract(baseToken, True)], \
                            arbitrageOpportunity.blockNumberDiscovered
        functionCall = self.API_GetManyExchangeTokenBalances
        hash = Libraries.utils.GenerateHashOfFunctionCallAndArguments(functionCall, functionArguments)
        PrintAndLog_Header(header, "hash = " + str(hash))
        return functionCall, functionArguments, hash

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def GetRateFormulaMetaData(self, resultForApiCall, arbitrageOpportunity, pathIndex, metaDataResultDict, key):
        header = "GetRateFormulaMetaData: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        exchangeContract = self.GetExchangeGivenTokenAddresses(quoteToken, baseToken)

        balanceList_quoteTokens, balanceList_baseTokens = resultForApiCall
        PrintAndLog_Header(header, "balanceList_quoteTokens = " + str(balanceList_quoteTokens))
        PrintAndLog_Header(header, "balanceList_baseTokens = " + str(balanceList_baseTokens))

        metaDataResultDict[key] = {
            'balance_quoteTokens': balanceList_quoteTokens[0],
            'balance_baseTokens': balanceList_baseTokens[0],
            'maxAmount_quoteTokens': balanceList_quoteTokens[0],
            'exchangeContract': exchangeContract,
        }
        PrintAndLog_Header(header, "Set metaDataResultDict = " + str(metaDataResultDict))

    # Assume the same metaData structure as set by GetRateFormulaMetaData for this exchange
    # calculate the rate (not price) when trading according to this metaData on this exchange
    def GetRate_FinalizeQuoteTokenAmount(self, arbitrageOpportunity, pathIndex, inFlowAmount, metaData,
                                         tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        from Exchanges.ninja_arbitrage import TradingTechnique
        header = "GetRate_FinalizeQuoteTokenAmount: " + str(self.exchangeName)

        mySide = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        price = None
        if tradingTechnique == TradingTechnique.Tailgate:
            # For tailgating, I need to get the price based on the pendingTradeTx object
            # since it knows about the future uniswap price assuming this other tx mines in
            price = tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating.GetPrice_AfterTxMinesIn_GivenInflowAmount(mySide, inFlowAmount)
            PrintAndLog_Header(header, "Using price formula via pendingTradeTx's function call, price = " + str(price))
            PrintAndLog_Header(header, "tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating.GetTokensBeingTraded() = " + str(
                tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating.GetTokensBeingTraded()))
            PrintAndLog_Header(header, "tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating.exchangeName = " + str(
                tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating.exchangeName))
            PrintAndLog_Header(header, "tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating.side = " + str(
                tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating.side))
        elif tradingTechnique == TradingTechnique.Arbitrage:
            # For arbitraging, just use the regular uniswap algorithm with the uniswap balances from the metadata
            price = Exchanges.uniswap.CalculatePriceGivenBalances(
                mySide, inFlowAmount, metaData['balance_quoteTokens'], metaData['balance_baseTokens'])
            # PrintAndLog_Header(header, "Using price formula with metaData's uniswap balances, price = " + str(price))

        rate = Libraries.utils.ConvertPriceToRate(price, mySide)
        # PrintAndLog_Header(header, "Converted " + str(self.exchangeName) + " price = " + str(
        #     price) + " to a rate = " + str(rate) + ", mySide = " + str(mySide))
        return rate

    def CreatePendingTradeTx(self, mempoolTx):
        # 1.) Determine what tokens are being traded so I know which token balances to check on this exchange
        # I can safely assume there's only 1 array variable in the params and the last properties are addresses of tokens being traded
        # I an also assume that the number before the addresses is the number of addresses in the array
        # So start from the bottom and convert data properties to numbers until you find a number less than 20.
        # When you find that, that's the length of the array of addresses
        # [5]:  0000000000000000000000000000000000000000000000000000000000000002
        # [6]:  000000000000000000000000a0b86991c6218b36c1d19d4a2e9eb0ce3606eb48
        # [7]:  000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc2

        # Find the uniswapAddress based on the mempoolTx.toAddress
        # uniswapAddress = mempoolTx.toAddress
        # tokenAddress = None
        # tokenSymbol = None
        # decimals_token = None
        # for local_tokenAddress in Exchanges.uniswap.ContractDict_Exchange:
        #     exchangeData = Exchanges.uniswap.ContractDict_Exchange[local_tokenAddress]
        #     exchangeAddress = exchangeData['exchange'].lower()
        #     if exchangeAddress == mempoolTx.toAddress.lower():
        #         tokenAddress = local_tokenAddress
        #         tokenSymbol = exchangeData['symbol']
        #         decimals_token = float("1e" + str(exchangeData['decimals']))
        #         PrintAndLog_FuncNameHeader("Found Uniswap exchangeAddress, uniswapAddress = " + str(uniswapAddress))

        # PrintAndLog_FuncNameHeader("uniswapAddress = " + str(uniswapAddress))
        # PrintAndLog_FuncNameHeader("tokenAddress = " + str(tokenAddress))
        # PrintAndLog_FuncNameHeader("tokenSymbol = " + str(tokenSymbol))
        # PrintAndLog_FuncNameHeader("decimals_token = " + str(decimals_token))

        gasPrice_wei = mempoolTx.gasPrice_wei
        gasPrice_gwei = Libraries.core.ConvertWeiToGwei(gasPrice_wei)

        toAddress = mempoolTx.toAddress
        fromAddress = mempoolTx.fromAddress
        data = mempoolTx.data
        nonce = mempoolTx.nonce
        value = mempoolTx.value

        PrintAndLog_FuncNameHeader("mempoolTx.txHash = " + str(mempoolTx.txHash))
        PrintAndLog_FuncNameHeader("toAddress = " + str(toAddress))
        PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress))
        PrintAndLog_FuncNameHeader("gasPrice_gwei = " + str(gasPrice_gwei))
        PrintAndLog_FuncNameHeader("data = " + str(data))

        myStringIWantToSplit = data
        # Extract the function hash
        myStringIWantToSplit = myStringIWantToSplit[Libraries.core.LengthOfFunctionHash_Including0x:]
        splitLength = Libraries.core.LengthOfDataProperty
        dataSplitArray = [myStringIWantToSplit[i:i + splitLength] for i in range(0, len(myStringIWantToSplit), splitLength)]

        tokenAddressList = []
        decimalsList = []
        for i in range(len(dataSplitArray) - 1, 0, -1):
            PrintAndLog_FuncNameHeader("Iterating over index " + str(i) + " with " + str(dataSplitArray[i]))
            # convert the data property to a number and if it's less than 20 it's pretty much a guarantee that it's our array index
            data_int = Libraries.core.GetIntFromDataProperty_WeiUnits(dataSplitArray[i])
            PrintAndLog_FuncNameHeader("data_int = " + str(data_int))
            if data_int < 20:
                PrintAndLog_FuncNameHeader("We found the data that represents how many addresses we have. Have we passed " + str(
                    data_int) + " addresses? tokenAddressList of len " + str(len(tokenAddressList)) + " = " + str(tokenAddressList))
                break
            else:
                data_address = Libraries.core.GetAddressFromDataProperty(dataSplitArray[i])
                tokenAddressList.append(data_address)
                decimalsList.append(Libraries.core.GetDecimalsForTokenContract(data_address, True))

        # Fix the order of the lists so it matches the trade path
        tokenAddressList.reverse()
        decimalsList.reverse()
        # if len(tokenAddressList) != 2:
        #     raise Exception("Tailgating on UniswapV2 for a trade containing " + str(len(tokenAddressList)) + " tokens is not yet supported! TODO add feature")

        # Break tokenAddressList into lists of pairs of token addresses
        # So say the list is as follows: ['0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2', '0xdac17f958d2ee523a2206206994597c13d831ec7', '0x3e9bc21c9b189c09df3ef1b824798658d5011937']
        # We want this below results so we can make a pendingTradeTxList for each sub list
        # [ ['0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2', '0xdac17f958d2ee523a2206206994597c13d831ec7'],
        #   ['0xdac17f958d2ee523a2206206994597c13d831ec7', '0x3e9bc21c9b189c09df3ef1b824798658d5011937'] ]
        # Organize the list items into pairs, and put each pair in the list
        tokenPairList = []
        uniswapV2ExchangeList = []
        decimalsPairList = []
        for i in range(len(tokenAddressList) - 1):
            token0 = tokenAddressList[i]
            token1 = tokenAddressList[i + 1]
            tokenPairList.append([token0, token1])
            uniswapV2ExchangeList.append(self.GetExchangeGivenTokenAddresses(token0, token1))
            decimals0 = decimalsList[i]
            decimals1 = decimalsList[i + 1]
            decimalsPairList.append([decimals0, decimals1])

        PrintAndLog_FuncNameHeader("tokenPairList = " + str(tokenPairList))
        PrintAndLog_FuncNameHeader("uniswapV2ExchangeList = " + str(uniswapV2ExchangeList))

        pendingTradeTxList = []
        for index, uniswapV2TokenList in enumerate(tokenPairList):
            uniswapV2Exchange = uniswapV2ExchangeList[index]
            uniswapV2DecimalList = decimalsPairList[index]

            # TODO, look up the uniswap contracts for trades that involve these tokens.
            #  There may be multiple trades for now just support trades with only 2 tokens.
            #  In future I can break this into multiple trades and consider them all
            uniswapV2ExchangeList_forAPICall = [uniswapV2Exchange] * len(uniswapV2TokenList)

            PrintAndLog_FuncNameHeader("uniswapV2ExchangeList_forAPICall = " + str(uniswapV2ExchangeList_forAPICall))
            PrintAndLog_FuncNameHeader("uniswapV2TokenList = " + str(uniswapV2TokenList))
            PrintAndLog_FuncNameHeader("uniswapV2DecimalList = " + str(uniswapV2DecimalList))

            resultDict_transactionCount = {}
            resultDict_balances = {}
            resultDict_estimatedGas = {}

            key_estimatedGas = "estimatedGas"
            key_dontCareWhatKeyIUse = "dontCareWhatKeyIUse"
            # key_exchangeBalances_ether = "exchangeBalances_ether"
            key_exchangeBalances_token = "exchangeBalances_token"
            # key_balance_ether = "balance_ether"
            # key_balance_tokens = "balance_tokens"
            # key_balance_tokenAllowance = "balance_tokenAllowance"

            lock_transactionCount = Lock()
            lock_balances = Lock()

            threads = []
            # We want to use a very short timeout here because this is time sensitive
            timeout_s = 1.3

            # Set a specifiedBlockNumber_int so we're consistent when we're making API calls over the next few seconds
            # Because we're analyzing pending transactions that could get mined in at any second
            specifiedBlockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
            PrintAndLog_FuncNameHeader("specifiedBlockNumber_int = " + str(specifiedBlockNumber_int))

            # TODO, add an eth_estimateGas to see if this tx will succeed
            # Check the nonce of the transaction sender to make sure this transaction is eligible to be mined in
            t = threading.Thread(
                target=Libraries.core.API_GetTransactionCount,
                args=(fromAddress, False, resultDict_transactionCount, key_dontCareWhatKeyIUse,
                      lock_transactionCount, timeout_s,))
            threads.append(t)
            t.start()

            # Get exchange token balances
            t = threading.Thread(
                target=Libraries.core.API_GetTokenBalance_Batched_Safe,
                args=(uniswapV2ExchangeList_forAPICall, uniswapV2TokenList, uniswapV2DecimalList,
                      resultDict_balances, key_exchangeBalances_token,
                      specifiedBlockNumber_int,))
            threads.append(t)
            t.start()

            # TODO so much easier if I just do ETH estimate gas here...
            # Calling eth_estimateGas here just to see if it passes.
            # If it passes we can assume that the trade will succeed
            # If it reverts, we will assume the trade will fail
            t = threading.Thread(
                target=Libraries.core.API_EstimateGas,
                args=(toAddress, fromAddress, data, value, False, 1.0,
                      timeout_s, None, resultDict_estimatedGas, key_estimatedGas,))
            threads.append(t)
            t.start()

            # # Get fromAddress' ether balance
            # t = threading.Thread(
            #     target=Libraries.core.API_GetEtherBalance,
            #     args=(fromAddress, resultDict_balances, key_balance_ether, lock_balances,
            #           timeout_s, specifiedBlockNumber_int,))
            # threads.append(t)
            # t.start()

            # # Get fromAddress' token balance
            # t = threading.Thread(
            #     target=Libraries.core.API_GetTokenBalance,
            #     args=(fromAddress, tokenAddress, decimals_token,
            #           resultDict_balances, key_balance_tokens, lock_balances,
            #           timeout_s, specifiedBlockNumber_int,))
            # threads.append(t)
            # t.start()
            #
            # # Get fromAddress' token allowance
            # t = threading.Thread(
            #     target=Libraries.core.API_GetTokenAllowance,
            #     args=(fromAddress, tokenAddress, uniswapAddress, decimals_token,
            #           resultDict_balances, key_balance_tokenAllowance, lock_balances,
            #           timeout_s, specifiedBlockNumber_int,))
            # threads.append(t)
            # t.start()

            for thread in threads:
                thread.join()

            # Check results

            # If we are using the time machine to debug with an older txHash, estimatedGas will fail
            if Libraries.defaults.Tailgating_WithHardCodedTxHash:
                # let estimatedGas fail by not even checking for it's result
                estimatedGas = "DontCareBecauseDebugFeatureIsEnabled_Tailgating_WithHardCodedTxHash"
                # don't bother checking transactionCount because this will be wrong since I don't think it's possible to check historic transactionCount
                transactionCount = nonce
            else:
                estimatedGas = resultDict_estimatedGas[key_estimatedGas]
                transactionCount = resultDict_transactionCount[key_dontCareWhatKeyIUse]

            uniswapBalances_tokens = resultDict_balances[key_exchangeBalances_token]
            # fromAddressBalance_ether = resultDict_balances[key_balance_ether]
            # fromAddressBalance_tokens = resultDict_balances[key_balance_tokens]
            # fromAddressTokenAllowance = resultDict_balances[key_balance_tokenAllowance]

            PrintAndLog_FuncNameHeader("estimatedGas = " + str(estimatedGas))
            PrintAndLog_FuncNameHeader("transactionCount = " + str(transactionCount))
            PrintAndLog_FuncNameHeader("specifiedBlockNumber_int = " + str(specifiedBlockNumber_int))
            PrintAndLog_FuncNameHeader("uniswapBalances_tokens = " + str(uniswapBalances_tokens))
            # PrintAndLog_FuncNameHeader("fromAddressBalance_ether = " + str(fromAddressBalance_ether) + " ETH")
            # PrintAndLog_FuncNameHeader("fromAddressBalance_tokens = " + str(fromAddressBalance_tokens) + " " + str(tokenSymbol))
            # PrintAndLog_FuncNameHeader("fromAddressTokenAllowance = " + str(fromAddressTokenAllowance) + " " + str(tokenSymbol))

            passedValidation = True
            # If this transaction is next to be mined in
            PrintAndLog_FuncNameHeader("Nonce validation: nonce = " + str(nonce) + ", transactionCount = " + str(
                transactionCount) + ", for user address " + str(fromAddress))
            if nonce == transactionCount:
                PrintAndLog_FuncNameHeader("Nonce has passed validation. "
                                           "This transaction can be mined into a block at any moment")
            elif nonce > transactionCount:
                PrintAndLog_FuncNameHeader("Nonce has failed validation. "
                                           "This transaction is set to a future nonce and is awaiting another "
                                           "transaction to be mined in first before it's eligible")
                passedValidation = False
            else:
                PrintAndLog_FuncNameHeader("Nonce has failed validation. Is this nonce old? Was this transaction already mined in?")
                passedValidation = False

            if not passedValidation:
                PrintAndLog_FuncNameHeader("Failed validation, ignoring this transaction")
                # continue
                return None

            else:
                pendingTradeTx = None
                try:
                    isFirstTradeLeg = True
                    # Set to None so that it determines it based on the tx data
                    inputTokenAmount = None
                    if index > 0:
                        isFirstTradeLeg = False
                        # Set this based on the output amount from the previous trade
                        inputTokenAmount = pendingTradeTxList[index - 1].outputTokenAmount_etherUnits

                    PrintAndLog_FuncNameHeader("isFirstTradeLeg = " + str(isFirstTradeLeg))
                    PrintAndLog_FuncNameHeader("inputTokenAmount = " + str(inputTokenAmount))

                    # Parse the pendingTradeTx, extract trading data, and predict exchange prices after this tx mines in
                    pendingTradeTx = PendingTradeTx_UniswapV2(
                        mempoolTx.txHash, self.exchangeName, data, value, specifiedBlockNumber_int, fromAddress,
                        uniswapV2Exchange, uniswapV2TokenList, uniswapBalances_tokens, gasPrice_wei, isFirstTradeLeg, inputTokenAmount)

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except Libraries.exceptions.DeadlineExpiredException:
                    PrintAndLog_FuncNameHeader("Found DeadlineExpiredException, gracefully skip this trade because it will revert")
                    # continue
                    return None

                except Libraries.exceptions.TradePriceRequirementException:
                    PrintAndLog_FuncNameHeader("Found TradePriceRequirementException, gracefully skip this trade because it will revert")
                    # continue
                    return None

                except Libraries.exceptions.TradeNotFundedException:
                    PrintAndLog_FuncNameHeader("Found TradeNotFundedException, gracefully skip this trade because it will revert")
                    # continue
                    return None

                except Libraries.exceptions.InsufficientTokenAllowanceException:
                    PrintAndLog_FuncNameHeader("Found InsufficientTokenAllowanceException, gracefully skip this trade because it will revert")
                    # continue
                    return None

                except:
                    PrintAndLogError("exception: " + traceback.format_exc())
                    raise

                if not pendingTradeTx.tradeFunctionName:
                    PrintAndLog_FuncNameHeader("Ignoring function " + str(
                        pendingTradeTx.tradeFunctionName) + " because either we aren't handling it yet or we aren't interested in it")
                    pass

                else:
                    PrintAndLog_FuncNameHeader("TailgateMemPool_Uniswap: MemPool sniffer found Uniswap trade mempoolTx.txHash = " +
                                               str(mempoolTx.txHash))
                    pendingTradeTxList.append(pendingTradeTx)

        PrintAndLog_FuncNameHeader("returning " + str(len(pendingTradeTxList)) + " pendingTradeTxs as a list out of a possible " +
                                   str(len(tokenAddressList) - 1) + " internal trades")
        return pendingTradeTxList


class Exchange_UniswapV2(Exchange_UniswapV2_Generic):

    def GetExchangeClass(self):
        return Exchanges.uniswapV2

    def SetMemoryCacheFromFileCache(self):
        Exchanges.uniswapV2.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.uniswapV2.API_GetExchangeDataDict")

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        exchange = Exchanges.uniswapV2.GetExchangeGivenTokenAddresses(quoteToken, baseToken)
        # PrintAndLog_Header(header_logging, "returning exchange = " + str(exchange))
        return exchange

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        # Not needed
        return None

    def GetTradableTokens(self):
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        # We don't want to just return all the exchange's tokens because there's so many that are absolute junk and don't have any value in the exchange anyways
        tradeableTokensList = self.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(
            list(Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict().keys()))
        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)

    def GetTokenAddressesGivenExchange(self, exchangeAddress):
        return Exchanges.uniswapV2.GetTokenAddressesGivenExchange(exchangeAddress)

    def GetExchangeGivenTokenAddresses(self, tokenAddress0, tokenAddress1):
        return Exchanges.uniswapV2.GetExchangeGivenTokenAddresses(tokenAddress0, tokenAddress1)

    def API_GetManyExchangeTokenBalances(self, exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens,
                                         tokensList_baseToken, decimalsList_baseTokens, blockNumber):
        return Exchanges.uniswapV2.API_GetManyExchangeTokenBalances(
            exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens, tokensList_baseToken, decimalsList_baseTokens, blockNumber)

    def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(self, quoteTokenList):
        return Exchanges.uniswapV2.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(quoteTokenList)

    def IsSpendTokenExchangesToken0(self, spendToken, receiveToken):
        return Exchanges.uniswapV2.IsSpendTokenExchangesToken0(spendToken, receiveToken)

    def IsSpendTokenExchangesToken1(self, spendToken, receiveToken):
        return Exchanges.uniswapV2.IsSpendTokenExchangesToken1(spendToken, receiveToken)

    def GetContract_Router(self):
        import Contracts.contracts
        return Contracts.contracts.Contract_UniswapV2_Router

    # Return True if this exchange is a candidate for Tailgating
    def CanTailgate(self):
        return True

    # These are addresses I want to monitor in the mempool for txs
    def GetTailgatingWatchAddresses(self):
        import Contracts.contracts

        return [
            Contracts.contracts.Contract_UniswapV2_Router.address.lower(),
            Contracts.contracts.Contract_UniswapV2_Router_Old.address.lower(),
        ]


class Exchange_Sushiswap(Exchange_UniswapV2_Generic):

    def GetExchangeClass(self):
        return Exchanges.sushiswap

    def SetMemoryCacheFromFileCache(self):
        Exchanges.sushiswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.sushiswap.API_GetExchangeDataDict")

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        exchange = Exchanges.sushiswap.GetExchangeGivenTokenAddresses(quoteToken, baseToken)
        # PrintAndLog_Header(header_logging, "returning exchange = " + str(exchange))
        return exchange

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        # Not needed
        return None

    def GetTradableTokens(self):
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        # We don't want to just return all the exchange's tokens because there's so many that are absolute junk and don't have any value in the exchange anyways
        tradeableTokensList = self.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(
            list(Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict().keys()))
        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)

    def GetTokenAddressesGivenExchange(self, exchangeAddress):
        return Exchanges.sushiswap.GetTokenAddressesGivenExchange(exchangeAddress)

    def GetExchangeGivenTokenAddresses(self, tokenAddress0, tokenAddress1):
        return Exchanges.sushiswap.GetExchangeGivenTokenAddresses(tokenAddress0, tokenAddress1)

    def API_GetManyExchangeTokenBalances(self, exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens,
                                         tokensList_baseToken, decimalsList_baseTokens, blockNumber):
        return Exchanges.sushiswap.API_GetManyExchangeTokenBalances(
            exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens, tokensList_baseToken, decimalsList_baseTokens, blockNumber)

    def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(self, quoteTokenList):
        return Exchanges.sushiswap.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(quoteTokenList)

    def IsSpendTokenExchangesToken0(self, spendToken, receiveToken):
        return Exchanges.sushiswap.IsSpendTokenExchangesToken0(spendToken, receiveToken)

    def IsSpendTokenExchangesToken1(self, spendToken, receiveToken):
        return Exchanges.sushiswap.IsSpendTokenExchangesToken1(spendToken, receiveToken)

    def GetContract_Router(self):
        import Contracts.contracts
        return Contracts.contracts.Contract_Sushiswap_Router

    # Return True if this exchange is a candidate for Tailgating
    def CanTailgate(self):
        return True

    # These are addresses I want to monitor in the mempool for txs
    def GetTailgatingWatchAddresses(self):
        import Contracts.contracts

        return [
            Contracts.contracts.Contract_Sushiswap_Router.address.lower(),
        ]


class Exchange_Defiswap(Exchange_UniswapV2_Generic):

    def GetExchangeClass(self):
        return Exchanges.defiswap

    def SetMemoryCacheFromFileCache(self):
        Exchanges.defiswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.defiswap.API_GetExchangeDataDict")

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        exchange = Exchanges.defiswap.GetExchangeGivenTokenAddresses(quoteToken, baseToken)
        # PrintAndLog_Header(header_logging, "returning exchange = " + str(exchange))
        return exchange

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        # Not needed
        return None

    def GetTradableTokens(self):
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        # We don't want to just return all the exchange's tokens because there's so many that are absolute junk and don't have any value in the exchange anyways
        tradeableTokensList = self.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(
            list(Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict().keys()))
        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)

    def GetTokenAddressesGivenExchange(self, exchangeAddress):
        return Exchanges.defiswap.GetTokenAddressesGivenExchange(exchangeAddress)

    def GetExchangeGivenTokenAddresses(self, tokenAddress0, tokenAddress1):
        return Exchanges.defiswap.GetExchangeGivenTokenAddresses(tokenAddress0, tokenAddress1)

    def API_GetManyExchangeTokenBalances(self, exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens,
                                         tokensList_baseToken, decimalsList_baseTokens, blockNumber):
        return Exchanges.defiswap.API_GetManyExchangeTokenBalances(
            exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens, tokensList_baseToken, decimalsList_baseTokens, blockNumber)

    def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(self, quoteTokenList):
        return Exchanges.defiswap.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(quoteTokenList)

    def IsSpendTokenExchangesToken0(self, spendToken, receiveToken):
        return Exchanges.defiswap.IsSpendTokenExchangesToken0(spendToken, receiveToken)

    def IsSpendTokenExchangesToken1(self, spendToken, receiveToken):
        return Exchanges.defiswap.IsSpendTokenExchangesToken1(spendToken, receiveToken)

    def GetContract_Router(self):
        import Contracts.contracts
        return Contracts.contracts.Contract_Defiswap_Router

    # Return True if this exchange is a candidate for Tailgating
    def CanTailgate(self):
        return True

    # These are addresses I want to monitor in the mempool for txs
    def GetTailgatingWatchAddresses(self):
        import Contracts.contracts

        return [
            Contracts.contracts.Contract_Defiswap_Router.address.lower(),
        ]


class Exchange_Sakeswap(Exchange_UniswapV2_Generic):

    def GetExchangeClass(self):
        return Exchanges.sakeswap

    def SetMemoryCacheFromFileCache(self):
        Exchanges.sakeswap.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.sakeswap.API_GetExchangeDataDict")

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        exchange = Exchanges.sakeswap.GetExchangeGivenTokenAddresses(quoteToken, baseToken)
        # PrintAndLog_Header(header_logging, "returning exchange = " + str(exchange))
        return exchange

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        # Not needed
        return None

    def GetTradableTokens(self):
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        # We don't want to just return all the exchange's tokens because there's so many that are absolute junk and don't have any value in the exchange anyways
        tradeableTokensList = self.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(
            list(Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict().keys()))
        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)

    def GetTokenAddressesGivenExchange(self, exchangeAddress):
        return Exchanges.sakeswap.GetTokenAddressesGivenExchange(exchangeAddress)

    def GetExchangeGivenTokenAddresses(self, tokenAddress0, tokenAddress1):
        return Exchanges.sakeswap.GetExchangeGivenTokenAddresses(tokenAddress0, tokenAddress1)

    def API_GetManyExchangeTokenBalances(self, exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens,
                                         tokensList_baseToken, decimalsList_baseTokens, blockNumber):
        return Exchanges.sakeswap.API_GetManyExchangeTokenBalances(
            exchangeContractList, tokensList_quoteToken, decimalsList_quoteTokens, tokensList_baseToken, decimalsList_baseTokens, blockNumber)

    def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(self, quoteTokenList):
        return Exchanges.sakeswap.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(quoteTokenList)

    def IsSpendTokenExchangesToken0(self, spendToken, receiveToken):
        return Exchanges.sakeswap.IsSpendTokenExchangesToken0(spendToken, receiveToken)

    def IsSpendTokenExchangesToken1(self, spendToken, receiveToken):
        return Exchanges.sakeswap.IsSpendTokenExchangesToken1(spendToken, receiveToken)

    def GetContract_Router(self):
        import Contracts.contracts
        return Contracts.contracts.Contract_Sakeswap_Router

    # Return True if this exchange is a candidate for Tailgating
    def CanTailgate(self):
        return True

    # These are addresses I want to monitor in the mempool for txs
    def GetTailgatingWatchAddresses(self):
        import Contracts.contracts

        return [
            Contracts.contracts.Contract_Sakeswap_Router.address.lower(),
        ]


class Exchange_Balancer(Exchange_Decentralized):

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        # PrintAndLog_Header(header_logging, "returning metaDataIdentifier = " + str(metaDataIdentifier))
        return metaDataIdentifier

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        # header_logging = "GetMetaDataIdentifier(" + str(self.exchangeName) + "): "
        # I must choose pdk_best_bidPrice_exchange or pdk_best_askPrice_exchange based on side for this pathIndex
        # And side is based on the quoteToken WRT pricesDict which comes from sidesList_wrtPricesDict
        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # PrintAndLog_Header(header_logging, "side = " + str(side))
        pkBestExchangeKey = None
        if Libraries.core.IsBuy(side):
            pkBestExchangeKey = pdk_best_askPrice_exchange
            # PrintAndLog_Header(header_logging, "setting pkBestExchangeKey based on pdk_best_askPrice_exchange = " + str(pdk_best_askPrice_exchange))
        elif Libraries.core.IsSell(side):
            pkBestExchangeKey = pdk_best_bidPrice_exchange
            # PrintAndLog_Header(header_logging, "setting pkBestExchangeKey based on pdk_best_bidPrice_exchange = " + str(pdk_best_bidPrice_exchange))
        else:
            raise Exception("Not buy or sell??")

        # PrintAndLog_Header(header_logging, "pkBestExchangeKey = " + str(pkBestExchangeKey))
        bestExchange = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pkBestExchangeKey]
        # PrintAndLog_Header(header_logging, "bestExchange = " + str(bestExchange))
        return bestExchange

    # Uniswap supports ETH, UniswapV2 does not, Kyber supports ETH, 0x does not, etc
    def SupportTradingEthDirectlyWithoutBeingWrapped(self):
        return False

    def GetTradableTokens(self):
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        # We don't want to just return all the exchange's tokens because there's so many that are absolute junk and don't have any value in the exchange anyways
        tradeableTokensList = Exchanges.balancer.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(
            list(Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict().keys()))
        return Exchanges.keeperDAO.EnsureETHIsInTheTokenList(tradeableTokensList)

    def SetMemoryCacheFromFileCache(self):
        Exchanges.balancer.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.balancer.API_GetExchangeDataDict")

    # Delete all exchanges from memory cache that we do not care about
    # This improves performance
    def RestrictMemoryCacheToOnlyTheseTokens(self, quoteTokensList, allTokensList):
        PrintAndLog_FuncNameHeader("len(Exchanges.balancer.ContractDict_Exchange) after = " + str(len(Exchanges.balancer.ContractDict_Exchange)))
        for poolAddress in Exchanges.balancer.ContractDict_Exchange.copy():
            poolsTokensList = Exchanges.balancer.ContractDict_Exchange[poolAddress]['tokensList']

            atLeastOneTokenMatches = False
            for quoteToken in quoteTokensList:
                if quoteToken.lower() in poolsTokensList:
                    # Enforce that the quoteTokenBalance is greater than ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth
                    # I will need to convert between quoteToken and ETH
                    quoteTokenBalance = Exchanges.balancer.GetExchangesTokenBalance(poolAddress, quoteToken)
                    priceForConversion = Libraries.priceOracle.GetOnChainPrice_FromCache(quoteToken)
                    effectiveEther_quoteTokenBalance = quoteTokenBalance * priceForConversion
                    if effectiveEther_quoteTokenBalance < Libraries.defaults.ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth:
                        # PrintAndLog_FuncNameHeader("Continuing because the effectiveEther_quoteTokenBalance was below our threshold.")
                        # PrintAndLog_FuncNameHeader("poolAddress = " + str(poolAddress))
                        # PrintAndLog_FuncNameHeader("quoteToken = " + str(quoteToken))
                        # PrintAndLog_FuncNameHeader("effectiveEther_quoteTokenBalance = " + str(effectiveEther_quoteTokenBalance))
                        # PrintAndLog_FuncNameHeader("quoteTokenBalance = " + str(quoteTokenBalance))
                        # PrintAndLog_FuncNameHeader("priceForConversion = " + str(priceForConversion))
                        continue

                    for token in allTokensList:
                        if token.lower() in poolsTokensList:
                            atLeastOneTokenMatches = True
                            break
                    if atLeastOneTokenMatches:
                        break

            if not atLeastOneTokenMatches:
                # PrintAndLog_FuncNameHeader("Deleting poolAddress for " + str(self.exchangeName) + " " + str(poolAddress))
                del Exchanges.balancer.ContractDict_Exchange[poolAddress]

        PrintAndLog_FuncNameHeader("len(Exchanges.balancer.ContractDict_Exchange) after = " + str(len(Exchanges.balancer.ContractDict_Exchange)))

    def GetPrices(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber,
                  resultDict, resultKey, lock_resultDict, detailed=False):
        self.EnforceEligibleQuoteToken(quoteToken)

        header_logging = Libraries.loggingConfig.GenerateHeaderForPrintAndLog(self.exchangeName)
        functionsStartDateTime = datetime.datetime.now()

        # Filter down the list based on exchange balances for this quote token
        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "baseTokenList before updating of len " + str(len(baseTokenList)) + " = " + str(baseTokenList))
        tokensThatHaveSomeDecentBalance = Exchanges.balancer.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance([quoteToken])
        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "tokensThatHaveSomeDecentBalance of len " + str(len(tokensThatHaveSomeDecentBalance)) + " = " + str(tokensThatHaveSomeDecentBalance))
        # Remove tokens from baseTokenList that are not in tokensThatHaveSomeDecentBalance
        baseTokenList = Libraries.utils.RemoveItemsFromThisListThatAreNotFoundInThatList(baseTokenList, tokensThatHaveSomeDecentBalance)
        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "baseTokenList after updating of len " + str(len(baseTokenList)) + " = " + str(baseTokenList))

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 1, Begin with quoteToken " + str(
            quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        # Based on quoteToken and baseTokenList, determine what balancer pools to make calls to
        allBalancerPoolsList = list(Exchanges.balancer.ContractDict_Exchange.keys())
        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "allBalancerPoolsList of len " + str(len(allBalancerPoolsList)) + " = " + str(allBalancerPoolsList))

        quoteToken_toUse = EnforceTokenAddressIsERC20Version(quoteToken).lower()
        decimals_quoteToken = Libraries.core.GetDecimalsForTokenContract(quoteToken, True)
        subSetOfBalancerPoolsList = []
        # Keyed by the exchange, these are the baseTokens that are found on this exchange
        intersectingBaseTokensDict = {}
        for exchange in allBalancerPoolsList:
            # Make sure the exchange has assets worth even considering a trade
            quoteTokenBalanceThreshold = Exchanges.keeperDAO.GetExchangeMinQuoteTokenBalanceRequirementDict()[quoteToken.lower()]

            tokenList_balancer = Exchanges.balancer.GetExchangesTokenList(exchange)
            # We only care about exchanges with this quoteToken in them
            if quoteToken_toUse in tokenList_balancer:
                # We also require that at least one token in baseTokenList is found in that pool
                intersectingTokens = Libraries.utils.FindIntersectingValuesInLists(baseTokenList, tokenList_balancer)
                intersectingBaseTokensDict[exchange] = intersectingTokens
                if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "intersectingTokens = " + str(intersectingTokens) + ", tokenList_balancer = " + str(tokenList_balancer))
                if len(intersectingTokens) > 0:
                    quoteTokenBalance = Exchanges.balancer.GetExchangesTokenBalance(exchange, quoteToken_toUse)
                    # Require that the quoteTokenBalance exceeds the threshold so we know this token is worth trading
                    if float(quoteTokenBalance) > quoteTokenBalanceThreshold:
                        subSetOfBalancerPoolsList.append(exchange)
                    else:
                        pass
                        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "balancer's quoteTokenBalance was below our quoteTokenBalanceThreshold, so we're excluding it. balancerPool = " + str(
                                                     exchange) + ", quoteTokenBalance = " + str(quoteTokenBalance) + ", quoteTokenBalanceThreshold = " + str(quoteTokenBalanceThreshold))
                else:
                    pass
                    if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "We did not find at least 1 baseToken from baseTokenList in tokenList_balancer, so we're excluding this exchange. balancerPool = " + str(
                                                 exchange) + ", baseTokenList = " + str(baseTokenList) + ", tokenList_balancer = " + str(tokenList_balancer))

        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "subSetOfBalancerPoolsList of len " + str(len(subSetOfBalancerPoolsList)) + " = " + str(subSetOfBalancerPoolsList))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "intersectingBaseTokensDict = " + str(intersectingBaseTokensDict))

        tokensList_quoteToken = [quoteToken_toUse] * len(subSetOfBalancerPoolsList)
        decimalsList_quoteTokens = [decimals_quoteToken] * len(subSetOfBalancerPoolsList)

        # This going to be a list of balancerPools but some of them may repeat many times
        # because it needs to correspond with the tokensList_baseToken which is created from intersectingBaseTokensDict
        balancerPoolsList_FromIntersectingBaseTokensDict = []
        tokensList_baseToken = []
        decimalsList_baseTokens = []
        for exchange in intersectingBaseTokensDict:
            exchangesBaseTokenList = intersectingBaseTokensDict[exchange]
            for token in exchangesBaseTokenList:
                balancerPoolsList_FromIntersectingBaseTokensDict.append(exchange)
                tokensList_baseToken.append(token)
                decimalsList_baseTokens.append(Libraries.core.GetDecimalsForTokenContract(token, True))

        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "balancerPoolsList_FromIntersectingBaseTokensDict of len " + str(len(
                                     balancerPoolsList_FromIntersectingBaseTokensDict)) + " = " + str(balancerPoolsList_FromIntersectingBaseTokensDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "tokensList_baseToken of len " + str(len(tokensList_baseToken)) + " = " + str(tokensList_baseToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "decimalsList_baseTokens of len " + str(len(decimalsList_baseTokens)) + " = " + str(decimalsList_baseTokens))

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
                self.exchangeName) + " step 2, quoteToken " + str(
                quoteToken) + " and matched exactly " + str(len(baseTokenList)) + " baseTokens over " + str(
                len(allBalancerPoolsList)) + " balancer pools. Calling API_GetTokenBalance_Batched_Safe")

        threads = []
        resultsDict = {}
        before = datetime.datetime.now()

        key_quoteTokenBalances = "quoteTokenBalances"
        t = threading.Thread(
            target=Libraries.core.API_GetTokenBalance_Batched_Safe,
            args=(subSetOfBalancerPoolsList, tokensList_quoteToken, decimalsList_quoteTokens, resultsDict, key_quoteTokenBalances))
        threads.append(t)
        t.start()

        key_baseTokenBalances = "baseTokenBalances"
        t = threading.Thread(
            target=Libraries.core.API_GetTokenBalance_Batched_Safe,
            args=(balancerPoolsList_FromIntersectingBaseTokensDict, tokensList_baseToken, decimalsList_baseTokens, resultsDict, key_baseTokenBalances))
        threads.append(t)
        t.start()

        for thread in threads:
            thread.join()

        duration_s = (datetime.datetime.now() - before).total_seconds()
        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
                self.exchangeName) + " step 3, parsing results. API call duration_s = " + str(round(duration_s, 2)) + " seconds")

        balanceList_quoteTokens = resultsDict[key_quoteTokenBalances]
        balanceList_baseTokens = resultsDict[key_baseTokenBalances]
        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceList_quoteTokens = " + str(balanceList_quoteTokens))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceList_baseTokens = " + str(balanceList_baseTokens))

        # Use balanceList_quoteTokens and subSetOfBalancerPoolsList to re-construct a dict that's more meaningful
        # Keyed by the exchange, value is the quoteToken balance on that exchange
        meaningfulQuoteTokenBalanceDict = {}
        for index, balance_quoteTokens in enumerate(balanceList_quoteTokens):
            # Pair up the exchange with the balance
            exchange = subSetOfBalancerPoolsList[index]

            # Require that the quoteTokenBalance exceeds the threshold so we know this token is worth trading
            if Exchanges.keeperDAO.DoesThisQuoteTokenHaveEnoughBalanceWorthTradingFor(quoteToken, balance_quoteTokens, self.exchangeName):
                if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "Adding this exchange and it's balance to meaningfulQuoteTokenBalanceDict because it has enough "
                                         "balance to be worth trading. exchange = " + str(exchange) + ", balance_quoteTokens = " + str(balance_quoteTokens))
                meaningfulQuoteTokenBalanceDict[exchange] = balance_quoteTokens
            else:
                if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "Not adding this exchange and it's balance to meaningfulQuoteTokenBalanceDict because it doesn't have enough "
                                         "balance to be worth trading. exchange = " + str(exchange) + ", balance_quoteTokens = " + str(balance_quoteTokens))

        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "meaningfulQuoteTokenBalanceDict of len " + str(len(meaningfulQuoteTokenBalanceDict)) + " = " + str(meaningfulQuoteTokenBalanceDict))

        # Use balanceList_baseTokens to re-construct a dict that's more meaningful
        # Keyed by the exchange
        # value is a another dictionary where the key is the token address and the value is the balance
        meaningfulBaseTokenBalanceDict = {}
        for index, balance_baseTokens in enumerate(balanceList_baseTokens):
            exchange = balancerPoolsList_FromIntersectingBaseTokensDict[index]
            token = tokensList_baseToken[index]

            # Create a new value dict if it's not already there for this exchange
            if exchange not in meaningfulBaseTokenBalanceDict:
                meaningfulBaseTokenBalanceDict[exchange] = {}

            # Set the balance
            meaningfulBaseTokenBalanceDict[exchange][token] = balance_baseTokens

        # Create a dict where the baseToken is the key and the value is a list of all balancer pools it exists on
        baseTokenBalancerPoolsDict = {}
        for baseToken in baseTokenList:
            baseTokenBalancerPoolsDict[baseToken] = []
            for exchange in intersectingBaseTokensDict:
                # If this baseToken is in this exchange's list of tokens
                if baseToken in intersectingBaseTokensDict[exchange]:
                    baseTokenBalancerPoolsDict[baseToken].append(exchange)

        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "baseTokenBalancerPoolsDict of len " + str(len(baseTokenBalancerPoolsDict)) + " = " + str(baseTokenBalancerPoolsDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Tracing performance " + str(self.exchangeName) + " step 3.4, parsing results")

        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "meaningfulBaseTokenBalanceDict of len " + str(len(meaningfulBaseTokenBalanceDict)) + " = " + str(meaningfulBaseTokenBalanceDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "intersectingBaseTokensDict of len " + str(len(intersectingBaseTokensDict)) + " = " + str(intersectingBaseTokensDict))

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokensToSpendList_etherUnits for quoteToken " + str(quoteToken) + " of len " + str(
                len(quoteTokensToSpendList_etherUnits)) + " = " + str(quoteTokensToSpendList_etherUnits))

        # Ready the cache
        if quoteToken not in self.pricesCache:
            self.pricesCache[quoteToken] = {}

        # Record some stats on cache accesses
        cacheAccessCount = 0
        priceCalculateCount = 0

        returnValue = {}
        returnValue[pdk_baseTokens] = {}

        for index_baseToken, baseToken in enumerate(baseTokenList):
            try:
                # Ready the cache
                if baseToken not in self.pricesCache[quoteToken]:
                    self.pricesCache[quoteToken][baseToken] = {}

                returnValue[pdk_baseTokens][baseToken] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_prices] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_metaData] = {}

                exchangesThisBaseTokenIsTradedOn = baseTokenBalancerPoolsDict[baseToken]
                # if VerboseLogging_GetPrices_Balancer:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "exchangesThisBaseTokenIsTradedOn for baseToken " + str(baseToken) + " = " + str(exchangesThisBaseTokenIsTradedOn))

                # Optimize the below loop by remove as many disregarded or filtered out exchanges from exchangesThisBaseTokenIsTradedOn
                # We may have disregard or filtered out this exchange (example, it only has dust and no real value on it)
                # So if that's the case, we can remove them
                # if VerboseLogging_GetPrices_Balancer:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "exchangesThisBaseTokenIsTradedOn before optimization of len " + str(
                #         len(exchangesThisBaseTokenIsTradedOn)) + " = " + str(exchangesThisBaseTokenIsTradedOn))
                for exchange in copy.deepcopy(exchangesThisBaseTokenIsTradedOn):
                    if exchange not in meaningfulQuoteTokenBalanceDict:
                        # if VerboseLogging_GetPrices_Balancer:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "Deleting this exchange from exchangesThisBaseTokenIsTradedOn "
                        #                          "because it was not found in meaningfulQuoteTokenBalanceDict. It must have"
                        #                          " been intentionally filtered out. exchange = " + str(exchange) + ", baseToken = " + str(baseToken))
                        exchangesThisBaseTokenIsTradedOn.remove(exchange)
                    else:
                        pass
                        # if VerboseLogging_GetPrices_Balancer:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "NOT Deleting this exchange from exchangesThisBaseTokenIsTradedOn "
                        #                          "because it was not found in meaningfulQuoteTokenBalanceDict. It must have"
                        #                          " been intentionally filtered out. exchange = " + str(exchange) + ", baseToken = " + str(baseToken))

                # if VerboseLogging_GetPrices_Balancer:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "baseToken " + str(baseToken) + " has " + str(
                #         len(exchangesThisBaseTokenIsTradedOn)) + " exchangesThisBaseTokenIsTradedOn after optimization = " + str(exchangesThisBaseTokenIsTradedOn))

                for index, quoteTokensToSpend_etherUnits in enumerate(quoteTokensToSpendList_etherUnits):
                    try:
                        # if VerboseLogging_GetPrices_Balancer:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "iterating with quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                        # Ready the cache
                        if str(index) not in self.pricesCache[quoteToken][baseToken]:
                            self.pricesCache[quoteToken][baseToken][str(index)] = {}

                        # Determine the best bidPrice and askPrice for all balancer pools based on the information we gathered
                        best_bidPrice = None
                        best_askPrice = None
                        best_bidPrice_exchange = None
                        best_askPrice_exchange = None
                        best_bidPrice_exchange_quoteTokenBalance = None
                        best_askPrice_exchange_quoteTokenBalance = None
                        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
                            before_exchangesThisBaseTokenIsTradedOn = datetime.datetime.now()
                        for exchange in exchangesThisBaseTokenIsTradedOn:
                            try:
                                # Ready the cache
                                if exchange not in self.pricesCache[quoteToken][baseToken][str(index)]:
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange] = {
                                        pdk_exchangeQuoteTokenBalance_etherUnits: None,
                                        pdk_exchangeBaseTokenBalance_etherUnits: None,
                                        pdk_value: None,
                                    }

                                # Get the balances from the balancer pool
                                exchangeBalanceQuoteTokens = meaningfulQuoteTokenBalanceDict[exchange]
                                exchangeBalanceBaseTokens = meaningfulBaseTokenBalanceDict[exchange][baseToken]

                                # this where I check quoteTokenBalance vs quoteTokensToSpend_etherUnits
                                # quoteTokenBalance_exchange = Exchanges.balancer.GetExchangesTokenBalance(exchange, quoteToken_toUse)
                                # Here is where I need to enforce token balances
                                # If the exchange does not have enough balance, don't provide prices for that trade quantity
                                # pretend the exchange has less balance to give use a safety buffer
                                exchangeBalanceQuoteTokens_withSafetyBuffer = exchangeBalanceQuoteTokens * Libraries.defaults.BalancerExchangeBalanceSafetyBufferPercentageMultiplier
                                if exchangeBalanceQuoteTokens_withSafetyBuffer < quoteTokensToSpend_etherUnits:
                                    # if VerboseLogging_GetPrices_Balancer:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                    #                          "Not able to trade on this exchange because it doesn't have enough quoteTokenBalance. "
                                    #                          "exchange = " + str(exchange) + ", exchangeBalanceQuoteTokens_withSafetyBuffer = " +
                                    #                          str(exchangeBalanceQuoteTokens_withSafetyBuffer) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                                    # Update the cache prices to None and do not consider updating best_askPrice/best_bidPrice since this exchange cannot trade at these quantities
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_exchangeQuoteTokenBalance_etherUnits] = \
                                        exchangeBalanceQuoteTokens
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_exchangeBaseTokenBalance_etherUnits] = \
                                        exchangeBalanceBaseTokens
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_bid] = None
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_ask] = None
                                    # We're done with this loop iteration
                                    continue

                                # TODO, check to see if the exchangeBalances have changed
                                #  if not, return the cached values
                                #  if they have changed, calculate price again
                                #  I could use the pricesDict as that cache, the problem with that is i may want to cache more data than what's in pricesDict
                                #  For example pricesDict does not cache the baseToken balance only the quoteToken balance, here I'll want both to be safe

                                exchangeBalancesHaveChanged = False
                                # If the exchange balances have changed
                                if exchangeBalanceQuoteTokens != self.pricesCache[quoteToken][baseToken][str(index)][exchange][
                                    pdk_exchangeQuoteTokenBalance_etherUnits] or \
                                        exchangeBalanceBaseTokens != self.pricesCache[quoteToken][baseToken][str(index)][exchange][
                                    pdk_exchangeBaseTokenBalance_etherUnits]:
                                    exchangeBalancesHaveChanged = True

                                # If we've disabled this exchange's cache, assume we have new data so it always calculates it
                                if not Libraries.defaults.EnableBalancersCacheToOptimizeGetPrices:
                                    # if VerboseLogging_GetPrices_Balancer:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                    #                          "EnableBalancersCacheToOptimizeGetPrices was disabled, "
                                    #                          "forcing this exchange to re-calculate prices as opposed to using cache")
                                    exchangeBalancesHaveChanged = True

                                if exchangeBalancesHaveChanged:
                                    # Get the new prices and update the cache
                                    priceCalculateCount += 1
                                    # if VerboseLogging_GetPrices_Balancer:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                    #                          "calculating new exchange price for Balancer pool " + str(
                                    #                              exchange) + ", quoteToken = " + str(quoteToken) + ", baseToken = " + str(
                                    #                              baseToken) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))

                                    askPrice = Exchanges.balancer.CalculateTradePrice(
                                        'buy', exchange, EnforceTokenAddressIsERC20Version(quoteToken),
                                        baseToken, quoteTokensToSpend_etherUnits,
                                        exchangeBalanceQuoteTokens, exchangeBalanceBaseTokens)

                                    # Now to calculate the bidPrice we have a chicken and egg problem here.
                                    # In order to get the bidPrice, we need the source amount which is in baseTokens
                                    # But in order to get that we need to get the equivalent based on quoteTokens which requires a price to convert
                                    # But if we plug in the askPrice to convert quoteTokens to baseTokens, we'll end up with a baseTokens amount that's slightly off
                                    # What we can do though is take that baseTokens amount that's slightly off, and plug it into our price formula to get a bidPrice
                                    # Then re-do that previous step with the new price information we just calculated
                                    # But there's a trick, don't just plug in that 2nd price we calculated, average both of them together because that will get you a "spot price"
                                    # The reason we go with the "spot price" for this conversion here is because we want the baseToken equivalent of quoteTokens
                                    baseTokensToSpend_toCalculateSpotPrice = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, askPrice)
                                    bidPrice_toCalculateSpotPrice = Exchanges.balancer.CalculateTradePrice(
                                        'sell', exchange, baseToken,
                                        EnforceTokenAddressIsERC20Version(quoteToken), baseTokensToSpend_toCalculateSpotPrice,
                                        exchangeBalanceBaseTokens, exchangeBalanceQuoteTokens)

                                    # Now we can derive a spot price to convert quoteTokens to baseTokens
                                    spotPrice = (askPrice + bidPrice_toCalculateSpotPrice) / 2.0

                                    # Finally we can do the actual conversion using the spot price which is way more accurate
                                    baseTokensToSpend = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, spotPrice)
                                    bidPrice = Exchanges.balancer.CalculateTradePrice(
                                        'sell', exchange, baseToken,
                                        EnforceTokenAddressIsERC20Version(quoteToken), baseTokensToSpend,
                                        exchangeBalanceBaseTokens, exchangeBalanceQuoteTokens)

                                    # Update the cache so we can use it later without having to re-calculate every block
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_exchangeQuoteTokenBalance_etherUnits] = \
                                        exchangeBalanceQuoteTokens
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_exchangeBaseTokenBalance_etherUnits] = \
                                        exchangeBalanceBaseTokens
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_bid] = bidPrice
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_ask] = askPrice

                                else:
                                    # Get the prices from cache
                                    cacheAccessCount += 1

                                    bidPrice = self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_bid]
                                    askPrice = self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_ask]

                                # Determine the best price
                                # if not best_askPrice or askPrice < best_askPrice:
                                if askPrice and (not best_askPrice or askPrice < best_askPrice):
                                    best_askPrice = askPrice
                                    best_askPrice_exchange = exchange
                                    best_askPrice_exchange_quoteTokenBalance = exchangeBalanceQuoteTokens
                                    # if VerboseLogging_GetPrices_Balancer:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Found new best_askPrice. best_askPrice = " + str(best_askPrice))

                                # Determine the best price
                                # if not best_bidPrice or bidPrice > best_bidPrice:
                                if bidPrice and (not best_bidPrice or bidPrice > best_bidPrice):
                                    best_bidPrice = bidPrice
                                    best_bidPrice_exchange = exchange
                                    best_bidPrice_exchange_quoteTokenBalance = exchangeBalanceQuoteTokens
                                    # if VerboseLogging_GetPrices_Balancer:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Found new best_bidPrice. best_bidPrice = " + str(best_bidPrice))

                            except (KeyboardInterrupt, SystemExit):
                                print('\nkeyboard interrupt caught')
                                print('\n...Program Stopped Manually!')
                                raise

                            except:
                                PrintAndLogError("exception when parsing exchangesThisBaseTokenIsTradedOn " + traceback.format_exc())

                        # PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                      "Updating returnValue for pricesDict with quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken) +
                        #                      ", str(index) = " + str(index) + ", best_bidPrice = " + str(best_bidPrice) + ", best_askPrice = " + str(best_askPrice))

                        returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)] = {
                            pdk_quoteTokensToSpend_etherUnits: quoteTokensToSpend_etherUnits,
                            pdk_bid: best_bidPrice,
                            pdk_ask: best_askPrice,
                        }

                        # TODO, in the future i'll want to include all possible profitable paths instead of just the one best
                        returnValue[pdk_baseTokens][baseToken][pdk_metaData][str(index)] = {
                            pdk_best_bidPrice_exchange: best_bidPrice_exchange,
                            pdk_best_askPrice_exchange: best_askPrice_exchange,
                            pdk_best_bidPrice_exchange_quoteTokenBalance: best_bidPrice_exchange_quoteTokenBalance,
                            pdk_best_askPrice_exchange_quoteTokenBalance: best_askPrice_exchange_quoteTokenBalance,
                        }

                        # PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                      "Calling UpdateNodeInRatesGraph with quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken) +
                        #                      ", str(index) = " + str(index) + ", best_bidPrice = " + str(best_bidPrice) + ", best_askPrice = " + str(best_askPrice))
                        Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, best_bidPrice, best_askPrice,
                                                                    self.exchangeName, self.exchangeName, blockNumber, str(index))

                        if Libraries.defaults.VerboseLogging_GetPrices_Balancer:
                            duration_s = (datetime.datetime.now() - before_exchangesThisBaseTokenIsTradedOn).total_seconds()
                            PrintAndLog_Detailed(
                                header_logging, functionsStartDateTime,
                                "duration_s of parsing " + str(
                                    len(exchangesThisBaseTokenIsTradedOn)) + " exchangesThisBaseTokenIsTradedOns before_exchangesThisBaseTokenIsTradedOn = " + str(duration_s))

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        # Generically show the error so I can easily search for it in the logs
                        self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken, quoteTokensToSpend_etherUnits)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                # Generically show the error so I can easily search for it in the logs
                self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken)

        numerator = 100 * cacheAccessCount
        denominator = float(cacheAccessCount + priceCalculateCount)
        cacheAccessPercentage = -123456789  # This number doesn't matter if denominator is zero
        if denominator != 0:
            cacheAccessPercentage = numerator / denominator

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 3.5, parsing results. cacheAccessPercentage = " + str(round(cacheAccessPercentage, 1)) + " %")

        if Libraries.defaults.VerboseLogging_SettingReturnValueForResultKey:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Setting returnValue for resultKey " + str(
                resultKey) + ", quoteToken = " + str(quoteToken) + ", detailed = " + str(detailed) + ", returnValue = " + str(returnValue))
        Libraries.utils.ConsiderSettingResultDict(returnValue, resultDict, resultKey, lock_resultDict)

        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Successfully ended with quoteToken " + str(
        #     quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 4, returning")

    def GenerateCallData_Check(self, arbitrageOpportunity, pathIndex, tradingTechnique):
        from Contracts.contracts import Contract_Ninja_Exchange_Balancer
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        # Extract any metadata I need
        exchangeQuoteTokenQuantity_etherUnits = None
        balancerPoolContract = None
        if Libraries.core.IsBuy(side):
            balancerPoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange]
            exchangeQuoteTokenQuantity_etherUnits = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange_quoteTokenBalance]
        elif Libraries.core.IsSell(side):
            balancerPoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange]
            exchangeQuoteTokenQuantity_etherUnits = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange_quoteTokenBalance]
        else:
            raise Exception("Not buy or sell?")

        # Determine minQuoteTokenQuantityAcceptable/maxQuoteTokenQuantityAcceptable based on exchangeQuoteTokenQuantity_etherUnits
        # priceMovementPercentageAllowed is how much we're willing to allow the price % to move before we reject it.
        # I like to set this based on expected profit percentage. So if profit percentage is 1%,
        #   and we want to reject the trade if profit percentage gets to 0.5%, then set this value to 0.5 so it draws the line at half
        priceMovementPercentageAllowed = arbitrageOpportunity.profitPercent * Libraries.defaults.AMMProfitPercentagePriceMovementMultiplier
        # priceMovementPercentageAllowed = Ninja_SimpleRequirements_WiggleRoomPercentage  # This was my old hard coded number
        decimals_quoteToken = Libraries.core.GetDecimalsForTokenContract(quoteToken, True)
        maxQuoteTokenQuantityAcceptable_weiUnits = None
        minQuoteTokenQuantityAcceptable_weiUnits = None
        if Libraries.core.IsBuy(side):
            multiplier = (1.0 + priceMovementPercentageAllowed)
            maxQuoteTokenQuantityAcceptable_etherUnits = exchangeQuoteTokenQuantity_etherUnits * multiplier
            maxQuoteTokenQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxQuoteTokenQuantityAcceptable_etherUnits, decimals_quoteToken)
        elif Libraries.core.IsSell(side):
            multiplier = (1.0 - priceMovementPercentageAllowed)
            minQuoteTokenQuantityAcceptable_etherUnits = exchangeQuoteTokenQuantity_etherUnits * multiplier
            minQuoteTokenQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minQuoteTokenQuantityAcceptable_etherUnits, decimals_quoteToken)
        else:
            raise Exception("Not buy or sell?")

        # If we want to force the simple requirements to pass for debug purposes
        if Libraries.defaults.Ninja_ForcePassSimpleRequirements:
            maxQuoteTokenQuantityAcceptable_weiUnits = int(Libraries.core.DataF_Int),
            minQuoteTokenQuantityAcceptable_weiUnits = 1
            self.SendMessageRegarding_Ninja_ForcePassSimpleRequirements()

        kwargs = None
        functionName = None
        if Libraries.core.IsBuy(side):
            #     function checkSimpleRequirements_buy_balancer(
            #         address quoteToken,
            #         uint256 maxQuoteTokenQuantityAcceptable,
            #         address balancerPoolContract
            #         ) external returns (bool)
            functionName = 'checkSimpleRequirements_buy_balancer'
            kwargs = {
                # Make sure this is the ERC20 version of the token, aka not 0xeeee but instead use WETH
                'quoteToken': Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(quoteToken)),
                'maxQuoteTokenQuantityAcceptable': int(maxQuoteTokenQuantityAcceptable_weiUnits),
                'balancerPoolContract': Libraries.nodes.Instance_Web3.toChecksumAddress(balancerPoolContract),
            }

        elif Libraries.core.IsSell(side):
            #     function checkSimpleRequirements_sell_balancer(
            #         address quoteToken,
            #         uint256 minQuoteTokenQuantityAcceptable,
            #         address balancerPoolContract
            #         ) external returns (bool)
            functionName = 'checkSimpleRequirements_sell_balancer'
            kwargs = {
                # Make sure this is the ERC20 version of the token, aka not 0xeeee but instead use WETH
                'quoteToken': Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(quoteToken)),
                'minQuoteTokenQuantityAcceptable': int(minQuoteTokenQuantityAcceptable_weiUnits),
                'balancerPoolContract': Libraries.nodes.Instance_Web3.toChecksumAddress(balancerPoolContract),
            }

        else:
            raise Exception("Not buy or sell?")

        # PrintAndLog_FuncNameHeader("Check calldata kwargs = " + str(kwargs))
        checkCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Exchange_Balancer, functionName)
        # PrintAndLog_FuncNameHeader("checkCallDataArray_encoded for " + self.exchangeName + " = " + str(checkCallDataArray_encoded))
        return [checkCallDataArray_encoded]

    def GenerateCallData_Trade(self, arbitrageOpportunity, pathIndex):
        from Contracts.contracts import Contract_Ninja_Exchange_Balancer
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        # Extract any metadata I need
        balancerPoolContract = None
        if Libraries.core.IsBuy(side):
            balancerPoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange]
        elif Libraries.core.IsSell(side):
            balancerPoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange]
        else:
            raise Exception("Not buy or sell?")

        # Set the tokens we're trading
        spendToken = EnforceTokenAddressIsERC20Version(arbitrageOpportunity.tradePath[pathIndex])
        receiveToken = EnforceTokenAddressIsERC20Version(arbitrageOpportunity.tradePath[pathIndex + 1])

        #     function trade_balancer_pool(
        #         uint256 tokenAmountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        #         address balancerPoolContract,
        #         address tokenIn,
        #         address tokenOut
        #         ) external onlyNinjaProxy returns (uint256 tokensReceived)
        functionName = 'trade_balancer_pool'
        kwargs = {
            'tokenAmountIn': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
            'balancerPoolContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(balancerPoolContract)),
            'tokenIn': str(Libraries.nodes.Instance_Web3.toChecksumAddress(spendToken)),
            'tokenOut': str(Libraries.nodes.Instance_Web3.toChecksumAddress(receiveToken)),
        }

        PrintAndLog_FuncNameHeader("Trade calldata kwargs = " + str(kwargs))
        tradeCallDataArray = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Exchange_Balancer, functionName, False, True)
        PrintAndLog_FuncNameHeader("tradeCallDataArray for " + self.exchangeName + " = " + str(tradeCallDataArray))
        return [tradeCallDataArray]

    # Return a list of spender proxy addresses we need to approve token transfers for this exchange
    def GetTokenSpenderProxys(self, tokenToApprove):
        # I'm trading directly through each exchange pool instead of the router
        # And to avoid having to approve them ahead of time I built the approve directly into the smart contract
        # In the future if I want to trade through the proxy/router i'll have to re-enable this and issue approves again
        return []

    def GetExpectedGasUsage_Check(self):
        return 3000

    def GetExpectedGasUsage_Trade(self):
        return 88761

    def GetEventLogsTradeTopics(self):
        from Libraries.topics import BuildTopicsArray_Balancer_Swap
        # https://etherscan.io/tx/0x49e897c3c81ca4812565df056dbf09d7ab6060351351812226002450b0b47f81#eventlog
        # See if any topics for this exchange are found in eventLogsList

        topicsList = []
        for topic in BuildTopicsArray_Balancer_Swap():
            topicsList.append(topic.lower())

        # PrintAndLog_FuncNameHeader("topicsList = " + str(topicsList))
        return topicsList

    # Get general exchange info at this block, given txReceipt
    def GetInfoAtBlockNumber(self, local_blockNumber, tokenAddressesToTraceList,
                             txReceipt, txReceiptList_forThisBlockNumber, textToPrepend):
        eventLogs = Libraries.core.GetEventLogsFromTxReceipt(txReceipt)
        topicsList = self.GetEventLogsTradeTopics()
        # PrintAndLog("eventLogs = " + str(eventLogs))

        # We want to loop for block local_blockNumber as well as the one before it so we can compare before and after of the transaction
        for blockNumberToUse in range(local_blockNumber - 1, local_blockNumber + 1):
            PrintAndLog(textToPrepend + "blockNumberToUse = " + str(blockNumberToUse))
            for eventLog in eventLogs:
                for topic in topicsList:
                    if topic.lower() in str(eventLog).lower():
                        # We found the exchangeAddress, now get the asset balances
                        exchangeAddress = eventLog['address']

                        # Create a list of tokens whose balances we want based on the tokens in the exchange and tokenAddressesToTraceList
                        tokenList_balancerExchange = Exchanges.balancer.GetExchangesTokenList(exchangeAddress)
                        tokenList_balancerExchange = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(copy.deepcopy(tokenList_balancerExchange))
                        tokenAddressList_toUse = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(copy.deepcopy(tokenAddressesToTraceList))

                        for token in tokenList_balancerExchange:
                            if token not in tokenAddressList_toUse:
                                tokenAddressList_toUse.append(token)

                        walletAddressList = [exchangeAddress] * len(tokenAddressList_toUse)
                        # PrintAndLog("walletAddressList = " + str(walletAddressList))
                        balanceList_token = Libraries.core.API_GetTokenBalance_Batched_Safe(
                            walletAddressList, tokenAddressList_toUse, None, None, None, blockNumberToUse)
                        # Get all the token balances
                        for index, tokenAddress in enumerate(tokenAddressList_toUse):
                            tokenSymbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)
                            PrintAndLog(textToPrepend + "balance_token = " + str(balanceList_token[index]) + " " + str(
                                tokenSymbol) + " (" + str(tokenAddress) + ")")

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def PrepareRateFormulaMetaData(self, arbitrageOpportunity, pathIndex):
        header = "PrepareRateFormulaMetaData: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        # Get the balancerPoolContract from metaData
        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # Extract any metadata I need
        balancerPoolContract = None
        if Libraries.core.IsBuy(side):
            balancerPoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange]
        elif Libraries.core.IsSell(side):
            balancerPoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange]
        else:
            raise Exception("Not buy or sell?")

        # Get the balances of the balancerPoolContract
        PrintAndLog_Header(header, "balancerPoolContract = " + str(balancerPoolContract))
        PrintAndLog_Header(header, "quoteToken = " + str(quoteToken))
        PrintAndLog_Header(header, "baseToken = " + str(baseToken))
        PrintAndLog_Header(header, "arbitrageOpportunity.blockNumberDiscovered = " + str(arbitrageOpportunity.blockNumberDiscovered))

        # functionArguments must be a tuple, so if it only has one item it must be followed by a comma to make it a tuple
        functionArguments = [balancerPoolContract, balancerPoolContract], \
                            [EnforceTokenAddressIsERC20Version(quoteToken), EnforceTokenAddressIsERC20Version(baseToken)], \
                            [Libraries.core.GetDecimalsForTokenContract(quoteToken, True), Libraries.core.GetDecimalsForTokenContract(baseToken, True)], \
                            None, None, arbitrageOpportunity.blockNumberDiscovered
        functionCall = Libraries.core.API_GetTokenBalance_Batched_Safe
        hash = Libraries.utils.GenerateHashOfFunctionCallAndArguments(functionCall, functionArguments)
        PrintAndLog_Header(header, "hash = " + str(hash))
        return functionCall, functionArguments, hash

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def GetRateFormulaMetaData(self, resultForApiCall, arbitrageOpportunity, pathIndex, metaDataResultDict, key):
        header = "GetRateFormulaMetaData: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        # Get the balancerPoolContract from metaData
        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # Extract any metadata I need
        balancerPoolContract = None
        if Libraries.core.IsBuy(side):
            balancerPoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange]
        elif Libraries.core.IsSell(side):
            balancerPoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange]
        else:
            raise Exception("Not buy or sell?")

        # Get the balances of the balancerPoolContract
        PrintAndLog_Header(header, "balancerPoolContract = " + str(balancerPoolContract))
        PrintAndLog_Header(header, "quoteToken = " + str(quoteToken))
        PrintAndLog_Header(header, "baseToken = " + str(baseToken))
        PrintAndLog_Header(header, "arbitrageOpportunity.blockNumberDiscovered = " + str(arbitrageOpportunity.blockNumberDiscovered))
        balancesList = resultForApiCall

        balance_quoteToken = balancesList[0]
        balance_baseToken = balancesList[1]
        PrintAndLog_Header(header, "balancerPoolContract obtained from metaData = " + str(balancerPoolContract))
        PrintAndLog_Header(header, "balance_quoteToken = " + str(balance_quoteToken))
        PrintAndLog_Header(header, "balance_baseToken = " + str(balance_baseToken))

        metaDataResultDict[key] = {
            'balance_quoteTokens': balance_quoteToken,
            'balance_baseTokens': balance_baseToken,
            'maxAmount_quoteTokens': balance_quoteToken,
            'balancerPoolContract': balancerPoolContract,
        }
        PrintAndLog_Header(header, "Set metaDataResultDict = " + str(metaDataResultDict))

    # Assume the same metaData structure as set by GetRateFormulaMetaData for this exchange
    # calculate the rate (not price) when trading according to this metaData on this exchange
    def GetRate_FinalizeQuoteTokenAmount(self, arbitrageOpportunity, pathIndex, inFlowAmount, metaData,
                                         tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        header = "GetRate_FinalizeQuoteTokenAmount: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        mySide = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        spendToken = None
        receiveToken = None
        exchangeBalanceSpendTokens_etherUnits = None
        exchangeBalanceReceiveTokens_etherUnits = None
        if Libraries.core.IsBuy(mySide):
            spendToken = quoteToken
            receiveToken = baseToken
            exchangeBalanceSpendTokens_etherUnits = metaData['balance_quoteTokens']
            exchangeBalanceReceiveTokens_etherUnits = metaData['balance_baseTokens']
        elif Libraries.core.IsSell(mySide):
            spendToken = baseToken
            receiveToken = quoteToken
            exchangeBalanceSpendTokens_etherUnits = metaData['balance_baseTokens']
            exchangeBalanceReceiveTokens_etherUnits = metaData['balance_quoteTokens']
        else:
            raise Exception("Not buy or sell?")

        # PrintAndLog_Header(header, "spendToken = " + str(spendToken))
        # PrintAndLog_Header(header, "receiveToken = " + str(receiveToken))
        # PrintAndLog_Header(header, "exchangeBalanceSpendTokens_etherUnits = " + str(exchangeBalanceSpendTokens_etherUnits))
        # PrintAndLog_Header(header, "exchangeBalanceReceiveTokens_etherUnits = " + str(exchangeBalanceReceiveTokens_etherUnits))

        price = Exchanges.balancer.CalculateTradePrice(
            mySide, metaData['balancerPoolContract'],
            EnforceTokenAddressIsERC20Version(spendToken), EnforceTokenAddressIsERC20Version(receiveToken),
            inFlowAmount, exchangeBalanceSpendTokens_etherUnits, exchangeBalanceReceiveTokens_etherUnits)
        rate = Libraries.utils.ConvertPriceToRate(price, mySide)
        # PrintAndLog_Header(header, "Converted " + str(self.exchangeName) + " price = " + str(
        #     price) + " to a rate = " + str(rate) + ", mySide = " + str(mySide))
        return rate


class Exchange_Curve(Exchange_Decentralized):

    def GetTradesUniqueIdentifier(self, quoteToken, baseToken, metaDataIdentifier):
        # header_logging = "GetTradesUniqueIdentifier(" + str(self.exchangeName) + "): "
        # PrintAndLog_Header(header_logging, "quoteToken = " + str(quoteToken))
        # PrintAndLog_Header(header_logging, "baseToken = " + str(baseToken))
        # PrintAndLog_Header(header_logging, "returning metaDataIdentifier = " + str(metaDataIdentifier))
        return metaDataIdentifier

    def GetMetaDataIdentifier(self, arbitrageOpportunity, pathIndex):
        # header_logging = "GetMetaDataIdentifier(" + str(self.exchangeName) + "): "
        # I must choose pdk_best_bidPrice_exchange or pdk_best_askPrice_exchange based on side for this pathIndex
        # And side is based on the quoteToken WRT pricesDict which comes from sidesList_wrtPricesDict
        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # PrintAndLog_Header(header_logging, "side = " + str(side))
        pkBestExchangeKey = None
        if Libraries.core.IsBuy(side):
            pkBestExchangeKey = pdk_best_askPrice_exchange
            # PrintAndLog_Header(header_logging, "setting pkBestExchangeKey based on pdk_best_askPrice_exchange = " + str(pdk_best_askPrice_exchange))
        elif Libraries.core.IsSell(side):
            pkBestExchangeKey = pdk_best_bidPrice_exchange
            # PrintAndLog_Header(header_logging, "setting pkBestExchangeKey based on pdk_best_bidPrice_exchange = " + str(pdk_best_bidPrice_exchange))
        else:
            raise Exception("Not buy or sell??")

        # PrintAndLog_Header(header_logging, "pkBestExchangeKey = " + str(pkBestExchangeKey))
        bestExchange = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pkBestExchangeKey]
        # PrintAndLog_Header(header_logging, "bestExchange = " + str(bestExchange))
        return bestExchange

    # Uniswap supports ETH, UniswapV2 does not, Kyber supports ETH, 0x does not, etc
    def SupportTradingEthDirectlyWithoutBeingWrapped(self):
        return False

    def GetTradableTokens(self):
        # Assume the data is cached, caches should be populated ahead of time, not in this function
        # We don't want to just return all the exchange's tokens because there's so many that are absolute junk and don't have any value in the exchange anyways
        tradeableTokensList = Exchanges.curve.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(
            list(Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict().keys()))
        return tradeableTokensList

    def SetMemoryCacheFromFileCache(self):
        Exchanges.curve.ContractDict_Exchange = Libraries.cache.GetDataFromCache("Exchanges.curve.API_GetExchangeDataDict")
        PrintAndLog("Exchanges.curve.ContractDict_Exchange = " + str(Exchanges.curve.ContractDict_Exchange))

    def GetPrices(self, quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber,
                  resultDict, resultKey, lock_resultDict, detailed=False):
        self.EnforceEligibleQuoteToken(quoteToken)

        header_logging = Libraries.loggingConfig.GenerateHeaderForPrintAndLog(self.exchangeName)
        functionsStartDateTime = datetime.datetime.now()

        # Filter down the list based on exchange balances for this quote token
        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "baseTokenList before updating of len " + str(len(baseTokenList)) + " = " + str(baseTokenList))
        tokensThatHaveSomeDecentBalance = Exchanges.curve.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance([quoteToken])
        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "tokensThatHaveSomeDecentBalance of len " + str(len(tokensThatHaveSomeDecentBalance)) + " = " + str(tokensThatHaveSomeDecentBalance))
        # Remove tokens from baseTokenList that are not in tokensThatHaveSomeDecentBalance
        baseTokenList = Libraries.utils.RemoveItemsFromThisListThatAreNotFoundInThatList(baseTokenList, tokensThatHaveSomeDecentBalance)
        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "baseTokenList after updating of len " + str(len(baseTokenList)) + " = " + str(baseTokenList))

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 1, Begin with quoteToken " + str(
            quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        allCurvePoolsList = list(Exchanges.curve.ContractDict_Exchange.keys())
        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "allCurvePoolsList of len " + str(len(allCurvePoolsList)) + " = " + str(allCurvePoolsList))

        quoteToken_toUse = EnforceTokenAddressIsERC20Version(quoteToken).lower()
        decimals_quoteToken = Libraries.core.GetDecimalsForTokenContract(quoteToken, True)
        subSetOfCurvePoolsList = []
        # Keyed by the exchange, these are the baseTokens that are found on this exchange
        intersectingBaseTokensDict = {}
        for exchange in allCurvePoolsList:
            # Make sure the exchange has assets worth even considering a trade
            quoteTokenBalanceThreshold = Exchanges.keeperDAO.GetExchangeMinQuoteTokenBalanceRequirementDict()[quoteToken.lower()]

            tokenList_curve = Exchanges.curve.GetExchangesTokenList(exchange)
            # We only care about exchanges with this quoteToken in them
            if quoteToken_toUse in tokenList_curve:
                # We also require that at least one token in baseTokenList is found in that pool
                intersectingTokens = Libraries.utils.FindIntersectingValuesInLists(baseTokenList, tokenList_curve)
                intersectingBaseTokensDict[exchange] = intersectingTokens
                if Libraries.defaults.VerboseLogging_GetPrices_Curve:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "intersectingTokens = " + str(intersectingTokens) + ", tokenList_curve = " + str(tokenList_curve))
                if len(intersectingTokens) > 0:
                    quoteTokenBalance = Exchanges.curve.GetExchangesTokenBalance(exchange, quoteToken_toUse)
                    # Require that the quoteTokenBalance exceeds the threshold so we know this token is worth trading
                    if float(quoteTokenBalance) > quoteTokenBalanceThreshold:
                        subSetOfCurvePoolsList.append(exchange)
                    else:
                        pass
                        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "curve's quoteTokenBalance was below our quoteTokenBalanceThreshold, so we're excluding it. curvePool = " + str(
                                                     exchange) + ", quoteTokenBalance = " + str(quoteTokenBalance) + ", quoteTokenBalanceThreshold = " + str(quoteTokenBalanceThreshold))
                else:
                    pass
                    if Libraries.defaults.VerboseLogging_GetPrices_Curve:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "We did not find at least 1 baseToken from baseTokenList in tokenList_curve, so we're excluding this exchange. curvePool = " + str(
                                                 exchange) + ", baseTokenList = " + str(baseTokenList) + ", tokenList_curve = " + str(tokenList_curve))

        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "subSetOfCurvePoolsList of len " + str(len(subSetOfCurvePoolsList)) + " = " + str(subSetOfCurvePoolsList))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "intersectingBaseTokensDict = " + str(intersectingBaseTokensDict))

        tokensList_quoteToken = [quoteToken_toUse] * len(subSetOfCurvePoolsList)
        decimalsList_quoteTokens = [decimals_quoteToken] * len(subSetOfCurvePoolsList)

        # This going to be a list of curvePools but some of them may repeat many times
        # because it needs to correspond with the tokensList_baseToken which is created from intersectingBaseTokensDict
        curvePoolsList_FromIntersectingBaseTokensDict = []
        tokensList_baseToken = []
        decimalsList_baseTokens = []
        for exchange in intersectingBaseTokensDict:
            exchangesBaseTokenList = intersectingBaseTokensDict[exchange]
            for token in exchangesBaseTokenList:
                curvePoolsList_FromIntersectingBaseTokensDict.append(exchange)
                tokensList_baseToken.append(token)
                decimalsList_baseTokens.append(Libraries.core.GetDecimalsForTokenContract(token, True))

        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "curvePoolsList_FromIntersectingBaseTokensDict of len " + str(len(
                                     curvePoolsList_FromIntersectingBaseTokensDict)) + " = " + str(curvePoolsList_FromIntersectingBaseTokensDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "tokensList_baseToken of len " + str(len(tokensList_baseToken)) + " = " + str(tokensList_baseToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "decimalsList_baseTokens of len " + str(len(decimalsList_baseTokens)) + " = " + str(decimalsList_baseTokens))

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
                self.exchangeName) + " step 2, quoteToken " + str(
                quoteToken) + " and matched exactly " + str(len(baseTokenList)) + " baseTokens over " + str(
                len(allCurvePoolsList)) + " curve pools. Calling API_GetTokenBalance_Batched_Safe")

        threads = []
        resultsDict = {}
        before = datetime.datetime.now()

        key_quoteTokenBalances = "quoteTokenBalances"
        t = threading.Thread(
            target=Libraries.core.API_GetTokenBalance_Batched_Safe,
            args=(subSetOfCurvePoolsList, tokensList_quoteToken, decimalsList_quoteTokens, resultsDict, key_quoteTokenBalances))
        threads.append(t)
        t.start()

        key_baseTokenBalances = "baseTokenBalances"
        t = threading.Thread(
            target=Libraries.core.API_GetTokenBalance_Batched_Safe,
            args=(curvePoolsList_FromIntersectingBaseTokensDict, tokensList_baseToken, decimalsList_baseTokens, resultsDict, key_baseTokenBalances))
        threads.append(t)
        t.start()

        for thread in threads:
            thread.join()

        duration_s = (datetime.datetime.now() - before).total_seconds()
        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
                self.exchangeName) + " step 3, parsing results. API call duration_s = " + str(round(duration_s, 2)) + " seconds")

        balanceList_quoteTokens = resultsDict[key_quoteTokenBalances]
        balanceList_baseTokens = resultsDict[key_baseTokenBalances]
        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceList_quoteTokens = " + str(balanceList_quoteTokens))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "balanceList_baseTokens = " + str(balanceList_baseTokens))

        subSetOfCurvePoolBalancesDict = Exchanges.curve.API_GetAllTokenBalancesInPool(subSetOfCurvePoolsList, blockNumber)
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "subSetOfCurvePoolBalancesDict for blockNumber " + str(blockNumber) + " = " + str(subSetOfCurvePoolBalancesDict))

        # Use balanceList_quoteTokens and subSetOfCurvePoolsList to re-construct a dict that's more meaningful
        # Keyed by the exchange, value is the quoteToken balance on that exchange
        meaningfulQuoteTokenBalanceDict = {}
        for index, balance_quoteTokens in enumerate(balanceList_quoteTokens):
            # Pair up the exchange with the balance
            exchange = subSetOfCurvePoolsList[index]

            # Require that the quoteTokenBalance exceeds the threshold so we know this token is worth trading
            if Exchanges.keeperDAO.DoesThisQuoteTokenHaveEnoughBalanceWorthTradingFor(quoteToken, balance_quoteTokens, self.exchangeName):
                if Libraries.defaults.VerboseLogging_GetPrices_Curve:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "Adding this exchange and it's balance to meaningfulQuoteTokenBalanceDict because it has enough "
                                         "balance to be worth trading. exchange = " + str(exchange) + ", balance_quoteTokens = " + str(balance_quoteTokens))
                meaningfulQuoteTokenBalanceDict[exchange] = balance_quoteTokens
            else:
                if Libraries.defaults.VerboseLogging_GetPrices_Curve:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "Not adding this exchange and it's balance to meaningfulQuoteTokenBalanceDict because it doesn't have enough "
                                         "balance to be worth trading. exchange = " + str(exchange) + ", balance_quoteTokens = " + str(balance_quoteTokens))

        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "meaningfulQuoteTokenBalanceDict of len " + str(len(meaningfulQuoteTokenBalanceDict)) + " = " + str(meaningfulQuoteTokenBalanceDict))

        # Use balanceList_baseTokens to re-construct a dict that's more meaningful
        # Keyed by the exchange
        # value is a another dictionary where the key is the token address and the value is the balance
        meaningfulBaseTokenBalanceDict = {}
        for index, balance_baseTokens in enumerate(balanceList_baseTokens):
            exchange = curvePoolsList_FromIntersectingBaseTokensDict[index]
            token = tokensList_baseToken[index]

            # Create a new value dict if it's not already there for this exchange
            if exchange not in meaningfulBaseTokenBalanceDict:
                meaningfulBaseTokenBalanceDict[exchange] = {}

            # Set the balance
            meaningfulBaseTokenBalanceDict[exchange][token] = balance_baseTokens

        # Create a dict where the baseToken is the key and the value is a list of all curve pools it exists on
        baseTokenCurvePoolsDict = {}
        for baseToken in baseTokenList:
            baseTokenCurvePoolsDict[baseToken] = []
            for exchange in intersectingBaseTokensDict:
                # If this baseToken is in this exchange's list of tokens
                if baseToken in intersectingBaseTokensDict[exchange]:
                    baseTokenCurvePoolsDict[baseToken].append(exchange)

        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "baseTokenCurvePoolsDict of len " + str(len(baseTokenCurvePoolsDict)) + " = " + str(baseTokenCurvePoolsDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Tracing performance " + str(self.exchangeName) + " step 3.4, parsing results")

        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "meaningfulBaseTokenBalanceDict of len " + str(len(meaningfulBaseTokenBalanceDict)) + " = " + str(meaningfulBaseTokenBalanceDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "intersectingBaseTokensDict of len " + str(len(intersectingBaseTokensDict)) + " = " + str(intersectingBaseTokensDict))

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokensToSpendList_etherUnits for quoteToken " + str(quoteToken) + " of len " + str(
                len(quoteTokensToSpendList_etherUnits)) + " = " + str(quoteTokensToSpendList_etherUnits))

        # Ready the cache
        if quoteToken not in self.pricesCache:
            self.pricesCache[quoteToken] = {}

        # Record some stats on cache accesses
        cacheAccessCount = 0
        priceCalculateCount = 0

        returnValue = {}
        returnValue[pdk_baseTokens] = {}

        for index_baseToken, baseToken in enumerate(baseTokenList):
            try:
                # Ready the cache
                if baseToken not in self.pricesCache[quoteToken]:
                    self.pricesCache[quoteToken][baseToken] = {}

                returnValue[pdk_baseTokens][baseToken] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_prices] = {}
                returnValue[pdk_baseTokens][baseToken][pdk_metaData] = {}

                exchangesThisBaseTokenIsTradedOn = baseTokenCurvePoolsDict[baseToken]
                # if VerboseLogging_GetPrices_Curve:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "exchangesThisBaseTokenIsTradedOn for baseToken " + str(baseToken) + " = " + str(exchangesThisBaseTokenIsTradedOn))

                # Optimize the below loop by remove as many disregarded or filtered out exchanges from exchangesThisBaseTokenIsTradedOn
                # We may have disregard or filtered out this exchange (example, it only has dust and no real value on it)
                # So if that's the case, we can remove them
                # if VerboseLogging_GetPrices_Curve:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "exchangesThisBaseTokenIsTradedOn before optimization of len " + str(
                #         len(exchangesThisBaseTokenIsTradedOn)) + " = " + str(exchangesThisBaseTokenIsTradedOn))
                for exchange in copy.deepcopy(exchangesThisBaseTokenIsTradedOn):
                    if exchange not in meaningfulQuoteTokenBalanceDict:
                        # if VerboseLogging_GetPrices_Curve:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "Deleting this exchange from exchangesThisBaseTokenIsTradedOn "
                        #                          "because it was not found in meaningfulQuoteTokenBalanceDict. It must have"
                        #                          " been intentionally filtered out. exchange = " + str(exchange) + ", baseToken = " + str(baseToken))
                        exchangesThisBaseTokenIsTradedOn.remove(exchange)
                    else:
                        pass
                        # if VerboseLogging_GetPrices_Curve:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "NOT Deleting this exchange from exchangesThisBaseTokenIsTradedOn "
                        #                          "because it was not found in meaningfulQuoteTokenBalanceDict. It must have"
                        #                          " been intentionally filtered out. exchange = " + str(exchange) + ", baseToken = " + str(baseToken))

                # if VerboseLogging_GetPrices_Curve:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "baseToken " + str(baseToken) + " has " + str(
                #         len(exchangesThisBaseTokenIsTradedOn)) + " exchangesThisBaseTokenIsTradedOn after optimization = " + str(exchangesThisBaseTokenIsTradedOn))

                for index, quoteTokensToSpend_etherUnits in enumerate(quoteTokensToSpendList_etherUnits):
                    try:
                        # if VerboseLogging_GetPrices_Curve:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "iterating with quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                        # Ready the cache
                        if str(index) not in self.pricesCache[quoteToken][baseToken]:
                            self.pricesCache[quoteToken][baseToken][str(index)] = {}

                        # Determine the best bidPrice and askPrice for all curve pools based on the information we gathered
                        best_bidPrice = None
                        best_askPrice = None
                        best_bidPrice_exchange = None
                        best_askPrice_exchange = None
                        best_bidPrice_exchange_quoteTokenBalance = None
                        best_askPrice_exchange_quoteTokenBalance = None
                        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
                            before_exchangesThisBaseTokenIsTradedOn = datetime.datetime.now()
                        for exchange in exchangesThisBaseTokenIsTradedOn:
                            try:
                                # Ready the cache
                                if exchange not in self.pricesCache[quoteToken][baseToken][str(index)]:
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange] = {
                                        pdk_exchangeQuoteTokenBalance_etherUnits: None,
                                        pdk_exchangeBaseTokenBalance_etherUnits: None,
                                        pdk_value: None,
                                    }

                                # Get the balances from the curve pool
                                exchangeBalanceQuoteTokens = meaningfulQuoteTokenBalanceDict[exchange]
                                exchangeBalanceBaseTokens = meaningfulBaseTokenBalanceDict[exchange][baseToken]

                                # this where I check quoteTokenBalance vs quoteTokensToSpend_etherUnits
                                # quoteTokenBalance_exchange = Exchanges.curve.GetExchangesTokenBalance(exchange, quoteToken_toUse)
                                # Here is where I need to enforce token balances
                                # If the exchange does not have enough balance, don't provide prices for that trade quantity
                                # pretend the exchange has less balance to give use a safety buffer
                                exchangeBalanceQuoteTokens_withSafetyBuffer = exchangeBalanceQuoteTokens * Libraries.defaults.CurveExchangeBalanceSafetyBufferPercentageMultiplier
                                if exchangeBalanceQuoteTokens_withSafetyBuffer < quoteTokensToSpend_etherUnits:
                                    # if VerboseLogging_GetPrices_Curve:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                    #                          "Not able to trade on this exchange because it doesn't have enough quoteTokenBalance. "
                                    #                          "exchange = " + str(exchange) + ", exchangeBalanceQuoteTokens_withSafetyBuffer = " +
                                    #                          str(exchangeBalanceQuoteTokens_withSafetyBuffer) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                                    # Update the cache prices to None and do not consider updating best_askPrice/best_bidPrice since this exchange cannot trade at these quantities
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_exchangeQuoteTokenBalance_etherUnits] = \
                                        exchangeBalanceQuoteTokens
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_exchangeBaseTokenBalance_etherUnits] = \
                                        exchangeBalanceBaseTokens
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_bid] = None
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_ask] = None
                                    # We're done with this loop iteration
                                    continue

                                # TODO, check to see if the exchangeBalances have changed
                                #  if not, return the cached values
                                #  if they have changed, calculate price again
                                #  I could use the pricesDict as that cache, the problem with that is i may want to cache more data than what's in pricesDict
                                #  For example pricesDict does not cache the baseToken balance only the quoteToken balance, here I'll want both to be safe

                                exchangeBalancesHaveChanged = False
                                # If the exchange balances have changed
                                if exchangeBalanceQuoteTokens != self.pricesCache[quoteToken][baseToken][str(index)][exchange][
                                    pdk_exchangeQuoteTokenBalance_etherUnits] or \
                                        exchangeBalanceBaseTokens != self.pricesCache[quoteToken][baseToken][str(index)][exchange][
                                    pdk_exchangeBaseTokenBalance_etherUnits]:
                                    exchangeBalancesHaveChanged = True

                                # If we've disabled this exchange's cache, assume we have new data so it always calculates it
                                if not Libraries.defaults.EnableCurvesCacheToOptimizeGetPrices:
                                    # if VerboseLogging_GetPrices_Curve:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                    #                          "EnableCurvesCacheToOptimizeGetPrices was disabled, "
                                    #                          "forcing this exchange to re-calculate prices as opposed to using cache")
                                    exchangeBalancesHaveChanged = True

                                if exchangeBalancesHaveChanged:
                                    # Get the new prices and update the cache
                                    priceCalculateCount += 1
                                    # if VerboseLogging_GetPrices_Curve:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                    #                          "calculating new exchange price for Curve pool " + str(
                                    #                              exchange) + ", quoteToken = " + str(quoteToken) + ", baseToken = " + str(
                                    #                              baseToken) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))

                                    balancesOfAllTokensOnThisExchange = subSetOfCurvePoolBalancesDict[exchange]

                                    askPrice, dontCare = Exchanges.curve.CalculateTradePrice(
                                        'buy', exchange, EnforceTokenAddressIsERC20Version(quoteToken), baseToken,
                                        balancesOfAllTokensOnThisExchange, quoteTokensToSpend_etherUnits, True)

                                    # Now to calculate the bidPrice we have a chicken and egg problem here.
                                    # In order to get the bidPrice, we need the source amount which is in baseTokens
                                    # But in order to get that we need to get the equivalent based on quoteTokens which requires a price to convert
                                    # But if we plug in the askPrice to convert quoteTokens to baseTokens, we'll end up with a baseTokens amount that's slightly off
                                    # What we can do though is take that baseTokens amount that's slightly off, and plug it into our price formula to get a bidPrice
                                    # Then re-do that previous step with the new price information we just calculated
                                    # But there's a trick, don't just plug in that 2nd price we calculated, average both of them together because that will get you a "spot price"
                                    # The reason we go with the "spot price" for this conversion here is because we want the baseToken equivalent of quoteTokens
                                    baseTokensToSpend_toCalculateSpotPrice = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, askPrice)
                                    bidPrice_toCalculateSpotPrice, dontCare = Exchanges.curve.CalculateTradePrice(
                                        'sell', exchange, EnforceTokenAddressIsERC20Version(quoteToken), baseToken,
                                        balancesOfAllTokensOnThisExchange, baseTokensToSpend_toCalculateSpotPrice, True)

                                    # Now we can derive a spot price to convert quoteTokens to baseTokens
                                    spotPrice = (askPrice + bidPrice_toCalculateSpotPrice) / 2.0

                                    # Finally we can do the actual conversion using the spot price which is way more accurate
                                    baseTokensToSpend = Libraries.core.ConvertQuoteTokensToBaseTokens(quoteTokensToSpend_etherUnits, spotPrice)
                                    bidPrice, dontCare = Exchanges.curve.CalculateTradePrice(
                                        'sell', exchange, EnforceTokenAddressIsERC20Version(quoteToken), baseToken,
                                        balancesOfAllTokensOnThisExchange, baseTokensToSpend, True)

                                    # Update the cache so we can use it later without having to re-calculate every block
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_exchangeQuoteTokenBalance_etherUnits] = \
                                        exchangeBalanceQuoteTokens
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_exchangeBaseTokenBalance_etherUnits] = \
                                        exchangeBalanceBaseTokens
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_bid] = bidPrice
                                    self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_ask] = askPrice

                                else:
                                    # Get the prices from cache
                                    cacheAccessCount += 1

                                    bidPrice = self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_bid]
                                    askPrice = self.pricesCache[quoteToken][baseToken][str(index)][exchange][pdk_ask]

                                # Determine the best price
                                # if not best_askPrice or askPrice < best_askPrice:
                                if askPrice and (not best_askPrice or askPrice < best_askPrice):
                                    best_askPrice = askPrice
                                    best_askPrice_exchange = exchange
                                    best_askPrice_exchange_quoteTokenBalance = exchangeBalanceQuoteTokens
                                    # if VerboseLogging_GetPrices_Curve:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Found new best_askPrice. best_askPrice = " + str(best_askPrice))

                                # Determine the best price
                                # if not best_bidPrice or bidPrice > best_bidPrice:
                                if bidPrice and (not best_bidPrice or bidPrice > best_bidPrice):
                                    best_bidPrice = bidPrice
                                    best_bidPrice_exchange = exchange
                                    best_bidPrice_exchange_quoteTokenBalance = exchangeBalanceQuoteTokens
                                    # if VerboseLogging_GetPrices_Curve:
                                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Found new best_bidPrice. best_bidPrice = " + str(best_bidPrice))

                            except (KeyboardInterrupt, SystemExit):
                                print('\nkeyboard interrupt caught')
                                print('\n...Program Stopped Manually!')
                                raise

                            except:
                                PrintAndLogError("exception when parsing exchangesThisBaseTokenIsTradedOn " + traceback.format_exc())

                        # PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                      "Updating returnValue for pricesDict with quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken) +
                        #                      ", str(index) = " + str(index) + ", best_bidPrice = " + str(best_bidPrice) + ", best_askPrice = " + str(best_askPrice))

                        returnValue[pdk_baseTokens][baseToken][pdk_prices][str(index)] = {
                            pdk_quoteTokensToSpend_etherUnits: quoteTokensToSpend_etherUnits,
                            pdk_bid: best_bidPrice,
                            pdk_ask: best_askPrice,
                        }

                        # TODO, in the future i'll want to include all possible profitable paths instead of just the one best
                        returnValue[pdk_baseTokens][baseToken][pdk_metaData][str(index)] = {
                            pdk_best_bidPrice_exchange: best_bidPrice_exchange,
                            pdk_best_askPrice_exchange: best_askPrice_exchange,
                            pdk_best_bidPrice_exchange_quoteTokenBalance: best_bidPrice_exchange_quoteTokenBalance,
                            pdk_best_askPrice_exchange_quoteTokenBalance: best_askPrice_exchange_quoteTokenBalance,
                        }

                        # PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                      "Calling UpdateNodeInRatesGraph with quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken) +
                        #                      ", str(index) = " + str(index) + ", best_bidPrice = " + str(best_bidPrice) + ", best_askPrice = " + str(best_askPrice))
                        Libraries.ratesGraph.UpdateNodeInRatesGraph(quoteToken, baseToken, best_bidPrice, best_askPrice,
                                                                    self.exchangeName, self.exchangeName, blockNumber, str(index))

                        if Libraries.defaults.VerboseLogging_GetPrices_Curve:
                            duration_s = (datetime.datetime.now() - before_exchangesThisBaseTokenIsTradedOn).total_seconds()
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "duration_s of parsing " + str(len(exchangesThisBaseTokenIsTradedOn)) +
                                                 " exchangesThisBaseTokenIsTradedOns before_exchangesThisBaseTokenIsTradedOn = " + str(duration_s))

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        # Generically show the error so I can easily search for it in the logs
                        self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken, quoteTokensToSpend_etherUnits)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                # Generically show the error so I can easily search for it in the logs
                self.PrintAndLogError_ACustomGetPricesException(traceback.format_exc(), quoteToken, baseToken)

        numerator = 100 * cacheAccessCount
        denominator = float(cacheAccessCount + priceCalculateCount)
        cacheAccessPercentage = -1234  # This number doesn't matter if denominator is zero
        if denominator != 0:
            cacheAccessPercentage = numerator / denominator

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 3.5, parsing results. cacheAccessPercentage = " + str(round(cacheAccessPercentage, 1)) + " %")

        if Libraries.defaults.VerboseLogging_SettingReturnValueForResultKey:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Setting returnValue for resultKey " + str(
                resultKey) + ", quoteToken = " + str(quoteToken) + ", detailed = " + str(detailed) + ", returnValue = " + str(returnValue))
        Libraries.utils.ConsiderSettingResultDict(returnValue, resultDict, resultKey, lock_resultDict)

        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Successfully ended with quoteToken " + str(
        #     quoteToken) + " and " + str(len(baseTokenList)) + " baseTokens")

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Tracing performance " + str(
            self.exchangeName) + " step 4, returning")

    def GenerateCallData_Check(self, arbitrageOpportunity, pathIndex, tradingTechnique):
        from Contracts.contracts import Contract_Ninja_Exchange_Curve
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        # Extract any metadata I need
        exchangeQuoteTokenQuantity_etherUnits = None
        curvePoolContract = None
        if Libraries.core.IsBuy(side):
            curvePoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange]
            exchangeQuoteTokenQuantity_etherUnits = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange_quoteTokenBalance]
        elif Libraries.core.IsSell(side):
            curvePoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange]
            exchangeQuoteTokenQuantity_etherUnits = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange_quoteTokenBalance]
        else:
            raise Exception("Not buy or sell?")

        # Determine minQuoteTokenQuantityAcceptable/maxQuoteTokenQuantityAcceptable based on exchangeQuoteTokenQuantity_etherUnits
        # priceMovementPercentageAllowed is how much we're willing to allow the price % to move before we reject it.
        # I like to set this based on expected profit percentage. So if profit percentage is 1%,
        #   and we want to reject the trade if profit percentage gets to 0.5%, then set this value to 0.5 so it draws the line at half
        priceMovementPercentageAllowed = arbitrageOpportunity.profitPercent * Libraries.defaults.AMMProfitPercentagePriceMovementMultiplier
        # priceMovementPercentageAllowed = Ninja_SimpleRequirements_WiggleRoomPercentage  # This was my old hard coded number
        decimals_quoteToken = Libraries.core.GetDecimalsForTokenContract(quoteToken, True)
        maxQuoteTokenQuantityAcceptable_weiUnits = None
        minQuoteTokenQuantityAcceptable_weiUnits = None
        if Libraries.core.IsBuy(side):
            multiplier = (1.0 + priceMovementPercentageAllowed)
            maxQuoteTokenQuantityAcceptable_etherUnits = exchangeQuoteTokenQuantity_etherUnits * multiplier
            maxQuoteTokenQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxQuoteTokenQuantityAcceptable_etherUnits, decimals_quoteToken)
        elif Libraries.core.IsSell(side):
            multiplier = (1.0 - priceMovementPercentageAllowed)
            minQuoteTokenQuantityAcceptable_etherUnits = exchangeQuoteTokenQuantity_etherUnits * multiplier
            minQuoteTokenQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minQuoteTokenQuantityAcceptable_etherUnits, decimals_quoteToken)
        else:
            raise Exception("Not buy or sell?")

        # If we want to force the simple requirements to pass for debug purposes
        if Libraries.defaults.Ninja_ForcePassSimpleRequirements:
            maxQuoteTokenQuantityAcceptable_weiUnits = int(Libraries.core.DataF_Int),
            minQuoteTokenQuantityAcceptable_weiUnits = 1
            self.SendMessageRegarding_Ninja_ForcePassSimpleRequirements()

        kwargs = None
        functionName = None
        if Libraries.core.IsBuy(side):
            #     function checkSimpleRequirements_buy_curve(
            #         address quoteToken,
            #         uint256 maxQuoteTokenQuantityAcceptable,
            #         address curvePoolContract
            functionName = 'checkSimpleRequirements_buy_curve'
            kwargs = {
                # Make sure this is the ERC20 version of the token, aka not 0xeeee but instead use WETH
                'quoteToken': Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(quoteToken)),
                'maxQuoteTokenQuantityAcceptable': int(maxQuoteTokenQuantityAcceptable_weiUnits),
                'curvePoolContract': Libraries.nodes.Instance_Web3.toChecksumAddress(curvePoolContract),
            }

        elif Libraries.core.IsSell(side):
            # function checkSimpleRequirements_sell_curve(
            #     address quoteToken,
            #     uint256 minQuoteTokenQuantityAcceptable,
            #     address curvePoolContract
            functionName = 'checkSimpleRequirements_sell_curve'
            kwargs = {
                # Make sure this is the ERC20 version of the token, aka not 0xeeee but instead use WETH
                'quoteToken': Libraries.nodes.Instance_Web3.toChecksumAddress(EnforceTokenAddressIsERC20Version(quoteToken)),
                'minQuoteTokenQuantityAcceptable': int(minQuoteTokenQuantityAcceptable_weiUnits),
                'curvePoolContract': Libraries.nodes.Instance_Web3.toChecksumAddress(curvePoolContract),
            }

        else:
            raise Exception("Not buy or sell?")

        # PrintAndLog_FuncNameHeader("Check calldata kwargs = " + str(kwargs))
        checkCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Exchange_Curve, functionName)
        # PrintAndLog_FuncNameHeader("checkCallDataArray_encoded for " + self.exchangeName + " = " + str(checkCallDataArray_encoded))
        return [checkCallDataArray_encoded]

    def GenerateCallData_Trade(self, arbitrageOpportunity, pathIndex):
        from Contracts.contracts import Contract_Ninja_Exchange_Curve
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        # Generate the encoded callData

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        self.EnforceEligibleQuoteToken(quoteToken)

        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        # Extract any metadata I need
        curvePoolContract = None
        if Libraries.core.IsBuy(side):
            curvePoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange]
        elif Libraries.core.IsSell(side):
            curvePoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange]
        else:
            raise Exception("Not buy or sell?")

        # Set the tokens we're trading
        spendToken = EnforceTokenAddressIsERC20Version(arbitrageOpportunity.tradePath[pathIndex])
        receiveToken = EnforceTokenAddressIsERC20Version(arbitrageOpportunity.tradePath[pathIndex + 1])

        #     function trade_curve_pool(
        #         uint256 tokenAmountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        #         address curvePoolContract,
        #         address tokenIn,
        #         address tokenOut,
        #         uint256 tokenIdIn,
        #         uint256 tokenIdOut
        #         ) external onlyNinjaProxy returns (uint256 tokensReceived)
        functionName = 'trade_curve_pool'
        kwargs = {
            'tokenAmountIn': int(Libraries.core.DataF_Int),  # Exclude from the python Ninja operator code since it gets injected by the contact before the call
            'curvePoolContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(curvePoolContract)),
            'tokenIn': str(Libraries.nodes.Instance_Web3.toChecksumAddress(spendToken)),
            'tokenOut': str(Libraries.nodes.Instance_Web3.toChecksumAddress(receiveToken)),
            'tokenIdIn': int(Exchanges.curve.GetIndexOfTokenInExchange(curvePoolContract, spendToken)),
            'tokenIdOut': int(Exchanges.curve.GetIndexOfTokenInExchange(curvePoolContract, receiveToken)),
        }

        PrintAndLog_FuncNameHeader("Trade calldata kwargs = " + str(kwargs))
        tradeCallDataArray = PreparePackedTradeCallData_GivenContractData(kwargs, Contract_Ninja_Exchange_Curve, functionName, False, True)
        PrintAndLog_FuncNameHeader("tradeCallDataArray for " + self.exchangeName + " = " + str(tradeCallDataArray))
        return [tradeCallDataArray]

    # Return a list of spender proxy addresses we need to approve token transfers for this exchange
    def GetTokenSpenderProxys(self, tokenToApprove):
        # I'm trading directly through each exchange pool instead of the router
        # And to avoid having to approve them ahead of time I built the approve directly into the smart contract
        # In the future if I want to trade through the proxy/router i'll have to re-enable this and issue approves again
        return []

    # TODO
    def GetExpectedGasUsage_Check(self):
        return 3000

    # TODO
    def GetExpectedGasUsage_Trade(self):
        return 88761

    def GetEventLogsTradeTopics(self):
        from Libraries.topics import BuildTopicsArray_Curve_Swap
        # See if any topics for this exchange are found in eventLogsList

        topicsList = []
        for topic in BuildTopicsArray_Curve_Swap():
            topicsList.append(topic.lower())

        # PrintAndLog_FuncNameHeader("topicsList = " + str(topicsList))
        return topicsList

    # TODO
    # Get general exchange info at this block, given txReceipt
    def GetInfoAtBlockNumber(self, local_blockNumber, tokenAddressesToTraceList,
                             txReceipt, txReceiptList_forThisBlockNumber, textToPrepend):
        pass
        # eventLogs = Libraries.core.GetEventLogsFromTxReceipt(txReceipt)
        # topicsList = self.GetEventLogsTradeTopics()
        # # PrintAndLog("eventLogs = " + str(eventLogs))
        #
        # # We want to loop for block local_blockNumber as well as the one before it so we can compare before and after of the transaction
        # for blockNumberToUse in range(local_blockNumber - 1, local_blockNumber + 1):
        #     PrintAndLog(textToPrepend + "blockNumberToUse = " + str(blockNumberToUse))
        #     for eventLog in eventLogs:
        #         for topic in topicsList:
        #             if topic.lower() in str(eventLog).lower():
        #                 # We found the exchangeAddress, now get the asset balances
        #                 exchangeAddress = eventLog['address']
        #
        #                 # Create a list of tokens whose balances we want based on the tokens in the exchange and tokenAddressesToTraceList
        #                 tokenList_balancerExchange = Exchanges.balancer.GetExchangesTokenList(exchangeAddress)
        #                 tokenList_balancerExchange = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(copy.deepcopy(tokenList_balancerExchange))
        #                 tokenAddressList_toUse = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(copy.deepcopy(tokenAddressesToTraceList))
        #
        #                 for token in tokenList_balancerExchange:
        #                     if token not in tokenAddressList_toUse:
        #                         tokenAddressList_toUse.append(token)
        #
        #                 walletAddressList = [exchangeAddress] * len(tokenAddressList_toUse)
        #                 # PrintAndLog("walletAddressList = " + str(walletAddressList))
        #                 balanceList_token = Libraries.core.API_GetTokenBalance_Batched_Safe(
        #                     walletAddressList, tokenAddressList_toUse, None, None, None, blockNumberToUse)
        #                 # Get all the token balances
        #                 for index, tokenAddress in enumerate(tokenAddressList_toUse):
        #                     tokenSymbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)
        #                     PrintAndLog(textToPrepend + "balance_token = " + str(balanceList_token[index]) + " " + str(
        #                         tokenSymbol) + " (" + str(tokenAddress) + ")")

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def PrepareRateFormulaMetaData(self, arbitrageOpportunity, pathIndex):
        header = "PrepareRateFormulaMetaData: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        # Get the curvePoolContract from metaData
        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # Extract any metadata I need
        curvePoolContract = None
        if Libraries.core.IsBuy(side):
            curvePoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange]
        elif Libraries.core.IsSell(side):
            curvePoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange]
        else:
            raise Exception("Not buy or sell?")

        # Get the balances of the curvePoolContract
        # PrintAndLog_Header(header, "curvePoolContract = " + str(curvePoolContract))
        PrintAndLog_Header(header, "quoteToken = " + str(quoteToken))
        PrintAndLog_Header(header, "baseToken = " + str(baseToken))
        PrintAndLog_Header(header, "arbitrageOpportunity.blockNumberDiscovered = " + str(arbitrageOpportunity.blockNumberDiscovered))

        # functionArguments must be a tuple, so if it only has one item it must be followed by a comma to make it a tuple
        functionArguments = [curvePoolContract], arbitrageOpportunity.blockNumberDiscovered
        functionCall = Exchanges.curve.API_GetAllTokenBalancesInPool
        hash = Libraries.utils.GenerateHashOfFunctionCallAndArguments(functionCall, functionArguments)
        PrintAndLog_Header(header, "hash = " + str(hash))
        return functionCall, functionArguments, hash

    # We want to get the rate formula metadata, but we cannot make any API calls in this function and expect performance to be optimal
    # So instead, we prepare the API call that we're going to make, and simply return the function call and function arguments
    # This way the caller can gather all of the API calls we're going to make, and more optimally make them somewhere else
    # PrepareRateFormulaMetaData has a direct relationship with GetRateFormulaMetaData
    # PrepareRateFormulaMetaData prepares the API calls to be made, another function actually executes them, then hands the result to GetRateFormulaMetaData
    def GetRateFormulaMetaData(self, resultForApiCall, arbitrageOpportunity, pathIndex, metaDataResultDict, key):
        header = "GetRateFormulaMetaData: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)

        # Get the curvePoolContract from metaData
        side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
        # Extract any metadata I need
        curvePoolContract = None
        if Libraries.core.IsBuy(side):
            curvePoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_askPrice_exchange]
        elif Libraries.core.IsSell(side):
            curvePoolContract = arbitrageOpportunity.metaDataPath[pathIndex][arbitrageOpportunity.index_string][pdk_best_bidPrice_exchange]
        else:
            raise Exception("Not buy or sell?")

        # Get the balances of the curvePoolContract
        # PrintAndLog_Header(header, "curvePoolContract = " + str(curvePoolContract))
        PrintAndLog_Header(header, "quoteToken = " + str(quoteToken))
        PrintAndLog_Header(header, "baseToken = " + str(baseToken))
        PrintAndLog_Header(header, "arbitrageOpportunity.blockNumberDiscovered = " + str(arbitrageOpportunity.blockNumberDiscovered))

        subSetOfCurvePoolBalancesDict = resultForApiCall
        # PrintAndLog_Header(header, "subSetOfCurvePoolBalancesDict = " + str(subSetOfCurvePoolBalancesDict))
        balancesOfAllTokensOnThisExchange = subSetOfCurvePoolBalancesDict[curvePoolContract]
        balance_quoteToken_weiUnits = balancesOfAllTokensOnThisExchange[Exchanges.curve.GetIndexOfTokenInExchange(curvePoolContract, quoteToken)]
        balance_baseToken_weiUnits = balancesOfAllTokensOnThisExchange[Exchanges.curve.GetIndexOfTokenInExchange(curvePoolContract, baseToken)]
        balance_quoteToken = Libraries.core.ConvertWeiToEther_GivenTokenAddress(balance_quoteToken_weiUnits, quoteToken)
        balance_baseToken = Libraries.core.ConvertWeiToEther_GivenTokenAddress(balance_baseToken_weiUnits, baseToken)

        # PrintAndLog_Header(header, "curvePoolContract obtained from metaData = " + str(curvePoolContract))
        # PrintAndLog_Header(header, "balance_quoteToken = " + str(balance_quoteToken))
        # PrintAndLog_Header(header, "balance_baseToken = " + str(balance_baseToken))

        metaDataResultDict[key] = {
            'balance_quoteToken': balance_quoteToken,
            'balance_baseToken': balance_baseToken,
            'balancesOfAllTokensOnThisExchange': balancesOfAllTokensOnThisExchange,
            'maxAmount_quoteTokens': balance_quoteToken,
            'curvePoolContract': curvePoolContract,
        }
        PrintAndLog_Header(header, "Set metaDataResultDict = " + str(metaDataResultDict))

    # Assume the same metaData structure as set by GetRateFormulaMetaData for this exchange
    # calculate the rate (not price) when trading according to this metaData on this exchange
    def GetRate_FinalizeQuoteTokenAmount(self, arbitrageOpportunity, pathIndex, inFlowAmount, metaData,
                                         tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        # header = "GetRate_FinalizeQuoteTokenAmount: " + str(self.exchangeName)

        quoteToken, baseToken = arbitrageOpportunity.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        mySide = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]

        curvePoolContract = metaData['curvePoolContract']
        balancesOfAllTokensOnThisExchange = metaData['balancesOfAllTokensOnThisExchange']
        # PrintAndLog_Header(header, "balancesOfAllTokensOnThisExchange = " + str(balancesOfAllTokensOnThisExchange))
        # PrintAndLog_Header(header, "quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken))
        # PrintAndLog_Header(header, "inFlowAmount = " + str(inFlowAmount))

        price, dontCare = Exchanges.curve.CalculateTradePrice(
            mySide, curvePoolContract, quoteToken, baseToken,
            balancesOfAllTokensOnThisExchange, inFlowAmount, True)

        rate = Libraries.utils.ConvertPriceToRate(price, mySide)
        # PrintAndLog_Header(header, "Converted " + str(self.exchangeName) + " price = " + str(
        #     price) + " to a rate = " + str(rate) + ", mySide = " + str(mySide))
        return rate


class Exchange_Set(Exchange_Decentralized):
    setTokenData = None

    # Some protocols are only available to trade under certain circumstances (Set for example, must be in rebalance mode)
    def IsAvailableToTrade(self):
        # TODO, Determine if this set is available to trade based on its state.
        #  Return True if in rebalance mode.
        #  Return False if in another mode
        return True

    def FinalizeMetaDataBeforeTrade(self, arbitrageOpportunity, pathIndex):
        pass
        # TODO, for return value this should return maxQuoteTokensAvailable divided by the arbitrageOpportunity.quoteTokensToSpend
        #  This makes sense because if we're trading on Set-0x then we could keep filling orders until either Set runs out of liquidity or until 0x runs out of orders

    # Some exchange trades result in a loss based on quoteTokens, but will make profit in baseTokens due to dust
    # For those exchanges, we want to allow for some loss because we expect to make profits in baseTokens
    # In a perfect world, the smart contract would either calculate that baseToken profit and convert to quoteTokens or
    # just liquidate those baseTokens but that's a lot of wasted gas that makes the trade inefficient
    # So the simplest way is to just keep the profit in baseTokens and accept a small loss in quoteTokens
    # Most exchanges should not have dust at all, and this function can simply return 0
    def GetMaxQuoteTokenTradeLossAcceptable_etherUnits(self, quoteToken):
        # Set leaves a lot of dust in both quoteToken and baseToken, so our acceptable loss is going to be pretty high
        if quoteToken.lower() == Exchanges.keeperDAO.EthTokenContract.lower():
            return 0.03
        else:
            raise Exception("Not yet implemented)")


class Exchange_Compound(Exchange_Decentralized):
    todo = None


# PayloadIdName is for keeping track of which payload Id is doing what.
# I'm batching many calls together in one single call and I may get uniswap info and kyber info all in one call and i'll need to know which id belongs to which call
# Any name with the text "range" is intended to send multiple calls to this function.
# If there are 10 calls, then this will span 10 ids instead of just one ids
class PayloadIdName(Enum):
    Uniswap_Balance_Ether = 1
    Uniswap_Balance_Token = 2
    Kyber_GetConversionRate_Range = 3
    Set_Price = 4
    Uniswap_QuoteToken_Balance_Ether = 5
    Uniswap_QuoteToken_Balance_Token = 6
    Uniswap_BaseToken_Balance_Ether = 7
    Uniswap_BaseToken_Balance_Token = 8
    Kyber_GetExpectedRate_Range = 9
    Uniswap_Balance_Ether_Range = 10
    Uniswap_Balance_Token_Range = 11


# Populate the ExchangeDict with all the exchanges
ExchangeDict = {}

exchangeName = ExchangeName_0xMesh
ExchangeDict[exchangeName] = Exchange_0xMesh(exchangeName)

exchangeName = ExchangeName_HidingBook
ExchangeDict[exchangeName] = Exchange_HidingBook(exchangeName)

exchangeName = ExchangeName_Uniswap
ExchangeDict[exchangeName] = Exchange_Uniswap(exchangeName)

exchangeName = ExchangeName_UniswapV2
ExchangeDict[exchangeName] = Exchange_UniswapV2(exchangeName)

exchangeName = ExchangeName_Sushiswap
ExchangeDict[exchangeName] = Exchange_Sushiswap(exchangeName)

exchangeName = ExchangeName_Defiswap
ExchangeDict[exchangeName] = Exchange_Defiswap(exchangeName)

exchangeName = ExchangeName_Sakeswap
ExchangeDict[exchangeName] = Exchange_Sakeswap(exchangeName)

exchangeName = ExchangeName_Balancer
ExchangeDict[exchangeName] = Exchange_Balancer(exchangeName)

exchangeName = ExchangeName_Curve
ExchangeDict[exchangeName] = Exchange_Curve(exchangeName)


# Turning this off for now because it's inefficient
# exchangeName = ExchangeName_Kyber
# ExchangeDict[exchangeName] = Exchange_Kyber(exchangeName)

# TODO
# if EnableSet:
#     exchangeName = ExchangeName_Set
#     ExchangeDict[exchangeName] = Exchange_Set(exchangeName)

# This must be alphabetized because you cannot rely on a dict's order
ExchangeNamesList_Alphabetized = list(ExchangeDict.keys())
ExchangeNamesList_Alphabetized.sort()

# Add exchangeNames to this list if you want to ban them from trading for debug purposes
ExchangeNamesNotPermittedToTrade = []
if Libraries.defaults.Ninja_BanASubsetOfExchangeNames:
    pass
    # ExchangeNamesNotPermittedToTrade.append(ExchangeName_Kyber)
    ExchangeNamesNotPermittedToTrade.append(ExchangeName_0xMesh)

if len(ExchangeNamesNotPermittedToTrade) > 0:
    Libraries.core.API_PostOperatorNotification("ExchangeNamesNotPermittedToTrade is being used! I hope this is intentional. "
                                                "ExchangeNamesNotPermittedToTrade = " + str(ExchangeNamesNotPermittedToTrade))


def DoesExchangeListContainAtLeastOneDX(exchangeList):
    for exchange in exchangeList:
        if isinstance(exchange, Exchange_Decentralized):
            return exchange

    return False


def IsUsdStableCoinAndSupportsFeature_FlipBaseOrQuote(marketName):
    if marketName.lower() == "dai-eth" \
            or marketName.lower() == "tusd-eth" \
            or marketName.lower() == "usdc-eth":
        return True
    else:
        return False


def GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates(results_Get_ArbyContractPricesDict, getMarketPriceTechnique, minEtherValue):
    # This function assumes that we're using a data set where etherArray dictates BOTH the marketOrderPriceList_whenBuyingOnDX and the marketOrderPriceList_whenSellingOnDX

    if not results_Get_ArbyContractPricesDict:
        raise Exception("results_Get_ArbyContractPricesDict is not valid")

    etherArray = results_Get_ArbyContractPricesDict[0]
    # tokensArray = results_Get_ArbyContractPricesDict[1]
    marketOrderPriceList_whenSellingOnDX = results_Get_ArbyContractPricesDict[2]
    marketOrderPriceList_whenBuyingOnDX = results_Get_ArbyContractPricesDict[3]

    # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates: etherArray = " + str(etherArray))
    # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates: tokensArray = " + str(tokensArray))
    # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates: marketOrderPriceList_whenSellingOnDX = " + str(marketOrderPriceList_whenSellingOnDX))
    # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates: marketOrderPriceList_whenBuyingOnDX = " + str(marketOrderPriceList_whenBuyingOnDX))

    # etherArray always dictates the price

    if getMarketPriceTechnique == GetMarketPriceTechnique_WithinOneSingleExchange.consideringBuyOrdersOnly:
        # Iterate the list backwards
        for index in range(len(etherArray) - 1, -1, -1):
            etherValue = etherArray[index]
            if etherValue > minEtherValue:
                # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates: chose index = " + str(index) + " from etherArray = " + str(etherArray))
                marketOrderPrice_whenSellingOnDX_toUse = marketOrderPriceList_whenSellingOnDX[index]
                # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates: returning marketOrderPrice_whenSellingOnDX_toUse = " + str(marketOrderPrice_whenSellingOnDX_toUse))
                return marketOrderPrice_whenSellingOnDX_toUse

    elif getMarketPriceTechnique == GetMarketPriceTechnique_WithinOneSingleExchange.consideringSellOrdersOnly:
        # Iterate the list backwards
        for index in range(len(etherArray) - 1, -1, -1):
            etherValue = etherArray[index]
            if etherValue > minEtherValue:
                # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates: chose index = " + str(index) + " from etherArray = " + str(etherArray))
                marketOrderPrice_whenBuyingOnDX_toUse = marketOrderPriceList_whenBuyingOnDX[index]
                # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_EtherAlwaysDictates: returning marketOrderPrice_whenBuyingOnDX_toUse = " + str(marketOrderPrice_whenBuyingOnDX_toUse))
                return marketOrderPrice_whenBuyingOnDX_toUse


def GetMarketPrice_BasedOnSingleExchange_GivenTopOrdersWithOutliersExcluded(topOrders_withOutliersExcluded, minEtherValue=10, minNumOfOrders=3):
    # PrintAndLog("GetMarketPrice_BasedOnSingleExchange_GivenTopOrdersWithOutliersExcluded len(topOrders_withOutliersExcluded) = " + str(
    #     len(topOrders_withOutliersExcluded)) + ", minEtherValue = " + str(minEtherValue) + ", minNumOfOrders = " + str(minNumOfOrders))
    # Determine what the marketPrice is based on EtherDelta only. I'm doing this because most of these coins
    # i'll only be able to trade on EtherDelta because I cannot trust the other exchanges
    # And if I buy something for a steal deal I'd like to resell it immediately and keep profit in ether.
    # In some cases, i'll be able to re-sell on Bittrex or some other exchange, but in most cases i'll be EtherDelta
    # Don't just use the first order and assume that's the market value. Go based on the top 10 or so ether's worth
    # We want it to have at least a few orders so it's not all one order that someone could pull

    # rollingPrice = None
    etherSum = 0
    productSum = 0
    i = 0
    # Use the topOrders here because we don't want to consider the outliers when
    # measuring the market price on this exchange.

    for order in topOrders_withOutliersExcluded:
        ordersEtherValue = order.GetEtherQuantity()

    for order in topOrders_withOutliersExcluded:
        ordersEtherValue = order.GetEtherQuantity()
        # prevent divide by zero in the case there's a super small order that ends up rounding to zero
        if ordersEtherValue > 0:
            # PrintAndLog(str(i) + ", order.Price = " + str(order.GetPrice()) + ", " + str(ordersEtherValue))
            etherSum += float(ordersEtherValue)

            etherTimesPrice = ordersEtherValue * order.GetPrice()
            # PrintAndLog("etherTimesPrice = " + str(etherTimesPrice))
            productSum += etherTimesPrice

            if etherSum >= minEtherValue and i >= minNumOfOrders:
                break

            i += 1

    # PrintAndLog("etherSum = " + str(etherSum))
    # PrintAndLog("productSum = " + str(productSum))

    marketPrice = productSum / float(etherSum)
    # PrintAndLog("GetMarketPrice_BasedOnSingleExchange: marketPrice = " + str(marketPrice))
    return marketPrice


def GetListOfExchangeNamesGivenListOfExchanges(exchangesList):
    exchangeNameList = []
    for exchange in exchangesList:
        exchangeNameList.append(exchange.exchangeName)

    return exchangeNameList


# TODO duplicate function need to merge with the one in Exchanges.ninja
def ApproveContractForTokenTransfer(fromAddress, fromPrivateKey, tokenAddressToApprove, spenderContractToApprove,
                                    nonce=None, forceSendRegardlessOfCurrentlyPendingForThisAddress=False):
    # Approve for the max amount
    amountData = Libraries.core.DataF

    decimals = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddressToApprove)))
    approvedTokenAmount_ether, approvedTokenAmount_weiUnits = \
        Libraries.core.API_GetTokenAllowance(fromAddress, tokenAddressToApprove, spenderContractToApprove, decimals)
    PrintAndLog("ApproveContractForTokenTransfer: approvedTokenAmount_ether = " + str(
        approvedTokenAmount_ether) + ", approvedTokenAmount_weiUnits = " + str(approvedTokenAmount_weiUnits))
    # Check to see if our approved amount is getting low
    if approvedTokenAmount_weiUnits > Libraries.core.DataF_Int_uint96 - 10:
        PrintAndLog("ApproveContractForTokenTransfer: we do NOT need to approve " + str(fromAddress) + " for contract " + str(
            spenderContractToApprove) + " to trade tokens " + str(
            tokenAddressToApprove) + " on our behalf, because it's already approved for a large amount")
    else:
        PrintAndLog("ApproveContractForTokenTransfer: we need to approve " + str(fromAddress) + " for contract " + str(
            spenderContractToApprove) + " to trade tokens " + str(tokenAddressToApprove) + " on our behalf")

        # gasPrice = Libraries.gasStation.GetGasPrice_Fast()
        # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
        gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
        data_hex = Libraries.core.GetDataFor_Approve(amountData, spenderContractToApprove)
        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, tokenAddressToApprove, 0, Libraries.gasStation.GetGasLimit_Other(),
                                                 gasPrice, data_hex, TransactionType.approve, nonce,
                                                 forceSendRegardlessOfCurrentlyPendingForThisAddress)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Approve_Contract. transactionId: " + transactionId
            Libraries.core.API_PostOperatorNotification(message)
        else:
            PrintAndLog("Failed to Approve_Contract, no transactionId")

        return transactionId


# https://support.bancor.network/hc/en-us/articles/360001455772-Build-a-transaction-using-the-Convert-API
def FillOrder_Bancor(fromAddress, fromPrivateKey, sendToAddress, data_hex, value, gas, gasPrice, nonce, transactionType):
    PrintAndLog("FillOrder_Bancor: with fromAddress = " + str(fromAddress) + ", sendToAddress = " + sendToAddress + ", value = " + str(
        value) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice) + ", nonce = " + str(nonce) + ", data_hex = " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, sendToAddress,
                                             value, gas, gasPrice, data_hex, transactionType, nonce, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        pass
        # message = "FillOrder_EtherDelta " + orderToFill.PrintOrderDetails() + ". transactionId: " + transactionId
        # Libraries.core.API_PostOperatorNotification(message)
    else:
        PrintAndLog("Failed to FillOrder_Bancor, no transactionId")

    return transactionId


def Approve_Bancor(fromAddress, fromPrivateKey, tokenContractAddress, bancorContractAddress, amountData):
    PrintAndLog(
        "Approve_Bancor: fromAddress = " + fromAddress + ", tokenContractAddress = " + tokenContractAddress + ", bancorContractAddress = " + bancorContractAddress + ", amountData = " + str(
            amountData))

    data_hex = Exchanges.bancor.GetDataFor_Approve(bancorContractAddress, amountData)
    # dataByteArray = Libraries.core.ConvertHexStringToDataByteArray(data_hex)

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, tokenContractAddress, 0,
                                             Libraries.gasStation.GetGasLimit_Other(), Libraries.gasStation.GetGasPrice_Cheap(), data_hex, TransactionType.approve)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Approve_Bancor. transactionId: " + transactionId
        Libraries.core.API_PostOperatorNotification(message)
    else:
        PrintAndLog("Failed to Approve_Bancor, no transactionId")

    return transactionId


def Approve_Ether_0x(fromAddress, fromPrivateKey):
    return Approve_0x(fromAddress, fromPrivateKey, Exchanges.zrx.Contract_WETH, Libraries.core.DataF)


def Approve_Tokens_0x(fromAddress, fromPrivateKey, tokenAddress):
    return Approve_0x(fromAddress, fromPrivateKey, tokenAddress, Libraries.core.DataF)


def ApproveReset_Ether_0x(fromAddress, fromPrivateKey):
    return Approve_0x(fromAddress, fromPrivateKey, Exchanges.zrx.Contract_WETH, Libraries.core.Data0)


def ApproveReset_Tokens_0x(fromAddress, fromPrivateKey, tokenAddress):
    return Approve_0x(fromAddress, fromPrivateKey, tokenAddress, Libraries.core.Data0)


def Approve_0x(fromAddress, fromPrivateKey, contractAddress, amountData):
    # https://etherscan.io/tx/0x2421abb062f001834610475ff79ae2e36ba549399a59af6d7ab432f33bfe13e9
    data_hex = Exchanges.zrx.GetDataFor_Approve(amountData)
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             Libraries.gasStation.GetGasLimit_Other(), Libraries.gasStation.GetGasPrice_Cheap(), data_hex, TransactionType.approve)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Approve_0x. transactionId: " + transactionId
        Libraries.core.API_PostOperatorNotification(message)
    else:
        PrintAndLog("Failed to Approve_0x, no transactionId")

    return transactionId


def Deposit_Ether_0x(fromAddress, fromPrivateKey, value_ether_int, wethAddress=Exchanges.zrx.Contract_WETH):
    value_wei_int = int(Libraries.core.ConvertEtherToWei(value_ether_int, Libraries.core.Ether_Decimals))
    data_hex = Exchanges.zrx.GetDataFor_Deposit_Ether()
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, wethAddress, value_wei_int,
                                             Libraries.gasStation.GetGasLimit_Other(), Libraries.gasStation.GetGasPrice_Cheap(), data_hex, TransactionType.deposit)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Depositing " + str(value_ether_int) + " ETH. transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Deposit_Ether_0x, no transactionId")

    return transactionId


def Withdraw_Ether_0x(fromAddress, fromPrivateKey, value_ether_int, wethAddress=Exchanges.zrx.Contract_WETH):
    value_wei_int = int(Libraries.core.ConvertEtherToWei(value_ether_int, Libraries.core.Ether_Decimals))
    data_hex = Exchanges.zrx.GetDataFor_Withdraw_Ether(value_wei_int)
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, wethAddress, 0,
                                             Libraries.gasStation.GetGasLimit_Other(), Libraries.gasStation.GetGasPrice_Cheap(), data_hex, TransactionType.withdraw)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Withdrawing " + str(value_ether_int) + " ETH. transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Withdraw_Ether_0x, no transactionId")

    return transactionId


def DepositERC20Token_CX(fromAddress, fromPrivateKey, depositToAddress, amountForDeposit_ether, tokenAddress, decimals, marketName):
    transactionId = API_SendTokens_ToAddress(fromAddress, fromPrivateKey, amountForDeposit_ether, tokenAddress, depositToAddress, decimals, TransactionType.deposit)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Deposit to CX " + str(amountForDeposit_ether) + " " + marketName + ". transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to DepositERC20Token_CX, no transactionId")

    return transactionId


def DepositEther_CX(fromAddress, fromPrivateKey, depositToAddress, value_ether):
    value_wei_int = int(Libraries.core.ConvertEtherToWei(value_ether, Libraries.core.Ether_Decimals))

    transactionId = API_SendEther_ToAddress(fromAddress, fromPrivateKey, depositToAddress, value_wei_int,
                                            Libraries.gasStation.GetGasLimit_Other(), Libraries.gasStation.GetGasPrice_Cheap(), TransactionType.deposit)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Deposit to CX " + str(value_ether) + " ETH. transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to DepositEther_CX, no transactionId")

    return transactionId


def GetValueFromPayloadResultDict_UniswapEtherBalance(payloadResultDict):
    balanceEther_UniswapContract_etherUnits = Libraries.core.ConvertWeiToEther(
        Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Uniswap_Balance_Ether]), Libraries.core.Ether_Decimals)
    # PrintAndLog("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))
    return balanceEther_UniswapContract_etherUnits


def GetValueFromPayloadResultDict_UniswapTokenBalance(payloadResultDict, token_decimals):
    balanceTokens_UniswapContract_etherUnits = Libraries.core.ConvertWeiToEther(
        Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Uniswap_Balance_Token]), token_decimals)
    # PrintAndLog("balanceTokens_UniswapContract_etherUnits = " + str(balanceTokens_UniswapContract_etherUnits))
    return balanceTokens_UniswapContract_etherUnits


def GetValueFromPayloadResultDict_UniswapEtherBalance_QuoteToken(payloadResultDict):
    balanceEther_UniswapContract_etherUnits = Libraries.core.ConvertWeiToEther(
        Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Uniswap_QuoteToken_Balance_Ether]), Libraries.core.Ether_Decimals)
    # PrintAndLog("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))
    return balanceEther_UniswapContract_etherUnits


def GetValueFromPayloadResultDict_UniswapTokenBalance_QuoteToken(payloadResultDict, token_decimals):
    balanceTokens_UniswapContract_etherUnits = Libraries.core.ConvertWeiToEther(
        Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Uniswap_QuoteToken_Balance_Token]), token_decimals)
    # PrintAndLog("balanceTokens_UniswapContract_etherUnits = " + str(balanceTokens_UniswapContract_etherUnits))
    return balanceTokens_UniswapContract_etherUnits


def GetValueFromPayloadResultDict_UniswapEtherBalance_BaseToken(payloadResultDict):
    balanceEther_UniswapContract_etherUnits = Libraries.core.ConvertWeiToEther(
        Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Uniswap_BaseToken_Balance_Ether]), Libraries.core.Ether_Decimals)
    # PrintAndLog("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))
    return balanceEther_UniswapContract_etherUnits


def GetValueFromPayloadResultDict_UniswapTokenBalance_BaseToken(payloadResultDict, token_decimals):
    balanceTokens_UniswapContract_etherUnits = Libraries.core.ConvertWeiToEther(
        Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Uniswap_BaseToken_Balance_Token]), token_decimals)
    # PrintAndLog("balanceTokens_UniswapContract_etherUnits = " + str(balanceTokens_UniswapContract_etherUnits))
    return balanceTokens_UniswapContract_etherUnits


def GetValueFromPayloadResultDict_UniswapEtherBalance_Value(payloadResultDict, index_token):
    # PrintAndLog_FuncNameHeader("index_token = " + str(index_token))
    # PrintAndLog_FuncNameHeader("payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Ether.value)] = " + str(
    #     payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Ether.value)]))
    balanceEther_UniswapContract_etherUnits = Libraries.core.ConvertWeiToEther(
        Libraries.core.ConvertHexToInt(payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Ether.value)]),
        Libraries.core.Ether_Decimals)
    # PrintAndLog("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))
    return balanceEther_UniswapContract_etherUnits


def GetValueFromPayloadResultDict_UniswapTokenBalance_Value(payloadResultDict, index_token, token_decimals):
    # PrintAndLog_FuncNameHeader("index_token = " + str(index_token))
    # PrintAndLog_FuncNameHeader("payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Token.value)] = " + str(
    #     payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Token.value)]))
    balanceTokens_UniswapContract_etherUnits = Libraries.core.ConvertWeiToEther(
        Libraries.core.ConvertHexToInt(payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Token.value)]),
        token_decimals)
    # PrintAndLog("balanceTokens_UniswapContract_etherUnits = " + str(balanceTokens_UniswapContract_etherUnits))
    return balanceTokens_UniswapContract_etherUnits


def GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits):
    payloadResultDict = {}
    # PrintAndLog_FuncNameHeader("payloadIdDict = " + str(payloadIdDict))
    for payloadId in payloadIdDict:
        id = payloadIdDict[payloadId]
        for jData in jDataList:
            if int(jData['id']) == id:
                # PrintAndLog_FuncNameHeader("Found id match for " + str(id))

                if DoesPayloadIdContainRange(payloadId):
                    # if payloadId not in payloadResultDict:
                    #     # Create a list because we're expecting multiple results for this payloadId
                    #     payloadResultDict[payloadId] = []
                    #
                    # # Append to the list, the list has already been created
                    # payloadResultDict[payloadId].append(jData['result'])

                    # Create a list because we're expecting multiple results for this payloadId
                    payloadResultDict[payloadId] = []
                    # Append to the list, the list has already been created
                    payloadResultDict[payloadId].append(jData['result'])
                    id_ofListItem = payloadIdDict[payloadId]

                    for index in range(1, len(etherToSpendList_weiUnits)):
                        id_iWant = id_ofListItem + index
                        # PrintAndLog_FuncNameHeader("id_iWant = " + str(id_iWant))
                        for sub_jData in jDataList:
                            if int(sub_jData['id']) == id_iWant:
                                # PrintAndLog_FuncNameHeader("Found id_iWant match for " + str(id_iWant))
                                # Append to the list, the list has already been created
                                payloadResultDict[payloadId].append(sub_jData['result'])

                else:
                    # We're only expecting one result for this call, so just set the value to the result
                    payloadResultDict[payloadId] = jData['result']

                break

    return payloadResultDict


def DoesPayloadIdContainRange(payloadId):
    # In case I use a number as a payloadId, let's check to make sure the payloadId has the attribute we're checking for
    stringToCheckFor = 'range'
    if hasattr(payloadId, 'name') and stringToCheckFor in payloadId.name.lower():
        return True
    elif isinstance(payloadId, int):
        # PrintAndLog_FuncNameHeader("payloadId % Libraries.utils.BaseNum  = " + str(payloadId % Libraries.utils.BaseNum))
        # PrintAndLog_FuncNameHeader("PayloadIdName(payloadId % Libraries.utils.BaseNum)  = " + str(PayloadIdName(payloadId % Libraries.utils.BaseNum)))
        # PrintAndLog_FuncNameHeader("PayloadIdName(payloadId % Libraries.utils.BaseNum).name.lower() = " + str(PayloadIdName(payloadId % Libraries.utils.BaseNum).name.lower()))
        # Let's assume that Libraries.utils.BaseNum is 1,000,000.
        # payloadId could be 0,000,001 or 1,000,001 or 2,000,001 or 3,000,001 and I would want ot treat them all as if they were just 1, cutting off the first part of the number
        # use mod (%) to handle this
        if stringToCheckFor in PayloadIdName(payloadId % Libraries.utils.BaseNum).name.lower():
            return True
        else:
            return False
    else:
        return False


def GetPayload_Uniswap_TokenBalance(uniswapContract, tokenAddress, id):
    walletAddress_With0xRemoved = uniswapContract.replace("0x", "")
    payload = {
        "id": int(id),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": "0x70a08231000000000000000000000000" + walletAddress_With0xRemoved,
                "to": tokenAddress
            },
        ]
    }
    return payload


def GetPayload_Uniswap_EtherBalance(uniswapContract, id):
    payload = {
        "id": int(id),
        "jsonrpc": "2.0",
        "method": "eth_getBalance",
        "params": [
            uniswapContract,
        ]
    }
    return payload


def GetPayload_Kyber_GetConversionRate(kyberReserveAddress, src, dest, srcQty, blockNumber_int, id):
    import Contracts.contracts

    kwargs = {
        'src': Libraries.nodes.Instance_Web3.toChecksumAddress(src),
        'dest': Libraries.nodes.Instance_Web3.toChecksumAddress(dest),
        'srcQty': int(srcQty),
        'blockNumber': blockNumber_int,
    }
    # PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    payload = {
        "id": int(id),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberReserve.encodeABI('getConversionRate', kwargs=kwargs),
                "to": kyberReserveAddress,
            },
        ]
    }
    return payload


def GetPayload_Kyber_GetReservesConversionRateUpdateBlock(kyberReserveConversionRatesContract, tokenAddress, id):
    import Contracts.contracts

    kwargs = {
        'token': Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress),
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    payload = {
        "id": int(id),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberReserveConversionRates.encodeABI('getRateUpdateBlock', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserveConversionRatesContract),
            },
        ]
    }

    return payload


def GetPayload_Kyber_GetExpectedRate(sourceAddress, destinationAddress, sourceQuantity_weiUnits, id):
    import Contracts.contracts

    kwargs = {
        'src': Libraries.nodes.Instance_Web3.toChecksumAddress(sourceAddress),
        'dest': Libraries.nodes.Instance_Web3.toChecksumAddress(destinationAddress),
        'srcQty': sourceQuantity_weiUnits,
    }

    # PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    payload = {
        "id": int(id),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberNetworkProxy.encodeABI('getExpectedRate', kwargs=kwargs),
                "to": Exchanges.kyber.Contract_KyberNetworkProxy
            },
        ]
    }

    return payload


def SetExchangeTokensDictWithValueList(exchangeTokensDict):
    global ExchangeTokensDictWithValueList
    ExchangeTokensDictWithValueList = exchangeTokensDict


def GetCopyOfExchangeTokensDictWithValueList(exchangeName):
    global ExchangeTokensDictWithValueList
    return copy.deepcopy(ExchangeTokensDictWithValueList.GetItems(exchangeName))


def GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken(exchangeName, quoteToken):
    global ExchangeTokensDictWithValueList

    from Exchanges.zrxV2 import Contract_WETH
    from Libraries.accounts import TokenDict_Ninja

    # Only get prices for the tokens this exchange has
    # make sure to get a copy of this list, do not modify or break the original
    baseTokenList = GetCopyOfExchangeTokensDictWithValueList(exchangeName)
    if Libraries.defaults.VerboseLogging_GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken:
        PrintAndLog_FuncNameHeader("baseTokenList for exchangeName " + str(exchangeName) + ", quoteToken = " + str(
            quoteToken) + ". baseTokenList = " + str(baseTokenList))

    # Handle the special case of Ninja_DeleteAllTokensExceptForThoseInThisList, we need to remove tokens from baseTokenList to accommodate
    if Libraries.defaults.Ninja_DeleteAllTokensExceptForThoseInThisList:
        PrintAndLog_FuncNameHeader("Ninja_DeleteAllTokensExceptForThoseInThisList is set so we're removing tokens from the baseTokenList to accommodate this debug flag")
        baseTokenToRemoveFromList = []
        for symbol in TokenDict_Ninja:
            for baseToken in baseTokenList:
                # If the baseToken is not in TokenDict_Ninja
                if baseToken.lower() != TokenDict_Ninja[symbol].erc20TokenContractAddress.lower():
                    # Remove this baseToken from the baseTokenList because I must have manually removed it from TokenDict_Ninja for some reason.
                    # Maybe for debug purposes because of Ninja_DeleteAllTokensExceptForThoseInThisList?
                    baseTokenToRemoveFromList.append(baseToken.lower())

        PrintAndLog_FuncNameHeader("baseTokenToRemoveFromList = " + str(baseTokenToRemoveFromList))
        for baseTokenToRemove in baseTokenToRemoveFromList:
            if baseTokenToRemove in baseTokenList:
                baseTokenList.remove(baseTokenToRemove)

    # PrintAndLog_FuncNameHeader("baseTokenList before removing quote token of len " + str(len(baseTokenList)) + " = " + str(baseTokenList))
    # Remove the quote token from the baseTokenList
    if quoteToken in baseTokenList:
        baseTokenList.remove(quoteToken)

    # If this is ETH
    if quoteToken == Exchanges.keeperDAO.EthTokenContract.lower():
        # Do not include WETH since ETH and WETH are being treated as the same thing
        if Contract_WETH.lower() in baseTokenList:
            baseTokenList.remove(Contract_WETH.lower())

    if Libraries.defaults.VerboseLogging_GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken:
        PrintAndLog_FuncNameHeader("baseTokenList for " + str(exchangeName) + " after removing quoteToken. baseTokenList of len " + str(
            len(baseTokenList)) + " = " + str(baseTokenList))
    return baseTokenList


def EnforceTokenAddressIsERC20Version(tokenAddress):
    # Make sure that this is an ERC20 token, so in other words if this tokenAddress represents ETH, make it WETH
    if tokenAddress.lower() == Exchanges.keeperDAO.EthTokenContract.lower():
        return Exchanges.zrxV2.Contract_WETH
    else:
        return tokenAddress


def GetListOfInEfficientExchangesNames_ToGetMassPricesForManyTradeQuantities():
    global ExchangeDict

    resultList = []
    for exchangeName in ExchangeDict:
        exchange = ExchangeDict[exchangeName]
        if not exchange.IsEfficientToGetMassPricesForManyTradeQuantities():
            resultList.append(exchangeName)

    return resultList
