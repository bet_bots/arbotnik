import datetime
import json
import threading
import traceback
from enum import Enum
from threading import Lock

import jsonpickle
import requests

import Libraries.core
import Libraries.priceOracle
from Libraries.core import ConvertGweiToWei, ConvertWeiToGwei, EnforceMinMax, GetGasCost
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader
import Libraries.defaults

Lock_RecommendedGasPriceDict = Lock()
RecommendedGasPriceDict = {}

DateTimeOfLast_GetRecommendedGasPrices = None
IntervalToGetRecommendedGasPrices_minutes = 3

DateTimeOfLast_GetKybersMaxGasPriceValue = None
IntervalToGetKybersMaxGasPriceValue_minutes = 1
KyberMaxGasPrice_wei = Libraries.core.ConvertGweiToWei(50)

DateTimeOfLast_GetBancorMaxGasPriceValue = None
IntervalToGetBancorMaxGasPriceValue_minutes = 1
BancorMaxGasPrice_wei = Libraries.core.ConvertGweiToWei(4)

GasPrice_GlobalMax = ConvertGweiToWei(13451.100007)
GasPrice_GlobalMin = ConvertGweiToWei(1.000005)

DefaultGasPrice_Cheap = ConvertGweiToWei(9.10004)
GasPrice_Cheap_Min = GasPrice_GlobalMin

GasPrice_StaleOrderFromOrderBook_Min = ConvertGweiToWei(2.0003)
GasPrice_NewWebSocketOrder_Min = ConvertGweiToWei(10.10007)

ApproximateGasLimit_CancelOrder = 60000


class RecommendedGasPriceSpeed(Enum):
    safeLow = 1
    standard = 2
    fast = 3


class RecommendedGasPrice:
    name = None
    gasPrice = None
    estimatedWait_m = None

    def __init__(self, _name, _gasPrice, _estimatedWait_m=None):
        self.name = _name
        self.gasPrice = _gasPrice
        self.estimatedWait_m = _estimatedWait_m

    def GetDetails(self):
        return self.name + " gas price " + str(self.gasPrice) + " Gwei, estimated wait " + str(self.estimatedWait_m) + " min"


class GasPriceSetting(Enum):
    smashNotoriousBTC = 1
    newWebSocketOrder = 2
    staleOrderFromOrderBook = 3


class GasPriceCustomType(Enum):
    DynamicGasPrice = "Dynamic Gas Price"


def EnforceGlobalMinMaxGasPrices(gasPrice):
    return EnforceMinMax(gasPrice, GasPrice_GlobalMin, GasPrice_GlobalMax)


def GetRecommendedGasPrices_EthGasWatch(resultDict, resultKey):
    global RecommendedGasPriceDict
    global Lock_RecommendedGasPriceDict

    # Set the results in the resultDict
    resultDict[resultKey] = API_GetEthGasWatch()


def GetRecommendedGasPrices_EtherChain(resultDict, resultKey):
    global RecommendedGasPriceDict
    global Lock_RecommendedGasPriceDict

    # Set the results in the resultDict
    resultDict[resultKey] = API_GetEtherChainGasInfo()


def GetRecommendedGasPrices_EthGasStation(resultDict, resultKey):
    global RecommendedGasPriceDict
    global Lock_RecommendedGasPriceDict

    # Set the results in the resultDict
    resultDict[resultKey] = API_GetEthGasStationInfo()


def GetRecommendedGasPrices_EthNode(resultDict, resultKey):
    global RecommendedGasPriceDict
    global Lock_RecommendedGasPriceDict

    gasPrice_int_weiUnits, gasPrice_int_gweiUnits = Libraries.core.API_GetGasPrice()
    PrintAndLog_FuncNameHeader("gasPrice_int_gweiUnits = " + str(gasPrice_int_gweiUnits) + " gwei")
    safeLow = gasPrice_int_gweiUnits
    # TODO, what to put here?
    standard = gasPrice_int_gweiUnits
    # TODO, what to put here?
    fast = gasPrice_int_gweiUnits

    ethNodeGasPriceDict = {}

    dictKey = RecommendedGasPriceSpeed.safeLow
    ethNodeGasPriceDict[dictKey] = RecommendedGasPrice(dictKey, float(safeLow), 30)

    dictKey = RecommendedGasPriceSpeed.standard
    ethNodeGasPriceDict[dictKey] = RecommendedGasPrice(dictKey, float(standard), 5)

    dictKey = RecommendedGasPriceSpeed.fast
    ethNodeGasPriceDict[dictKey] = RecommendedGasPrice(dictKey, float(fast), 2)

    # Set the results in the resultDict
    resultDict[resultKey] = ethNodeGasPriceDict


def ConsiderGettingRecommendedGasPrices():
    global IntervalToGetRecommendedGasPrices_minutes
    global DateTimeOfLast_GetRecommendedGasPrices
    global RecommendedGasPriceDict
    global Lock_RecommendedGasPriceDict

    intervalToGetRecommendedGasPrices_seconds = IntervalToGetRecommendedGasPrices_minutes * 60
    try:
        if not DateTimeOfLast_GetRecommendedGasPrices or ((datetime.datetime.now() - DateTimeOfLast_GetRecommendedGasPrices).total_seconds() > intervalToGetRecommendedGasPrices_seconds):
            # PrintAndLog("Calling GetRecommendedGasPrices")
            # Set the datetime to keep track of how often we make the API call
            DateTimeOfLast_GetRecommendedGasPrices = datetime.datetime.now()

            threads = []
            resultDict = {}

            before = datetime.datetime.now()

            resultKey = "ethGasStation"
            t_EthGasStation = threading.Thread(target=GetRecommendedGasPrices_EthGasStation, args=(resultDict, resultKey,))
            threads.append(t_EthGasStation)
            t_EthGasStation.start()

            resultKey = "etherChain"
            t_EtherChain = threading.Thread(target=GetRecommendedGasPrices_EtherChain, args=(resultDict, resultKey,))
            threads.append(t_EtherChain)
            t_EtherChain.start()

            resultKey = "ethGasWatch"
            t_EthGasWatch = threading.Thread(target=GetRecommendedGasPrices_EthGasWatch, args=(resultDict, resultKey,))
            threads.append(t_EthGasWatch)
            t_EthGasWatch.start()

            resultKey = "ethNode"
            t_EthNode = threading.Thread(target=GetRecommendedGasPrices_EthNode, args=(resultDict, resultKey,))
            threads.append(t_EthNode)
            t_EthNode.start()

            PrintAndLog_FuncNameHeader("Waiting for threads to finish")
            for thread in threads:
                thread.join()

            duration_s = (datetime.datetime.now() - before).total_seconds()
            PrintAndLog_FuncNameHeader("duration_s = " + str(duration_s) + " seconds")

            # in a thread safe manor, analyze the results from a variety of Ethereum gasstations.
            # Use the greatest number from each category. Example; if ethgasstation says fast price is 5 Gwei and etherchain says it's 8 Gwei, go with 8.
            # If only one succeeds and the others fail, just go with that one.
            Lock_RecommendedGasPriceDict.acquire()
            try:
                RecommendedGasPriceDict = {}

                for gasStationKey in resultDict.copy():
                    try:
                        # PrintAndLog_FuncNameHeader("resultDict[gasStationKey] = " + str(resultDict[gasStationKey]))
                        dictKeyList = [RecommendedGasPriceSpeed.safeLow, RecommendedGasPriceSpeed.standard, RecommendedGasPriceSpeed.fast]
                        # PrintAndLog_FuncNameHeader("dictKeyList = " + str(dictKeyList))
                        for dictKey in dictKeyList:
                            # PrintAndLog_FuncNameHeader("Iterating over " + str(dictKey))
                            if dictKey in resultDict[gasStationKey]:
                                # If we do not yet have this dictKey
                                if dictKey not in RecommendedGasPriceDict:
                                    RecommendedGasPriceDict[dictKey] = resultDict[gasStationKey][dictKey]
                                    PrintAndLog_FuncNameHeader("dictKey not yet in dict, so setting " + str(dictKey) + " from " + str(
                                        gasStationKey) + " as gasPrice = " + str(resultDict[gasStationKey][dictKey].gasPrice))

                                # We already have the dictKey, check to see if this gasStation is reporting a higher gas price for this dictKey
                                else:
                                    # PrintAndLog_FuncNameHeader("dictKey already in dict, comparing RecommendedGasPriceDict[dictKey].gasPrice =  " + str(
                                    #     RecommendedGasPriceDict[dictKey].gasPrice) + " with " + str(gasStationKey) + "'s gasPrice = " + str(
                                    #     resultDict[gasStationKey][dictKey].gasPrice))
                                    if float(resultDict[gasStationKey][dictKey].gasPrice) > float(RecommendedGasPriceDict[dictKey].gasPrice):
                                        RecommendedGasPriceDict[dictKey] = resultDict[gasStationKey][dictKey]
                                        PrintAndLog_FuncNameHeader("Setting new " + str(
                                            dictKey) + " via " + str(gasStationKey) + " as gasPrice = " + str(
                                            resultDict[gasStationKey][dictKey].gasPrice))
                                    else:
                                        PrintAndLog_FuncNameHeader("NOT setting new " + str(
                                            dictKey) + " because it didn't exceed the one that already exists")

                    except:
                        PrintAndLogError("exception in ConsiderGettingRecommendedGasPrices: " + traceback.format_exc())

                PrintAndLog_FuncNameHeader("RecommendedGasPriceDict set to: " + str(jsonpickle.encode(RecommendedGasPriceDict)))

                if Libraries.defaults.UseDebugGasStation:
                    PrintAndLog_FuncNameHeader("UseDebugGasStation was set so we're putting debug gas prices in RecommendedGasPriceDict")
                    for key in RecommendedGasPriceDict:
                        # Hard code this to something that fits debug purposes
                        RecommendedGasPriceDict[key].gasPrice = 1.0

            finally:
                Lock_RecommendedGasPriceDict.release()

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception in ConsiderGettingRecommendedGasPrices = " + traceback.format_exc())


def ConsiderGettingKybersMaxGasPriceValue():
    global IntervalToGetKybersMaxGasPriceValue_minutes
    global DateTimeOfLast_GetKybersMaxGasPriceValue
    global KyberMaxGasPrice_wei

    from Exchanges.kyber import API_GetMaxGasPriceValue

    intervalToGetKybersMaxGasPriceValue_seconds = IntervalToGetKybersMaxGasPriceValue_minutes * 60
    try:
        if not DateTimeOfLast_GetKybersMaxGasPriceValue or (
                (datetime.datetime.now() - DateTimeOfLast_GetKybersMaxGasPriceValue).total_seconds() > intervalToGetKybersMaxGasPriceValue_seconds):
            # PrintAndLog("Calling GetKybersMaxGasPriceValue")
            # Set the datetime to keep track of how often we make the API call
            DateTimeOfLast_GetKybersMaxGasPriceValue = datetime.datetime.now()
            KyberMaxGasPrice_wei = Libraries.core.ConvertGweiToWei(API_GetMaxGasPriceValue())

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception in ConsiderGettingKybersMaxGasPriceValue = " + traceback.format_exc())


def ConsiderGettingBancorsMaxGasPriceValue():
    global IntervalToGetBancorMaxGasPriceValue_minutes
    global DateTimeOfLast_GetBancorMaxGasPriceValue
    global BancorMaxGasPrice_wei

    from Exchanges.bancor import API_GetBancorMaxGasPriceLimit, API_GetBancorGasPriceLimitContract

    intervalToGetBancorMaxGasPriceValue_seconds = IntervalToGetBancorMaxGasPriceValue_minutes * 60
    try:
        if not DateTimeOfLast_GetBancorMaxGasPriceValue or (
                (datetime.datetime.now() - DateTimeOfLast_GetBancorMaxGasPriceValue).total_seconds() > intervalToGetBancorMaxGasPriceValue_seconds):
            # PrintAndLog("Calling GetBancorMaxGasPriceValue")
            # Set the datetime to keep track of how often we make the API call
            DateTimeOfLast_GetBancorMaxGasPriceValue = datetime.datetime.now()
            # First I must ask the BancorNetwork Contract for the proper GasPriceLimitContract
            API_GetBancorGasPriceLimitContract()
            # Then I can ask the GasPriceLimitContract for the gas price limit
            BancorMaxGasPrice_wei = Libraries.core.ConvertGweiToWei(API_GetBancorMaxGasPriceLimit())
            PrintAndLog("Set BancorMaxGasPrice_wei to " + str(BancorMaxGasPrice_wei) + " wei and " + str(Libraries.core.ConvertWeiToGwei(BancorMaxGasPrice_wei)) + " gwei")

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception in ConsiderGettingBancorMaxGasPriceValue = " + traceback.format_exc())


def GetMaxGasPrice_WhenArbitragingBetweenBancorAndKyber():
    global KyberMaxGasPrice_wei
    global BancorMaxGasPrice_wei
    return min(BancorMaxGasPrice_wei, KyberMaxGasPrice_wei)


def GetMaxGasPrice_WhenArbitragingBetweenSetAndKyber():
    global KyberMaxGasPrice_wei
    return KyberMaxGasPrice_wei


def GetMaxGasPrice_WhenArbitragingBetweenUniswapAndKyber():
    global KyberMaxGasPrice_wei
    return KyberMaxGasPrice_wei


def API_GetEthGasWatch():
    url = "https://ethgas.watch/api/gas"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("API_GetEthGasWatch jData = " + str(jData))

        # Prepare the returnDict
        returnDict = {}

        dictKey = RecommendedGasPriceSpeed.safeLow
        returnDict[dictKey] = RecommendedGasPrice(dictKey, float(jData['slow']['gwei']), 30)

        dictKey = RecommendedGasPriceSpeed.standard
        returnDict[dictKey] = RecommendedGasPrice(dictKey, float(jData['normal']['gwei']), 5)

        dictKey = RecommendedGasPriceSpeed.fast
        returnDict[dictKey] = RecommendedGasPrice(dictKey, float(jData['fast']['gwei']), 2)

        return returnDict


def API_GetEtherChainGasInfo():
    url = "https://www.etherchain.org/api/gasPriceOracle"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("API_GetEtherChainGasInfo jData = " + str(jData))

        # Prepare the returnDict
        returnDict = {}

        dictKey = RecommendedGasPriceSpeed.safeLow
        returnDict[dictKey] = RecommendedGasPrice(dictKey, float(jData['safeLow']), 30)

        dictKey = RecommendedGasPriceSpeed.standard
        returnDict[dictKey] = RecommendedGasPrice(dictKey, float(jData['standard']), 5)

        dictKey = RecommendedGasPriceSpeed.fast
        returnDict[dictKey] = RecommendedGasPrice(dictKey, float(jData['fast']), 2)

        return returnDict


def API_GetEthGasStationInfo():
    url = "https://ethgasstation.info/json/priceWait.json"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("API_GetEthGasStationInfo jData = " + str(jData))

        # Prepare the return dict
        returnDict = {}

        for data in jData:
            delay = None
            prop = 'delay'
            prop2 = 'delay2'
            prop3 = 'delay3'
            if prop in data and data[prop]:
                delay = float(data[prop])
            elif prop2 in data and data[prop2]:
                delay = float(data[prop2])
            elif prop3 in data and data[prop3]:
                delay = float(data[prop3])

            minedGasPrice = float(data['minedGasPrice'])
            # PrintAndLog("delay = " + str(delay) + ", minedGasPrice = " + str(minedGasPrice))

            if minedGasPrice > 0 and delay:
                dictKey = RecommendedGasPriceSpeed.safeLow
                if dictKey not in returnDict and delay < 30.0:
                    returnDict[dictKey] = RecommendedGasPrice(dictKey, float(minedGasPrice), 30)

                dictKey = RecommendedGasPriceSpeed.standard
                if dictKey not in returnDict and delay < 5.0:
                    returnDict[dictKey] = RecommendedGasPrice(dictKey, float(minedGasPrice), 5)

                dictKey = RecommendedGasPriceSpeed.fast
                if dictKey not in returnDict and delay < 2.0:
                    returnDict[dictKey] = RecommendedGasPrice(dictKey, float(minedGasPrice), 2)

        return returnDict


def GetGasPrice_SafeLow():
    global RecommendedGasPriceDict
    global Lock_RecommendedGasPriceDict

    try:
        Lock_RecommendedGasPriceDict.acquire()
        try:
            adder = 0.0004005
            # PrintAndLog("GetGasPrice_SafeLow RecommendedGasPriceDict[RecommendedGasPriceSpeed.safeLow].estimatedWait_m = " + str(
            #     RecommendedGasPriceDict[RecommendedGasPriceSpeed.safeLow].estimatedWait_m))
            return max(int(GasPrice_Cheap_Min), int(EnforceGlobalMinMaxGasPrices(ConvertGweiToWei(RecommendedGasPriceDict[RecommendedGasPriceSpeed.safeLow].gasPrice + adder))))

        finally:
            Lock_RecommendedGasPriceDict.release()

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception = " + traceback.format_exc())
        return DefaultGasPrice_Cheap


def GetGasPrice_Cheap():
    global RecommendedGasPriceDict
    global Lock_RecommendedGasPriceDict

    try:
        Lock_RecommendedGasPriceDict.acquire()
        try:
            adder = 0.0005006
            multiplier = 1.00
            if RecommendedGasPriceDict[RecommendedGasPriceSpeed.safeLow].estimatedWait_m < 5:
                return max(int(GasPrice_Cheap_Min),
                           int(EnforceGlobalMinMaxGasPrices(ConvertGweiToWei((RecommendedGasPriceDict[RecommendedGasPriceSpeed.safeLow].gasPrice + adder) * multiplier))))
            else:
                return max(int(GasPrice_Cheap_Min),
                           int(EnforceGlobalMinMaxGasPrices(ConvertGweiToWei((RecommendedGasPriceDict[RecommendedGasPriceSpeed.standard].gasPrice + adder) * multiplier))))

        finally:
            Lock_RecommendedGasPriceDict.release()

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception = " + traceback.format_exc())
        return DefaultGasPrice_Cheap


def GetGasPrice_Fast():
    global RecommendedGasPriceDict
    global Lock_RecommendedGasPriceDict

    try:
        Lock_RecommendedGasPriceDict.acquire()
        try:
            adder = 1
            multiplier = 1.10
            return int(EnforceGlobalMinMaxGasPrices(ConvertGweiToWei((RecommendedGasPriceDict[RecommendedGasPriceSpeed.fast].gasPrice + adder) * multiplier)))

        finally:
            Lock_RecommendedGasPriceDict.release()

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception = " + traceback.format_exc())
        return GasPrice_StaleOrderFromOrderBook_Min


def GetGasPrice_CancelOrder():
    adder_wei = ConvertGweiToWei(1)
    return int(EnforceGlobalMinMaxGasPrices((GetGasPrice_Cheap() + adder_wei) * 1.25))


def ActualGasUsage_EtherDeltaAnd0x_CancelOrder():
    return 59800


def GetGasPrice_StaleOrderFromOrderBook_Min():
    # 1.50 worked well here, trying to lower it to see if I can save gas and trade with a lower min profit requirement
    return int(EnforceGlobalMinMaxGasPrices(max(max(GetGasPrice_Cheap() * 1.05, GetGasPrice_CancelOrder() * 1.11), GasPrice_StaleOrderFromOrderBook_Min)))


def GetGasPrice_StaleOrderFromOrderBook_Max():
    return int(EnforceGlobalMinMaxGasPrices(GetGasPrice_StaleOrderFromOrderBook_Min() * 2.65))


def GetGasPrice_CancelOrder_HighPriority():
    return int(EnforceGlobalMinMaxGasPrices(min(GetGasPrice_CancelOrder() * 1.70, GetGasPrice_StaleOrderFromOrderBook_Min())))


def GetGasPrice_NewWebSocketOrder_Min():
    # 1.7 worked well here, trying to lower it to see if I can save gas and trade with a lower min profit requirement
    return int(EnforceGlobalMinMaxGasPrices(max(GasPrice_NewWebSocketOrder_Min, GetGasPrice_Fast() * 1.6)))


def GetGasPrice_NewWebSocketOrder_Max():
    return int(EnforceGlobalMinMaxGasPrices(max(GetGasPrice_NewWebSocketOrder_Min(), GetGasPrice_NewWebSocketOrder_Min() * 2.25)))


def GetGasPrice_SmashNotoriousBTC_Min():
    return int(EnforceGlobalMinMaxGasPrices(max(ConvertGweiToWei(100.10002), GetGasPrice_NewWebSocketOrder_Min() * 1.15)))


def GetGasPrice_SmashNotoriousBTC_Max():
    return int(EnforceGlobalMinMaxGasPrices(max(ConvertGweiToWei(125.10002), GetGasPrice_NewWebSocketOrder_Min() * 1.15)))


def GetGasLimit_Trade(exchangeContractAddress=None, tokenContractAddress=None):
    # DNT, transfers cost a lot...
    if tokenContractAddress.lower() == "0x0abdace70d3790235af448c88547603b945604ea".lower():
        return 300000
    # DNT, transfers cost a lot...
    elif tokenContractAddress.lower() == "0x0abdace70d3790235af448c88547603b945604ea".lower():
        return 300000
    # FUN, transfers MAY cost more...
    elif tokenContractAddress.lower() == "0x419d0d8bdd9af5e606ae2232ed285aff190e711b".lower():
        return 300000
    else:
        return 180000


# GetGasLimit is how much gas limit we should supply the trade, this includes some extra just in case
# GetGasExpectedToSpend will be how much gas we actually expect to use most of the time

# This is the estimated overhead cost of trading through KeeperDAO
# Overhead cost here means the difference in cost to trade through KeeperDAO with flash loan vs to trade without keeperDAO
def GetGasExpectedToSpend_TradeWithFlashLoanThroughKeeperDAO_OverheadCostOnly():
    return 50000


def GetGasLimit_Trade_Bancor():
    return 690000


def GetGasLimit_Trade_Kyber():
    return 700000


def GetGasExpectedToSpend_Trade_Kyber():
    return 563779


def GetGasLimit_Trade_Uniswap():
    return 89000


def GetGasExpectedToSpend_Trade_Uniswap():
    return 72000


def GetGasLimit_Trade_Airswap():
    return 190000


def GetGasLimit_Other():
    return 190000


def GetGasLimit_SendingEther():
    return 59000


def GetGasLimit_Estimated_TradeCustomContract():
    return 764080


def GetGasLimit_TradeCustomContract():
    return 999999


def GetGasLimit_Ninja_KyberAnd0x():
    return 1199999


def GetGasLimit_Ninja_BancorAndKyber():
    return 1299999


def GetGasLimit_Ninja_UseThisForTradingProfitEstimations_BancorAndKyber():
    return 695000


def GetGasLimit_Ninja_UseThisForTradingProfitEstimations_UniswapAndKyber():
    return 501212


def GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAndKyber():
    return 910845


def GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAndKyber_UseCTokenBidderContract():
    return 1173182


def GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAndUniswap():
    return 432693


def GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAnd0x():
    return 535163


def GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAnd0x_UseCTokenBidderContract():
    return 823565


def GetGasLimit_Ninja_BancorAnd0x():
    return 999999


def GetGasLimit_TradeDaiMatchingMaker():
    return 1600000


def GetGasLimit_Set_StartRebalance():
    return 1599296


def GetGasLimit_Ninja_UseThisForTradingProfitEstimations():
    return 799999


# Gas usage from set documentation
# 1 cToken Inflow, 1 non-cToken Outflow
# 595,768
# 1 non-cToken Inflow, 1 cToken Outflow
# 477,158
# 1 non-cToken Inflow, 1 non-cToken Outflow
# 389,451

def GetGasLimit_Trade_Set():
    return 635768


def GetGasLimit_Arby_Set(useSetCTokenBidderContract):
    if useSetCTokenBidderContract:
        return GetGasLimit_Arby_Set_IncludingCToken()
    else:
        return GetGasLimit_Arby_Set_NoCTokens()


def GetGasLimit_Arby_Set_UseThisForTradingProfitEstimations(useSetCTokenBidderContract):
    if useSetCTokenBidderContract:
        return GetGasLimit_Arby_Set_IncludingCToken_UseThisForTradingProfitEstimations()
    else:
        return GetGasLimit_Arby_Set_NoCTokens_UseThisForTradingProfitEstimations()


def GetGasLimit_Arby_Set_NoCTokens_UseThisForTradingProfitEstimations():
    return 389451


def GetGasLimit_Arby_Set_NoCTokens():
    return int(GetGasLimit_Arby_Set_NoCTokens_UseThisForTradingProfitEstimations() * 1.4)


def GetGasLimit_Arby_Set_IncludingCToken_UseThisForTradingProfitEstimations():
    return 536463


def GetGasLimit_Arby_Set_IncludingCToken():
    return int(GetGasLimit_Arby_Set_IncludingCToken_UseThisForTradingProfitEstimations() * 1.4)


def GetSuggested_Taker_MinimumProfitRequirement_NewWebSocketOrder(gasPrice=None, exchangeContractAddress=None, tokenContractAddress=None):
    if not gasPrice:
        gasPrice = GetGasPrice_NewWebSocketOrder_Min()

    gasLimit = GetGasLimit_Trade(exchangeContractAddress, tokenContractAddress)

    return GetSuggested_Taker_MinimumProfitRequirement(gasPrice, gasLimit)


def GetSuggested_Taker_MinimumProfitRequirement_StaleOrderFromOrderBook(gasPrice=None, exchangeContractAddress=None, tokenContractAddress=None):
    if not gasPrice:
        gasPrice = GetGasPrice_StaleOrderFromOrderBook_Min()

    gasLimit = GetGasLimit_Trade(exchangeContractAddress, tokenContractAddress)

    return GetSuggested_Taker_MinimumProfitRequirement(gasPrice, gasLimit)


def GetSuggested_Taker_MinimumProfitRequirement_Bancor(gasPrice=None):
    if not gasPrice:
        gasPrice = GetGasPrice_StaleOrderFromOrderBook_Min()

    return GetSuggested_Taker_MinimumProfitRequirement(gasPrice, GetGasLimit_Trade_Bancor())


def GetSuggested_Taker_MinimumProfitRequirement(gasPrice_wei=None, approximateGasLimit_Trade=None):
    if not gasPrice_wei:
        gasPrice_wei = GetGasPrice_NewWebSocketOrder_Min()

    if not approximateGasLimit_Trade:
        approximateGasLimit_Trade = GetGasLimit_Trade()

    gasCost_ether = GetGasCost(gasPrice_wei, approximateGasLimit_Trade)
    # PrintAndLog("gasCost_ether = " + str(gasCost_ether))
    if gasCost_ether < 0.0033:
        minimumProfitRequirement_ether = 0.016
    elif gasCost_ether < 0.005:
        minimumProfitRequirement_ether = 0.021
    elif gasCost_ether < 0.01:
        minimumProfitRequirement_ether = 0.028
    elif gasCost_ether < 0.02:
        minimumProfitRequirement_ether = 0.038
    elif gasCost_ether < 0.04:
        minimumProfitRequirement_ether = 0.055
    elif gasCost_ether < 0.06:
        minimumProfitRequirement_ether = 0.075
    elif gasCost_ether < 0.08:
        minimumProfitRequirement_ether = 0.095
    else:
        minimumProfitRequirement_ether = 0.10

    # PrintAndLog("gasCost_ether = " + str(gasCost_ether) + ", when gasPrice_wei = " + str(ConvertWeiToGwei(gasPrice_wei)) + " Gwei, minimumProfitRequirement_ether = " + str(minimumProfitRequirement_ether))
    return minimumProfitRequirement_ether


def GetSuggested_MinimumProfitRequirement(gasPrice, gas):
    minimumProfitRequirement_ether = GetGasCost(gasPrice, gas)
    # PrintAndLog("GetSuggested_MinimumProfitRequirement: minimumProfitRequirement_ether = " + str(
    #     minimumProfitRequirement_ether) + ", when gasPrice = " + str(ConvertWeiToGwei(gasPrice)) + " Gwei and gas = " + str(gas))
    return minimumProfitRequirement_ether


def GetSuggested_MinimumProfitRequirement_NinjaWithHardCodedGas(gasPrice, gas):
    # TODO NOTE!  I'm not using gas sent in here because it's usually an over estimated value with extra gas built in as padding
    # overriding gas with something that's more realistic that makes my trading logic more aggressive
    gas = GetGasLimit_Ninja_UseThisForTradingProfitEstimations()
    minimumProfitRequirement_ether = GetGasCost(gasPrice, gas)

    PrintAndLog("GetSuggested_MinimumProfitRequirement_NinjaWithHardCodedGas: minimumProfitRequirement_ether = " + str(minimumProfitRequirement_ether) + ", when gasPrice = " + str(
        ConvertWeiToGwei(gasPrice)) + " Gwei")
    return minimumProfitRequirement_ether


def CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens(
        expectedProfit_denominationToken, expectedGasToSpend_wei, percentageProfitToSpendOnGas,
        profitDenominationTokenAddress=Libraries.core.GetEtherContractAddress()):
    from Libraries.cache import GetSymbolForTokenContract_UseCachingSystem

    # I must convert expectedProfit_denominationToken to expectedProfit_eth since ETH is used to pay gas
    price_denominationToken = Libraries.priceOracle.GetOnChainPrice_FromCache(profitDenominationTokenAddress.lower())
    if Libraries.defaults.VerboseLogging_CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens:
        PrintAndLog_FuncNameHeader("price_denominationToken = " + str(price_denominationToken))
    expectedProfit_eth = Libraries.core.ConvertBaseTokensToQuoteTokens(expectedProfit_denominationToken, price_denominationToken)
    if Libraries.defaults.VerboseLogging_CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens:
        PrintAndLog_FuncNameHeader("expectedProfit_eth = " + str(expectedProfit_eth))
    symbol_denominationToken = GetSymbolForTokenContract_UseCachingSystem(profitDenominationTokenAddress)
    if Libraries.defaults.VerboseLogging_CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens:
        PrintAndLog_FuncNameHeader("These two amounts should be equal in value = " + str(
            expectedProfit_eth) + " ETH, and " + str(expectedProfit_denominationToken) + " " + str(symbol_denominationToken.upper()))

    # Calculate how much ether we want to spend on gas cost
    expectedProfitToSpendOnTxGasCost_eth = expectedProfit_eth * percentageProfitToSpendOnGas
    # if VerboseLogging_CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens:
    #     PrintAndLog_FuncNameHeader("expectedProfitToSpendOnTxGasCost_eth = " + str(expectedProfitToSpendOnTxGasCost_eth) + " ETH")

    # We know the expectedGasToSpend_wei, and we know the expectedProfitToSpendOnTxGasCost_eth
    # So simply solve for the gasPrice_ether and then convert it to the units we need
    gasPrice_ether = expectedProfitToSpendOnTxGasCost_eth / expectedGasToSpend_wei
    gasPrice_wei = Libraries.core.ConvertEtherToWei(gasPrice_ether, Libraries.core.Ether_Decimals)
    gasPrice_gwei = Libraries.core.ConvertWeiToGwei(gasPrice_wei)
    if Libraries.defaults.VerboseLogging_CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens:
        # PrintAndLog_FuncNameHeader("gasPrice_ether = " + str(gasPrice_ether))
        PrintAndLog_FuncNameHeader("gasPrice_wei = " + str(gasPrice_wei))
        PrintAndLog_FuncNameHeader("gasPrice_gwei = " + str(gasPrice_gwei) + " gwei. When expectedProfit_eth = " + str(
            expectedProfit_eth) + ", expectedGasToSpend_wei = " + str(expectedGasToSpend_wei) + ", and percentageProfitToSpendOnGas = " + str(percentageProfitToSpendOnGas))

    if gasPrice_wei <= 0:
        raise Exception("gasPrice_wei was calculated to be negative. This is not allowed! "
                        "You probably passed in a negative value to this function. expectedProfit_eth "
                        "= " + str(expectedProfit_eth) + ", expectedGasToSpend_wei = " +
                        str(expectedGasToSpend_wei) + " percentageProfitToSpendOnGas = " + str(percentageProfitToSpendOnGas))
    return gasPrice_wei


def GetMinProfitRequirement_GivenGasProperties(
        gasPrice, gas, profitDenominationTokenAddress=Libraries.core.GetEtherContractAddress()):
    from Libraries.cache import GetSymbolForTokenContract_UseCachingSystem

    # PrintAndLog_FuncNameHeader("gasPrice = " + str(gasPrice))
    # PrintAndLog_FuncNameHeader("gas = " + str(gas))
    # PrintAndLog_FuncNameHeader("profitDenominationTokenAddress = " + str(profitDenominationTokenAddress))

    if gasPrice == GasPriceCustomType.DynamicGasPrice:
        # Just plug in a small gas price here, like based on GetGasPrice_SafeLow because this will ultimately be dynamic
        # and the min we allow will be based on something small like GetGasPrice_SafeLow
        minProfitRequirement_ether = GetSuggested_MinimumProfitRequirement(GetGasPrice_SafeLow(), gas)
        if Libraries.defaults.VerboseLogging_GetMinProfitRequirement_GivenGasProperties:
            PrintAndLog_FuncNameHeader("minProfitRequirement_ether = " + str(minProfitRequirement_ether) + " because it will be dynamic just like our DynamicGasPrice")
            PrintAndLog_FuncNameHeader("GetGasPrice_SafeLow() = " + str(GetGasPrice_SafeLow()))
    else:
        # Set the minProfitRequirement_ether based on the gas price
        minProfitRequirement_ether = GetSuggested_MinimumProfitRequirement(gasPrice, gas)
        if Libraries.defaults.VerboseLogging_GetMinProfitRequirement_GivenGasProperties:
            PrintAndLog_FuncNameHeader("minProfitRequirement_ether = " + str(minProfitRequirement_ether))

    # Convert minProfitRequirement_ether to the quoteToken units we're expecting based on profitDenominationTokenAddress
    price_denominationToken = Libraries.priceOracle.GetOnChainPrice_FromCache(profitDenominationTokenAddress)

    minProfitRequirement_quoteToken = Libraries.core.ConvertQuoteTokensToBaseTokens(minProfitRequirement_ether, price_denominationToken)
    symbol_denominationToken = GetSymbolForTokenContract_UseCachingSystem(profitDenominationTokenAddress)

    if Libraries.defaults.VerboseLogging_GetMinProfitRequirement_GivenGasProperties:
        PrintAndLog_FuncNameHeader("price_denominationToken = " + str(price_denominationToken) + ", profitDenominationTokenAddress = " + str(profitDenominationTokenAddress))
        PrintAndLog_FuncNameHeader("minProfitRequirement_quoteToken = " + str(minProfitRequirement_quoteToken))
        PrintAndLog_FuncNameHeader("These two amounts should be equal in value = " + str(
            minProfitRequirement_ether) + " ETH, and " + str(minProfitRequirement_quoteToken) + " " + str(symbol_denominationToken.upper()))

    return minProfitRequirement_quoteToken


def GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade_NinjaCounterToNotoriousBTC(expectedProfit_ether):
    gas = 125000
    # Notorious BTC seems to use about 2,500 or 2,600 Gwei per ether expected profit.
    # So to beat him, use about 2,700.  That's an insane amount.
    gasPrice = int(2750000000000 * expectedProfit_ether)

    # Let's also cap it for now
    cappedDueToHighGasCost = False

    # maxIllGo = 451000020000
    maxIllGo = 4100000200
    if gasPrice > maxIllGo:
        cappedDueToHighGasCost = True
        gasPrice = maxIllGo

    estimatedGasCost_ether = Libraries.core.GetGasCost(gasPrice, gas)

    PrintAndLog("gasPrice = " + str(gasPrice) + " and gas = " + str(gas) + " estimatedGasCost_ether = " + str(
        estimatedGasCost_ether) + " ETH, cappedDueToHighGasCost = " + str(cappedDueToHighGasCost))
    return gas, gasPrice, cappedDueToHighGasCost, estimatedGasCost_ether


def GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade(expectedProfit_ether, gasPriceSetting, exchangeContractAddress=None, tokenContractAddress=None):
    if gasPriceSetting == GasPriceSetting.smashNotoriousBTC:
        gasPrice, constraint = GetGasPriceBasedOn_SmashNotoriousBTC(expectedProfit_ether)

    elif gasPriceSetting == GasPriceSetting.newWebSocketOrder:
        gasPrice, constraint = GetGasPriceBasedOn_NewWebSocketOrder(expectedProfit_ether)

    elif gasPriceSetting == GasPriceSetting.staleOrderFromOrderBook:
        gasPrice, constraint = GetGasPriceBasedOn_StaleOrderFromOrderBook(expectedProfit_ether)

    else:
        raise ValueError("GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade doesn't know this GasPriceSetting, is this a new GasPriceSetting?")

    gas = GetGasLimit_Trade(exchangeContractAddress, tokenContractAddress)

    PrintAndLog("GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade: " + str(gasPriceSetting) + ", expectedProfit_ether = " + str(
        expectedProfit_ether) + ", gasPrice = " + str(gasPrice) + " (" + str(
        Libraries.core.ConvertWeiToGwei(gasPrice)) + " Gwei), gas = " + str(gas) + ", constraint = " + str(constraint))
    estimatedGasCost_ether = gasPrice * gas / float(Libraries.core.Ether_Decimals)
    PrintAndLog("estimatedGasCost_ether = " + str(estimatedGasCost_ether))
    return gas, gasPrice


def GetGasPriceBasedOn_SmashNotoriousBTC(expectedProfit_ether):
    PrintAndLog("GetGasAndGasPrice_Trade_BasedOnExpectedProfitOnTrade with gasPriceSetting == GasPriceSetting.smashNotoriousBTC, expectedProfit_ether = " + str(expectedProfit_ether))
    # Notorious BTC has been using slightly less gas for less profit and more gas for more profit. I'll document that here.
    # 1,706 Gwei to profit 0.3 ETH -> 5687 Gwei per ETH expected profit. He did this on REQ which is a very popular market.
    # 533 Gwei to profit 0.339 ETH -> 1573 Gwei per ETH expected profit
    # 11,248 Gwei to profit 3.5 ETH -> 3214 Gwei per ETH expected profit
    # 199 Gwei to profit 1.622 ETH -> 122 Gwei per ETH expected profit
    # 5,426 Gwei to profit 3.2 ETH -> 1695.625 Gwei per ETH expected profit
    # 4500 Gwei so far is 100% success rate! but is wicked expensive so let's tone it down and see what we can settle with especially so we can raise that max we pass into EnforceMinAndMaxValues_GivenValue

    # 2,690 Gwei to profit 0.46 ETH. He spent 0.48830628604797 Ether ($175.02) to win 0.46 ETH.... wtf?

    # Calculate a multiplier based on expectedProfit_ether.
    # The multiplier represents the Gwei to expected profit in ETH

    if expectedProfit_ether < 0.09:
        PrintAndLog("")
        # multiplier = Libraries.core.ConvertGweiToWei(1000.0000002)
        multiplier = Libraries.core.ConvertGweiToWei(600.0000002)
    elif expectedProfit_ether < 0.5:
        PrintAndLog("")
        # multiplier = Libraries.core.ConvertGweiToWei(1700.0000002)
        multiplier = Libraries.core.ConvertGweiToWei(600.0000002)
    else:
        # multiplier = Libraries.core.ConvertGweiToWei(3400.0000002)
        multiplier = Libraries.core.ConvertGweiToWei(600.0000002)

    gasPrice = int(multiplier * expectedProfit_ether)
    # Enforce min and max values that i'm comfortable with
    constrainedGasPrice, constraint = Libraries.core.EnforceMinAndMaxValues_GivenValue(gasPrice, GetGasPrice_SmashNotoriousBTC_Min(),
                                                                                       GetGasPrice_SmashNotoriousBTC_Max())
    # If it's not constrained or it's constrained by the min
    if not constraint or constraint == "min":
        gasPrice = constrainedGasPrice
    else:
        PrintAndLog("Competing with Notorious BTC, and our gas price was constrained by our max. constraint = " + str(
            constraint) + ". Either I need to increase this max and be willing to gamble with higher gas prices, "
                          "or I need to be willing to throw this lower gas price at this transaction knowing Notorious BTC will beat me and this gas will be wasted... ")
        PrintAndLog("Calling GetGasPriceBasedOn_NewWebSocketOrder so I don't waste gas until i'm ready to raise my maximum limit")
        gasPrice, constraint = GetGasPriceBasedOn_NewWebSocketOrder(expectedProfit_ether)

    return gasPrice, constraint


def GetGasPriceBasedOn_NewWebSocketOrder(expectedProfit_ether):
    # The way this seems to work, if this multiplier's Gwei is at 70.0, then if expected profit is 1.0 then gasPrice will be 70 Gwei. Then it gets constrained by the min and maxes below.
    multiplier = Libraries.core.ConvertGweiToWei(1270.0000002)
    gasPrice = int(multiplier * expectedProfit_ether)
    # Enforce min and max values
    constrainedGasPrice, constraint = Libraries.core.EnforceMinAndMaxValues_GivenValue(
        gasPrice, GetGasPrice_NewWebSocketOrder_Min(), GetGasPrice_NewWebSocketOrder_Max())
    gasPrice = constrainedGasPrice

    return gasPrice, constraint


def GetGasPriceBasedOn_StaleOrderFromOrderBook(expectedProfit_ether):
    # The way this seems to work, if this multiplier's Gwei is at 70.0, then if expected profit is 1.0 then gasPrice will be 70 Gwei. Then it gets constrained by the min and maxes below.
    multiplier = Libraries.core.ConvertGweiToWei(1270.0000002)
    gasPrice = int(multiplier * expectedProfit_ether)
    # Enforce min and max values
    constrainedGasPrice, constraint = Libraries.core.EnforceMinAndMaxValues_GivenValue(
        gasPrice, GetGasPrice_StaleOrderFromOrderBook_Min(), GetGasPrice_StaleOrderFromOrderBook_Max())
    gasPrice = constrainedGasPrice

    return gasPrice, constraint


def GetMinGasPrice_ArbitrageGeneric():
    # This sets the minimum gasPrice for trading through ArbitrageGeneric
    # The SafeLow trade opportunities have been extremely hard to capture lately, so I can set this to Fast to completely avoid them and leave them be
    return GetGasPrice_SafeLow()
    # return GetGasPrice_Cheap()
    # return GetGasPrice_Fast()


def GetMaxOpeningBidGasPrice():
    # Make it a little faster than Fast. I can fine tune this in the future
    return GetGasPrice_Fast() * 1.1


def GetMaxGasPrice_DebuggingTailgatingAndForcingTradesThrough():
    # When we're debugging tailgating and forcing trades through, we don't want to have it tailgating a like 100000 Gwei :-D  Tha would be an expensive test
    return GetGasPrice_Fast() * 3
