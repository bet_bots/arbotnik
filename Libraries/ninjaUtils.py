import copy
import traceback

import Exchanges.bancor
import Exchanges.kyber
import Exchanges.radarRelay2
import Exchanges.set
import Exchanges.uniswap
import Libraries.exchanges
import Exchanges.keeperDAO
import Exchanges.ninja
import Exchanges.zrxV2
import Libraries.cache
import Libraries.core
import Libraries.gasStation
import Libraries.relayProxy
from Libraries.accounts import TokenDict_Ninja, CreateAddNewToken_Ninja, DeleteToken_Ninja
import Libraries.defaults
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader
from Libraries.mempool import AreAnyOfTheseAddressesAWatchedVillain
from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings


def PopulateTokenDict_NinjaBetweenBancorAndKyber():
    # Dynamically populate the TokenDict_Ninja based on API calls

    # tokenList_kyber = Exchanges.kyber.API_GetListOfTokenAddresses()
    # # PrintAndLog("tokenList_kyber = " + str(tokenList_kyber))
    # currencies_bancor = Exchanges.bancor.API_GetAllCurrencies()

    tokenList_kyber = Libraries.cache.GetDataFromCache("Exchanges.kyber.API_GetListOfTokenAddresses")
    currencies_bancor = Libraries.cache.GetDataFromCache("Exchanges.bancor.API_GetAllCurrencies")

    # The tokenList_kyber will dictate the order.  So let's alphabetize the address list so we know it's always in a standard order
    tokenList_kyber.sort()
    PrintAndLog("tokenList_kyber after sorting = " + str(tokenList_kyber))

    symbolsToEnable = SetupTokenDict_Ninja_GivenBancorDataAndOtherDXsTokenLists(currencies_bancor, tokenList_kyber)

    # for tokenName in TokenDict_Ninja:
    #     PrintAndLog("TokenDict_Ninja instance " + tokenName + ", erc20TokenContractAddress = " + str(TokenDict_Ninja[tokenName].erc20TokenContractAddress) + ", decimals = " + str(
    #         TokenDict_Ninja[tokenName].decimals))
    #     PrintAndLog("   pathList_BuyingTokens_Bancor = " + str(TokenDict_Ninja[tokenName].pathList_BuyingTokens_Bancor))
    #     PrintAndLog("   pathList_SellingTokens_Bancor = " + str(TokenDict_Ninja[tokenName].pathList_SellingTokens_Bancor))
    #     PrintAndLog("   pathList_BuyingTokens_Kyber = " + str(TokenDict_Ninja[tokenName].pathList_BuyingTokens_Kyber))
    #     PrintAndLog("   pathList_SellingTokens_Kyber = " + str(TokenDict_Ninja[tokenName].pathList_SellingTokens_Kyber))

    PrintAndLog("Kyber has " + str(len(tokenList_kyber)) + " tokens listed")
    PrintAndLog("Of that, " + str(len(TokenDict_Ninja)) + " have matched and are eligible to trade across both")

    tokenNamesToDelete = []
    # Delete all tokens that cannot be traded or approved or have issues.
    # Example, for some reason I get exceptions when trying ot approve trading for OMG via my ninja. Maybe I need to call approve differently?
    # Example, MYB cannot trade for some reason. It's either trade or tradesafe or getarbtirage that has some weird exception i cannot resolve
    tokenNamesToDelete.append("omg")
    tokenNamesToDelete.append("mtl")
    tokenNamesToDelete.append("myb")

    for tokenName in tokenNamesToDelete:
        errorMessage = "Tried to delete " + str(tokenName) + " from the TokenDict_Ninja but we failed.  Maybe this exchange is no longer supporting it."
        try:
            if tokenName in TokenDict_Ninja and tokenName in symbolsToEnable:
                DeleteToken_Ninja(tokenName)
                symbolsToEnable.remove(tokenName)
                PrintAndLog("Deleted " + str(tokenName) + " from the TokenDict_Ninja because we have blacklisted it via tokenNamesToDelete")
            else:
                PrintAndLogError(errorMessage)

        except:
            PrintAndLogError(errorMessage)

    PrintAndLog("After deleting some because of Bancor API limitation, We now have " + str(len(TokenDict_Ninja)) + " markets to arbitrage on")

    PrintAndLog("PopulateTokenDict_NinjaBetweenBancorAndKyber: symbolsToEnable = " + str(symbolsToEnable))
    for tokenName in symbolsToEnable:
        # Enable this token
        TokenDict_Ninja[tokenName].tokenIsEnabled_betweenBancorAndKyber = True


def PopulateTokenDict_NinjaBetweenBancorAnd0x():
    # Dynamically populate the TokenDict_Ninja based on API calls

    # tokenList_radarRelay = Exchanges.radarRelay2.API_GetListOfTokenAddresses()
    # PrintAndLog("tokenList_radarRelay = " + str(tokenList_radarRelay))
    # currencies_bancor = Exchanges.bancor.API_GetAllCurrencies()
    tokenList_radarRelay = Libraries.cache.GetDataFromCache("Exchanges.radarRelay2.API_GetListOfTokenAddresses")
    currencies_bancor = Libraries.cache.GetDataFromCache("Exchanges.bancor.API_GetAllCurrencies")

    # The tokenList_radarRelay will dictate the order.  So let's alphabetize the address list so we know it's always in a standard order
    tokenList_radarRelay.sort()
    PrintAndLog("tokenList_radarRelay after sorting = " + str(tokenList_radarRelay))

    symbolsToEnable = SetupTokenDict_Ninja_GivenBancorDataAndOtherDXsTokenLists(currencies_bancor, tokenList_radarRelay)

    # for tokenName in TokenDict_Ninja:
    # PrintAndLog("TokenDict_Ninja instance " + tokenName + ", erc20TokenContractAddress = " + str(TokenDict_Ninja[tokenName].erc20TokenContractAddress) + ", decimals = " + str(
    #     TokenDict_Ninja[tokenName].decimals))
    # PrintAndLog("   pathList_BuyingTokens_Bancor = " + str(TokenDict_Ninja[tokenName].pathList_BuyingTokens_Bancor))
    # PrintAndLog("   pathList_SellingTokens_Bancor = " + str(TokenDict_Ninja[tokenName].pathList_SellingTokens_Bancor))

    PrintAndLog("RadarRelay has " + str(len(tokenList_radarRelay)) + " tokens listed")
    PrintAndLog("Of that, " + str(len(TokenDict_Ninja)) + " have matched and are eligible to trade across both")

    tokenNamesToDelete = []
    # Delete all tokens that cannot be traded or approved or have issues.
    # Example, for some reason I get exceptions when trying ot approve trading for OMG via my ninja. Maybe I need to call approve differently?
    # Example, MYB cannot trade for some reason. It's either trade or tradesafe or getarbtirage that has some weird exception i cannot resolve
    tokenNamesToDelete.append("omg")
    tokenNamesToDelete.append("mtl")
    tokenNamesToDelete.append("myb")

    for tokenName in tokenNamesToDelete:
        errorMessage = "Tried to delete " + str(tokenName) + " from the TokenDict_Ninja but we failed.  Maybe this exchange is no longer supporting it."
        try:
            if tokenName in TokenDict_Ninja and tokenName in symbolsToEnable:
                DeleteToken_Ninja(tokenName)
                symbolsToEnable.remove(tokenName)
                PrintAndLog("Deleted " + str(tokenName) + " from the TokenDict_Ninja because we have blacklisted it via tokenNamesToDelete")
            else:
                PrintAndLogError(errorMessage)

        except:
            PrintAndLogError(errorMessage)

    PrintAndLog("After deleting some because of Bancor API limitation, We now have " + str(len(TokenDict_Ninja)) + " markets to arbitrage on")

    PrintAndLog("PopulateTokenDict_NinjaBetweenBancorAnd0x: symbolsToEnable = " + str(symbolsToEnable))
    for tokenName in symbolsToEnable:
        # Enable this token
        TokenDict_Ninja[tokenName].tokenIsEnabled_betweenBancorAnd0xv2 = True


def PopulateTokenDict_NinjaBetween0xAndKyber():
    # Dynamically populate the TokenDict_Ninja based on API calls

    tokenList_radarRelay = Libraries.cache.GetDataFromCache("Exchanges.radarRelay2.API_GetListOfTokenAddresses")
    tokenList_kyber = Libraries.cache.GetDataFromCache("Exchanges.kyber.API_GetListOfTokenAddresses")
    # PrintAndLog("tokenList_radarRelay = " + str(tokenList_radarRelay))
    # PrintAndLog("tokenList_kyber = " + str(tokenList_kyber))

    # The tokenList_radarRelay will dictate the order.  So let's alphabetize the address list so we know it's always in a standard order
    tokenList_radarRelay.sort()
    PrintAndLog("tokenList_radarRelay after sorting = " + str(tokenList_radarRelay))

    symbolsToEnable = SetupTokenDict_Ninja_GivenTwoDXTokenLists(tokenList_radarRelay, tokenList_kyber)

    PrintAndLog("RadarRelay has " + str(len(tokenList_radarRelay)) + " tokens listed")
    PrintAndLog("Kyber has " + str(len(tokenList_kyber)) + " tokens listed")
    PrintAndLog("Of that, " + str(len(TokenDict_Ninja)) + " have matched and are eligible to trade across both")

    tokenNamesToDelete = []
    # Delete all tokens that cannot be traded or approved or have issues.
    # Example, for some reason I get exceptions when trying ot approve trading for OMG via my ninja. Maybe I need to call approve differently?
    tokenNamesToDelete.append("omg")

    for tokenName in tokenNamesToDelete:
        errorMessage = "Tried to delete " + str(tokenName) + " from the TokenDict_Ninja but we failed.  Maybe this exchange is no longer supporting it."
        try:
            if tokenName in TokenDict_Ninja and tokenName in symbolsToEnable:
                DeleteToken_Ninja(tokenName)
                symbolsToEnable.remove(tokenName)
                PrintAndLog("Deleted " + str(tokenName) + " from the TokenDict_Ninja because we have blacklisted it via tokenNamesToDelete")
            else:
                PrintAndLogError(errorMessage)

        except:
            PrintAndLogError(errorMessage)

    PrintAndLog("After deleting some because of (????), We now have " + str(len(TokenDict_Ninja)) + " markets to arbitrage on")

    PrintAndLog("PopulateTokenDict_NinjaBetween0xAndKyber: symbolsToEnable = " + str(symbolsToEnable))
    for tokenName in symbolsToEnable:
        # Enable this token
        TokenDict_Ninja[tokenName].tokenIsEnabled_between0xv2AndKyber = True


def PopulateTokenDict_NinjaBetweenUniswapAndKyber():
    # Dynamically populate the TokenDict_Ninja based on API calls
    tokenList_uniswap = Exchanges.uniswap.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance()
    tokenList_kyber = Libraries.cache.GetDataFromCache("Exchanges.kyber.API_GetListOfTokenAddresses")
    PrintAndLog("tokenList_uniswap = " + str(tokenList_uniswap))
    PrintAndLog("tokenList_kyber = " + str(tokenList_kyber))

    # The tokenList_uniswap will dictate the order.  So let's alphabetize the address list so we know it's always in a standard order
    tokenList_uniswap.sort()
    PrintAndLog("tokenList_uniswap after sorting = " + str(tokenList_uniswap))

    symbolsToEnable = SetupTokenDict_Ninja_GivenTwoDXTokenLists(tokenList_uniswap, tokenList_kyber)

    PrintAndLog("Uniswap has " + str(len(tokenList_uniswap)) + " tokens listed")
    PrintAndLog("Kyber has " + str(len(tokenList_kyber)) + " tokens listed")
    PrintAndLog("Of that, " + str(len(TokenDict_Ninja)) + " have matched and are eligible to trade across both")

    tokenNamesToDelete = []
    # Delete all tokens that cannot be traded or approved or have issues.
    # Example, for some reason I get exceptions when trying ot approve trading for OMG via my ninja. Maybe I need to call approve differently?
    tokenNamesToDelete.append("omg")
    tokenNamesToDelete.append("pay")
    tokenNamesToDelete.append("mtl")
    tokenNamesToDelete.append("usdt")
    tokenNamesToDelete.append("dgx")  # Reverts when trades, not sure why

    for tokenName in tokenNamesToDelete:
        errorMessage = "Tried to delete " + str(tokenName) + " from the TokenDict_Ninja but we failed.  Maybe this exchange is no longer supporting it."
        try:
            if tokenName in TokenDict_Ninja and tokenName in symbolsToEnable:
                DeleteToken_Ninja(tokenName)
                symbolsToEnable.remove(tokenName)
                PrintAndLog("Deleted " + str(tokenName) + " from the TokenDict_Ninja because we have blacklisted it via tokenNamesToDelete")
            else:
                PrintAndLogError(errorMessage)

        except:
            PrintAndLogError(errorMessage)

    PrintAndLog("After deleting some because of (????), We now have " + str(len(TokenDict_Ninja)) + " markets to arbitrage on")

    PrintAndLog("PopulateTokenDict_NinjaBetweenUniswapAndKyber: symbolsToEnable = " + str(symbolsToEnable))
    for tokenName in symbolsToEnable:
        # Enable this token
        TokenDict_Ninja[tokenName].tokenIsEnabled_betweenUniswapAndKyber = True


def PopulateTokenDict_NinjaBetweenUniswapAnd0x():
    # Dynamically populate the TokenDict_Ninja based on API calls
    tokenList_uniswap = Exchanges.uniswap.GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance()
    tokenList_radarRelay = Libraries.cache.GetDataFromCache("Exchanges.radarRelay2.API_GetListOfTokenAddresses")
    PrintAndLog("tokenList_uniswap = " + str(tokenList_uniswap))
    PrintAndLog("tokenList_radarRelay = " + str(tokenList_radarRelay))

    # The tokenList_uniswap will dictate the order.  So let's alphabetize the address list so we know it's always in a standard order
    tokenList_uniswap.sort()
    PrintAndLog("tokenList_uniswap after sorting = " + str(tokenList_uniswap))

    symbolsToEnable = SetupTokenDict_Ninja_GivenTwoDXTokenLists(tokenList_uniswap, tokenList_radarRelay)

    PrintAndLog("Uniswap has " + str(len(tokenList_uniswap)) + " tokens listed")
    PrintAndLog("0x has " + str(len(tokenList_radarRelay)) + " tokens listed")
    PrintAndLog("Of that, " + str(len(TokenDict_Ninja)) + " have matched and are eligible to trade across both")

    tokenNamesToDelete = []
    # Delete all tokens that cannot be traded or approved or have issues.
    # Example, for some reason I get exceptions when trying ot approve trading for OMG via my ninja. Maybe I need to call approve differently?
    tokenNamesToDelete.append("omg")
    tokenNamesToDelete.append("pay")
    tokenNamesToDelete.append("mtl")

    for tokenName in tokenNamesToDelete:
        errorMessage = "Tried to delete " + str(tokenName) + " from the TokenDict_Ninja but we failed.  Maybe this exchange is no longer supporting it."
        try:
            if tokenName in TokenDict_Ninja and tokenName in symbolsToEnable:
                DeleteToken_Ninja(tokenName)
                symbolsToEnable.remove(tokenName)
                PrintAndLog("Deleted " + str(tokenName) + " from the TokenDict_Ninja because we have blacklisted it via tokenNamesToDelete")
            else:
                PrintAndLogError(errorMessage)

        except:
            PrintAndLogError(errorMessage)

    PrintAndLog("After deleting some because of (????), We now have " + str(len(TokenDict_Ninja)) + " markets to arbitrage on")

    PrintAndLog("PopulateTokenDict_NinjaBetweenUniswapAnd0x: symbolsToEnable = " + str(symbolsToEnable))
    for tokenName in symbolsToEnable:
        # Enable this token
        TokenDict_Ninja[tokenName].tokenIsEnabled_betweenUniswapAnd0x = True


# I'm updating this after Bancor changed their data structure and broke stuff as of January 16 2019
def SetupTokenDict_Ninja_GivenBancorDataAndOtherDXsTokenLists(currencies_bancor, tokenList_otherDX):
    # PrintAndLog("SetupTokenDict_Ninja_GivenBancorDataAndOtherDXsTokenLists: currencies_bancor = " + str(currencies_bancor))

    # Return a list of symbols that overlapped between these two lists
    symbolsToEnable = []

    for tokenAddress in tokenList_otherDX:
        for currency in currencies_bancor:
            try:
                for detail in currency['details']:
                    # PrintAndLog("bancor detail = " + str(detail))

                    # Determine if this is valid data we want
                    if detail['blockchain']['chainId'].lower() == "mainnet" and detail['blockchain']['type'].lower() == "ethereum" and 'reserves' in detail:
                        for reserveId in detail['reserves']:
                            reserve = detail['reserves'][reserveId]
                            # PrintAndLog("reserve = " + str(reserve))
                            if reserve['blockchainId'].lower() == tokenAddress.lower():
                                # PrintAndLog("Matched the tokenAddress within a reserve: tokenAddress = " + str(tokenAddress) + ", reserve = " + str(reserve))
                                # Extract the relayerTokenAddress for this match
                                for subReserveId in detail['reserves']:
                                    subReserve = detail['reserves'][subReserveId]
                                    if subReserve['currencyCode'].lower() == "bnt":
                                        relayerTokenAddress = detail['blockchainId']
                                        tokenConverterContract = detail['converter']['blockchainId']
                                        symbol = reserve['currencyCode'].lower()

                                        # Skip the bnt token because it's bancor's converter thingy and it isn't necessary here
                                        if symbol == "bnt":
                                            continue

                                        PrintAndLog("Found Bancor's relayerTokenAddress for " + str(symbol).upper() + ", relayerTokenAddress = " + str(
                                            relayerTokenAddress) + " matches with tokenAddress = " + str(tokenAddress))

                                        pathList_BuyingTokens_Bancor = [
                                            Exchanges.bancor.BancorWrappedETH,
                                            Exchanges.bancor.BancorTokenContractAddress,
                                            Exchanges.bancor.BancorTokenContractAddress,
                                            relayerTokenAddress,
                                            tokenAddress,
                                        ]

                                        pathList_SellingTokens_Bancor = [
                                            tokenAddress,
                                            relayerTokenAddress,
                                            Exchanges.bancor.BancorTokenContractAddress,
                                            Exchanges.bancor.BancorTokenContractAddress,
                                            Exchanges.bancor.BancorWrappedETH,
                                        ]

                                        # PrintAndLog("pathList_BuyingTokens_Bancor = " + str(pathList_BuyingTokens_Bancor))
                                        # PrintAndLog("pathList_SellingTokens_Bancor = " + str(pathList_SellingTokens_Bancor))

                                        # If this token has not yet been registered with the TokenDict_Ninja
                                        if symbol not in TokenDict_Ninja:
                                            PrintAndLog("Creating a new Token_Ninja object for " + str(symbol))
                                            CreateAddNewToken_Ninja(symbol, tokenAddress)

                                        # If the address does not match with what's already in there
                                        elif TokenDict_Ninja[symbol].erc20TokenContractAddress.lower() != tokenAddress.lower():
                                            message = "We have an address conflict for " + str(symbol) + ", tried setting tokenAddress to " + str(
                                                tokenAddress) + " but erc20TokenContractAddress was found to be " + str(TokenDict_Ninja[symbol].erc20TokenContractAddress)
                                            Libraries.core.API_PostOperatorNotification(message)
                                            PrintAndLogError(message)
                                            raise Exception(message)

                                        # The address matches with what's already in there, so it's valid
                                        else:
                                            PrintAndLog("Not creating a new Token_Ninja object for " + str(symbol) + ", it already exists")

                                        # Set properties the ninja will need
                                        TokenDict_Ninja[symbol].SetPathList_BuyingTokens_Bancor(pathList_BuyingTokens_Bancor)
                                        TokenDict_Ninja[symbol].SetPathList_SellingTokens_Bancor(pathList_SellingTokens_Bancor)
                                        TokenDict_Ninja[symbol].bancorTokenConverterContract = tokenConverterContract
                                        TokenDict_Ninja[symbol].bancorRelayerTokenAddress = relayerTokenAddress

                                        symbolsToEnable.append(symbol)

            except:
                PrintAndLogError("exception: " + traceback.format_exc())
                pass

    return symbolsToEnable


def SetupTokenDict_Ninja_GivenTwoDXTokenLists(tokenList_A, tokenList_B):
    # Return a list of symbols that overlapped between these two lists
    symbolsToEnable = []

    for token_B in tokenList_B:
        for token_A in tokenList_A:
            try:
                if token_B.lower() == token_A.lower():
                    symbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token_B).lower()
                    # PrintAndLog("Found match: " + str(symbol) + ", " + str(token_B))

                    # If this token has not yet been registered with the TokenDict_Ninja
                    if symbol not in TokenDict_Ninja:
                        PrintAndLog("Creating a new Token_Ninja object for " + str(symbol))
                        CreateAddNewToken_Ninja(symbol, token_B)

                    # If the address does not match with what's already in there
                    elif TokenDict_Ninja[symbol].erc20TokenContractAddress.lower() != token_B.lower():
                        message = "We have an address conflict for " + str(symbol) + ", tried setting token_B to " + str(
                            token_B) + " but erc20TokenContractAddress was found to be " + str(TokenDict_Ninja[symbol].erc20TokenContractAddress)
                        Libraries.core.API_PostOperatorNotification(message)
                        PrintAndLogError(message)

                    # The address matches with what's already in there, so it's valid
                    else:
                        PrintAndLog("Not creating a new Token_Ninja object for " + str(symbol) + ", it already exists")

                    symbolsToEnable.append(symbol)

            except:
                PrintAndLogError("exception: " + traceback.format_exc())

    return symbolsToEnable


def PopulateTokenDict_DaiAndSai_BugFix():
    # I have to init TokenDict_Ninja this way because if I call the smart contract to get the symbol,
    # the old SAI still thinks it's called DAI since the smart contract cannot change.
    # And I want to be able to trade both...
    # In the future, I could update TokenDict_Ninja to be keyed by the smart contract address but that's a bit inconvenient to do at the moment
    myList = []
    myList.append(('dai'.lower(), '0x6b175474e89094c44da98b954eedeac495271d0f'.lower()))  # Multi collateral DAI
    myList.append(('sai'.lower(), '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359'.lower()))  # Single collateral DAI
    for item in myList:
        symbol = item[0]
        tokenAddress = item[1]
        PrintAndLog("PopulateTokenDict_DaiAndSai_BugFix: symbol = " + str(symbol) + ", tokenAddress = " + str(tokenAddress))
        if symbol not in TokenDict_Ninja:
            CreateAddNewToken_Ninja(symbol, tokenAddress)
        else:
            pass
            # Do nothing it's already there


def PopulateTokenDict_Ninja_Generic():
    from Libraries.exchanges import ExchangeDict, SetExchangeTokensDictWithValueList
    from Libraries.customDicts import DictWithValueList

    # Iterate over all exchanges
    # Get each of their tradable token lists
    # Join the lists into one single list of tokens that are common among at least two exchanges.
    # So remove any tokens only found on one single exchange
    # Then execute the SetupTokenDict_Ninja_GivenTwoDXTokenLists logic but with only one list

    # Also keep track of all tokens on each exchange
    allExchangeTokensDict = DictWithValueList()

    total_tradeableTokensList = []
    for exchangeName in ExchangeDict:
        exchange = ExchangeDict[exchangeName]
        try:
            # Copy this list so we don't modify the original
            # tradeableTokensList = copy.deepcopy(exchange.GetTradableTokens())
            tradeableTokensList = exchange.GetTradableTokens().copy()

            if Libraries.defaults.UseHardCodedTradableTokensList:
                # Define a hard coded list for debug
                tradeableTokensList = Exchanges.keeperDAO.GetBorrowableQuoteTokensList(False, False)
                # tradeableTokensList = [
                #     "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
                #     "0x3e9bc21c9b189c09df3ef1b824798658d5011937",
                #     # "0x8762db106b2c2a0bccb3a80d1ed41273552616e8"
                #     # "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48",
                #     # "0x6b175474e89094c44da98b954eedeac495271d0f",
                #     # "0xeb4c2781e4eba804ce9a9803c67d0893436bb27d",
                #     # "0xdac17f958d2ee523a2206206994597c13d831ec7",
                #     # "0x1f9840a85d5af5bf1d1762f925bdaddc4201f984",
                #     # "0xc00e94cb662c3520282e6f5717214004a7f26888",
                #     # "0x0bc529c00c6401aef6d220be8c6ea1667f6ad93e",
                #     # "0xD533a949740bb3306d119CC777fa900bA034cd52",
                #     "0xdac17f958d2ee523a2206206994597c13d831ec7",
                # ]
            PrintAndLog_FuncNameHeader(exchangeName + " has " + str(len(tradeableTokensList)) + " tradeableTokens, tradeableTokensList = " + str(tradeableTokensList))
            # Make sure everything is lowercase so we can compare
            tradeableTokensList = ConvertListOfStringsToLowercaseListOfStrings(tradeableTokensList)
            # Don't allow duplicates from within a single exchange
            tradeableTokensList = Libraries.core.RemoveDuplicatesFromList(tradeableTokensList)
            # Combine the lists and allow duplicates in the total list since duplicates are how we will tell if a token is on more than one exchanges
            total_tradeableTokensList += tradeableTokensList

            # Maintain the allExchangeTokensDict
            allExchangeTokensDict.SetValueList(exchangeName, tradeableTokensList)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when checking " + str(exchangeName) + "'s tokens " + traceback.format_exc())

    # Remove banned tokens from total_tradeableTokensList
    PrintAndLog_FuncNameHeader("total_tradeableTokensList len before ban = " + str(len(total_tradeableTokensList)))
    for token in copy.deepcopy(total_tradeableTokensList):
        if token.lower() in Libraries.defaults.Ninja_BannedTokens:
            total_tradeableTokensList.remove(token.lower())

    PrintAndLog_FuncNameHeader("total_tradeableTokensList len after ban = " + str(len(total_tradeableTokensList)))

    # total_tradeableTokensList
    tokensOnMoreThanOneExchangeList = []
    uniqueTokens_tradeableTokensList = Libraries.core.RemoveDuplicatesFromList(total_tradeableTokensList)
    for token in uniqueTokens_tradeableTokensList:
        # Count occurrences of each unique token in the total_tradeableTokensList
        occurrences = total_tradeableTokensList.count(token)
        PrintAndLog_FuncNameHeader("Found token " + str(token) + " on " + str(occurrences) + " exchanges")
        PrintAndLog_FuncNameHeader("Found token " + str(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token)) + " " + str(
            token) + " on " + str(occurrences) + " exchanges")
        # If it occurs more than once, that means it's on more than one exchange
        if occurrences > 1:
            PrintAndLog_FuncNameHeader("Adding to tokensOnMoreThanOneExchangeList")
            tokensOnMoreThanOneExchangeList.append(token)
        else:
            PrintAndLog_FuncNameHeader("NOT adding to tokensOnMoreThanOneExchangeList")

    PrintAndLog_FuncNameHeader("tokensOnMoreThanOneExchangeList of len " + str(len(tokensOnMoreThanOneExchangeList)) + " = " + str(
        tokensOnMoreThanOneExchangeList))

    # Create a TokenDict_Ninja object for each token in tokensOnMoreThanOneExchangeList
    for token in tokensOnMoreThanOneExchangeList:
        try:
            symbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token).lower()
            PrintAndLog_FuncNameHeader("Found match: " + str(symbol) + ", " + str(token))

            # If this token has not yet been registered with the TokenDict_Ninja
            if symbol not in TokenDict_Ninja:
                PrintAndLog_FuncNameHeader("Creating a new Token_Ninja object for " + str(symbol))
                CreateAddNewToken_Ninja(symbol, token)

            # If the address does not match with what's already in there
            elif TokenDict_Ninja[symbol].erc20TokenContractAddress.lower() != token.lower():
                message = "We have an address conflict for " + str(symbol) + ", tried setting token to " + str(
                    token) + " but erc20TokenContractAddress was found to be " + str(TokenDict_Ninja[symbol].erc20TokenContractAddress)
                Libraries.core.API_PostOperatorNotification(message)
                PrintAndLogError(message)

            # The address matches with what's already in there, so it's valid
            else:
                PrintAndLog_FuncNameHeader("Not creating a new Token_Ninja object for " + str(symbol) + ", it already exists")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception " + traceback.format_exc())

    # Further maintain allExchangeTokensDict by iterating over each token and remove it if it's not in tokensOnMoreThanOneExchangeList
    for exchangeName in allExchangeTokensDict.GetKeys():
        copy_exchangesTokenList = allExchangeTokensDict.GetItems(exchangeName)

        # Update allExchangeTokensDict based on tokens that are on more than one exchange as well as on this specific exchange
        tradeableTokensList_forThisExchangeOnly = Libraries.utils.FindIntersectingValuesInLists(tokensOnMoreThanOneExchangeList, copy_exchangesTokenList)
        PrintAndLog_FuncNameHeader(exchangeName + " has " + str(len(copy_exchangesTokenList)) + " tokens listed on the exchange and " + str(
            len(tradeableTokensList_forThisExchangeOnly)) + " of them are tradable by Ninja")
        # Update allExchangeTokensDict accordingly
        allExchangeTokensDict.SetValueList(exchangeName, tradeableTokensList_forThisExchangeOnly)

    message_subString = ''
    for exchangeName in allExchangeTokensDict.GetKeys():
        copy_exchangesTokenList = allExchangeTokensDict.GetItems(exchangeName)
        PrintAndLog_FuncNameHeader("Calling SetExchangeTokensDictWithValueList with " + str(
            exchangeName) + "'s contents of copy_exchangesTokenList of len " + str(len(copy_exchangesTokenList)) + " = " + str(copy_exchangesTokenList))
        message_subString += exchangeName + '-' + str(len(copy_exchangesTokenList)) + ', '

    message = "Ninjaing across " + str(len(allExchangeTokensDict.GetKeys())) + " exchanges and " + str(
        len(tokensOnMoreThanOneExchangeList)) + " tokens. Tokens per exchange are: " + str(message_subString)
    PrintAndLog_FuncNameHeader(message)
    Libraries.core.API_PostOperatorNotification(message)

    PrintAndLog_FuncNameHeader("allExchangeTokensDict = " + str(allExchangeTokensDict))
    SetExchangeTokensDictWithValueList(allExchangeTokensDict)


def PopulateTokenDict_Ninja_Set():
    # Build a uniqueTokenAddressList from the SetTokenDataDict
    uniqueTokenAddressList = []
    for setKey in Exchanges.set.SetTokenDataDict:
        for component in Exchanges.set.SetTokenDataDict[setKey].tokenComponents:
            if component['address'].lower() not in uniqueTokenAddressList:
                uniqueTokenAddressList.append(component['address'].lower())

    # For some reason i'm getting an error when calling API_GetERC20Symbol_Batched when TimeMachine_EnabledForHistoricBlockDebug is set to true
    # So when i'm debugging with a hard coded block number, i have to hard code this symbolDict as well...
    # otherwise just always make the call for the symbols
    if Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        symbolDict = {
            '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2': 'weth',
            '0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359': 'dai',
            '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599': 'wbtc',
            '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48': 'usdc',
            '0x39aa39c021dfbae8fac545936693ac917d5e7563': 'cusdc',
            '0x514910771af9ca656af840dff83e8264ecf986ca': 'link',
        }
        Libraries.core.API_PostOperatorNotification("REMOVEME when done debugging. Hard coded symbolDict")
    else:
        symbolDict = Libraries.core.API_GetERC20Symbol_Batched(uniqueTokenAddressList)

    PrintAndLog_FuncNameHeader("uniqueTokenAddressList = " + str(uniqueTokenAddressList))
    PrintAndLog_FuncNameHeader("symbolDict = " + str(symbolDict))
    # Populate TokenDict_Ninja for each tokenAddress in uniqueTokenAddressList if it's not already in TokenDict_Ninja
    for tokenAddress in uniqueTokenAddressList:
        symbol = symbolDict[tokenAddress].lower()
        # PrintAndLog_FuncNameHeader("Creating entry in TokenDict_Ninja with symbol = " + str(symbol) + ", tokenAddress = " + str(tokenAddress))
        if symbol not in TokenDict_Ninja:
            CreateAddNewToken_Ninja(symbol, tokenAddress)
        else:
            pass
            # Do nothing it's already there


def PopulateTokenDict_Consider_Ninja_DeleteAllTokensExceptForThoseInThisList():
    # For debug purposes, I can delete all tokens except for one so that my logs are cleaner when debugging
    if Libraries.defaults.Ninja_DeleteAllTokensExceptForThoseInThisList:
        tokensToKeep = ConvertListOfStringsToLowercaseListOfStrings(Libraries.defaults.Ninja_DeleteAllTokensExceptForThoseInThisList)
        PrintAndLog_FuncNameHeader("TokenDict_Ninja before = " + str(TokenDict_Ninja))
        for tokenName in TokenDict_Ninja.copy():
            if tokenName.lower() not in tokensToKeep:
                DeleteToken_Ninja(tokenName)

        PrintAndLog_FuncNameHeader("TokenDict_Ninja after = " + str(TokenDict_Ninja))


def Approves_Generic():
    from Libraries.accounts import NinjaOpAccountDict
    from Libraries.exchanges import ExchangeDict, GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken
    from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings

    ninjaName = "ninja-op-100"
    publicAddress_Ninja = NinjaOpAccountDict[ninjaName].publicAddress
    privateKey_Ninja = NinjaOpAccountDict[ninjaName].DecryptPK()
    gas = Libraries.gasStation.GetGasLimit_Other()
    # gasPrice = Libraries.gasStation.ConvertGweiToWei(68)
    gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    # gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    nonceToUse = Libraries.core.API_GetTransactionCount(publicAddress_Ninja)

    # TODO, enable quoteTokens somewhere that makes sense
    quoteTokenList = []
    quoteTokenList.append(Exchanges.keeperDAO.EthTokenContract.lower())

    for quoteToken in quoteTokenList:
        for exchangeName in ExchangeDict:
            # Make a tokensToApproveList containing all tokens we're trading
            tokensToApproveList = []

            # Make sure the baseTokens are approved
            baseTokenList = GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken(exchangeName, quoteToken)
            baseTokenList = ConvertListOfStringsToLowercaseListOfStrings(baseTokenList)
            tokensToApproveList += baseTokenList

            # Make sure the quoteTokens are approved as well
            for sub_quoteToken in ConvertListOfStringsToLowercaseListOfStrings(copy.deepcopy(quoteTokenList)):
                # Exclude 0xEEEE...EEEE since it's not a real token
                if sub_quoteToken not in tokensToApproveList and sub_quoteToken.lower() != Exchanges.keeperDAO.EthTokenContract.lower():
                    tokensToApproveList.append(sub_quoteToken)

            # Add WETH if it's not already in there
            if Exchanges.zrxV2.Contract_WETH.lower() not in tokensToApproveList:
                tokensToApproveList.append(Exchanges.zrxV2.Contract_WETH.lower())

            PrintAndLog_FuncNameHeader("tokensToApproveList for " + str(exchangeName) + " of len " + str(len(tokensToApproveList)) + " = " + str(tokensToApproveList))

            # TODO, these banned tokens should not be tradable yet somehow they made their way into this list...
            #  Fix the issue they should not be in the baseTokenList at all
            #  For now i'll just manually remove them
            for bannedToken in Libraries.defaults.Ninja_BannedTokens:
                if bannedToken in tokensToApproveList:
                    tokensToApproveList.remove(bannedToken)

            for tokenToApprove in tokensToApproveList:
                # Determine spenderContractToApprove based on exchangeName
                spenderContractToApproveList = ExchangeDict[exchangeName].GetTokenSpenderProxys(tokenToApprove)
                PrintAndLog_FuncNameHeader("exchangeName " + str(exchangeName) + " and tokenToApprove " + str(tokenToApprove) + " have " + str(
                    len(spenderContractToApproveList)) + " spender proxy contracts we need to approve, spenderContractToApproveList = " + str(
                    spenderContractToApproveList))

                for spenderContractToApprove in spenderContractToApproveList:
                    PrintAndLog("Approve with nonce " + str(nonceToUse) + " spenderContractToApprove " + str(
                        spenderContractToApprove) + " to move our token " + str(tokenToApprove))

                    transactionId = None
                    try:
                        # Only fire off the approve if our debug flag is set
                        transactionId = Exchanges.ninja.NinjaInstance_Generic.ApproveContractForTokenTransfer(
                            publicAddress_Ninja, privateKey_Ninja, tokenToApprove,
                            spenderContractToApprove, nonceToUse, False, Libraries.defaults.IssueApprovesOnStartup_DoExecute, gas, gasPrice)

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        PrintAndLogError("exception when trying to approve token " + str(tokenToApprove) + " for spender " + str(
                            spenderContractToApprove) + " on " + str(exchangeName) + ", " + traceback.format_exc())

                    if transactionId or not Libraries.defaults.IssueApprovesOnStartup_DoExecute:
                        nonceToUse += 1


def TraceTrade(txHash, tokenAddressesToTraceList, logContents_Blocks=None, logContents_Mempool=None):
    from Libraries.exchanges import ExchangeDict

    # Trace the trade's history and what lead to this trade being available
    # Find addresses of interest in event logs

    try:
        txReceipt = Libraries.core.API_GetTransactionReceipt(txHash)
        txInfo = Libraries.core.API_GetTransactionInfo(txHash)

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLog("Cannot trace origin of txHash, it must have poofed. txHash = " + str(txHash))
        PrintAndLogError("exception when trying to get txInfo and txReceipt: " + traceback.format_exc())
        return

    # eventLogs = Libraries.core.GetEventLogsFromTxReceipt(txReceipt)
    blockNumber = Libraries.core.GetBlockNumberFromTxReceipt(txReceipt)

    # PrintAndLog("blockNumber = " + str(blockNumber))
    # PrintAndLog("eventLogs = " + str(eventLogs))

    # Find all relevant transactions and store their data in a dictionary keyed by the blockNumber_txIndexInBlock as a string
    # We're storing them in a dict so we can sort them by key later and print them out in order that they mined into the blockchain
    txDict = {}

    # Blocks to scan before the txHash was mined in
    previousBlocksToScan = 3
    blockNumberList = range(blockNumber - previousBlocksToScan, blockNumber + 1)
    # PrintAndLog("blockNumberList = " + str(blockNumberList))
    blockDataList = Libraries.core.API_GetBlock_Batched_Safe(blockNumberList, True, True)
    # PrintAndLog("blockDataList = " + str(blockDataList))
    PrintAndLog("Tracing origin of txHash " + str(txHash))

    # Add this tx to the txDict
    txDict[GetTxDictKey(txReceipt)] = txHash, txInfo, txReceipt

    for blockData in blockDataList:
        local_blockNumber = Libraries.core.ConvertHexToInt(blockData['number'])

        transactionDataList = blockData['transactions']
        PrintAndLog("Block number " + str(local_blockNumber) + " has " + str(len(transactionDataList)) + " transactions")
        txHashList = []
        for transactionData in transactionDataList:
            txHashList.append(transactionData['hash'])

        responseList_txReceipt = Libraries.core.API_GetTransactionReceipt_Batched_Safe(txHashList)
        responseList_txInfo = Libraries.core.API_GetTransactionInfo_Batched_Safe(txHashList)
        # PrintAndLog("responseList_txReceipt = " + str(responseList_txReceipt))
        for index, local_txReceipt in enumerate(responseList_txReceipt):
            local_txHash = txHashList[index]
            local_txInfo = responseList_txInfo[index]
            # PrintAndLog("Found local_txReceipt for " + str(local_txHash))
            local_eventLogs = Libraries.core.GetEventLogsFromTxReceipt(local_txReceipt)
            eventLogsDoContainAllTargetAddresses = Libraries.utils.DoesStringContainAllSubStrings(str(local_eventLogs).lower(), tokenAddressesToTraceList)
            # PrintAndLog("eventLogsDoContainAllTargetAddresses = " + str(eventLogsDoContainAllTargetAddresses))
            if eventLogsDoContainAllTargetAddresses:
                # PrintAndLog("local_txReceipt that contains a match = " + str(local_txReceipt))

                didPrintFoundExchange = False
                for exchangeName in ExchangeDict:
                    try:
                        exchange = ExchangeDict[exchangeName]
                        if exchange.DoEventLogsContainInteractionWithThisExchange(local_eventLogs):
                            if not didPrintFoundExchange:
                                PrintAndLog("   Found exchange in event logs of txHash " + str(local_txHash))
                                # Add this tx to the txDict
                                txDict[GetTxDictKey(local_txReceipt)] = local_txHash, local_txInfo, local_txReceipt
                                didPrintFoundExchange = True

                            PrintAndLog("      " + str(exchangeName))
                            exchange.GetInfoAtBlockNumber(local_blockNumber, tokenAddressesToTraceList, local_txReceipt, responseList_txReceipt, "         ")

                    except (KeyboardInterrupt, SystemExit):
                        print('\nkeyboard interrupt caught')
                        print('\n...Program Stopped Manually!')
                        raise

                    except:
                        PrintAndLogError("exception, " + traceback.format_exc())

    # Sort all the keys in txDict and print the details of the txs in order of when they happened
    keysList = list(txDict.keys())
    # keysList.sort()
    keysList.sort(reverse=False)
    # PrintAndLog_FuncNameHeader("keysList = " + str(keysList))
    for key in keysList:
        PrintAndLog_FuncNameHeader("Found key in keysList " + str(key))
        txHash = txDict[key][0]
        txInfo = txDict[key][1]
        txReceipt = txDict[key][2]
        PrintTxTracingDetails('   ', txInfo, txReceipt, logContents_Blocks, logContents_Mempool)
        # webbrowser.open_new('https://etherscan.io/tx/' + str(txHash))


def PrintTxTracingDetails(stringToPrepend, txInfo, txReceipt, logContents_Blocks=None, logContents_Mempool=None):
    from Contracts.contracts import Contract_Ninja_DiamondProxy

    txHash = Libraries.core.GetTxHashFromTxInfo(txInfo)
    PrintAndLog(stringToPrepend + "txHash = " + str(txHash))
    fromAddress = Libraries.core.GetFromAddressFromTxInfo(txInfo)
    toAddress = Libraries.core.GetToAddressFromTxInfo(txInfo)
    isVillain = AreAnyOfTheseAddressesAWatchedVillain([fromAddress, toAddress])

    villainString = ''
    if isVillain:
        villainString += "!!!!!!!!!!!!! VILLAIN !!!!!!!!!!!!!!!!!! VILLAIN !!!!!!!!!!!!!!!! VILLAIN !!!!!!!!!!!!!!!!!"
    ninjaString = ''
    if toAddress.lower() == Contract_Ninja_DiamondProxy.address.lower() or toAddress.lower() == Libraries.relayProxy.RelayerProxyContract.lower():
        ninjaString += "!!!!!!!!!!!!! NINJA !!!!!!!!!!!!!!!!!! NINJA !!!!!!!!!!!!!!!! NINJA !!!!!!!!!!!!!!!!!"

    blockNumber = Libraries.core.GetBlockNumberFromTxReceipt(txReceipt)

    # Get first occurrence of the block number in logContents_Blocks
    dateTimeOfBlocksFirstDiscovery_string = None
    if logContents_Blocks:
        for line in logContents_Blocks:
            # PrintAndLog(stringToPrepend + "line = " + str(line))
            if str(blockNumber).lower() in line.lower():
                dateTimeOfBlocksFirstDiscovery_string = Libraries.loggingConfig.GetDateTimeStringFromLogFileLine(line)
                break

    # Get first occurrence of txHash in the logContents_Mempool
    dateTimeOfTxHashMempoolFirstDiscovery_string = None
    if logContents_Mempool:
        for line in logContents_Mempool:
            # PrintAndLog(stringToPrepend + "line = " + str(line))
            if txHash.lower() in line.lower():
                dateTimeOfTxHashMempoolFirstDiscovery_string = Libraries.loggingConfig.GetDateTimeStringFromLogFileLine(line)
                break

    PrintAndLog(stringToPrepend + "from = " + str(fromAddress))
    PrintAndLog(stringToPrepend + "to = " + str(toAddress) + " " + str(ninjaString))
    PrintAndLog(stringToPrepend + "isVillain = " + str(isVillain) + " " + str(villainString))
    PrintAndLog(stringToPrepend + "blockNumber = " + str(blockNumber))
    PrintAndLog(stringToPrepend + "dateTimeOfBlocksFirstDiscovery_string = " + str(dateTimeOfBlocksFirstDiscovery_string))
    PrintAndLog(stringToPrepend + "dateTimeOfTxHashMempoolFirstDiscovery_string = " + str(dateTimeOfTxHashMempoolFirstDiscovery_string))
    PrintAndLog(stringToPrepend + "gasPrice = " + str(Libraries.core.ConvertWeiToGwei(Libraries.core.ConvertHexToInt(txInfo['gasPrice']))) + " gwei")
    PrintAndLog(stringToPrepend + "gas used = " + str(Libraries.core.ConvertHexToInt(txReceipt['gasUsed'])))
    PrintAndLog(stringToPrepend + "gas limit = " + str(Libraries.core.ConvertHexToInt(txInfo['gas'])))
    PrintAndLog(stringToPrepend + "index in block = " + str(Libraries.core.GetTxIndexInBlockFromTxReceipt(txReceipt)))
    # PrintAndLog(stringToPrepend + "txReceipt = " + str(txReceipt))
    # PrintAndLog(stringToPrepend + "txInfo = " + str(txInfo))


def GetTxDictKey(txReceipt):
    blockNumber = Libraries.core.GetBlockNumberFromTxReceipt(txReceipt)
    txIndexInBlock = Libraries.core.GetTxIndexInBlockFromTxReceipt(txReceipt)
    txIndexInBlock_string = str(txIndexInBlock)
    len_txIndexInBlock_string = 6
    zerosToPrepend = len_txIndexInBlock_string - len(txIndexInBlock_string)
    txIndexInBlock_string = '0' * zerosToPrepend + txIndexInBlock_string
    # txIndexInBlock_string_forSorting = str(int(999999 - float(txIndexInBlock_string)))
    # return str(blockNumber) + "_" + str(txIndexInBlock_string_forSorting) + "_" + str(txIndexInBlock_string)
    return str(blockNumber) + "_" + str(txIndexInBlock_string)


def SetMemoryCacheFromFileCache_AllExchanges():
    for exchangeName in Libraries.exchanges.ExchangeDict:
        Libraries.exchanges.ExchangeDict[exchangeName].SetMemoryCacheFromFileCache()


def RestrictMemoryCacheTokensBasedOn_TokenDict_Ninja():
    from Libraries.accounts import TokenDict_Ninja

    tokensList = []
    for key in TokenDict_Ninja:
        token = TokenDict_Ninja[key].erc20TokenContractAddress
        if token.lower() not in tokensList:
            tokensList.append(token.lower())

    for exchangeName in Libraries.exchanges.ExchangeDict:
        Libraries.exchanges.ExchangeDict[exchangeName].RestrictMemoryCacheToOnlyTheseTokens(
            Exchanges.keeperDAO.GetBorrowableAssetsList(False), tokensList)
