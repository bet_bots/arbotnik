import Libraries.core
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader
from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings
from Libraries.exchanges import GetCopyOfExchangeTokensDictWithValueList, EnforceTokenAddressIsERC20Version


class Token_Ninja:
    tokenName = None
    # Mainnet token contract address
    erc20TokenContractAddress = None
    decimals = None

    # OrderAggregator = None
    # Keyed by exchangeName
    # The value is another dict keyed by the quoteToken
    OrderAggregatorDict = None

    # Trading path for Bancor
    pathList_BuyingTokens_Bancor = None
    pathList_SellingTokens_Bancor = None

    # Trading path for Kyber
    pathList_BuyingTokens_Kyber = None
    pathList_SellingTokens_Kyber = None

    tokenIsEnabled_betweenBancorAndKyber = None
    tokenIsEnabled_betweenBancorAnd0xv2 = None
    tokenIsEnabled_between0xv2AndKyber = None
    tokenIsEnabled_betweenUniswapAndKyber = None
    tokenIsEnabled_betweenUniswapAnd0x = None

    # Specific to Bancor exchange
    bancorTokenConverterContract = None
    bancorRelayerTokenAddress = None

    def __init__(self, _tokenName, _erc20TokenContractAddress):
        PrintAndLog_FuncNameHeader("Token_Ninja constructor = " + str(_tokenName) + ", " + str(_erc20TokenContractAddress))
        from Libraries.core import GetDecimalsForTokenContract
        import Exchanges.kyber

        self.tokenName = _tokenName
        self.erc20TokenContractAddress = _erc20TokenContractAddress

        # convert decimals to it's proper form. I should probably rename it too...
        # Use E notation to make the big number from the _decimals variable we get
        # Make sure that the decimals are added to Libraries.core.GetDecimalsForTokenContract
        self.decimals = float("1e" + str(GetDecimalsForTokenContract(_erc20TokenContractAddress)))

        self.pathList_BuyingTokens_Bancor = []
        self.pathList_SellingTokens_Bancor = []
        self.pathList_BuyingTokens_Kyber = []
        self.pathList_SellingTokens_Kyber = []

        self.SetPathList_BuyingTokens_Kyber([Exchanges.kyber.EtherToken, _erc20TokenContractAddress])
        self.SetPathList_SellingTokens_Kyber([_erc20TokenContractAddress, Exchanges.kyber.EtherToken])

        self.tokenIsEnabled_betweenBancorAndKyber = False
        self.tokenIsEnabled_betweenBancorAnd0xv2 = False
        self.tokenIsEnabled_between0xv2AndKyber = False
        self.tokenIsEnabled_betweenUniswapAndKyber = False
        self.tokenIsEnabled_betweenUniswapAnd0x = False

        self.bancorTokenConverterContract = None
        self.bancorRelayerTokenAddress = None

        self.OrderAggregatorDict = {}

    def CreateOrderAggregator(self, exchangeName, quoteToken):
        from Libraries.orderAggregator import OrderAggregator

        if exchangeName not in self.OrderAggregatorDict:
            self.OrderAggregatorDict[exchangeName] = {}
        self.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()] = OrderAggregator(self.tokenName, quoteToken, exchangeName, self)

    def GetCopyOfAllOrders_Bids_FromTheseOrderAggregators(self, quoteToken, doConvertedFromJSONToOrderObjects=True, exchangeNames=None):
        # PrintAndLog_FuncNameHeader("quoteToken = " + str(quoteToken))
        # PrintAndLog_FuncNameHeader("doConvertedFromJSONToOrderObjects = " + str(doConvertedFromJSONToOrderObjects))
        # PrintAndLog_FuncNameHeader("exchangeNames = " + str(exchangeNames))
        returnDict = {}
        # Call GetCopyOfAllOrders_Bids for all orderAggregators
        for exchangeName in self.OrderAggregatorDict:
            # If exchangeNames == None, assume the caller wants to match all exchangeNames
            # else assume they want to match only the exchangeNames they passed into this call
            match = False
            if not exchangeNames or exchangeName in exchangeNames:
                match = True

            if match:
                # PrintAndLog_FuncNameHeader("Match found")
                orderAggregator = self.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()]
                # PrintAndLog_FuncNameHeader("orderAggregator = " + str(orderAggregator.GetDetails()))
                returnDict = Libraries.core.MergeDictionaries(returnDict, orderAggregator.GetCopyOfAllOrders_Bids(doConvertedFromJSONToOrderObjects))
            # else:
            #     PrintAndLog_FuncNameHeader("Match not found")

        return returnDict

    def GetCopyOfAllOrders_Asks_FromTheseOrderAggregators(self, quoteToken, doConvertedFromJSONToOrderObjects=True, exchangeNames=None):
        returnDict = {}
        # Call GetCopyOfAllOrders_Asks for all orderAggregators
        for exchangeName in self.OrderAggregatorDict:
            # If exchangeNames == None, assume the caller wants to match all exchangeNames
            # else assume they want to match only the exchangeNames they passed into this call
            match = False
            if not exchangeNames or exchangeName in exchangeNames:
                match = True

            if match:
                orderAggregator = self.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()]
                returnDict = Libraries.core.MergeDictionaries(returnDict, orderAggregator.GetCopyOfAllOrders_Asks(doConvertedFromJSONToOrderObjects))

        return returnDict

    def IsTokenEnabledAtLeastOnePlace(self):
        return self.tokenIsEnabled_betweenBancorAndKyber or \
               self.tokenIsEnabled_betweenBancorAnd0xv2 or \
               self.tokenIsEnabled_between0xv2AndKyber or \
               self.tokenIsEnabled_betweenUniswapAndKyber or \
               self.tokenIsEnabled_betweenUniswapAnd0x

    def SetPathList_BuyingTokens_Bancor(self, path):
        self.pathList_BuyingTokens_Bancor = path

    def SetPathList_SellingTokens_Bancor(self, path):
        self.pathList_SellingTokens_Bancor = path

    def SetPathList_BuyingTokens_Kyber(self, path):
        self.pathList_BuyingTokens_Kyber = path

    def SetPathList_SellingTokens_Kyber(self, path):
        self.pathList_SellingTokens_Kyber = path

    def ShouldUseOrderBookAggregator(self, exchangeName):
        # Get all of the ExchangeName_0xMesh tokens that are tradable on the Ninja
        tradeableTokensList = GetCopyOfExchangeTokensDictWithValueList(exchangeName).copy()
        tradeableTokensList = ConvertListOfStringsToLowercaseListOfStrings(tradeableTokensList)

        # PrintAndLog_FuncNameHeader("Checking to see if self.erc20TokenContractAddress " + str(
        #     self.erc20TokenContractAddress) + " is in tradeableTokensList " + str(tradeableTokensList))
        if self.erc20TokenContractAddress.lower() in tradeableTokensList:
            return True
        else:
            return False


def GetListOfTokenNamesFromTokenList(tokensList):
    returnList = []
    for token in tokensList:
        returnList.append(token.tokenName)

    return returnList


def GetTokenObjectGivenTokenAddress(tokenAddress):
    from Libraries.accounts import TokenDict_Ninja

    for tokenName in TokenDict_Ninja:
        token = TokenDict_Ninja[tokenName]
        if token.erc20TokenContractAddress.lower() == tokenAddress.lower():
            return token
