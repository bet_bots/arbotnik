class TradeProperties:
    quoteToken = None
    # Set to None if we don't care how many trade legs it has
    numOfTradeLegs = None
    # Set to None if we don't care how many trade legs it has
    tradedWithinFlashLoan = None
    # Set to None if we don't care how many trade legs it has
    exchangeName_toExclude = None

    def __init__(self, _quoteToken, _numOfTradeLegs, _tradedWithinFlashLoan, _exchangeName_toExclude):
        self.quoteToken = _quoteToken
        self.numOfTradeLegs = _numOfTradeLegs
        self.tradedWithinFlashLoan = _tradedWithinFlashLoan
        self.exchangeName_toExclude = _exchangeName_toExclude
