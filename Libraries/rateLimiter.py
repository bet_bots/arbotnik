import time

rate = 2.0  # unit: messages
per = 1.0  # unit: seconds
AllowanceDict = {}  # keyed by something unique about the message type, valued by allowance
LastCheckDict = {}  # keyed by something unique about the message type, valued by time in seconds


def EnforceRateLimit(key):
    global rate
    global per
    global AllowanceDict
    global LastCheckDict

    # Check for new keys
    if key.lower() not in AllowanceDict:
        AllowanceDict[key.lower()] = rate

    # Check for new keys
    if key.lower() not in LastCheckDict:
        LastCheckDict[key.lower()] = rate

    current = time.time()
    time_passed = current - LastCheckDict[key.lower()]
    LastCheckDict[key.lower()] = current

    # Update the allowance
    AllowanceDict[key.lower()] += time_passed * (rate / per)

    if AllowanceDict[key.lower()] > rate:
        AllowanceDict[key.lower()] = rate  # throttle
    if AllowanceDict[key.lower()] < 1.0:
        return False
    else:
        AllowanceDict[key.lower()] -= 1.0
        return True
