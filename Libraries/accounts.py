import copy
import datetime
from threading import Lock

import Libraries.security
import Libraries.transactions
import Libraries.nodes
import Libraries.cache
import Libraries.defaults
from Libraries.aesCipher import AESCipher
from Libraries.core import API_PostOperatorNotification
from Libraries.exceptions import OutOfAvailableAccountsException
from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader
from Libraries.market import Token_Ninja
from Libraries.transactions import PendingTransaction, TransactionType

OldWalletPublicAddresses = []

PrivateKey_NotSet = "Private key not set"


class EthereumAccount:
    marketName = None
    publicAddress = None
    privateKey = None

    # The balances within this account
    etherBalance_Wallet = None
    tokenBalance_Wallet = None
    # This is a pending withdraw from a CX
    etherBalancePendingFromCX_Wallet = None
    tokenBalancePendingFromCX_Wallet = None

    pendingTransactionList = None
    lock_pendingTransactionList = None

    def __init__(self, _marketName, _publicAddress):
        if len(_publicAddress) != Libraries.core.LengthOfPublicAddress_Including0x:
            raise Exception('_publicAddress is not the proper length')

        self.marketName = _marketName
        self.publicAddress = Libraries.nodes.Instance_Web3.toChecksumAddress(_publicAddress)
        self.privateKey = PrivateKey_NotSet
        self.etherBalance_Wallet = 0
        self.tokenBalance_Wallet = 0
        self.etherBalancePendingFromCX_Wallet = 0
        self.tokenBalancePendingFromCX_Wallet = 0

        self.pendingTransactionList = []
        self.lock_pendingTransactionList = Lock()

    def GetDetails(self):
        return self.marketName + ": " + str(self.publicAddress)

    def SetNewPendingTransaction(self, transactionId, transactionType, nonce):
        PrintAndLog("SetNewPendingTransaction: transactionId = " + str(transactionId) + ", transactionType = " + str(transactionType) + ", nonce = " + str(nonce))
        self.lock_pendingTransactionList.acquire()
        try:
            newPendingTransaction = PendingTransaction(transactionId, transactionType, nonce, self.publicAddress, self.marketName)
            self.pendingTransactionList.append(newPendingTransaction)

        finally:
            self.lock_pendingTransactionList.release()

    def RemovePendingTransaction(self, transactionToRemove):
        PrintAndLog("RemovePendingTransaction: transactionToRemove = " + transactionToRemove.GetDetails())
        self.RemovePendingTransaction_GivenTransactionId(transactionToRemove.transactionId)

    def RemovePendingTransaction_GivenTransactionId(self, transactionIdToRemove):
        # PrintAndLog("RemovePendingTransaction_GivenTransactionId: transactionIdToRemove = " + transactionIdToRemove)

        indexesToRemoveList = []
        self.lock_pendingTransactionList.acquire()
        try:
            for index, pendingTransaction in enumerate(self.pendingTransactionList):
                if pendingTransaction.transactionId.lower() == transactionIdToRemove.lower():
                    indexesToRemoveList.append(index)

            # Sort the list by largest to smallest
            sortedListOfIndexesToRemove = sorted(indexesToRemoveList, key=int, reverse=True)
            # PrintAndLog("sortedListOfIndexesToRemove = " + str(sortedListOfIndexesToRemove))

            # Remove the list items
            for indexToRemove in sortedListOfIndexesToRemove:
                # PrintAndLog("Removing item at index " + str(indexToRemove))
                del self.pendingTransactionList[indexToRemove]

        finally:
            self.lock_pendingTransactionList.release()

    def GetCopyOfPendingTransactionList(self):
        copyOfPendingTransactionList = None
        self.lock_pendingTransactionList.acquire()
        try:
            copyOfPendingTransactionList = copy.deepcopy(self.pendingTransactionList)

        finally:
            self.lock_pendingTransactionList.release()

        return copyOfPendingTransactionList

    def GetPendingTransactionCount(self):
        pendingTransactionCount = False
        self.lock_pendingTransactionList.acquire()
        try:
            pendingTransactionCount = len(self.pendingTransactionList) > 0

        finally:
            self.lock_pendingTransactionList.release()

        return pendingTransactionCount

    def SetEncryptedPKFromFile(self):
        # Set the encrypted private key for this account
        file = open(Libraries.defaults.Directory_PKs + "/" + self.marketName, "r")
        contents = file.read()
        file.close()

        self.privateKey = contents

    def DecryptPK(self):
        self.ConsiderCalling_SetEncryptedPKFromFile()
        return self.DecryptPK_ExplicitlyProvidePrivateKey(self.privateKey)

    def DecryptPK_ExplicitlyProvidePrivateKey(self, encryptedPrivateKey):
        self.ConsiderCalling_SetEncryptedPKFromFile()
        aesCipher = AESCipher(Libraries.security.GetP4s5wo0dFromMem())
        pk = str(aesCipher.decrypt(encryptedPrivateKey))
        return pk

    def ConsiderCalling_SetEncryptedPKFromFile(self):
        # Only do this if the privateKey has not yet been set
        if self.privateKey == PrivateKey_NotSet:
            self.SetEncryptedPKFromFile()

    def GetNonceToUseForQueueableTransaction(self):
        # Is there a current pending transaction transaction that's a trade, then get the largest nonce in the list
        highestNonce_trades = self.GetHighestNonceForTransactionType(TransactionType.trade)
        PrintAndLog("GetNonceToUseForQueueableTransaction: highestNonce_trades = " + str(highestNonce_trades))
        highestNonce_cancels = self.GetHighestNonceForTransactionType(TransactionType.cancelOrder)
        PrintAndLog("GetNonceToUseForQueueableTransaction: highestNonce_cancels = " + str(highestNonce_cancels))

        # We want to return the highest nonce + 1

        # If we have pending cancels but no pending trades
        if not highestNonce_trades and highestNonce_cancels:
            PrintAndLog("GetNonceToUseForQueueableTransaction: we have pending cancels and NOT trades, so we're basing our nonce off cancel nonces")
            nonceToUse = highestNonce_cancels
        # if we have pending trades but no pending cancels
        elif highestNonce_trades and not highestNonce_cancels:
            PrintAndLog("GetNonceToUseForQueueableTransaction: we have pending trades and NOT cancels, so we're basing our nonce off trade nonces")
            nonceToUse = highestNonce_trades
        # if we have both pending trades and cancels
        elif highestNonce_trades and highestNonce_cancels:
            PrintAndLog("GetNonceToUseForQueueableTransaction: we have both pending trades and cancels, so we're basing our nonce off their nonces")
            nonceToUse = max(highestNonce_trades, highestNonce_cancels)
        # else we don't have any pending trades or cancels (but we may have pending withdrawals/deposits)
        else:
            # None means no pending trades we are aware of, so just get the current transactionCount and use that as the nonce
            nonceToUse = None

        # Increment the nonce (if not null) because we want to queue the transaction
        if nonceToUse:
            nonceToUse = nonceToUse + 1

        PrintAndLog("GetNonceToUseForQueueableTransaction: nonceToUse = " + str(nonceToUse))
        return nonceToUse


class ReservableEthereumAccountPool:
    lock_allLists = None
    # Lists of accounts, available and in use
    accountsAvailableList = None
    accountsInUseList = None
    # DateTime that the account was reserved from the pool, keyed by the account
    accountsDateTimeDict = None
    # TransactionId that the account was reserved from the pool with, keyed by the account
    accountsNonceDict = None
    # Name that the account was reserved with, this way I can prevent all the accounts in the pool making the same transaction
    # For example, I can use the name "Ninja_Bancor-Kyber_DAI" and this will represent the account that's ninja-ing between bancor and kyber with DAI.
    # That way all accounts in the pool don't try and do the same thing.
    # But we want to allow many like this: "Ninja_Bancor-Kyber_DAI", "Ninja_Bancor-Kyber_USDC", "Ninja_Bancor-Kyber_SNT", etc, to grab a unique account from the pool
    accountsNameDict = None

    def __init__(self, _accountList):
        if len(_accountList) <= 0:
            raise Exception("_accountList must be > 0")

        self.lock_allLists = Lock()
        self.accountsAvailableList = _accountList
        self.accountsInUseList = []
        self.accountsDateTimeDict = {}
        self.accountsNonceDict = {}
        self.accountsNameDict = {}

    def GetSumOfTotalAccounts(self):
        return len(self.accountsAvailableList) + len(self.accountsInUseList)

    def ReserveAccountFromPool(self, name):
        firstAccountInList = None

        self.lock_allLists.acquire()
        try:
            # For debug purposes, get me the list of names currently in use
            listOfNamesCurrentlyInUse = []
            for account in self.accountsNameDict:
                local_name = self.accountsNameDict[account]
                listOfNamesCurrentlyInUse.append(local_name)

            PrintAndLog_FuncNameHeader("listOfNamesCurrentlyInUse = " + str(listOfNamesCurrentlyInUse))

            # If this name is already being used, we don't want multiple accounts in the pool to associate with the same name
            # because that can result in all the accounts in the pool making the same transaction
            for account in self.accountsNameDict:
                local_name = self.accountsNameDict[account]
                # If we've found a match of the same name already in use
                if local_name == name:
                    # Since we found a match, it should also be in accountsInUseList. If it's not something is wrong
                    if account not in self.accountsInUseList:
                        message = "This should never happen! An account and its name were entered into the accountsNameDict " \
                                  "but not in accountsInUseList. name = " + str(name) + " and account = " + str(account.GetDetails())
                        API_PostOperatorNotification(message)
                        raise Exception(message)

                    # Just return the account because this one already has a pending transaction under this same name
                    # We still want to return it in case the function caller wants to overwrite the current pending transaction with a higher gas price
                    message = "is returning an account that is currently in use. name = " + str(
                        name) + " and account = " + str(account.GetDetails())
                    PrintAndLog_FuncNameHeader(message)
                    # Tell the function caller that we're returning an account that is currently in use
                    # This is important because the caller doesn't want to call ReleaseAccountBackToPool
                    # if there's a pending transaction AND they choose to NOT overwrite the transaction with a higher gas price
                    return account, True

            if len(self.accountsAvailableList) <= 0:
                message = "Could not ReserveAccountFromPool because we're out of available accounts!"
                API_PostOperatorNotification(message)
                raise OutOfAvailableAccountsException(message)

            PrintAndLog_FuncNameHeader("accountsAvailableList BEFORE = " + str(GetDetailsOfAListOfEthereumAccounts(self.accountsAvailableList)))
            PrintAndLog_FuncNameHeader("accountsInUseList BEFORE = " + str(GetDetailsOfAListOfEthereumAccounts(self.accountsInUseList)))

            firstAccountInList = self.accountsAvailableList[0]
            # Remove it from the accountsAvailableList
            self.accountsAvailableList.remove(firstAccountInList)
            # Add it to the accountsInUseList
            self.accountsInUseList.append(firstAccountInList)
            # Set this account's entry in various dicts
            self.accountsDateTimeDict[firstAccountInList] = datetime.datetime.now()
            self.accountsNameDict[firstAccountInList] = name

            PrintAndLog_FuncNameHeader("Reserving " + str(firstAccountInList.GetDetails()) + " from accountsAvailableList under the name " + str(name))
            PrintAndLog_FuncNameHeader("accountsAvailableList AFTER = " + str(GetDetailsOfAListOfEthereumAccounts(self.accountsAvailableList)))
            PrintAndLog_FuncNameHeader("accountsInUseList AFTER = " + str(GetDetailsOfAListOfEthereumAccounts(self.accountsInUseList)))

        finally:
            self.lock_allLists.release()

        # Tell the function caller that the account was not in use prior to reserving
        return firstAccountInList, False

    def ReleaseAccountBackToPool(self, account, accountWasInUsePriorToReserving):
        self.lock_allLists.acquire()
        try:
            # Because of multi threading it's possible something releases it before something else wants to release it
            # So allow this and just return if it's already been released
            if account not in self.accountsInUseList:
                PrintAndLog_FuncNameHeader("account not in accountsInUseList, must have been already released. account = " + str(account.publicAddress))
                return

            if accountWasInUsePriorToReserving:
                # Do nothing here, we don't want to release this because it was in use prior to reserving it again.
                # This means function caller reserved it and considered overriding it with a higher gas price but either did not or failed to do so.
                # So we return here without releasing because we want that first transaction to have time to mine in.
                message = "ReleaseAccountBackToPool was called on account " + str(
                    account.GetDetails()) + " but it was in use prior to reserving. " \
                                            "So we're not going to release it because the previous reservation could still be pending"
                PrintAndLog(message)
                return

            PrintAndLog("ReleaseAccountBackToPool: accountsAvailableList BEFORE = " + str(GetDetailsOfAListOfEthereumAccounts(self.accountsAvailableList)))
            PrintAndLog("ReleaseAccountBackToPool: accountsInUseList BEFORE = " + str(GetDetailsOfAListOfEthereumAccounts(self.accountsInUseList)))

            # Remove it from the accountsInUseList
            self.accountsInUseList.remove(account)
            # Add it to the accountsAvailableList
            self.accountsAvailableList.append(account)
            # Remove this account's entry in various dicts
            if account in self.accountsDateTimeDict:
                del self.accountsDateTimeDict[account]
            if account in self.accountsNonceDict:
                del self.accountsNonceDict[account]
            if account in self.accountsNameDict:
                del self.accountsNameDict[account]

            PrintAndLog("ReleaseAccountBackToPool: Releasing " + str(account.GetDetails()) + " from accountsAvailableList")
            PrintAndLog("ReleaseAccountBackToPool: accountsAvailableList AFTER = " + str(GetDetailsOfAListOfEthereumAccounts(self.accountsAvailableList)))
            PrintAndLog("ReleaseAccountBackToPool: accountsInUseList AFTER = " + str(GetDetailsOfAListOfEthereumAccounts(self.accountsInUseList)))

        finally:
            self.lock_allLists.release()

    def GetTimeAccountHasBeenReserved(self, account, doLock=True):
        duration_s = None

        if not doLock:
            if account not in self.accountsDateTimeDict:
                raise Exception("account is not in accountsDateTimeDict")

            duration_s = (datetime.datetime.now() - self.accountsDateTimeDict[account]).total_seconds()
        else:
            self.lock_allLists.acquire()
            try:
                if account not in self.accountsDateTimeDict:
                    raise Exception("account is not in accountsDateTimeDict")

                duration_s = (datetime.datetime.now() - self.accountsDateTimeDict[account]).total_seconds()

            finally:
                self.lock_allLists.release()

        return duration_s

    def AssociateAccountWithNonce(self, account, transactionId):
        # Get the nonce for this transactionId from our convenient feature
        nonce = Libraries.transactions.TransactionNonceAssociationDict.GetValue(transactionId.lower())

        self.lock_allLists.acquire()
        try:
            # Associate the latest transaction nonce with this account
            if account in self.accountsNonceDict and self.accountsNonceDict[account]:
                # There's already a nonce set, so update it to the max nonce we know about
                self.accountsNonceDict[account] = max(self.accountsNonceDict[account], nonce)
            else:
                # Set the first ever nonce we know about
                self.accountsNonceDict[account] = nonce

        finally:
            self.lock_allLists.release()

    # TODO, make a function that cancels transactions that have been pending for too long.  AKA overwrites trade with same nonce with a tx that sends 0 ether to self.
    # TODO, should I put this here???

    def ReleaseAccountsInUseForTooLong(self):
        PrintAndLog("ReleaseAccountsInUseForTooLong len(self.accountsInUseList) = " + str(len(self.accountsInUseList)))
        accountsToRelease = []

        self.lock_allLists.acquire()
        try:
            for account in self.accountsInUseList:
                # If the account has been reserved for too long, release it
                timeAccountHasBeenReservedFor_seconds = self.GetTimeAccountHasBeenReserved(account, False)
                timeAccountHasBeenReservedFor_minutes = timeAccountHasBeenReservedFor_seconds / float(60)
                if timeAccountHasBeenReservedFor_minutes > Libraries.defaults.ReleaseAnInUse_ReservableEthereumAccountPool_AfterItsBeenInuseForTooLong_ExpirationDuration_minutes:
                    accountsToRelease.append(account)

        finally:
            self.lock_allLists.release()

        # Release all accounts necessary, make sure not to double lock...
        for account in accountsToRelease:
            message = "ReleaseAccountsInUseForTooLong: Releasing " + str(account.GetDetails()) + " because it was in use for too long"
            API_PostOperatorNotification(message)
            PrintAndLog(message)
            # Pass in False for accountWasInUsePriorToReserving here because we're assuming that this has been in use for too long and could have timed out or poofed
            self.ReleaseAccountBackToPool(account, False)

    def ReleaseAccountInUseIfNonceBecomesUsed(self):
        PrintAndLog_FuncNameHeader("JoeyZ Debug here!")
        accountsToRelease = []

        self.lock_allLists.acquire()
        try:
            # # Iterate through each account tx association and if any transactionIds match, flag this account to be released
            # for account in self.accountsNonceDict:
            #     transactionId_forThisAssociation = self.accountsNonceDict[account]
            #     if transactionId_forThisAssociation.lower() == transactionId.lower():
            #         accountsToRelease.append(account)

            for account in self.accountsInUseList:
                if account in self.accountsNonceDict:
                    accountsTransactionCount, dontCare = Libraries.cache.GetTransactionCountFromCacheIfPossible(account.publicAddress)
                    nonce_forThisAssociation = self.accountsNonceDict[account]
                    PrintAndLog_FuncNameHeader("account " + str(account.publicAddress) + "'s transaction count = " + str(
                        accountsTransactionCount) + ", nonce_forThisAssociation = " + str(nonce_forThisAssociation))
                    # If the transaction count exceeds the nonce_forThisAssociation we know the transaction was mined in
                    if accountsTransactionCount > nonce_forThisAssociation:
                        PrintAndLog_FuncNameHeader("releasing this account")
                        accountsToRelease.append(account)
                    else:
                        PrintAndLog_FuncNameHeader("NOT releasing this account")

        finally:
            self.lock_allLists.release()

        # Release all accounts necessary, make sure not to double lock...
        for account in accountsToRelease:
            message = "ReleaseAccountInUseIfNonceBecomesUsed: Releasing " + str(
                account.GetDetails()) + " because its transactionId was matched with one we targeted to release"
            API_PostOperatorNotification(message)
            PrintAndLog(message)
            # Pass in False for accountWasInUsePriorToReserving here because we're assuming the function caller has done this intentionally
            # An example of this, is the transaction has mined into a block
            self.ReleaseAccountBackToPool(account, False)


# Keep track of all ReservableEthereumAccountPools we're using, so that I can iterate over them and call ReleaseAccountBackToPool on accounts that need freed up
ReservableEthereumAccountPoolDict = {}

# Utility accounts
UtilityAccountDict = {}

name = "set-social-testing"
SetSocialTestAccount = EthereumAccount(name, "0x6D34dd153Cc6ecF82f8C6f7Cb8fB51A0E9Af5aa0".lower())
UtilityAccountDict[name] = SetSocialTestAccount

name = "bloxroute-tx-send-test-1"
TestAccount = EthereumAccount(name, "0xC741DC53281eDC7AAEbc99A41B93cb595D0cC580".lower())
UtilityAccountDict[name] = TestAccount

name = "bloxroute-tx-send-test-2"
TestAccount = EthereumAccount(name, "0xfE3b307a0E83100CdDc195252DbC88b4a83A659c".lower())
UtilityAccountDict[name] = TestAccount

name = "bloxroute-tx-send-test-3"
TestAccount = EthereumAccount(name, "0xDCadb6Cb35e681FbC7ECE3198b9d0cD85DFB8387".lower())
UtilityAccountDict[name] = TestAccount

name = "hiding-game-rfq-origins"
HidingGameRfqOriginsAccount = EthereumAccount(name, "0xBd49A97300E10325c78D6b4EC864Af31623Bb5dD".lower())
UtilityAccountDict[name] = HidingGameRfqOriginsAccount

name = "dedicated-tx-prediction"
DedicatedTxPredictionAccount = EthereumAccount(name, "0x11d6EdbE6a1735505f4920ac917BD7fC93335346".lower())
UtilityAccountDict[name] = DedicatedTxPredictionAccount

# Keyed by token symbol (technically, token symbol is NOT unique, but it should be for the tokens I trade)
TokenDict_Ninja = {}

# Ninja accounts
NinjaOpAccountDict = {}

# TODO modifying this to withdraw UNi only...
# name = "ninja-op-1"
# NinjaOpAccountDict[name] = EthereumAccount(name, "0x3bd189a9b1c11684b08f6464866dFa431362A06e".lower())

name = "ninja-op-100"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x211B6a1137BF539B2750e02b9E525CF5757A35aE".lower())

name = "ninja-op-102"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xcA77Dc47eec9E1C46c9F541ba0f222E741d6236b".lower())

name = "ninja-op-103"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x08e2AF71DB66d0a64136BE9d4DaBB8A3334F152F".lower())

name = "ninja-op-104"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x89D2495fd1D327ecB7fD420447D460E5A92e6DaC".lower())

name = "ninja-op-105"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x4f0604820e8ddc0959BC6143196e3091D02F061c".lower())

name = "ninja-op-106"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xED158390268bbe9d2cc450AcAbA2459C02bbCb03".lower())

name = "ninja-op-107"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xea97BE92160121A4EE95A1BE3Fd61c7E663596D9".lower())

name = "ninja-op-108"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xFf80D0aba54b1b89547E029A76Fe93e7807d8eE7".lower())

name = "ninja-op-109"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xA41164009Fa1021aC3CFF34813E461cA445D0712".lower())

name = "ninja-op-110"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xCf1c458992ef8d97093b067404A599fE4d9d8cDA".lower())

name = "ninja-op-111"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xc8c4C03E491052edd15a8D2b6E284Fc273619CE9".lower())

name = "ninja-op-112"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xCBAa8Ddc4B3bA61e3c52F4A05550F2C69912e0F0".lower())

name = "ninja-op-113"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x674D268f06A746385F996186696CbD81DF3769A4".lower())

name = "ninja-op-114"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xF7A22bB09049bbC7706483db119dbD15aEa97B5a".lower())

# ninja-op-115 is now being used for dedicated eth_estimateGas and eth_call in PurseTradeOpportunity

# *************************************************************************************************** #

name = "ninja-op-116"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x975Eac3e6dA5281d00844b251Cd146b9621Ef824".lower())

name = "ninja-op-117"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x6F1791978Ef090d3cEd90144536BC35103f434eD".lower())

name = "ninja-op-118"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x9c429360B5F265aa1aA10c700b7164898b8c71A7".lower())

name = "ninja-op-119"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xAe563C1f00FC7008A1224F9EfFE285E35eEc11b5".lower())

name = "ninja-op-120"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xfEFdF7d47156c8fD170B3A6F6c2359Fc4f0DB125".lower())

name = "ninja-op-121"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x752E9e35c6E323bed8121e6dAa35652cc00BD830".lower())

name = "ninja-op-122"
NinjaOpAccountDict[name] = EthereumAccount(name, "0xDBdE82967F75E3c4d16F717917320aED6f6c8d29".lower())

name = "ninja-op-123"
NinjaOpAccountDict[name] = EthereumAccount(name, "0x68017560682EfaEC734DcB4b00FD03c812398E52".lower())


def CreateNewReservableEthereumAccountPoolList(name, accountsList):
    global ReservableEthereumAccountPoolDict

    # PrintAndLog("CreateNewReservableEthereumAccountPoolList with accountsList = " + str(accountsList))
    if name in ReservableEthereumAccountPoolDict:
        # PrintAndLog("CreateNewReservableEthereumAccountPoolList: " + str(name) + " already exists in ReservableEthereumAccountPoolDict. Will not create it again")
        pass
    else:
        PrintAndLog("CreateNewReservableEthereumAccountPoolList: Creating new object for " + str(name) + " in ReservableEthereumAccountPoolDict")
        ReservableEthereumAccountPoolDict[name] = ReservableEthereumAccountPool(accountsList)

    return ReservableEthereumAccountPoolDict[name]


# Periodically call this so it releases accounts that have been in use for too long
def ReleaseReservableEthereumAccountPoolAccounts_ThatHaveBeenInUseForTooLong():
    global ReservableEthereumAccountPoolDict

    # PrintAndLog("ReleaseReservableEthereumAccountPoolAccounts_ThatHaveBeenInUseForTooLong with " + str(
    #     len(ReservableEthereumAccountPoolDict)) + " ReservableEthereumAccountPoolDicts")
    for name in ReservableEthereumAccountPoolDict:
        reservableEthereumAccountPool = ReservableEthereumAccountPoolDict[name]
        # PrintAndLog("calling ReleaseAccountsInUseForTooLong on " + str(name) + "'s reservableEthereumAccountPool")
        reservableEthereumAccountPool.ReleaseAccountsInUseForTooLong()
        # PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
        # PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
        # PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
        # PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
        # PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))


# Call this when a transaction you've been watching gets mined into a block or times out
def ReleaseReservableEthereumAccountPoolAccounts_BasedOnNonces():
    global ReservableEthereumAccountPoolDict

    PrintAndLog("ReleaseReservableEthereumAccountPoolAccounts_BasedOnNonces with " + str(
        len(ReservableEthereumAccountPoolDict)) + " ReservableEthereumAccountPoolDicts")
    for name in ReservableEthereumAccountPoolDict:
        reservableEthereumAccountPool = ReservableEthereumAccountPoolDict[name]
        PrintAndLog("calling ReleaseAccountInUseIfNonceBecomesUsed on " + str(name) + "'s reservableEthereumAccountPool")
        reservableEthereumAccountPool.ReleaseAccountInUseIfNonceBecomesUsed()
        PrintAndLog("reservableEthereumAccountPool.accountsAvailableList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsAvailableList)))
        PrintAndLog("reservableEthereumAccountPool.accountsInUseList = " + str(GetDetailsOfAListOfEthereumAccounts(reservableEthereumAccountPool.accountsInUseList)))
        PrintAndLog("reservableEthereumAccountPool.accountsDateTimeDict = " + str(reservableEthereumAccountPool.accountsDateTimeDict))
        PrintAndLog("reservableEthereumAccountPool.accountsNonceDict = " + str(reservableEthereumAccountPool.accountsNonceDict))
        PrintAndLog("reservableEthereumAccountPool.accountsNameDict = " + str(reservableEthereumAccountPool.accountsNameDict))


def GetDetailsOfAListOfEthereumAccounts(listOfAccounts):
    returnString = ""
    for account in listOfAccounts:
        returnString += str(account.GetDetails()) + ", "

    return returnString


def GetListOfAllKnownHotWalletAddresses(doIncludeArchivedAddresses=False):
    global OldWalletPublicAddresses

    addressList = []

    for key in NinjaOpAccountDict:
        addressList.append(NinjaOpAccountDict[key].publicAddress)

    for key in UtilityAccountDict:
        addressList.append(UtilityAccountDict[key].publicAddress)

    if doIncludeArchivedAddresses:
        addressList = addressList + OldWalletPublicAddresses

    # Remove duplicates
    addressList = list(dict.fromkeys(addressList))

    # PrintAndLog("addressList = " + str(addressList))
    return addressList


def CreateAddNewToken_Ninja(symbol, tokenAddress):
    global TokenDict_Ninja
    TokenDict_Ninja[symbol] = Token_Ninja(symbol, tokenAddress)


def DeleteToken_Ninja(symbol):
    global TokenDict_Ninja
    del TokenDict_Ninja[symbol]
