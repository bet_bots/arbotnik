import os
import sys
import time
import traceback
from cherrypy.process import bus

from Libraries.loggingConfig import PrintAndLogError

Filename_GracefulKill = "gracefullyKill.kill"


def GetFilename_GracefulKill():
    return Filename_GracefulKill


def RemoveFile_GracefullyKill():
    # Remove the Filename_GracefulKill since we are starting the script
    try:
        os.remove(Filename_GracefulKill)
    except OSError:
        pass


def CreateFile_GracefullyKill():
    try:
        file = open(Filename_GracefulKill, 'r')
    except IOError:
        file = open(Filename_GracefulKill, 'w')

    file.close()


def ConsiderGracefullyKillingTheScript():
    from Libraries.loggingConfig import PrintAndLog
    from Libraries.quoteTokenProcesses import SignalAllQuoteTokenProcesses_Kill

    # If we received the signal Filename_GracefulKill
    # beforeDateTime = datetime.datetime.now()
    if os.path.isfile(GetFilename_GracefulKill()):
        PrintAndLog("Gracefully killing Ninja on command")
        PrintAndLog("Sending kill command to processes, let's wait for them to die off")

        SignalAllQuoteTokenProcesses_Kill()

        time.sleep(2)

        RemoveFile_GracefullyKill()

        # Kill it
        
        # I was getting this error so I added bus.exit()
        # RuntimeWarning: The main thread is exiting, but the Bus is in the states.STARTED state; shutting it down automatically now.
        # You must either call bus.block() after start(), or call bus.exit() before the main thread exits.
        try:
            bus.exit()

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception, " + traceback.format_exc())

        try:
            sys.exit()

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception, " + traceback.format_exc())

    # duration_s = (datetime.datetime.now() - beforeDateTime).total_seconds()
    # PrintAndLog("ConsiderGracefullyKillingTheScript duration_s = " + str(duration_s))
