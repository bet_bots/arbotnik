from zero_ex.order_utils import generate_order_hash_hex

from Libraries.signingUtils import SignHash, GetSignatureStringFromVRS


def SignOrder_0xv2(order, privateKey):
    from Exchanges.zrxV2 import EncodeERC20AssetData, AppendSignatureTypeToSignatureString, SignatureType

    orderData_jData = {
        'makerAddress': order.maker,
        'takerAddress': order.taker,
        'feeRecipientAddress': order.feeRecipient,
        'senderAddress': order.senderAddress,
        'makerAssetAmount': order.makerTokenAmount,
        'takerAssetAmount': order.takerTokenAmount,
        'makerFee': order.makerFee,
        'takerFee': order.takerFee,
        'expirationTimeSeconds': order.expirationUnixTimestampSec,
        'salt': order.salt,
        'makerAssetData': EncodeERC20AssetData(order.makerTokenAddress),
        'takerAssetData': EncodeERC20AssetData(order.takerTokenAddress),
        'exchangeAddress': order.exchangeContractAddress,
    }
    # PrintAndLog_FuncNameHeader("orderData_jData = " + str(orderData_jData))

    orderHash = generate_order_hash_hex(orderData_jData)
    orderHash_bytesArray = bytes.fromhex(orderHash)
    v, r, s = SignHash(orderHash_bytesArray, privateKey)

    order.hash = '0x' + orderHash
    order.v = v
    order.r = r
    order.s = s
    print("order.hash = ", order.hash)
    print("order.v = ", order.v)
    print("order.r = ", order.r)
    print("order.s = ", order.s)

    pythonGeneratedSig = GetSignatureStringFromVRS(v, r, s)
    pythonGeneratedSig = AppendSignatureTypeToSignatureString(pythonGeneratedSig, SignatureType.EthSign)
    order.signature = pythonGeneratedSig
    # PrintAndLog_FuncNameHeader("order.signature = " + str(order.signature))
