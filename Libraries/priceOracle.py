import datetime
import threading
from threading import Lock
import time
import traceback

from Libraries.loggingConfig import PrintAndLog, PrintAndLogError
from Libraries.priceOracleSources import PriceSource_UniswapV2
from Libraries.utils import IsTokenAKnownEtherToken

# TODO, add UniswapV2 as the new source
PriceSourceList = []
PriceSourceList.append(PriceSource_UniswapV2('UniswapV2'))

ServiceHasBeenStarted = False
PriceOracleCachingService = None

TokenList = []
Lock_TokenList = Lock()

PriceDict = {}
Lock_PriceDict = Lock()

# This is when a price is considered old and we should remove it if it hasn't already been updated
PriceAgeThreshold_seconds = 900
SleepTime_seconds_Thread_PriceOracleCachingService = 290


class Price:
    # I created an object so I can track tings like how old this price quote is
    # If it's too old, I can get rid of it
    price = None
    dateTime = None

    def __init__(self, _price):
        self.price = _price
        self.dateTime = datetime.datetime.now()

    def GetAge_seconds(self):
        age_s = (datetime.datetime.now() - self.dateTime).total_seconds()
        return age_s

    def GetDetails(self):
        return "price = " + str(self.price) + ", age = " + str(self.GetAge_seconds())


class Thread_PriceOracleCachingService(object):

    def __init__(self):
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        global ServiceHasBeenStarted
        global TokenList
        global Lock_TokenList
        global PriceDict
        global Lock_PriceDict

        ServiceHasBeenStarted = True

        """ Method that runs forever """
        while True:
            try:
                newPriceDict = {}
                Lock_TokenList.acquire()
                try:
                    # Get new prices for each token
                    for tokenAddress in TokenList:
                        newPriceDict[tokenAddress.lower()] = Price(GetOnChainPriceFromAllSources(tokenAddress))
                        # Sleep a bit between, this isn't a HUGE priority since we're caching in the background
                        # Just don't want this spamming any nodes
                        time.sleep(0.5)

                finally:
                    Lock_TokenList.release()

                Lock_PriceDict.acquire()
                try:
                    # Get rid of all old prices in PriceDict
                    deleteList = []
                    for tokenAddress in PriceDict:
                        priceObject = PriceDict[tokenAddress]
                        # If this price is old
                        if priceObject.GetAge_seconds() > PriceAgeThreshold_seconds:
                            deleteList.append(tokenAddress)

                    for tokenAddress in deleteList:
                        del PriceDict[tokenAddress]

                    # Update the PriceDict with values from newPriceDict
                    for tokenAddress in newPriceDict:
                        newPriceObject = newPriceDict[tokenAddress]
                        PriceDict[tokenAddress] = newPriceObject

                    PrintAndLog("Thread_PriceOracleCachingService has " + str(len(PriceDict)) + " items")
                    # for tokenAddress in PriceDict:
                    #     priceObject = PriceDict[tokenAddress]
                    #     PrintAndLog("Thread_PriceOracleCachingService " + str(tokenAddress) + " 's price = " + str(priceObject.price) + " and it's " + str(
                    #         priceObject.GetAge_seconds()) + " seconds old")

                finally:
                    Lock_PriceDict.release()

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception = " + traceback.format_exc())
                pass

            time.sleep(SleepTime_seconds_Thread_PriceOracleCachingService)


def StartService():
    global PriceOracleCachingService
    global ServiceHasBeenStarted

    if ServiceHasBeenStarted:
        PrintAndLog("PriceOracleCachingService has already been started. Not starting it again")
    else:
        # Start thread that periodically updates the on chain price for each token we're interested in
        PriceOracleCachingService = Thread_PriceOracleCachingService()


def GetOnChainPriceFromAllSources(tokenAddress):
    global ServiceHasBeenStarted

    # This call can take some time, so I'm going to do it in a background thread periodically and return the cached result to the function caller to save time
    # Do not use this call for Ninja or Arby since it takes a long time.  Instead use the GetOnChainPrice_FromCache call since it gets the price from the cache

    if not ServiceHasBeenStarted:
        raise Exception("You must start the service so we can use the caching system")

    # if the function caller passes in the ether address, the price of ether with respect to ether is always 1.0
    if IsTokenAKnownEtherToken(tokenAddress):
        return 1.0

    # before = datetime.datetime.now()
    # For now, we're assuming that this token is priced against Ether
    # Let's just average all the prices together for now.  In the future, I can weight them based on asset depth or something
    priceSum = 0
    for priceSource in PriceSourceList:
        price = priceSource.GetOnChainPrice(tokenAddress)
        priceSum += price

    averagePrice = priceSum / float(len(PriceSourceList))
    # duration_s = (datetime.datetime.now() - before).total_seconds()
    # PrintAndLog("GetOnChainPriceFromAllSources: duration_s = " + str(duration_s))
    # PrintAndLog("GetOnChainPriceFromAllSources: averagePrice = " + str(averagePrice))
    return averagePrice


def GetOnChainPrice_FromCache(tokenAddress):
    global PriceDict
    global Lock_PriceDict

    # If the price is NOT in the cache
    if tokenAddress.lower() not in PriceDict:
        # Get the price
        price = GetOnChainPriceFromAllSources(tokenAddress.lower())

        # Update the PriceDict
        Lock_PriceDict.acquire()
        try:
            # PrintAndLog("GetOnChainPrice_FromCache: PriceDict has been updated for this individual price. " + str(tokenAddress) + " 's price = " + str(price))
            PriceDict[tokenAddress.lower()] = Price(price)

        finally:
            Lock_PriceDict.release()

        return price

    # Else it's in the cache
    else:
        priceToReturn = None

        Lock_PriceDict.acquire()
        try:
            priceObject = PriceDict[tokenAddress.lower()]
            priceToReturn = priceObject.price

        finally:
            Lock_PriceDict.release()

        # PrintAndLog("GetOnChainPrice_FromCache: Returning price from cache since it's legit. " + str(tokenAddress) + " 's priceToReturn = " + str(priceToReturn))
        return priceToReturn


def GetOnChainPrice_FromCache_TokenForToken(quoteTokenAddress, baseTokenAddress):
    # Get the prices with respect to ETH then convert it to quote with respect to base
    price_quoteTokenWithRespectToEth = GetOnChainPrice_FromCache(quoteTokenAddress)
    price_baseTokenWithRespectToEth = GetOnChainPrice_FromCache(baseTokenAddress)
    # PrintAndLog("price_quoteTokenWithRespectToEth = " + str(price_quoteTokenWithRespectToEth))
    # PrintAndLog("price_baseTokenWithRespectToEth = " + str(price_baseTokenWithRespectToEth))
    price_withRespectToQuoteToken = price_baseTokenWithRespectToEth / price_quoteTokenWithRespectToEth
    # PrintAndLog("price_withRespectToQuoteToken = " + str(price_withRespectToQuoteToken))
    return price_withRespectToQuoteToken
