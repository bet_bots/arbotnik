from sha3 import keccak_256
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog_FuncNameHeader
from Libraries.signingUtils import address, uint, SignHash


def HashOrder_0x(exchangeContractAddress, maker, taker, makerTokenAddress, takerTokenAddress, feeRecipient,
                 makerTokenAmount, takerTokenAmount, makerFee, takerFee, expirationUnixTimestampSec, salt):
    hashed_order = keccak_256(
        address(exchangeContractAddress) +
        address(maker) +
        address(taker) +
        address(makerTokenAddress) +
        address(takerTokenAddress) +
        address(feeRecipient) +
        uint(makerTokenAmount) +
        uint(takerTokenAmount) +
        uint(makerFee) +
        uint(takerFee) +
        uint(expirationUnixTimestampSec) +
        uint(salt))

    PrintAndLog_FuncNameHeader('orderHash.hexdigest = ' + str(hashed_order.hexdigest()))
    PrintAndLog_FuncNameHeader('hashed_order of type ' + str(type(hashed_order)) + ' = ' + str(hashed_order))
    return hashed_order, "0x" + str(hashed_order.hexdigest())


def SignOrder_0x(order, fromPrivateKey):
    hash_keccak_256, hashString = HashOrder_0x(order.exchangeContractAddress, order.maker, order.taker, order.makerTokenAddress, order.takerTokenAddress, order.feeRecipient,
                                               order.makerTokenAmount, order.takerTokenAmount, order.makerFee, order.takerFee, order.expirationUnixTimestampSec, order.salt)
    v, r, s = SignHash(hash_keccak_256.digest(), fromPrivateKey)

    order.hash = '0x' + hash_keccak_256.hexdigest()
    order.v = v
    order.r = r
    order.s = s
    print("order.hash = ", order.hash)
    print("order.v = ", order.v)
    print("order.r = ", order.r)
    print("order.s = ", order.s)
