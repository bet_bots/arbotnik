import rlp
import random
import string
from eth_abi import encode_single
from random import randint
from web3 import Web3
from web3.auto import w3
from eth_account.messages import defunct_hash_message
from eth_utils import address as eth_address
from eth_utils import hexadecimal
from eth_utils import keccak, to_checksum_address, to_bytes
from sha3 import keccak_256
from bitcoin import ecdsa_raw_sign
import unittest

from Libraries.loggingConfig import PrintAndLogError, PrintAndLog_FuncNameHeader
import Libraries.network
import Libraries.core
from Libraries.nodes import Instance_Web3


def address(val):
    try:
        return eth_address.to_canonical_address(val)
    except ValueError as e:
        PrintAndLogError('Could not convert address {0}: {1}'.format(val, e))
        raise


def uint(val):
    try:
        hashedSignedAmountGet = Web3.toHex(int(val))
        decode_hex = hexadecimal.decode_hex('0x' + hashedSignedAmountGet[2:].zfill(64))
        return decode_hex
    except TypeError as e:
        PrintAndLogError('Could not convert uint {0}: {1}'.format(val, e))
        raise


def ConvertIntToHex(number_int):
    return "%x" % int(number_int)


def ConvertHexToInt(hex):
    return int(hex, 16)


def Enforce64Characters_AppendLeadingZerosIfShort(str):
    # Handle the case where there's less than 64 characters, this means there are leading zeros but they have been left off... I need to fill them in so I have the proper length
    if len(str) < 64:
        diff = 64 - len(str)
        str = ('0' * diff) + str

    return str


def SignHash(hash_bytesArray, privateKey):
    magic = keccak_256(b'\x19Ethereum Signed Message:\n32' + hash_bytesArray)
    V, R, S = ecdsa_raw_sign(magic.hexdigest(), hexadecimal.decode_hex(privateKey))

    v = V
    r = ConvertIntToHex(R)
    s = ConvertIntToHex(S)
    # PrintAndLog_FuncNameHeader("v = " + str(v))
    # PrintAndLog_FuncNameHeader("r = " + r)
    # PrintAndLog_FuncNameHeader("s = " + s)
    r = '0x' + Enforce64Characters_AppendLeadingZerosIfShort(r)
    s = '0x' + Enforce64Characters_AppendLeadingZerosIfShort(s)
    # PrintAndLog_FuncNameHeader("v after enforcing character count and appending leading zeros if need be = " + str(v))
    # PrintAndLog_FuncNameHeader("r after enforcing character count and appending leading zeros if need be = " + r)
    # PrintAndLog_FuncNameHeader("s after enforcing character count and appending leading zeros if need be = " + s)
    return v, r, s


def GetSignatureStringFromVRS(v, r, s):
    return '0x' + Libraries.core.ConvertIntToHex(v) + Libraries.core.Remove0XfromHexString(r) + Libraries.core.Remove0XfromHexString(s)


# TODO, this assumes the itemsList is a list of addresses. It should support any type and detect it...
def Keccak256(itemsList):
    # Concatenate all values in the list
    concatenatedByteArray = b''

    for item in itemsList:
        concatenatedByteArray += address(item)

    hash_bytes = keccak_256(concatenatedByteArray)
    return Web3.toHex(hash_bytes.digest())


def GetMethodSignature(methodName, params, doConvertFromBytesToHexString=True):
    textToHash = str(methodName) + "(" + str(params) + ")"
    methodSignature = Web3.sha3(text=textToHash)[0:4]
    # PrintAndLog_FuncNameHeader("methodSignature of type " + str(type(methodSignature)) + " = " + str(methodSignature))
    if doConvertFromBytesToHexString:
        methodSignature = methodSignature.hex()

    return methodSignature


def GetMethodSignature_GivenAbi(methodName, abi):
    # PrintAndLog_FuncNameHeader("found a trade function: " + str(methodName))
    params = ''
    for index, input in enumerate(abi['inputs']):
        if index > 0:
            params += ','

        if input['type'] == 'tuple':
            params += '('
            for index2, tupleComponent in enumerate(input['components']):
                if index2 > 0:
                    params += ','

                params += tupleComponent['type']

            params += ')'

        else:
            params += input['type']

    methodSignature = GetMethodSignature(methodName, params)
    # PrintAndLog_FuncNameHeader("methodSignature = " + str(methodSignature) + ", methodName " + str(methodName) + ", params = " + str(params))
    return methodSignature


def GenerateContractAddress(sendersAddress: str, nonce: int):
    # Generate the contract address in the event sendersAddress deploys a contract.
    # nonce for the contracts sendersAddress has deployed. It starts at zero.
    """Create a contract address using eth-utils.
    # https://ethereum.stackexchange.com/a/761/620
    """

    # datetimeBefore = datetime.datetime.now()

    sendersAddress_bytes = to_bytes(hexstr=sendersAddress)
    raw = rlp.encode([sendersAddress_bytes, nonce])
    h = keccak(raw)
    address_bytes = h[12:]

    # duration_ms = (datetime.datetime.now() - datetimeBefore).total_seconds() * 1000
    # PrintAndLog_FuncNameHeader("duration = " + str(duration_ms) + " ms")

    return to_checksum_address(address_bytes)


def GenerateNewEthereumAccount(resultDict=None, resultKey=None, lock_resultDict=None):
    from Libraries.utils import ConsiderSettingResultDict

    # datetimeBefore = datetime.datetime.now()

    entropy = "asdfjkqqAAAsjlgh834790y" + "".join(random.choice(string.digits) for x in range(randint(100, 100)))
    # PrintAndLog_FuncNameHeader("GenerateNewEthereumAccount: entropy = " + str(entropy))
    account = Instance_Web3.eth.account.create(entropy)
    accountAddress_string = account.address
    privateKey_string = str(account.privateKey.hex().replace("0x", ""))

    # duration_ms = (datetime.datetime.now() - datetimeBefore).total_seconds() * 1000
    # PrintAndLog_FuncNameHeader("duration = " + str(duration_ms) + " ms")

    ConsiderSettingResultDict((privateKey_string, accountAddress_string, account), resultDict, resultKey, lock_resultDict)
    return privateKey_string, accountAddress_string, account


def GetFunctionSelectorsListForAllFunctionsInContract(contract, doPrintDebug=False):
    # functionselectors, methodhashes, functionhashes, methodselectors
    functionSelectorList = []
    for function in contract.all_functions():
        # PrintAndLog_FuncNameHeader("function = " + str(function))
        # PrintAndLog_FuncNameHeader("function.abi = " + str(function.abi))
        # PrintAndLog_FuncNameHeader("function.abi['name'] = " + str(function.abi['name']))

        methodName = function.abi['name']

        methodSignature = GetMethodSignature_GivenAbi(methodName, function.abi)
        if doPrintDebug:
            PrintAndLog_FuncNameHeader("methodSignature = " + str(methodSignature) + " for method " + str(methodName) + " in contract " + str(contract.address))

        functionSelectorList.append(methodSignature.lower())

    return functionSelectorList


def GetFunctionSelectorsDictForAllFunctionsInContract(contract, doPrintDebug=False):
    # functionselectors, methodhashes, functionhashes, methodselectors
    functionNameDict = {}
    functionSelectorDict = {}
    for function in contract.all_functions():
        # PrintAndLog_FuncNameHeader("function = " + str(function))
        # PrintAndLog_FuncNameHeader("function.abi = " + str(function.abi))
        # PrintAndLog_FuncNameHeader("function.abi['name'] = " + str(function.abi['name']))

        methodName = function.abi['name']

        methodSignature = GetMethodSignature_GivenAbi(methodName, function.abi).lower()
        if doPrintDebug:
            PrintAndLog_FuncNameHeader("methodSignature = " + str(methodSignature) + " for method " + str(methodName) + " in contract " + str(contract.address))

        # Key it both ways for convenience
        functionNameDict[methodSignature] = methodName

        functionSelectorDict[methodName] = methodSignature

    return functionSelectorDict, functionNameDict


def GetFunctionSelectorFromTxInputData(txInputData):
    return Libraries.core.GetFirstXCharsInString(txInputData, 10).replace("0x", "")


def EncodeABI(functionName, parameterTypes, parametersList):
    # PrintAndLog_FuncNameHeader("parameterTypes = " + str(parameterTypes))
    method_signature = Web3.sha3(text=f"{functionName}({parameterTypes})")[0:4]
    method_parameters = encode_single(f"({parameterTypes})", parametersList)
    data_hex = '0x' + (method_signature + method_parameters).hex()
    return data_hex


def SignMessage(message_text, private_key):
    # print('sign 1 message_text = ', message_text)
    message_hash = defunct_hash_message(text=message_text)
    # print('sign 2 ', sign)
    signature_obj = w3.eth.account.signHash(message_hash, private_key=private_key)
    # print('sign 3 returning ', signature_obj['signature'].hex())
    return signature_obj['signature'].hex()


def RecoverAddressFromSignature(text, signature):
    message_hash = defunct_hash_message(text=text)
    address = w3.eth.account.recoverHash(message_hash, signature=signature)
    return address.lower()


class Test(unittest.TestCase):
    def test_signature(self):
        random_acc = w3.eth.account.create()

        address = random_acc.address.lower()
        priv_key = random_acc.privateKey.hex()
        message = 'hi'
        signature = SignMessage(message, priv_key)

        self.assertEqual(RecoverAddressFromSignature(message, signature), address)
