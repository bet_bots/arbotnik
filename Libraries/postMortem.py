from Libraries.loggingConfig import PrintAndLog
import Libraries.ninjaUtils


def PostMortem_Logs(filepath_directory):
    logContents_PursueTradeOpportunity = GetFileContentsContaining_PostMortemString(filepath_directory)
    logContents_Blocks = GetFileContentsContaining_Blocks(filepath_directory)
    logContents_Mempool = GetFileContentsContaining_Mempool(filepath_directory)

    for traceData in logContents_PursueTradeOpportunity:
        traceString = traceData[0]
        line = traceData[1]
        splitString = traceString.split(' ')
        txHash = splitString[0].lower()
        # PrintAndLog("txHash = " + str(txHash))
        tokenAddressesToTraceList = splitString[1].lower().split(',')
        # PrintAndLog("tokenAddressesToTraceList = " + str(tokenAddressesToTraceList))
        PrintAndLog("Calling TraceTrade with line: " + str(line))
        Libraries.ninjaUtils.TraceTrade(txHash, tokenAddressesToTraceList, logContents_Blocks, logContents_Mempool)


def GetFileContentsContaining_PostMortemString(filepath_directory):
    filepath = filepath_directory + 'logfile_zz_PursueTradeOpportunity.log'
    logContents = []
    file = open(filepath, "r")
    for line in file:
        line = line.lower()
        searchString = 'post mortem string = '.lower()
        if searchString in line:
            # PrintAndLog("line = " + str(line))
            splitString = line.split(searchString)
            # PrintAndLog("splitString[1] = " + str(splitString[1]))
            traceString = splitString[1]
            traceString = traceString.replace("\n", "")
            logContents.append((traceString, line))

    file.close()
    return logContents


def GetFileContentsContaining_Blocks(filepath_directory):
    filepath = filepath_directory + 'logfile_blocks.log'
    logContents = []
    file = open(filepath, "r")
    for line in file:
        line = line.lower()
        searchString = 'found NewHead with blockNumber'.lower()
        if searchString in line:
            logContents.append(line)

    file.close()
    return logContents


def GetFileContentsContaining_Mempool(filepath_directory):
    filepath = filepath_directory + 'logfile_mempool.log'
    logContents = []
    file = open(filepath, "r")
    for line in file:
        line = line.lower()
        logContents.append(line)

    file.close()
    return logContents
