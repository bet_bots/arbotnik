import Libraries.defaults


def GetAsciiArt(mediaFilename):
    file = open(Libraries.defaults.Directory_Media + "/" + mediaFilename, "r")
    spaceShipContents = file.read()
    file.close()
    return spaceShipContents
