import traceback

import Libraries.gasStation
import Libraries.cache
import Libraries.core

# Multiply our gas estimate by something, and that will be our gas limit
from Libraries.exchanges import Exchange_Kyber, Exchange_Uniswap, ExchangeName_Kyber, ExchangeName_Uniswap, ExchangeName_Bancor, ExchangeName_Set, ExchangeName_0x
from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader

# Lowering this should be fine for Ninja V2 Generic, but will probably result in a lot of old set integrations to ber broken and have out of gas errors
# Multiplier_GetSuggestedGasLimit = 2.3
Multiplier_GetSuggestedGasLimit = 1.79  # Had lots of out of gas errors I should have fixed the errors...  ~1.5 may be fine now, may even be overkill

EstimatedGasOverheadCost_NinjaTxCallData = 30000
EstimatedGasOverheadCost_General = 15000


def CalculateAndCache_EstimatedGasCost_Ninja_Trade(TokenDict_Ninja, publicAddress_Ninja):
    dataToStore = {}

    for tokenSymbol in TokenDict_Ninja:
        token = TokenDict_Ninja[tokenSymbol]

        # PrintAndLog("Estimating trading gas usage for " + str(tokenSymbol))

        # TODO, hard coded for now, try with a few different amounts and take one of the valid responses as a valid result
        sourceQuantityList_etherUnits = [1.0, 0.1, 0.001]
        estimatedGas_kyber = None
        estimatedGas_uniswap = None
        estimatedGas_bancor = None
        for sourceQuantity_etherUnits in sourceQuantityList_etherUnits:
            estimatedGas_kyber_local = None
            estimatedGas_uniswap_local = None
            estimatedGas_bancor_local = None

            try:
                PrintAndLog("Calling Exchange_Kyber.GetEstimatedGasForTrade for tokenSymbol = " + str(tokenSymbol))
                estimatedGas_kyber_local = Exchange_Kyber.GetEstimatedGasForTrade(publicAddress_Ninja, token, sourceQuantity_etherUnits)
                # We want only the amount of gas used for the trade, so subtract GasUsedWhenExecutingEmptyFunctionWithNothingInIt
                estimatedGas_kyber_local -= Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt
                PrintAndLog("estimatedGas_kyber_local = " + str(estimatedGas_kyber_local))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                message = "exception in estimate gas logic " + traceback.format_exc()
                Libraries.core.API_PostOperatorNotification(message)

            try:
                PrintAndLog("Calling Exchange_Uniswap.GetEstimatedGasForTrade for tokenSymbol = " + str(tokenSymbol))
                estimatedGas_uniswap_local = Exchange_Uniswap.GetEstimatedGasForTrade(publicAddress_Ninja, token, sourceQuantity_etherUnits)
                # We want only the amount of gas used for the trade, so subtract GasUsedWhenExecutingEmptyFunctionWithNothingInIt
                estimatedGas_uniswap_local -= Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt
                PrintAndLog("estimatedGas_uniswap_local = " + str(estimatedGas_uniswap_local))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                message = "exception in estimate gas logic " + traceback.format_exc()
                Libraries.core.API_PostOperatorNotification(message)

            try:
                # I'm too busy to make one of these functions for Bancor, and Bancor seems to be very similar to kyber.
                # My estimations say that Bancor is about 90% as expensive as Kyber.  So I can just assume they are equal and call it a day.
                estimatedGas_bancor_local = estimatedGas_kyber_local
                PrintAndLog("estimatedGas_bancor_local = " + str(estimatedGas_bancor_local))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                message = "exception in estimate gas logic " + traceback.format_exc()
                Libraries.core.API_PostOperatorNotification(message)

            # Take the smallest one in case some are different.  Not sure if this is correct but let's try it!
            if estimatedGas_kyber_local and (not estimatedGas_kyber or estimatedGas_kyber_local < estimatedGas_kyber):
                estimatedGas_kyber = estimatedGas_kyber_local

            if estimatedGas_uniswap_local and (not estimatedGas_uniswap or estimatedGas_uniswap_local < estimatedGas_uniswap):
                estimatedGas_uniswap = estimatedGas_uniswap_local

            if estimatedGas_bancor_local and (not estimatedGas_bancor or estimatedGas_bancor_local < estimatedGas_bancor):
                estimatedGas_bancor = estimatedGas_bancor_local

        PrintAndLog("Estimating trading gas usage for " + str(tokenSymbol) + ":")
        PrintAndLog("   estimatedGas_kyber = " + str(estimatedGas_kyber))
        PrintAndLog("   estimatedGas_uniswap = " + str(estimatedGas_uniswap))
        PrintAndLog("   estimatedGas_bancor = " + str(estimatedGas_bancor))

        if token.erc20TokenContractAddress.lower() not in dataToStore:
            dataToStore[token.erc20TokenContractAddress.lower()] = {}

        if estimatedGas_kyber:
            dataToStore[token.erc20TokenContractAddress.lower()][ExchangeName_Kyber.lower()] = estimatedGas_kyber

        if estimatedGas_uniswap:
            dataToStore[token.erc20TokenContractAddress.lower()][ExchangeName_Uniswap.lower()] = estimatedGas_uniswap

        if estimatedGas_bancor:
            dataToStore[token.erc20TokenContractAddress.lower()][ExchangeName_Bancor.lower()] = estimatedGas_bancor

    PrintAndLog("Caching dataToStore: " + str(dataToStore))
    Libraries.cache.GetAndCache_GasEstimates_Trades(dataToStore)


def GetSuggestedGasLimit_Ninja_Trade_QuoteTokenIsEther(baseToken, exchangeNameList, useCTokenBidderContract):
    global Multiplier_GetSuggestedGasLimit

    return int(Multiplier_GetSuggestedGasLimit * GetEstimatedGasCost_Ninja_Trade_QuoteTokenIsEther(
        baseToken, exchangeNameList, useCTokenBidderContract))


def GetSuggestedGasLimit_Ninja_Trade_TokenForToken(quoteToken, baseToken, exchangeNameList, useCTokenBidderContract):
    global Multiplier_GetSuggestedGasLimit

    return int(Multiplier_GetSuggestedGasLimit * GetEstimatedGasCost_Ninja_Trade_TokenForToken(
        quoteToken, baseToken, exchangeNameList, useCTokenBidderContract))


def GetEstimatedGasCost_Ninja_Trade_QuoteTokenIsEther(baseToken, exchangeNameList, useCTokenBidderContract):
    return GetEstimatedGasCost_Ninja_Trade_TokenForToken(
        Libraries.core.GetEtherContractAddress(), baseToken, exchangeNameList, useCTokenBidderContract)


def GetEstimatedGasCost_Ninja_Trade_TokenForToken(quoteToken, baseToken, exchangeNameList, useCTokenBidderContract):
    from Libraries.core import GasUsedWhenExecutingEmptyFunctionWithNothingInIt
    from Libraries.cache import GetDataFromCache
    from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings
    from Libraries.utils import IsTokenAKnownEtherToken

    # exchangeNameList must be lowercase when we do string compares
    exchangeNameList = ConvertListOfStringsToLowercaseListOfStrings(exchangeNameList)

    if len(exchangeNameList) > 2 or len(exchangeNameList) <= 0:
        raise Exception("this function was not built to handle trades on this number (" + str(exchangeNameList) + ") of exchanges")

    if useCTokenBidderContract and ExchangeName_Set.lower() not in exchangeNameList:
        raise Exception("ExchangeName_Set wasn't found in exchangeNameList yet function parameter specified a Set only option.")

    gasCostToCheckRequirement_getBalance = 15000
    gasCostToCheckRequirement_kyber_getConversionRate = 60000

    gasCostForEntireTrade = GasUsedWhenExecutingEmptyFunctionWithNothingInIt
    # PrintAndLog_FuncNameHeader("gasCostForEntireTrade = " + str(gasCostForEntireTrade) + " after base transaction cost")
    gasCostForEntireTrade += gasCostToCheckRequirement_getBalance
    gasCostForEntireTrade += gasCostToCheckRequirement_kyber_getConversionRate
    # PrintAndLog_FuncNameHeader("gasCostForEntireTrade = " + str(gasCostForEntireTrade) + " after simple requirement costs")

    # Set a gasCostMultiplier which we will multiply by the return value to achieve the following
    # When trading token for token on certain protocols, the gas cost is doubled! Because it's actually making two trades under the hood
    # So I'm applying the gasCostMultiplier where that's the case, example: kyber, uniswap...  But not 0x
    gasCostMultiplier = 1
    if not IsTokenAKnownEtherToken(quoteToken):
        gasCostMultiplier = 1.4
        # PrintAndLog_FuncNameHeader("set gasCostMultiplier to " + str(
        #     gasCostMultiplier) + " because we're trading token to token instead of token to eth")

    # PrintAndLog_FuncNameHeader("gasCostMultiplier = " + str(gasCostMultiplier))

    # Factor in the overhead gas cost of trading through KeeperDAO
    doFactorInKeeperDAOOverheadGasCost = True
    gasCostAdder = 0
    if doFactorInKeeperDAOOverheadGasCost:
        gasCostAdder = Libraries.gasStation.GetGasExpectedToSpend_TradeWithFlashLoanThroughKeeperDAO_OverheadCostOnly()

    # PrintAndLog_FuncNameHeader("gasCostAdder = " + str(gasCostAdder))

    # Check to see if we have a specified gas cost for an exchange pair
    # If so, we'll just return here and be done because this number includes all overhead trading costs in its estimation

    if ExchangeName_Kyber.lower() in exchangeNameList and ExchangeName_Uniswap.lower() in exchangeNameList:
        return int(gasCostAdder) + int(gasCostMultiplier * Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_UniswapAndKyber())

    if ExchangeName_Kyber.lower() in exchangeNameList and ExchangeName_Bancor.lower() in exchangeNameList:
        return int(gasCostAdder) + int(gasCostMultiplier * Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_BancorAndKyber())

    if ExchangeName_Set.lower() in exchangeNameList and ExchangeName_Kyber.lower() in exchangeNameList:
        if useCTokenBidderContract:
            # PrintAndLog_FuncNameHeader("JOEYZ DEBUG")
            # PrintAndLog_FuncNameHeader("gasCostMultiplier = " + str(gasCostMultiplier))
            # PrintAndLog_FuncNameHeader("Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAndKyber_UseCTokenBidderContract() = " + str(
            #     Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAndKyber_UseCTokenBidderContract()))

            return int(gasCostAdder) + int(gasCostMultiplier * Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAndKyber_UseCTokenBidderContract())
        else:
            return int(gasCostAdder) + int(gasCostMultiplier * Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAndKyber())

    if ExchangeName_Set.lower() in exchangeNameList and ExchangeName_Uniswap.lower() in exchangeNameList:
        return int(gasCostAdder) + int(gasCostMultiplier * Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAndUniswap())

    if ExchangeName_Set.lower() in exchangeNameList and ExchangeName_0x.lower() in exchangeNameList:
        if useCTokenBidderContract:
            return int(gasCostAdder) + int(gasCostMultiplier * Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAnd0x_UseCTokenBidderContract())
        else:
            return int(gasCostAdder) + int(gasCostMultiplier * Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations_SetAnd0x())

    # If we've made it this far and Set is in the exchangeNameList and we have not yet returned a value, we need to add a feature above to handle it!
    # Set is a very unique use case and requires me to put in a value to help handle useCTokenBidderContract
    if ExchangeName_Set.lower() in exchangeNameList:
        raise Exception("This set exchange pair(" + str(exchangeNameList) + ") Is not yet supported, please add a feature to support this gas estimation!!")

    # If we've made it this far, we do not have an exchange pair specific number, so try the loop below instead
    # Loop through the exchangeNameList and build an estimated gas cost for the transaction
    estimatedGasDataDict = GetDataFromCache("GasEstimates_Trades")
    # PrintAndLog_FuncNameHeader("estimated gas data = " + str(estimatedGasDataDict[tokenContract]))
    for exchangeName in exchangeNameList:
        if (baseToken.lower() not in estimatedGasDataDict) or (exchangeName.lower() not in estimatedGasDataDict[baseToken.lower()]):
            PrintAndLog_FuncNameHeader("Could not find the token's gas cost for exchangeName " + str(
                exchangeName) + " so we're resorting to the fallback hard coded gas limit value. exchangeNameList = " + str(exchangeNameList))

            # estimatedGasDataDict cannot handle this exchange, so let's just return a general value until we can upgrade estimatedGasDataDict
            return int(gasCostAdder) + int(gasCostMultiplier * Libraries.gasStation.GetGasLimit_Ninja_UseThisForTradingProfitEstimations())

        else:
            estimatedGasCost = estimatedGasDataDict[baseToken.lower()][exchangeName.lower()]

        gasCostForEntireTrade += estimatedGasCost
        # PrintAndLog_FuncNameHeader("gasCostForEntireTrade = " + str(gasCostForEntireTrade) + " after " + str(exchangeName) + " trade for token " + str(baseToken))

    # PrintAndLog_FuncNameHeader("gasCostForEntireTrade = " + str(gasCostForEntireTrade))
    return int(gasCostAdder) + int(gasCostMultiplier * gasCostForEntireTrade)
