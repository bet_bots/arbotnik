from threading import Lock

NonceUsedDict = {}
Lock_NonceUsedDict = Lock()


def SetNonceUsed(address, nonce):
    global NonceUsedDict
    global Lock_NonceUsedDict

    Lock_NonceUsedDict.acquire()
    try:
        if address.lower() not in NonceUsedDict:
            # Set the new nonce
            NonceUsedDict[address.lower()] = int(nonce)
        else:
            # Set to the max of this new nonce and the current nonce
            NonceUsedDict[address.lower()] = max(NonceUsedDict[address.lower()], int(nonce))

    finally:
        Lock_NonceUsedDict.release()


def GetHighestKnownNonceUsedAndConfirmed(address):
    global NonceUsedDict
    global Lock_NonceUsedDict

    # I'm assuming that it only qualifies as used and confirmed if the transaction has been confirmed on the blockchain. If it's pending, it shouldn't be in here yet until it confirms
    Lock_NonceUsedDict.acquire()
    try:
        if address.lower() not in NonceUsedDict:
            return -1
        else:
            return int(NonceUsedDict[address.lower()])

    finally:
        Lock_NonceUsedDict.release()
