from Libraries.loggingConfig import PrintAndLog

from threading import Lock
import datetime

# Keyed by market's exchange
OrderbookCacheDataDict = {}
Lock_OrderbookCacheDataDict = Lock()


class OrderbookCacheData:
    exchange = None
    datetimeOfPrice = None
    price_basedOnBuyOrdersOnly = None
    price_basedOnSellOrdersOnly = None

    def __init__(self, _exchange, _datetimeOfPrice, _price_basedOnBuyOrdersOnly, _price_basedOnSellOrdersOnly):
        self.exchange = _exchange
        self.datetimeOfPrice = _datetimeOfPrice
        self.price_basedOnBuyOrdersOnly = _price_basedOnBuyOrdersOnly
        self.price_basedOnSellOrdersOnly = _price_basedOnSellOrdersOnly

    def GetDetails(self):
        roundNum_amount = 7
        return str(self.exchange.market.marketName.upper()) + " " + str(self.exchange.exchangeName) + ", dataAge_s = " + str(
            round(self.GetAge_s(), 0)) + ", price_basedOnBuyOrdersOnly = " + str(round(self.price_basedOnBuyOrdersOnly, roundNum_amount)) + ", price_basedOnSellOrdersOnly = " + str(
            round(self.price_basedOnSellOrdersOnly, roundNum_amount))

    def GetAge_s(self):
        return (datetime.datetime.now() - self.datetimeOfPrice).total_seconds()


def AddOrderbookCacheDataToList(orderbookCacheData):
    global OrderbookCacheDataDict
    global Lock_OrderbookCacheDataDict

    Lock_OrderbookCacheDataDict.acquire()
    try:
        if orderbookCacheData.exchange not in OrderbookCacheDataDict:
            OrderbookCacheDataDict[orderbookCacheData.exchange] = []

        OrderbookCacheDataDict[orderbookCacheData.exchange].insert(0, orderbookCacheData)

    finally:
        Lock_OrderbookCacheDataDict.release()


def GetOrderbookCacheDataList(exchange):
    global OrderbookCacheDataDict
    global Lock_OrderbookCacheDataDict

    returnValue = None
    Lock_OrderbookCacheDataDict.acquire()
    try:
        returnValue = OrderbookCacheDataDict[exchange]

    finally:
        Lock_OrderbookCacheDataDict.release()

    return returnValue


def RemoveOldOrderbookCacheDataFromList():
    global OrderbookCacheDataDict
    global Lock_OrderbookCacheDataDict

    returnValue = None
    Lock_OrderbookCacheDataDict.acquire()
    try:
        for exchange in OrderbookCacheDataDict:
            # PrintAndLog("RemoveOldOrderbookCacheDataFromList where OrderbookCacheDataDict[exchange] has a len of " + str(len(OrderbookCacheDataDict[exchange])))
            for orderbookCacheData in OrderbookCacheDataDict[exchange]:
                # PrintAndLog("RemoveOldOrderbookCacheDataFromList: considering a orderbookCacheData who's age_s = " + str(
                #     orderbookCacheData.GetAge_s()) + ". orderbookCacheData: " + str(orderbookCacheData.GetDetails()))
                # If it's gotten old
                if orderbookCacheData.GetAge_s() > (60 * 25):
                    # Remove the orderbookCacheData since it's old and we no longer care about it.
                    OrderbookCacheDataDict[exchange].remove(orderbookCacheData)
                    # PrintAndLog("Removing orderbookCacheData because it's old and we no longer care about it: " + str(
                    #     orderbookCacheData.GetDetails()) + ", OrderbookCacheDataDict[exchange] len now = " + str(len(OrderbookCacheDataDict[exchange])))

    finally:
        Lock_OrderbookCacheDataDict.release()

    return returnValue


def GetAnExchangesVolatility(exchange, time_seconds):
    # PrintAndLog("GetAnExchangesVolatility with exchange = " + str(exchange.exchangeName) + ", time_seconds = " + str(time_seconds))
    # Get an exchange's volatility over hte last time period
    # I'm considering volatility to be the max change in price during the time period.
    orderbookCacheDataList = GetOrderbookCacheDataList(exchange)

    if len(orderbookCacheDataList) <= 0:
        return None, None, None

    # Calculate the min and max for each price.
    max_price_basedOnBuyOrdersOnly = orderbookCacheDataList[0].price_basedOnBuyOrdersOnly
    max_price_basedOnSellOrdersOnly = orderbookCacheDataList[0].price_basedOnSellOrdersOnly
    min_price_basedOnBuyOrdersOnly = orderbookCacheDataList[0].price_basedOnBuyOrdersOnly
    min_price_basedOnSellOrdersOnly = orderbookCacheDataList[0].price_basedOnSellOrdersOnly

    thereIsEnoughDataToCover_time_seconds = False
    maxDataAge_s = 0
    for orderbookCacheData in orderbookCacheDataList:
        # PrintAndLog("looping time_seconds = " + str(time_seconds) + ", orderbookCacheData: " + str(orderbookCacheData.GetDetails()))
        # orderbookCacheData.price_basedOnBuyOrdersOnly
        # orderbookCacheData.price_basedOnSellOrdersOnly

        if orderbookCacheData.GetAge_s() > maxDataAge_s:
            maxDataAge_s = orderbookCacheData.GetAge_s()

        # Determine if there was enough data to go back far enough to cover the time_seconds
        if orderbookCacheData.GetAge_s() > time_seconds:
            thereIsEnoughDataToCover_time_seconds = True
            # PrintAndLog("Breaking because we've encountered data that's outside of our window")
            break

        # This data fits within the time window of what we're looking for, so let's analyze it
        else:
            # Check for new max buy
            if orderbookCacheData.price_basedOnBuyOrdersOnly > max_price_basedOnBuyOrdersOnly:
                max_price_basedOnBuyOrdersOnly = orderbookCacheData.price_basedOnBuyOrdersOnly

            # Check for new max sell
            if orderbookCacheData.price_basedOnSellOrdersOnly > max_price_basedOnSellOrdersOnly:
                max_price_basedOnSellOrdersOnly = orderbookCacheData.price_basedOnSellOrdersOnly

            # Check for new min buy
            if orderbookCacheData.price_basedOnBuyOrdersOnly < min_price_basedOnBuyOrdersOnly:
                min_price_basedOnBuyOrdersOnly = orderbookCacheData.price_basedOnBuyOrdersOnly

            # Check for new min sell
            if orderbookCacheData.price_basedOnSellOrdersOnly < min_price_basedOnSellOrdersOnly:
                min_price_basedOnSellOrdersOnly = orderbookCacheData.price_basedOnSellOrdersOnly

    # PrintAndLog("max_price_basedOnBuyOrdersOnly = " + str(max_price_basedOnBuyOrdersOnly))
    # PrintAndLog("max_price_basedOnSellOrdersOnly = " + str(max_price_basedOnSellOrdersOnly))
    # PrintAndLog("min_price_basedOnBuyOrdersOnly = " + str(min_price_basedOnBuyOrdersOnly))
    # PrintAndLog("min_price_basedOnSellOrdersOnly = " + str(min_price_basedOnSellOrdersOnly))
    # PrintAndLog("maxDataAge_s = " + str(maxDataAge_s))
    # PrintAndLog("thereIsEnoughDataToCover_time_seconds = " + str(thereIsEnoughDataToCover_time_seconds))

    # Find difference between min and max for each price
    volatilityPercent_basedOnBuyOrdersOnly = 1.0 - (min_price_basedOnBuyOrdersOnly / max_price_basedOnBuyOrdersOnly)
    volatilityPercent_basedOnSellOrdersOnly = 1.0 - (min_price_basedOnSellOrdersOnly / max_price_basedOnSellOrdersOnly)
    PrintAndLog("GetAnExchangesVolatility: " + str(exchange.market.marketName.upper()) + " " + str(exchange.exchangeName) + " volatilityPercent_basedOnBuyOrdersOnly = " + str(
        volatilityPercent_basedOnBuyOrdersOnly) + ", time_seconds = " + str(time_seconds))
    PrintAndLog("GetAnExchangesVolatility: " + str(exchange.market.marketName.upper()) + " " + str(exchange.exchangeName) + " volatilityPercent_basedOnSellOrdersOnly = " + str(
        volatilityPercent_basedOnSellOrdersOnly) + ", time_seconds = " + str(time_seconds))

    if volatilityPercent_basedOnBuyOrdersOnly < 0:
        raise Exception("This should never be negative")

    if volatilityPercent_basedOnSellOrdersOnly < 0:
        raise Exception("This should never be negative")

    return volatilityPercent_basedOnBuyOrdersOnly, volatilityPercent_basedOnSellOrdersOnly, thereIsEnoughDataToCover_time_seconds
