# rotate bits of number
INT_BITS = 256


def BitWiseRotateLeft(number, d):
    # Function to left rotate number by d bits
    # In number << d, last d bits are 0.
    # To put first 3 bits of number at
    # last, do bitwise or of n<<d
    # with number >> (INT_BITS - d)
    return (number << d) | (number >> (INT_BITS - d))


def BitWiseRotateRight(number, d):
    # Function to right rotate number by d bits
    # In number >> d, first d bits are 0.
    # To put last 3 bits of at
    # first, do bitwise or of n>>d
    # with number << (INT_BITS - d)
    return (number >> d) | (number << (INT_BITS - d)) & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
