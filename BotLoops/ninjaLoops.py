import copy
import sys
import threading
import time
import traceback
import datetime

import Exchanges.kyber
import Exchanges.ninja
import Exchanges.ninja_tailgating
import Exchanges.radarRelay2
import Exchanges.set
import Exchanges.uniswap
import Exchanges.uniswapV2
import Exchanges.sushiswap
import Exchanges.defiswap
import Exchanges.sakeswap
import Exchanges.zrxApi
import Exchanges.zrxV2
import Exchanges.zrxV4
import Exchanges.balancer
import Exchanges.keeperDAO
import Exchanges.hidingBook
import Libraries.cache
import Libraries.core
import Libraries.gasStation
import Libraries.loggingConfig
import Libraries.network
import Libraries.nodes
import Libraries.orderAggregator
import Libraries.pendingTransactionMonitor
import Libraries.priceOracle
import Libraries.utils
import Libraries.node_geth
import Libraries.blockNative
import Libraries.alchemy
import Libraries.bloxroute
import Libraries.tokenTransferCost
import Libraries.processCommunication
import Libraries.quoteTokenProcesses
import Libraries.ratesGraph
import Libraries.config
from Exchanges.ninja_arbitrage import CleanUpOldPaths_PursueTradeOpportunityPathsDict
from Libraries.arbitrageOpportunityHistory import RemoveOldItemsIn_ArbitrageOpportunitysPerBlockDict
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader
from Libraries.accounts import TokenDict_Ninja
from Libraries.exchanges import ExchangeName_0xMesh, ExchangeName_RadarRelay2, EnforceTokenAddressIsERC20Version, ExchangeName_HidingBook
from Libraries.media import GetAsciiArt
from Libraries.messages import ConsiderPostingMessageSayingWeHadAnException
from Libraries.ninjaOps import RebalanceNinjaOpAccount
from Libraries.ninjaUtils import PopulateTokenDict_DaiAndSai_BugFix, PopulateTokenDict_NinjaBetweenBancorAnd0x, PopulateTokenDict_NinjaBetweenBancorAndKyber, \
    PopulateTokenDict_NinjaBetween0xAndKyber, PopulateTokenDict_NinjaBetweenUniswapAndKyber, PopulateTokenDict_Ninja_Set, \
    PopulateTokenDict_Consider_Ninja_DeleteAllTokensExceptForThoseInThisList, PopulateTokenDict_Ninja_Generic, Approves_Generic, SetMemoryCacheFromFileCache_AllExchanges, \
    RestrictMemoryCacheTokensBasedOn_TokenDict_Ninja
from Libraries.stateManager import RemoveFile_GracefullyKill, ConsiderGracefullyKillingTheScript
from Libraries.core import API_PostOperatorNotification
from Libraries.executeOnInterval import IsTimeToExecute


def NinjaLoop():
    import Libraries.defaults
    from Libraries.accounts import NinjaOpAccountDict
    import BotEvents.ninjaEvents

    PrintAndLog_FuncNameHeader("Loading Step 0. Checking run modes")

    if Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        PrintAndLog_FuncNameHeader(GetAsciiArt("spaceship.txt"))
        PrintAndLog_FuncNameHeader("Ninja is about to travel through time")
        PrintAndLog_FuncNameHeader("3 - Disabling mempool")
        time.sleep(1)
        PrintAndLog_FuncNameHeader("2 - Disabling exchanges we don't support")
        time.sleep(1)
        PrintAndLog_FuncNameHeader("1")
        time.sleep(1)
        PrintAndLog_FuncNameHeader("Blastoff")
        time.sleep(1)

        # We must set RemoteNodeList_QuoteTokenProcessSelection since our normal methods aren't being called because of TimeMachine_EnabledForHistoricBlockDebug
        Libraries.nodes.RemoteNodeList_QuoteTokenProcessSelection = copy.deepcopy(Libraries.nodes.RemoteNodeList)

        # We must disable these features in order for TimeMachine_EnabledForHistoricBlockDebug to work properly
        Libraries.defaults.EnableOrderAggregator_0xMesh = False
        Libraries.defaults.EnableOrderAggregator_HidingBook = False
        Libraries.defaults.EnableMempool = False
        Libraries.defaults.EnableNinja_TradeExecution = False

        # We must disable certain exchanges in order for TimeMachine_EnabledForHistoricBlockDebug to work properly
        # 0x uses an offchain orderbook. We cannot use time machine for it since it does not get price data from the blockchain
        if ExchangeName_0xMesh in Libraries.exchanges.ExchangeDict:
            del Libraries.exchanges.ExchangeDict[ExchangeName_0xMesh]

    if Libraries.config.ActiveConfig != Libraries.config.Config.Hide:
        # Do not trade on ExchangeName_HidingBook outside of the HidingGame
        PrintAndLog_FuncNameHeader("Deleting HidingBook from ExchangeDict because we're not running the HidingGame")
        del Libraries.exchanges.ExchangeDict[ExchangeName_HidingBook]

    PrintAndLog_FuncNameHeader("Loading Step 1")
    Libraries.utils.SetCPUCount()
    Libraries.gasStation.ConsiderGettingRecommendedGasPrices()
    if not Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        Libraries.core.ConsiderUpdatingLatestBlockNumber()

    PrintAndLog_FuncNameHeader("Loading Step 2")
    if not Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        # Connect to nodes via websockets for new block head event
        for remoteNode in Libraries.nodes.RemoteNodeWebsocketsList:
            Libraries.node_geth.ConnectWebSocketClient(remoteNode)

        if Libraries.defaults.EnableBloxrouteWebsockets:
            # Note, do not subscribeToMempool here since that's to much on the main process. A sub process will be doing that
            Libraries.bloxroute.ConnectToAllWebSocketClients()

        Libraries.cache.RegisterAddressesInTransactionCountCache_AllTradingAddresses()

    # Remove the kill file since we are starting the script
    RemoveFile_GracefullyKill()

    Exchanges.zrxV2.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.zrxV4.SetNetwork(Libraries.network.Network.mainnet)
    Exchanges.set.SetNetwork(Libraries.network.Network.mainnet)

    Libraries.priceOracle.StartService()

    # Connect to mempool listening services
    PrintAndLog_FuncNameHeader("Loading Step 3 - Mempool")
    if not Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        if Libraries.defaults.EnableMempool and Libraries.defaults.EnableMempool_BlockNative:
            Libraries.blockNative.CreateWebhookListener()
        if Libraries.defaults.EnableMempool and Libraries.defaults.EnableMempool_Alchemy:
            Libraries.alchemy.ConnectWebSocketClient()

    PrintAndLog_FuncNameHeader("Loading Step 4 - Populate cache, quick load")
    # Get and cache data we need from various DXs

    # Short calls, make them all the time
    Libraries.cache.GetAndCache_RadarRelay_API_GetListOfTokenAddresses()
    Libraries.cache.GetAndCache_Kyber_API_GetListOfTokenAddresses()
    Libraries.cache.GetAndCache_Bancor_API_GetAllCurrencies()
    Libraries.cache.GetAndCache_0xApi_API_GetListOfTokenAddresses()
    Libraries.cache.GetAndCache_HidingBook_API_GetListOfTokenAddresses()
    Libraries.cache.GetAndCache_Curve_API_GetExchangeDataDict()

    PrintAndLog_FuncNameHeader("Loading Step 5 - Populate cache, long load")
    if Libraries.defaults.ConsiderUpdatingCachesBeforeBotLoop:
        # Long calls only need to be called once in a while
        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.uniswap.API_GetExchangeDataDict')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_Uniswap_API_GetExchangeDataDict()

        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.kyber.GetTradeableReserves')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_Kyber_GetTradeableReserves()

        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.uniswapV2.API_GetExchangeDataDict')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_UniswapV2_API_GetExchangeDataDict()

        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.sushiswap.API_GetExchangeDataDict')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_Sushiswap_API_GetExchangeDataDict()

        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.defiswap.API_GetExchangeDataDict')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_Defiswap_API_GetExchangeDataDict()

        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.sakeswap.API_GetExchangeDataDict')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_Sakeswap_API_GetExchangeDataDict()

        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.balancer.API_GetExchangeDataDict')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_Balancer_API_GetExchangeDataDict()

        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Exchanges.curve.API_GetExchangeDataDict')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_Curve_API_GetExchangeDataDict()

        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days('Set_RebalancingSets')
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_Set_RebalancingSets()

    PrintAndLog_FuncNameHeader("Loading Step 6 - Populate cache, finalize")
    if Libraries.defaults.EnableSet:
        Exchanges.set.PopulateSetTokenDataDict()

    SetMemoryCacheFromFileCache_AllExchanges()

    PrintAndLog_FuncNameHeader("Loading Step 7 - Initializing tokens")

    PopulateTokenDict_DaiAndSai_BugFix()

    # PopulateTokenDict_NinjaBetweenBancorAnd0x()
    # PopulateTokenDict_NinjaBetweenBancorAndKyber()
    # PopulateTokenDict_NinjaBetween0xAndKyber()
    # PopulateTokenDict_NinjaBetweenUniswapAndKyber()
    # PopulateTokenDict_Ninja_Set()
    #
    # PopulateTokenDict_Consider_Ninja_DeleteAllTokensExceptForThoseInThisList()

    PopulateTokenDict_DaiAndSai_BugFix()
    PopulateTokenDict_Ninja_Generic()
    PopulateTokenDict_Consider_Ninja_DeleteAllTokensExceptForThoseInThisList()

    RestrictMemoryCacheTokensBasedOn_TokenDict_Ninja()

    PrintAndLog_FuncNameHeader("TokenDict_Ninja of len " + str(len(TokenDict_Ninja)) + " = " + str(TokenDict_Ninja))
    for symbol in TokenDict_Ninja:
        PrintAndLog_FuncNameHeader("Token_Ninja object " + str(symbol) + " " + str(TokenDict_Ninja[symbol].erc20TokenContractAddress))

    # Populate the RatesGraphTokenList and empty RatesGraphs here at startup, then when we want to create a new graph just copy the empty
    Libraries.ratesGraph.PopulateRatesGraphTokenList()
    Libraries.ratesGraph.Set_HighestInitializedRatesGraphBlockNumber_BasedOnLatestBlockNumber()
    Libraries.ratesGraph.InitializeRatesGraphsForNearFutureBlocks()

    PrintAndLog_FuncNameHeader("Loading Step 7.1 - Starting processCommunication server")
    Libraries.processCommunication.StartServer_InBackgroundThread()

    PrintAndLog_FuncNameHeader("Loading Step 7.5 - Populate token gas cost cache, long load")
    if Libraries.defaults.ConsiderUpdatingCachesBeforeBotLoop:
        timeSinceLastModified_days = Libraries.cache.GetFileTimeSinceLastModified_Days("API_GetTokenTransferCost")
        if not timeSinceLastModified_days or timeSinceLastModified_days > 1.0:
            Libraries.cache.GetAndCache_API_GetTokenTransferCost()

    PrintAndLog_FuncNameHeader("Loading Step 7.6 - Populate token gas cost cache, finalize")
    Libraries.tokenTransferCost.TokenTransferGasCostDict = Libraries.cache.GetDataFromCache("API_GetTokenTransferCost")

    # Create the NinjaInstance object
    Exchanges.ninja.NinjaInstance_Generic = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.Generic)
    # TODO, DEPRECATED
    Exchanges.ninja.NinjaInstance_A = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.A)
    Exchanges.ninja.NinjaInstance_B = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.B)
    Exchanges.ninja.NinjaInstance_C = Exchanges.ninja.CreateNinjaObject(Exchanges.ninja.NinjaType.C)

    PrintAndLog_FuncNameHeader("Loading Step 8.1 - Generate HidingBook tokenList, GenerateHidingBookTokenListOnStartUp = " +
                               str(Libraries.defaults.GenerateHidingBookTokenListOnStartUp))
    if Libraries.defaults.GenerateHidingBookTokenListOnStartUp:
        Libraries.cache.GetAndCache_HidingBook_GenerateTokenList()

        if Libraries.defaults.GenerateHidingBookTokenListOnStartUp_ExitAfterApproves:
            sys.exit()

    PrintAndLog_FuncNameHeader("Loading Step 8.2 - Approves, IssueApprovesOnStartup = " + str(Libraries.defaults.IssueApprovesOnStartup))
    if not Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        if Libraries.defaults.IssueApprovesOnStartup:
            try:
                Approves_Generic()

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception when Approves_Generic, " + traceback.format_exc())

            if Libraries.defaults.IssueApprovesOnStartup_ExitAfterApproves:
                sys.exit()

    if Libraries.defaults.Ninja_VerifyThatTokenDictIsSetCorrectly:
        # Debug for verifying that all markets are correct
        for tokenName in TokenDict_Ninja:
            PrintAndLog_FuncNameHeader("TokenDict_Ninja has token " + str(tokenName) + ", " + str(TokenDict_Ninja[tokenName].erc20TokenContractAddress))

        sys.exit()

    PrintAndLog_FuncNameHeader("Loading Step 9 - 0x websockets")
    if not Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
        quoteTokensList_Erc20Format = Exchanges.keeperDAO.GetBorrowableQuoteTokensList(True)
        if Libraries.defaults.EnableOrderAggregator_0xMesh:
            exchangeName = ExchangeName_0xMesh
            for quoteToken in quoteTokensList_Erc20Format:
                Libraries.orderAggregator.CreateOrderAggregator_BasedOnTokenDict_Ninja(exchangeName, quoteToken.lower())
            Exchanges.zrxApi.ConnectWebSocketClient(exchangeName)

        if Libraries.defaults.EnableOrderAggregator_HidingBook:
            exchangeName = ExchangeName_HidingBook
            for quoteToken in quoteTokensList_Erc20Format:
                Libraries.orderAggregator.CreateOrderAggregator_BasedOnTokenDict_Ninja(exchangeName, quoteToken.lower())
            Exchanges.hidingBook.ConnectWebSocketClient(exchangeName)

        # if EnableOrderAggregator_RadarRelay:
        #     exchangeName = ExchangeName_RadarRelay2
        #     for quoteToken in quoteTokensList_Erc20Format:
        #         Libraries.orderAggregator.CreateOrderAggregator_BasedOnTokenDict_Ninja(exchangeName, quoteToken.lower())
        #     Exchanges.radarRelay2.Set_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses(Libraries.orderAggregator.GetOrderAggregator_BasedOnTokenDict_Ninja)
        #     Exchanges.radarRelay2.ConnectWebSocketClient(exchangeName)

        # ******* BEGIN Testing websockets ******* #
        # The purpose of this is to test websockets for debug purposes
        # I'm putting this test here right in the ninjaLoop because this already has the ninja initialization completed
        while Libraries.defaults.Test0xWebsocketsAndOrderAggregatorsOnly:
            exchangeName = ExchangeName_0xMesh
            # exchangeName = ExchangeName_HidingBook

            # ******* Begin Manually pole 0x orderbooks ******* #

            # if EnableOrderAggregator_RadarRelay:
            #     IsTimeToExecute("UpdateOrderAggregator_GivenTokens_RadarRelay", Libraries.defaults.Interval_seconds_ManuallyPoleOrderbooks_0xMesh,
            #                     Libraries.orderAggregator.UpdateOrderAggregator_GivenTokens_RadarRelay, True)

            if Libraries.defaults.EnableOrderAggregator_0xMesh:
                IsTimeToExecute("UpdateAllOrderAggregators_ByPollingOrderbooks_0xMesh", Libraries.defaults.Interval_seconds_ManuallyPoleOrderbooks_0xMesh,
                                Libraries.orderAggregator.UpdateAllOrderAggregators_ByPollingOrderbooks_0xMesh, True)

            if Libraries.defaults.EnableOrderAggregator_HidingBook:
                IsTimeToExecute("UpdateAllOrderAggregators_ByPollingOrderbooks_HidingBook", Libraries.defaults.Interval_seconds_ManuallyPoleOrderbooks_HidingBook,
                                Libraries.orderAggregator.UpdateAllOrderAggregators_ByPollingOrderbooks_HidingBook, True)

            if Libraries.defaults.EnableOrderAggregator_0xMesh or Libraries.defaults.EnableOrderAggregator_HidingBook:
                IsTimeToExecute("PruneAllOrderAggregators_0x_Slow", Libraries.defaults.Interval_seconds_PruneAllOrderAggregators_0x_Slow,
                                Libraries.orderAggregator.PruneAllOrderAggregators_0x, True)

            # ******* End Manually pole 0x orderbooks ******* #

            time.sleep(10)

            try:
                quoteToken = Exchanges.zrxV2.Contract_WETH
                # quoteToken = Exchanges.keeperDAO.EthTokenContract
                token = TokenDict_Ninja['usdc']
                # quoteToken = Exchanges.keeperDAO.Contract_USDC
                # token = TokenDict_Ninja['weth']
                # quoteToken = Exchanges.keeperDAO.Contract_USDC
                # token = TokenDict_Ninja['dai']
                # quoteToken = Exchanges.keeperDAO.Contract_DAI
                # token = TokenDict_Ninja['usdc']

                orderDict_bids_0x = token.GetCopyOfAllOrders_Bids_FromTheseOrderAggregators(quoteToken, True, [exchangeName])
                orderDict_asks_0x = token.GetCopyOfAllOrders_Asks_FromTheseOrderAggregators(quoteToken, True, [exchangeName])
                PrintAndLog_FuncNameHeader("token " + str(token.tokenName) + " orderDict_bids_0x of len " + str(len(orderDict_bids_0x)) + " = " + str(orderDict_bids_0x))
                PrintAndLog_FuncNameHeader("token " + str(token.tokenName) + " orderDict_asks_0x of len " + str(len(orderDict_asks_0x)) + " = " + str(orderDict_asks_0x))

                # if EnableOrderAggregator_RadarRelay:
                #     orders_bids_RadarRelay2 = token.OrderAggregatorDict[ExchangeName_RadarRelay2][EnforceTokenAddressIsERC20Version(quoteToken).lower()].GetCopyOfAllOrders_Bids()
                #     orders_asks_RadarRelay2 = token.OrderAggregatorDict[ExchangeName_RadarRelay2][EnforceTokenAddressIsERC20Version(quoteToken).lower()].GetCopyOfAllOrders_Asks()
                #     PrintAndLog_FuncNameHeader("token " + str(token.tokenName) + " orders_bids_RadarRelay2 of len " + str(
                #         len(orders_bids_RadarRelay2)) + " = " + str(orders_bids_RadarRelay2))
                #     PrintAndLog_FuncNameHeader("token " + str(token.tokenName) + " orders_asks_RadarRelay2 of len " + str(
                #         len(orders_asks_RadarRelay2)) + " = " + str(orders_asks_RadarRelay2))

                if Libraries.defaults.EnableOrderAggregator_0xMesh:
                    orders_bids_0xMesh = token.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()].GetCopyOfAllOrders_Bids()
                    orders_asks_0xMesh = token.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()].GetCopyOfAllOrders_Asks()
                    PrintAndLog_FuncNameHeader("token " + str(token.tokenName) + " orders_bids_0xMesh of len " +
                                               str(len(orders_bids_0xMesh)) + " = " + str(orders_bids_0xMesh))
                    PrintAndLog_FuncNameHeader("token " + str(token.tokenName) + " orders_asks_0xMesh of len " +
                                               str(len(orders_asks_0xMesh)) + " = " + str(orders_asks_0xMesh))

                if Libraries.defaults.EnableOrderAggregator_HidingBook:
                    orders_bids_HidingBook = token.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()].GetCopyOfAllOrders_Bids()
                    orders_asks_HidingBook = token.OrderAggregatorDict[exchangeName][EnforceTokenAddressIsERC20Version(quoteToken).lower()].GetCopyOfAllOrders_Asks()
                    PrintAndLog_FuncNameHeader("token " + str(token.tokenName) + " orders_bids_HidingBook of len " +
                                               str(len(orders_bids_HidingBook)) + " = " + str(orders_bids_HidingBook))
                    PrintAndLog_FuncNameHeader("token " + str(token.tokenName) + " orders_asks_HidingBook of len " +
                                               str(len(orders_asks_HidingBook)) + " = " + str(orders_asks_HidingBook))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("Exception: " + traceback.format_exc())

    PrintAndLog_FuncNameHeader("Loading Step Complete")
    # Set the loading complete flag so that events know they can now start processing events
    BotEvents.ninjaEvents.LoadingComplete_YouCanNowProcessEvents = True

    Libraries.utils.DateTimeThatNinjaLoopLoadingCompleted = datetime.datetime.now()

    # ******* END Testing websockets ******* #
    # Making a performance modification to this loop
    # Rather than just spam calling my NinjaBetween functions every ~3 seconds
    # I'm going to spam call API_GetLatestBlockNumber hard and when the new block is mined in i'll then immediately call my NinjaBetween functions
    # Then there's no reason to call them again until there is a new block... right?
    # Is there any reason to call it a second time during this block?  Maybe in case it failed the first time for some reason which could happen
    # To do this optimization i'll need to split up my main loop below, I don't want to run everything every loop
    firstRun = True
    dateTimeOfLast_Increment_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_ToSimulateNewBlocksBeingMined = datetime.datetime.now()
    while Libraries.defaults.EnterMainLoop:
        ConsiderGracefullyKillingTheScript()

        try:
            # PrintAndLog_FuncNameHeader(str(datetime.datetime.now()) + "--------------------- Begin loop ---------------------------------------------------------------")

            # Not calling this anymore because i'm getting alerted of new blocks via websockets now
            # Libraries.core.API_GetLatestBlockNumber()
            if Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug and \
                    Libraries.defaults.Increment_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_ToSimulateNewBlocksBeingMined:
                if not dateTimeOfLast_Increment_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_ToSimulateNewBlocksBeingMined:
                    dateTimeOfLast_Increment_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_ToSimulateNewBlocksBeingMined = datetime.datetime.now()
                else:
                    duration_s = (datetime.datetime.now() - dateTimeOfLast_Increment_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_ToSimulateNewBlocksBeingMined).total_seconds()
                    if duration_s > 11:
                        # Simulate a new block by incrementing the historic block number
                        dateTimeOfLast_Increment_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_ToSimulateNewBlocksBeingMined = datetime.datetime.now()
                        Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug += 1
                        if Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug > \
                                Libraries.defaults.StopIncrementing_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_EndBlock:
                            PrintAndLog_FuncNameHeader(GetAsciiArt("cake.txt"))
                            PrintAndLog_FuncNameHeader("We've returned from the past and brought cake")
                            sys.exit()

                        Libraries.core.SimulateLatestBlockNumberChange(Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug)
                        PrintAndLog_FuncNameHeader("New simulated block mined in, " + str(Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug))

                        # Call NinjaEvent_NewBlock since we have a new simulated block
                        t = threading.Thread(target=BotEvents.ninjaEvents.NinjaEvent_NewBlock, args=(Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug,))
                        t.start()

            # PrintAndLog_FuncNameHeader("firstRun = " + str(firstRun) + ", GetAgeOfLatestBlockNumber_int() = " + str(
            #     Libraries.core.GetAgeOfLatestBlockNumber_int()) + ", NinjaThreshold_TradingFunctionCalls_seconds = " + str(
            #     NinjaThreshold_TradingFunctionCalls_seconds) + ", NinjaThreshold_NonTradingFunctionCalls_seconds = " + str(
            #     NinjaThreshold_NonTradingFunctionCalls_seconds))

            # We don't want these low priority function calls to spam the node when the trading function calls are being made
            # This condition helps alleviate that possibility
            isTimeToCallLowPriorityNinjaLoopFunctions = None

            ageOfLatestBlockNumber_int = None
            # Set ageOfLatestBlockNumber_int based on whether or not we're looking at historic data or real time data
            if Libraries.defaults.TimeMachine_EnabledForHistoricBlockDebug:
                ageOfLatestBlockNumber_int = \
                    (datetime.datetime.now() - dateTimeOfLast_Increment_TimeMachine_EnabledForHistoricBlockDebugEverySoOften_ToSimulateNewBlocksBeingMined).total_seconds()
            else:
                ageOfLatestBlockNumber_int = Libraries.core.GetAgeOfLatestBlockNumber_int()

            goodTimeToMakeLowPriorityCalls = \
                Libraries.defaults.NinjaThreshold_TradingFunctionCalls_seconds < ageOfLatestBlockNumber_int < Libraries.defaults.NinjaThreshold_NonTradingFunctionCalls_seconds

            if firstRun or goodTimeToMakeLowPriorityCalls:
                firstRun = False
                message = "Eligible to call only non NinjaBetween functions and general low priority functions"
                if IsTimeToExecute(message, 1):
                    PrintAndLog_FuncNameHeader(message)

                t = threading.Thread(target=Libraries.gasStation.ConsiderGettingRecommendedGasPrices, args=())
                t.start()

                t = threading.Thread(target=Libraries.gasStation.ConsiderGettingBancorsMaxGasPriceValue, args=())
                t.start()

                t = threading.Thread(target=Libraries.gasStation.ConsiderGettingKybersMaxGasPriceValue, args=())
                t.start()

                # I shouldn't need to call this on every single set too often. It should only be often on the ones that are rebalancing
                # TODO, make these calls to a lower priority node... They are very node consuming and will slow down the node
                if Libraries.defaults.EnableSet:
                    IsTimeToExecute("UpdateAllSetTokenData", 210, Exchanges.set.UpdateAllSetTokenData, True)
                    IsTimeToExecute("CalculateAllSetsAverageTimeBetweenPriceChanges", 240, Exchanges.set.CalculateAllSetsAverageTimeBetweenPriceChanges, True)

                IsTimeToExecute("Exchanges.ninja.NinjaInstance_A.UpdateBalances", 30, Exchanges.ninja.NinjaInstance_A.UpdateBalances, True)
                IsTimeToExecute("Exchanges.ninja.NinjaInstance_B.UpdateBalances", 30, Exchanges.ninja.NinjaInstance_B.UpdateBalances, True)
                IsTimeToExecute("Exchanges.ninja.NinjaInstance_C.UpdateBalances", 30, Exchanges.ninja.NinjaInstance_C.UpdateBalances, True)
                IsTimeToExecute("Exchanges.ninja.NinjaInstance_Generic.UpdateBalances", 30, Exchanges.ninja.NinjaInstance_Generic.UpdateBalances, True)

                IsTimeToExecute("Libraries.accounts.ReleaseReservableEthereumAccountPoolAccounts_ThatHaveBeenInUseForTooLong", 20,
                                Libraries.accounts.ReleaseReservableEthereumAccountPoolAccounts_ThatHaveBeenInUseForTooLong, True)

                IsTimeToExecute("CleanUpOldPaths_PursueTradeOpportunityPathsDict", 200, CleanUpOldPaths_PursueTradeOpportunityPathsDict, True)

                # ******* Begin Manually pole 0x orderbooks ******* #

                # if EnableOrderAggregator_RadarRelay:
                #     IsTimeToExecute("UpdateOrderAggregator_GivenTokens_RadarRelay", Libraries.defaults.Interval_seconds_ManuallyPoleOrderbooks_0xMesh,
                #                     Libraries.orderAggregator.UpdateOrderAggregator_GivenTokens_RadarRelay, True)

                if Libraries.defaults.EnableOrderAggregator_0xMesh:
                    IsTimeToExecute("UpdateAllOrderAggregators_ByPollingOrderbooks_0xMesh", Libraries.defaults.Interval_seconds_ManuallyPoleOrderbooks_0xMesh,
                                    Libraries.orderAggregator.UpdateAllOrderAggregators_ByPollingOrderbooks_0xMesh, True)

                if Libraries.defaults.EnableOrderAggregator_HidingBook:
                    IsTimeToExecute("UpdateAllOrderAggregators_ByPollingOrderbooks_HidingBook", Libraries.defaults.Interval_seconds_ManuallyPoleOrderbooks_HidingBook,
                                    Libraries.orderAggregator.UpdateAllOrderAggregators_ByPollingOrderbooks_HidingBook, True)

                if Libraries.defaults.EnableOrderAggregator_0xMesh or Libraries.defaults.EnableOrderAggregator_HidingBook:
                    # GetOrderInfos is a very slow call and should be called less frequently
                    doCheck_orderInfos = True
                    doCheck_balances = False
                    doCheck_allowances = True
                    functionArguments = doCheck_orderInfos, doCheck_balances, doCheck_allowances
                    IsTimeToExecute("PruneAllOrderAggregators_0x_Slow", Libraries.defaults.Interval_seconds_PruneAllOrderAggregators_0x_Slow,
                                    Libraries.orderAggregator.PruneAllOrderAggregators_0x, True, True, False, *functionArguments)

                    # GetBalances is a faster call and can be called more frequently
                    # This is awkward, ideally I'd update the 0x OrderAggregator maker balances in NinjaEvent before calling ArbitrageGeneric,
                    # but I do not want to delay ArbitrageGeneric at all
                    # And since ArbitrageGeneric needs to pass the OrderAggregator onto the sub processes immediately,
                    # I would have to wait for the APi call to complete before the sub process can run. I do not want to delay the sub processes
                    # It's probably best to just update the 0x OrderAggregator maker balances in NinjaLoops every n seconds instead of updating it in NinjaEvents
                    doCheck_orderInfos = False
                    doCheck_balances = True
                    doCheck_allowances = False
                    functionArguments = doCheck_orderInfos, doCheck_balances, doCheck_allowances
                    IsTimeToExecute("PruneAllOrderAggregators_0x_Fast", Libraries.defaults.Interval_seconds_PruneAllOrderAggregators_0x_Fast,
                                    Libraries.orderAggregator.PruneAllOrderAggregators_0x, True, True, False, *functionArguments)

                # ******* End Manually pole 0x orderbooks ******* #

                IsTimeToExecute("RebalanceNinjaOpAccount", 7200, RebalanceNinjaOpAccount, True)

                IsTimeToExecute("Exchanges.ninja.UpdateGasTokenProperties", 300, Exchanges.ninja.UpdateGasTokenProperties, True)

                IsTimeToExecute("UpdateManuallyAddedTradeAmountArrayDict_BasedOnPriceOracle", 200,
                                Exchanges.keeperDAO.UpdateManuallyAddedTradeAmountArrayDict_BasedOnPriceOracle, True)

                IsTimeToExecute("Exchanges.ninja.RemoveOldPricesDicts", 140, Exchanges.ninja.RemoveOldPricesDicts, True)

                IsTimeToExecute("RemoveOldItemsIn_ArbitrageOpportunitysPerBlockDict", 311, RemoveOldItemsIn_ArbitrageOpportunitysPerBlockDict, True)

                IsTimeToExecute("Libraries.ratesGraph.InitializeRatesGraphsForNearFutureBlocks_ThenCall_RemoveOldRatesGraphs",
                                Libraries.defaults.Interval_seconds_InitializeRatesGraphsForNearFutureBlocks,
                                Libraries.ratesGraph.InitializeRatesGraphsForNearFutureBlocks_ThenCall_RemoveOldRatesGraphs, True)

                IsTimeToExecute("Libraries.processCommunication.RemoveOldResultsFromResultIdDict", 220,
                                Libraries.processCommunication.RemoveOldResultsFromResultIdDict, True)

            if Libraries.utils.ActiveScriptName != Libraries.utils.ScriptName.NinjaTail:
                # We aren't in NinjaTail mode, so do not tailgate
                pass
            elif not Libraries.defaults.EnableNinja_Tailgating:
                message = "EnableNinja_Tailgating is disabled, skipping ninja tailgating features"
                if IsTimeToExecute(message, 600):
                    API_PostOperatorNotification(message)
            else:
                # Run this only once because it has its own built in loop,
                # Putting a massive number in for the time interval to prevent it from starting more than once
                # IsTimeToExecute("Exchanges.ninja_tailgating.TailgateMemPool_Uniswap", 999999999999999,
                #                 Exchanges.ninja_tailgating.TailgateMemPool_Uniswap, True)
                IsTimeToExecute("TailgateMemPool", 999999999999999, Exchanges.ninja_tailgating.TailgateMemPool, True, True, True)

            if Libraries.utils.ActiveScriptName != Libraries.utils.ScriptName.NinjaArb:
                # We aren't in NinjaArb mode, so do not arbitrage
                pass

            elif not Libraries.defaults.EnableNinja_Arbitrage:
                message = "EnableNinja_Loops is disabled, skipping all ninja loop trading features"
                if IsTimeToExecute(message, 600):
                    API_PostOperatorNotification(message)

            else:
                pass
                # NinjaBetween functions that run all the time regardless of when blocks are mined
                # TODO, I don't yet know if this is working on the new keeperDAO LP
                # IsTimeToExecute("NinjaBetweenSetAnd0x_Config_A", Interval_seconds_NinjaBetweenSetAnd0x, NinjaBetweenSetAnd0x_Config_A, True, True, True)
                # IsTimeToExecute("NinjaBetweenSetAnd0x_Config_B", Interval_seconds_NinjaBetweenSetAnd0x, NinjaBetweenSetAnd0x_Config_B, True, True, True)
                # IsTimeToExecute("NinjaBetweenSetAnd0x_Config_C", Interval_seconds_NinjaBetweenSetAnd0x, NinjaBetweenSetAnd0x_Config_C, True, True, True)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception = " + traceback.format_exc())
            ConsiderPostingMessageSayingWeHadAnException(traceback.format_exc())
            pass

        # PrintAndLog_FuncNameHeader(str(datetime.datetime.now()) + "--------------------- End loop ---------------------------------------------------------------")

        # Randomizing some sleeps to make it look more human
        if Libraries.defaults.LongSleepForDebug_Ninja:
            Libraries.core.SleepRandomTimeBetween_Seconds(2, 3, True)
        else:
            Libraries.core.SleepRandomTimeBetween_Float(0.1, 0.2)

    PrintAndLog_FuncNameHeader(str(datetime.datetime.now()) +
                               "--------------------- Exiting Ninja loop, this should never happen ---------------------------------------------------------------")
