import json

from Exchanges.ninja import Contract_Ninja_A, Contract_Ninja_B, Contract_Ninja_C, Contract_NinjaUtility
from Libraries.nodes import Instance_Web3

Subdirectory_Default = 'Contracts/'
Subdirectory_NinjaABI = 'Contracts/Ninja/ABI/'
Subdirectory_KeeperDAOABI = 'Contracts/KeeperDAO/ABI/'


def LoadContractABI(abiFileName, subdirectory=Subdirectory_Default):
    with open(subdirectory + abiFileName, 'r') as abi_definition:
        return json.load(abi_definition)


def LoadContract(address, abi):
    return Instance_Web3.eth.contract(address=address, abi=abi)


ABI_EtherDelta = LoadContractABI("etherDelta.json")
Contract_EtherDelta = LoadContract(None, ABI_EtherDelta)

ABI_OasisDex = LoadContractABI("oasisDex.json")
Contract_OasisDex = LoadContract(None, ABI_OasisDex)

ABI_ArbyUtility = LoadContractABI("arbyUtility_4.json")
Contract_ArbyUtility = LoadContract(
    Instance_Web3.toChecksumAddress("0xc254709e9750637757916056ba106dd207269876"), ABI_ArbyUtility)

ABI_Erc20 = LoadContractABI("erc20.json")
Contract_Erc20 = LoadContract(None, ABI_Erc20)

ABI_0x = LoadContractABI("0x.json")
Contract_0x = LoadContract(None, ABI_0x)

ABI_0xv3 = LoadContractABI("0xv3.json")
Contract_0xv3 = LoadContract(None, ABI_0xv3)

Contract_0xv4 = LoadContract(None, LoadContractABI("0xv4.json"))

ABI_KyberNetworkProxy = LoadContractABI("kyberNetworkProxy.json")
Contract_KyberNetworkProxy = LoadContract(None, ABI_KyberNetworkProxy)

ABI_KyberNetwork = LoadContractABI("kyberNetwork.json")
Contract_KyberNetwork = LoadContract(None, ABI_KyberNetwork)

ABI_KyberReserve = LoadContractABI("kyberReserve.json")
Contract_KyberReserve = LoadContract(None, ABI_KyberReserve)

ABI_KyberReserveConversionRates = LoadContractABI("kyberReserveConversionRates.json")
Contract_KyberReserveConversionRates = LoadContract(None, ABI_KyberReserveConversionRates)

ABI_Ninja_A = LoadContractABI("ninja_A_14.json")
Contract_Ninja_A = LoadContract(Contract_Ninja_A, ABI_Ninja_A)

ABI_Ninja_B = LoadContractABI("ninja_B_14.json")
Contract_Ninja_B = LoadContract(Contract_Ninja_B, ABI_Ninja_B)

ABI_Ninja_C = LoadContractABI("ninja_C_12.json")
Contract_Ninja_C = LoadContract(Contract_Ninja_C, ABI_Ninja_C)

ABI_NinjaUtility = LoadContractABI("ninja_utility_7.json")
Contract_NinjaUtility = LoadContract(Contract_NinjaUtility, ABI_NinjaUtility)

ABI_UniswapExchange_dai = LoadContractABI("uniswap_exchange.json")
Contract_UniswapExchange_dai = LoadContract(None, ABI_UniswapExchange_dai)

ABI_UniswapFactory = LoadContractABI("uniswap_factory.json")
Contract_UniswapFactory = LoadContract(None, ABI_UniswapFactory)

Contract_UniswapV2_Router = LoadContract(
    Instance_Web3.toChecksumAddress("0x7a250d5630b4cf539739df2c5dacb4c659f2488d"),
    LoadContractABI("uniswapV2_router.json"))

Contract_UniswapV2_Router_Old = LoadContract(
    Instance_Web3.toChecksumAddress("0xf164fC0Ec4E93095b804a4795bBe1e041497b92a"),
    LoadContractABI("uniswapV2_router.json"))

Contract_UniswapV2_Factory = LoadContract(
    None, LoadContractABI("uniswapV2_factory.json"))

Contract_UniswapV2_Pair = LoadContract(
    None, LoadContractABI("uniswapV2_pair.json"))

Contract_Sushiswap_Router = LoadContract(
    Instance_Web3.toChecksumAddress("0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F"),
    LoadContractABI("uniswapV2_router.json"))

Contract_Sushiswap_Factory = LoadContract(
    None, LoadContractABI("uniswapV2_factory.json"))

Contract_Defiswap_Router = LoadContract(
    Instance_Web3.toChecksumAddress("0xCeB90E4C17d626BE0fACd78b79c9c87d7ca181b3"),
    LoadContractABI("uniswapV2_router.json"))

Contract_Defiswap_Factory = LoadContract(
    None, LoadContractABI("uniswapV2_factory.json"))

Contract_Sakeswap_Router = LoadContract(
    Instance_Web3.toChecksumAddress("0x9c578b573ede001b95d51a55a3fafb45f5608b1f"),
    LoadContractABI("uniswapV2_router.json"))

Contract_Sakeswap_Factory = LoadContract(
    None, LoadContractABI("uniswapV2_factory.json"))

Contract_Balancer_Factory = LoadContract(
    Instance_Web3.toChecksumAddress("0x9424b1412450d0f8fc2255faf6046b98213b76bd"),
    LoadContractABI("balancer_factory.json"))

Contract_Balancer_Exchange = LoadContract(
    Instance_Web3.toChecksumAddress("0x6317c5e82a06e1d8bf200d21f4510ac2c038ac81"),
    LoadContractABI("balancer_exchange.json"))

Contract_Balancer_Pool = LoadContract(
    None, LoadContractABI("balancer_pool.json"))

Contract_Curve_AddressProvider = LoadContract(
    Instance_Web3.toChecksumAddress("0x0000000022D53366457F9d5E68Ec105046FC4383"),
    LoadContractABI("curve_addressProvider.json"))

Contract_Curve_Registry = LoadContract(
    None, LoadContractABI("curve_registry.json"))

Contract_Curve_Pool_3Pool = LoadContract(
    None, LoadContractABI("curve_pool_3pool.json"))

ABI_BancorNetwork = LoadContractABI("bancorNetwork.json")
Contract_BancorNetwork = LoadContract(None, ABI_BancorNetwork)

ABI_BancorGasPriceLimit = LoadContractABI("bancorGasPriceLimit.json")
Contract_BancorGasPriceLimit = LoadContract(None, ABI_BancorGasPriceLimit)

# This contract should work for every contract since i'm only calling generic functions that are in all the BancorConverter contracts
# If for some reason something doesn't work for a specific market, then i'll have to add in that contract for that market's token...
ABI_BancorConverter_mana = LoadContractABI("bancorConverter_mana.json")
Contract_BancorConverter_mana = LoadContract(None, ABI_BancorConverter_mana)

ABI_BancorConverter_ethbnt = LoadContractABI("bancorConverter_ethbnt.json")
Contract_BancorConverter_ethbnt = LoadContract(None, ABI_BancorConverter_ethbnt)

ABI_SetToken = LoadContractABI("set_rebalancingSet.json")
Contract_SetToken = LoadContract('0x9ea463Ec4cE9E9E5bc9cFd0187C4Ac3a70DD951D', ABI_SetToken)  # TODO, hard coded this is bad I shouldn't have to do this

ABI_Set_MACOStrategyManager = LoadContractABI("set_MACOStrategyManagerV2.json")
Contract_Set_MACOStrategyManager = LoadContract(None, ABI_Set_MACOStrategyManager)

ABI_Set_BTCDaiRebalancingManager = LoadContractABI("set_BTCDaiRebalancingManager.json")
Contract_Set_BTCDaiRebalancingManager = LoadContract(None, ABI_Set_BTCDaiRebalancingManager)

ABI_Set_SocialTradingManager = LoadContractABI("set_SocialTradingManager.json")
Contract_Set_SocialTradingManager = LoadContract(None, ABI_Set_SocialTradingManager)

ABI_Set_CTokenBidderContract = LoadContractABI("set_CTokenBidderContract.json")
Contract_Set_CTokenBidderContract = LoadContract(None, ABI_Set_CTokenBidderContract)

KeeperDAOLiquidityProviderAssetProxyAddress = Instance_Web3.toChecksumAddress('0x0AD8229D4bC84135786AE752B9A9D53392A8afd4')

Contract_DiamondLoupe = LoadContract(None, LoadContractABI("test_NinjaProxy_DiamondLoupe.json"))

# ****** Begin Ninja Contracts ****** #

# NOTE: This is important!  With the new solidity compiler version 0.6.x, the proxy ABI will contain the following
# 	{
# 		"stateMutability": "payable",
# 		"type": "receive"
# 	},
# My version of web3.py that I'm using has issues with this. It will result in an error that looks like this:
#   File "/Users/pako/Projects/Eth/KeeperDAO/Env_Ninja_Mac/lib/python3.6/site-packages/web3/utils/abi.py", line 46, in filter_by_name
#     in contract_abi
#   File "/Users/pako/Projects/Eth/KeeperDAO/Env_Ninja_Mac/lib/python3.6/site-packages/web3/utils/abi.py", line 49, in <listcomp>
#     abi['name'] == name
# KeyError: 'name'
# To properly fix this, I should update to a newer version of web3.py, but right now I don't want to deal with that upgrade because that may break other things
# To work around this issue for now, simply remove this part of the ABI from the JSON ABI in the python code since we do not need it
Contract_Ninja_DiamondProxy = LoadContract(
    Instance_Web3.toChecksumAddress('0x3d71d79c224998e608d03c5ec9b405e7a38505f0'),
    LoadContractABI("proxy.json", Subdirectory_NinjaABI))

Contract_Ninja_AssetManager = LoadContract(
    Instance_Web3.toChecksumAddress('0x045f22d0aaf99790971f004e0614f2a001cbda50'),
    LoadContractABI("assetManager.json", Subdirectory_NinjaABI))

Contract_Ninja_GasTokens = LoadContract(
    Instance_Web3.toChecksumAddress('0x68d4f0b51d422afd6b1f9627b6b65ddbc61ec8a0'),
    LoadContractABI("gasTokens_CHI.json", Subdirectory_NinjaABI))

Contract_Ninja_Properties = LoadContract(
    Instance_Web3.toChecksumAddress('0xce9c9ebf51b70982cd62495da0bbda4163549784'),
    LoadContractABI("properties.json", Subdirectory_NinjaABI))

Contract_Ninja_Authentication_KeeperDAOLPPs = LoadContract(
    Instance_Web3.toChecksumAddress('0x6e7fc49d52ba63fbae1ad19ab749fc70097b09dc'),
    LoadContractABI("authentication_keeperDAOLPPs.json", Subdirectory_NinjaABI))

Contract_Ninja_Trade_A = LoadContract(
    Instance_Web3.toChecksumAddress('0xcec101e23c1d5b5138d55adf6aa00a95248986cf'),
    LoadContractABI("trade_A.json", Subdirectory_NinjaABI))

Contract_Ninja_Trade_B = LoadContract(
    Instance_Web3.toChecksumAddress('0xbcebb0a60f0ba624ad9e56b1a2e7ed8ed556081a'),
    LoadContractABI("trade_B.json", Subdirectory_NinjaABI))

Contract_Ninja_Trade_C = LoadContract(
    Instance_Web3.toChecksumAddress('0xa1c9ce2d8c3e52f0609281055f6196e4b9bc26ed'),
    LoadContractABI("trade_C.json", Subdirectory_NinjaABI))

Contract_Ninja_Exchange_Uniswap = LoadContract(
    Instance_Web3.toChecksumAddress('0xc0a9adceac294599acd1d5571cc99552677ca5fc'),
    LoadContractABI("exchange_uniswap.json", Subdirectory_NinjaABI))

Contract_Ninja_Exchange_UniswapV2 = LoadContract(
    Instance_Web3.toChecksumAddress('0x8bc8ca27810983086f16794cbe4123059089946e'),
    LoadContractABI("exchange_uniswapV2.json", Subdirectory_NinjaABI))

Contract_Ninja_Exchange_Kyber = LoadContract(
    Instance_Web3.toChecksumAddress('0x612d91b92061f0cfa89199ba1dca352893ccec1c'),
    LoadContractABI("exchange_kyber.json", Subdirectory_NinjaABI))

Contract_Ninja_Exchange_0xv3 = LoadContract(
    Instance_Web3.toChecksumAddress('0xefb9ba49bdcf687fd57b955baf225ebef2c25c1b'),
    LoadContractABI("exchange_0xv3.json", Subdirectory_NinjaABI))

Contract_Ninja_Exchange_0xv4 = LoadContract(
    Instance_Web3.toChecksumAddress('0x1d7ea14796a706487bf12639163a04555b6d396f'),
    LoadContractABI("exchange_0xv4.json", Subdirectory_NinjaABI))

Contract_Ninja_Exchange_Balancer = LoadContract(
    Instance_Web3.toChecksumAddress('0x4ba41d3146ec701aeb05264991802a6864284315'),
    LoadContractABI("exchange_balancer.json", Subdirectory_NinjaABI))

Contract_Ninja_Exchange_Curve = LoadContract(
    Instance_Web3.toChecksumAddress('0x14cb7dd77e013b81dbfd380085d5118e29b03fc8'),
    LoadContractABI("exchange_curve.json", Subdirectory_NinjaABI))

# Deprecated
# Contract_Ninja_Trade = LoadContract(
#     Instance_Web3.toChecksumAddress('0x62b1180a953b490d9118ce106a725c1ee7438ee3'),
#     LoadContractABI("trade.json", Subdirectory_NinjaABI))

# Deprecated
# Contract_Ninja_Trade_2 = LoadContract(
#     Instance_Web3.toChecksumAddress('0x45d78a00e948fe52507d8daa0b9fafa2c170283d'),
#     LoadContractABI("trade_2.json", Subdirectory_NinjaABI))

Contract_Ninja_Trade_3 = LoadContract(
    Instance_Web3.toChecksumAddress('0x238a4191c25467df3deaee10f164d48748605609'),
    LoadContractABI("trade_3.json", Subdirectory_NinjaABI))

Contract_RelayProxy = LoadContract(None, LoadContractABI("relayProxy.json", Subdirectory_NinjaABI))

# ****** End Ninja Contracts ****** #

# ****** Begin KeeperDAO LPs Contracts ****** #

Contract_KeeperDAO_LP_Simple = LoadContract(
    Instance_Web3.toChecksumAddress('0x35ffd6e268610e764ff6944d07760d0efe5e40e5'),
    LoadContractABI("simpleLP.json", Subdirectory_KeeperDAOABI))

ContractAddress_KeeperDAO_BorrowerProxy = Instance_Web3.toChecksumAddress('0xde92742213FEa5f78c6840B6EcBf214115ea8002')

# ****** End KeeperDAO LPs Contracts ****** #
