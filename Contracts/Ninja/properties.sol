pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }

    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray contains only all addresses in here in the exact order they are listed below
        // intArray contains only all addresses in here in the exact order they are listed below

        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // Enables/Disables informative event logging. 1 means yes, 0 means no
        uint256 doLogEvents;
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;

        // Whitelists to ensure only ninjaProxyContract can call trade functions
        address ninjaProxyContract;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract WhitelistedOwners is
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

contract Ninja_Properties is
    StorageContract_Properties,
    WhitelistedOwners
{
    function setAddresses(
        address[] memory addressArray
        ) public onlyWhitelist
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        uint256 i = 0;

        ds.kyberProxyContract = addressArray[i++];
        ds.wethContract = addressArray[i++];
        ds.zrxV3ExchangeContract = addressArray[i++];
        ds.kyberEthTokenContract = addressArray[i++];
        ds.keeperDaoEthTokenContract = addressArray[i++];
        ds.keeperDaoLppContract_Eth = address(uint160(addressArray[i++]));
        ds.ninjaProxyContract = addressArray[i++];
    }

    function setInts(
        uint256[] memory intArray
        ) public onlyWhitelist
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        uint256 i = 0;

        ds.doLogEvents = intArray[i++];
        ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx = intArray[i++];
        ds.gasPriceThresholdForSpendingGasTokens = intArray[i++];
        ds.minQuoteTokenAllowedToSpendDivider = intArray[i++];
        ds.divider_profitToShareWithKeeperDAO = intArray[i++];
    }

    function getAddresses(
        ) public view returns (address[] memory addressArray)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        uint256 i = 0;
        addressArray = new address[](7);  // NOTE, Update this fixed array size as you add items!

        addressArray[i++] = ds.kyberProxyContract;
        addressArray[i++] = ds.wethContract;
        addressArray[i++] = ds.zrxV3ExchangeContract;
        addressArray[i++] = ds.kyberEthTokenContract;
        addressArray[i++] = ds.keeperDaoEthTokenContract;
        addressArray[i++] = ds.keeperDaoLppContract_Eth;
        addressArray[i++] = ds.ninjaProxyContract;

        return addressArray;
    }

    function getInts(
        ) public view returns (uint256[] memory intArray)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        uint256 i = 0;
        intArray = new uint256[](5);  // NOTE, Update this fixed array size as you add items!

        intArray[i++] = ds.doLogEvents;
        intArray[i++] = ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;
        intArray[i++] = ds.gasPriceThresholdForSpendingGasTokens;
        intArray[i++] = ds.minQuoteTokenAllowedToSpendDivider;
        intArray[i++] = ds.divider_profitToShareWithKeeperDAO;

        return intArray;
    }
}
