pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray contains only all addresses in here in the exact order they are listed below
        // intArray contains only all addresses in here in the exact order they are listed below

        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // Enables/Disables informative event logging. 1 means yes, 0 means no
        uint256 doLogEvents;
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;

        // Whitelists to ensure only ninjaProxyContract can call trade functions
        address ninjaProxyContract;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract StorageContract_Authentication_KeeperDAOLPPs
{
    struct DiamondStorage_Authentication_KeeperDAOLPPs
    {
        mapping (address => bool) whitelistedKeeperDAOLPPs;
    }

    function diamondStorage_Authentication_KeeperDAOLPPs() internal pure returns(DiamondStorage_Authentication_KeeperDAOLPPs storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication.keeperdaolpps");
        assembly { ds_slot := 0x4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f0 }
    }
}

contract SafeTraders is
    StorageContract_Properties,
    StorageContract_Authentication_KeeperDAOLPPs
{
    modifier onlySafeTraders()
    {
        require(
            msg.sender == diamondStorage_Properties().ninjaProxyContract || diamondStorage_Authentication_KeeperDAOLPPs().whitelistedKeeperDAOLPPs[msg.sender] == true, 
            "Must be Ninja or whitelisted KeeperDAOLP");
        _;
    }
}

contract Logs is
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0x order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth filling a dust order
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it
    // 27 = UniswapV2 QuoteTokens balance fell outside requirement
    // 28 = Balancer QuoteTokens balance fell outside requirement
    // 29 = Sushiswap QuoteTokens balance fell outside requirement
    // 30 = UniswapV2 blockTimestampLast failed validation
    // 31 = Sushiswap blockTimestampLast failed validation
    // 32 = Curve QuoteTokens balance fell outside requirement

    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);

    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }

    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

abstract contract ERC20
{
    function balanceOf(address tokenOwner) public view virtual returns (uint balance);
    // function transfer(address toAddress, uint tokens) public virtual returns (bool success);
    // function approve(address _spender, uint256 _value) public virtual returns (bool success);
    // function allowance(address _owner, address _spender) public view virtual returns (uint256 remaining);
    // function decimals() public view returns (uint8 decimals);
}

contract SafeMath_Old
{
    function safeMul(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        require(c / a == b);
        require(
            c / a == b,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    function safeDiv(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a / b;
        return c;
    }

    // function safeSub(
    //     uint256 a, uint256 b
    //     ) internal pure returns (uint256)
    // {
    //     require(b <= a);
    //     require(
    //         b <= a,
    //         "UINT256_UNDERFLOW"
    //     );
    //     return a - b;
    // }

    function safeAdd(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a + b;
        require(c >= a);
        require(
            c >= a,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    // function max64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    // function min64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a < b ? a : b;
    // }

    // function max256(
    //     uint256 a, uint256 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    function min256(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        return a < b ? a : b;
    }

    function safetifyThisNumber(
        uint256 value,
        uint256 factor
        ) internal pure returns (uint256)
    {
        // Multiply by ~99%, just to be safe so there isn't a round up problem
        // In solididty, it's safer to underfill an order by a hair than to risk overfilling it resulting in an exception
        // To multiply by 99% simply pass in the number 99.
        // It will mulitply by 99 then divide by 100 in order to achieve the desired result without having an exception
        // To multiply by 99.99%, simply pass in the number 9999.
        // It will mulitply by 9999 then divide by 10000 in order to achieve the desired result without having an exception
        value = safeMul(value, factor);
        value = safeDiv(value, safeAdd(factor, 1));
        return value;
    }
}

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

/**
 * @dev Collection of functions related to the address type
 */
library Address
{
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, "Address: insufficient balance");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }("");
        require(success, "Address: unable to send value, recipient may have reverted");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionCall(target, data, "Address: low-level call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, "Address: low-level call with value failed");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, "Address: insufficient balance for call");
        require(isContract(target), "Address: call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, "Address: low-level static call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), "Address: static call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.3._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, "Address: low-level delegate call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.3._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), "Address: delegate call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath
{
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "SafeMath: subtraction overflow");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, "SafeMath: division by zero");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, "SafeMath: modulo by zero");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}

contract Allowances
{
    using SafeMath for uint256;
    using Address for address;

    function allowExchangeToMoveMyTokens(
        IERC20 token,
        address spender
        ) internal
    {
        uint256 allowance = token.allowance(address(this), spender);
        // If my allowance is zero or getting low
        if (allowance < uint96(-1))
        {
            // if the allowance is nonzero, must reset it to 0 first
            if (allowance != 0)
            {
                _callOptionalReturn(token, abi.encodeWithSelector(IERC20.approve.selector, spender, 0));
            }

            // approve the new allowance
            _callOptionalReturn(token, abi.encodeWithSelector(IERC20.approve.selector, spender, uint256(-1)));
        }
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(
        IERC20 token,
        bytes memory data
        ) private
    {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, "SafeERC20: low-level call failed");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), "SafeERC20: ERC20 operation did not succeed");
        }
    }
}

contract ZRXv4_LibSignature
{
    /// @dev Allowed signature types.
    enum SignatureType {
        ILLEGAL,
        INVALID,
        EIP712,
        ETHSIGN
    }

    /// @dev Encoded EC signature.
    struct Signature {
        // How to validate the signature.
        SignatureType signatureType;
        // EC Signature data.
        uint8 v;
        // EC Signature data.
        bytes32 r;
        // EC Signature data.
        bytes32 s;
    }
}

contract ZRXv4_LibOrder 
{
    enum OrderStatus 
    {
        INVALID,
        FILLABLE,
        FILLED,
        CANCELLED,
        EXPIRED
    }

    /// @dev A standard OTC or OO limit order.
    struct LimitOrder 
    {
        // IERC20TokenV06 makerToken;
        // IERC20TokenV06 takerToken;
        address makerToken;
        address takerToken;
        uint128 makerAmount;
        uint128 takerAmount;
        uint128 takerTokenFeeAmount;
        address maker;
        address taker;
        address sender;
        address feeRecipient;
        bytes32 pool;
        uint64 expiry;
        uint256 salt;
    }

    /// @dev An RFQ limit order.
    struct RfqOrder 
    {
        // IERC20TokenV06 makerToken;
        // IERC20TokenV06 takerToken;
        address makerToken;
        address takerToken;
        uint128 makerAmount;
        uint128 takerAmount;
        address maker;
        address taker;
        address txOrigin;
        bytes32 pool;
        uint64 expiry;
        uint256 salt;
    }

    /// @dev Info on a limit or RFQ order.
    struct OrderInfo 
    {
        bytes32 orderHash;
        OrderStatus status;
        uint128 takerTokenFilledAmount;
    }
}

abstract contract ZRXv4 is
    ZRXv4_LibOrder,
    ZRXv4_LibSignature
{
    function getRfqOrderInfo(RfqOrder memory order) public view virtual returns (OrderInfo memory orderInfo);
    function getLimitOrderInfo(LimitOrder memory order) public view virtual returns (OrderInfo memory orderInfo);

    function fillRfqOrder(
        RfqOrder memory order, Signature memory signature, uint128 takerTokenFillAmount
        ) public virtual returns (uint128 takerTokenFilledAmount, uint128 makerTokenFilledAmount);

    function fillLimitOrder(
        LimitOrder memory order, Signature memory signature, uint128 takerTokenFillAmount
        ) public payable virtual returns (uint128 takerTokenFilledAmount, uint128 makerTokenFilledAmount);
}

contract ZRXv4_Utils is
    StorageContract_Properties,
    SafeMath_Old,
    ZRXv4_LibOrder,
    ZRXv4_LibSignature
{
    // function calculateProtocolFee(
    //     uint8 numOfOrdersToFill
    //     ) internal view returns (uint256)
    // {
    //     return safeMul(safeMul(70000, tx.gasprice), numOfOrdersToFill);
    // }

    function convertBaseTokensToQuoteTokens(
        uint256 orderQuoteTokenAmount_forPrice, // to calculate price
        uint256 orderBaseTokenAmount_forPrice, // to calculate price
        uint256 baseTokenAmount_toConvert  // To convert to etherAmount
        ) internal pure returns (uint256)
    {
        // Use this formula
        // price = ether / tokens
        // Rearrange the formula like so
        // ether = tokens * price

        // I cannot use this formula because it will result in the answer being zero.
        // I must multiply BEFORE I divide here since I do not have floats, only int256's.
        // uint256 price = safeDiv(orderQuoteTokenAmount_forPrice, orderBaseTokenAmount_forPrice);
        // // We know remaining Token quantity, let's calculate remaining Ether quantity
        // uint256 etherAmount = safeMul(baseTokenAmount_toConvert, price);
        // So my above price formula becomes
        return safeDiv(safeMul(baseTokenAmount_toConvert, orderQuoteTokenAmount_forPrice), orderBaseTokenAmount_forPrice);
    }

    function convertQuoteTokensToBaseTokens(
        uint256 orderQuoteTokenAmount_forPrice, // to calculate price
        uint256 orderBaseTokenAmount_forPrice, // to calculate price
        uint256 quoteTokenAmount_toConvert  // To convert to tokenAmount
        ) internal pure returns (uint256)
    {
        // Use this formula
        // price = ether / tokens
        // Rearrange the formula like so
        // tokens = ether / price

        // I cannot use this formula because it will result in the answer being zero.
        // I must multiply BEFORE I divide here since I do not have floats, only int256's.
        // uint256 price = safeDiv(orderQuoteTokenAmount_forPrice, orderBaseTokenAmount_forPrice);
        // Confirm that 5 / (1 / 3) is the same as 5 * 3 / 1
        // rearrange the above formula to make this, doing the mulitplication before the division
        // quoteTokenAmount_toConvert * orderBaseTokenAmount_forPrice / orderQuoteTokenAmount_forPrice
        return safeDiv(safeMul(quoteTokenAmount_toConvert, orderBaseTokenAmount_forPrice), orderQuoteTokenAmount_forPrice);
    }

    // function is0xOrderDust(
    //     uint256 orderInfo_orderTakerAssetFilledAmount,
    //     uint256 order_takerAssetAmount
    //     ) internal pure returns (bool)
    // {
    //     if (safeDiv(safeMul(orderInfo_orderTakerAssetFilledAmount, 102), 100) > order_takerAssetAmount)
    //     {
    //         return true;
    //     }
    //     else
    //     {
    //         return false;
    //     }
    // }
}

contract Ninja_Exchange_0xv4 is
    StorageContract_Properties,
    SafeTraders,
    Logs,
    ZRXv4_Utils,
    Allowances
{
    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_buy_0xv4_rfq(
        uint256 quoteTokensToSpend,
        address baseToken,
        RfqOrder calldata order
        ) external returns (bool)
    {
        uint256 minQuoteTokensToSpend = quoteTokensToSpend;

        // Require the order to be fillable
        OrderInfo memory orderInfo = ZRXv4(0xDef1C0ded9bec7F1a1670819833240f027b25EfF).getRfqOrderInfo(order);
        if (orderInfo.status != OrderStatus.FILLABLE)
        {
            logCode_ifEnabled(3);
            return false;
        }

        // Make sure we don't try to overfill the order
        // Update etherToSpend based on the etherAmountLeftFillable if the etherAmountLeftFillable is less than etherToSpend
        uint256 etherAmountLeftFillable_0x = order.takerAmount - orderInfo.takerTokenFilledAmount;
        logCodeWithValue_ifEnabled(18, etherAmountLeftFillable_0x);
        quoteTokensToSpend = min256(quoteTokensToSpend, etherAmountLeftFillable_0x);

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(24);
            return false;
        }

        // Require the order maker to have enough assets to trade
        uint256 makersMakerTokenBalance = ERC20(baseToken).balanceOf(order.maker);
        uint256 etherToSpendConvertedToTokens = convertQuoteTokensToBaseTokens(order.takerAmount, order.makerAmount, quoteTokensToSpend);
        // Convert etherToSpend to tokens so I can make sure makers balance is enough to cover
        if (makersMakerTokenBalance < etherToSpendConvertedToTokens)
        {
            logCode_ifEnabled(25);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_sell_0xv4_rfq(
        uint256 quoteTokensToSpend,
        address quoteToken,
        RfqOrder calldata order
        ) external returns (bool)
    {
        uint256 minQuoteTokensToSpend = quoteTokensToSpend;

        // Require the order to be fillable
        OrderInfo memory orderInfo = ZRXv4(0xDef1C0ded9bec7F1a1670819833240f027b25EfF).getRfqOrderInfo(order);
        if (orderInfo.status != OrderStatus.FILLABLE)
        {
            logCode_ifEnabled(3);
            return false;
        }

        // Make sure we don't try to overfill the order
        // Update etherToSpend based on the tokenAmountLeftFillable_0x if the tokenAmountLeftFillable_0x is less than etherToSpend
        uint256 tokenAmountLeftFillable_0x = order.takerAmount - orderInfo.takerTokenFilledAmount;

        // Enforce tokenAmountLeftFillable_0x is not in ether here, it's actually tokens
        // So I need to convert tokenAmountLeftFillable_0x from tokens to ether so I can lower etherToSpend accordingly so we don't try and overfill the trade.
        uint256 etherAmountLeftFillable_0x = convertBaseTokensToQuoteTokens(
            order.makerAmount, order.takerAmount, tokenAmountLeftFillable_0x);
        etherAmountLeftFillable_0x = safetifyThisNumber(etherAmountLeftFillable_0x, 9999);
        // logCodeWithValue_ifEnabled(17, etherAmountLeftFillable_0x);
        quoteTokensToSpend = min256(quoteTokensToSpend, etherAmountLeftFillable_0x);

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(24);
            return false;
        }

        // Require the order maker to have enough assets to trade
        quoteTokensToSpend = min256(quoteTokensToSpend, ERC20(quoteToken).balanceOf(order.maker));

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(25);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_buy_0xv4_limit(
        uint256 quoteTokensToSpend,
        address baseToken,
        LimitOrder calldata order
        ) external returns (bool)
    {
        uint256 minQuoteTokensToSpend = quoteTokensToSpend;

        // Require the order to be fillable
        OrderInfo memory orderInfo = ZRXv4(0xDef1C0ded9bec7F1a1670819833240f027b25EfF).getLimitOrderInfo(order);
        if (orderInfo.status != OrderStatus.FILLABLE)
        {
            logCode_ifEnabled(3);
            return false;
        }

        // Make sure we don't try to overfill the order
        // Update etherToSpend based on the etherAmountLeftFillable if the etherAmountLeftFillable is less than etherToSpend
        uint256 etherAmountLeftFillable_0x = order.takerAmount - orderInfo.takerTokenFilledAmount;
        logCodeWithValue_ifEnabled(18, etherAmountLeftFillable_0x);
        quoteTokensToSpend = min256(quoteTokensToSpend, etherAmountLeftFillable_0x);

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(24);
            return false;
        }

        // Require the order maker to have enough assets to trade
        uint256 makersMakerTokenBalance = ERC20(baseToken).balanceOf(order.maker);
        uint256 etherToSpendConvertedToTokens = convertQuoteTokensToBaseTokens(order.takerAmount, order.makerAmount, quoteTokensToSpend);
        // Convert etherToSpend to tokens so I can make sure makers balance is enough to cover
        if (makersMakerTokenBalance < etherToSpendConvertedToTokens)
        {
            logCode_ifEnabled(25);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_sell_0xv4_limit(
        uint256 quoteTokensToSpend,
        address quoteToken,
        LimitOrder calldata order
        ) external returns (bool)
    {
        uint256 minQuoteTokensToSpend = quoteTokensToSpend;

        // Require the order to be fillable
        OrderInfo memory orderInfo = ZRXv4(0xDef1C0ded9bec7F1a1670819833240f027b25EfF).getLimitOrderInfo(order);
        if (orderInfo.status != OrderStatus.FILLABLE)
        {
            logCode_ifEnabled(3);
            return false;
        }

        // Make sure we don't try to overfill the order
        // Update etherToSpend based on the tokenAmountLeftFillable_0x if the tokenAmountLeftFillable_0x is less than etherToSpend
        uint256 tokenAmountLeftFillable_0x = order.takerAmount - orderInfo.takerTokenFilledAmount;

        // Enforce tokenAmountLeftFillable_0x is not in ether here, it's actually tokens
        // So I need to convert tokenAmountLeftFillable_0x from tokens to ether so I can lower etherToSpend accordingly so we don't try and overfill the trade.
        uint256 etherAmountLeftFillable_0x = convertBaseTokensToQuoteTokens(
            order.makerAmount, order.takerAmount, tokenAmountLeftFillable_0x);
        etherAmountLeftFillable_0x = safetifyThisNumber(etherAmountLeftFillable_0x, 9999);
        // logCodeWithValue_ifEnabled(17, etherAmountLeftFillable_0x);
        quoteTokensToSpend = min256(quoteTokensToSpend, etherAmountLeftFillable_0x);

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(24);
            return false;
        }

        // Require the order maker to have enough assets to trade
        quoteTokensToSpend = min256(quoteTokensToSpend, ERC20(quoteToken).balanceOf(order.maker));

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(25);
            return false;
        }

        return true;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_0xv4_rfq(
        uint256 takerTokensToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        RfqOrder memory order,
        Signature memory signature
        ) public onlySafeTraders returns (uint256 makerTokensReceived)
    {
        // Dynamically approve the token before the trade
        // this allows me to instantly add new tokens without having to pre-approve
        allowExchangeToMoveMyTokens(IERC20(order.takerToken), 0xDef1C0ded9bec7F1a1670819833240f027b25EfF);

        // No protocol fee
        (, uint128 makerTokenFilledAmount) = ZRXv4(0xDef1C0ded9bec7F1a1670819833240f027b25EfF).fillRfqOrder(
            order, signature, uint128(takerTokensToSpend));
        return uint256(makerTokenFilledAmount);
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_0xv4_limit(
        uint256 takerTokensToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        LimitOrder memory order,
        Signature memory signature
        ) public onlySafeTraders returns (uint256 makerTokensReceived)
    {
        // Dynamically approve the token before the trade
        // this allows me to instantly add new tokens without having to pre-approve
        allowExchangeToMoveMyTokens(IERC20(order.takerToken), 0xDef1C0ded9bec7F1a1670819833240f027b25EfF);

        // Pay protocol fee by sending ETH to the call
        (, uint128 makerTokenFilledAmount) = ZRXv4(0xDef1C0ded9bec7F1a1670819833240f027b25EfF).fillLimitOrder{
            value: safeMul(70000, tx.gasprice)}(order, signature, uint128(takerTokensToSpend));
        return uint256(makerTokenFilledAmount);
    }
}
