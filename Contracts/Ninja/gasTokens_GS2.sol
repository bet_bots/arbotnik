pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }
    
    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds) 
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract WhitelistedOwners is 
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

abstract contract GasToken2 
{
    function mint(uint256 value) public virtual;
    function free(uint256 value) public virtual returns (bool success);
}

contract Ninja_GasTokens is 
    WhitelistedOwners
{
    function mintGasTokens(
        uint gasTokens
        ) public
    {
        // When using the public contract GasToken2
        GasToken2(0x0000000000b3F879cb30FE243b4Dfee438691c04).mint(gasTokens);
    }
    
    function freeGasTokens(
        uint gasTokens
        ) public onlyWhitelist returns (bool success)
    {
        // When using the public contract GasToken2
        return GasToken2(0x0000000000b3F879cb30FE243b4Dfee438691c04).free(gasTokens);
    }
}
