pragma solidity ^0.6.4;

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray contains only all addresses in here in the exact order they are listed below
        // intArray contains only all addresses in here in the exact order they are listed below

        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // Enables/Disables informative event logging. 1 means yes, 0 means no
        uint256 doLogEvents;
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;

        // Whitelists to ensure only ninjaProxyContract can call trade functions
        address ninjaProxyContract;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract Logs is
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0xv3 order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth filling a dust order
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it
    // 27 = UniswapV2 QuoteTokens balance fell outside requirement
    // 28 = Balancer QuoteTokens balance fell outside requirement
    // 29 = Sushiswap QuoteTokens balance fell outside requirement
    // 30 = UniswapV2 blockTimestampLast failed validation

    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);

    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }

    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

contract NinjaProxyOwner is
    StorageContract_Properties
{
    modifier onlyNinjaProxy()
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        require(msg.sender == ds.ninjaProxyContract, "Must be Ninja");
        _;
    }
}

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

/**
 * @dev Collection of functions related to the address type
 */
library Address
{
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, "Address: insufficient balance");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }("");
        require(success, "Address: unable to send value, recipient may have reverted");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, "Address: low-level call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, "Address: low-level call with value failed");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, "Address: insufficient balance for call");
        require(isContract(target), "Address: call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, "Address: low-level static call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), "Address: static call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.3._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, "Address: low-level delegate call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.3._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), "Address: delegate call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath
{
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "SafeMath: subtraction overflow");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, "SafeMath: division by zero");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, "SafeMath: modulo by zero");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}

contract Allowances
{
    using SafeMath for uint256;
    using Address for address;

    function allowExchangeToMoveMyTokens(
        IERC20 token,
        address spender
        ) internal
    {
        uint256 allowance = token.allowance(address(this), spender);
        // If my allowance is zero or getting low
        if (allowance < uint96(-1))
        {
            // if the allowance is nonzero, must reset it to 0 first
            if (allowance != 0)
            {
                _callOptionalReturn(token, abi.encodeWithSelector(IERC20.approve.selector, spender, 0));
            }

            // approve the new allowance
            _callOptionalReturn(token, abi.encodeWithSelector(IERC20.approve.selector, spender, uint256(-1)));
        }
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(
        IERC20 token,
        bytes memory data
        ) private
    {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, "SafeERC20: low-level call failed");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), "SafeERC20: ERC20 operation did not succeed");
        }
    }
}

abstract contract UniswapV2
{
    function swapExactTokensForETH(
        uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline
        ) external virtual returns (uint[] memory amounts);
    function swapExactETHForTokens(
        uint amountOutMin, address[] calldata path, address to, uint deadline
        ) external payable virtual returns (uint[] memory amounts);
    function swapExactTokensForTokens(
        uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline
        ) external virtual returns (uint[] memory amounts);
    function getReserves(
        ) external view virtual returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
}

abstract contract UniswapV2Pool
{
    function token0() external view virtual returns (address);
    function token1() external view virtual returns (address);
    function swap(uint amount0Out, uint amount1Out, address to, bytes calldata data) external virtual;
    function getReserves() external view virtual returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
}

contract Ninja_Exchange_UniswapV2 is
    Logs,
    NinjaProxyOwner,
    Allowances
{
    using SafeMath for uint;

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_buy_uniswapV2(
        address quoteToken,
        uint256 maxUniswapQuoteTokenQuantityAcceptable,
        address exchange
        ) external returns (bool)
    {
        uint256 uniswapQuoteTokenBalance = IERC20(quoteToken).balanceOf(exchange);

        if (uniswapQuoteTokenBalance > maxUniswapQuoteTokenQuantityAcceptable)
        {
            logCodeWithValue_ifEnabled(27, uniswapQuoteTokenBalance);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_sell_uniswapV2(
        address quoteToken,
        uint256 minUniswapQuoteTokenQuantityAcceptable,
        address exchange
        ) external returns (bool)
    {
        uint256 uniswapQuoteTokenBalance = IERC20(quoteToken).balanceOf(exchange);

        if (uniswapQuoteTokenBalance < minUniswapQuoteTokenQuantityAcceptable)
        {
            logCodeWithValue_ifEnabled(27, uniswapQuoteTokenBalance);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_timestamp_uniswapV2(
        address exchange
        ) external returns (bool)
    {
        (, , uint32 blockTimestampLast) = UniswapV2(exchange).getReserves();
        // The timestamp will be set to the current block's timestamp if someone beat me to the trade
        // This is an imperfect strategy, but it is the most gas efficient and will work most of the time
        // uint32 currentBlockTimestamp = uint32(block.timestamp % 2**32);
        if (blockTimestampLast == uint32(block.timestamp % 2**32))
        {
            logCode_ifEnabled(30);
            return false;
        }

        return true;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_uniswapV2_router(
        uint256 amountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address[] calldata path
        ) external onlyNinjaProxy returns (uint256 tokensReceived)
    {
        uint256[] memory amounts = UniswapV2(0xf164fC0Ec4E93095b804a4795bBe1e041497b92a).swapExactTokensForTokens(
            amountIn, 1, path, address(this), 69203865833239757421118596509098632427930889272824243351707071692229331386368);
        return amounts[amounts.length - 1];
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_uniswapV2_pool_spendToken0(
        uint256 amountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        bool doSendTokensToPool,
        address poolContract,
        address tokenOutRecipient  // Based on doSendTokensToPool, either another UniswapV2 pool or this proxy contract
        ) external onlyNinjaProxy returns (uint256 tokensReceived)
    {
        // Get balances of the pool
        (
            uint112 reserve0,
            uint112 reserve1,
        ) = UniswapV2Pool(poolContract).getReserves();

        UniswapV2Pool pool = UniswapV2Pool(poolContract);
        address token0_in = pool.token0();

        uint256 amountOut = convertAmountInToAmountOut(
            amountIn, reserve0,  reserve1);

        // Ensure token allowance
        // To trade directly with the pools, we must approve each individual pool as a spender
        // I'm not going to do this ahead of time because it's painful so I'll just instead do it here
        allowExchangeToMoveMyTokens(IERC20(token0_in), poolContract);

        // Trade
        if (doSendTokensToPool)
        {
            // Before I call the swap function, I must send the exchange the amountIn
            IERC20(token0_in).transfer(poolContract, amountIn);
        }
        // else we assume that tokenIn has already been transfered to poolContract by another trade
        pool.swap(0, amountOut, tokenOutRecipient, new bytes(0));
        return amountOut;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_uniswapV2_pool_spendToken1(
        uint256 amountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        bool doSendTokensToPool,
        address poolContract,
        address tokenOutRecipient  // Based on doSendTokensToPool, either another UniswapV2 pool or this proxy contract
        ) external onlyNinjaProxy returns (uint256 tokensReceived)
    {
        // Get balances of the pool
        (
            uint112 reserve0,
            uint112 reserve1,
        ) = UniswapV2Pool(poolContract).getReserves();

        UniswapV2Pool pool = UniswapV2Pool(poolContract);
        address token1_in = pool.token1();

        // Convert amountIn to amountOut
        uint256 amountOut = convertAmountInToAmountOut(
            amountIn, reserve1,  reserve0);

        // Ensure token allowance
        // To trade directly with the pools, we must approve each individual pool as a spender
        // I'm not going to do this ahead of time because it's painful so I'll just instead do it here
        allowExchangeToMoveMyTokens(IERC20(token1_in), poolContract);

        // Trade
        if (doSendTokensToPool)
        {
            // Before I call the swap function, I must send the exchange the amountIn
            IERC20(token1_in).transfer(poolContract, amountIn);
        }
        // else we assume that tokenIn has already been transfered to poolContract by another trade
        pool.swap(amountOut, 0, tokenOutRecipient, new bytes(0));
        return amountOut;
    }

    function convertAmountInToAmountOut(
        uint256 amountIn,
        uint256 reserveIn,
        uint256 reserveOut
        ) internal pure returns (uint256 amountOut)
    {
        // given an input amount of an asset and pair reserves, returns the maximum output amount of the other asset
        require(amountIn > 0, 'convertAmountInToAmountOut: INSUFFICIENT_INPUT_AMOUNT');
        require(reserveIn > 0 && reserveOut > 0, 'convertAmountInToAmountOut: INSUFFICIENT_LIQUIDITY');
        uint amountInWithFee = amountIn.mul(997);
        uint numerator = amountInWithFee.mul(reserveOut);
        uint denominator = reserveIn.mul(1000).add(amountInWithFee);
        return numerator / denominator;
    }
}
