pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }

    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray
        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // intArray
        uint256 doLogEvents;  // 1 means yes, 0 means no
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract StorageContract_Authentication_KeeperDAOTransferProxys
{
    struct DiamondStorage_Authentication_KeeperDAOTransferProxys
    {
        mapping (address => bool) whitelistedKeeperDAOTransferProxys;
    }

    function diamondStorage_Authentication_KeeperDAOTransferProxys() internal pure returns(DiamondStorage_Authentication_KeeperDAOTransferProxys storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication.keeperdaolpps");
        assembly { ds_slot := 0x4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f0 }
    }
}

contract WhitelistedOwners is
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

contract WhitelistedKeeperDAOTransferProxys is
    StorageContract_Authentication_KeeperDAOTransferProxys
{
    modifier onlyWhitelistedKeeperDAOTransferProxys()
    {
        DiamondStorage_Authentication_KeeperDAOTransferProxys storage ds = diamondStorage_Authentication_KeeperDAOTransferProxys();
        require(ds.whitelistedKeeperDAOTransferProxys[msg.sender] == true, "Must be whitelisted KeeperDAO transfer proxy.");
        _;
    }
}

contract Logs is
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0xv3 order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth filling a dust order
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it

    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);

    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }

    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

contract SafeMath
{
    function safeMul(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        require(c / a == b);
        require(
            c / a == b,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    function safeDiv(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a / b;
        return c;
    }

    function safeSub(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        require(b <= a);
        require(
            b <= a,
            "UINT256_UNDERFLOW"
        );
        return a - b;
    }

    function safeAdd(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a + b;
        require(c >= a);
        require(
            c >= a,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    // function max64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    // function min64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a < b ? a : b;
    // }

    // function max256(
    //     uint256 a, uint256 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    function min256(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        return a < b ? a : b;
    }

    function safetifyThisNumber(
        uint256 value,
        uint256 factor
        ) internal pure returns (uint256)
    {
        // Multiply by ~99%, just to be safe so there isn't a round up problem
        // In solididty, it's safer to underfill an order by a hair than to risk overfilling it resulting in an exception
        // To multiply by 99% simply pass in the number 99.
        // It will mulitply by 99 then divide by 100 in order to achieve the desired result without having an exception
        // To multiply by 99.99%, simply pass in the number 9999.
        // It will mulitply by 9999 then divide by 10000 in order to achieve the desired result without having an exception
        value = safeMul(value, factor);
        value = safeDiv(value, safeAdd(factor, 1));
        return value;
    }
}

abstract contract ERC20
{
    function balanceOf(address tokenOwner) public view virtual returns (uint balance);
    function transfer(address toAddress, uint tokens) public virtual returns (bool success);
    function approve(address _spender, uint256 _value) public virtual returns (bool success);
    function allowance(address _owner, address _spender) public view virtual returns (uint256 remaining);
    // function decimals() public view returns (uint8 decimals);
}

contract ZRXv3_LibOrder
{
    // A valid order remains fillable until it is expired, fully filled, or cancelled.
    // An order's state is unaffected by external factors, like account balances.
    enum OrderStatus {
        INVALID,                     // Default value
        INVALID_MAKER_ASSET_AMOUNT,  // Order does not have a valid maker asset amount
        INVALID_TAKER_ASSET_AMOUNT,  // Order does not have a valid taker asset amount
        FILLABLE,                    // Order is fillable
        EXPIRED,                     // Order has already expired
        FULLY_FILLED,                // Order is fully filled
        CANCELLED                    // Order has been cancelled
    }

    struct Order
    {
        address makerAddress;           // Address that created the order.
        address takerAddress;           // Address that is allowed to fill the order. If set to 0, any address is allowed to fill the order.
        address feeRecipientAddress;    // Address that will recieve fees when order is filled.
        address senderAddress;          // Address that is allowed to call Exchange contract methods that affect this order. If set to 0, any address is allowed to call these methods.
        uint256 makerAssetAmount;       // Amount of makerAsset being offered by maker. Must be greater than 0.
        uint256 takerAssetAmount;       // Amount of takerAsset being bid on by maker. Must be greater than 0.
        uint256 makerFee;               // Amount of ZRX paid to feeRecipient by maker when order is filled. If set to 0, no transfer of ZRX from maker to feeRecipient will be attempted.
        uint256 takerFee;               // Amount of ZRX paid to feeRecipient by taker when order is filled. If set to 0, no transfer of ZRX from taker to feeRecipient will be attempted.
        uint256 expirationTimeSeconds;  // Timestamp in seconds at which order expires.
        uint256 salt;                   // Arbitrary number to facilitate uniqueness of the order's hash.
        bytes makerAssetData;           // Encoded data that can be decoded by a specified proxy contract when transferring makerAsset. The last byte references the id of this proxy.
        bytes takerAssetData;           // Encoded data that can be decoded by a specified proxy contract when transferring takerAsset. The last byte references the id of this proxy.
        bytes makerFeeAssetData;
        bytes takerFeeAssetData;
    }

    struct Order_Encoded
    {
        uint256 makerAddress_encoded;           // Address that created the order.
        address takerAddress;           // Address that is allowed to fill the order. If set to 0, any address is allowed to fill the order.
        address feeRecipientAddress;    // Address that will recieve fees when order is filled.
        address senderAddress;          // Address that is allowed to call Exchange contract methods that affect this order. If set to 0, any address is allowed to call these methods.
        uint256 makerAssetAmount_encoded;       // Amount of makerAsset being offered by maker. Must be greater than 0.
        uint256 takerAssetAmount_encoded;       // Amount of takerAsset being bid on by maker. Must be greater than 0.
        uint256 makerFee;               // Amount of ZRX paid to feeRecipient by maker when order is filled. If set to 0, no transfer of ZRX from maker to feeRecipient will be attempted.
        uint256 takerFee;               // Amount of ZRX paid to feeRecipient by taker when order is filled. If set to 0, no transfer of ZRX from taker to feeRecipient will be attempted.
        uint256 expirationTimeSeconds_encoded;  // Timestamp in seconds at which order expires.
        uint256 salt_encoded;                   // Arbitrary number to facilitate uniqueness of the order's hash.
        uint256[] makerAssetData_encoded;           // Encoded data that can be decoded by a specified proxy contract when transferring makerAsset. The last byte references the id of this proxy.
        uint256[] takerAssetData_encoded;           // Encoded data that can be decoded by a specified proxy contract when transferring takerAsset. The last byte references the id of this proxy.
        bytes makerFeeAssetData;
        bytes takerFeeAssetData;
    }

    struct OrderInfo
    {
        uint8 orderStatus;                    // Status that describes order's validity and fillability.
        bytes32 orderHash;                    // EIP712 hash of the order (see LibOrder.getOrderHash).
        uint256 orderTakerAssetFilledAmount;  // Amount of order that has already been filled.
    }

    struct FillResults
    {
        uint256 makerAssetFilledAmount;  // Total amount of makerAsset(s) filled.
        uint256 takerAssetFilledAmount;  // Total amount of takerAsset(s) filled.
        uint256 makerFeePaid;
        uint256 takerFeePaid;
        uint256 protocolFeePaid;
    }
}

abstract contract ZRXv3 is
    ZRXv3_LibOrder
{
    function getOrderInfo(Order memory order) public view virtual returns (OrderInfo memory orderInfo);
    function fillOrder(
        Order memory order, uint256 takerAssetFillAmount, bytes memory signature
        ) public payable virtual returns (FillResults memory fillResults);
    // function batchFillOrdersNoThrow(
    //     Order[] memory orders, uint256[] memory takerAssetFillAmounts, bytes[] memory signatures
    //     ) public payable virtual returns (FillResults[] memory fillResults);
}

contract ZRXv3_Utils is
    StorageContract_Properties,
    SafeMath,
    ZRXv3_LibOrder
{
    function calculateProtocolFee(
        uint8 numOfOrdersToFill
        ) internal view returns (uint256)
    {
        return 150000 * tx.gasprice * numOfOrdersToFill;
    }

    function tradeTokensOn0xv3(
        Order memory order,
        uint256 takerAssetFillAmount,
        bytes memory signature,
        uint256 protocolFee
        ) internal returns (FillResults memory fillResults)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // This function assumes the balances are all ready to trade
        // Including wrapped ether, it must be wrapped before calling this function
	   // return ZRXv3(ds.zrxV3ExchangeContract).fillOrder.value(protocolFee)(order, takerAssetFillAmount, signature);
	    return ZRXv3(ds.zrxV3ExchangeContract).fillOrder{value: protocolFee}(order, takerAssetFillAmount, signature);
    }

    // function convertTokensToEther(
    //     uint256 orderEtherAmount_forPrice, // to calculate price
    //     uint256 orderTokenAmount_forPrice, // to calculate price
    //     uint256 tokenAmount_toConvert  // To convert to etherAmount
    //     ) internal pure returns (uint256 etherAmount)
    // {
    //     // Use this formula
    //     // price = ether / tokens
    //     // Rearrange the formula like so
    //     // ether = tokens * price

    //     // I cannot use this formula because it will result in the answer being zero.
    //     // I must multiply BEFORE I divide here since I do not have floats, only int256's.
    //     // uint256 price = safeDiv(orderEtherAmount_forPrice, orderTokenAmount_forPrice);
    //     // // We know remaining Token quantity, let's calculate remaining Ether quantity
    //     // uint256 etherAmount = safeMul(tokenAmount_toConvert, price);

    //     // So my above price formula becomes
    //     etherAmount = safeDiv(safeMul(tokenAmount_toConvert, orderEtherAmount_forPrice), orderTokenAmount_forPrice);
    //     // It's safer to underfill the order by a hair than to risk overfilling it resulting in an exception
    //     return safetifyThisNumber(etherAmount, 9999);
    // }

    function convertTokensToEther(
        uint256 orderEtherAmount_forPrice, // to calculate price
        uint256 orderTokenAmount_forPrice, // to calculate price
        uint256 tokenAmount_toConvert  // To convert to etherAmount
        ) internal pure returns (uint256)
    {
        // Use this formula
        // price = ether / tokens
        // Rearrange the formula like so
        // ether = tokens * price

        // I cannot use this formula because it will result in the answer being zero.
        // I must multiply BEFORE I divide here since I do not have floats, only int256's.
        // uint256 price = safeDiv(orderEtherAmount_forPrice, orderTokenAmount_forPrice);
        // // We know remaining Token quantity, let's calculate remaining Ether quantity
        // uint256 etherAmount = safeMul(tokenAmount_toConvert, price);
        // So my above price formula becomes
        return safeDiv(safeMul(tokenAmount_toConvert, orderEtherAmount_forPrice), orderTokenAmount_forPrice);
    }

    function convertEtherToTokens(
        uint256 orderEtherAmount_forPrice, // to calculate price
        uint256 orderTokenAmount_forPrice, // to calculate price
        uint256 etherAmount_toConvert  // To convert to tokenAmount
        ) internal pure returns (uint256)
    {
        // Use this formula
        // price = ether / tokens
        // Rearrange the formula like so
        // tokens = ether / price

        // I cannot use this formula because it will result in the answer being zero.
        // I must multiply BEFORE I divide here since I do not have floats, only int256's.
        // uint256 price = safeDiv(orderEtherAmount_forPrice, orderTokenAmount_forPrice);
        // Confirm that 5 / (1 / 3) is the same as 5 * 3 / 1
        // rearrange the above formula to make this, doing the mulitplication before the division
        // etherAmount_toConvert * orderTokenAmount_forPrice / orderEtherAmount_forPrice
        return safeDiv(safeMul(etherAmount_toConvert, orderTokenAmount_forPrice), orderEtherAmount_forPrice);
    }

    function is0xOrderDust(
        uint256 orderInfo_orderTakerAssetFilledAmount,
        uint256 order_takerAssetAmount
        ) internal pure returns (bool)
    {
        if (safeDiv(safeMul(orderInfo_orderTakerAssetFilledAmount, 102), 100) > order_takerAssetAmount)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

abstract contract WETH is
    ERC20
{
    function deposit() public payable virtual;
    function withdraw(uint wad) public virtual;
}

contract WETH_Utils is
    StorageContract_Properties,
    WhitelistedOwners
{
    function wrapEther(
        uint amount
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        require(address(this).balance >= amount);
        WETH weth = WETH(ds.wethContract);
        // weth.deposit.value(amount)();
        weth.deposit{value: amount}();
    }

    function unwrapEther(
        uint amount
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        WETH weth = WETH(ds.wethContract);
        weth.withdraw(amount);
    }
}

abstract contract SetToken is
    ERC20
{
    // function rebalanceState() public view returns (uint8 state);
    // function getCombinedTokenArray() external view returns (address[] memory);
    function getBidPrice(uint256 _quantity) public view virtual returns (uint256[] memory, uint256[] memory);
    function getBiddingParameters() external view virtual returns (uint256[] memory);
}

abstract contract RebalanceAuctionModule
{
    function bidAndWithdraw(address _rebalancingSetToken, uint256 _quantity, bool _allowPartialFill) external virtual;
}

abstract contract CTokenBidderContract
{
    function getAddressAndBidPriceArray(
        address _rebalancingSetToken, uint256 _quantity
        ) external view virtual returns (address[] memory combinedTokenArray, uint256[] memory inflowUnitsArray, uint256[] memory outflowUnitsArray);
}

contract Set_Utils is
    SafeMath
{
    function tradeTokensOnSet(
        address[] memory addressArray_Set,
        uint256 inflowTokensToSpend,
        uint256 minBidSize,
        uint256 magicConverter_inflowTokensToShares
        ) internal returns (uint outflowTokensReceivedFromTrade)
    {
        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // Convert inflowTokensToSpend to shares
        uint256 shares = convertInflowTokenQuantityToSharesQuantity(inflowTokensToSpend, minBidSize, magicConverter_inflowTokensToShares);

        // Get before-trade balance of token i'm expecting to receive
        uint outflowTokenBalance_beforeTrade = ERC20(addressArray_Set[3]).balanceOf(address(this));

        // Trade.  Send inflowTokenAddress and receive outflowTokenAddress
        RebalanceAuctionModule rebalanceAuctionModule = RebalanceAuctionModule(addressArray_Set[0]);
        rebalanceAuctionModule.bidAndWithdraw(addressArray_Set[1], shares, true);

        // Get after-trade balance of token i'm expecting to receive
        uint outflowTokenBalance_afterTrade = ERC20(addressArray_Set[3]).balanceOf(address(this));

        outflowTokensReceivedFromTrade = safeSub(outflowTokenBalance_afterTrade, outflowTokenBalance_beforeTrade);
        // Event_Test("outflowTokensReceivedFromTrade", outflowTokensReceivedFromTrade);
        // Return token amount i received
        return outflowTokensReceivedFromTrade;
    }

    function convertInflowTokenQuantityToSharesQuantity(
        uint256 inflowTokensToSpend,
        uint256 minBidSize,
        uint256 magicConverter_inflowTokensToShares
        ) internal pure returns (uint256 shares)
    {
        // # Formula: shares = inflowTokensToSpend_weiUnits / magicNumber_weiUnits
        // # Formula solidity: shares = safeDiv(safeMul(inflowTokensToSpend_weiUnits, 1000000000000000000), magicNumber_weiUnits)
        shares = safeDiv(safeMul(inflowTokensToSpend, 1000000000000000000), magicConverter_inflowTokensToShares);
        // I'm reducing this number slightly to be on the safe side so we don't try to over trade
        // The consiquence of this is i'll be left with dust after trading, but at least it works
        // shares = safetifyThisNumber(shares, 999999);  // This might be too large
        shares = safetifyThisNumber(shares, 9999);
        // Enforce shares limitations with respect to sharesQuantity
        shares = enforceDivisibleMinBidSize(shares, minBidSize);
        return shares;
    }

    function enforceDivisibleMinBidSize(
        uint256 shares,
        uint256 minBidSize
        ) internal pure returns (uint256)
    {
        // So if minBidSize = 100000, and shares = 912345. It needs to return 900000.
        // return safeMul(safeDiv(shares, minBidSize), minBidSize);

        shares = safeMul(safeDiv(shares, minBidSize), minBidSize);
        // Also enforce that shares must be greater than minBidSize
        if (shares < minBidSize)
        {
            return 0;
        }
        else
        {
            return shares;
        }
    }

    function getRemainingCurrentUnits(
        address setTokenAddress
        ) internal view returns (uint256 remainingCurrentUnits)
    {
        SetToken setToken = SetToken(setTokenAddress);
        uint256[] memory biddingParameters = setToken.getBiddingParameters();
        return biddingParameters[1];
    }

    function getMaxInflowTokenQuantityRemaining(
        uint256 minBidSize,
        uint256 minInflowTokenQuantity,
        uint256 minOutflowTokenQuantity,
        uint256 remainingCurrentUnits
        ) internal pure returns (uint256 maxInflowTokenQuantity)
    {
        maxInflowTokenQuantity;
        uint256 dontCare;
        (
            maxInflowTokenQuantity,
            dontCare
        ) = getMaxFlowTokenQuantitiesRemaining(
            minBidSize, minInflowTokenQuantity, minOutflowTokenQuantity, remainingCurrentUnits);
        return maxInflowTokenQuantity;
    }

    function getMaxOutflowTokenQuantityRemaining(
        uint256 minBidSize,
        uint256 minInflowTokenQuantity,
        uint256 minOutflowTokenQuantity,
        uint256 remainingCurrentUnits
        ) internal pure returns (uint256 maxOutflowTokenQuantity)
    {
        uint256 dontCare;
        maxOutflowTokenQuantity;
        (
            dontCare,
            maxOutflowTokenQuantity
        ) = getMaxFlowTokenQuantitiesRemaining(
            minBidSize, minInflowTokenQuantity, minOutflowTokenQuantity, remainingCurrentUnits);
        return maxOutflowTokenQuantity;
    }

    function getMaxFlowTokenQuantitiesRemaining(
        uint256 minBidSize,
        uint256 minInflowTokenQuantity,
        uint256 minOutflowTokenQuantity,
        uint256 remainingCurrentUnits
        ) internal pure returns (uint256 maxInflowTokenQuantity, uint256 maxOutflowTokenQuantity)
    {
        // self.remainingCurrentShares_weiUnits = float(self.remainingCurrentUnits_weiUnits) / float(self.minBidSize_weiUnits)
        uint256 remainingCurrentShares = safeDiv(remainingCurrentUnits, minBidSize);

        // self.maxInflowTokenQuantity_weiUnits = int(int(self.remainingCurrentShares_weiUnits) * self.minInflowTokenQuantity_weiUnits)
        maxInflowTokenQuantity = safeMul(remainingCurrentShares, minInflowTokenQuantity);
        // self.maxOutflowTokenQuantity_weiUnits = int(int(self.remainingCurrentShares_weiUnits) * self.minOutflowTokenQuantity_weiUnits)
        maxOutflowTokenQuantity = safeMul(remainingCurrentShares, minOutflowTokenQuantity);

        return (maxInflowTokenQuantity, maxOutflowTokenQuantity);
    }
}

// abstract contract Uniswap
// {
//     function ethToTokenSwapInput(uint256 min_tokens, uint256 deadline) public payable virtual returns (uint256 out);
//     function tokenToEthSwapInput(uint256 tokens_sold, uint256 min_eth, uint256 deadline) public virtual returns (uint256 out);
// }

abstract contract KyberProxy
{
    function trade(
        address src, uint srcAmount, address dest, address destAddress,
        uint maxDestAmount, uint minConversionRate, address walletId
        ) public payable virtual returns(uint);
}

abstract contract KyberReserve
{
    function getConversionRate(
        ERC20 src, ERC20 dest, uint srcQty, uint blockNumber
        ) public view virtual returns(uint);
}

// contract Uniswap_Utils
// {
//     function buyTokensOnUniswap(
//         address uniswapContract,
//         uint256 msgValue,
//         uint256 min_tokens,
//         uint256 deadline
//         ) internal returns (uint256)
//     {
//         // Send ether quantity as the msgValue if you're buying tokens
//         // And send 0 as the msgValue if you're selling tokens
//         // return Uniswap(uniswapContract).ethToTokenSwapInput.value(msgValue)(min_tokens, deadline);
//         return Uniswap(uniswapContract).ethToTokenSwapInput{value: msgValue}(min_tokens, deadline);
//     }

//     function sellTokensOnUniswap(
//         address uniswapContract,
//         uint256 tokens_sold,
//         uint256 min_eth,
//         uint256 deadline
//         ) internal returns (uint256)
//     {
//         return Uniswap(uniswapContract).tokenToEthSwapInput(tokens_sold, min_eth, deadline);
//     }
// }

contract Kyber_Utils is
    StorageContract_Properties
{
	function tradeTokensOnKyber(
	    uint256 msgValue,
	    address src,
	    uint srcAmount,
	    address dest,
	    uint maxDestAmount,
	    uint minConversionRate,
	    address walletId
	   ) internal returns (uint)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // Send ether quantity as the msgValue if you're buying tokens
        // And send 0 as the msgValue if you're selling tokens
	   // return KyberProxy(ds.kyberProxyContract).trade.value(msgValue)(
	   //     src, srcAmount, dest, address(this), maxDestAmount, minConversionRate, walletId);
	   return KyberProxy(ds.kyberProxyContract).trade{value: msgValue}(
	        src, srcAmount, dest, address(this), maxDestAmount, minConversionRate, walletId);
    }
}

contract Encoding_Utils
{
    function bitWiseRotateLeft(
        uint256 number,
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to left rotate number by d bits
        // # In number<<d, last d bits are 0.
        // # To put first 3 bits of number at
        // # last, do bitwise or of n<<d
        // # with number >>(256 - d)
        return (number << d) | (number >> (256 - d));
    }

    function bitWiseRotateRight(
        uint256 number,
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to right rotate n by d bits
        // # In n>>d, first d bits are 0.
        // # To put last 3 bits of at
        // # first, do bitwise or of n>>d
        // # with n <<(256 - d)
        return (number >> d) | (number << (256 - d)) & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    }

    function decodeSignature(
        uint256[] memory signatureIntList
        ) internal pure returns (bytes memory decodedSignature)
    {
        // Python has broken thke signature into hex strings,
        // then converted them into ints, then rotated them to the right
        // to decode, do the opposite

        uint8 numOfRotations = 2;
        // Rotate to the left
        uint256 x1 = bitWiseRotateLeft(signatureIntList[0], numOfRotations);
        uint256 x2 = bitWiseRotateLeft(signatureIntList[1], numOfRotations);
        uint256 x3 = bitWiseRotateLeft(signatureIntList[2], numOfRotations);

        decodedSignature = new bytes(66);
        assembly
        {
            mstore(add(decodedSignature, 66), x3)
            mstore(add(decodedSignature, 50), x2)
            mstore(add(decodedSignature, 25), x1)
            mstore(decodedSignature, 66)
        }

        return decodedSignature;
    }

    function decode0xAssetData(
        uint256[] memory zrxAssetDataIntList
        ) internal pure returns (bytes memory decoded0xAssetData)
    {
        // Python has broken thke signature into hex strings,
        // then converted them into ints, then rotated them to the right
        // to decode, do the opposite

        uint8 numOfRotations = 7;
        // Rotate to the left
        uint256 x1 = bitWiseRotateLeft(zrxAssetDataIntList[0], numOfRotations);
        uint256 x2 = bitWiseRotateLeft(zrxAssetDataIntList[1], numOfRotations);

        decoded0xAssetData = new bytes(36);
        assembly
        {
            mstore(add(decoded0xAssetData, 36), x2)
            mstore(add(decoded0xAssetData, 18), x1)
            mstore(decoded0xAssetData, 36)
        }

        return decoded0xAssetData;
    }

    function decodeAddress(
        uint256 myAddress
        ) internal pure returns (address decodedAddress)
    {
        // Python has converted myAddress to an int, added an obscure number, and rotated to the right
        // to decode, do the opposite

        // Subtract my random obscure number, no need for safe math here
        myAddress = myAddress - 891754983589020875164519;
        // Rotate to the left
        myAddress = bitWiseRotateLeft(myAddress, 5);

        bytes memory b = new bytes(20);
        assembly
        {
            mstore(add(b, 20), myAddress)
            mstore(b, 20)
        }

        assembly
        {
            decodedAddress := mload(add(b, 20))
        }

        return decodedAddress;
    }

    function decodeAddressArray(
        uint256[] memory encodedAddresses
        ) internal pure returns (address[] memory decodedAddressArray)
    {
        // Decode the addresses
        decodedAddressArray = new address[](encodedAddresses.length);
        for (uint256 i = 0; i < encodedAddresses.length; i++)
        {
            decodedAddressArray[i] = decodeAddress(encodedAddresses[i]);
        }

        return decodedAddressArray;
    }

    function decodeInt(
        uint256 myInt
        ) internal pure returns (uint256 decodedNumber)
    {
        // Python code has rotated myInt to the right
        // to decode, do the opposite

        // Rotate to the left
        myInt = bitWiseRotateLeft(myInt, 16);
        return myInt;
    }

    function decodeIntArray(
        uint256[] memory encodedInts
        ) internal pure returns (uint256[] memory decodedIntList)
    {
        decodedIntList = new uint256[](encodedInts.length);
        for (uint256 i = 0; i < encodedInts.length; i++)
        {
            decodedIntList[i] = decodeInt(encodedInts[i]);
        }

        return decodedIntList;
    }

    function decodeInt_injectIntAsFirstParameterInToCallData(
        uint256 myInt
        ) internal pure returns (uint256 decodedNumber)
    {
        // Python code has rotated myInt to the right
        // to decode, do the opposite

        // Rotate to the left
        myInt = bitWiseRotateLeft(myInt, 11);
        return myInt;
    }

    function decodeIntArray_injectIntAsFirstParameterInToCallData(
        uint256[] memory encodedInts
        ) internal pure returns (uint256[] memory decodedIntList)
    {
        decodedIntList = new uint256[](encodedInts.length);
        for (uint256 i = 0; i < encodedInts.length; i++)
        {
            decodedIntList[i] = decodeInt_injectIntAsFirstParameterInToCallData(encodedInts[i]);
        }

        return decodedIntList;
    }

    function injectIntAsFirstParameterInToCallData(
        uint256[] memory callDataArray_encoded,
        uint256 quoteTokensToSpend_toInject
        ) internal pure returns (bytes memory updatedCallData)
    {
        // callDataArray_encoded is made up to two parts
        // the first index in callDataArray_encoded is the encoded function selector
        // all the other indexes in callDataArray_encoded are the encoded parameters in the calldata

        // Sample input data with the first number as some uint256
        // 0xcb3c28c70000000000000000000000000000000000000000000000000000000001b8141a0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000
        // Sample input data with the first number as FFFF...FFFF showing what to replace with your own uint256
        // 0xcb3c28c7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000

        // The actual input data should be encoded, and this function will decode it

        // // TODO, replace this with parameter passed into the function
        // uint256[] memory callDataArray_encoded = new uint256[](8);
        // // The first item is the function selector
        // callDataArray_encoded[0] = 11251282108508751410786438484730905108789417455303941525318388289831404398469;
        // // All the other items are the parameters in the calldata
        // callDataArray_encoded[1] = 59366061376553713473998796024962062131803458935020796992885968363432024487682;
        // callDataArray_encoded[2] = 81020539002477591817371690194067367295966331559958661468723494924294866565208;
        // callDataArray_encoded[3] = 100300374173339321621784632522174664355417868639112964214766417266043084725725;
        // callDataArray_encoded[4] = 29513413370058131841359401452410130929006865798253763713474781928186275684624;
        // callDataArray_encoded[5] = 33790950113886600303280564701708316615200629527746212574075718599721353216;
        // callDataArray_encoded[6] = 22504664344615616;
        // callDataArray_encoded[7] = 0;

        uint256 callDataArrayLength = callDataArray_encoded.length;

        // Decode the encoded parameters
        callDataArray_encoded = decodeIntArray_injectIntAsFirstParameterInToCallData(callDataArray_encoded);

        uint256 functionSelector_encoded = callDataArray_encoded[0];

        uint8 functionSelectorLength = 4;
        uint256 updatedCallDataLength = functionSelectorLength + ((callDataArrayLength - 1)* 32);
        uint256 overwriteLocation = 32 + functionSelectorLength;

        // Declare the size of our updatedCallData
        updatedCallData = new bytes(updatedCallDataLength);

        // We must use assembly here so it's gas efficient
        assembly
        {
            // Iterate over the array
            // skip the first item since it's the function selector
            // skip the second item becuase that's the value we're overwriting anyways
            for { let i := 2 } lt(i, callDataArrayLength) { i := add(1, i) }
            {
                // Write each value to the bytes
                mstore(
                    // position of where we want to write the bytes within updatedCallData
                    add(updatedCallData, add(functionSelectorLength, mul(32, add(0, i)))),
                    // value of what we want to write to updatedCallData
                    mload(add(callDataArray_encoded, mul(32, add(1, i))))
                )
            }

            // Write the value we're overwriting to the bytes
            mstore(add(updatedCallData, overwriteLocation), quoteTokensToSpend_toInject)

            // Write the function selector to the bytes
            mstore(add(updatedCallData, functionSelectorLength), functionSelector_encoded)

            // Not sure what this does but it's needed to finish up
            mstore(updatedCallData, updatedCallDataLength)
        }

        return updatedCallData;
    }
}

abstract contract KeeperDaoLpp
{
    function borrow(address _token, uint256 _amount, bytes calldata _data) external virtual;
}

abstract contract GasToken2
{
    // function mint(uint256 value) public virtual;
    function free(uint256 value) public virtual returns (bool success);
}

contract Ninja_GasTokens
{
    // This is my local logic contract version of this function.
    // This assumes that this logic contract's proxy is holding gas tokens
    function freeGasTokens(
        uint gasTokens
        ) internal returns (bool success)
    {
        // When using the public contract GasToken2
        return GasToken2(0x0000000000b3F879cb30FE243b4Dfee438691c04).free(gasTokens);
    }
}

contract Ninja is
    WhitelistedOwners,
    WhitelistedKeeperDAOTransferProxys,
    SafeMath,
    // Uniswap_Utils,
    Kyber_Utils,
    ZRXv3_LibOrder,
    ZRXv3_Utils,
    WETH_Utils,
    Set_Utils,
    Logs,
    Encoding_Utils,
    Ninja_GasTokens
{

    // ***** Begin tradeSimpleRequirements functions ***** //

    // Trades if state meets simple requirements
    function tradeSimpleRequirements_encoded_buyOnSetSellOnKyber(
        uint256 etherToSpend,
        uint256[] memory intArray,
        uint256[] memory addressArray_Kyber_encoded,
        uint256[] memory addressArray_Set_encoded,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) public
    {
        tradeSimpleRequirements_buyOnSetSellOnKyber(
            etherToSpend, intArray,
            decodeAddressArray(addressArray_Kyber_encoded),
            decodeAddressArray(addressArray_Set_encoded),
            prePackedTradeCallDataArray_encoded);
    }

    // Trades if state meets simple requirements
    function tradeSimpleRequirements_buyOnSetSellOnKyber(
        uint256 etherToSpend,
        uint256[] memory intArray,
        address[] memory addressArray_Kyber,
        address[] memory addressArray_Set,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) public onlyWhitelist
    {
        // intArray[0] = minBidSize
        // intArray[1] = minInflowTokenQuantity
        // intArray[2] = minOutflowTokenQuantity
        // intArray[3] = expectedTokensToTrade,
        // intArray[4] = minKyberRateAcceptable,
        // intArray[5] = magicConverter_inflowTokensToShares,

        // addressArray_Kyber[0] = tokenAddress
        // addressArray_Kyber[1] = kyberReserve

        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(etherToSpend);

        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // Perform the checkSimpleRequirements and update etherToSpend
        etherToSpend = checkSimpleRequirements_buyOnSetSellOnKyber(
            beforeTradeDataArray_ints,
            ds,
            etherToSpend,
            intArray,
            addressArray_Kyber,
            addressArray_Set);

        // etherToSpend will be 0 if our checkSimpleRequirements failed
        if (etherToSpend == 0)
        {
            // If thats the case, return because it's not worth trading
            handlePostTradeActions(beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1]);
            return;
        }

        tradeWithinFlashLoan(ds, etherToSpend, prePackedTradeCallDataArray_encoded);

        handlePostTradeActions(beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1]);
    }

    function checkSimpleRequirements_buyOnSetSellOnKyber(
        uint256[] memory beforeTradeDataArray_ints,
        DiamondStorage_Properties storage ds,
        uint256 etherToSpend,
        uint256[] memory intArray,
        address[] memory addressArray_Kyber,
        address[] memory addressArray_Set
        ) internal returns (uint256 /*etherToSpend*/)
    {
        // intArray[0] = minBidSize
        // intArray[1] = minInflowTokenQuantity
        // intArray[2] = minOutflowTokenQuantity
        // intArray[3] = expectedTokensToTrade,
        // intArray[4] = minKyberRateAcceptable,
        // intArray[5] = magicConverter_inflowTokensToShares,

        // addressArray_Kyber[0] = tokenAddress
        // addressArray_Kyber[1] = kyberReserve

        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // Enforce minKyberRateAcceptable
        uint rate = KyberReserve(addressArray_Kyber[1]).getConversionRate(
            ERC20(addressArray_Kyber[0]), ERC20(ds.kyberEthTokenContract), intArray[3], block.number);
        if (rate < intArray[4])
        {
            logCodeWithValue_ifEnabled(7, rate);
            return 0;
        }

        // Enforce rebalance is not yet 100% complete
        uint256 remainingCurrentUnits = getRemainingCurrentUnits(addressArray_Set[1]);
        if (remainingCurrentUnits < intArray[0])
        {
            logCode_ifEnabled(19);
            return 0;
        }

        // Enforce etherAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
        uint256 etherAmountLeftFillable_Set = getMaxInflowTokenQuantityRemaining(
            intArray[0], intArray[1], intArray[2], remainingCurrentUnits);
        logCodeWithValue_ifEnabled(15, etherAmountLeftFillable_Set);
        etherToSpend = min256(etherToSpend, etherAmountLeftFillable_Set);

        // Enforce no trading dust
        if (etherToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(26);
            return 0;
        }

        return etherToSpend;
    }

    function tradeWithinFlashLoan_buyOnSetSellOnKyber(
        uint256 borrowedQuoteTokens,
        uint256 magicConverter_inflowTokensToShares,
        uint256[] memory intArray,
        address[] memory addressArray_Kyber,
        address[] memory addressArray_Set
        ) public onlyWhitelistedKeeperDAOTransferProxys
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 contractBalanceBefore = address(this).balance;

        // ****** Begin trading logic ****** //

        // wrap ETH that we're trading
        wrapEther(borrowedQuoteTokens);

        // Buy tokens on Set
        uint256 tokensReceived = tradeTokensOnSet(
            addressArray_Set, borrowedQuoteTokens, intArray[0], magicConverter_inflowTokensToShares);

	    // Sell them on Kyber
	    tradeTokensOnKyber(
	        0, addressArray_Kyber[0], tokensReceived, ds.kyberEthTokenContract,
	        69203865833239757421118596509098632427930889272824243351707071692229331386368,
	        1, 0x0000000000000000000000000000000000000000);

        // ****** End trading logic ****** //

        returnBorrowedQuoteTokensAndShareProfit(ds, borrowedQuoteTokens, contractBalanceBefore);
    }

    // Trades if state meets simple requirements
    function tradeSimpleRequirements_encoded_buyOnKyberSellOnSet(
        uint256 etherToSpend,
        uint256[] memory intArray,
        uint256[] memory addressArray_Kyber_encoded,
        uint256[] memory addressArray_Set_encoded,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) public
    {
        tradeSimpleRequirements_buyOnKyberSellOnSet(
            etherToSpend, intArray,
            decodeAddressArray(addressArray_Kyber_encoded),
            decodeAddressArray(addressArray_Set_encoded),
            prePackedTradeCallDataArray_encoded);
    }

    // Trades if state meets simple requirements
    function tradeSimpleRequirements_buyOnKyberSellOnSet(
        uint256 etherToSpend,
        uint256[] memory intArray,
        address[] memory addressArray_Kyber,
        address[] memory addressArray_Set,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) public onlyWhitelist
    {
        // intArray[0] = minBidSize
        // intArray[1] = minInflowTokenQuantity
        // intArray[2] = minOutflowTokenQuantity
        // intArray[3] = minKyberRateAcceptable,
        // intArray[4] = magicConverter_inflowTokensToShares,

        // addressArray_Kyber[0] = tokenAddress
        // addressArray_Kyber[1] = kyberReserve

        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(etherToSpend);

        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        etherToSpend = checkSimpleRequirements_buyOnKyberSellOnSet(
            beforeTradeDataArray_ints,
            ds,
            etherToSpend,
            intArray,
            addressArray_Kyber,
            addressArray_Set);

        // etherToSpend will be 0 if our checkSimpleRequirements failed
        if (etherToSpend == 0)
        {
            // If thats the case, return because it's not worth trading
            handlePostTradeActions(beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1]);
            return;
        }

        tradeWithinFlashLoan(ds, etherToSpend, prePackedTradeCallDataArray_encoded);

        handlePostTradeActions(beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1]);
    }

    function checkSimpleRequirements_buyOnKyberSellOnSet(
        uint256[] memory beforeTradeDataArray_ints,
        DiamondStorage_Properties storage ds,
        uint256 etherToSpend,
        uint256[] memory intArray,
        address[] memory addressArray_Kyber,
        address[] memory addressArray_Set
        ) internal returns (uint256 /*etherToSpend*/)
    {
        // intArray[0] = minBidSize
        // intArray[1] = minInflowTokenQuantity
        // intArray[2] = minOutflowTokenQuantity
        // intArray[3] = minKyberRateAcceptable,
        // intArray[4] = magicConverter_inflowTokensToShares,

        // addressArray_Kyber[0] = tokenAddress
        // addressArray_Kyber[1] = kyberReserve

        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // Enforce minKyberRateAcceptable
        uint rate = KyberReserve(addressArray_Kyber[1]).getConversionRate(
            ERC20(ds.kyberEthTokenContract), ERC20(addressArray_Kyber[0]), etherToSpend, block.number);
        if (rate < intArray[3])
        {
            logCodeWithValue_ifEnabled(7, rate);
            return 0;
        }

        // Enforce rebalance is not yet 100% complete
        uint256 remainingCurrentUnits = getRemainingCurrentUnits(addressArray_Set[1]);
        if (remainingCurrentUnits < intArray[0])
        {
            logCode_ifEnabled(19);
            return 0;
        }

        // Enforce effectiveEtherAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
        uint256 effectiveEtherAmountLeftFillable_Set = getMaxOutflowTokenQuantityRemaining(
            intArray[0], intArray[1], intArray[2], remainingCurrentUnits);
        // Reduce this number slightly becauase this is the outflow token and I don't want to do all the math to convert that using price
        effectiveEtherAmountLeftFillable_Set = safetifyThisNumber(effectiveEtherAmountLeftFillable_Set, 75);
        logCodeWithValue_ifEnabled(16, effectiveEtherAmountLeftFillable_Set);
        etherToSpend = min256(etherToSpend, effectiveEtherAmountLeftFillable_Set);

        // Enforce no trading dust
        if (etherToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(26);
            return 0;
        }

        return etherToSpend;
    }

    function tradeWithinFlashLoan_buyOnKyberSellOnSet(
        uint256 borrowedQuoteTokens,
        uint256[] memory intArray,
        address[] memory addressArray_Kyber,
        address[] memory addressArray_Set
        ) public onlyWhitelistedKeeperDAOTransferProxys
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 contractBalanceBefore = address(this).balance;

        // ****** Begin trading logic ****** //

        // Buy tokens on Kyber
        uint256 tokensReceived = tradeTokensOnKyber(
	        borrowedQuoteTokens, ds.kyberEthTokenContract, borrowedQuoteTokens, addressArray_Kyber[0],
	        69203865833239757421118596509098632427930889272824243351707071692229331386368,
	        1, 0x0000000000000000000000000000000000000000);

        // Sell tokens on Set
        uint outflowTokensReceivedFromTrade = tradeTokensOnSet(
            addressArray_Set, tokensReceived, intArray[0], intArray[4]);

        // Unwrap the WETH I received from the trade
        unwrapEther(outflowTokensReceivedFromTrade);

        // ****** End trading logic ****** //

        returnBorrowedQuoteTokensAndShareProfit(ds, borrowedQuoteTokens, contractBalanceBefore);
    }

    // Trades if state meets simple requirements
    function tradeSimpleRequirements_encoded_buyOnSetSellOn0xv3(
        uint256 etherToSpend,
        uint256[] memory intArray,
        Order_Encoded memory order_encoded,
        uint256[] memory addressArray_Set_encoded,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) public
    {
        tradeSimpleRequirements_buyOnSetSellOn0xv3(
            etherToSpend, intArray,
            decodeOrder(order_encoded),
            decodeAddressArray(addressArray_Set_encoded),
            prePackedTradeCallDataArray_encoded);
    }

    // Trades if state meets simple requirements
    function tradeSimpleRequirements_buyOnSetSellOn0xv3(
        uint256 etherToSpend,
        uint256[] memory intArray,
        Order memory order,
        address[] memory addressArray_Set,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) public onlyWhitelist
    {
        // intArray[0] = minBidSize
        // intArray[1] = etherToSpendSafetiplier
        // intArray[2] = magicConverter_inflowTokensToShares
        // intArray[3] = minInflowTokenQuantity
        // intArray[4] = minOutflowTokenQuantity

        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(etherToSpend);

        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        etherToSpend = checkSimpleRequirements_buyOnSetSellOn0xv3(
            beforeTradeDataArray_ints,
            ds,
            etherToSpend,
            intArray,
            order,
            addressArray_Set);

        // etherToSpend will be 0 if our checkSimpleRequirements failed
        if (etherToSpend == 0)
        {
            // If thats the case, return because it's not worth trading
            handlePostTradeActions(beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1]);
            return;
        }

        tradeWithinFlashLoan(ds, etherToSpend, prePackedTradeCallDataArray_encoded);

        handlePostTradeActions(beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1]);
    }

    function checkSimpleRequirements_buyOnSetSellOn0xv3(
        uint256[] memory beforeTradeDataArray_ints,
        DiamondStorage_Properties storage ds,
        uint256 etherToSpend,
        uint256[] memory intArray,
        Order memory order,
        address[] memory addressArray_Set
        ) internal returns (uint256 /*etherToSpend*/)
    {
        // intArray[0] = minBidSize
        // intArray[1] = etherToSpendSafetiplier
        // intArray[2] = magicConverter_inflowTokensToShares
        // intArray[3] = minInflowTokenQuantity
        // intArray[4] = minOutflowTokenQuantity

        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // Require the order to be fillable
        OrderInfo memory orderInfo = ZRXv3(ds.zrxV3ExchangeContract).getOrderInfo(order);
        if (orderInfo.orderStatus != uint8(OrderStatus.FILLABLE))
        {
            logCode_ifEnabled(3);
            return 0;
        }

        // Make sure we don't try to overfill the order
        // Update etherToSpend based on the tokenAmountLeftFillable_0x if the tokenAmountLeftFillable_0x is less than etherToSpend

        // Enforce tokenAmountLeftFillable_0x is not in ether here, it's actually tokens
        // So I need to convert tokenAmountLeftFillable_0x from tokens to ether so I can lower etherToSpend accordingly so we don't try and overfill the trade.
        uint256 etherAmountLeftFillable_0x = convertTokensToEther(
            order.makerAssetAmount,
            order.takerAssetAmount,
            order.takerAssetAmount - orderInfo.orderTakerAssetFilledAmount); // tokenAmountLeftFillable_0x
        etherAmountLeftFillable_0x = safetifyThisNumber(etherAmountLeftFillable_0x, 9999);
        logCodeWithValue_ifEnabled(17, etherAmountLeftFillable_0x);
        etherToSpend = min256(etherToSpend, etherAmountLeftFillable_0x);

        // Enforce no trading dust
        if (etherToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(24);
            return 0;
        }

        // Require the order maker to have enough assets to trade
        // inflowTokenAddress is makerTokenAddress
        // Make sure makers balance is enough to cover
        // Because the makerTokenAddress is WETH, I can simply update etherToSpend based like this
        etherToSpend = min256(etherToSpend, ERC20(addressArray_Set[2]).balanceOf(order.makerAddress));

        // Enforce no trading dust
        if (etherToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(25);
            return 0;
        }

        // Enforce rebalance is not yet 100% complete
        uint256 remainingCurrentUnits = getRemainingCurrentUnits(addressArray_Set[1]);
        if (remainingCurrentUnits < intArray[0])
        {
            logCode_ifEnabled(19);
            return 0;
        }

        // Enforce etherAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
        uint256 etherAmountLeftFillable_Set = getMaxInflowTokenQuantityRemaining(
            intArray[0], intArray[3], intArray[4], remainingCurrentUnits);
        logCodeWithValue_ifEnabled(15, etherAmountLeftFillable_Set);
        etherToSpend = min256(etherToSpend, etherAmountLeftFillable_Set);

        // Enforce no trading dust
        if (etherToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(26);
            return 0;
        }

        // Enforce etherToSpendSafetiplier
        etherToSpend = safetifyThisNumber(etherToSpend, intArray[1]);

        return etherToSpend;
    }

    function tradeWithinFlashLoan_buyOnSetSellOn0xv3(
        uint256 borrowedQuoteTokens,
        uint256[] memory intArray,
        Order memory order,
        bytes memory signature,
        address[] memory addressArray_Set
        ) public onlyWhitelistedKeeperDAOTransferProxys
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 contractBalanceBefore = address(this).balance;

        // ****** Begin trading logic ****** //

        // wrap ETH that we're trading
        wrapEther(borrowedQuoteTokens);

        // Buy tokens on Set
        uint256 tokensReceived = tradeTokensOnSet(
            addressArray_Set, borrowedQuoteTokens, intArray[0], intArray[2]);

        // Sell tokens on 0xv3
        FillResults memory fillResults = tradeTokensOn0xv3(
            order, tokensReceived, signature, calculateProtocolFee(1));

        // Unwrap the WETH I received from 0xv2 trade
        // So if i'm selling tokens, the maker asset should be weth
        unwrapEther(fillResults.makerAssetFilledAmount);

        // ****** End trading logic ****** //

        returnBorrowedQuoteTokensAndShareProfit(ds, borrowedQuoteTokens, contractBalanceBefore);
    }

    // Trades if state meets simple requirements
    function tradeSimpleRequirements_encoded_buyOn0xv3SellOnSet(
        uint256 etherToSpend,
        uint256[] memory intArray,
        Order_Encoded memory order_encoded,
        uint256[] memory addressArray_Set_encoded,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) public
    {
        tradeSimpleRequirements_buyOn0xv3SellOnSet(
            etherToSpend, intArray,
            decodeOrder(order_encoded),
            decodeAddressArray(addressArray_Set_encoded),
            prePackedTradeCallDataArray_encoded);
    }

    // Trades if state meets simple requirements
    function tradeSimpleRequirements_buyOn0xv3SellOnSet(
        uint256 etherToSpend,
        uint256[] memory intArray,
        Order memory order,
        address[] memory addressArray_Set,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) public onlyWhitelist
    {
        // intArray[0] = minBidSize
        // intArray[1] = etherToSpendSafetiplier
        // intArray[2] = magicConverter_inflowTokensToShares
        // intArray[3] = minInflowTokenQuantity
        // intArray[4] = minOutflowTokenQuantity

        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(etherToSpend);

        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        etherToSpend = checkSimpleRequirements_buyOn0xv3SellOnSet(
            beforeTradeDataArray_ints,
            ds,
            etherToSpend,
            intArray,
            order,
            addressArray_Set);

        // etherToSpend will be 0 if our checkSimpleRequirements failed
        if (etherToSpend == 0)
        {
            // If thats the case, return because it's not worth trading
            handlePostTradeActions(beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1]);
            return;
        }

        tradeWithinFlashLoan(ds, etherToSpend, prePackedTradeCallDataArray_encoded);

        handlePostTradeActions(beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1]);
    }

    function checkSimpleRequirements_buyOn0xv3SellOnSet(
        uint256[] memory beforeTradeDataArray_ints,
        DiamondStorage_Properties storage ds,
        uint256 etherToSpend,
        uint256[] memory intArray,
        Order memory order,
        address[] memory addressArray_Set
        ) internal returns (uint256 /*etherToSpend*/)
    {
        // intArray[0] = minBidSize
        // intArray[1] = etherToSpendSafetiplier
        // intArray[2] = magicConverter_inflowTokensToShares
        // intArray[3] = minInflowTokenQuantity
        // intArray[4] = minOutflowTokenQuantity

        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress

        // Require the order to be fillable
        OrderInfo memory orderInfo = ZRXv3(ds.zrxV3ExchangeContract).getOrderInfo(order);
        if (orderInfo.orderStatus != uint8(OrderStatus.FILLABLE))
        {
            logCode_ifEnabled(3);
            return 0;
        }

        // Make sure we don't try to overfill the order
        // Update etherToSpend based on the etherAmountLeftFillable if the etherAmountLeftFillable is less than etherToSpend
        // uint256 etherAmountLeftFillable_0x = order.takerAssetAmount - orderInfo.orderTakerAssetFilledAmount;
        logCodeWithValue_ifEnabled(18, order.takerAssetAmount - orderInfo.orderTakerAssetFilledAmount);
        etherToSpend = min256(etherToSpend, order.takerAssetAmount - orderInfo.orderTakerAssetFilledAmount);

        // Enforce no trading dust
        if (etherToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(24);
            return 0;
        }

        // Require the order maker to have enough assets to trade
        // inflowTokenAddress is makerTokenAddress
        // Convert etherToSpend to tokens so I can make sure makers balance is enough to cover
        if (
            ERC20(addressArray_Set[2]).balanceOf(order.makerAddress) // makersMakerTokenBalance
            <
            convertEtherToTokens(order.takerAssetAmount, order.makerAssetAmount, etherToSpend) // etherToSpendConvertedToTokens
            )
        {
            logCode_ifEnabled(25);
            return 0;
        }

        // Enforce rebalance is not yet 100% complete
        uint256 remainingCurrentUnits = getRemainingCurrentUnits(addressArray_Set[1]);
        if (remainingCurrentUnits < intArray[0])
        {
            logCode_ifEnabled(19);
            return 0;
        }

        // Enforce effectiveEtherAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
        uint256 effectiveEtherAmountLeftFillable_Set = getMaxOutflowTokenQuantityRemaining(
            intArray[0], intArray[3], intArray[4], remainingCurrentUnits);
        // Reduce this number slightly becauase this is the outflow token and I don't want to do all the math to convert that using price
        effectiveEtherAmountLeftFillable_Set = safetifyThisNumber(effectiveEtherAmountLeftFillable_Set, 75);
        logCodeWithValue_ifEnabled(16, effectiveEtherAmountLeftFillable_Set);
        etherToSpend = min256(etherToSpend, effectiveEtherAmountLeftFillable_Set);

        // Enforce no trading dust
        if (etherToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(26);
            return 0;
        }

        // Enforce etherToSpendSafetiplier
        etherToSpend = safetifyThisNumber(etherToSpend, intArray[1]);

        return etherToSpend;
    }

    function tradeWithinFlashLoan_buyOn0xv3SellOnSet(
        uint256 borrowedQuoteTokens,
        uint256[] memory intArray,
        Order memory order,
        bytes memory signature,
        address[] memory addressArray_Set
        ) public onlyWhitelistedKeeperDAOTransferProxys
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 contractBalanceBefore = address(this).balance;

        // ****** Begin trading logic ****** //

        // wrap ETH that we're trading
        wrapEther(borrowedQuoteTokens);

        // Buy tokens on 0xv3
        FillResults memory fillResults = tradeTokensOn0xv3(
            order, borrowedQuoteTokens, signature, calculateProtocolFee(1));

        // Sell tokens on Set
        uint outflowTokensReceivedFromTrade = tradeTokensOnSet(
            addressArray_Set, fillResults.makerAssetFilledAmount, intArray[0], intArray[2]);

        // Unwrap the WETH I received from the trade
        unwrapEther(outflowTokensReceivedFromTrade);

        // ****** End trading logic ****** //

        returnBorrowedQuoteTokensAndShareProfit(ds, borrowedQuoteTokens, contractBalanceBefore);
    }


    // // Trades if state meets simple requirements
    // function tradeSimpleRequirements_buyOnSetSellOnUniswap(
    //     uint256 etherToSpend,
    //     uint256 minUniswapEthQuantityAcceptable,
    //     uint256 minBidSize_Set,
    //     address[] memory addressArray_Uniswap,
    //     address[] memory addressArray_Set
    //     ) public onlyWhitelist
    // {
    //     // addressArray_Uniswap[0] = uniswapContract,

    //     // addressArray_Set[0] = rebalanceAuctionModuleAddress
    //     // addressArray_Set[1] = setTokenAddress
    //     // addressArray_Set[2] = inflowTokenAddress
    //     // addressArray_Set[3] = outflowTokenAddress

    //     // Before trade actions
    //     uint256 contractBalanceBefore = address(this).balance;
    //     uint256 gasBefore = gasleft();

    //     // Enforce rebalance mode
    //     if (SetToken(addressArray_Set[1]).rebalanceState() != 2)
    //     {
    //         // logCode_ifEnabled(13);
    //         return;
    //     }

    //     // Enforce rebalance is not yet 100% complete
    //     if (getRemainingCurrentUnits(addressArray_Set[1]) < minBidSize_Set)
    //     {
    //         // logCode_ifEnabled(19);
    //         return;
    //     }

    //     // Enforce minUniswapEthQuantityAcceptable
    //     uint256 uniswapEthBalance = address(addressArray_Uniswap[0]).balance;
    //     if (uniswapEthBalance < minUniswapEthQuantityAcceptable)
    //     {
    //         logCodeWithValue_ifEnabled(10, uniswapEthBalance);
    //         handlePostTradeActions(contractBalanceBefore, gasBefore);
    //         return;
    //     }

    //     // Enforce etherAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
    //     uint256 etherAmountLeftFillable_Set = getMaxInflowTokenQuantityRemaining(addressArray_Set[1], minBidSize_Set);
    //     logCodeWithValue_ifEnabled(15, etherAmountLeftFillable_Set);
    //     etherToSpend = min256(etherToSpend, etherAmountLeftFillable_Set);

    //     // wrap ETH that we're trading
    //     wrapEther(etherToSpend);

    //     // Buy tokens on Set
    //     uint256 tokensReceived = tradeTokensOnSet(
    //         addressArray_Set, etherToSpend, minBidSize_Set);

    //     // Sell them on Uniswap
    //     sellTokensOnUniswap(
    //         addressArray_Uniswap[0], tokensReceived, 1,
    //         69203865833239757421118596509098632427930889272824243351707071692229331386368);

    //     handlePostTradeActions(contractBalanceBefore, gasBefore);
    // }

    // // Trades if state meets simple requirements
    // function tradeSimpleRequirements_buyOnUniswapSellOnSet(
    //     uint256 etherToSpend,
    //     uint256 maxUniswapEthQuantityAcceptable,
    //     uint256 minBidSize_Set,
    //     address[] memory addressArray_Uniswap,
    //     address[] memory addressArray_Set
    //     ) public onlyWhitelist
    // {
    //     // addressArray_Uniswap[0] = uniswapContract,

    //     // addressArray_Set[0] = rebalanceAuctionModuleAddress
    //     // addressArray_Set[1] = setTokenAddress
    //     // addressArray_Set[2] = inflowTokenAddress
    //     // addressArray_Set[3] = outflowTokenAddress

    //     // Before trade actions
    //     uint256 contractBalanceBefore = address(this).balance;
    //     uint256 gasBefore = gasleft();

    //     // Enforce rebalance mode
    //     if (SetToken(addressArray_Set[1]).rebalanceState() != 2)
    //     {
    //         // logCode_ifEnabled(13);
    //         return;
    //     }

    //     // Enforce rebalance is not yet 100% complete
    //     if (getRemainingCurrentUnits(addressArray_Set[1]) < minBidSize_Set)
    //     {
    //         // logCode_ifEnabled(19);
    //         return;
    //     }

    //     // Enforce maxUniswapEthQuantityAcceptable
    //     uint256 uniswapEthBalance = address(addressArray_Uniswap[0]).balance;
    //     if (uniswapEthBalance > maxUniswapEthQuantityAcceptable)
    //     {
    //         logCodeWithValue_ifEnabled(10, uniswapEthBalance);
    //         handlePostTradeActions(contractBalanceBefore, gasBefore);
    //         return;
    //     }

    //     // Enforce effectiveEtherAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
    //     uint256 effectiveEtherAmountLeftFillable_Set = getMaxOutflowTokenQuantityRemaining(addressArray_Set[1], minBidSize_Set);
    //     // Reduce this number slightly becauase this is the outflow token and I don't want to do all the math to convert that using price
    //     effectiveEtherAmountLeftFillable_Set = safetifyThisNumber(effectiveEtherAmountLeftFillable_Set, 75);
    //     logCodeWithValue_ifEnabled(16, effectiveEtherAmountLeftFillable_Set);
    //     etherToSpend = min256(etherToSpend, effectiveEtherAmountLeftFillable_Set);

    //     // Buy tokens on Uniswap
    //     uint256 tokensReceived = buyTokensOnUniswap(
    //         addressArray_Uniswap[0], etherToSpend, 1,
    //         69203865833239757421118596509098632427930889272824243351707071692229331386368);

    //     // Sell tokens on Set
    //     uint outflowTokensReceivedFromTrade = tradeTokensOnSet(
    //         addressArray_Set, tokensReceived, minBidSize_Set);

    //     // Unwrap the WETH I received from the trade
    //     unwrapEther(outflowTokensReceivedFromTrade);

    //     handlePostTradeActions(contractBalanceBefore, gasBefore);
    // }

    // ***** End tradeSimpleRequirements functions ***** //

    // ***** Begin Utility functions ***** //

    function tradeWithinFlashLoan(
        DiamondStorage_Properties storage ds,
        uint256 etherToSpend,
        uint256[] memory prePackedTradeCallDataArray_encoded
        ) internal
    {
        // Flash loan from KeeperDAO, and trade via appropriate tradeWithinFlashLoan
        KeeperDaoLpp(ds.keeperDaoLppContract_Eth).borrow(
            ds.keeperDaoEthTokenContract,
            etherToSpend,
            // Update the quantity we are trading in the calldata, since that could have changed somewhere along the way
            injectIntAsFirstParameterInToCallData(prePackedTradeCallDataArray_encoded, etherToSpend));
    }

    function returnBorrowedQuoteTokensAndShareProfit(
        DiamondStorage_Properties storage ds,
        uint256 borrowedQuoteTokens,
        uint256 contractBalanceBefore
        ) internal
    {
        // Calculate the sharableProfitQuantity
        uint256 profitToShareWithKeeperDAO = 0;
        // Calculate profit made in quoteTokens
        if (address(this).balance > contractBalanceBefore)
        {
            // We've made profit in quoteTokens
            profitToShareWithKeeperDAO = safeSub(address(this).balance, contractBalanceBefore);
            // Determine how much profit to share with KeeperDAO in a gas efficient way
            profitToShareWithKeeperDAO = safeDiv(safeMul(profitToShareWithKeeperDAO, 1000000000000000000), ds.divider_profitToShareWithKeeperDAO);
        }
        // Else, we had a loss in quoteTokens.
        // This means one of two things:
        // 1.) The profit was actually made in baseTokens (and that may be left as dust)
        // 2.) The trade was not proftiable!
        //     NOTE: A non-profitable trade will NOT be enforced/reverted here.
        //           It's up to the other trade logic to determine whether or not we should revert
        //           This function will attempt to return the amount borrowed in quoteTokens even if that means this transaction results in a loss
        //           If this function tries to return more quoteTokens than it has,
        //              the tx will revert/fail because that's not possible.
        //           If this function succeeds in returning the borrowed quoteTokens,
        //              it's up to the other trade logic to decide if the trade was profitable or if it needs reverted

        // Return the borrowedQuoteTokens + profitToShareWithKeeperDAO
        (bool success, ) = ds.keeperDaoLppContract_Eth.call{value:
            safeAdd(borrowedQuoteTokens, profitToShareWithKeeperDAO)
        }("");
        require(success, "Transfer failed.");
    }

    function decodeOrder(
        Order_Encoded memory order_encoded
        ) internal pure returns (Order memory decodedOrder)
    {
        decodedOrder = Order({
            makerAddress:decodeAddress(order_encoded.makerAddress_encoded),
            takerAddress:order_encoded.takerAddress,
            feeRecipientAddress:order_encoded.feeRecipientAddress,
            senderAddress:order_encoded.senderAddress,
            makerAssetAmount:decodeInt(order_encoded.makerAssetAmount_encoded),
            takerAssetAmount:decodeInt(order_encoded.takerAssetAmount_encoded),
            makerFee:order_encoded.makerFee,
            takerFee:order_encoded.takerFee,
            expirationTimeSeconds:decodeInt(order_encoded.expirationTimeSeconds_encoded),
            salt:decodeInt(order_encoded.salt_encoded),
            makerAssetData:decode0xAssetData(order_encoded.makerAssetData_encoded),
            takerAssetData:decode0xAssetData(order_encoded.takerAssetData_encoded),
            makerFeeAssetData:order_encoded.makerFeeAssetData,
            takerFeeAssetData:order_encoded.takerFeeAssetData
        });
        return decodedOrder;
    }

    function handlePostTradeActions(
        uint256 contractBalanceBefore,
        uint256 gasBefore
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // The 0x integration here tends to get a lot of dust in base tokens as well as quote tokens
        // So this number needs to either be pretty high (AKA 0.01 quote tokens)
        // or we need to calcualte the dust in both base and quote which is pretty time consuming and costs a lot of gas
        // To avoid timing consuming and expensive calulations, Just hard code in a number here that works most all the time
        // And put trust into the ninja code that's calling this contract
        // uint256 maxTradeLossAcceptable = 10000000000000000;  // I was seeing code 5's due to dust with this amount...
        uint256 maxTradeLossAcceptable = 30000000000000000;

        // If I lost ether during this charade (considering the max amount we're allowed to lose)
        if (address(this).balance + maxTradeLossAcceptable < contractBalanceBefore)
        {
            // Do not let it trade since it lost ether!
            // I may have tokens left over though so I may actually want to let this trade go through in some cases.
            // Question is, how complicated is that logic and is it worth it?
            // I'll need to check before and after token balances and then value them with respect to ETH
            revert("Code 5");
        }
        // else profit or break even
        else
        {
            // Calculate the profit i'm making
            // uint256 profit_eth_weiUnits = safeSub(address(this).balance, contractBalanceBefore);
            // TODO, should I do something with this?

            // Only consider spending gasTokens to save gas if this tx gas price is high enough to make it worth it
            // I may send a trade through at SafeLow and it doesn't make sense to kill a contract
            // So check gasPriceThresholdForSpendingGasTokens
            if (tx.gasprice > ds.gasPriceThresholdForSpendingGasTokens)
            {
                // Have to add in the gas cost to perform a transaction call on an empty function
                // For this i'm using 21483
                uint256 gasUsed = safeSub(safeAdd(21483, gasBefore), gasleft());
                // logCodeWithValue_ifEnabled(8, gasUsed);
                // Determine how many contracts to kill to save gas

                // If we are less than our threshold, do not kill any
                if (gasUsed < ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx)
                {
                    // Do not kill any contracts
                }
                // Else if we are greater than our threshold
                else
                {
                    // Calculate how many contracts to kill based on how much gas we've used
                    uint256 numOfContractsToKill = safeDiv(gasUsed, ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx);
                    // logCodeWithValue_ifEnabled(9, numOfContractsToKill);
                    if (numOfContractsToKill > 0)
                    {
                        freeGasTokens(numOfContractsToKill);
                    }
                }
            }
        }
    }

    function getMinQuoteTokensToSpendAllowed(
        uint quoteTokensToSpend
        ) internal view returns (uint minQuoteTokensToSpendAllowed)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        return safeDiv(quoteTokensToSpend, ds.minQuoteTokenAllowedToSpendDivider);
    }

    function getBeforeTradeDataArray_ints(
        uint256 etherToSpend
        ) internal view returns (uint256[] memory beforeTradeDataArray_ints)
    {
        beforeTradeDataArray_ints = new uint256[](3);
        beforeTradeDataArray_ints[0] = address(this).balance;  // contractBalanceBefore
        beforeTradeDataArray_ints[1] = gasleft();  // gasBefore
        beforeTradeDataArray_ints[2] = getMinQuoteTokensToSpendAllowed(etherToSpend);  // minEtherToSpendAllowed
        return beforeTradeDataArray_ints;
    }

    // ***** End Utility functions ***** //

}
