pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }
    
    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds) 
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract StorageContract_Properties
{
    struct DiamondStorage_Properties 
    {
        // addressArray
        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        
        // intArray
        uint256 doLogEvents;  // 1 means yes, 0 means no
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.  
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in 
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number. 
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
    }
    
    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract WhitelistedOwners is 
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

contract Logs is 
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0xv3 order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set 
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth filling a dust order
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it
    
    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);
    
    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }
    
    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

contract SafeMath 
{
    function safeMul(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        require(c / a == b);
        require(
            c / a == b, 
            "UINT256_OVERFLOW"
        );
        return c;
    }

    function safeDiv(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a / b;
        return c;
    }

    function safeSub(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        require(b <= a);
        require(
            b <= a,
            "UINT256_UNDERFLOW"
        );
        return a - b;
    }

    function safeAdd(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a + b;
        require(c >= a);
        require(
            c >= a,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    // function max64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    // function min64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a < b ? a : b;
    // }

    // function max256(
    //     uint256 a, uint256 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    function min256(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        return a < b ? a : b;
    }
    
    function safetifyThisNumber(
        uint256 value,
        uint256 factor
        ) internal pure returns (uint256)
    {
        // Multiply by ~99%, just to be safe so there isn't a round up problem
        // In solididty, it's safer to underfill an order by a hair than to risk overfilling it resulting in an exception
        // To multiply by 99% simply pass in the number 99.  
        // It will mulitply by 99 then divide by 100 in order to achieve the desired result without having an exception
        // To multiply by 99.99%, simply pass in the number 9999. 
        // It will mulitply by 9999 then divide by 10000 in order to achieve the desired result without having an exception
        value = safeMul(value, factor);
        value = safeDiv(value, safeAdd(factor, 1));
        return value;
    }
    
    function calculatePriceForTrade(
        uint256 quoteTokenQuantity,
        uint256 baseTokenQuantity,
        uint256 quoteTokenDecimals,
        uint256 baseTokenDecimals
        ) internal pure returns (uint256 price) 
    {
        // Actual formula:
        // 1000000000000000000 * (quoteTokenQuantity / quoteTokenDecimals) / (baseTokenQuantity / baseTokenDecimals)
        // But for solidity I need to reformat this expression so that all the mulitplcation happens first to avoid dividing down to zero
        // New formula:
        // 1000000000000000000 * quoteTokenQuantity * baseTokenDecimals / quoteTokenDecimals / baseTokenQuantity
        // Note: Price on protcols like Kyber is always using 18 decimals, hence the 1000000000000000000 value
        return safeDiv(safeDiv(safeMul(safeMul(1000000000000000000, quoteTokenQuantity), baseTokenDecimals), quoteTokenDecimals), baseTokenQuantity);
    }
}

abstract contract ERC20 
{
    function balanceOf(address tokenOwner) public view virtual returns (uint balance);
    function transfer(address toAddress, uint tokens) public virtual returns (bool success);
    function approve(address _spender, uint256 _value) public virtual returns (bool success);
    function allowance(address _owner, address _spender) public view virtual returns (uint256 remaining);
    // function decimals() public view returns (uint8 decimals);
}

abstract contract SetToken is
    ERC20
{
    // function rebalanceState() public view returns (uint8 state);
    // function getCombinedTokenArray() external view returns (address[] memory);
    function getBidPrice(uint256 _quantity) public view virtual returns (uint256[] memory, uint256[] memory);
    function getBiddingParameters() external view virtual returns (uint256[] memory);
}

abstract contract RebalanceAuctionModule
{
    function bidAndWithdraw(address _rebalancingSetToken, uint256 _quantity, bool _allowPartialFill) external virtual;
}

abstract contract CTokenBidderContract
{
    function getAddressAndBidPriceArray(
        address _rebalancingSetToken, uint256 _quantity
        ) external view virtual returns (address[] memory combinedTokenArray, uint256[] memory inflowUnitsArray, uint256[] memory outflowUnitsArray);
}

contract Set_Utils is
    SafeMath
{
    function tradeTokensOnSet(
        address[] memory addressArray_Set, 
        uint256 inflowTokensToSpend, 
        uint256 minBidSize,
        uint256 magicConverter_inflowTokensToShares
        ) internal returns (uint outflowTokensReceivedFromTrade)
    {
        // addressArray_Set[0] = rebalanceAuctionModuleAddress
        // addressArray_Set[1] = setTokenAddress
        // addressArray_Set[2] = inflowTokenAddress
        // addressArray_Set[3] = outflowTokenAddress
        
        // Convert inflowTokensToSpend to shares
        uint256 shares = convertInflowTokenQuantityToSharesQuantity(inflowTokensToSpend, minBidSize, magicConverter_inflowTokensToShares);
        
        // Get before-trade balance of token i'm expecting to receive
        uint outflowTokenBalance_beforeTrade = ERC20(addressArray_Set[3]).balanceOf(address(this));
        
        // Trade.  Send inflowTokenAddress and receive outflowTokenAddress
        RebalanceAuctionModule rebalanceAuctionModule = RebalanceAuctionModule(addressArray_Set[0]);
        rebalanceAuctionModule.bidAndWithdraw(addressArray_Set[1], shares, true);
        
        // Get after-trade balance of token i'm expecting to receive
        uint outflowTokenBalance_afterTrade = ERC20(addressArray_Set[3]).balanceOf(address(this));
        
        outflowTokensReceivedFromTrade = safeSub(outflowTokenBalance_afterTrade, outflowTokenBalance_beforeTrade);
        // Event_Test("outflowTokensReceivedFromTrade", outflowTokensReceivedFromTrade);
        // Return token amount i received
        return outflowTokensReceivedFromTrade;
    }
    
    function convertInflowTokenQuantityToSharesQuantity(
        uint256 inflowTokensToSpend,
        uint256 minBidSize,
        uint256 magicConverter_inflowTokensToShares
        ) internal pure returns (uint256 shares) 
    {
        // # Formula: shares = inflowTokensToSpend_weiUnits / magicNumber_weiUnits
        // # Formula solidity: shares = safeDiv(safeMul(inflowTokensToSpend_weiUnits, 1000000000000000000), magicNumber_weiUnits)
        shares = safeDiv(safeMul(inflowTokensToSpend, 1000000000000000000), magicConverter_inflowTokensToShares);
        // I'm reducing this number slightly to be on the safe side so we don't try to over trade
        // The consiquence of this is i'll be left with dust after trading, but at least it works
        // shares = safetifyThisNumber(shares, 999999);  // This might be too large
        shares = safetifyThisNumber(shares, 9999);
        // Enforce shares limitations with respect to sharesQuantity
        shares = enforceDivisibleMinBidSize(shares, minBidSize);
        return shares;
    }
    
    function enforceDivisibleMinBidSize(
        uint256 shares, 
        uint256 minBidSize
        ) internal pure returns (uint256)
    {
        // So if minBidSize = 100000, and shares = 912345. It needs to return 900000.
        // return safeMul(safeDiv(shares, minBidSize), minBidSize);
        
        shares = safeMul(safeDiv(shares, minBidSize), minBidSize);
        // Also enforce that shares must be greater than minBidSize
        if (shares < minBidSize) 
        {
            return 0;
        } 
        else 
        {
            return shares;
        }
    }
    
    function getRemainingCurrentUnits(
        address setTokenAddress
        ) internal view returns (uint256 remainingCurrentUnits)
    {
        SetToken setToken = SetToken(setTokenAddress);
        uint256[] memory biddingParameters = setToken.getBiddingParameters();
        return biddingParameters[1];
    }
    
    function getMaxInflowTokenQuantityRemaining(
        // bool useCTokenBidderContract,
        // address auctionModule,
        // address setTokenAddress,
        uint256 minBidSize,
        uint256 minInflowTokenQuantity,
        uint256 minOutflowTokenQuantity,
        uint256 remainingCurrentUnits
        ) internal pure returns (uint256 maxInflowTokenQuantity)
    {
        maxInflowTokenQuantity;
        uint256 dontCare;
        (
            maxInflowTokenQuantity, 
            dontCare
        ) = getMaxFlowTokenQuantitiesRemaining(
            // useCTokenBidderContract, auctionModule, setTokenAddress, 
            minBidSize, minInflowTokenQuantity, minOutflowTokenQuantity, remainingCurrentUnits);
        return maxInflowTokenQuantity;
    }
    
    function getMaxOutflowTokenQuantityRemaining(
        // bool useCTokenBidderContract,
        // address auctionModule,
        // address setTokenAddress,
        uint256 minBidSize,
        uint256 minInflowTokenQuantity,
        uint256 minOutflowTokenQuantity,
        uint256 remainingCurrentUnits
        ) internal pure returns (uint256 maxOutflowTokenQuantity)
    {
        uint256 dontCare;
        maxOutflowTokenQuantity;
        (
            dontCare, 
            maxOutflowTokenQuantity
        ) = getMaxFlowTokenQuantitiesRemaining(
            // useCTokenBidderContract, auctionModule, setTokenAddress, 
            minBidSize, minInflowTokenQuantity, minOutflowTokenQuantity, remainingCurrentUnits);
        return maxOutflowTokenQuantity;
    }
    
    function getMaxFlowTokenQuantitiesRemaining(
        uint256 minBidSize,
        uint256 minInflowTokenQuantity,
        uint256 minOutflowTokenQuantity, 
        uint256 remainingCurrentUnits
        ) internal pure returns (uint256 maxInflowTokenQuantity, uint256 maxOutflowTokenQuantity)
    {
        // minBidSize, remainingCurrentUnits = getBiddingParameters()
        // uint256 remainingCurrentUnits = getRemainingCurrentUnits(setTokenAddress);
        // self.remainingCurrentShares_weiUnits = float(self.remainingCurrentUnits_weiUnits) / float(self.minBidSize_weiUnits)
        uint256 remainingCurrentShares = safeDiv(remainingCurrentUnits, minBidSize);
        
        // self.maxInflowTokenQuantity_weiUnits = int(int(self.remainingCurrentShares_weiUnits) * self.minInflowTokenQuantity_weiUnits)
        maxInflowTokenQuantity = safeMul(remainingCurrentShares, minInflowTokenQuantity);
        // self.maxOutflowTokenQuantity_weiUnits = int(int(self.remainingCurrentShares_weiUnits) * self.minOutflowTokenQuantity_weiUnits)
        maxOutflowTokenQuantity = safeMul(remainingCurrentShares, minOutflowTokenQuantity);
        
        return (maxInflowTokenQuantity, maxOutflowTokenQuantity);
    }
}

abstract contract Uniswap
{
    function ethToTokenSwapInput(uint256 min_tokens, uint256 deadline) public payable virtual returns (uint256 out);
    function tokenToEthSwapInput(uint256 tokens_sold, uint256 min_eth, uint256 deadline) public virtual returns (uint256 out);
    function tokenToTokenSwapInput(
        uint256 tokens_sold, uint256 min_tokens_bought, uint256 min_eth_bought, 
        uint256 deadline, address token_addr) public virtual returns (uint256 out);
}

abstract contract KyberProxy 
{
    function trade(
        address src, uint srcAmount, address dest, address destAddress, 
        uint maxDestAmount, uint minConversionRate, address walletId
        ) public payable virtual returns(uint);
}

abstract contract KyberReserve
{
    function getConversionRate(
        ERC20 src, ERC20 dest, uint srcQty, uint blockNumber
        ) public view virtual returns(uint);
}

contract Uniswap_Utils is
    SafeMath
{
    function tradeTokensOnUniswap(
        address uniswapContract_tokenWereSelling,
        uint256 tokens_sold, 
        uint256 min_tokens_bought,
        address tokenWereBuying
        ) internal returns (uint256) 
    {
        // TODO tokenToTokenSwapInput
        // For some reason token to token trades have 1 as min_eth_bought.  No idea why this argument is even here
        return Uniswap(uniswapContract_tokenWereSelling).tokenToTokenSwapInput(
            tokens_sold, min_tokens_bought, 1, 
            69203865833239757421118596509098632427930889272824243351707071692229331386368, 
            tokenWereBuying);
    }
    
    function buyTokensOnUniswap(
        address uniswapContract,
        uint256 msgValue,
        uint256 min_tokens,
        uint256 deadline
        ) internal returns (uint256) 
    {
        // Send ether quantity as the msgValue if you're buying tokens
        // And send 0 as the msgValue if you're selling tokens
        return Uniswap(uniswapContract).ethToTokenSwapInput{value: msgValue}(min_tokens, deadline);
    }
    
    function sellTokensOnUniswap(
        address uniswapContract,
        uint256 tokens_sold, 
        uint256 min_eth, 
        uint256 deadline
        ) internal returns (uint256) 
    {
        return Uniswap(uniswapContract).tokenToEthSwapInput(tokens_sold, min_eth, deadline);
    }
    
    function getTokenForTokenTradePrice_Uniswap(
        address tokenToSell,
        address uniswapContract_tokenToSell,
        uint256 decimals_tokenToSell,
        uint256 quantity_tokenToSell,
        address tokenToBuy,
        address uniswapContract_tokenToBuy,
        uint256 decimals_tokenToBuy,
        bool tokenToSellIsQuoteToken
        ) internal view returns (uint256 price)
    {
        // On Uniswap, to trade token for token I must sell Token A for ETH, then use that ETH to buy Token B. 
        // So calculating price is done in that same fashion.
        
        uint256 ethToReceive = getSellReturn_Uniswap(uniswapContract_tokenToSell, tokenToSell, quantity_tokenToSell);
        uint256 quantity_tokensToReceive = getBuyReturn_Uniswap(uniswapContract_tokenToBuy, tokenToBuy, ethToReceive);
        
        // I can calculate price using quantity_tokenToSell as the input and quantity_tokensToReceive as the output
        // To determine price, I need to know which token is the quote token and which is the base token
        if (tokenToSellIsQuoteToken)
        {
            return calculatePriceForTrade(
                quantity_tokenToSell, quantity_tokensToReceive, decimals_tokenToSell, decimals_tokenToBuy);
        }
        else
        {
            return calculatePriceForTrade(
                quantity_tokensToReceive, quantity_tokenToSell, decimals_tokenToBuy, decimals_tokenToSell);
        }
        
    }
    
    function getBuyReturn_Uniswap(
        address uniswapContract,
        address token,
        uint256 ethToSpend
        ) internal view returns (uint256 tokensToReceive)
    {
        uint256 inputReserve = address(uniswapContract).balance;
        uint256 outputReserve = ERC20(token).balanceOf(uniswapContract);
        
        // uint256 numerator = ethToSpend * outputReserve * 997;
        // uint256 denominator = inputReserve * 1000 + ethToSpend * 997;
        // uint256 outputAmount = numerator / denominator;
        
        // safeDiv(uint256 a, uint256 b)
        // safeMul(uint256 a, uint256 b)
        // safeAdd(uint256 a, uint256 b)
        uint256 numerator = safeMul(safeMul(ethToSpend, outputReserve), 997);
        uint256 denominator = safeAdd(safeMul(inputReserve, 1000), safeMul(ethToSpend, 997));
        uint256 outputAmount = safeDiv(numerator, denominator);
        return outputAmount;
    }
    
    function getSellReturn_Uniswap(
        address uniswapContract,
        address token,
        uint256 tokensToSpend
        ) internal view returns (uint256 ethToReceive)
    {
        uint256 inputReserve = ERC20(token).balanceOf(uniswapContract);
        uint256 outputReserve = address(uniswapContract).balance;
        
        // uint256 numerator = tokensToSpend * outputReserve * 997;
        // uint256 denominator = inputReserve * 1000 + tokensToSpend * 997;
        // uint256 outputAmount = numerator / denominator;
        
        // safeDiv(uint256 a, uint256 b)
        // safeMul(uint256 a, uint256 b)
        // safeAdd(uint256 a, uint256 b)
        uint256 numerator = safeMul(safeMul(tokensToSpend, outputReserve), 997);
        uint256 denominator = safeAdd(safeMul(inputReserve, 1000), safeMul(tokensToSpend, 997));
        uint256 outputAmount = safeDiv(numerator, denominator);
        return outputAmount;
    }
}

contract Kyber_Utils is
    StorageContract_Properties,
    SafeMath
{
    function tradeTokensOnKyber(
	    address src, 
	    uint srcAmount, 
	    address dest,
	    uint minConversionRate
	    ) internal returns (uint) 
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        
	    return KyberProxy(ds.kyberProxyContract).trade{value: 0}(
	        src, srcAmount, dest, address(this), 
	        69203865833239757421118596509098632427930889272824243351707071692229331386368, 
	        minConversionRate, 0x0000000000000000000000000000000000000000);
    }
    
    function getRateGivenPrice(
        uint256 price,
        bool sideIsBuy
        ) internal pure returns (uint256)
    {
        // sideIsBuy = True when you're buying on Kyber
        // sideIsBuy = False when selling on Kyber
        
        // When buying on Kyber, rate = 1 / price
        // When selling on Kyber, rate = price
        if (sideIsBuy)
        {
            return safeDiv(safeMul(1000000000000000000, 1000000000000000000), price);
        }
        else
        {
            return price;
        }
    }
}

contract Encoding_Utils
{
    function bitWiseRotateLeft(
        uint256 number, 
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to left rotate number by d bits
        // # In number<<d, last d bits are 0.
        // # To put first 3 bits of number at
        // # last, do bitwise or of n<<d
        // # with number >>(256 - d)
        return (number << d) | (number >> (256 - d));
    }

    function bitWiseRotateRight(
        uint256 number, 
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to right rotate n by d bits
        // # In n>>d, first d bits are 0.
        // # To put last 3 bits of at
        // # first, do bitwise or of n>>d
        // # with n <<(256 - d)
        return (number >> d) | (number << (256 - d)) & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    }
    
    function decodeSignature(
        uint256[] memory signatureIntList
        ) internal pure returns (bytes memory decodedSignature)
    {
        // Python has broken thke signature into hex strings, 
        // then converted them into ints, then rotated them to the right
        // to decode, do the opposite 
        
        uint8 numOfRotations = 2;
        // Rotate to the left
        uint256 x1 = bitWiseRotateLeft(signatureIntList[0], numOfRotations);
        uint256 x2 = bitWiseRotateLeft(signatureIntList[1], numOfRotations);
        uint256 x3 = bitWiseRotateLeft(signatureIntList[2], numOfRotations);
        
        decodedSignature = new bytes(66);
        assembly 
        {
            mstore(add(decodedSignature, 66), x3)
            mstore(add(decodedSignature, 50), x2)
            mstore(add(decodedSignature, 25), x1)
            mstore(decodedSignature, 66)
        }
        
        return decodedSignature;
    }
    
    function decode0xAssetData(
        uint256[] memory zrxAssetDataIntList
        ) internal pure returns (bytes memory decoded0xAssetData) 
    {
        // Python has broken thke signature into hex strings, 
        // then converted them into ints, then rotated them to the right
        // to decode, do the opposite
        
        uint8 numOfRotations = 7;
        // Rotate to the left
        uint256 x1 = bitWiseRotateLeft(zrxAssetDataIntList[0], numOfRotations);
        uint256 x2 = bitWiseRotateLeft(zrxAssetDataIntList[1], numOfRotations);
        
        decoded0xAssetData = new bytes(36);
        assembly 
        {
            mstore(add(decoded0xAssetData, 36), x2)
            mstore(add(decoded0xAssetData, 18), x1)
            mstore(decoded0xAssetData, 36)
        }
        
        return decoded0xAssetData;
    }
    
    function decodeAddress(
        uint256 myAddress
        ) internal pure returns (address decodedAddress) 
    {
        // Python has converted myAddress to an int, added an obscure number, and rotated to the right 
        // to decode, do the opposite 
        
        // Subtract my random obscure number, no need for safe math here
        myAddress = myAddress - 891754983589020875164519;
        // Rotate to the left
        myAddress = bitWiseRotateLeft(myAddress, 5);
        
        bytes memory b = new bytes(20);
        assembly 
        {
            mstore(add(b, 20), myAddress)
            mstore(b, 20)
        }
        
        assembly 
        {
            decodedAddress := mload(add(b, 20))
        } 
        
        return decodedAddress;
    }
    
    function decodeAddressArray(
        uint256[] memory encodedAddresses
        ) internal pure returns (address[] memory decodedAddressArray) 
    {
        // Decode the addresses
        decodedAddressArray = new address[](encodedAddresses.length);
        for (uint256 i = 0; i < encodedAddresses.length; i++)
        {
            decodedAddressArray[i] = decodeAddress(encodedAddresses[i]);
        }
        
        return decodedAddressArray;
    }
    
    function decodeInt(
        uint256 myInt
        ) internal pure returns (uint256 decodedNumber) 
    {
        // Python code has rotated myInt to the right
        // to decode, do the opposite 
        
        // Rotate to the left
        myInt = bitWiseRotateLeft(myInt, 16);
        return myInt;
    }
    
    function decodeIntArray(
        uint256[] memory encodedInts
        ) internal pure returns (uint256[] memory decodedIntList) 
    {
        decodedIntList = new uint256[](encodedInts.length);
        for (uint256 i = 0; i < encodedInts.length; i++)
        {
            decodedIntList[i] = decodeInt(encodedInts[i]);
        }
        
        return decodedIntList;
    }
}

abstract contract GasToken2 
{
    // function mint(uint256 value) public virtual;
    function free(uint256 value) public virtual returns (bool success);
}

contract Ninja_GasTokens
{
    // This is my local logic contract version of this function.
    // This assumes that this logic contract's proxy is holding gas tokens
    function freeGasTokens(
        uint gasTokens
        ) internal returns (bool success)
    {
        // When using the public contract GasToken2
        return GasToken2(0x0000000000b3F879cb30FE243b4Dfee438691c04).free(gasTokens);
    }
}

contract Ninja is
    WhitelistedOwners,
    SafeMath,
    Uniswap_Utils,
    Kyber_Utils,
    Set_Utils,
    Logs,
    Ninja_GasTokens,
    Encoding_Utils
{

    // ***** Begin tradeSimpleRequirements functions ***** //
    
    // Trades if state meets simple requirements
    function tradeSimpleRequirements_buyOnSetSellOnKyber(
        uint256 quoteTokensToSpend, 
        uint256[] memory intArray,
        address[] memory addressArray
        ) public onlyWhitelist 
    {
        // intArray[0] = minBidSize
        // intArray[1] = magicConverter_inflowTokensToShares
        // intArray[2] = minInflowTokenQuantity
        // intArray[3] = minOutflowTokenQuantity
        // intArray[4] = quoteTokenDecimals
        // intArray[5] = baseTokenDecimals
        
        // addressArray[0] = quoteTokenAddress WBTC
        // addressArray[1] = baseTokenAddress USDC (or other ERC20 token)
        // addressArray[2] = rebalanceAuctionModuleAddress
        // addressArray[3] = setTokenAddress
        
        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(quoteTokensToSpend);
        
        address[] memory addressArray_Set = new address[](4);
        addressArray_Set[0] = addressArray[2];  // rebalanceAuctionModuleAddress
        addressArray_Set[1] = addressArray[3];  // setTokenAddress
        addressArray_Set[2] = addressArray[0];  // inflowTokenAddress
        addressArray_Set[3] = addressArray[1];  // outflowTokenAddress
        
        // // Enforce rebalance mode and that the set is not yet 100% complete
        // if (SetToken(addressArray_Set[1]).rebalanceState() != 2)
        // {
        //     logCode_ifEnabled(13);
        //     return;
        // }
        
        // Enforce rebalance is not yet 100% complete
        uint256 remainingCurrentUnits = getRemainingCurrentUnits(addressArray_Set[1]);
        if (remainingCurrentUnits < intArray[0])
        {
            logCode_ifEnabled(19);
            return;
        }
        
        // Get the price from set and convert that to rate for kyber so I can use that as minConversionRate in the kyber trade
        // I want to call getMaxInflowTokenQuantityRemaining, but I actually can kill two birds with one stone 
        // by calling getMaxFlowTokenQuantitiesRemaining instead and I can also obtain the price from set which i'll need for later
        uint256 maxInflowTokenQuantity;
        uint256 maxOutflowTokenQuantity;
        (
            maxInflowTokenQuantity, 
            maxOutflowTokenQuantity
        ) = getMaxFlowTokenQuantitiesRemaining(
            intArray[0], intArray[2], intArray[3], remainingCurrentUnits);
        // Calculate minConversionRate for Kyber from the set token flow.  
        // Let's just assume that the price on set is the worst price i'll accept on kyber
        // So USDC will be outflow and WBTC will be inflow
        // Example numbers would be:
        // maxInflowTokenQuantity = 100000000 (WBTC)
        // maxOutflowTokenQuantity = 9000000000 (USDC)
        uint256 price_Set = calculatePriceForTrade(
            maxInflowTokenQuantity, maxOutflowTokenQuantity, intArray[4], intArray[5]);
        logCodeWithValue_ifEnabled(21, price_Set);
        uint256 minConversionRate_kyber = getRateGivenPrice(price_Set, false);
        logCodeWithValue_ifEnabled(20, minConversionRate_kyber);
        
        // Enforce quoteTokenAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
        uint256 quoteTokenAmountLeftFillable_Set = maxInflowTokenQuantity;
        logCodeWithValue_ifEnabled(15, quoteTokenAmountLeftFillable_Set);
        quoteTokensToSpend = min256(quoteTokensToSpend, quoteTokenAmountLeftFillable_Set);
        
        // Enforce no trading dust
        if (quoteTokensToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(26);
            handlePostTradeActions(beforeTradeDataArray_ints[1]);
            return;
        }

        // Buy tokens on Set
        uint256 baseTokensReceived = tradeTokensOnSet(
            addressArray_Set, quoteTokensToSpend, intArray[0], intArray[1]);
    
        // Sell them on Kyber
        tradeTokensOnKyber(
            addressArray[1], baseTokensReceived, 
            addressArray[0], minConversionRate_kyber);
	        
        handlePostTradeActions(beforeTradeDataArray_ints[1]);
    }
    
    // Trades if state meets simple requirements
    function tradeSimpleRequirements_buyOnKyberSellOnSet(
        uint256 quoteTokensToSpend, 
        uint256[] memory intArray,
        address[] memory addressArray
        ) public onlyWhitelist 
    {
        // intArray[0] = minBidSize
        // intArray[1] = magicConverter_inflowTokensToShares
        // intArray[2] = minInflowTokenQuantity
        // intArray[3] = minOutflowTokenQuantity
        // intArray[4] = quoteTokenDecimals
        // intArray[5] = baseTokenDecimals
        
        // addressArray[0] = quoteTokenAddress WBTC
        // addressArray[1] = baseTokenAddress USDC (or other ERC20 token)
        // addressArray[2] = rebalanceAuctionModuleAddress
        // addressArray[3] = setTokenAddress
        
        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(quoteTokensToSpend);
        
        address[] memory addressArray_Set = new address[](4);
        addressArray_Set[0] = addressArray[2];  // rebalanceAuctionModuleAddress
        addressArray_Set[1] = addressArray[3];  // setTokenAddress
        addressArray_Set[2] = addressArray[1];  // inflowTokenAddress
        addressArray_Set[3] = addressArray[0];  // outflowTokenAddress
        
        // // Enforce rebalance mode and that the set is not yet 100% complete
        // if (SetToken(addressArray_Set[1]).rebalanceState() != 2)
        // {
        //     logCode_ifEnabled(13);
        //     return;
        // }
        
        // Enforce rebalance is not yet 100% complete
        uint256 remainingCurrentUnits = getRemainingCurrentUnits(addressArray_Set[1]);
        if (remainingCurrentUnits < intArray[0])
        {
            logCode_ifEnabled(19);
            return;
        }
        
        // Get the price from set and convert that to rate for kyber so I can use that as minConversionRate in the kyber trade
        // I want to call getMaxInflowTokenQuantityRemaining, but I actually can kill two birds with one stone 
        // by calling getMaxFlowTokenQuantitiesRemaining instead and I can also obtain the price from set which i'll need for later
        uint256 maxInflowTokenQuantity;
        uint256 maxOutflowTokenQuantity;
        (
            maxInflowTokenQuantity, 
            maxOutflowTokenQuantity
        ) = getMaxFlowTokenQuantitiesRemaining(
            intArray[0], intArray[2], intArray[3], remainingCurrentUnits);
        // Calculate minConversionRate for Kyber from the set token flow.  
        // Let's just assume that the price on set is the worst price i'll accept on kyber
        // So WBTC will be outflow and USDC will be inflow
        // Example numbers would be:
        // maxInflowTokenQuantity = 9000000000 (USDC)
        // maxOutflowTokenQuantity = 100000000 (WBTC)
        uint256 price_Set = calculatePriceForTrade(
            maxOutflowTokenQuantity, maxInflowTokenQuantity, intArray[4], intArray[5]);
        logCodeWithValue_ifEnabled(21, price_Set);
        uint256 minConversionRate_kyber = getRateGivenPrice(price_Set, true);
        logCodeWithValue_ifEnabled(20, minConversionRate_kyber);
        
        // Enforce effectiveQuoteTokenAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
        uint256 effectiveQuoteTokenAmountLeftFillable_Set = maxOutflowTokenQuantity;
        // Reduce this number slightly because this is the outflow token and I don't want to do all the math to convert that using price
        effectiveQuoteTokenAmountLeftFillable_Set = safetifyThisNumber(effectiveQuoteTokenAmountLeftFillable_Set, 75);
        logCodeWithValue_ifEnabled(16, effectiveQuoteTokenAmountLeftFillable_Set);
        quoteTokensToSpend = min256(quoteTokensToSpend, effectiveQuoteTokenAmountLeftFillable_Set);
        
        // Enforce no trading dust
        if (quoteTokensToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(26);
            handlePostTradeActions(beforeTradeDataArray_ints[1]);
            return;
        }

        // Buy tokens on Kyber
        uint256 baseTokensReceived = tradeTokensOnKyber(
	        addressArray[0], quoteTokensToSpend, 
	        addressArray[1], minConversionRate_kyber);
        
        // Sell tokens on Set
        tradeTokensOnSet(addressArray_Set, baseTokensReceived, intArray[0], intArray[1]);
        
        handlePostTradeActions(beforeTradeDataArray_ints[1]);
    }
    
    // Trades if state meets simple requirements
    function tradeSimpleRequirements_buyOnSetSellOnUniswap(
        uint256 quoteTokensToSpend, 
        address[] memory addressArray,
        uint256[] memory intArray
        ) public onlyWhitelist 
    {
        // addressArray[0] = quoteTokenAddress WBTC
        // addressArray[1] = baseTokenAddress USDC (or other ERC20 token)
        // addressArray[2] = rebalanceAuctionModuleAddress
        // addressArray[3] = setTokenAddress
        // addressArray[4] = uniswapContract_quoteToken
        // addressArray[5] = uniswapContract_baseToken
        
        // intArray[0] = minBidSize
        // intArray[1] = quoteTokenDecimals
        // intArray[2] = baseTokenDecimals
        // intArray[3] = expectedBaseTokensToTrade
        // intArray[4] = magicConverter_inflowTokensToShares
        // intArray[5] = minInflowTokenQuantity
        // intArray[6] = minOutflowTokenQuantity
        
        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(quoteTokensToSpend);
        
        address[] memory addressArray_Set = new address[](4);
        addressArray_Set[0] = addressArray[2];  // rebalanceAuctionModuleAddress
        addressArray_Set[1] = addressArray[3];  // setTokenAddress
        addressArray_Set[2] = addressArray[0];  // inflowTokenAddress
        addressArray_Set[3] = addressArray[1];  // outflowTokenAddress
        
        // // Enforce rebalance mode and that the set is not yet 100% complete
        // if (SetToken(addressArray_Set[1]).rebalanceState() != 2)
        // {
        //     logCode_ifEnabled(13);
        //     return;
        // }
        
        // Enforce rebalance is not yet 100% complete
        uint256 remainingCurrentUnits = getRemainingCurrentUnits(addressArray_Set[1]);
        if (remainingCurrentUnits < intArray[0])
        {
            logCode_ifEnabled(19);
            return;
        }
        
        uint256 maxInflowTokenQuantity;
        uint256 maxOutflowTokenQuantity;
        (
            maxInflowTokenQuantity, 
            maxOutflowTokenQuantity
        ) = getMaxFlowTokenQuantitiesRemaining(
            intArray[0], intArray[5], intArray[6], remainingCurrentUnits);
        // So USDC will be outflow and WBTC will be inflow
        // Example numbers would be:
        // maxInflowTokenQuantity = 100000000 (WBTC)
        // maxOutflowTokenQuantity = 9000000000 (USDC)
        uint256 price_Set = calculatePriceForTrade(
            maxInflowTokenQuantity, maxOutflowTokenQuantity, intArray[1], intArray[2]);
        logCodeWithValue_ifEnabled(21, price_Set);
        
        // Enforce quoteTokenAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
        uint256 quoteTokenAmountLeftFillable_Set = maxInflowTokenQuantity;
        logCodeWithValue_ifEnabled(15, quoteTokenAmountLeftFillable_Set);
        quoteTokensToSpend = min256(quoteTokensToSpend, quoteTokenAmountLeftFillable_Set);
        
        // Enforce no trading dust
        if (quoteTokensToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(26);
            handlePostTradeActions(beforeTradeDataArray_ints[1]);
            return;
        }
    
        // Calculate Uniswap price
        uint256 price_Uniswap = getTokenForTokenTradePrice_Uniswap_sellOnUniswap(
            addressArray, intArray);
        logCodeWithValue_ifEnabled(22, price_Uniswap);
        
        // Enforce that the sell price is higher than the buy price
        if (price_Uniswap < price_Set)
        {
            logCode_ifEnabled(23);
            handlePostTradeActions(beforeTradeDataArray_ints[1]);
            return;
        }

        // Buy tokens on Set
        uint256 baseTokensReceived = tradeTokensOnSet(
            addressArray_Set, quoteTokensToSpend, intArray[0], intArray[4]);
    
        // Sell them on Uniswap
        // TODO, calculate min_tokens_bought here??  Or should I continue to rely on the Enforce logic above?
        tradeTokensOnUniswap(
            addressArray[5], baseTokensReceived, 1, addressArray[0]);
	        
        handlePostTradeActions(beforeTradeDataArray_ints[1]);
    }
    
    // Trades if state meets simple requirements
    function tradeSimpleRequirements_buyOnUniswapSellOnSet(
        uint256 quoteTokensToSpend, 
        address[] memory addressArray,
        uint256[] memory intArray
        ) public onlyWhitelist 
    {
        // addressArray[0] = quoteTokenAddress WBTC
        // addressArray[1] = baseTokenAddress USDC (or other ERC20 token)
        // addressArray[2] = rebalanceAuctionModuleAddress
        // addressArray[3] = setTokenAddress
        // addressArray[4] = uniswapContract_quoteToken
        // addressArray[5] = uniswapContract_baseToken
        
        // intArray[0] = minBidSize
        // intArray[1] = quoteTokenDecimals
        // intArray[2] = baseTokenDecimals
        // intArray[3] = expectedBaseTokensToTrade
        // intArray[4] = magicConverter_inflowTokensToShares
        // intArray[5] = minInflowTokenQuantity
        // intArray[6] = minOutflowTokenQuantity
        
        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(quoteTokensToSpend);
        
        address[] memory addressArray_Set = new address[](4);
        addressArray_Set[0] = addressArray[2];  // rebalanceAuctionModuleAddress
        addressArray_Set[1] = addressArray[3];  // setTokenAddress
        addressArray_Set[2] = addressArray[1];  // inflowTokenAddress
        addressArray_Set[3] = addressArray[0];  // outflowTokenAddress
        
        // // Enforce rebalance mode and that the set is not yet 100% complete
        // if (SetToken(addressArray_Set[1]).rebalanceState() != 2)
        // {
        //     logCode_ifEnabled(13);
        //     return;
        // }
        
        // Enforce rebalance is not yet 100% complete
        uint256 remainingCurrentUnits = getRemainingCurrentUnits(addressArray_Set[1]);
        if (remainingCurrentUnits < intArray[0])
        {
            logCode_ifEnabled(19);
            return;
        }
        
        uint256 maxInflowTokenQuantity;
        uint256 maxOutflowTokenQuantity;
        (
            maxInflowTokenQuantity, 
            maxOutflowTokenQuantity
        ) = getMaxFlowTokenQuantitiesRemaining(
            intArray[0], intArray[5], intArray[6], remainingCurrentUnits);
        // So WBTC will be outflow and USDC will be inflow
        // Example numbers would be:
        // maxInflowTokenQuantity = 9000000000 (USDC)
        // maxOutflowTokenQuantity = 100000000 (WBTC)
        uint256 price_Set = calculatePriceForTrade(
            maxOutflowTokenQuantity, maxInflowTokenQuantity, intArray[1], intArray[2]);
        logCodeWithValue_ifEnabled(21, price_Set);
        
        // Enforce effectiveQuoteTokenAmountLeftFillable_Set to ensure we're not spending more than the set has to offer
        uint256 effectiveQuoteTokenAmountLeftFillable_Set = maxOutflowTokenQuantity;
        // Reduce this number slightly because this is the outflow token and I don't want to do all the math to convert that using price
        effectiveQuoteTokenAmountLeftFillable_Set = safetifyThisNumber(effectiveQuoteTokenAmountLeftFillable_Set, 75);
        logCodeWithValue_ifEnabled(16, effectiveQuoteTokenAmountLeftFillable_Set);
        quoteTokensToSpend = min256(quoteTokensToSpend, effectiveQuoteTokenAmountLeftFillable_Set);
        
        // Enforce no trading dust
        if (quoteTokensToSpend < beforeTradeDataArray_ints[2])
        {
            logCode_ifEnabled(26);
            handlePostTradeActions(beforeTradeDataArray_ints[1]);
            return;
        }
        
        // Calculate Uniswap price
        uint256 price_Uniswap = getTokenForTokenTradePrice_Uniswap_buyOnUniswap(
            quoteTokensToSpend, addressArray, intArray);
        logCodeWithValue_ifEnabled(22, price_Uniswap);
        
        // Enforce that the sell price is higher than the buy price
        if (price_Set < price_Uniswap)
        {
            logCode_ifEnabled(23);
            handlePostTradeActions(beforeTradeDataArray_ints[1]);
            return;
        }

        // Buy tokens on Uniswap
        // TODO, calculate min_tokens_bought here??  Or should I continue to rely on the Enforce logic above?
        uint256 baseTokensReceived = tradeTokensOnUniswap(
            addressArray[4], quoteTokensToSpend, 1, addressArray[1]);
        
        // Sell tokens on Set
        tradeTokensOnSet(
            addressArray_Set, baseTokensReceived, intArray[0], intArray[4]);
        
        handlePostTradeActions(beforeTradeDataArray_ints[1]);
    }
    
    // ***** End tradeSimpleRequirements functions ***** //
    
    // ***** Begin Utility functions ***** //
    
    function getTokenForTokenTradePrice_Uniswap_sellOnUniswap(
        address[] memory addressArray,
        uint256[] memory intArray
        ) internal view returns (uint256 price_Uniswap)
    {
        // Calculate Uniswap price
        // tokenToSell is the base and tokenToBuy is the quote
        return getTokenForTokenTradePrice_Uniswap(
            addressArray[1], addressArray[5], intArray[2], intArray[3], 
            addressArray[0], addressArray[4], intArray[1], false);
    }
    
    function getTokenForTokenTradePrice_Uniswap_buyOnUniswap(
        uint256 quoteTokensToSpend,
        address[] memory addressArray,
        uint256[] memory intArray
        ) internal view returns (uint256 price_Uniswap)
    {
        // Calculate Uniswap price
        // tokenToSell is the quote and tokenToBuy is the base
        return getTokenForTokenTradePrice_Uniswap(
            addressArray[0], addressArray[4], intArray[1], quoteTokensToSpend, 
            addressArray[1], addressArray[5], intArray[2], true);
    }
    
    function handlePostTradeActions(
        uint256 gasBefore
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
    
        // Only consider spending gasTokens to save gas if this tx gas price is high enough to make it worth it
        // I may send a trade through at SafeLow and it doesn't make sense to kill a contract
        // So check gasPriceThresholdForSpendingGasTokens 
        if (tx.gasprice > ds.gasPriceThresholdForSpendingGasTokens)
        {
            // Have to add in the gas cost to perform a transaction call on an empty function
            // For this i'm using 21483
            uint256 gasUsed = 21483 + gasBefore - gasleft();
            // logCodeWithValue_ifEnabled(8, gasUsed);
            // Determine how many contracts to kill to save gas
            
            // If we are less than our threshold, do not kill any
            if (gasUsed < ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx)
            {
                // Do not kill any contracts
            }
            // Else if we are greater than our threshold
            else
            {
                // Calculate how many contracts to kill based on how much gas we've used
                uint256 numOfContractsToKill = safeDiv(gasUsed, ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx);
                // logCodeWithValue_ifEnabled(9, numOfContractsToKill);
                if (numOfContractsToKill > 0)
                {
                    freeGasTokens(numOfContractsToKill);
                }
            }
        }
    }
    
    function getMinQuoteTokensToSpendAllowed(
        uint quoteTokensToSpend
        ) internal view returns (uint minQuoteTokensToSpendAllowed)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        return safeDiv(quoteTokensToSpend, ds.minQuoteTokenAllowedToSpendDivider);
    }
    
    function getBeforeTradeDataArray_ints(
        uint256 etherToSpend
        ) internal view returns (uint256[] memory beforeTradeDataArray_ints)
    {
        beforeTradeDataArray_ints = new uint256[](3);
        beforeTradeDataArray_ints[0] = address(this).balance;  // contractBalanceBefore
        beforeTradeDataArray_ints[1] = gasleft();  // gasBefore
        beforeTradeDataArray_ints[2] = getMinQuoteTokensToSpendAllowed(etherToSpend);  // minEtherToSpendAllowed
        return beforeTradeDataArray_ints;
    }
    
    // ***** End Utility functions ***** //
    
}
