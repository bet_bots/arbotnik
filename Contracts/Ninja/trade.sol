pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }

    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray
        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // intArray
        uint256 doLogEvents;  // 1 means yes, 0 means no
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract StorageContract_Authentication_KeeperDAOTransferProxys
{
    struct DiamondStorage_Authentication_KeeperDAOTransferProxys
    {
        mapping (address => bool) whitelistedKeeperDAOTransferProxys;
    }

    function diamondStorage_Authentication_KeeperDAOTransferProxys() internal pure returns(DiamondStorage_Authentication_KeeperDAOTransferProxys storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication.keeperdaolpps");
        assembly { ds_slot := 0x4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f0 }
    }
}

contract WhitelistedOwners is
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

contract WhitelistedKeeperDAOTransferProxys is
    StorageContract_Authentication_KeeperDAOTransferProxys
{
    modifier onlyWhitelistedKeeperDAOTransferProxys()
    {
        DiamondStorage_Authentication_KeeperDAOTransferProxys storage ds = diamondStorage_Authentication_KeeperDAOTransferProxys();
        require(ds.whitelistedKeeperDAOTransferProxys[msg.sender] == true, "Must be whitelisted KeeperDAO transfer proxy.");
        _;
    }
}

contract Logs is
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0xv3 order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth trading
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it
    // 27 = UniswapV2 ETH balance fell outside requirement

    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);

    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }

    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

contract SafeMath
{
    function safeMul(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        require(c / a == b);
        require(
            c / a == b,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    function safeDiv(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a / b;
        return c;
    }

    function safeSub(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        require(b <= a);
        require(
            b <= a,
            "UINT256_UNDERFLOW"
        );
        return a - b;
    }

    function safeAdd(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a + b;
        require(c >= a);
        require(
            c >= a,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    // function max64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    // function min64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a < b ? a : b;
    // }

    // function max256(
    //     uint256 a, uint256 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    function min256(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        return a < b ? a : b;
    }

    function safetifyThisNumber(
        uint256 value,
        uint256 factor
        ) internal pure returns (uint256)
    {
        // Multiply by ~99%, just to be safe so there isn't a round up problem
        // In solididty, it's safer to underfill an order by a hair than to risk overfilling it resulting in an exception
        // To multiply by 99% simply pass in the number 99.
        // It will mulitply by 99 then divide by 100 in order to achieve the desired result without having an exception
        // To multiply by 99.99%, simply pass in the number 9999.
        // It will mulitply by 9999 then divide by 10000 in order to achieve the desired result without having an exception
        value = safeMul(value, factor);
        value = safeDiv(value, safeAdd(factor, 1));
        return value;
    }
}

abstract contract ERC20
{
    function balanceOf(address tokenOwner) public view virtual returns (uint balance);
    function transfer(address toAddress, uint tokens) public virtual returns (bool success);
//     function approve(address _spender, uint256 _value) public virtual returns (bool success);
//     function allowance(address _owner, address _spender) public view virtual returns (uint256 remaining);
//     // function decimals() public view returns (uint8 decimals);
}

abstract contract WETH is
    ERC20
{
    function deposit() public payable virtual;
    function withdraw(uint wad) public virtual;
}

contract WETH_Utils is
    StorageContract_Properties,
    WhitelistedOwners
{
    function wrapEther(
        uint amount
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        require(address(this).balance >= amount, "Tried to wrap more ETH than we have");
        WETH weth = WETH(ds.wethContract);
        weth.deposit{value: amount}();
    }

    function unwrapEther(
        uint amount
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        WETH weth = WETH(ds.wethContract);
        weth.withdraw(amount);
    }
}

contract Encoding_Utils
{
    function bitWiseRotateLeft(
        uint256 number,
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to left rotate number by d bits
        // # In number<<d, last d bits are 0.
        // # To put first 3 bits of number at
        // # last, do bitwise or of n<<d
        // # with number >>(256 - d)
        return (number << d) | (number >> (256 - d));
    }

    function bitWiseRotateRight(
        uint256 number,
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to right rotate n by d bits
        // # In n>>d, first d bits are 0.
        // # To put last 3 bits of at
        // # first, do bitwise or of n>>d
        // # with n <<(256 - d)
        return (number >> d) | (number << (256 - d)) & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    }

    // function decodeSignature(
    //     uint256[] memory signatureIntList
    //     ) internal pure returns (bytes memory decodedSignature)
    // {
    //     // Python has broken thke signature into hex strings,
    //     // then converted them into ints, then rotated them to the right
    //     // to decode, do the opposite

    //     uint8 numOfRotations = 2;
    //     // Rotate to the left
    //     uint256 x1 = bitWiseRotateLeft(signatureIntList[0], numOfRotations);
    //     uint256 x2 = bitWiseRotateLeft(signatureIntList[1], numOfRotations);
    //     uint256 x3 = bitWiseRotateLeft(signatureIntList[2], numOfRotations);

    //     decodedSignature = new bytes(66);
    //     assembly
    //     {
    //         mstore(add(decodedSignature, 66), x3)
    //         mstore(add(decodedSignature, 50), x2)
    //         mstore(add(decodedSignature, 25), x1)
    //         mstore(decodedSignature, 66)
    //     }

    //     return decodedSignature;
    // }

    // function decode0xAssetData(
    //     uint256[] memory zrxAssetDataIntList
    //     ) internal pure returns (bytes memory decoded0xAssetData)
    // {
    //     // Python has broken thke signature into hex strings,
    //     // then converted them into ints, then rotated them to the right
    //     // to decode, do the opposite

    //     uint8 numOfRotations = 7;
    //     // Rotate to the left
    //     uint256 x1 = bitWiseRotateLeft(zrxAssetDataIntList[0], numOfRotations);
    //     uint256 x2 = bitWiseRotateLeft(zrxAssetDataIntList[1], numOfRotations);

    //     decoded0xAssetData = new bytes(36);
    //     assembly
    //     {
    //         mstore(add(decoded0xAssetData, 36), x2)
    //         mstore(add(decoded0xAssetData, 18), x1)
    //         mstore(decoded0xAssetData, 36)
    //     }

    //     return decoded0xAssetData;
    // }

    // function decodeAddress(
    //     uint256 myAddress
    //     ) internal pure returns (address decodedAddress)
    // {
    //     // Python has converted myAddress to an int, added an obscure number, and rotated to the right
    //     // to decode, do the opposite

    //     // Subtract my random obscure number, no need for safe math here
    //     myAddress = myAddress - 891754983589020875164519;
    //     // Rotate to the left
    //     myAddress = bitWiseRotateLeft(myAddress, 5);

    //     bytes memory b = new bytes(20);
    //     assembly
    //     {
    //         mstore(add(b, 20), myAddress)
    //         mstore(b, 20)
    //     }

    //     assembly
    //     {
    //         decodedAddress := mload(add(b, 20))
    //     }

    //     return decodedAddress;
    // }

    // function decodeAddressArray(
    //     uint256[] memory encodedAddresses
    //     ) internal pure returns (address[] memory decodedAddressArray)
    // {
    //     // Decode the addresses
    //     decodedAddressArray = new address[](encodedAddresses.length);
    //     for (uint256 i = 0; i < encodedAddresses.length; i++)
    //     {
    //         decodedAddressArray[i] = decodeAddress(encodedAddresses[i]);
    //     }

    //     return decodedAddressArray;
    // }

    // function decodeInt(
    //     uint256 myInt
    //     ) internal pure returns (uint256 decodedNumber)
    // {
    //     // Python code has rotated myInt to the right
    //     // to decode, do the opposite

    //     // Rotate to the left
    //     myInt = bitWiseRotateLeft(myInt, 16);
    //     return myInt;
    // }

    // function decodeIntArray(
    //     uint256[] memory encodedInts
    //     ) internal pure returns (uint256[] memory decodedIntList)
    // {
    //     decodedIntList = new uint256[](encodedInts.length);
    //     for (uint256 i = 0; i < encodedInts.length; i++)
    //     {
    //         decodedIntList[i] = decodeInt(encodedInts[i]);
    //     }

    //     return decodedIntList;
    // }

    function decodeInt_injectIntAsFirstParameterInToCallData(
        uint256 myInt
        ) internal pure returns (uint256 decodedNumber)
    {
        // Python code has rotated myInt to the right
        // to decode, do the opposite

        // Rotate to the left
        myInt = bitWiseRotateLeft(myInt, 11);
        return myInt;
    }

    function decodeIntArray_injectIntAsFirstParameterInToCallData(
        uint256[] memory encodedInts
        ) internal pure returns (uint256[] memory decodedIntList)
    {
        decodedIntList = new uint256[](encodedInts.length);
        for (uint256 i = 0; i < encodedInts.length; i++)
        {
            decodedIntList[i] = decodeInt_injectIntAsFirstParameterInToCallData(encodedInts[i]);
        }

        return decodedIntList;
    }

    function injectIntAsFirstParameterInToCallData(
        uint256[] memory callDataArray,
        uint256 quoteTokensToSpend_toInject,
        bool doDecode
        ) internal pure returns (bytes memory updatedCallData)
    {
        // callDataArray is made up to two parts
        // the first index in callDataArray is the encoded function selector
        // all the other indexes in callDataArray are the encoded parameters in the calldata

        // Sample input data with the first number as some uint256
        // 0xcb3c28c70000000000000000000000000000000000000000000000000000000001b8141a0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000
        // Sample input data with the first number as FFFF...FFFF showing what to replace with your own uint256
        // 0xcb3c28c7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000

        // The actual input data should be encoded, and this function will decode it

        // // replace the first parameter with the one passed into the function
        // uint256[] memory callDataArray = new uint256[](8);
        // // The first item is the function selector
        // callDataArray[0] = 11251282108508751410786438484730905108789417455303941525318388289831404398469;
        // // All the other items are the parameters in the calldata
        // callDataArray[1] = 59366061376553713473998796024962062131803458935020796992885968363432024487682;
        // callDataArray[2] = 81020539002477591817371690194067367295966331559958661468723494924294866565208;
        // callDataArray[3] = 100300374173339321621784632522174664355417868639112964214766417266043084725725;
        // callDataArray[4] = 29513413370058131841359401452410130929006865798253763713474781928186275684624;
        // callDataArray[5] = 33790950113886600303280564701708316615200629527746212574075718599721353216;
        // callDataArray[6] = 22504664344615616;
        // callDataArray[7] = 0;

        uint256 callDataArrayLength = callDataArray.length;

        if (doDecode)
        {
            // Decode the encoded parameters
            callDataArray = decodeIntArray_injectIntAsFirstParameterInToCallData(callDataArray);
        }

        uint256 functionSelector = callDataArray[0];

        uint8 functionSelectorLength = 4;
        uint256 updatedCallDataLength = functionSelectorLength + ((callDataArrayLength - 1)* 32);
        uint256 overwriteLocation = 32 + functionSelectorLength;

        // Declare the size of our updatedCallData
        updatedCallData = new bytes(updatedCallDataLength);

        // We must use assembly here so it's gas efficient
        assembly
        {
            // Iterate over the array
            // skip the first item since it's the function selector
            // skip the second item becuase that's the value we're overwriting anyways
            for { let i := 2 } lt(i, callDataArrayLength) { i := add(1, i) }
            {
                // Write each value to the bytes
                mstore(
                    // position of where we want to write the bytes within updatedCallData
                    add(updatedCallData, add(functionSelectorLength, mul(32, add(0, i)))),
                    // value of what we want to write to updatedCallData
                    mload(add(callDataArray, mul(32, add(1, i))))
                )
            }

            // Write the value we're overwriting to the bytes
            mstore(add(updatedCallData, overwriteLocation), quoteTokensToSpend_toInject)

            // Write the function selector to the bytes
            mstore(add(updatedCallData, functionSelectorLength), functionSelector)

            // Not sure what this does but it's needed to finish up
            mstore(updatedCallData, updatedCallDataLength)
        }

        return updatedCallData;
    }
}

abstract contract KeeperDaoLpp
{
    function borrow(address _token, uint256 _amount, bytes calldata _data) external virtual;
}

abstract contract GasToken2
{
    // function mint(uint256 value) public virtual;
    function free(uint256 value) public virtual returns (bool success);
}

contract Ninja_GasTokens
{
    // This is my local logic contract version of this function.
    // This assumes that this logic contract's proxy is holding gas tokens
    function freeGasTokens(
        uint gasTokens
        ) internal returns (bool success)
    {
        // When using the public contract GasToken2
        return GasToken2(0x0000000000b3F879cb30FE243b4Dfee438691c04).free(gasTokens);
    }
}

contract Ninja_Trade is
    WhitelistedOwners,
    WhitelistedKeeperDAOTransferProxys,
    SafeMath,
    WETH_Utils,
    Logs,
    Encoding_Utils,
    Ninja_GasTokens
{

    function tradeSimpleRequirements(
        uint256 quoteTokensToSpend,
        address quoteToken,
        uint256 maxQuoteTokenTradeLossAcceptable,
        uint256[][] calldata checkCallDataArrays_encoded,
        uint256[] calldata tradeCallDataArray_encoded
        ) external onlyWhitelist
    {
        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(
            quoteTokensToSpend, quoteToken);

        // Iterate over the checks and execute them all to enforce that profit is still available before we consider trading
		for (uint8 i = 0; i < checkCallDataArrays_encoded.length; i++)
		{
            // Decode my callData and update the quoteTokensToSpend in the callData
            bytes memory checkCallData = injectIntAsFirstParameterInToCallData(
                checkCallDataArrays_encoded[i], quoteTokensToSpend, true);

			// Call our check to see if profit is still available and update quoteTokensToSpend in the process
            (bool success, bytes memory callResult) = address(this).call{value:0}(checkCallData);
            require(success, "Check failed during execution");
            assembly
            {
                quoteTokensToSpend := mload(add(callResult,0x20))
            }

            // quoteTokensToSpend will be 0 if our check failed
            if (quoteTokensToSpend == 0)
            {
                // Our check failed and this trade is not profitable
                handlePostTradeActions(
                    beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1],
                    quoteToken, maxQuoteTokenTradeLossAcceptable);
                return;
            }
            // Enforce no trading dust
            else if (quoteTokensToSpend < beforeTradeDataArray_ints[2])
            {
                // Our check passed, but the quoteTokensToSpend is too small to be worth trading
                logCode_ifEnabled(24);
                handlePostTradeActions(
                    beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1],
                    quoteToken, maxQuoteTokenTradeLossAcceptable);
                return;
            }
		}

        // If we've made it this far without returning, profit is still available

        // Initiate the flash loan and trade
        initiateTradeWithinFlashLoan(quoteTokensToSpend, quoteToken, tradeCallDataArray_encoded);

        handlePostTradeActions(
            beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1],
            quoteToken, maxQuoteTokenTradeLossAcceptable);
    }

    function initiateTradeWithinFlashLoan(
        uint256 quoteTokensToSpend,
        address quoteToken,
        uint256[] memory tradeCallDataArray_encoded
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // Flash loan from KeeperDAO, and trade via appropriate tradeWithinFlashLoan
        KeeperDaoLpp(ds.keeperDaoLppContract_Eth).borrow(
            quoteToken,
            quoteTokensToSpend,
            // Update the quantity we are trading in the calldata, since that could have changed somewhere along the way
            injectIntAsFirstParameterInToCallData(tradeCallDataArray_encoded, quoteTokensToSpend, true));
    }

    function tradeWithinFlashLoan(
        uint256 borrowedQuoteTokens,
        address quoteToken,
        uint256[][] calldata exchangeSpecificTradeCallDataArrays,
        bool doWrapEthBeforeFirstTrade,
        bool doUnwrapEthAfterLastTrade
        ) external onlyWhitelistedKeeperDAOTransferProxys
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 quoteTokenBalance_beforeTrading = getThisContractsTokenBalance(quoteToken);

        // Do I need to wrap ETH before the trade?
        if (doWrapEthBeforeFirstTrade)
        {
            // Assume that if doWrapEthBeforeFirstTrade is set that borrowedQuoteTokens is ETH
            // wrap ETH that we're trading
            wrapEther(borrowedQuoteTokens);
        }

        // ****** Begin trading logic ****** //

        uint256 tokensToSpend;
        uint256 tokensReceived;

        // Iterate over the trades and execute them all
        for (uint8 i = 0; i < exchangeSpecificTradeCallDataArrays.length; i++)
		{
            if (i == 0)
            {
                // Spend the tokens we borrowed
                tokensToSpend = borrowedQuoteTokens;
            }
            else
            {
                // Spend the tokens we received from the previous trade
                tokensToSpend = tokensReceived;
            }

            // Decode my callData and update the quoteTokensToSpend in the callData
            bytes memory tradeCallData = injectIntAsFirstParameterInToCallData(
                exchangeSpecificTradeCallDataArrays[i], tokensToSpend, false);

            (bool success, bytes memory callResult) = address(this).call{value:0}(tradeCallData);
            require(success, "Trade failed during execution");
            assembly
            {
                tokensReceived := mload(add(callResult,0x20))
            }
        }
        // ****** End trading logic ****** //

        // Do I need to wrap ETH after the trade?
        if (doUnwrapEthAfterLastTrade)
        {
            // Assume that if doUnwrapEthAfterLastTrade is set that tokensReceived is WETH
            // Unwrap the WETH I received from the trade
            unwrapEther(tokensReceived);
        }

        returnBorrowedQuoteTokensAndShareProfit(
            ds, borrowedQuoteTokens, quoteToken, quoteTokenBalance_beforeTrading);
    }

    function returnBorrowedQuoteTokensAndShareProfit(
        DiamondStorage_Properties storage ds,
        uint256 borrowedQuoteTokens,
        address quoteToken,
        uint256 quoteTokenBalance_beforeTrading
        ) internal
    {
        uint256 quoteTokenBalance_afterTrading = getThisContractsTokenBalance(quoteToken);

        // Calculate the sharableProfitQuantity
        uint256 profitToShareWithKeeperDAO = 0;
        // Calculate profit made in quoteTokens
        if (quoteTokenBalance_afterTrading > quoteTokenBalance_beforeTrading)
        {
            // We've made profit in quoteTokens
            // Determine how much profit to share with KeeperDAO in a gas efficient way
            profitToShareWithKeeperDAO = safeSub(quoteTokenBalance_afterTrading, quoteTokenBalance_beforeTrading);
            profitToShareWithKeeperDAO = safeDiv(
                safeMul(profitToShareWithKeeperDAO, 1000000000000000000), ds.divider_profitToShareWithKeeperDAO);
        }
        // Else, we had a loss in quoteTokens.
        // This means one of two things:
        // 1.) The profit was actually made in baseTokens (and that may be left as dust)
        // 2.) The trade was not proftiable!
        //     NOTE: A non-profitable trade will NOT be enforced/reverted here.
        //           It's up to the other trade logic to determine whether or not we should revert
        //           This function will attempt to return the amount borrowed in quoteTokens even if that means this transaction results in a loss
        //           If this function tries to return more quoteTokens than it has,
        //              the tx will revert/fail because that's not possible.
        //           If this function succeeds in returning the borrowed quoteTokens,
        //              it's up to the other trade logic to decide if the trade was profitable or if it needs reverted

        // Return the borrowedQuoteTokens + profitToShareWithKeeperDAO
        SendQuoteTokens(
            ds,
            safeAdd(borrowedQuoteTokens, profitToShareWithKeeperDAO),
            quoteToken);
    }

    function SendQuoteTokens(
        DiamondStorage_Properties storage ds,
        uint256 quoteTokensToReturn,
        address quoteToken
        ) internal
    {
        // If it's ETH
        if (quoteToken == 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE)
        {
            // Send ETH
            (bool success, ) = ds.keeperDaoLppContract_Eth.call{value: quoteTokensToReturn}("");
            require(success, "Transfer of borrowedQuoteTokens back to LP has failed.");
        }
        // Else assume it's an ERC20 token
        else
        {
            // Send ERC20
            bool success = ERC20(quoteToken).transfer(address(ds.keeperDaoLppContract_Eth), quoteTokensToReturn);
            require(success, "Transfer of borrowedQuoteTokens back to LP has failed.");
        }
    }

    function handlePostTradeActions(
        uint256 quoteTokenBalance_beforeTrading,
        uint256 gasBefore,
        address quoteToken,
        uint256 maxQuoteTokenTradeLossAcceptable
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // The 0x integration here tends to get a lot of dust in base tokens as well as quote tokens
        // So this number needs to either be pretty high (AKA 0.01 quote tokens)
        // or we need to calcualte the dust in both base and quote which is pretty time consuming and costs a lot of gas
        // To avoid timing consuming and expensive calulations, Just hard code in a number here that works most all the time
        // And put trust into the ninja code that's calling this contract
        // uint256 maxTradeLossAcceptable = 10000000000000000;  // I was seeing code 5's due to dust with this amount...
        // uint256 maxTradeLossAcceptable = 30000000000000000;
        // uint256 maxTradeLossAcceptable = 0;

        // If I lost quoteTokens during this charade (considering the max amount we're allowed to lose)
        if (getThisContractsTokenBalance(quoteToken) + maxQuoteTokenTradeLossAcceptable < quoteTokenBalance_beforeTrading)
        {
            // Do not let it trade since it lost ether!
            // I may have tokens left over though so I may actually want to let this trade go through in some cases.
            // Question is, how complicated is that logic and is it worth it?
            // I'll need to check before and after token balances and then value them with respect to ETH
            revert("Code 5");
        }
        // else profit or break even
        else
        {
            // Calculate the profit i'm making
            // uint256 profit_quoteTokens = safeSub(getThisContractsTokenBalance(quoteToken), contractBalanceBefore);

            // Only consider spending gasTokens to save gas if this tx gas price is high enough to make it worth it
            // I may send a trade through at SafeLow and it doesn't make sense to kill a contract
            // So check gasPriceThresholdForSpendingGasTokens
            if (tx.gasprice > ds.gasPriceThresholdForSpendingGasTokens)
            {
                // Have to add in the gas cost to perform a transaction call on an empty function
                // For this i'm using 21483
                uint256 gasUsed = safeSub(safeAdd(21483, gasBefore), gasleft());
                // logCodeWithValue_ifEnabled(8, gasUsed);
                // Determine how many contracts to kill to save gas

                // If we are less than our threshold, do not kill any
                if (gasUsed < ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx)
                {
                    // Do not kill any contracts
                }
                // Else if we are greater than our threshold
                else
                {
                    // Calculate how many contracts to kill based on how much gas we've used
                    uint256 numOfContractsToKill = safeDiv(gasUsed, ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx);
                    // logCodeWithValue_ifEnabled(9, numOfContractsToKill);
                    if (numOfContractsToKill > 0)
                    {
                        freeGasTokens(numOfContractsToKill);
                    }
                }
            }
        }
    }

    function getMinQuoteTokensToSpendAllowed(
        uint quoteTokensToSpend
        ) internal view returns (uint minQuoteTokensToSpendAllowed)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        return safeDiv(quoteTokensToSpend, ds.minQuoteTokenAllowedToSpendDivider);
    }

    function getBeforeTradeDataArray_ints(
        uint256 quoteTokensToSpend,
        address quoteToken
        ) internal view returns (uint256[] memory beforeTradeDataArray_ints)
    {
        beforeTradeDataArray_ints = new uint256[](3);
        beforeTradeDataArray_ints[0] = getThisContractsTokenBalance(quoteToken);  // quoteTokenBalance_beforeTrading
        beforeTradeDataArray_ints[1] = gasleft();  // gasBefore
        beforeTradeDataArray_ints[2] = getMinQuoteTokensToSpendAllowed(quoteTokensToSpend);  // minQuoteTokensToSpend
        return beforeTradeDataArray_ints;
    }

    function getThisContractsTokenBalance(
        address token
        ) internal view returns (uint256 balance)
    {
        // If it's ETH
        if (token == 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE)
        {
            return address(this).balance;
        }
        // Else assume it's an ERC20 token
        else
        {
            return ERC20(token).balanceOf(address(this));
        }
    }

    // ***** End Utility functions ***** //

}
