pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }

    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract WhitelistedOwners is
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

abstract contract CHI
{
    function mint(uint256 value) public virtual;
    function free(uint256 value) public virtual returns (uint256);
    // function freeFromUpTo(address from, uint256 value) public virtual returns (uint256);
}

contract Ninja_GasTokens is
    WhitelistedOwners
{
    function mintGasTokens(
        uint gasTokens
        ) public
    {
        CHI(0x0000000000004946c0e9F43F4Dee607b0eF1fA1c).mint(gasTokens);
    }

    function freeGasTokens(
        uint gasTokens
        ) public onlyWhitelist returns (uint256 freedAmount)
    {
        // This assumes that this logic contract's proxy is holding gas tokens
        // I'm not wasting gas to check to confirm that I have enough gas tokens
        return CHI(0x0000000000004946c0e9F43F4Dee607b0eF1fA1c).free(gasTokens);
    }
}
