pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }

    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray contains only all addresses in here in the exact order they are listed below
        // intArray contains only all addresses in here in the exact order they are listed below

        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // Enables/Disables informative event logging. 1 means yes, 0 means no
        uint256 doLogEvents;
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;

        // Whitelists to ensure only ninjaProxyContract can call trade functions
        address ninjaProxyContract;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract StorageContract_Proxy
{
    struct DiamondStorage_Proxy
    {
        // maps function selectors to the facets that execute the functions.
        // and maps the selectors to the slot in the selectorSlots array.
        // and maps the selectors to the position in the slot.
        // func selector => address facet, uint64 slotsIndex, uint64 slotIndex
        mapping(bytes4 => bytes32) facets;

        // array of slots of function selectors.
        // each slot holds 8 function selectors.
        mapping(uint => bytes32) selectorSlots;  

        // uint128 numSelectorsInSlot, uint128 selectorSlotsLength
        // selectorSlotsLength is the number of 32-byte slots in selectorSlots.
        // selectorSlotLength is the number of selectors in the last slot of
        // selectorSlots.
        uint selectorSlotsLength;

        // Used to query if a contract implements an interface.
        // Used to implement ERC-165.
        mapping(bytes4 => bool) supportedInterfaces;
    }
    
    function diamondStorage_Proxy() internal pure returns(DiamondStorage_Proxy storage ds) 
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.proxy");
        assembly { ds_slot := 0xaab760237e39f9aad10fcfc005484cb19a7fafbfa4772d5ae586b83789f67cd1 }
    }
}

contract WhitelistedOwners is
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

contract StorageContract_Authentication_KeeperDAOLPPs
{
    struct DiamondStorage_Authentication_KeeperDAOLPPs
    {
        mapping (address => bool) whitelistedKeeperDAOLPPs;
    }

    function diamondStorage_Authentication_KeeperDAOLPPs() internal pure returns(DiamondStorage_Authentication_KeeperDAOLPPs storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication.keeperdaolpps");
        assembly { ds_slot := 0x4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f0 }
    }
}

contract WhitelistedKeeperDAOLPPs is
    StorageContract_Authentication_KeeperDAOLPPs
{
    modifier onlyWhitelistedKeeperDAOLPPs()
    {
        DiamondStorage_Authentication_KeeperDAOLPPs storage ds = diamondStorage_Authentication_KeeperDAOLPPs();
        require(ds.whitelistedKeeperDAOLPPs[msg.sender] == true, "Must be whitelisted KeeperDAOLP");
        _;
    }
}

contract NinjaProxyOwner is
    StorageContract_Properties
{
    modifier onlyNinjaProxy()
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        require(msg.sender == ds.ninjaProxyContract, "Must be Ninja");
        _;
    }
}

contract Logs_HidingGame
{
    event Event_LogCode(address quoteToken, uint256 profit);
}

contract Logs is
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0xv3 order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth trading
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it
    // 27 = UniswapV2 ETH balance fell outside requirement
    // 28 = Balancer QuoteTokens balance fell outside requirement
    // 30 = UniswapV2 blockTimestampLast failed validation

    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);

    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }

    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets `amount` as the allowance of `spender` over the caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 amount) external returns (bool);

    /**
     * @dev Moves `amount` tokens from `sender` to `recipient` using the
     * allowance mechanism. `amount` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

/**
 * @title SafeERC20
 * @dev Wrappers around ERC20 operations that throw on failure (when the token
 * contract returns false). Tokens that return no value (and instead revert or
 * throw on failure) are also supported, non-reverting calls are assumed to be
 * successful.
 * To use this library you can add a `using SafeERC20 for IERC20;` statement to your contract,
 * which allows you to call the safe operations as `token.safeTransfer(...)`, etc.
 */
library SafeERC20 {
    using SafeMath for uint256;
    using Address for address;

    function safeTransfer(IERC20 token, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transfer.selector, to, value));
    }

    function safeTransferFrom(IERC20 token, address from, address to, uint256 value) internal {
        _callOptionalReturn(token, abi.encodeWithSelector(token.transferFrom.selector, from, to, value));
    }

    /**
     * @dev Deprecated. This function has issues similar to the ones found in
     * {IERC20-approve}, and its usage is discouraged.
     *
     * Whenever possible, use {safeIncreaseAllowance} and
     * {safeDecreaseAllowance} instead.
     */
    function safeApprove(IERC20 token, address spender, uint256 value) internal {
        // safeApprove should only be called when setting an initial allowance,
        // or when resetting it to zero. To increase and decrease it, use
        // 'safeIncreaseAllowance' and 'safeDecreaseAllowance'
        // solhint-disable-next-line max-line-length
        require((value == 0) || (token.allowance(address(this), spender) == 0),
            "SafeERC20: approve from non-zero to non-zero allowance"
        );
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, value));
    }

    function safeIncreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).add(value);
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }

    function safeDecreaseAllowance(IERC20 token, address spender, uint256 value) internal {
        uint256 newAllowance = token.allowance(address(this), spender).sub(value, "SafeERC20: decreased allowance below zero");
        _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, newAllowance));
    }
    
    // JoeyZ made this one specifically and optimally for Ninja 
    function approveMaxOnlyIfNeeded(IERC20 token, address spender) internal {
        // If my allowance is zero or getting low based on modern standards
        if (token.allowance(address(this), spender) < uint96(-1))
        {
            _callOptionalReturn(token, abi.encodeWithSelector(token.approve.selector, spender, uint256(-1)));
        }
    }

    /**
     * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
     * on the return value: the return value is optional (but if data is returned, it must not be false).
     * @param token The token targeted by the call.
     * @param data The call data (encoded using abi.encode or one of its variants).
     */
    function _callOptionalReturn(IERC20 token, bytes memory data) private {
        // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
        // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
        // the target address contains contract code and also asserts for success in the low-level call.

        bytes memory returndata = address(token).functionCall(data, "SafeERC20: low-level call failed");
        if (returndata.length > 0) { // Return data is optional
            // solhint-disable-next-line max-line-length
            require(abi.decode(returndata, (bool)), "SafeERC20: ERC20 operation did not succeed");
        }
    }
}

/**
 * @dev Collection of functions related to the address type
 */
library Address
{
    /**
     * @dev Returns true if `account` is a contract.
     *
     * [IMPORTANT]
     * ====
     * It is unsafe to assume that an address for which this function returns
     * false is an externally-owned account (EOA) and not a contract.
     *
     * Among others, `isContract` will return false for the following
     * types of addresses:
     *
     *  - an externally-owned account
     *  - a contract in construction
     *  - an address where a contract will be created
     *  - an address where a contract lived, but was destroyed
     * ====
     */
    function isContract(address account) internal view returns (bool) {
        // This method relies on extcodesize, which returns 0 for contracts in
        // construction, since the code is only stored at the end of the
        // constructor execution.

        uint256 size;
        // solhint-disable-next-line no-inline-assembly
        assembly { size := extcodesize(account) }
        return size > 0;
    }

    /**
     * @dev Replacement for Solidity's `transfer`: sends `amount` wei to
     * `recipient`, forwarding all available gas and reverting on errors.
     *
     * https://eips.ethereum.org/EIPS/eip-1884[EIP1884] increases the gas cost
     * of certain opcodes, possibly making contracts go over the 2300 gas limit
     * imposed by `transfer`, making them unable to receive funds via
     * `transfer`. {sendValue} removes this limitation.
     *
     * https://diligence.consensys.net/posts/2019/09/stop-using-soliditys-transfer-now/[Learn more].
     *
     * IMPORTANT: because control is transferred to `recipient`, care must be
     * taken to not create reentrancy vulnerabilities. Consider using
     * {ReentrancyGuard} or the
     * https://solidity.readthedocs.io/en/v0.5.11/security-considerations.html#use-the-checks-effects-interactions-pattern[checks-effects-interactions pattern].
     */
    function sendValue(address payable recipient, uint256 amount) internal {
        require(address(this).balance >= amount, "Address: insufficient balance");

        // solhint-disable-next-line avoid-low-level-calls, avoid-call-value
        (bool success, ) = recipient.call{ value: amount }("");
        require(success, "Address: unable to send value, recipient may have reverted");
    }

    /**
     * @dev Performs a Solidity function call using a low level `call`. A
     * plain`call` is an unsafe replacement for a function call: use this
     * function instead.
     *
     * If `target` reverts with a revert reason, it is bubbled up by this
     * function (like regular Solidity function calls).
     *
     * Returns the raw returned data. To convert to the expected return value,
     * use https://solidity.readthedocs.io/en/latest/units-and-global-variables.html?highlight=abi.decode#abi-encoding-and-decoding-functions[`abi.decode`].
     *
     * Requirements:
     *
     * - `target` must be a contract.
     * - calling `target` with `data` must not revert.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data) internal returns (bytes memory) {
      return functionCall(target, data, "Address: low-level call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`], but with
     * `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        return functionCallWithValue(target, data, 0, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but also transferring `value` wei to `target`.
     *
     * Requirements:
     *
     * - the calling contract must have an ETH balance of at least `value`.
     * - the called Solidity function must be `payable`.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value) internal returns (bytes memory) {
        return functionCallWithValue(target, data, value, "Address: low-level call with value failed");
    }

    /**
     * @dev Same as {xref-Address-functionCallWithValue-address-bytes-uint256-}[`functionCallWithValue`], but
     * with `errorMessage` as a fallback revert reason when `target` reverts.
     *
     * _Available since v3.1._
     */
    function functionCallWithValue(address target, bytes memory data, uint256 value, string memory errorMessage) internal returns (bytes memory) {
        require(address(this).balance >= value, "Address: insufficient balance for call");
        require(isContract(target), "Address: call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.call{ value: value }(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data) internal view returns (bytes memory) {
        return functionStaticCall(target, data, "Address: low-level static call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a static call.
     *
     * _Available since v3.3._
     */
    function functionStaticCall(address target, bytes memory data, string memory errorMessage) internal view returns (bytes memory) {
        require(isContract(target), "Address: static call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.staticcall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.3._
     */
    function functionDelegateCall(address target, bytes memory data) internal returns (bytes memory) {
        return functionDelegateCall(target, data, "Address: low-level delegate call failed");
    }

    /**
     * @dev Same as {xref-Address-functionCall-address-bytes-string-}[`functionCall`],
     * but performing a delegate call.
     *
     * _Available since v3.3._
     */
    function functionDelegateCall(address target, bytes memory data, string memory errorMessage) internal returns (bytes memory) {
        require(isContract(target), "Address: delegate call to non-contract");

        // solhint-disable-next-line avoid-low-level-calls
        (bool success, bytes memory returndata) = target.delegatecall(data);
        return _verifyCallResult(success, returndata, errorMessage);
    }

    function _verifyCallResult(bool success, bytes memory returndata, string memory errorMessage) private pure returns(bytes memory) {
        if (success) {
            return returndata;
        } else {
            // Look for revert reason and bubble it up if present
            if (returndata.length > 0) {
                // The easiest way to bubble the revert reason is using memory via assembly

                // solhint-disable-next-line no-inline-assembly
                assembly {
                    let returndata_size := mload(returndata)
                    revert(add(32, returndata), returndata_size)
                }
            } else {
                revert(errorMessage);
            }
        }
    }
}

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath
{
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "SafeMath: subtraction overflow");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, "SafeMath: division by zero");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, "SafeMath: modulo by zero");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}

// contract Allowances
// {
//     using SafeMath for uint256;
//     using Address for address;

//     function allowExchangeToMoveMyTokens(
//         SafeERC20 token,
//         address spender
//         ) internal
//     {
//         uint256 allowance = token.allowance(address(this), spender);
//         // If my allowance is zero or getting low
//         if (allowance < uint96(-1))
//         {
//             // if the allowance is nonzero, must reset it to 0 first
//             if (allowance != 0)
//             {
//                 _callOptionalReturn(token, abi.encodeWithSelector(IERC20.approve.selector, spender, 0));
//             }

//             // approve the new allowance
//             _callOptionalReturn(token, abi.encodeWithSelector(IERC20.approve.selector, spender, uint256(-1)));
//         }
//     }

//     // /**
//     //  * @dev Imitates a Solidity high-level call (i.e. a regular function call to a contract), relaxing the requirement
//     //  * on the return value: the return value is optional (but if data is returned, it must not be false).
//     //  * @param token The token targeted by the call.
//     //  * @param data The call data (encoded using abi.encode or one of its variants).
//     //  */
//     // function _callOptionalReturn(
//     //     IERC20 token,
//     //     bytes memory data
//     //     ) private
//     // {
//     //     // We need to perform a low level call here, to bypass Solidity's return data size checking mechanism, since
//     //     // we're implementing it ourselves. We use {Address.functionCall} to perform this call, which verifies that
//     //     // the target address contains contract code and also asserts for success in the low-level call.

//     //     bytes memory returndata = address(token).functionCall(data, "SafeERC20: low-level call failed");
//     //     if (returndata.length > 0) { // Return data is optional
//     //         // solhint-disable-next-line max-line-length
//     //         require(abi.decode(returndata, (bool)), "SafeERC20: ERC20 operation did not succeed");
//     //     }
//     // }
// }

abstract contract WETH is
    IERC20
{
    function deposit() public payable virtual;
    function withdraw(uint wad) public virtual;
}

contract WETH_Utils is
    StorageContract_Properties,
    WhitelistedOwners
{
    function wrapEther(
        uint amount
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        require(address(this).balance >= amount, "Tried to wrap more ETH than we have");
        WETH weth = WETH(ds.wethContract);
        weth.deposit{value: amount}();
    }

    function unwrapEther(
        uint amount
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        WETH weth = WETH(ds.wethContract);
        weth.withdraw(amount);
    }
}

contract Encoding_Utils
{
    function bitWiseRotateLeft(
        uint256 number,
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to left rotate number by d bits
        // # In number<<d, last d bits are 0.
        // # To put first 3 bits of number at
        // # last, do bitwise or of n<<d
        // # with number >>(256 - d)
        return (number << d) | (number >> (256 - d));
    }

    function bitWiseRotateRight(
        uint256 number,
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to right rotate n by d bits
        // # In n>>d, first d bits are 0.
        // # To put last 3 bits of at
        // # first, do bitwise or of n>>d
        // # with n <<(256 - d)
        return (number >> d) | (number << (256 - d)) & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    }

    // function decodeSignature(
    //     uint256[] memory signatureIntList
    //     ) internal pure returns (bytes memory decodedSignature)
    // {
    //     // Python has broken thke signature into hex strings,
    //     // then converted them into ints, then rotated them to the right
    //     // to decode, do the opposite

    //     uint8 numOfRotations = 2;
    //     // Rotate to the left
    //     uint256 x1 = bitWiseRotateLeft(signatureIntList[0], numOfRotations);
    //     uint256 x2 = bitWiseRotateLeft(signatureIntList[1], numOfRotations);
    //     uint256 x3 = bitWiseRotateLeft(signatureIntList[2], numOfRotations);

    //     decodedSignature = new bytes(66);
    //     assembly
    //     {
    //         mstore(add(decodedSignature, 66), x3)
    //         mstore(add(decodedSignature, 50), x2)
    //         mstore(add(decodedSignature, 25), x1)
    //         mstore(decodedSignature, 66)
    //     }

    //     return decodedSignature;
    // }

    // function decode0xAssetData(
    //     uint256[] memory zrxAssetDataIntList
    //     ) internal pure returns (bytes memory decoded0xAssetData)
    // {
    //     // Python has broken thke signature into hex strings,
    //     // then converted them into ints, then rotated them to the right
    //     // to decode, do the opposite

    //     uint8 numOfRotations = 7;
    //     // Rotate to the left
    //     uint256 x1 = bitWiseRotateLeft(zrxAssetDataIntList[0], numOfRotations);
    //     uint256 x2 = bitWiseRotateLeft(zrxAssetDataIntList[1], numOfRotations);

    //     decoded0xAssetData = new bytes(36);
    //     assembly
    //     {
    //         mstore(add(decoded0xAssetData, 36), x2)
    //         mstore(add(decoded0xAssetData, 18), x1)
    //         mstore(decoded0xAssetData, 36)
    //     }

    //     return decoded0xAssetData;
    // }

    function decodeAddress(
        uint256 myAddress
        ) internal pure returns (address decodedAddress)
    {
        // Python has converted myAddress to an int, added an obscure number, and rotated to the right
        // to decode, do the opposite

        // Subtract my random obscure number, no need for safe math here
        myAddress = myAddress - 891754983589020875164519;
        // Rotate to the left
        myAddress = bitWiseRotateLeft(myAddress, 5);

        bytes memory b = new bytes(20);
        assembly
        {
            mstore(add(b, 20), myAddress)
            mstore(b, 20)
        }

        assembly
        {
            decodedAddress := mload(add(b, 20))
        }

        return decodedAddress;
    }

    // function decodeAddressArray(
    //     uint256[] memory encodedAddresses
    //     ) internal pure returns (address[] memory decodedAddressArray)
    // {
    //     // Decode the addresses
    //     decodedAddressArray = new address[](encodedAddresses.length);
    //     for (uint256 i = 0; i < encodedAddresses.length; i++)
    //     {
    //         decodedAddressArray[i] = decodeAddress(encodedAddresses[i]);
    //     }

    //     return decodedAddressArray;
    // }

    // function decodeInt(
    //     uint256 myInt
    //     ) internal pure returns (uint256 decodedNumber)
    // {
    //     // Python code has rotated myInt to the right
    //     // to decode, do the opposite

    //     // Rotate to the left
    //     myInt = bitWiseRotateLeft(myInt, 16);
    //     return myInt;
    // }

    // function decodeIntArray(
    //     uint256[] memory encodedInts
    //     ) internal pure returns (uint256[] memory decodedIntList)
    // {
    //     decodedIntList = new uint256[](encodedInts.length);
    //     for (uint256 i = 0; i < encodedInts.length; i++)
    //     {
    //         decodedIntList[i] = decodeInt(encodedInts[i]);
    //     }

    //     return decodedIntList;
    // }

    function decodeInt_decodeCallDataArrayAndConsiderInjectingInt(
        uint256 myInt
        ) internal pure returns (uint256 decodedNumber)
    {
        // Python code has rotated myInt to the right
        // to decode, do the opposite

        // Rotate to the left
        myInt = bitWiseRotateLeft(myInt, 11);
        return myInt;
    }

    function decodeIntArray_decodeCallDataArrayAndConsiderInjectingInt(
        uint256[] memory encodedInts
        ) internal pure returns (uint256[] memory decodedIntList)
    {
        decodedIntList = new uint256[](encodedInts.length);
        for (uint256 i = 0; i < encodedInts.length; i++)
        {
            decodedIntList[i] = decodeInt_decodeCallDataArrayAndConsiderInjectingInt(encodedInts[i]);
        }

        return decodedIntList;
    }

    function decodeCallDataArrayAndConsiderInjectingInt(
        uint256[] memory callDataArray,
        uint256 quoteTokensToSpend_toInject,
        bool doDecode,
        bool doInjectInt
        ) internal pure returns (bytes memory updatedCallData)
    {
        // callDataArray is made up to two parts
        // the first index in callDataArray is the encoded function selector
        // all the other indexes in callDataArray are the encoded parameters in the calldata

        // Sample input data with the first number as some uint256
        // 0xcb3c28c70000000000000000000000000000000000000000000000000000000001b8141a0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000
        // Sample input data with the first number as FFFF...FFFF showing what to replace with your own uint256
        // 0xcb3c28c7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000

        // The actual input data should be encoded, and this function will decode it

        // // replace the first parameter with the one passed into the function
        // uint256[] memory callDataArray = new uint256[](8);
        // The first parameter is the function selector
        // callDataArray[0] = 11251282108508751410786438484730905108789417455303941525318388289831404398469;
        // // All the other items are the parameters in the calldata
        // callDataArray[1] = 59366061376553713473998796024962062131803458935020796992885968363432024487682;
        // callDataArray[2] = 81020539002477591817371690194067367295966331559958661468723494924294866565208;
        // callDataArray[3] = 100300374173339321621784632522174664355417868639112964214766417266043084725725;
        // callDataArray[4] = 29513413370058131841359401452410130929006865798253763713474781928186275684624;
        // If an int is to be injected into the callData, it will be injected after the function selector
        // So in this example, index 0 is the function selector and index's 1 through 4 are just data.
        // The int would be injected as index 1, and the data that was in index 1 through 4 would move to 2 through 5.

        if (doDecode)
        {
            // Decode the encoded parameters
            callDataArray = decodeIntArray_decodeCallDataArrayAndConsiderInjectingInt(callDataArray);
        }

        uint256 functionSelector = callDataArray[0];
        uint256 callDataArrayLength = callDataArray.length;
        uint8 startIndex;
        uint8 indexAdder_callDataArray;
        if (doInjectInt)
        {
            // Increase the callDataArrayLength by 1 because we're going to inject an int to it
            callDataArrayLength = callDataArrayLength + 1;
            // We are injecting an int, so we need to skip both the function selector and the index of the int we're going to inject later
            // So start at index 1
            startIndex = 2;
            indexAdder_callDataArray = 0;
        }
        else
        {
            // If we're not injecting an int, we only need to skip the function selector
            // So start at index 1
            startIndex = 1;
            indexAdder_callDataArray = 1;
        }

        uint8 functionSelectorLength = 4;
        uint256 updatedCallDataLength = functionSelectorLength + ((callDataArrayLength - 1)* 32);
        uint256 pointer_quoteTokensToSpend_toInject = 32 + functionSelectorLength;

        // Declare the size of our updatedCallData
        updatedCallData = new bytes(updatedCallDataLength);

        // We must use assembly here so it's gas efficient
        assembly
        {
            // Iterate over the array
            // skip the first item since it's the function selector
            // conditionally skip the second item because that's the value we're overwriting anyways (which we'll conditionally do later)
            // for { let i := 2 } lt(i, callDataArrayLength) { i := add(1, i) }
            for { let i := startIndex } lt(i, callDataArrayLength) { i := add(1, i) }
            {
                // TODO figure out what needs to change here with callDataArray's i since I've injected a new varaible that callDataArray doesn't know about

                // TODO the indexes may be fucked up here.  Should the first index ever change??  Should the second index ever change??

                // Write each value to the bytes
                mstore(
                    // position of where we want to write the bytes within updatedCallData
                    add(updatedCallData, add(functionSelectorLength, mul(32, add(0, i)))),
                    // value of what we want to write to updatedCallData
                    // mload(add(callDataArray, mul(32, add(1, i))))
                    mload(add(callDataArray, mul(32, add(indexAdder_callDataArray, i))))
                )
            }

            if eq(doInjectInt, true)
            {
                // Write the value we're overwriting to the bytes
                mstore(add(updatedCallData, pointer_quoteTokensToSpend_toInject), quoteTokensToSpend_toInject)
            }

            // Write the function selector to the bytes
            mstore(add(updatedCallData, functionSelectorLength), functionSelector)

            // Not sure what this does but it's needed to finish up
            mstore(updatedCallData, updatedCallDataLength)
        }

        return updatedCallData;
    }

    function convertIntToFunctionSelector(
        uint256 functionSelector_int
        ) internal pure returns (bytes4 functionSelector)
    {
        bytes memory functionSelector_bytes = new bytes(4);
        assembly 
        {
            mstore(add(functionSelector_bytes, 4), functionSelector_int)
            mstore(functionSelector_bytes, 4)
            functionSelector := mload(add(functionSelector_bytes, 32))
        }
    }

    function getFunctionSelectorFromCallData(
        bytes memory callData
        ) internal pure returns (bytes4 functionSelector)
    {
        assembly 
        {
            functionSelector := mload(add(callData, 32))
        }
    }

    function getArgumentFromCallData_uint256(
        bytes memory callData,
        uint8 index
        ) internal pure returns (uint256 x)
    {
        // This assumes the first 4 bytes are the function selector
        uint256 startPointer = 4 + (index * 32);
        require(callData.length >= startPointer + 32, "extract arg out of range");
        
        assembly
        {
            x := mload(add(callData, add(0x20, startPointer)))
        }
    }
    
    function getArgumentFromCallData_address(
        bytes memory callData,
        uint8 index
        ) internal pure returns (address a)
    {
        // This assumes the first 4 bytes are the function selector
        uint256 startPointer = 4 + (index * 32);
        require(callData.length >= startPointer + 32, "extract arg out of range");
        
        assembly
        {
            a := mload(add(callData, add(0x20, startPointer)))
        }
    }
    
    function getArgumentFromCallData_bool(
        bytes memory callData,
        uint8 index
        ) internal pure returns (bool b)
    {
        // This assumes the first 4 bytes are the function selector
        uint256 startPointer = 4 + (index * 32);
        require(callData.length >= startPointer + 32, "extract arg out of range");
        
        assembly
        {
            b := mload(add(callData, add(0x20, startPointer)))
        }
    }
}

abstract contract KeeperDaoLpp
{
    function borrow(address _token, uint256 _amount, bytes calldata _data) external virtual;
}

abstract contract CHI
{
    function mint(uint256 value) public virtual;
    function free(uint256 value) public virtual returns (uint256);
    // function freeFromUpTo(address from, uint256 value) public virtual returns (uint256);
}

contract Ninja_GasTokens
{
    // This is my local logic contract version of this function.
    function freeGasTokens(
        uint gasTokens
        ) internal returns (uint256 freedAmount)
    {
        // This assumes that this logic contract's proxy is holding gas tokens
        // I'm not wasting gas to check to confirm that I have enough gas tokens
        return CHI(0x0000000000004946c0e9F43F4Dee607b0eF1fA1c).free(gasTokens);
    }
}

abstract contract UniswapV2Pool
{
    function token0() external view virtual returns (address);
    function token1() external view virtual returns (address);
    function swap(uint amount0Out, uint amount1Out, address to, bytes calldata data) external virtual;
    function getReserves() external view virtual returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
}

interface INinja_Exchange_UniswapV2
{
    function checkSimpleRequirements_timestamp_uniswapV2(address exchange) external returns (bool);
    function trade_uniswapV2_pool_spendToken0(uint256 amountIn, bool doSendTokensToPool, 
        address poolContract, address tokenOutRecipient) external returns (uint256 tokensReceived);
    function trade_uniswapV2_pool_spendToken1(uint256 amountIn, bool doSendTokensToPool,
        address poolContract, address tokenOutRecipient) external returns (uint256 tokensReceived);
}

abstract contract BalancerPool
{
    function swapExactAmountIn(
        address tokenIn, uint tokenAmountIn,
        address tokenOut, uint minAmountOut, uint maxPrice
        ) external virtual returns (uint tokenAmountOut, uint spotPriceAfter);
}

interface INinja_Exchange_Balancer
{
    function checkSimpleRequirements_buy_balancer(address quoteToken, uint256 maxQuoteTokenQuantityAcceptable, 
        address balancerPoolContract) external returns (bool);
    function checkSimpleRequirements_sell_balancer(address quoteToken, uint256 minQuoteTokenQuantityAcceptable, 
        address balancerPoolContract) external returns (bool);
    function trade_balancer_pool(uint256 tokenAmountIn, address balancerPoolContract, 
        address tokenIn, address tokenOut) external returns (uint256 tokensReceived);
}

abstract contract Uniswap
{
    function ethToTokenSwapInput(uint256 min_tokens, uint256 deadline) public payable virtual returns (uint256 out);
    function tokenToEthSwapInput(uint256 tokens_sold, uint256 min_eth, uint256 deadline) public virtual returns (uint256 out);
}

interface INinja_Exchange_Uniswap
{
    function checkSimpleRequirements_buy_uniswap(uint256 maxUniswapEthQuantityAcceptable, 
        address uniswapContract) external returns (bool);
    function checkSimpleRequirements_sell_uniswap(uint256 minUniswapEthQuantityAcceptable,
        address uniswapContract) external returns (bool);
    function trade_buy_uniswap(uint256 etherToSpend, address uniswapContract) external returns (uint256 tokensReceived);
    function trade_sell_uniswap(uint256 tokensToSpend, address uniswapContract) external returns (uint256 etherReceived);
}

// abstract contract Debugger
// {
//     function test_uint256(uint256 thingy) public virtual;
//     function test_uint256array(uint256[] memory thingy) public virtual;
//     function test_address(address thingy) public virtual;
//     function test_addressarray(address[] memory thingy) public virtual;
//     function test_bool(bool thingy) public virtual;
//     function test_uint112(uint112 thingy) public  virtual;
//     function test_bytes(bytes memory thingy) public virtual;
//     function test_bytes4(bytes4 thingy) public virtual;
// }

contract Ninja_Trade is
    WhitelistedOwners,
    WhitelistedKeeperDAOLPPs,
    NinjaProxyOwner,
    WETH_Utils,
    Logs,
    Logs_HidingGame,
    Encoding_Utils,
    Ninja_GasTokens,
    StorageContract_Proxy
{
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    // ***** Begin Generic Trade functions ***** //

    function tradeSimpleRequirements(
        uint256[] calldata quoteTokensToSpendArray,
        uint256 quoteToken_encoded,
        uint256[][] calldata checkCallDataArrays_encoded,
        uint256[] calldata tradeCallDataArray_encoded,
        bool doTradeWithinFlashLoan
        ) external onlyWhitelist returns (uint256 profit_quoteTokens)
    {
        // quoteTokensToSpendArray should have 1 less item than checkCallDataArrays_encoded
        // For a 2 leg trade
        // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade

        // For a 3 leg trade
        // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade
        // quoteTokensToSpendArray[1] = quoteTokensToSpend for second trade

        // For a 4 leg trade
        // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade
        // quoteTokensToSpendArray[1] = quoteTokensToSpend for second trade
        // quoteTokensToSpendArray[2] = quoteTokensToSpend for third trade

        address quoteToken = decodeAddress(quoteToken_encoded);
        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(quoteToken);

        // Iterate over the checks and execute them all to enforce that profit is still available before we consider trading
		for (uint8 i = 0; i < checkCallDataArrays_encoded.length; i++)
		{
		    // Set quoteTokensToSpend based on index
		    uint256 quoteTokensToSpend;
		    // If index is the first or the last
		    if (i == 0 || i == checkCallDataArrays_encoded.length - 1)
		    {
		        // Just use the first value in quoteTokensToSpendArray
		        quoteTokensToSpend = quoteTokensToSpendArray[0];
		    }
		    else
		    {
		        // Use the index i
		        quoteTokensToSpend = quoteTokensToSpendArray[i];
		    }

            // Perform the check on the trade to confirm profitability and update our new min percentage
            // Decode my callData
            bool priceVerifiedSuccessfully = performCheck(
                decodeCallDataArrayAndConsiderInjectingInt(
                    checkCallDataArrays_encoded[i], 0, true, false));

            if (!priceVerifiedSuccessfully)
            {
                // Our check failed and this trade is not profitable
                return handlePostTradeActions(
                    beforeTradeDataArray_ints[0], 
                    beforeTradeDataArray_ints[1],
                    quoteToken);
            }
		}

        // If we've made it this far without returning,
        // all of our checks have passed and we're assuming that profit is still available
        if (doTradeWithinFlashLoan)
        {
            // Initiate the flash loan and trade
            initiateTradeWithinFlashLoan(
                quoteTokensToSpendArray[0],
                quoteToken,
                tradeCallDataArray_encoded);
        }
        else
        {
            // Trade using funds in this contract, no flash loan
            initiateTradeWithoutFlashLoan(
                decodeCallDataArrayAndConsiderInjectingInt(
                    tradeCallDataArray_encoded,
                    quoteTokensToSpendArray[0],
                    true,
                    true));
        }

        return handlePostTradeActions(
            beforeTradeDataArray_ints[0], 
            beforeTradeDataArray_ints[1], 
            quoteToken);
    }

    function performCheck(
        bytes memory checkCallData
        ) internal returns (bool priceVerifiedSuccessfully)
    {
        DiamondStorage_Proxy storage ds_proxy = diamondStorage_Proxy();
        // Get the functionSelector
        // Get facet address of function within the proxy
        // Make the delegatecall on the logic contract
        bytes4 functionSelector = getFunctionSelectorFromCallData(checkCallData);
        address facet = address(bytes20(ds_proxy.facets[functionSelector]));

        // Determine if I call a local function or delegatecall
        // I have to compare with a function selector from an interface since I cannot get it from a private/internal function
        // All local function should be private even if they're copied over from external contract
        // The external contract's function should still be accessible in case I ever remove it from this contract
        // It's more gas efficient to call the function locally, but only limited code will fit in this contract
        // It costs more gas to do a delegatecall, but I can scale infinitely using delegatecall
        if (functionSelector == INinja_Exchange_UniswapV2.checkSimpleRequirements_timestamp_uniswapV2.selector)
        {
            priceVerifiedSuccessfully = checkSimpleRequirements_timestamp_uniswapV2(
                getArgumentFromCallData_address(checkCallData, 0));
        }
        else if (functionSelector == INinja_Exchange_Balancer.checkSimpleRequirements_buy_balancer.selector)
        {
            priceVerifiedSuccessfully = checkSimpleRequirements_buy_balancer(
                getArgumentFromCallData_address(checkCallData, 0), 
                getArgumentFromCallData_uint256(checkCallData, 1),
                getArgumentFromCallData_address(checkCallData, 2));
        }
        else if (functionSelector == INinja_Exchange_Balancer.checkSimpleRequirements_sell_balancer.selector)
        {
            priceVerifiedSuccessfully = checkSimpleRequirements_sell_balancer(
                getArgumentFromCallData_address(checkCallData, 0), 
                getArgumentFromCallData_uint256(checkCallData, 1),
                getArgumentFromCallData_address(checkCallData, 2));
        }
        else if (functionSelector == INinja_Exchange_Uniswap.checkSimpleRequirements_buy_uniswap.selector)
        {
            priceVerifiedSuccessfully = checkSimpleRequirements_buy_uniswap(
                getArgumentFromCallData_uint256(checkCallData, 0),
                getArgumentFromCallData_address(checkCallData, 1));
        }
        else if (functionSelector == INinja_Exchange_Uniswap.checkSimpleRequirements_sell_uniswap.selector)
        {
            priceVerifiedSuccessfully = checkSimpleRequirements_sell_uniswap(
                getArgumentFromCallData_uint256(checkCallData, 0),
                getArgumentFromCallData_address(checkCallData, 1));
        }
        else
        {
            // If we made it to the else, we did not find a local implementation of this function
            // So we will fallback to the delegatecall to execute this function call
            // It will still work, but it will be less gas efficient than running the code locally
            (bool success, bytes memory callResult) = facet.delegatecall(checkCallData);
            require(success, "performCheck failed during execution");
            assembly
            {
                priceVerifiedSuccessfully := mload(add(callResult,0x20))
            }
        }

        return priceVerifiedSuccessfully;
    }

    function initiateTradeWithoutFlashLoan(
        bytes memory tradeCallData
        ) internal
    {
        // Works
        (bool success,) = address(this).call{value:0}(tradeCallData);
        require(success, "initiateTrade failed during execution");

        // Does not work, no idea why because it should do the same thing... 
        // Maybe I made a mistake deploying the contract?  I should try again and see if it works the 2nd time around
        // DiamondStorage_Proxy storage ds_proxy = diamondStorage_Proxy();
        // // Get the functionSelector
        // // Get facet address of function within the proxy
        // // Make the delegatecall on the logic contract
        // (bool success,) = address(bytes20(ds_proxy.facets[getFunctionSelectorFromCallData(tradeCallData)])).delegatecall(tradeCallData);
        // require(success, "initiateTrade failed during execution");
    }

    function initiateTradeWithinFlashLoan(
        uint256 quoteTokensToSpend,
        address quoteToken,
        uint256[] memory tradeCallDataArray_encoded
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // Flash loan from KeeperDAO, and trade via appropriate tradeWithinFlashLoan
        KeeperDaoLpp(ds.keeperDaoLppContract_Eth).borrow(
            quoteToken,
            quoteTokensToSpend,
            // Update the quantity we are trading in the calldata, since that could have changed somewhere along the way
            decodeCallDataArrayAndConsiderInjectingInt(
                tradeCallDataArray_encoded, quoteTokensToSpend, true, true));
    }

    function tradeWithinFlashLoan(
        uint256 borrowedQuoteTokens,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address quoteToken,
        uint256[][] memory exchangeSpecificTradeCallDataArrays,
        bool doWrapEthBeforeFirstTrade,
        bool doUnwrapEthAfterLastTrade
        ) public onlyWhitelistedKeeperDAOLPPs
    {
        preparePerformTrade(borrowedQuoteTokens, quoteToken, exchangeSpecificTradeCallDataArrays,
            doWrapEthBeforeFirstTrade, doUnwrapEthAfterLastTrade, true);
    }

    function tradeWithoutFlashLoan(
        uint256 quoteTokenQuantity,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address quoteToken,
        uint256[][] memory exchangeSpecificTradeCallDataArrays,
        bool doWrapEthBeforeFirstTrade,
        bool doUnwrapEthAfterLastTrade
        ) public onlyNinjaProxy
    {
        preparePerformTrade(quoteTokenQuantity, quoteToken, exchangeSpecificTradeCallDataArrays,
            doWrapEthBeforeFirstTrade, doUnwrapEthAfterLastTrade, false);
    }

    function preparePerformTrade(
        uint256 quoteTokenQuantity,
        address quoteToken,
        uint256[][] memory exchangeSpecificTradeCallDataArrays,
        bool doWrapEthBeforeFirstTrade,
        bool doUnwrapEthAfterLastTrade,
        bool doTradeWithinFlashLoan
        ) internal
    {
        uint256 quoteTokenBalance_beforeTrading = getThisContractsTokenBalance(quoteToken);

        // Do I need to wrap ETH before the trade?
        if (doWrapEthBeforeFirstTrade)
        {
            // Assume that if doWrapEthBeforeFirstTrade is set that quoteToken is ETH
            // wrap ETH that we're trading
            wrapEther(quoteTokenQuantity);
        }

        // ****** Begin trading logic ****** //
        uint256 tokensToSpend;
        uint256 tokensReceived;
        // Iterate over the trades and execute them all
        for (uint8 i = 0; i < exchangeSpecificTradeCallDataArrays.length; i++)
		{
            if (i == 0)
            {
                // Spend the tokens we borrowed
                tokensToSpend = quoteTokenQuantity;
            }
            else
            {
                // Spend the tokens we received from the previous trade
                tokensToSpend = tokensReceived;
            }

            // Decode my callData and update the quoteTokensToSpend in the callData
            bytes memory tradeCallData = decodeCallDataArrayAndConsiderInjectingInt(
                exchangeSpecificTradeCallDataArrays[i], tokensToSpend, false, true);

            DiamondStorage_Proxy storage ds_proxy = diamondStorage_Proxy();
            // Get the functionSelector and convert it from an int to a bytes4
            // Get facet address of function within the proxy
            bytes4 functionSelector = convertIntToFunctionSelector(exchangeSpecificTradeCallDataArrays[i][0]);
            // bytes4 functionSelector = getFunctionSelectorFromCallData(tradeCallData);
            address facet = address(bytes20(ds_proxy.facets[functionSelector]));
            tokensReceived = performTrade(tradeCallData, facet, functionSelector);
        }
        // ****** End trading logic ****** //

        // Do I need to wrap ETH after the trade?
        if (doUnwrapEthAfterLastTrade)
        {
            // Assume that if doUnwrapEthAfterLastTrade is set that tokensReceived is WETH
            // Unwrap the WETH I received from the trade
            unwrapEther(tokensReceived);
        }

        if (doTradeWithinFlashLoan)
        {
            returnBorrowedQuoteTokensAndShareProfit(
                quoteTokenQuantity, quoteToken, quoteTokenBalance_beforeTrading);
        }
    }

    function performTrade(
        bytes memory tradeCallData,
        address facet,
        bytes4 functionSelector
        ) internal returns (uint256 tokensReceived)
    {
        // address debugger = 0xb5a944194c4723866027f87D7703A670C104Fa9E;
        // Debugger(debugger).test_bytes(tradeCallData);
        // Debugger(debugger).test_address(facet);
        // Debugger(debugger).test_bytes4(functionSelector);
        // Debugger(debugger).test_uint256(getArgumentFromCallData_uint256(tradeCallData, 0));
        // Debugger(debugger).test_uint256(getArgumentFromCallData_uint256(tradeCallData, 1));

        // Determine if I call a local function or delegatecall
        // I have to compare with a function selector from an interface since I cannot get it from a private/internal function
        // All local function should be private even if they're copied over from external contract
        // The external contract's function should still be accessible in case I ever remove it from this contract
        // It's more gas efficient to call the function locally, but only limited code will fit in this contract
        // It costs more gas to do a delegatecall, but I can scale infinitely using delegatecall
        if (functionSelector == INinja_Exchange_UniswapV2.trade_uniswapV2_pool_spendToken0.selector)
        {
            tokensReceived = trade_uniswapV2_pool_spendToken0(
                getArgumentFromCallData_uint256(tradeCallData, 0),
                getArgumentFromCallData_bool(tradeCallData, 1),
                getArgumentFromCallData_address(tradeCallData, 2),
                getArgumentFromCallData_address(tradeCallData, 3));
        }
        else if (functionSelector == INinja_Exchange_UniswapV2.trade_uniswapV2_pool_spendToken1.selector)
        {
            tokensReceived = trade_uniswapV2_pool_spendToken1(
                getArgumentFromCallData_uint256(tradeCallData, 0),
                getArgumentFromCallData_bool(tradeCallData, 1),
                getArgumentFromCallData_address(tradeCallData, 2),
                getArgumentFromCallData_address(tradeCallData, 3));
        }
        else if (functionSelector == INinja_Exchange_Balancer.trade_balancer_pool.selector)
        {
            tokensReceived = trade_balancer_pool(
                getArgumentFromCallData_uint256(tradeCallData, 0),
                getArgumentFromCallData_address(tradeCallData, 1),
                getArgumentFromCallData_address(tradeCallData, 2),
                getArgumentFromCallData_address(tradeCallData, 3));
        }
        else if (functionSelector == INinja_Exchange_Uniswap.trade_buy_uniswap.selector)
        {
            tokensReceived = trade_buy_uniswap(
                getArgumentFromCallData_uint256(tradeCallData, 0),
                getArgumentFromCallData_address(tradeCallData, 1));
        }
        else if (functionSelector == INinja_Exchange_Uniswap.trade_sell_uniswap.selector)
        {
            tokensReceived = trade_sell_uniswap(
                getArgumentFromCallData_uint256(tradeCallData, 0),
                getArgumentFromCallData_address(tradeCallData, 1));
        }
        else
        {
            // If we made it to the else, we did not find a local implementation of this function
            // So we will fallback to the delegatecall to execute this function call
            // It will still work, but it will be less gas efficient than running the code locally
            (bool success, bytes memory callResult) = facet.delegatecall(tradeCallData);
            require(success, "performTrade failed during execution");
            assembly
            {
                tokensReceived := mload(add(callResult,0x20))
            }
        }

        return tokensReceived;
    }

    // ***** End Generic Trade functions ***** //

    // ***** Begin UniswapV2 functions ***** //

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_timestamp_uniswapV2(
        address exchange
        ) internal returns (bool)
    {
        (, , uint32 blockTimestampLast) = UniswapV2Pool(exchange).getReserves();
        // The timestamp will be set to the current block's timestamp if someone beat me to the trade
        // This is an imperfect strategy, but it is the most gas efficient and will work most of the time
        // uint32 currentBlockTimestamp = uint32(block.timestamp % 2**32);
        if (blockTimestampLast == uint32(block.timestamp % 2**32))
        {
            logCode_ifEnabled(30);
            return false;
        }

        return true;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_uniswapV2_pool_spendToken0(
        uint256 amountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        bool doSendTokensToPool,
        address poolContract,
        address tokenOutRecipient  // Based on doSendTokensToPool, either another UniswapV2 pool or this proxy contract
        ) internal returns (uint256 tokensReceived)
    {
        // Get balances of the pool
        (
            uint112 reserve0,
            uint112 reserve1,
        ) = UniswapV2Pool(poolContract).getReserves();

        UniswapV2Pool pool = UniswapV2Pool(poolContract);
        address token0_in = pool.token0();

        uint256 amountOut = convertAmountInToAmountOut_uniswapV2(
            amountIn, reserve0,  reserve1);

        // To trade directly with the pools, we must approve each individual pool as a spender
        // It's nice to do this ahead of time at safe low gas prices
        // but there are so many pools that can be impossible
        // So we're approving now before the trade only if needed
        IERC20(token0_in).approveMaxOnlyIfNeeded(poolContract);

        // Trade
        if (doSendTokensToPool)
        {
            // Before I call the swap function, I must send the exchange the amountIn
            IERC20(token0_in).safeTransfer(poolContract, amountIn);
        }
        // else we assume that tokenIn has already been transfered to poolContract by another trade
        pool.swap(0, amountOut, tokenOutRecipient, new bytes(0));
        return amountOut;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_uniswapV2_pool_spendToken1(
        uint256 amountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        bool doSendTokensToPool,
        address poolContract,
        address tokenOutRecipient  // Based on doSendTokensToPool, either another UniswapV2 pool or this proxy contract
        ) internal returns (uint256 tokensReceived)
    {
        // Get balances of the pool
        (
            uint112 reserve0,
            uint112 reserve1,
        ) = UniswapV2Pool(poolContract).getReserves();

        UniswapV2Pool pool = UniswapV2Pool(poolContract);
        address token1_in = pool.token1();

        uint256 amountOut = convertAmountInToAmountOut_uniswapV2(
            amountIn, reserve1,  reserve0);

        // To trade directly with the pools, we must approve each individual pool as a spender
        // It's nice to do this ahead of time at safe low gas prices
        // but there are so many pools that can be impossible
        // So we're approving now before the trade only if needed
        IERC20(token1_in).approveMaxOnlyIfNeeded(poolContract);

        // Trade
        if (doSendTokensToPool)
        {
            // Before I call the swap function, I must send the exchange the amountIn
            IERC20(token1_in).safeTransfer(poolContract, amountIn);
        }
        // else we assume that tokenIn has already been transfered to poolContract by another trade
        pool.swap(amountOut, 0, tokenOutRecipient, new bytes(0));
        return amountOut;
    }

    function convertAmountInToAmountOut_uniswapV2(
        uint256 amountIn,
        uint256 reserveIn,
        uint256 reserveOut
        ) internal pure returns (uint256 amountOut)
    {
        // given an input amount of an asset and pair reserves, returns the maximum output amount of the other asset
        require(amountIn > 0, 'convertAmountInToAmountOut: INSUFFICIENT_INPUT_AMOUNT');
        require(reserveIn > 0 && reserveOut > 0, 'convertAmountInToAmountOut: INSUFFICIENT_LIQUIDITY');
        uint amountInWithFee = amountIn.mul(997);
        uint numerator = amountInWithFee.mul(reserveOut);
        uint denominator = reserveIn.mul(1000).add(amountInWithFee);
        return numerator / denominator;
    }

    // ***** End UniswapV2 functions ***** //

    // ***** Begin Balancer functions ***** //

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_buy_balancer(
        address quoteToken,
        uint256 maxQuoteTokenQuantityAcceptable,
        address balancerPoolContract
        ) internal returns (bool)
    {
        uint256 quoteTokenBalance = IERC20(quoteToken).balanceOf(balancerPoolContract);

        if (quoteTokenBalance > maxQuoteTokenQuantityAcceptable)
        {
            logCodeWithValue_ifEnabled(28, quoteTokenBalance);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_sell_balancer(
        address quoteToken,
        uint256 minQuoteTokenQuantityAcceptable,
        address balancerPoolContract
        ) internal returns (bool)
    {
        uint256 quoteTokenBalance = IERC20(quoteToken).balanceOf(balancerPoolContract);

        if (quoteTokenBalance < minQuoteTokenQuantityAcceptable)
        {
            logCodeWithValue_ifEnabled(28, quoteTokenBalance);
            return false;
        }

        return true;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_balancer_pool(
        uint256 tokenAmountIn,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address balancerPoolContract,
        address tokenIn,
        address tokenOut
        ) internal returns (uint256 tokensReceived)
    {
        // To trade directly with the pools, we must approve each individual pool as a spender
        // It's nice to do this ahead of time at safe low gas prices
        // but there are so many pools that can be impossible
        // So we're approving now before the trade only if needed
        IERC20(tokenIn).approveMaxOnlyIfNeeded(balancerPoolContract);

        (tokensReceived,) = BalancerPool(balancerPoolContract).swapExactAmountIn(
            tokenIn, tokenAmountIn,
            tokenOut, 1, 115792089237316195423570985008687907853269984665640564039457584007913129639935);

        return tokensReceived;
    }

    // ***** End Balancer functions ***** //

    // ***** Begin Uniswap functions ***** //

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_buy_uniswap(
        uint256 maxUniswapEthQuantityAcceptable,
        address uniswapContract
        ) internal returns (bool)
    {
        uint256 uniswapEthBalance = address(uniswapContract).balance;

        if (uniswapEthBalance > maxUniswapEthQuantityAcceptable)
        {
            logCodeWithValue_ifEnabled(10, uniswapEthBalance);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_sell_uniswap(
        uint256 minUniswapEthQuantityAcceptable,
        address uniswapContract
        ) internal returns (bool)
    {
        uint256 uniswapEthBalance = address(uniswapContract).balance;

        if (uniswapEthBalance < minUniswapEthQuantityAcceptable)
        {
            logCodeWithValue_ifEnabled(10, uniswapEthBalance);
            return false;
        }

        return true;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_buy_uniswap(
        uint256 etherToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address uniswapContract
        ) internal returns (uint256 tokensReceived)
    {
        return Uniswap(uniswapContract).ethToTokenSwapInput{value: etherToSpend}(
            1, 69203865833239757421118596509098632427930889272824243351707071692229331386368);
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_sell_uniswap(
        uint256 tokensToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address uniswapContract
        ) internal returns (uint256 etherReceived)
    {
        return Uniswap(uniswapContract).tokenToEthSwapInput(
            tokensToSpend, 1, 69203865833239757421118596509098632427930889272824243351707071692229331386368);
    }

    // ***** End Uniswap functions ***** //

    // ***** Begin KeeperDAO functions ***** //

    function returnBorrowedQuoteTokensAndShareProfit(
        uint256 borrowedQuoteTokens,
        address quoteToken,
        uint256 quoteTokenBalance_beforeTrading
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 quoteTokenBalance_afterTrading = getThisContractsTokenBalance(quoteToken);

        // Calculate the sharableProfitQuantity
        uint256 profitToShareWithKeeperDAO = 0;
        // Calculate profit made in quoteTokens
        if (quoteTokenBalance_afterTrading > quoteTokenBalance_beforeTrading)
        {
            // We've made profit in quoteTokens
            // Determine how much profit to share with KeeperDAO in a gas efficient way
            profitToShareWithKeeperDAO = quoteTokenBalance_afterTrading.sub(quoteTokenBalance_beforeTrading);
            profitToShareWithKeeperDAO = (profitToShareWithKeeperDAO.mul(
                1000000000000000000)).div(ds.divider_profitToShareWithKeeperDAO);
        }
        // Else, we had a loss in quoteTokens.
        // This means one of two things:
        // 1.) The profit was actually made in baseTokens (and that may be left as dust)
        // 2.) The trade was not proftiable!
        //     NOTE: A non-profitable trade will NOT be enforced/reverted here.
        //           It's up to the other trade logic to determine whether or not we should revert
        //           This function will attempt to return the amount borrowed in quoteTokens even if that means this transaction results in a loss
        //           If this function tries to return more quoteTokens than it has,
        //              the tx will revert/fail because that's not possible.
        //           If this function succeeds in returning the borrowed quoteTokens,
        //              it's up to the other trade logic to decide if the trade was profitable or if it needs reverted

        // Return the borrowedQuoteTokens + profitToShareWithKeeperDAO
        sendTokensToKeeperDAO(
            ds,
            borrowedQuoteTokens.add(profitToShareWithKeeperDAO),
            quoteToken);
    }

    function shareProfitWithKeeperDAO(
        uint256 profitToShare,
        address token
        ) external onlyWhitelist
    {
        uint256 tokenBalance = getThisContractsTokenBalance(token);
        require(tokenBalance >= profitToShare, "shareProfitWithKeeperDAO: Insufficient balance");

        uint256 borrowAmount = 1;
        uint256 returnAmount = profitToShare.add(borrowAmount);

        // Flash loan from KeeperDAO, then immediately return so that we can share profit
        // To keep the math simple, we're borrowing 1 wei, and then returning profitToShare + 1
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        KeeperDaoLpp(ds.keeperDaoLppContract_Eth).borrow(
            token,
            borrowAmount,
            abi.encodeWithSelector(
                this.shareProfitWithKeeperDAOCallback.selector,
                returnAmount,
                token
            )
        );
    }

    function shareProfitWithKeeperDAOCallback(
        uint256 tokensToReturn,
        address token
        ) external onlyWhitelistedKeeperDAOLPPs
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 tokenBalance = getThisContractsTokenBalance(token);
        require(tokenBalance >= tokensToReturn, "shareProfitWithKeeperDAOCallback: Insufficient balance");

        // Return the borrowed + profitToShareWithKeeperDAO
        sendTokensToKeeperDAO(ds, tokensToReturn, token);
    }

    function sendTokensToKeeperDAO(
        DiamondStorage_Properties storage ds,
        uint256 tokensToSend,
        address token
        ) internal
    {
        // If it's ETH
        if (token == 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE)
        {
            // Send ETH
            (bool success, ) = ds.keeperDaoLppContract_Eth.call{value: tokensToSend}("");
            require(success, "Transfer of borrowedQuoteTokens back to LP has failed.");
        }
        // Else assume it's an ERC20 token
        else
        {
            // Send ERC20
            // bool success = IERC20(token).transfer(address(ds.keeperDaoLppContract_Eth), tokensToSend);
            // require(success, "Transfer of borrowedQuoteTokens back to LP has failed.");

            // I changed this over to safeTransfer
            IERC20(token).safeTransfer(address(ds.keeperDaoLppContract_Eth), tokensToSend);
        }
    }

    // ***** End KeeperDAO functions ***** //

    // ***** Begin Utility functions ***** //

    function handlePostTradeActions(
        uint256 quoteTokenBalance_beforeTrading,
        uint256 gasBefore,
        address quoteToken
        ) internal returns (uint256 profit_quoteTokens)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // The 0x integration here tends to get a lot of dust in base tokens as well as quote tokens
        // So this number needs to either be pretty high (AKA 0.01 quote tokens)
        // or we need to calcualte the dust in both base and quote which is pretty time consuming and costs a lot of gas
        // To avoid timing consuming and expensive calulations, Just hard code in a number here that works most all the time
        // And put trust into the ninja code that's calling this contract
        // uint256 maxTradeLossAcceptable = 10000000000000000;  // I was seeing code 5's due to dust with this amount...
        // uint256 maxTradeLossAcceptable = 30000000000000000;
        // uint256 maxTradeLossAcceptable = 0;

        uint256 quoteTokenBalance_afterTrading = getThisContractsTokenBalance(quoteToken);
        // If I lost quoteTokens during this charade (considering the max amount we're allowed to lose)
        if (quoteTokenBalance_afterTrading < quoteTokenBalance_beforeTrading)
        {
            // Do not let it trade since it lost ether!
            // I may have tokens left over though so I may actually want to let this trade go through in some cases.
            // Question is, how complicated is that logic and is it worth it?
            // I'll need to check before and after token balances and then value them with respect to ETH
            revert("Code 5");
        }
        // else profit or break even
        else
        {
            // Event log for KeeperDAO Hiding Game
            // If maxQuoteTokenTradeLossAcceptable is not zero, this logic gets complicated and I don't want to bother with that right now
            // I currently do not support protocols with a non zero maxQuoteTokenTradeLossAcceptable anyways, 
            // I'm disabling Set for the Hiding Game and removing maxQuoteTokenTradeLossAcceptable
            profit_quoteTokens = quoteTokenBalance_afterTrading.sub(quoteTokenBalance_beforeTrading);
            emit Event_LogCode(quoteToken, profit_quoteTokens);

            // Only consider spending gasTokens to save gas if this tx gas price is high enough to make it worth it
            // I may send a trade through at SafeLow and it doesn't make sense to burn gas tokens
            // So check gasPriceThresholdForSpendingGasTokens
            if (tx.gasprice > ds.gasPriceThresholdForSpendingGasTokens)
            {
                // Have to add in the gas cost to perform a transaction call on an empty function
                // For this i'm using 21483
                // uint256 gasUsed = safeSub(safeAdd(21483, gasBefore), gasleft());
                uint256 gasUsed = 21000 + gasBefore - gasleft() + (16 * msg.data.length);
                // logCodeWithValue_ifEnabled(8, gasUsed);
                // Determine how many gas tokens to burn to save gas
                uint256 gasTokensToFree = (gasUsed + 14154) / 41947;
                // logCodeWithValue_ifEnabled(9, gasTokensToFree);
                if (gasTokensToFree > 0)
                {
                    freeGasTokens(gasTokensToFree);
                }
            }

            return profit_quoteTokens;
        }
    }

    function getMinQuoteTokensToSpendAllowed(
        uint quoteTokensToSpend
        ) internal view returns (uint minQuoteTokensToSpendAllowed)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        return quoteTokensToSpend.div(ds.minQuoteTokenAllowedToSpendDivider);
    }

    function getBeforeTradeDataArray_ints(
        address quoteToken
        ) internal view returns (uint256[] memory beforeTradeDataArray_ints)
    {
        beforeTradeDataArray_ints = new uint256[](3);
        beforeTradeDataArray_ints[0] = getThisContractsTokenBalance(quoteToken);  // quoteTokenBalance_beforeTrading
        beforeTradeDataArray_ints[1] = gasleft();  // gasBefore
        return beforeTradeDataArray_ints;
    }

    function getThisContractsTokenBalance(
        address token
        ) internal view returns (uint256 balance)
    {
        // If it's ETH
        if (token == 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE)
        {
            return address(this).balance;
        }
        // Else assume it's an ERC20 token
        else
        {
            return IERC20(token).balanceOf(address(this));
        }
    }

    // ***** End Utility functions ***** //
}
