pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }

    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract WhitelistedOwners is
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

contract StorageContract_Authentication_KeeperDAOLPPs
{
    struct DiamondStorage_Authentication_KeeperDAOLPPs
    {
        mapping (address => bool) whitelistedKeeperDAOLPPs;
    }

    function diamondStorage_Authentication_KeeperDAOLPPs() internal pure returns(DiamondStorage_Authentication_KeeperDAOLPPs storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication.keeperdaolpps");
        assembly { ds_slot := 0x4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f0 }
    }
}

contract Ninja_Authentication_KeeperDAOLPPs is
    WhitelistedOwners,
    StorageContract_Authentication_KeeperDAOLPPs
{
    function whitelistKeeperDAOLPPs (
        address[] memory users,
        bool value
        ) public onlyWhitelist
    {
        // Only the owner can modify the whitelist
        DiamondStorage_Authentication_KeeperDAOLPPs storage ds = diamondStorage_Authentication_KeeperDAOLPPs();

        for(uint i = 0; i < users.length; i++)
		{
		    ds.whitelistedKeeperDAOLPPs[users[i]] = value;
		}
    }
}
