pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Authentication
{
    struct DiamondStorage_Authentication
    {
        // This should NEVER be modified outside of the proxy.
        mapping (address => bool) whitelistedUsers;
    }

    function diamondStorage_Authentication() internal pure returns(DiamondStorage_Authentication storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication");
        assembly { ds_slot := 0x23c99902bd20175af7869f805847c34572dcf321a973ed1a88e16e09df61afe6 }
    }
}

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray contains only all addresses in here in the exact order they are listed below
        // intArray contains only all addresses in here in the exact order they are listed below

        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // Enables/Disables informative event logging. 1 means yes, 0 means no
        uint256 doLogEvents;
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;

        // Whitelists to ensure only ninjaProxyContract can call trade functions
        address ninjaProxyContract;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract StorageContract_Authentication_KeeperDAOTransferProxys
{
    struct DiamondStorage_Authentication_KeeperDAOTransferProxys
    {
        mapping (address => bool) whitelistedKeeperDAOTransferProxys;
    }

    function diamondStorage_Authentication_KeeperDAOTransferProxys() internal pure returns(DiamondStorage_Authentication_KeeperDAOTransferProxys storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication.keeperdaolpps");
        assembly { ds_slot := 0x4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f0 }
    }
}

contract WhitelistedOwners is
    StorageContract_Authentication
{
    modifier onlyWhitelist()
    {
        DiamondStorage_Authentication storage ds = diamondStorage_Authentication();
        require(ds.whitelistedUsers[msg.sender] == true, "Must be whitelisted.");
        _;
    }
}

contract WhitelistedKeeperDAOTransferProxys is
    StorageContract_Authentication_KeeperDAOTransferProxys
{
    modifier onlyWhitelistedKeeperDAOTransferProxys()
    {
        DiamondStorage_Authentication_KeeperDAOTransferProxys storage ds = diamondStorage_Authentication_KeeperDAOTransferProxys();
        require(ds.whitelistedKeeperDAOTransferProxys[msg.sender] == true, "Must be whitelisted KeeperDAO transfer proxy.");
        _;
    }
}

contract NinjaProxyOwner is
    StorageContract_Properties
{
    modifier onlyNinjaProxy()
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        require(msg.sender == ds.ninjaProxyContract, "Must be Ninja");
        _;
    }
}

contract Logs is
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0xv3 order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth trading
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it
    // 27 = UniswapV2 ETH balance fell outside requirement
    // 28 = Balancer QuoteTokens balance fell outside requirement

    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);

    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }

    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

contract SafeMath
{
    function safeMul(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        require(c / a == b);
        require(
            c / a == b,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    function safeDiv(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a / b;
        return c;
    }

    function safeSub(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        require(b <= a);
        require(
            b <= a,
            "UINT256_UNDERFLOW"
        );
        return a - b;
    }

    function safeAdd(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a + b;
        require(c >= a);
        require(
            c >= a,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    // function max64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    // function min64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a < b ? a : b;
    // }

    // function max256(
    //     uint256 a, uint256 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    function min256(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        return a < b ? a : b;
    }

    function safetifyThisNumber(
        uint256 value,
        uint256 factor
        ) internal pure returns (uint256)
    {
        // Multiply by ~99%, just to be safe so there isn't a round up problem
        // In solididty, it's safer to underfill an order by a hair than to risk overfilling it resulting in an exception
        // To multiply by 99% simply pass in the number 99.
        // It will mulitply by 99 then divide by 100 in order to achieve the desired result without having an exception
        // To multiply by 99.99%, simply pass in the number 9999.
        // It will mulitply by 9999 then divide by 10000 in order to achieve the desired result without having an exception
        value = safeMul(value, factor);
        value = safeDiv(value, safeAdd(factor, 1));
        return value;
    }
}

abstract contract ERC20
{
    function balanceOf(address tokenOwner) public view virtual returns (uint balance);
    function transfer(address toAddress, uint tokens) public virtual returns (bool success);
//     function approve(address _spender, uint256 _value) public virtual returns (bool success);
//     function allowance(address _owner, address _spender) public view virtual returns (uint256 remaining);
//     // function decimals() public view returns (uint8 decimals);
}

abstract contract WETH is
    ERC20
{
    function deposit() public payable virtual;
    function withdraw(uint wad) public virtual;
}

contract WETH_Utils is
    StorageContract_Properties,
    WhitelistedOwners
{
    function wrapEther(
        uint amount
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        require(address(this).balance >= amount, "Tried to wrap more ETH than we have");
        WETH weth = WETH(ds.wethContract);
        weth.deposit{value: amount}();
    }

    function unwrapEther(
        uint amount
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        WETH weth = WETH(ds.wethContract);
        weth.withdraw(amount);
    }
}

contract Encoding_Utils
{
    function bitWiseRotateLeft(
        uint256 number,
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to left rotate number by d bits
        // # In number<<d, last d bits are 0.
        // # To put first 3 bits of number at
        // # last, do bitwise or of n<<d
        // # with number >>(256 - d)
        return (number << d) | (number >> (256 - d));
    }

    function bitWiseRotateRight(
        uint256 number,
        uint256 d
        ) internal pure returns (uint256)
    {
        // # Function to right rotate n by d bits
        // # In n>>d, first d bits are 0.
        // # To put last 3 bits of at
        // # first, do bitwise or of n>>d
        // # with n <<(256 - d)
        return (number >> d) | (number << (256 - d)) & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    }

    // function decodeSignature(
    //     uint256[] memory signatureIntList
    //     ) internal pure returns (bytes memory decodedSignature)
    // {
    //     // Python has broken thke signature into hex strings,
    //     // then converted them into ints, then rotated them to the right
    //     // to decode, do the opposite

    //     uint8 numOfRotations = 2;
    //     // Rotate to the left
    //     uint256 x1 = bitWiseRotateLeft(signatureIntList[0], numOfRotations);
    //     uint256 x2 = bitWiseRotateLeft(signatureIntList[1], numOfRotations);
    //     uint256 x3 = bitWiseRotateLeft(signatureIntList[2], numOfRotations);

    //     decodedSignature = new bytes(66);
    //     assembly
    //     {
    //         mstore(add(decodedSignature, 66), x3)
    //         mstore(add(decodedSignature, 50), x2)
    //         mstore(add(decodedSignature, 25), x1)
    //         mstore(decodedSignature, 66)
    //     }

    //     return decodedSignature;
    // }

    // function decode0xAssetData(
    //     uint256[] memory zrxAssetDataIntList
    //     ) internal pure returns (bytes memory decoded0xAssetData)
    // {
    //     // Python has broken thke signature into hex strings,
    //     // then converted them into ints, then rotated them to the right
    //     // to decode, do the opposite

    //     uint8 numOfRotations = 7;
    //     // Rotate to the left
    //     uint256 x1 = bitWiseRotateLeft(zrxAssetDataIntList[0], numOfRotations);
    //     uint256 x2 = bitWiseRotateLeft(zrxAssetDataIntList[1], numOfRotations);

    //     decoded0xAssetData = new bytes(36);
    //     assembly
    //     {
    //         mstore(add(decoded0xAssetData, 36), x2)
    //         mstore(add(decoded0xAssetData, 18), x1)
    //         mstore(decoded0xAssetData, 36)
    //     }

    //     return decoded0xAssetData;
    // }

    function decodeAddress(
        uint256 myAddress
        ) internal pure returns (address decodedAddress)
    {
        // Python has converted myAddress to an int, added an obscure number, and rotated to the right
        // to decode, do the opposite

        // Subtract my random obscure number, no need for safe math here
        myAddress = myAddress - 891754983589020875164519;
        // Rotate to the left
        myAddress = bitWiseRotateLeft(myAddress, 5);

        bytes memory b = new bytes(20);
        assembly
        {
            mstore(add(b, 20), myAddress)
            mstore(b, 20)
        }

        assembly
        {
            decodedAddress := mload(add(b, 20))
        }

        return decodedAddress;
    }

    // function decodeAddressArray(
    //     uint256[] memory encodedAddresses
    //     ) internal pure returns (address[] memory decodedAddressArray)
    // {
    //     // Decode the addresses
    //     decodedAddressArray = new address[](encodedAddresses.length);
    //     for (uint256 i = 0; i < encodedAddresses.length; i++)
    //     {
    //         decodedAddressArray[i] = decodeAddress(encodedAddresses[i]);
    //     }

    //     return decodedAddressArray;
    // }

    // function decodeInt(
    //     uint256 myInt
    //     ) internal pure returns (uint256 decodedNumber)
    // {
    //     // Python code has rotated myInt to the right
    //     // to decode, do the opposite

    //     // Rotate to the left
    //     myInt = bitWiseRotateLeft(myInt, 16);
    //     return myInt;
    // }

    // function decodeIntArray(
    //     uint256[] memory encodedInts
    //     ) internal pure returns (uint256[] memory decodedIntList)
    // {
    //     decodedIntList = new uint256[](encodedInts.length);
    //     for (uint256 i = 0; i < encodedInts.length; i++)
    //     {
    //         decodedIntList[i] = decodeInt(encodedInts[i]);
    //     }

    //     return decodedIntList;
    // }

    function decodeInt_decodeCallDataArrayAndConsiderInjectingInt(
        uint256 myInt
        ) internal pure returns (uint256 decodedNumber)
    {
        // Python code has rotated myInt to the right
        // to decode, do the opposite

        // Rotate to the left
        myInt = bitWiseRotateLeft(myInt, 11);
        return myInt;
    }

    function decodeIntArray_decodeCallDataArrayAndConsiderInjectingInt(
        uint256[] memory encodedInts
        ) internal pure returns (uint256[] memory decodedIntList)
    {
        decodedIntList = new uint256[](encodedInts.length);
        for (uint256 i = 0; i < encodedInts.length; i++)
        {
            decodedIntList[i] = decodeInt_decodeCallDataArrayAndConsiderInjectingInt(encodedInts[i]);
        }

        return decodedIntList;
    }

    function decodeCallDataArrayAndConsiderInjectingInt(
        uint256[] memory callDataArray,
        uint256 quoteTokensToSpend_toInject,
        bool doDecode,
        bool doInjectInt
        ) internal pure returns (bytes memory updatedCallData)
    {
        // callDataArray is made up to two parts
        // the first index in callDataArray is the encoded function selector
        // all the other indexes in callDataArray are the encoded parameters in the calldata

        // Sample input data with the first number as some uint256
        // 0xcb3c28c70000000000000000000000000000000000000000000000000000000001b8141a0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000
        // Sample input data with the first number as FFFF...FFFF showing what to replace with your own uint256
        // 0xcb3c28c7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee00000000000000000000000096aea3a04627f96a038b348b4d34ac24df08820a99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000027f9efbe066d600000000000000000000000000000000000000000000000000000000000000000000

        // The actual input data should be encoded, and this function will decode it

        // // replace the first parameter with the one passed into the function
        // uint256[] memory callDataArray = new uint256[](8);
        // The first parameter is the function selector
        // callDataArray[0] = 11251282108508751410786438484730905108789417455303941525318388289831404398469;
        // // All the other items are the parameters in the calldata
        // callDataArray[1] = 59366061376553713473998796024962062131803458935020796992885968363432024487682;
        // callDataArray[2] = 81020539002477591817371690194067367295966331559958661468723494924294866565208;
        // callDataArray[3] = 100300374173339321621784632522174664355417868639112964214766417266043084725725;
        // callDataArray[4] = 29513413370058131841359401452410130929006865798253763713474781928186275684624;
        // If an int is to be injected into the callData, it will be injected after the function selector
        // So in this example, index 0 is the function selector and index's 1 through 4 are just data.
        // The int would be injected as index 1, and the data that was in index 1 through 4 would move to 2 through 5.

        if (doDecode)
        {
            // Decode the encoded parameters
            callDataArray = decodeIntArray_decodeCallDataArrayAndConsiderInjectingInt(callDataArray);
        }

        uint256 functionSelector = callDataArray[0];
        uint256 callDataArrayLength = callDataArray.length;
        uint8 startIndex;
        uint8 indexAdder_callDataArray;
        if (doInjectInt)
        {
            // Increase the callDataArrayLength by 1 because we're going to inject an int to it
            callDataArrayLength = callDataArrayLength + 1;
            // We are injecting an int, so we need to skip both the function selector and the index of the int we're going to inject later
            // So start at index 1
            startIndex = 2;
            indexAdder_callDataArray = 0;
        }
        else
        {
            // If we're not injecting an int, we only need to skip the function selector
            // So start at index 1
            startIndex = 1;
            indexAdder_callDataArray = 1;
        }

        uint8 functionSelectorLength = 4;
        uint256 updatedCallDataLength = functionSelectorLength + ((callDataArrayLength - 1)* 32);
        uint256 pointer_quoteTokensToSpend_toInject = 32 + functionSelectorLength;

        // Declare the size of our updatedCallData
        updatedCallData = new bytes(updatedCallDataLength);

        // We must use assembly here so it's gas efficient
        assembly
        {
            // Iterate over the array
            // skip the first item since it's the function selector
            // conditionally skip the second item because that's the value we're overwriting anyways (which we'll conditionally do later)
            // for { let i := 2 } lt(i, callDataArrayLength) { i := add(1, i) }
            for { let i := startIndex } lt(i, callDataArrayLength) { i := add(1, i) }
            {
                // TODO figure out what needs to change here with callDataArray's i since I've injected a new varaible that callDataArray doesn't know about

                // TODO the indexes may be fucked up here.  Should the first index ever change??  Should the second index ever change??

                // Write each value to the bytes
                mstore(
                    // position of where we want to write the bytes within updatedCallData
                    add(updatedCallData, add(functionSelectorLength, mul(32, add(0, i)))),
                    // value of what we want to write to updatedCallData
                    // mload(add(callDataArray, mul(32, add(1, i))))
                    mload(add(callDataArray, mul(32, add(indexAdder_callDataArray, i))))
                )
            }

            if eq(doInjectInt, true)
            {
                // Write the value we're overwriting to the bytes
                mstore(add(updatedCallData, pointer_quoteTokensToSpend_toInject), quoteTokensToSpend_toInject)
            }

            // Write the function selector to the bytes
            mstore(add(updatedCallData, functionSelectorLength), functionSelector)

            // Not sure what this does but it's needed to finish up
            mstore(updatedCallData, updatedCallDataLength)
        }

        return updatedCallData;
    }
}

abstract contract KeeperDaoLpp
{
    function borrow(address _token, uint256 _amount, bytes calldata _data) external virtual;
}

abstract contract CHI
{
    function mint(uint256 value) public virtual;
    function free(uint256 value) public virtual returns (uint256);
    // function freeFromUpTo(address from, uint256 value) public virtual returns (uint256);
}

contract Ninja_GasTokens
{
    // This is my local logic contract version of this function.
    function freeGasTokens(
        uint gasTokens
        ) internal returns (uint256 freedAmount)
    {
        // This assumes that this logic contract's proxy is holding gas tokens
        // I'm not wasting gas to check to confirm that I have enough gas tokens
        return CHI(0x0000000000004946c0e9F43F4Dee607b0eF1fA1c).free(gasTokens);
    }
}

contract Ninja_Trade is
    WhitelistedOwners,
    WhitelistedKeeperDAOTransferProxys,
    NinjaProxyOwner,
    SafeMath,
    WETH_Utils,
    Logs,
    Encoding_Utils,
    Ninja_GasTokens
{
    // ***** Begin Trade functions ***** //

    function tradeSimpleRequirements(
        uint256[] calldata quoteTokensToSpendArray,
        uint256 quoteToken_encoded,
        uint256 maxQuoteTokenTradeLossAcceptable,
        uint256[][] calldata checkCallDataArrays_encoded,
        uint256[] calldata tradeCallDataArray_encoded,
        bool doTradeWithinFlashLoan
        ) external onlyWhitelist
    {
        // quoteTokensToSpendArray should have 1 less item than checkCallDataArrays_encoded
        // For a 2 leg trade
        // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade

        // For a 3 leg trade
        // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade
        // quoteTokensToSpendArray[1] = quoteTokensToSpend for second trade

        // For a 4 leg trade
        // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade
        // quoteTokensToSpendArray[1] = quoteTokensToSpend for second trade
        // quoteTokensToSpendArray[2] = quoteTokensToSpend for third trade

        address quoteToken = decodeAddress(quoteToken_encoded);
        // Before trade actions
        uint256[] memory beforeTradeDataArray_ints = getBeforeTradeDataArray_ints(quoteToken);

        // Iterate over the checks and execute them all to enforce that profit is still available before we consider trading
		for (uint8 i = 0; i < checkCallDataArrays_encoded.length; i++)
		{
		    // Set quoteTokensToSpend based on index
		    uint256 quoteTokensToSpend;
		    // If index is the first or the last
		    if (i == 0 || i == checkCallDataArrays_encoded.length - 1)
		    {
		        // Just use the first value in quoteTokensToSpendArray
		        quoteTokensToSpend = quoteTokensToSpendArray[0];
		    }
		    else
		    {
		        // Use the index i
		        quoteTokensToSpend = quoteTokensToSpendArray[i];
		    }

            // Perform the check on the trade to confirm profitability and update our new min percentage
            // Decode my callData
            bool priceVerifiedSuccessfully = performCheck(
                decodeCallDataArrayAndConsiderInjectingInt(
                    checkCallDataArrays_encoded[i], 0, true, false));

            if (!priceVerifiedSuccessfully)
            {
                // Our check failed and this trade is not profitable
                handlePostTradeActions(
                    beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1],
                    quoteToken, maxQuoteTokenTradeLossAcceptable);
                return;
            }
		}

        // If we've made it this far without returning,
        // all of our checks have passed and we're assuming that profit is still available
        if (doTradeWithinFlashLoan)
        {
            // Initiate the flash loan and trade
            initiateTradeWithinFlashLoan(
                quoteTokensToSpendArray[0],
                quoteToken,
                tradeCallDataArray_encoded);
        }
        else
        {
            // Trade using funds in this contract, no flash loan
            initiateTradeWithoutFlashLoan(
                decodeCallDataArrayAndConsiderInjectingInt(
                    tradeCallDataArray_encoded,
                    quoteTokensToSpendArray[0],
                    true,
                    true));
        }

        handlePostTradeActions(
            beforeTradeDataArray_ints[0], beforeTradeDataArray_ints[1],
            quoteToken, maxQuoteTokenTradeLossAcceptable);
    }

    function performCheck(
        bytes memory checkCallData
        ) internal returns (bool priceVerifiedSuccessfully)
    {
        (bool success, bytes memory callResult) = address(this).call{value:0}(checkCallData);
        require(success, "performCheck failed during execution");
        assembly
        {
            priceVerifiedSuccessfully := mload(add(callResult,0x20))
        }
        return priceVerifiedSuccessfully;
    }

    function initiateTradeWithoutFlashLoan(
        bytes memory tradeCallData
        ) internal
    {
        (bool success,) = address(this).call{value:0}(tradeCallData);
        require(success, "initiateTrade failed during execution");
    }

    function initiateTradeWithinFlashLoan(
        uint256 quoteTokensToSpend,
        address quoteToken,
        uint256[] memory tradeCallDataArray_encoded
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // Flash loan from KeeperDAO, and trade via appropriate tradeWithinFlashLoan
        KeeperDaoLpp(ds.keeperDaoLppContract_Eth).borrow(
            quoteToken,
            quoteTokensToSpend,
            // Update the quantity we are trading in the calldata, since that could have changed somewhere along the way
            decodeCallDataArrayAndConsiderInjectingInt(
                tradeCallDataArray_encoded, quoteTokensToSpend, true, true));
    }

    function tradeWithinFlashLoan(
        uint256 borrowedQuoteTokens,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address quoteToken,
        uint256[][] memory exchangeSpecificTradeCallDataArrays,
        bool doWrapEthBeforeFirstTrade,
        bool doUnwrapEthAfterLastTrade
        ) public onlyWhitelistedKeeperDAOTransferProxys
    {
        performTrade(borrowedQuoteTokens, quoteToken, exchangeSpecificTradeCallDataArrays,
            doWrapEthBeforeFirstTrade, doUnwrapEthAfterLastTrade, true);
    }

    function tradeWithoutFlashLoan(
        uint256 quoteTokenQuantity,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address quoteToken,
        uint256[][] memory exchangeSpecificTradeCallDataArrays,
        bool doWrapEthBeforeFirstTrade,
        bool doUnwrapEthAfterLastTrade
        ) public onlyNinjaProxy
    {
        performTrade(quoteTokenQuantity, quoteToken, exchangeSpecificTradeCallDataArrays,
            doWrapEthBeforeFirstTrade, doUnwrapEthAfterLastTrade, false);
    }

    function performTrade(
        uint256 quoteTokenQuantity,
        address quoteToken,
        uint256[][] memory exchangeSpecificTradeCallDataArrays,
        bool doWrapEthBeforeFirstTrade,
        bool doUnwrapEthAfterLastTrade,
        bool doTradeWithinFlashLoan
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 quoteTokenBalance_beforeTrading = getThisContractsTokenBalance(quoteToken);

        // Do I need to wrap ETH before the trade?
        if (doWrapEthBeforeFirstTrade)
        {
            // Assume that if doWrapEthBeforeFirstTrade is set that quoteToken is ETH
            // wrap ETH that we're trading
            wrapEther(quoteTokenQuantity);
        }

        // ****** Begin trading logic ****** //
        uint256 tokensToSpend;
        uint256 tokensReceived;
        // Iterate over the trades and execute them all
        for (uint8 i = 0; i < exchangeSpecificTradeCallDataArrays.length; i++)
		{
            if (i == 0)
            {
                // Spend the tokens we borrowed
                tokensToSpend = quoteTokenQuantity;
            }
            else
            {
                // Spend the tokens we received from the previous trade
                tokensToSpend = tokensReceived;
            }

            // Decode my callData and update the quoteTokensToSpend in the callData
            bytes memory tradeCallData = decodeCallDataArrayAndConsiderInjectingInt(
                exchangeSpecificTradeCallDataArrays[i], tokensToSpend, false, true);

            (bool success, bytes memory callResult) = address(this).call{value:0}(tradeCallData);
            require(success, "performTrade failed during execution");
            assembly
            {
                tokensReceived := mload(add(callResult,0x20))
            }
        }
        // ****** End trading logic ****** //

        // Do I need to wrap ETH after the trade?
        if (doUnwrapEthAfterLastTrade)
        {
            // Assume that if doUnwrapEthAfterLastTrade is set that tokensReceived is WETH
            // Unwrap the WETH I received from the trade
            unwrapEther(tokensReceived);
        }

        if (doTradeWithinFlashLoan)
        {
            returnBorrowedQuoteTokensAndShareProfit(
                ds, quoteTokenQuantity, quoteToken, quoteTokenBalance_beforeTrading);
        }
    }

    // ***** End Trade functions ***** //

    // ***** Begin KeeperDAO functions ***** //

    function returnBorrowedQuoteTokensAndShareProfit(
        DiamondStorage_Properties storage ds,
        uint256 borrowedQuoteTokens,
        address quoteToken,
        uint256 quoteTokenBalance_beforeTrading
        ) internal
    {
        uint256 quoteTokenBalance_afterTrading = getThisContractsTokenBalance(quoteToken);

        // Calculate the sharableProfitQuantity
        uint256 profitToShareWithKeeperDAO = 0;
        // Calculate profit made in quoteTokens
        if (quoteTokenBalance_afterTrading > quoteTokenBalance_beforeTrading)
        {
            // We've made profit in quoteTokens
            // Determine how much profit to share with KeeperDAO in a gas efficient way
            profitToShareWithKeeperDAO = safeSub(quoteTokenBalance_afterTrading, quoteTokenBalance_beforeTrading);
            profitToShareWithKeeperDAO = safeDiv(
                safeMul(profitToShareWithKeeperDAO, 1000000000000000000), ds.divider_profitToShareWithKeeperDAO);
        }
        // Else, we had a loss in quoteTokens.
        // This means one of two things:
        // 1.) The profit was actually made in baseTokens (and that may be left as dust)
        // 2.) The trade was not proftiable!
        //     NOTE: A non-profitable trade will NOT be enforced/reverted here.
        //           It's up to the other trade logic to determine whether or not we should revert
        //           This function will attempt to return the amount borrowed in quoteTokens even if that means this transaction results in a loss
        //           If this function tries to return more quoteTokens than it has,
        //              the tx will revert/fail because that's not possible.
        //           If this function succeeds in returning the borrowed quoteTokens,
        //              it's up to the other trade logic to decide if the trade was profitable or if it needs reverted

        // Return the borrowedQuoteTokens + profitToShareWithKeeperDAO
        sendTokensToKeeperDAO(
            ds,
            safeAdd(borrowedQuoteTokens, profitToShareWithKeeperDAO),
            quoteToken);
    }

    function shareProfitWithKeeperDAO(
        uint256 profitToShare,
        address token
        ) external onlyWhitelist
    {
        uint256 tokenBalance = getThisContractsTokenBalance(token);
        require(tokenBalance >= profitToShare, "shareProfitWithKeeperDAO: Insufficient balance");

        uint256 borrowAmount = 1;
        uint256 returnAmount = safeAdd(profitToShare, borrowAmount);

        // Flash loan from KeeperDAO, then immediately return so that we can share profit
        // To keep the math simple, we're borrowing 1 wei, and then returning profitToShare + 1
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        KeeperDaoLpp(ds.keeperDaoLppContract_Eth).borrow(
            token,
            borrowAmount,
            abi.encodeWithSelector(
                this.shareProfitWithKeeperDAOCallback.selector,
                returnAmount,
                token
            )
        );
    }

    function shareProfitWithKeeperDAOCallback(
        uint256 tokensToReturn,
        address token
        ) external onlyWhitelistedKeeperDAOTransferProxys
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 tokenBalance = getThisContractsTokenBalance(token);
        require(tokenBalance >= tokensToReturn, "shareProfitWithKeeperDAOCallback: Insufficient balance");

        // Return the borrowed + profitToShareWithKeeperDAO
        sendTokensToKeeperDAO(ds, tokensToReturn, token);
    }

    function sendTokensToKeeperDAO(
        DiamondStorage_Properties storage ds,
        uint256 tokensToSend,
        address token
        ) internal
    {
        // If it's ETH
        if (token == 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE)
        {
            // Send ETH
            (bool success, ) = ds.keeperDaoLppContract_Eth.call{value: tokensToSend}("");
            require(success, "Transfer of borrowedQuoteTokens back to LP has failed.");
        }
        // Else assume it's an ERC20 token
        else
        {
            // Send ERC20
            bool success = ERC20(token).transfer(address(ds.keeperDaoLppContract_Eth), tokensToSend);
            require(success, "Transfer of borrowedQuoteTokens back to LP has failed.");
        }
    }

    // ***** End KeeperDAO functions ***** //

    // ***** Begin Utility functions ***** //

    function handlePostTradeActions(
        uint256 quoteTokenBalance_beforeTrading,
        uint256 gasBefore,
        address quoteToken,
        uint256 maxQuoteTokenTradeLossAcceptable
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // The 0x integration here tends to get a lot of dust in base tokens as well as quote tokens
        // So this number needs to either be pretty high (AKA 0.01 quote tokens)
        // or we need to calcualte the dust in both base and quote which is pretty time consuming and costs a lot of gas
        // To avoid timing consuming and expensive calulations, Just hard code in a number here that works most all the time
        // And put trust into the ninja code that's calling this contract
        // uint256 maxTradeLossAcceptable = 10000000000000000;  // I was seeing code 5's due to dust with this amount...
        // uint256 maxTradeLossAcceptable = 30000000000000000;
        // uint256 maxTradeLossAcceptable = 0;

        // If I lost quoteTokens during this charade (considering the max amount we're allowed to lose)
        if (getThisContractsTokenBalance(quoteToken) + maxQuoteTokenTradeLossAcceptable < quoteTokenBalance_beforeTrading)
        {
            // Do not let it trade since it lost ether!
            // I may have tokens left over though so I may actually want to let this trade go through in some cases.
            // Question is, how complicated is that logic and is it worth it?
            // I'll need to check before and after token balances and then value them with respect to ETH
            revert("Code 5");
        }
        // else profit or break even
        else
        {
            // Calculate the profit i'm making
            // uint256 profit_quoteTokens = safeSub(getThisContractsTokenBalance(quoteToken), contractBalanceBefore);

            // Only consider spending gasTokens to save gas if this tx gas price is high enough to make it worth it
            // I may send a trade through at SafeLow and it doesn't make sense to burn gas tokens
            // So check gasPriceThresholdForSpendingGasTokens
            if (tx.gasprice > ds.gasPriceThresholdForSpendingGasTokens)
            {
                // Have to add in the gas cost to perform a transaction call on an empty function
                // For this i'm using 21483
                // uint256 gasUsed = safeSub(safeAdd(21483, gasBefore), gasleft());
                uint256 gasUsed = 21000 + gasBefore - gasleft() + (16 * msg.data.length);
                // logCodeWithValue_ifEnabled(8, gasUsed);
                // Determine how many gas tokens to burn to save gas
                uint256 gasTokensToFree = (gasUsed + 14154) / 41947;
                // logCodeWithValue_ifEnabled(9, gasTokensToFree);
                if (gasTokensToFree > 0)
                {
                    freeGasTokens(gasTokensToFree);
                }
            }
        }
    }

    function getMinQuoteTokensToSpendAllowed(
        uint quoteTokensToSpend
        ) internal view returns (uint minQuoteTokensToSpendAllowed)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        return safeDiv(quoteTokensToSpend, ds.minQuoteTokenAllowedToSpendDivider);
    }

    function getBeforeTradeDataArray_ints(
        address quoteToken
        ) internal view returns (uint256[] memory beforeTradeDataArray_ints)
    {
        beforeTradeDataArray_ints = new uint256[](3);
        beforeTradeDataArray_ints[0] = getThisContractsTokenBalance(quoteToken);  // quoteTokenBalance_beforeTrading
        beforeTradeDataArray_ints[1] = gasleft();  // gasBefore
        return beforeTradeDataArray_ints;
    }

    function getThisContractsTokenBalance(
        address token
        ) internal view returns (uint256 balance)
    {
        // If it's ETH
        if (token == 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE)
        {
            return address(this).balance;
        }
        // Else assume it's an ERC20 token
        else
        {
            return ERC20(token).balanceOf(address(this));
        }
    }

    // ***** End Utility functions ***** //
}
