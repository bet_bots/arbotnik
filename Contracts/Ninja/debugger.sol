pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";


// This was deployed at https://etherscan.io/address/0xb5a944194c4723866027f87D7703A670C104Fa9E
contract Test
{
    function test_uint256(
        uint256 thingy
        ) public
    {
    }

    function test_uint256array(
        uint256[] memory thingy
        ) public
    {
    }

    function test_address(
        address thingy
        ) public
    {
    }

    function test_addressarray(
        address[] memory thingy
        ) public
    {
    }

    function test_bool(
        bool thingy
        ) public
    {
    }

    function test_uint112(
        uint112 thingy
        ) public
    {
    }

    function test_bytes(
        bytes memory thingy
        ) public
    {
    }

    function test_bytes4(
        bytes4 thingy
        ) public
    {
    }

}
