pragma solidity ^0.6.4;

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray contains only all addresses in here in the exact order they are listed below
        // intArray contains only all addresses in here in the exact order they are listed below

        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // Enables/Disables informative event logging. 1 means yes, 0 means no
        uint256 doLogEvents;
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;

        // Whitelists to ensure only ninjaProxyContract can call trade functions
        address ninjaProxyContract;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract NinjaProxyOwner is
    StorageContract_Properties
{
    modifier onlyNinjaProxy()
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        require(msg.sender == ds.ninjaProxyContract, "Must be Ninja");
        _;
    }
}

contract Logs is
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0xv3 order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth filling a dust order
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it
    // 27 = UniswapV2 QuoteTokens balance fell outside requirement
    // 28 = Balancer QuoteTokens balance fell outside requirement
    // 29 = Sushiswap QuoteTokens balance fell outside requirement
    // 30 = UniswapV2 blockTimestampLast failed validation
    // 31 = Sushiswap blockTimestampLast failed validation

    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);

    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }

    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

abstract contract ERC20
{
    // function balanceOf(address tokenOwner) public view virtual returns (uint balance);
    // function transfer(address toAddress, uint tokens) public virtual returns (bool success);
    // function approve(address _spender, uint256 _value) public virtual returns (bool success);
    // function allowance(address _owner, address _spender) public view virtual returns (uint256 remaining);
    // function decimals() public view returns (uint8 decimals);
}

abstract contract KyberReserve
{
    function getConversionRate(
        ERC20 src, ERC20 dest, uint srcQty, uint blockNumber
        ) public view virtual returns(uint);
}

abstract contract KyberProxy
{
    function trade(
        address src, uint srcAmount, address dest, address destAddress,
        uint maxDestAmount, uint minConversionRate, address walletId
        ) public payable virtual returns(uint);
}

contract Kyber_Utils is
    StorageContract_Properties
{
	function tradeTokensOnKyber(
        uint256 msgValue,
        address src,
        uint srcAmount,
        address dest,
        uint maxDestAmount,
        uint minConversionRate,
        address walletId
        ) internal returns (uint)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        return KyberProxy(ds.kyberProxyContract).trade{value: msgValue}(
            src, srcAmount, dest, address(this), maxDestAmount, minConversionRate, walletId);
    }
}

contract Ninja_Exchange_Kyber is
    StorageContract_Properties,
    Logs,
    Kyber_Utils,
    NinjaProxyOwner
{
    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_buy_kyber(
        uint256 etherToSpend,
        address baseTokenAddress,
        address kyberReserve,
        uint256 minKyberRateAcceptable
        ) external returns (bool)
    {
        uint rate = KyberReserve(kyberReserve).getConversionRate(
            ERC20(0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE), ERC20(baseTokenAddress), etherToSpend, block.number);

        if (rate < minKyberRateAcceptable)
        {
            logCodeWithValue_ifEnabled(7, rate);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_sell_kyber(
        address baseTokenAddress,
        address kyberReserve,
        uint256 expectedBaseTokensToTrade,
        uint256 minKyberRateAcceptable
        ) external returns (bool)
    {
        uint rate = KyberReserve(kyberReserve).getConversionRate(
            ERC20(baseTokenAddress), ERC20(0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE), expectedBaseTokensToTrade, block.number);

        if (rate < minKyberRateAcceptable)
        {
            logCodeWithValue_ifEnabled(7, rate);
            return false;
        }

        return true;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_buy_kyber(
        uint256 etherToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address tokenAddress
        ) external onlyNinjaProxy returns (uint256 tokensReceived)
    {
        return tradeTokensOnKyber(
            etherToSpend, 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE, etherToSpend, tokenAddress,
            69203865833239757421118596509098632427930889272824243351707071692229331386368,
            1, 0x0000000000000000000000000000000000000000);
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_sell_kyber(
        uint256 tokensToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        address tokenAddress
        ) external onlyNinjaProxy returns (uint256 etherReceived)
    {
        return tradeTokensOnKyber(
            0, tokenAddress, tokensToSpend, 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE,
            69203865833239757421118596509098632427930889272824243351707071692229331386368,
            1, 0x0000000000000000000000000000000000000000);
    }
}
