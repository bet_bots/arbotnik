pragma solidity ^0.6.4;
pragma experimental "ABIEncoderV2";

contract StorageContract_Properties
{
    struct DiamondStorage_Properties
    {
        // addressArray contains only all addresses in here in the exact order they are listed below
        // intArray contains only all addresses in here in the exact order they are listed below

        address kyberProxyContract;
        address wethContract;
        address zrxV3ExchangeContract;
        address kyberEthTokenContract;
        address keeperDaoEthTokenContract;
        // This must be payable so I can send the borrowed ether back
        address payable keeperDaoLppContract_Eth;

        // Enables/Disables informative event logging. 1 means yes, 0 means no
        uint256 doLogEvents;
        // This number allows me to determine how many gasTokens to use when i'm trading depending on how much gas the tx costs
        // Notorious seems to be using about 36622
        uint256 killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx;// = 36622;
        // 1400000000 wei = 1.4 gwei
        // 2000000000 wei = 2.0 gwei
        // https://gastoken.io/ says GasToken2 has a Required gas price volatility of 2.14x.
        // So that might mean if I mint at 1.0 Gwei, I should only free at 2.14 Gwei or higher.  I should double check that math...
        // Obviously I can try and mint below 1.0 Gwei too, but it'll be hard to get mined in
        uint256 gasPriceThresholdForSpendingGasTokens;// = 2440000000;
        // This is used to help me prevent trading dust. Take the amount I intend to trade, and divide by this number.
        // That's the min I'll accept to trade, else do not trade becuase someone else filled the set/order/etc
        uint256 minQuoteTokenAllowedToSpendDivider;// = 3;
        // To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
        // profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
        // So if you want to share exactly half of the profit you make with KeeperDAO
        // then set divider_profitToShareWithKeeperDAO = 2000000000000000000
        // because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
        uint256 divider_profitToShareWithKeeperDAO;

        // Whitelists to ensure only ninjaProxyContract can call trade functions
        address ninjaProxyContract;
    }

    function diamondStorage_Properties() internal pure returns(DiamondStorage_Properties storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.properties");
        assembly { ds_slot := 0x2d99bdc0377a9272f421949b5c02af29648abf622739472da701fb5afc381ff3 }
    }
}

contract StorageContract_Authentication_KeeperDAOLPPs
{
    struct DiamondStorage_Authentication_KeeperDAOLPPs
    {
        mapping (address => bool) whitelistedKeeperDAOLPPs;
    }

    function diamondStorage_Authentication_KeeperDAOLPPs() internal pure returns(DiamondStorage_Authentication_KeeperDAOLPPs storage ds)
    {
        // NOTE: this ds_slot must be the shared if you want to share storage with another contract under the proxy umbrella
        // NOTE: this ds_slot must be unique if you want to NOT share storage with another contract under the proxy umbrella
        // ds_slot = keccak256("diamond.storage.ninja.authentication.keeperdaolpps");
        assembly { ds_slot := 0x4fc23ab2536f4a0f7b9c2ae26d325fc93a77be5f4c2de98a4ede4c0fd84f96f0 }
    }
}

contract SafeTraders is
    StorageContract_Properties,
    StorageContract_Authentication_KeeperDAOLPPs
{
    modifier onlySafeTraders()
    {
        require(
            msg.sender == diamondStorage_Properties().ninjaProxyContract || diamondStorage_Authentication_KeeperDAOLPPs().whitelistedKeeperDAOLPPs[msg.sender] == true, 
            "Must be Ninja or whitelisted KeeperDAOLP");
        _;
    }
}

contract Logs is
    StorageContract_Properties
{
    // Event_Trade codes:
    // 1 = Trade was profitable and succeeded
    // 2 = Trade was not profitable and was not performed
    // 3 = 0x order was not fillable and trade was not performed
    // 4 = KyberReserve tradeEnabled was false
    // 5 = Passed all requirements, traded, but ended up losing ether! Time to revert!
    // 6 = Bancor BNT balance fell outside requirement
    // 7 = Kyber rate fell outside requirement
    // 8 = Gas used
    // 9 = Num of gas tokens to spend
    // 10 = Uniswap ETH balance fell outside requirement
    // 11 = 0x amount fell outside requirement
    // 12 = 0x order only has dust left, not worth filling
    // 13 = Set rebalance state is not rebalance
    // 14 = Error in set non zero function
    // 15 = etherAmountLeftFillable_Set
    // 16 = effectiveEtherAmountLeftFillable_Set
    // 17 = etherAmountLeftFillable_0x when selling on 0x
    // 18 = etherAmountLeftFillable_0x when buying on 0x
    // 19 = Set rebalance is 100% complete and still in Rebalance mode, no more trades can take place
    // 24 = Failed to met min quote tokens allowed to spend. Not worth filling a dust order
    // 25 = 0x order maker does not have enough balance to fulfill trade
    // 26 = Set doesn't have enough tokens remaining for the trade to be worth it

    event Event_LogCode(uint8 code);
    event Event_LogCodeWithValue(uint8 code, uint256 value);

    function logCode_ifEnabled(
        uint8 code
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCode(code);
        }
    }

    function logCodeWithValue_ifEnabled(
        uint8 code,
        uint256 value
        ) internal
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();
        if (ds.doLogEvents == 1)
        {
            emit Event_LogCodeWithValue(code, value);
        }
    }
}

abstract contract ERC20
{
    function balanceOf(address tokenOwner) public view virtual returns (uint balance);
    // function transfer(address toAddress, uint tokens) public virtual returns (bool success);
    // function approve(address _spender, uint256 _value) public virtual returns (bool success);
    // function allowance(address _owner, address _spender) public view virtual returns (uint256 remaining);
    // function decimals() public view returns (uint8 decimals);
}

contract SafeMath
{
    function safeMul(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        require(c / a == b);
        require(
            c / a == b,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    function safeDiv(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a / b;
        return c;
    }

    // function safeSub(
    //     uint256 a, uint256 b
    //     ) internal pure returns (uint256)
    // {
    //     require(b <= a);
    //     require(
    //         b <= a,
    //         "UINT256_UNDERFLOW"
    //     );
    //     return a - b;
    // }

    function safeAdd(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        uint256 c = a + b;
        require(c >= a);
        require(
            c >= a,
            "UINT256_OVERFLOW"
        );
        return c;
    }

    // function max64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    // function min64(
    //     uint64 a, uint64 b
    //     ) internal pure returns (uint256)
    // {
    //     return a < b ? a : b;
    // }

    // function max256(
    //     uint256 a, uint256 b
    //     ) internal pure returns (uint256)
    // {
    //     return a >= b ? a : b;
    // }

    function min256(
        uint256 a, uint256 b
        ) internal pure returns (uint256)
    {
        return a < b ? a : b;
    }

    function safetifyThisNumber(
        uint256 value,
        uint256 factor
        ) internal pure returns (uint256)
    {
        // Multiply by ~99%, just to be safe so there isn't a round up problem
        // In solididty, it's safer to underfill an order by a hair than to risk overfilling it resulting in an exception
        // To multiply by 99% simply pass in the number 99.
        // It will mulitply by 99 then divide by 100 in order to achieve the desired result without having an exception
        // To multiply by 99.99%, simply pass in the number 9999.
        // It will mulitply by 9999 then divide by 10000 in order to achieve the desired result without having an exception
        value = safeMul(value, factor);
        value = safeDiv(value, safeAdd(factor, 1));
        return value;
    }
}

contract ZRXv3_LibOrder
{
    // A valid order remains fillable until it is expired, fully filled, or cancelled.
    // An order's state is unaffected by external factors, like account balances.
    enum OrderStatus {
        INVALID,                     // Default value
        INVALID_MAKER_ASSET_AMOUNT,  // Order does not have a valid maker asset amount
        INVALID_TAKER_ASSET_AMOUNT,  // Order does not have a valid taker asset amount
        FILLABLE,                    // Order is fillable
        EXPIRED,                     // Order has already expired
        FULLY_FILLED,                // Order is fully filled
        CANCELLED                    // Order has been cancelled
    }

    struct Order
    {
        address makerAddress;           // Address that created the order.
        address takerAddress;           // Address that is allowed to fill the order. If set to 0, any address is allowed to fill the order.
        address feeRecipientAddress;    // Address that will recieve fees when order is filled.
        address senderAddress;          // Address that is allowed to call Exchange contract methods that affect this order. If set to 0, any address is allowed to call these methods.
        uint256 makerAssetAmount;       // Amount of makerAsset being offered by maker. Must be greater than 0.
        uint256 takerAssetAmount;       // Amount of takerAsset being bid on by maker. Must be greater than 0.
        uint256 makerFee;               // Amount of ZRX paid to feeRecipient by maker when order is filled. If set to 0, no transfer of ZRX from maker to feeRecipient will be attempted.
        uint256 takerFee;               // Amount of ZRX paid to feeRecipient by taker when order is filled. If set to 0, no transfer of ZRX from taker to feeRecipient will be attempted.
        uint256 expirationTimeSeconds;  // Timestamp in seconds at which order expires.
        uint256 salt;                   // Arbitrary number to facilitate uniqueness of the order's hash.
        bytes makerAssetData;           // Encoded data that can be decoded by a specified proxy contract when transferring makerAsset. The last byte references the id of this proxy.
        bytes takerAssetData;           // Encoded data that can be decoded by a specified proxy contract when transferring takerAsset. The last byte references the id of this proxy.
        bytes makerFeeAssetData;
        bytes takerFeeAssetData;
    }

    struct OrderInfo
    {
        uint8 orderStatus;                    // Status that describes order's validity and fillability.
        bytes32 orderHash;                    // EIP712 hash of the order (see LibOrder.getOrderHash).
        uint256 orderTakerAssetFilledAmount;  // Amount of order that has already been filled.
    }

    struct FillResults
    {
        uint256 makerAssetFilledAmount;  // Total amount of makerAsset(s) filled.
        uint256 takerAssetFilledAmount;  // Total amount of takerAsset(s) filled.
        uint256 makerFeePaid;
        uint256 takerFeePaid;
        uint256 protocolFeePaid;
    }
}

abstract contract ZRXv3 is
    ZRXv3_LibOrder
{
    function getOrderInfo(Order memory order) public view virtual returns (OrderInfo memory orderInfo);
    function fillOrder(
        Order memory order, uint256 takerAssetFillAmount, bytes memory signature
        ) public payable virtual returns (FillResults memory fillResults);
    // function batchFillOrdersNoThrow(
    //     Order[] memory orders, uint256[] memory takerAssetFillAmounts, bytes[] memory signatures
    //     ) public payable virtual returns (FillResults[] memory fillResults);
}

contract ZRXv3_Utils is
    StorageContract_Properties,
    SafeMath,
    ZRXv3_LibOrder
{
    function calculateProtocolFee(
        uint8 numOfOrdersToFill
        ) internal view returns (uint256)
    {
        return safeMul(safeMul(70000, tx.gasprice), numOfOrdersToFill);
    }

    function tradeTokensOn0xv3(
        Order memory order,
        uint256 takerAssetFillAmount,
        bytes memory signature,
        uint256 protocolFee
        ) internal returns (FillResults memory fillResults)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        // This function assumes the balances are all ready to trade
        // Including wrapped ether, it must be wrapped before calling this function
	    return ZRXv3(ds.zrxV3ExchangeContract).fillOrder{value: protocolFee}(order, takerAssetFillAmount, signature);
    }

    function convertTokensToEther(
        uint256 orderEtherAmount_forPrice, // to calculate price
        uint256 orderTokenAmount_forPrice, // to calculate price
        uint256 tokenAmount_toConvert  // To convert to etherAmount
        ) internal pure returns (uint256)
    {
        // Use this formula
        // price = ether / tokens
        // Rearrange the formula like so
        // ether = tokens * price

        // I cannot use this formula because it will result in the answer being zero.
        // I must multiply BEFORE I divide here since I do not have floats, only int256's.
        // uint256 price = safeDiv(orderEtherAmount_forPrice, orderTokenAmount_forPrice);
        // // We know remaining Token quantity, let's calculate remaining Ether quantity
        // uint256 etherAmount = safeMul(tokenAmount_toConvert, price);
        // So my above price formula becomes
        return safeDiv(safeMul(tokenAmount_toConvert, orderEtherAmount_forPrice), orderTokenAmount_forPrice);
    }

    function convertEtherToTokens(
        uint256 orderEtherAmount_forPrice, // to calculate price
        uint256 orderTokenAmount_forPrice, // to calculate price
        uint256 etherAmount_toConvert  // To convert to tokenAmount
        ) internal pure returns (uint256)
    {
        // Use this formula
        // price = ether / tokens
        // Rearrange the formula like so
        // tokens = ether / price

        // I cannot use this formula because it will result in the answer being zero.
        // I must multiply BEFORE I divide here since I do not have floats, only int256's.
        // uint256 price = safeDiv(orderEtherAmount_forPrice, orderTokenAmount_forPrice);
        // Confirm that 5 / (1 / 3) is the same as 5 * 3 / 1
        // rearrange the above formula to make this, doing the mulitplication before the division
        // etherAmount_toConvert * orderTokenAmount_forPrice / orderEtherAmount_forPrice
        return safeDiv(safeMul(etherAmount_toConvert, orderTokenAmount_forPrice), orderEtherAmount_forPrice);
    }

    // function is0xOrderDust(
    //     uint256 orderInfo_orderTakerAssetFilledAmount,
    //     uint256 order_takerAssetAmount
    //     ) internal pure returns (bool)
    // {
    //     if (safeDiv(safeMul(orderInfo_orderTakerAssetFilledAmount, 102), 100) > order_takerAssetAmount)
    //     {
    //         return true;
    //     }
    //     else
    //     {
    //         return false;
    //     }
    // }
}

contract Ninja_Exchange_0xv3 is
    StorageContract_Properties,
    SafeTraders,
    Logs,
    ZRXv3_Utils
{
    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_buy_0xv3(
        uint256 quoteTokensToSpend,
        address baseToken,
        Order calldata order
        ) external returns (bool)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 minQuoteTokensToSpend = quoteTokensToSpend;

        // Require the order to be fillable
        OrderInfo memory orderInfo = ZRXv3(ds.zrxV3ExchangeContract).getOrderInfo(order);
        if (orderInfo.orderStatus != uint8(OrderStatus.FILLABLE))
        {
            logCode_ifEnabled(3);
            return false;
        }

        // Make sure we don't try to overfill the order
        // Update etherToSpend based on the etherAmountLeftFillable if the etherAmountLeftFillable is less than etherToSpend
        uint256 etherAmountLeftFillable_0x = order.takerAssetAmount - orderInfo.orderTakerAssetFilledAmount;
        logCodeWithValue_ifEnabled(18, etherAmountLeftFillable_0x);
        quoteTokensToSpend = min256(quoteTokensToSpend, etherAmountLeftFillable_0x);

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(24);
            return false;
        }

        // Require the order maker to have enough assets to trade
        uint256 makersMakerTokenBalance = ERC20(baseToken).balanceOf(order.makerAddress);
        uint256 etherToSpendConvertedToTokens = convertEtherToTokens(order.takerAssetAmount, order.makerAssetAmount, quoteTokensToSpend);
        // Convert etherToSpend to tokens so I can make sure makers balance is enough to cover
        if (makersMakerTokenBalance < etherToSpendConvertedToTokens)
        {
            logCode_ifEnabled(25);
            return false;
        }

        return true;
    }

    // Each check function's first parameter must the check's uint256 quantity of tokens we are attempting to spend
    // The parameters that follow can be anything
    // The return value must be the check's uint256 confirmed quantity of tokens we actually can spend
    // If you return zero, that means the check has faied and the trade is not profitable
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function checkSimpleRequirements_sell_0xv3(
        uint256 quoteTokensToSpend,
        address quoteToken,
        Order calldata order
        ) external returns (bool)
    {
        DiamondStorage_Properties storage ds = diamondStorage_Properties();

        uint256 minQuoteTokensToSpend = quoteTokensToSpend;

        // Require the order to be fillable
        OrderInfo memory orderInfo = ZRXv3(ds.zrxV3ExchangeContract).getOrderInfo(order);
        if (orderInfo.orderStatus != uint8(OrderStatus.FILLABLE))
        {
            logCode_ifEnabled(3);
            return false;
        }

        // Make sure we don't try to overfill the order
        // Update etherToSpend based on the tokenAmountLeftFillable_0x if the tokenAmountLeftFillable_0x is less than etherToSpend
        uint256 tokenAmountLeftFillable_0x = order.takerAssetAmount - orderInfo.orderTakerAssetFilledAmount;

        // Enforce tokenAmountLeftFillable_0x is not in ether here, it's actually tokens
        // So I need to convert tokenAmountLeftFillable_0x from tokens to ether so I can lower etherToSpend accordingly so we don't try and overfill the trade.
        uint256 etherAmountLeftFillable_0x = convertTokensToEther(
            order.makerAssetAmount, order.takerAssetAmount, tokenAmountLeftFillable_0x);
        etherAmountLeftFillable_0x = safetifyThisNumber(etherAmountLeftFillable_0x, 9999);
        // logCodeWithValue_ifEnabled(17, etherAmountLeftFillable_0x);
        quoteTokensToSpend = min256(quoteTokensToSpend, etherAmountLeftFillable_0x);

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(24);
            return false;
        }

        // Require the order maker to have enough assets to trade
        quoteTokensToSpend = min256(quoteTokensToSpend, ERC20(quoteToken).balanceOf(order.makerAddress));

        // Enforce no trading dust
        if (quoteTokensToSpend < minQuoteTokensToSpend)
        {
            logCode_ifEnabled(25);
            return false;
        }

        return true;
    }

    // Each trade function's first parameter must the trade's uint256 quantity of tokens we are spending
    // The parameters that follow can be anything
    // The return value must be the trade's uint256 quantity of tokens we are receiving
    // This function's name & selector must be unique so it can coexist with all exchange function selectors within the ninja proxy
    function trade_0xv3(
        uint256 takerTokensToSpend,  // Exclude from the python Ninja operator code since it gets injected by the contact before the call
        Order memory order,
        bytes memory signature
        ) public onlySafeTraders returns (uint256 makerTokensReceived)
    {
        FillResults memory fillResults = tradeTokensOn0xv3(
            order, takerTokensToSpend, signature, calculateProtocolFee(1));

        return fillResults.makerAssetFilledAmount;
    }
}
