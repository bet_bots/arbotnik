import json
import threading
import traceback
from random import randint
import requests
import pandas as pd

import Libraries.core
import Libraries.cache
import Libraries.nodes
import Exchanges.keeperDAO
import Contracts.contracts
from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader, PrintAndLogError
from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings

ContractDict_Exchange = None

df_Exchange = pd.DataFrame(
    columns=['poolAddress', 'tokens', 'symbols', 'decimals', 'rates', 'balances', 'fee', 'amp'])

SwapTupleString_Contents = 'address,uint256,uint256,uint256'
SwapTupleString = '(' + SwapTupleString_Contents + ')'
SwapTupleStringArray = '(' + SwapTupleString_Contents + ')[]'

WhitelistedCurvePools = [
    '0xbebc44782c7db0a1a60cb6fe97d0b483032ff1c7'.lower(),
    '0xa5407eae9ba41422680e2e00537571bcc53efbfd'.lower(),
    '0x93054188d876f558f4a66b2ef1d97d16edf0895b'.lower(),
    '0x7fc77b5c7614e1533320ea6ddc2eb61fa00a9714'.lower(),
    '0x329239599afb305da0a2ec69c58f8a6697f9f88d'.lower(),
    '0x3ba734d5e4e78801ab22cf55c5760e121e1c2c42'.lower(),
    '0x4ca9b3063ec5866a4b82e437059d2c43d1be596f'.lower(),
    '0xdebf20617708857ebe4f679508e7b7863a8a8eee'.lower(),
]


def API_GetRegistryAddress():
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_Curve_AddressProvider.encodeABI('get_registry', kwargs={}),
                "to": Contracts.contracts.Contract_Curve_AddressProvider.address
            },
        ]
    }

    response = Libraries.core.SendRequestToAllNodes(
        payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        registryAddress = '0x' + \
                          jData['result'][-Libraries.core.LengthOfPublicAddress_Excludes0x:]
        # print('curve registry: ', registryAddress)
        return registryAddress


def API_GetAmountOfPools():
    registryAddress = API_GetRegistryAddress()
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_Curve_Registry.encodeABI('pool_count', kwargs={}),
                # "data": Contracts.contracts.Contract_Curve_Registry.encodeABI('pool_list', kwargs={}),
                "to": registryAddress
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(
        payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        poolCount = Libraries.core.ConvertHexToInt(jData['result'])
        # trim jData because raw result comes with many 0s in front
        poolCount = jData['result'][-2:]
        poolCount = int(poolCount, 16)  # result comes as hexstring
        # PrintAndLog_FuncNameHeader("poolCount = " + str(poolCount))
        return poolCount

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader(
            "response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetAllPools_Batched():
    amountOfPools = API_GetAmountOfPools()
    registryAddress = API_GetRegistryAddress()

    payload_total = []
    exchangeIdPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    index = 0
    while amountOfPools > index:
        # Increment the id each time around
        payloadId += 1

        # Pair the ... with the payloadId so I can know which is which in the response
        exchangeIdPayloadIdDict[payloadId] = payloadId

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Curve_Registry.encodeABI('pool_list', kwargs={'arg0': index}),
                    "to": registryAddress
                },
            ]
        }
        payload_total.append(payload)
        index += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(
        payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, False, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in exchangeIdPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    # id = exchangeIdPayloadIdDict[id]
                    result = Libraries.core.GetAddressFromDataProperty(batchedResult['result'])
                    # PrintAndLog_FuncNameHeader("found Id " + str(id) + " in the response. result = " + str(result))
                    resultList.append(result)
                    break

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def curves_GetAddressesFromDataProperty(dataProperties):
    result = dataProperties.replace("0x", "")
    splitArray = Libraries.core.SplitStringIntoChunks(
        result, Libraries.core.LengthOfDataProperty)
    new_addresses = []
    for dataProperty in splitArray:
        if dataProperty == '0000000000000000000000000000000000000000000000000000000000000000':
            continue
        new_addresses.append(
            Libraries.core.GetAddressFromDataProperty(dataProperty))
    return new_addresses


def API_GetTokens_Batched(poolAddressList):
    registryAddress = API_GetRegistryAddress()
    payload_total = []
    exchangeIdPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    for poolAddress in poolAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the ... with the payloadId so I can know which is which in the response
        exchangeIdPayloadIdDict[payloadId] = payloadId
        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Curve_Registry.encodeABI('get_coins', kwargs={'_pool': Libraries.nodes.Instance_Web3.toChecksumAddress(poolAddress)}),
                    "to": registryAddress
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, False, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        index = 0
        for id in exchangeIdPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    results = curves_GetAddressesFromDataProperty(
                        batchedResult['result'])
                    index += 1
                    resultList.append(results)
                    break

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader(
            "response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetDecimals_Batched(poolAddresses):
    amountOfPools = API_GetAmountOfPools()
    registryAddress = API_GetRegistryAddress()
    payload_total = []
    exchangeIdPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    for poolAddress in poolAddresses:
        # Increment the id each time around
        payloadId += 1

        # Pair the ... with the payloadId so I can know which is which in the response
        exchangeIdPayloadIdDict[payloadId] = payloadId
        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Curve_Registry.encodeABI('get_decimals', kwargs={'_pool': Libraries.nodes.Instance_Web3.toChecksumAddress(poolAddress)}),
                    "to": registryAddress
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, False, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in exchangeIdPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    batchedResult['result'] = batchedResult['result'].replace(
                        "0x", "")
                    results = Libraries.core.SplitStringIntoChunks(
                        batchedResult['result'], Libraries.core.LengthOfDataProperty)
                    results_clean = []
                    for result in results:
                        result_clean = Libraries.core.GetIntFromDataProperty_WeiUnits(
                            result)
                        if not result_clean == 0:
                            results_clean.append(result_clean)
                    resultList.append(results_clean)
                    break

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader(
            "response was not ok response = " + str(response))
        response.raise_for_status()


# TODO do not get balances via the registry, just get the token balance of the contract instead
#  I'm leaving this function here commented out as a future reference
# def API_GetBalances_Batched(poolAddressList, blockNumber=None):
#     global df_Exchange
#
#     amountOfPools = API_GetAmountOfPools()
#     registryAddress = API_GetRegistryAddress()
#     payload_total = []
#     exchangeIdPayloadIdDict = {}
#     payloadId = randint(0, 99999999999999)
#
#     for poolAddress in poolAddressList:
#         # Increment the id each time around
#         payloadId += 1
#
#         # Pair the ... with the payloadId so I can know which is which in the response
#         exchangeIdPayloadIdDict[payloadId] = payloadId
#         payload = {
#             "id": payloadId,
#             "jsonrpc": "2.0",
#             "method": "eth_call",
#             "params": [
#                 {
#                     "data": Contracts.contracts.Contract_Curve_Registry.encodeABI(
#                         'get_balances', kwargs={'_pool': Libraries.nodes.Instance_Web3.toChecksumAddress(poolAddress)}),
#                     "to": registryAddress
#                 },
#             ]
#         }
#         payload_total.append(payload)
#
#     # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
#     response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds,
#                                                     None, False, False, True, True, blockNumber)
#     if response.ok:
#         responseData = response.content
#         jData = json.loads(responseData)
#         # PrintAndLog_FuncNameHeader("jData = " + str(jData))
#
#         # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
#         resultList = []
#         for id in exchangeIdPayloadIdDict:
#             for batchedResult in jData:
#                 if int(batchedResult['id']) == id:
#                     batchedResult['result'] = batchedResult['result'].replace("0x", "")
#                     results = Libraries.core.SplitStringIntoChunks(batchedResult['result'], Libraries.core.LengthOfDataProperty)
#                     results_clean = []
#                     for result in results:
#                         result_clean = Libraries.core.GetIntFromDataProperty_WeiUnits(
#                             result)
#                         if not result_clean == 0:
#                             results_clean.append(result_clean)
#                     resultList.append(results_clean)
#                     break
#
#         # if len(resultList) != amountOfPools:
#         #     raise Exception("Length of lists did not match, response data is bad?")
#         return resultList
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
#         response.raise_for_status()


# TODO do not get balances via the registry, just get the token balance of the contract instead
#  I'm leaving this function here commented out as a future reference
# def API_GetAdminBalances_Batched(poolAddressList, blockNumber=None):
#     global df_Exchange
#
#     amountOfPools = API_GetAmountOfPools()
#     registryAddress = API_GetRegistryAddress()
#     payload_total = []
#     exchangeIdPayloadIdDict = {}
#     payloadId = randint(0, 99999999999999)
#
#     for poolAddress in poolAddressList:
#         # Increment the id each time around
#         payloadId += 1
#
#         # Pair the ... with the payloadId so I can know which is which in the response
#         exchangeIdPayloadIdDict[payloadId] = payloadId
#         payload = {
#             "id": payloadId,
#             "jsonrpc": "2.0",
#             "method": "eth_call",
#             "params": [
#                 {
#                     "data": Contracts.contracts.Contract_Curve_Registry.encodeABI(
#                         'get_admin_balances', kwargs={'_pool': Libraries.nodes.Instance_Web3.toChecksumAddress(poolAddress)}),
#                     "to": registryAddress
#                 },
#             ]
#         }
#         payload_total.append(payload)
#
#     # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
#     response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds,
#                                                     None, False, False, True, True, blockNumber)
#     if response.ok:
#         responseData = response.content
#         jData = json.loads(responseData)
#         # PrintAndLog_FuncNameHeader("jData = " + str(jData))
#
#         # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
#         resultList = []
#         for id in exchangeIdPayloadIdDict:
#             for batchedResult in jData:
#                 if int(batchedResult['id']) == id:
#                     batchedResult['result'] = batchedResult['result'].replace("0x", "")
#                     results = Libraries.core.SplitStringIntoChunks(batchedResult['result'], Libraries.core.LengthOfDataProperty)
#                     results_clean = []
#                     for result in results:
#                         result_clean = Libraries.core.GetIntFromDataProperty_WeiUnits(
#                             result)
#                         if not result_clean == 0:
#                             results_clean.append(result_clean)
#                     resultList.append(results_clean)
#                     break
#
#         # if len(resultList) != amountOfPools:
#         #     raise Exception("Length of lists did not match, response data is bad?")
#         return resultList
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
#         response.raise_for_status()


def API_GetFeesOfPools(poolAddresses):
    amountOfPools = API_GetAmountOfPools()
    payload_total = []
    exchangeIdPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    for poolAddress in poolAddresses:
        # Increment the id each time around
        payloadId += 1

        # Pair the ... with the payloadId so I can know which is which in the response
        exchangeIdPayloadIdDict[payloadId] = payloadId
        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Curve_Pool_3Pool.encodeABI('fee', kwargs={}),
                    "to": poolAddress
                },
            ]
        }
        payload_total.append(payload)

    PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, False, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in exchangeIdPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    fee = Libraries.core.GetIntFromDataProperty_EtherUnits(batchedResult['result'], 1)
                    resultList.append(fee)
                    break

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetDy(poolAddress, inputTokenIndex, outputTokenIndex, inputAmount_weiUnits, decimals_outputToken, blockNumber=None):
    kwargs = {
        'i': inputTokenIndex,
        'j': outputTokenIndex,
        'dx': inputAmount_weiUnits,
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_Curve_Pool_3Pool.encodeABI('get_dy', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(poolAddress),
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, False, True, blockNumber)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        outputAmount_weiUnits = Libraries.core.ConvertHexToInt(jData['result'])
        outputAmount_etherUnits = Libraries.core.ConvertWeiToEther(outputAmount_weiUnits, decimals_outputToken)
        return outputAmount_weiUnits, outputAmount_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_SetSymbolsFromTokens(all_tokens):
    global df_Exchange
    index = 0
    for tokens in all_tokens:
        symbols = []
        for token in tokens:
            symbols.append(
                Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token))
        df_Exchange.at[index, 'symbols'] = symbols
        index = index + 1


def API_GetAmp_Batched():
    global df_Exchange

    amountOfPools = API_GetAmountOfPools()
    registryAddress = API_GetRegistryAddress()
    payload_total = []
    exchangeIdPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    for poolAddress in df_Exchange['poolAddress']:
        # Increment the id each time around
        payloadId += 1

        # Pair the ... with the payloadId so I can know which is which in the response
        exchangeIdPayloadIdDict[payloadId] = payloadId
        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Curve_Registry.encodeABI('get_A', kwargs={'_pool': Libraries.nodes.Instance_Web3.toChecksumAddress(poolAddress)}),
                    "to": registryAddress
                },
            ]
        }
        payload_total.append(payload)

        # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(
        payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, False, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        index = 0
        x = 0
        for id in exchangeIdPayloadIdDict:
            x += 1

            for batchedResult in jData:
                index += 1
                # PrintAndLog_FuncNameHeader("batchedResult = " + str(jData))        
                if int(batchedResult['id']) == id:
                    batchedResult['result'] = batchedResult['result'].replace(
                        "0x", "")
                    result = Libraries.core.GetIntFromDataProperty_WeiUnits(batchedResult['result'])
                    resultList.append(result)
                    break

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader(
            "response was not ok response = " + str(response))
        response.raise_for_status()


def TransformAndFormat_DataFrameToDict():
    Exchange_dict = {}
    index = 0
    while index < len(df_Exchange):
        # if you add one here you have to add it as well at API_GetExchangeDataDict()
        dict_entry = Exchange_dict[df_Exchange.at[index, 'poolAddress']] = {}
        dict_entry['exchange'] = df_Exchange.at[index, 'poolAddress'].lower()
        dict_entry['tokens'] = df_Exchange.at[index, 'tokens']
        dict_entry['balances'] = df_Exchange.at[index, 'balances']
        dict_entry['decimals'] = df_Exchange.at[index, 'decimals']
        dict_entry['rates'] = df_Exchange.at[index, 'rates']
        dict_entry['symbols'] = df_Exchange.at[index, 'symbols']
        dict_entry['fee'] = int(df_Exchange.at[index, 'fee'])
        dict_entry['amp'] = int(df_Exchange.at[index, 'amp'])
        index += 1
    return Exchange_dict


def GetExchangeDataFrame():
    global df_Exchange
    return df_Exchange


def ConvertValuesInBalancesListFromWeiToEtherUnits_usingTokenAddresses(balances, tokenAddresses):
    resultMatrix = []
    print(len(balances), ' - ', len(tokenAddresses))
    for balances_row, tokenAddresses_row in zip(balances, tokenAddresses):
        resultList = []
        if len(balances_row) != len(tokenAddresses_row):
            print('LENGHTS ARE NOT THE SAME')
            continue
        print(len(balances_row), ' - ', len(tokenAddresses_row))
        for balance, tokenAddress in zip(balances_row, tokenAddresses_row):
            resultList.append(Libraries.core.ConvertWeiToEther_GivenTokenAddress(balance, tokenAddress))
        resultMatrix.append(resultList)
    print(len(balances), ' - ', len(tokenAddresses), '#2')
    print(resultMatrix)
    return resultMatrix


def API_GetExchangeDataDict():
    global ContractDict_Exchange

    # if you add one here you have to add it as well at TransformAndFormat_DataFrameToDict()
    poolAddressList = API_GetAllPools_Batched()

    PrintAndLog_FuncNameHeader("poolAddressList before ban = " + str(poolAddressList))
    # TODO I'm banning all curve exchanges except the ones I know work with the current implementation of the curve price algorithm
    #  For some reason only certain curve pools work with our price algorithm
    for pool in poolAddressList.copy():
        if pool.lower() not in WhitelistedCurvePools:
            poolAddressList.remove(pool)

    PrintAndLog_FuncNameHeader("poolAddressList after ban = " + str(poolAddressList))
    df_Exchange['poolAddress'] = poolAddressList
    tokenAddresses = API_GetTokens_Batched(poolAddressList)
    df_Exchange['tokens'] = tokenAddresses
    resultList_decimals = API_GetDecimals_Batched(poolAddressList)
    df_Exchange['decimals'] = resultList_decimals
    PrintAndLog_FuncNameHeader("tokenAddresses = " + str(tokenAddresses))
    df_Exchange['rates'] = GenerateRates2dListFromDecimals2dList(poolAddressList, resultList_decimals)
    # resultList_balances = API_GetBalances_Batched(poolAddressList)
    subSetOfCurvePoolBalancesDict = API_GetAllTokenBalancesInPool(poolAddressList, Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int(), tokenAddresses)

    resultList_balances = []
    for poolAddress in poolAddressList:
        resultList_balances.append(subSetOfCurvePoolBalancesDict[poolAddress])
    PrintAndLog_FuncNameHeader("subSetOfCurvePoolBalancesDict = " + str(subSetOfCurvePoolBalancesDict))
    # PrintAndLog_FuncNameHeader("resultList_balances = " + str(resultList_balances))
    df_Exchange['balances'] = resultList_balances
    # Convert the resultList_balances from weiUnits to etherUnits
    PrintAndLog_FuncNameHeader("resultList_balances of len " + str(len(resultList_balances)) + " = " + str(resultList_balances))
    PrintAndLog_FuncNameHeader("tokenAddresses of len " + str(len(tokenAddresses)) + " = " + str(tokenAddresses))
    balances_etherUnits = []
    for index, balanceList in enumerate(resultList_balances):
        tokenList = tokenAddresses[index]
        newList = []
        for index_inner, balance_weiUnits in enumerate(balanceList):
            token = tokenList[index_inner]
            balance_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(balance_weiUnits, token)
            newList.append(balance_etherUnits)

        balances_etherUnits.append(newList)

    PrintAndLog_FuncNameHeader("balances_etherUnits of len " + str(len(balances_etherUnits)) + " = " + str(balances_etherUnits))
    # Set the final variable in df_Exchange
    df_Exchange['balances'] = balances_etherUnits

    df_Exchange['fee'] = API_GetFeesOfPools(poolAddressList)
    API_SetSymbolsFromTokens(df_Exchange['tokens'])
    df_Exchange['amp'] = API_GetAmp_Batched()
    dictionary = TransformAndFormat_DataFrameToDict()

    # # Hardcoded swerve pool
    # dictionary['0x329239599afb305da0a2ec69c58f8a6697f9f88d'.lower()] = {
    #     'exchange': '0x329239599afb305da0a2ec69c58f8a6697f9f88d'.lower(),
    #     'tokens': ['0x6B175474E89094C44Da98b954EedeAC495271d0F'.lower(), '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48'.lower(),
    #                '0xdAC17F958D2ee523a2206206994597C13D831ec7'.lower(), '0x0000000000085d4780B73119b644AE5ecd22b376'.lower()],
    #     'balances': [4787581, 4785014, 4948225, 4820117], 'decimals': [18, 6, 6, 18],
    #     'rates': [1000000000000000000, 1000000000000000000000000000000, 1000000000000000000000000000000, 1000000000000000000],
    #     'symbols': ['DAI', 'USDC', 'USDT', 'TUSD'],
    #     'fee': 3000000, 'amp': 100}

    # Hardcoded banned curve pool
    dictionary['0x3ba734d5e4e78801ab22cf55c5760e121e1c2c42'.lower()] = {
        'exchange': '0x3ba734d5e4e78801ab22cf55c5760e121e1c2c42'.lower(),
        'tokens': ['0x6B175474E89094C44Da98b954EedeAC495271d0F'.lower(), '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48'.lower(),
                   '0xdAC17F958D2ee523a2206206994597C13D831ec7'.lower(), '0x0000000000085d4780B73119b644AE5ecd22b376'.lower(),
                   '0x8e870d67f660d95d5be530380d0ec0bd388289e1'.lower()],
        'balances': [692791, 715880, 787358, 687829, 687343], 'decimals': [18, 6, 6, 18, 18],
        'rates': [1000000000000000000, 1000000000000000000000000000000, 1000000000000000000000000000000, 1000000000000000000, 1000000000000000000],
        'symbols': ['DAI', 'USDC', 'USDT', 'TUSD', 'PAX'],
        'fee': 4000000, 'amp': 200}

    ContractDict_Exchange = dictionary

    return dictionary


def GenerateRates2dListFromDecimals2dList(poolAddressList, decimals2dList):
    # Use this example for reference when converting decimals to rate
    # balances = [69200642588666739304490957, 128768563408428, 141936435754105]  # balances before the tx
    # rates = [1000000000000000000, 1000000000000000000000000000000, 1000000000000000000000000000000]  # hardcoded as constant in SC
    # PrintAndLog_FuncNameHeader("decimals2dList = " + str(decimals2dList))

    rates2dList = []
    for index, decimalsList in enumerate(decimals2dList):
        poolAddress = poolAddressList[index]
        rates = []
        for decimals in decimalsList:
            # Convert decimals to curveDecimals

            # NOTE:  This is critical
            # You must verify that the below formula works for a new curve pool
            # It was a nightmare to discover that curve pools behave differently
            # Here is where I specify which formula works for which pool
            if poolAddress.lower() == '0xa5407eae9ba41422680e2e00537571bcc53efbfd'.lower():
                curveDecimals = 18 + (18 - decimals)
            elif poolAddress.lower() == '0x93054188d876f558f4a66b2ef1d97d16edf0895b'.lower() or \
                    poolAddress.lower() == '0x7fc77b5c7614e1533320ea6ddc2eb61fa00a9714'.lower() or \
                    poolAddress.lower() == '0x329239599afb305da0a2ec69c58f8a6697f9f88d'.lower() or \
                    poolAddress.lower() == '0x3ba734d5e4e78801ab22cf55c5760e121e1c2c42'.lower() or \
                    poolAddress.lower() == '0x4ca9b3063ec5866a4b82e437059d2c43d1be596f'.lower() or \
                    poolAddress.lower() == '0xdebf20617708857ebe4f679508e7b7863a8a8eee'.lower() or \
                    poolAddress.lower() == '0xbebc44782c7db0a1a60cb6fe97d0b483032ff1c7'.lower():
                curveDecimals = 26 - decimals
            else:
                raise Exception("Curve rates formula not yet implemented. "
                                "I either need to reverse engineer it or discover it via machine learning "
                                "because the curve registry gives me the incorrect rates!")

            # Convert curveDecimals to rate
            rate = int("1" + "0" * curveDecimals)
            rates.append(rate)

        rates2dList.append(rates)

    # PrintAndLog_FuncNameHeader("rates = " + str(rates))
    return rates2dList


def ModifyRatesBasedOnSpendAndReceiveTokenOfTrade(spendToken, receiveToken, rates):
    # NOTE: Make sure you're never passing the original rates list into the
    # curve exchange because it will modify it. You must make a copy first
    decimals_spendToken = Libraries.core.GetDecimalsForTokenContract(spendToken)
    decimals_receiveToken = Libraries.core.GetDecimalsForTokenContract(receiveToken)

    diff_decimals = decimals_spendToken - decimals_receiveToken
    # PrintAndLog_FuncNameHeader("diff_decimals = " + str(diff_decimals))
    if diff_decimals > 0:

        newRates = []
        for rate in rates:
            newRates.append(rate * (10 ** diff_decimals))

        # Update the rates
        # PrintAndLog_FuncNameHeader("Updating rates based on diff_decimals. rates before = " + str(rates))
        # PrintAndLog_FuncNameHeader("Updating rates based on diff_decimals. newRates after = " + str(newRates))
        return newRates

    else:
        return rates


def API_GetExchangeEvents():
    from Contracts.contracts import Contract_Curve_Registry
    from Libraries.topics import BuildTopicsArray_Curve_Swap

    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    toBlock_int = latestBlockNumber
    fromBlock_int = latestBlockNumber - 500

    resultsList = Libraries.core.API_GetLogs_Safe(Contract_Curve_Registry.address, BuildTopicsArray_Curve_Swap(), fromBlock_int, toBlock_int)
    # PrintAndLog_FuncNameHeader("found " + str(len(resultsList)) + " pools in Balancer's factory")
    tokenExchanges = []
    for result in resultsList:
        print('result: ', result)
        # tokenExchange = result
        tokenExchange_data = Libraries.core.GetAddressFromDataProperty(result['data'])
        tokenExchange = Libraries.core.GetAddressFromDataProperty(result['topics'][0])
        PrintAndLog_FuncNameHeader("Found tokenExchange " + str(tokenExchange_data))
        tokenExchanges.append(tokenExchange_data)
        # poolTokens = API_GetPoolsTokens(tokenExchange)
        # PrintAndLog_FuncNameHeader("   poolTokens = " + str(poolTokens))

    PrintAndLog_FuncNameHeader("tokenExchanges = " + str(tokenExchanges))
    return tokenExchanges


def GetTokenFlowGivenQuoteAndBase(side, quoteToken, baseToken, poolDataDict):
    # Convert quoteToken and baseToken to spendToken and receiveToken
    spendToken = None
    receiveToken = None
    if Libraries.core.IsBuy(side):
        spendToken = quoteToken
        receiveToken = baseToken
    elif Libraries.core.IsSell(side):
        spendToken = baseToken
        receiveToken = quoteToken
    else:
        raise Exception("not buy or sell?")

    # Determine spendTokenId and receiveTokenId given spendToken and receiveToken
    spendTokenId = poolDataDict['tokens'].index(spendToken.lower())
    receiveTokenId = poolDataDict['tokens'].index(receiveToken.lower())

    return spendToken, receiveToken, spendTokenId, receiveTokenId


def CalculateTradePrice(side, poolAddress, quoteToken, baseToken, exchangeBalances_weiUnits,
                        inputAmount, doConvertInputAmountToWeiUnits=False, rates=None):
    global ContractDict_Exchange

    # NOTE: Make sure you're never passing the original balances or rates list through the below logic
    # Curve protocol/formula requires this to be a copy or else the original list will get modified
    exchangeBalances_weiUnits = exchangeBalances_weiUnits.copy()

    # Note, order of balances must match the order of the tokens in the pool

    poolDataDict = ContractDict_Exchange[poolAddress.lower()]
    tokenCount = len(poolDataDict['tokens'])
    if not rates:
        rates = poolDataDict['rates'].copy()
        # PrintAndLog_FuncNameHeader("Using rates from cache")
    else:
        rates = rates.copy()
        # PrintAndLog_FuncNameHeader("Using passed in hard coded rates")

    amp = poolDataDict['amp']
    fee = poolDataDict['fee']
    spendToken, receiveToken, spendTokenId, receiveTokenId = GetTokenFlowGivenQuoteAndBase(
        side, quoteToken, baseToken, poolDataDict)

    rates = ModifyRatesBasedOnSpendAndReceiveTokenOfTrade(spendToken, receiveToken, rates)

    # Consider converting the inputAmount from etherUnits to weiUnits
    inputAmount_weiUnits = None
    if doConvertInputAmountToWeiUnits:
        inputAmount_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(inputAmount, spendToken)
        # PrintAndLog_FuncNameHeader("converted inputAmount from " + str(inputAmount) + " to inputAmount_weiUnits " +
        #             str(inputAmount_weiUnits) + " because the flag doConvertInputAmountToWeiUnits was True")
    else:
        inputAmount_weiUnits = inputAmount

    # PrintAndLog_FuncNameHeader("poolAddress = " + str(poolAddress))
    # PrintAndLog_FuncNameHeader("amp = " + str(amp))
    # PrintAndLog_FuncNameHeader("fee = " + str(fee))
    # PrintAndLog_FuncNameHeader("exchangeBalances_weiUnits = " + str(exchangeBalances_weiUnits))
    # PrintAndLog_FuncNameHeader("tokenCount = " + str(tokenCount))
    # PrintAndLog_FuncNameHeader("rates = " + str(rates))
    # PrintAndLog_FuncNameHeader("inputAmount_weiUnits = " + str(inputAmount_weiUnits))
    # PrintAndLog_FuncNameHeader("token id path = " + str(spendTokenId) + " " + str(receiveTokenId))
    # You have to copy the exchange balances,
    # because internally the curve_price class is modifying the array pointer you pass in which modifies the data you pass in!
    # This may not be desired, so pass a copy in to be safe
    curve_price = Curve_Price(amp, exchangeBalances_weiUnits, tokenCount, rates.copy(), None, fee)
    outputAmount_weiUnits_equalizedDecimals = curve_price.exchange(spendTokenId, receiveTokenId, inputAmount_weiUnits)
    # PrintAndLog_FuncNameHeader("outputAmount_weiUnits_equalizedDecimals = " + str(outputAmount_weiUnits_equalizedDecimals))

    # Note: outputAmount_weiUnits_equalizedDecimals is not the correct decimals for the outputToken in weiUnits
    # I have to do some ghetto voodoo to convert this to the correct decimals for weiUnits

    decimals_spendToken = Libraries.core.ConvertDecimalsToUsableDecimals(poolDataDict['decimals'][spendTokenId])
    # PrintAndLog_FuncNameHeader("decimals_spendToken = " + str(decimals_spendToken))

    # equalizedDecimals is assumed to be 18
    outputAmount_etherUnits = Libraries.core.ConvertWeiToEther(outputAmount_weiUnits_equalizedDecimals, decimals_spendToken)
    outputAmount_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(outputAmount_etherUnits, receiveToken)
    inputAmount_etherUnits = Libraries.core.ConvertWeiToEther_GivenTokenAddress(inputAmount_weiUnits, spendToken)

    quoteTokenAmount_etherUnits = None
    baseTokenAmount_etherUnits = None
    if Libraries.core.IsBuy(side):
        quoteTokenAmount_etherUnits = inputAmount_etherUnits
        baseTokenAmount_etherUnits = outputAmount_etherUnits
    elif Libraries.core.IsSell(side):
        quoteTokenAmount_etherUnits = outputAmount_etherUnits
        baseTokenAmount_etherUnits = inputAmount_etherUnits
    else:
        raise Exception("not buy or sell?")

    price = quoteTokenAmount_etherUnits / baseTokenAmount_etherUnits

    return price, outputAmount_weiUnits


class Curve_Price:
    """
    Python model of Curve pool math.
    """

    def __init__(self, A, D, n, p=None, tokens=None, _fee=None):
        # NOTE: Make sure you're never passing the original balances list into this class
        # because it will modify it. You must make a copy first
        """
        A: Amplification coefficient
        D: Total deposit size
        n: number of currencies
        p: target prices
        """
        self.A = A  # actually A * n ** (n - 1) because it's an invariant
        self.n = n
        # self.fee = 10 ** 7
        self.fee = _fee
        # PrintAndLog_FuncNameHeader("_fee = " + str(_fee))
        # PrintAndLog_FuncNameHeader("self.fee = " + str(self.fee))

        if p:
            self.p = p
        else:
            self.p = [10 ** 18] * n
        if isinstance(D, list):
            self.x = D
        else:
            self.x = [D // n * 10 ** 18 // _p for _p in self.p]
        self.tokens = tokens

    def xp(self):
        return [x * p // 10 ** 18 for x, p in zip(self.x, self.p)]

    def D(self):
        """
        D invariant calculation in non-overflowing integer operations
        iteratively
        A * sum(x_i) * n**n + D = A * D * n**n + D**(n+1) / (n**n * prod(x_i))
        Converging solution:
        D[j+1] = (A * n**n * sum(x_i) - D[j]**(n+1) / (n**n prod(x_i))) / (A * n**n - 1)
        """
        Dprev = 0
        xp = self.xp()
        S = sum(xp)
        D = S
        Ann = self.A * self.n
        while abs(D - Dprev) > 1:
            D_P = D
            for x in xp:
                D_P = D_P * D // (self.n * x)
            Dprev = D
            D = (Ann * S + D_P * self.n) * D // ((Ann - 1) * D + (self.n + 1) * D_P)

        return D

    def y(self, i, j, x):
        """
        Calculate x[j] if one makes x[i] = x
        Done by solving quadratic equation iteratively.
        x_1**2 + x1 * (sum' - (A*n**n - 1) * D / (A * n**n)) = D ** (n + 1) / (n ** (2 * n) * prod' * A)
        x_1**2 + b*x_1 = c
        x_1 = (x_1**2 + c) / (2*x_1 + b)
        """
        D = self.D()
        xx = self.xp()
        xx[i] = x  # x is quantity of underlying asset brought to 1e18 precision
        xx = [xx[k] for k in range(self.n) if k != j]
        Ann = self.A * self.n
        c = D
        for y in xx:
            c = c * D // (y * self.n)
        c = c * D // (self.n * Ann)
        b = sum(xx) + D // Ann - D
        y_prev = 0
        y = D
        while abs(y - y_prev) > 1:
            y_prev = y
            y = (y ** 2 + c) // (2 * y + b)
        return y  # the result is in underlying units too

    def dy(self, i, j, dx):
        # dx and dy are in underlying units
        xp = self.xp()
        return xp[j] - self.y(i, j, xp[i] + dx)

    def exchange(self, i, j, dx):
        xp = self.xp()
        x = xp[i] + dx
        y = self.y(i, j, x)
        dy = xp[j] - y
        fee = dy * self.fee // 10 ** 10
        assert dy > 0
        self.x[i] = x * 10 ** 18 // self.p[i]
        self.x[j] = (y + fee) * 10 ** 18 // self.p[j]
        return dy - fee


def GetExchangesTokenBalance(exchange, token_toMatch):
    global ContractDict_Exchange

    key = exchange.lower()
    tokenBalancesList = ContractDict_Exchange[key]['balances']
    tokensList = ContractDict_Exchange[key]['tokens']

    # Find token_toMatch's balance
    indexOfToken = tokensList.index(token_toMatch.lower())
    tokenBalance = tokenBalancesList[indexOfToken]
    return tokenBalance


def GetExchangesTokenList(exchange):
    global ContractDict_Exchange

    key = exchange.lower()
    tokensList = ContractDict_Exchange[key]['tokens']
    return tokensList


def GetIndexOfTokenInExchange(exchange, token):
    tokensList = GetExchangesTokenList(exchange)
    return tokensList.index(token.lower())


def GetExchangesThatHaveTheseTokens(tokensList_toMatch):
    global ContractDict_Exchange

    exchangesList = []
    for exchange in ContractDict_Exchange:
        tokensList = ContractDict_Exchange[exchange]['tokens']
        allTokensToMatchAreFound = True
        for token_toMatch in tokensList_toMatch:
            if token_toMatch.lower() not in tokensList:
                allTokensToMatchAreFound = False
                # We want to match all tokens in tokensList_toMatch so as soon as we don't find one, break from the loop
                break

        # If we've made it this far and allTokensToMatchAreFound is still True, add it to the list
        if allTokensToMatchAreFound:
            exchangesList.append(exchange.lower())

    return exchangesList


def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(quoteTokenList):
    global ContractDict_Exchange

    from Libraries.exchanges import EnforceTokenAddressIsERC20Version, ExchangeName_Curve

    # The purpose of this function is to get a list of tokens on exchange
    # that have an exchange and that have some kind of balance on them that's tradeable.
    # So not dust and greater than zero.

    # Start by getting all the possible tokens
    allTokenList = []
    for exchange in ContractDict_Exchange:
        tokensList = ContractDict_Exchange[exchange]['tokens']

        for token in tokensList:
            if token.lower() not in allTokenList:
                allTokenList.append(token.lower())

    # PrintAndLog_FuncNameHeader("quoteTokenList has " + str(len(quoteTokenList)) + " items. quoteTokenList = " + str(quoteTokenList))
    # PrintAndLog_FuncNameHeader("allTokenList has " + str(len(allTokenList)) + " items. allTokenList = " + str(allTokenList))

    tokenListToReturn = []
    # Iterate over all the quoteTokens in quoteTokenList
    for quoteToken in quoteTokenList:
        quoteToken_toUseForThisExchange = EnforceTokenAddressIsERC20Version(quoteToken)

        for baseToken in allTokenList:
            # Skip tokens already added to the list
            if baseToken.lower() in tokenListToReturn:
                continue

            # cannot trade the same token with itself
            if baseToken.lower() == quoteToken_toUseForThisExchange.lower():
                continue

            # Find at least one exchange whose quoteTokenBalance exceeds the quoteTokenBalanceThreshold
            # If we find that, add the baseToken to tokenListToReturn
            exchangesList = GetExchangesThatHaveTheseTokens([quoteToken_toUseForThisExchange, baseToken])
            for exchange in exchangesList:
                tokenListToReturn.append(baseToken.lower())
                # TODO Implement the below, but first the balances in the curve cache need to be etherUnits instead of weiUnits
                #  For now just assume all the curve pools have enough balance

                # quoteTokenBalance = GetExchangesTokenBalance(exchange, quoteToken_toUseForThisExchange)
                #
                # # Require that the quoteTokenBalance exceeds the threshold so we know this token is worth trading
                # if Exchanges.keeperDAO.DoesThisQuoteTokenHaveEnoughBalanceWorthTradingFor(quoteToken, float(quoteTokenBalance), ExchangeName_Curve):
                #     # it's worth trading so add to the list
                #     tokenListToReturn.append(baseToken.lower())
                #     # PrintAndLog_FuncNameHeader("baseToken " + str(
                #     #     baseToken) + " has sufficient balance for quoteToken_toUseForThisExchange " + str(
                #     #     quoteToken_toUseForThisExchange) + " in exchange " + str(exchange))
                #     # Break because we don't need to look at any more exchanges for this baseToken
                #     break
                # else:
                #     pass
                #     # PrintAndLog_FuncNameHeader("baseToken " + str(
                #     #     baseToken) + " does not have sufficient balance for quoteToken_toUseForThisExchange " + str(
                #     #     quoteToken_toUseForThisExchange) + " in exchange " + str(exchange))

    PrintAndLog_FuncNameHeader("Curve: of the " + str(len(allTokenList)) + " tokens on Curve, " + str(
        len(tokenListToReturn)) + " of them have a balance greater than the respective quoteTokenBalanceThreshold")
    return tokenListToReturn


def API_GetAllTokenBalancesInPool(subSetOfCurvePoolsList, blockNumber, correspondingCurvePoolToken2dList=None):
    # It turns out a curve exchange needs all of it's token balances in order to use the price formula
    #  So i'm modifying this in order to facilitate that requirement

    # only do this for a subset of exchanges we care about here that have our quoteToken in it
    # PrintAndLog_FuncNameHeader("subSetOfCurvePoolsList = " + str(subSetOfCurvePoolsList))

    # Get the balances of all tokens within all curve pools
    # Prepare the data we need to call API_GetTokenBalance_Batched_Safe
    walletAddressList = []
    tokenAddressList = []
    for index_exchange, exchange in enumerate(subSetOfCurvePoolsList):
        tokenList_curve = None
        if not correspondingCurvePoolToken2dList:
            # Get the token list from the cache
            tokenList_curve = GetExchangesTokenList(exchange)
        else:
            # Get the token list from the function args
            tokenList_curve = correspondingCurvePoolToken2dList[index_exchange]

        for token in tokenList_curve:
            walletAddressList.append(exchange)
            tokenAddressList.append(token)

    # PrintAndLog_FuncNameHeader("walletAddressList = " + str(walletAddressList))
    # PrintAndLog_FuncNameHeader("tokenAddressList = " + str(tokenAddressList))
    resultList = Libraries.core.API_GetTokenBalance_Batched_Safe(
        walletAddressList, tokenAddressList, None, None, None, blockNumber)
    PrintAndLog_FuncNameHeader("resultList = " + str(resultList))

    if len(resultList) != len(walletAddressList) != len(tokenAddressList):
        raise Exception("resultList did not match the parameters we sent the function. Did the call fail or did I send the wrong data?")

    # Build a dict containing the balances lists above
    # dict keyed by exchange, value is list of balances in weiUnits
    subSetOfCurvePoolBalancesDict = {}
    index = 0
    for index_exchange, exchange in enumerate(subSetOfCurvePoolsList):
        tokenList_curve = None
        if not correspondingCurvePoolToken2dList:
            # Get the token list from the cache
            tokenList_curve = GetExchangesTokenList(exchange)
        else:
            # Get the token list from the function args
            tokenList_curve = correspondingCurvePoolToken2dList[index_exchange]

        subSetOfCurvePoolBalancesDict[exchange] = []
        for token in tokenList_curve:
            balance_etherUnits = resultList[index]
            balance_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(balance_etherUnits, token)
            subSetOfCurvePoolBalancesDict[exchange].append(balance_weiUnits)
            index += 1

    PrintAndLog_FuncNameHeader("subSetOfCurvePoolBalancesDict = " + str(subSetOfCurvePoolBalancesDict))
    return subSetOfCurvePoolBalancesDict
