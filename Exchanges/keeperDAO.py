import copy
import json
import random
import string
import threading
import traceback
import requests

import Libraries.nodes
import Libraries.core
import Libraries.priceOracle
import Libraries.defaults
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError
from Libraries.transactions import API_SendEther_ToContract, TransactionType
from Libraries.utils import RemoveListOfItemsFromList, ConvertListOfStringsToLowercaseListOfStrings, CreateArrayOfTradeAmountValues_UsingCustomQuadraticFormula

KTokenDict = None

EthTokenContract = Libraries.nodes.Instance_Web3.toChecksumAddress('0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE')
Contract_USDC = Libraries.nodes.Instance_Web3.toChecksumAddress("0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48")
Contract_DAI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x6b175474e89094c44da98b954eedeac495271d0f")
Contract_renBTC = Libraries.nodes.Instance_Web3.toChecksumAddress("0xeb4c2781e4eba804ce9a9803c67d0893436bb27d")
Contract_USDT = Libraries.nodes.Instance_Web3.toChecksumAddress("0xdac17f958d2ee523a2206206994597c13d831ec7")
Contract_wBTC = Libraries.nodes.Instance_Web3.toChecksumAddress("0x2260fac5e5542a773aa44fbcfedf7c193bc2c599")
Contract_UNI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x1f9840a85d5af5bf1d1762f925bdaddc4201f984")
Contract_COMP = Libraries.nodes.Instance_Web3.toChecksumAddress("0xc00e94cb662c3520282e6f5717214004a7f26888")
Contract_YFI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x0bc529c00c6401aef6d220be8c6ea1667f6ad93e")
Contract_CRV = Libraries.nodes.Instance_Web3.toChecksumAddress("0xD533a949740bb3306d119CC777fa900bA034cd52")
Contract_SNX = Libraries.nodes.Instance_Web3.toChecksumAddress("0xc011a73ee8576fb46f5e1c5751ca3b9fe0af2a6f")
Contract_BAL = Libraries.nodes.Instance_Web3.toChecksumAddress("0xba100000625a3754423978a60c9317c58a424e3d")
Contract_HAKKA = Libraries.nodes.Instance_Web3.toChecksumAddress("0x0E29e5AbbB5FD88e28b2d355774e73BD47dE3bcd")
Contract_MKR = Libraries.nodes.Instance_Web3.toChecksumAddress("0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2")
Contract_LINK = Libraries.nodes.Instance_Web3.toChecksumAddress("0x514910771af9ca656af840dff83e8264ecf986ca")
Contract_UMA = Libraries.nodes.Instance_Web3.toChecksumAddress("0x04Fa0d235C4abf4BcF4787aF4CF447DE572eF828")
Contract_AAVE = Libraries.nodes.Instance_Web3.toChecksumAddress("0x7fc66500c84a76ad7e9c93437bfc5ac33e2ddae9")
Contract_TUSD = Libraries.nodes.Instance_Web3.toChecksumAddress("0x0000000000085d4780B73119b644AE5ecd22b376")
Contract_PAX = Libraries.nodes.Instance_Web3.toChecksumAddress("0x8e870d67f660d95d5be530380d0ec0bd388289e1")
Contract_BUSD = Libraries.nodes.Instance_Web3.toChecksumAddress("0x4fabb145d64652a948d72533023f6e7a623c7c53")
Contract_cDAI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x5d3a536E4D6DbD6114cc1Ead35777bAB948E3643")
Contract_gUSD = Libraries.nodes.Instance_Web3.toChecksumAddress("0x056fd409e1d7a124bd7017459dfea2f387b6d5cd")


# This is where I enable the Ninja to borrow/trade new quoteTokens
BorrowableQuoteTokensList = []
# Note, the more quoteTokens I add the worse performance gets.
# High performance machines can handle 10-20 quoteTokens.
# Medium to low performance machines should run a max of 4 quoteTokens
# More quoteTokens = more memory usage. 10-20 quoteTokens can use around 12++ GB of memory
BorrowableQuoteTokensList.append(Contract_USDC)
BorrowableQuoteTokensList.append(Contract_DAI)
BorrowableQuoteTokensList.append(Contract_USDT)
BorrowableQuoteTokensList.append(Contract_renBTC)
# ********************************************* #
BorrowableQuoteTokensList.append(Contract_wBTC)
BorrowableQuoteTokensList.append(Contract_LINK)
# BorrowableQuoteTokensList.append(Contract_AAVE)
# BorrowableQuoteTokensList.append(Contract_UNI)
# ********************************************* #
# BorrowableQuoteTokensList.append(Contract_YFI)
# BorrowableQuoteTokensList.append(Contract_COMP)
# BorrowableQuoteTokensList.append(Contract_UMA)
# BorrowableQuoteTokensList.append(Contract_BAL)
# ********************************************* #
# BorrowableQuoteTokensList.append(Contract_HAKKA)
# BorrowableQuoteTokensList.append(Contract_SNX)
# BorrowableQuoteTokensList.append(Contract_CRV)
# ************ PRODUCTION QUOTE TOKENS STOP HERE. Optional quote tokens are below ************ #
# BorrowableQuoteTokensList.append(Contract_cDAI)
# ********************************************* #
# BorrowableQuoteTokensList.append(Contract_TUSD)
# BorrowableQuoteTokensList.append(Contract_BUSD)
# BorrowableQuoteTokensList.append(Contract_MKR)
# BorrowableQuoteTokensList.append(Contract_PAX)
# BorrowableQuoteTokensList.append(Contract_gUSD)

ManuallyAddedTradeAmountArrayDict = {}
# The more values in this list, the worse performance gets but the more precise Ninja is in finding arbitrage
ManuallyAddedTradeAmountArrayDict[EthTokenContract.lower()] = [2000, 400, 90, 20, 10, 4, 2, 1, 0.5, 0.25]  # Use this for Production, performance is fine
# All non-ETH ManuallyAddedTradeAmountArrayDict will be added/updated later based on the ninja price oracle
# I need the amounts in ManuallyAddedTradeAmountArrayDict to correspond together to support 3-leg + trades

# To determine the divider_profitToShareWithKeeperDAO, assume the following formula:
# profitToShareWithKeeperDAO = profit * 1000000000000000000 / divider_profitToShareWithKeeperDAO
# So if you want to share exactly half of the profit you make with KeeperDAO
# then set divider_profitToShareWithKeeperDAO = 2000000000000000000
# because profit * 1000000000000000000 / 2000000000000000000 = half of the profit
# Experiment with how much profit is being shared until I find the right number.
# Remember, the Keeper needs to retain some profit to pay for his gas.
# So not all profit can be shared or else the Keeper will eventually run out of gas (ether) and go broke!
# So to increase profit shared with KeeperDAO, decrease this number
# And to decrease profit shared with KeeperDAO, increase this number
# Divider_profitToShareWithKeeperDAO = 4000000000000000000
# Divider_profitToShareWithKeeperDAO = 8000000000000000000
# Divider_profitToShareWithKeeperDAO = 10000000000000000000
Divider_profitToShareWithKeeperDAO = 50000000000000000000

# This is an approximation of how much overhead gas costs comes from trading through KeeperDAO via flash loan
# It's possible this is ~140k of overhead, but this is hard to measure and I'm seeing weird results
GasCost_EstimatedOverheadGasUsedForTradingViaFlashLoan = 99000


def GetKeeperDAOLPsThatSupportPayableEtherDeposits():
    from Contracts.contracts import Contract_KeeperDAO_LP_Simple
    # These are LPs that support Ether deposits
    # There will probably only ever be one of these, but I made it a list in case there are more added in the future
    return [
        Contract_KeeperDAO_LP_Simple,
    ]


def GetBorrowableQuoteTokensList(doFormatAsAllERC20s=False, doExcludeWETHFromList=False):
    from Exchanges.zrxV2 import Contract_WETH as Contract_WETH_zrxV2
    from Libraries.exchanges import EnforceTokenAddressIsERC20Version

    global BorrowableQuoteTokensList

    # Critical to copy the list here and not modify the original
    returnList = BorrowableQuoteTokensList.copy()

    if not doExcludeWETHFromList:
        returnList.append(Contract_WETH_zrxV2)

    # TODO, doFormatAsAllERC20s flag is useless here because it's already returning WETH not ETH???
    #  try removing this flag and the logic within the flag and see if it still works
    if doFormatAsAllERC20s:
        # Convert all addresses to the ERC20 version, aka don't use 0xeeee...eeeee use WETH's address instead
        new_returnList = []
        for address in returnList:
            new_returnList.append(EnforceTokenAddressIsERC20Version(address))

        returnList = new_returnList

    return returnList


def GetBorrowableAssetsList(doExcludeWETHFromList=True):
    # This is a list of all assets, so ETH + ERC20 tokens
    # This list must be sorted by most important to least important
    # Meaning, if a trading path could be arbed using two different quote tokens, say ETH-USDC for example since both are valid quoteTokens
    # The first one in this list will get selected as the quoteToken in some places of my logic (tailgating for example)
    returnList = []
    returnList += [EthTokenContract]
    returnList += GetBorrowableQuoteTokensList(False, doExcludeWETHFromList)

    returnList = ConvertListOfStringsToLowercaseListOfStrings(returnList)

    numOfProcesses = 0
    numOfProcesses += 1  # Reserve a process for the main process
    numOfProcesses += 1  # Reserve a process for the process manager which is created when you call multiprocessing.Manager()
    numOfProcesses += len(returnList)  # Reserve one process per borrowableAssets aka quoteToken since one sub process will be spawned per quoteToken

    if Libraries.defaults.LimitNumOfProcessesToNumOfCPUCores:
        cpuCount = Libraries.utils.CPUCount
        # if cpuCount is not set, just ignore it
        if cpuCount and numOfProcesses > cpuCount:
            message = "numOfProcesses has exceeded the cpuCount. This is very bad for performance! I recommend removing quoteTokens. " \
                      "numOfProcesses = " + str(numOfProcesses) + ", cpuCount = " + str(cpuCount)
            Libraries.core.API_PostOperatorNotification(message)
            raise Exception(message)

    return returnList


def GetExchangeMinQuoteTokenBalanceRequirementDict():
    # TODO, refactor this so i'm not generating this dict every single time this function is called. This is very inefficient!
    exchangeMinQuoteTokenBalanceRequirementDict = {}
    exchangeMinQuoteTokenBalanceRequirementDict[EthTokenContract.lower()] = Libraries.defaults.ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth
    # PrintAndLog_FuncNameHeader("Populating exchangeMinQuoteTokenBalanceRequirementDict based on price oracle's price conversions "
    #                            "from our hard coded value for ETH's requirement")
    quoteTokenList = GetBorrowableAssetsList()
    for quoteToken in quoteTokenList:
        price_token = Libraries.priceOracle.GetOnChainPrice_FromCache(quoteToken)
        amount_ether = exchangeMinQuoteTokenBalanceRequirementDict[EthTokenContract.lower()]
        amount_equivalentTokens = amount_ether / price_token
        exchangeMinQuoteTokenBalanceRequirementDict[quoteToken.lower()] = amount_equivalentTokens

    # PrintAndLog_FuncNameHeader("exchangeMinQuoteTokenBalanceRequirementDict = " + str(exchangeMinQuoteTokenBalanceRequirementDict))
    return exchangeMinQuoteTokenBalanceRequirementDict


def GetLiquidityProviderPoolContractDict():
    from Contracts.contracts import Contract_KeeperDAO_LP_Simple

    # Someday I may want to have some quoteTokens point to different LP contracts, but for now they all point to the same one
    lppContractDict = {}
    quoteTokenList = GetBorrowableAssetsList()
    for quoteToken in quoteTokenList:
        lppContractDict[quoteToken.lower()] = Contract_KeeperDAO_LP_Simple

    return lppContractDict


def GetSmallestPriceCheckQuoteTokenAmount(quoteToken):
    # TODO, refactor this so i'm not generating this dict every single time this function is called. This is very inefficient!
    smallestPriceCheckQuoteTokenAmountDict = {}
    smallestPriceCheckQuoteTokenAmountDict[EthTokenContract.lower()] = 0.1
    # PrintAndLog_FuncNameHeader("Populating smallestPriceCheckQuoteTokenAmountDict based on price oracle's price conversions "
    #                            "from our hard coded value for ETH's requirement")
    quoteTokenList = GetBorrowableAssetsList()
    for quoteToken in quoteTokenList:
        price_token = Libraries.priceOracle.GetOnChainPrice_FromCache(quoteToken)
        amount_ether = smallestPriceCheckQuoteTokenAmountDict[EthTokenContract.lower()]
        amount_equivalentTokens = amount_ether / price_token
        smallestPriceCheckQuoteTokenAmountDict[quoteToken.lower()] = amount_equivalentTokens

    # PrintAndLog_FuncNameHeader("smallestPriceCheckQuoteTokenAmountDict = " + str(smallestPriceCheckQuoteTokenAmountDict))
    return smallestPriceCheckQuoteTokenAmountDict[quoteToken.lower()]


def GetQuoteTokensToSpendList_etherUnits(quoteToken):
    # Get prices for an array of various sizes of quote tokens
    return GetManuallyAddedTradeAmountArray(quoteToken)


def VerifyValidityOfQuoteTokensToSpendList(quoteTokensToSpendList_etherUnits):
    global ManuallyAddedTradeAmountArrayDict

    # The lengths must be equal
    # Since the EthTokenContract key of ManuallyAddedTradeAmountArrayDict dictates the lengths of all lists
    # we must compare the length of quoteTokensToSpendList_etherUnits to the EthTokenContract key
    if len(quoteTokensToSpendList_etherUnits) == len(ManuallyAddedTradeAmountArrayDict[EthTokenContract.lower()]):
        return True
    else:
        return False


# TODO, make this into a config
def GetNumOfDataValuesForQuoteTokensToSpendList(quoteToken, dramaticallyIncreaseNumOfDataValues):
    # # Set dramaticallyIncreaseNumOfDataValues to True only for exchange protocols that are extremely efficient in getting price data for
    # # For example, Uniswap makes it possible to get all possible price data just by making 2 eth_calls.
    # # But Kyber's protocol is extremely inefficient and that's not the case.
    # # So some exchanges will support a larger NumOfDataValues without taking a performance hit
    # # while some will require a smaller NumOfDataValues so we avoid a performance hit
    # returnValue = None
    #
    # # Consider using debug sizes for testing purposes
    # if UseDebugTradeArraySizes:
    #     return 5
    #
    # localDict = {}
    # localDict[EthTokenContract.lower()] = 0
    # localDict[Contract_USDC.lower()] = 0
    #
    # if quoteToken.lower() not in localDict:
    #     raise Exception("quoteToken " + str(quoteToken) + " was not in localDict. Did I forget to add it?")
    # else:
    #     returnValue = localDict[quoteToken.lower()]
    #
    # if dramaticallyIncreaseNumOfDataValues:
    #     returnValue += 18
    #
    # return returnValue

    # GetManuallyAddedTradeAmountArray has been refactored with ManuallyAddedTradeAmountArrayDict
    # Because I'm trying to support 3-leg trades which require the pricesDict to have exactly the same quantities across all tokens,
    # I'm forced to use the ManuallyAddedTradeAmountArrayDict so I can force all the same quantity values across all tokens
    # NOTE: returning 0 here assumes GetManuallyAddedTradeAmountArray is working properly
    return 0


def GetManuallyAddedTradeAmountArray(quoteToken):
    global ManuallyAddedTradeAmountArrayDict

    # PrintAndLog_FuncNameHeader("quoteToken = " + str(quoteToken))
    # PrintAndLog_FuncNameHeader("ManuallyAddedTradeAmountArrayDict = " + str(ManuallyAddedTradeAmountArrayDict))

    # Make sure all the borrowableTokens except WETH are in the ManuallyAddedTradeAmountArrayDict
    borrowableTokensExceptWETH = GetBorrowableQuoteTokensList(True, True)
    for token in borrowableTokensExceptWETH:
        # If they're not yet in ManuallyAddedTradeAmountArrayDict (it should be soon)
        if token.lower() not in ManuallyAddedTradeAmountArrayDict:
            # Init it so it won't break any existing logic until the ManuallyAddedTradeAmountArrayDict is populated
            # I cannot init this with an empty list because it breaks my logic.  So I have to add at least one value to it so the prices dict function works.
            # Go with the GetSmallestPriceCheckQuoteTokenAmount since it will be so small that no arbitrage should be found using this value
            ManuallyAddedTradeAmountArrayDict[token.lower()] = [GetSmallestPriceCheckQuoteTokenAmount(quoteToken)]
            # ManuallyAddedTradeAmountArrayDict[token.lower()] = []

    # I'm copying ManuallyAddedTradeAmountArrayDict here in case I want to modify it without breaking the original
    localDict = copy.deepcopy(ManuallyAddedTradeAmountArrayDict)

    # Consider using debug sizes for testing purposes
    if Libraries.defaults.UseDebugTradeArraySizes:
        for key in localDict:
            localDict[key] = [0.1]

    # PrintAndLog_FuncNameHeader("localDict[quoteToken.lower()] = " + str(localDict[quoteToken.lower()]))

    if quoteToken.lower() not in localDict:
        raise Exception("quoteToken " + str(quoteToken) + " was not in localDict. Did I forget to add it?")
    else:
        return localDict[quoteToken.lower()]


def GetEligibleExchangeNamesPerQuoteTokenDict():
    from Libraries.exchanges import ExchangeDict

    # NOTE: here is one way I can ban an exchange from a quoteToken if for some reason I want to do that
    # otherwise, just include all exchanges for all quoteTokens
    localDict = {}
    quoteTokenList = GetBorrowableAssetsList()
    for quoteToken in quoteTokenList:
        localDict[quoteToken.lower()] = list(ExchangeDict.keys())

    return localDict


def DoesThisQuoteTokenHaveEnoughBalanceWorthTradingFor(quoteToken, quoteTokenBalance, exchangeName):
    balanceThresholdMultiplier = Libraries.defaults.ExchangeMinQuoteTokenBalanceRequiredToConsiderTradingThisToken_Eth
    return DoesThisQuoteTokenHaveEnoughBalanceWorthTradingFor_internal(quoteToken, quoteTokenBalance, balanceThresholdMultiplier)


def DoesThisQuoteTokenHaveEnoughBalanceWorthTradingFor_internal(quoteToken, quoteTokenBalance, balanceThresholdMultiplier=1.0):
    # Make sure the exchange has assets worth even considering a trade
    # balanceThresholdMultiplier allows me to increase or decrease the requirement based on what exchange i'm trading on or some other factor
    # I found this useful for Balancer because it has so any balancer pools and
    # I don't want to sift through dust all the time, it slows my bot down
    # Use a balanceThresholdMultiplier > 1.0 if you want it to be harder to achieve
    # Use a balanceThresholdMultiplier of < 1.0 if you want it to be easier to achieve

    quoteTokenBalanceThreshold = GetExchangeMinQuoteTokenBalanceRequirementDict()[quoteToken.lower()]
    # Apply the balanceThresholdMultiplier
    quoteTokenBalanceThreshold *= balanceThresholdMultiplier

    # Require that the quoteTokenBalance exceeds the threshold so we know this token is worth trading
    if float(quoteTokenBalance) > quoteTokenBalanceThreshold:
        return True
    else:
        return False


def GetKTokenDict():
    global KTokenDict

    from Contracts.contracts import Contract_KeeperDAO_LP_Simple

    if not KTokenDict:
        KTokenDict = {}

        lpContract = Contract_KeeperDAO_LP_Simple
        KTokenDict[lpContract] = API_GetKToken_Batched(lpContract, GetBorrowableAssetsList())
        PrintAndLog_FuncNameHeader("KTokenDict = " + str(KTokenDict))

    return KTokenDict


def API_PostDiscordNotification(message, doRunInBackgroundThread=True):
    if doRunInBackgroundThread:
        t = threading.Thread(
            name=threading.currentThread().getName(),  # Keep the thread name the same to help with logging consistency
            target=API_PostDiscordNotification_CurrentThread,
            args=(message,))
        t.start()
    else:
        API_PostDiscordNotification_CurrentThread(message)


def API_PostDiscordNotification_CurrentThread(message):
    try:
        PrintAndLog_FuncNameHeader("message = " + message)
        data = {"content": message}
        # url = 'https://maker.ifttt.com/trigger/' + str(operatorNotificationKey.value) + '/with/key/c18NHpBI3NZT5DqfOWkG1efl0QAB3VkYN-Bg9z9K4TM'
        url = 'https://discordapp.com/api/webhooks/707816174582497280/d68I-XhuXZ2PZLvcBLYLo_NVjZz4_fQ_Cciu0So2j3d2H3HSSBPqKn--z_KEW0AhxWza'
        requests.post(url, data=data, timeout=Libraries.core.RequestTimeout_seconds)

    except:
        PrintAndLogError("Exception inside API_PostOperatorNotification: " + str(traceback.format_exc()))
        PrintAndLogError(message)
        pass


def API_DepositToLiquidityProvider(fromAddress, fromPrivateKey, liquidityProviderContractObject,
                                   tokenAddress, amount_weiUnits, gasPrice, nonceToUse=None):
    PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress) + ", tokenAddress = " + str(tokenAddress) + ", amount_weiUnits = " + str(
        amount_weiUnits) + ", liquidityProvider = " + str(liquidityProviderContractObject.address) + ", gasPrice = " + str(gasPrice))

    # {
    #   "constant": false,
    #   "inputs": [
    #     { "internalType": "address", "name": "_token", "type": "address" },
    #     { "internalType": "uint256", "name": "_amount", "type": "uint256" }
    #   ],
    #   "name": "deposit",
    #   "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }],
    #   "payable": true,
    #   "stateMutability": "payable",
    #   "type": "function"
    # },

    kwargs = {
        '_token': Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress),
        '_amount': int(amount_weiUnits),
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = liquidityProviderContractObject.encodeABI('deposit', kwargs=kwargs)
    # PrintAndLog_FuncNameHeader("data_hex: " + str(data_hex))

    toAddress = liquidityProviderContractObject.address

    # Set the msg.value for the call.  If we're depositing ether, value_wei will be non-zero, else it will be zero
    value_wei = 0
    errorMessage = "The liquidityProviderContractObject and tokenAddress must agree that it's either Ether based or Token based. It cannot be both."

    if liquidityProviderContractObject in GetKeeperDAOLPsThatSupportPayableEtherDeposits():
        if tokenAddress.lower() == EthTokenContract.lower():
            value_wei = amount_weiUnits

    else:
        if tokenAddress.lower() == EthTokenContract.lower():
            raise Exception(errorMessage)

    estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex, value_wei)
    # estimatedGas = 300000

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, value_wei,
                                             estimatedGas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "transactionId: " + transactionId
        PrintAndLog_FuncNameHeader(message)
    else:
        PrintAndLog_FuncNameHeader("Failed: no transactionId")

    return transactionId


def API_WithdrawFromLiquidityProvider(fromAddress, fromPrivateKey, liquidityProviderContractObject,
                                      kTokenAddress, kTokenAmount_weiUnits, gasPrice, nonceToUse=None):
    PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress) + ", kTokenAddress = " + str(kTokenAddress) + ", kTokenAmount_weiUnits = " + str(
        kTokenAmount_weiUnits) + ", liquidityProvider = " + str(liquidityProviderContractObject.address) + ", gasPrice = " + str(gasPrice))

    kwargs = {
        '_to': Libraries.nodes.Instance_Web3.toChecksumAddress(fromAddress),
        '_token': Libraries.nodes.Instance_Web3.toChecksumAddress(kTokenAddress),
        '_kTokenAmount': int(kTokenAmount_weiUnits),
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = liquidityProviderContractObject.encodeABI('withdraw', kwargs=kwargs)
    # PrintAndLog_FuncNameHeader("data_hex: " + str(data_hex))

    toAddress = liquidityProviderContractObject.address

    # errorMessage = "The liquidityProviderContractObject and tokenAddress must agree that it's either Ether based or Token based. It cannot be both."
    #
    # if liquidityProviderContractObject in GetKeeperDAOLPsThatSupportPayableEtherDeposits():
    #     if kTokenAddress.lower() != EthTokenContract.lower():
    #         raise Exception(errorMessage)
    #
    # else:
    #     if kTokenAddress.lower() == EthTokenContract.lower():
    #         raise Exception(errorMessage)

    estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    # estimatedGas = 400000
    PrintAndLog_FuncNameHeader("estimatedGas = " + str(estimatedGas))
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                             estimatedGas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "transactionId: " + transactionId
        PrintAndLog_FuncNameHeader(message)
    else:
        PrintAndLog_FuncNameHeader("Failed: no transactionId")

    return transactionId


def API_GetBorrowableBalance(liquidityProviderContractObject, tokenAddress):
    kwargs = {
        '_token': Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress),
    }

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": liquidityProviderContractObject.encodeABI('borrowableBalance', kwargs=kwargs),
                "to": liquidityProviderContractObject.address,
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        decimals_token = Libraries.core.GetDecimalsForTokenContract(tokenAddress, True)
        return Libraries.core.GetIntFromDataProperty_EtherUnits(jData['result'], decimals_token)

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetKToken(liquidityProviderContractObject, tokenAddress):
    kwargs = {
        '_token': Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress),
    }

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": liquidityProviderContractObject.encodeABI('kToken', kwargs=kwargs),
                "to": liquidityProviderContractObject.address,
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))
        kTokenAddress = Libraries.core.GetAddressFromDataProperty(jData['result'])
        return kTokenAddress

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetKToken_Batched(liquidityProviderContractObject, addressList):
    PrintAndLog_FuncNameHeader("addressList = " + str(addressList))

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = random.randint(0, 99999999999999)
    for address in addressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        kwargs = {
            '_token': Libraries.nodes.Instance_Web3.toChecksumAddress(address),
        }

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": liquidityProviderContractObject.encodeABI('kToken', kwargs=kwargs),
                    "to": liquidityProviderContractObject.address,
                },
            ]
        }

        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers,
                                                    Libraries.core.RequestTimeout_veryLong_seconds,
                                                    None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = addressPayloadIdDict[id]
                    if 'result' in batchedResult:
                        try:
                            result = Libraries.core.GetAddressFromDataProperty(batchedResult['result'])
                            resultDict[id] = result
                            break
                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(addressList):
            PrintAndLog_FuncNameHeader("Length of resultDict did not match length of addressList.")

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def UpdateManuallyAddedTradeAmountArrayDict_BasedOnPriceOracle():
    global ManuallyAddedTradeAmountArrayDict

    tokenList = GetBorrowableQuoteTokensList(True, True)
    for token in tokenList:
        price_token = Libraries.priceOracle.GetOnChainPrice_FromCache(token)

        newList = []
        # Populate the list
        for amount_ether in ManuallyAddedTradeAmountArrayDict[EthTokenContract.lower()]:
            amount_equivalentTokens = amount_ether / price_token
            # PrintAndLog_FuncNameHeader(str(amount_ether) + " ETH is equivalent to " + str(amount_equivalentTokens) + " = " + str(token))
            newList.append(amount_equivalentTokens)

        # Update the list
        ManuallyAddedTradeAmountArrayDict[token.lower()] = newList

    PrintAndLog_FuncNameHeader("ManuallyAddedTradeAmountArrayDict = " + str(ManuallyAddedTradeAmountArrayDict))


def ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(token1, token2, quoteTokenWereExpecting):
    if quoteTokenWereExpecting.lower() == token1.lower():
        return token1, token2
    elif quoteTokenWereExpecting.lower() == token2.lower():
        return token2, token1

    # If we haven't returned yet, this set of tokens does not contain the quoteTokenWereExpecting and that's bad
    raise Exception("Could not find the quoteToken based on token1 = " + str(
        token1) + " and token2 = " + str(token2) + ", where quoteTokenWereExpecting = " + str(quoteTokenWereExpecting) + " We must have a bug somewhere")


def EnsureETHIsInTheTokenList(tradeableTokensList):
    # Make sure ETH is added to the list
    if EthTokenContract.lower() not in tradeableTokensList:
        tradeableTokensList.insert(0, EthTokenContract.lower())

    return tradeableTokensList
