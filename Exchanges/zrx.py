import json
from random import randint

import requests

import Libraries.core
import Libraries.nodes
import Libraries.signingUtils
from Libraries.loggingConfig import PrintAndLog

URL_RadarRelay_Base = "https://api.radarrelay.com/0x/v0/"
URL_JoeyZRelay_Base = "http://ec2-35-174-24-226.compute-1.amazonaws.com/"
URL_ErcDex_Base = "https://api.ercdex.com/api/"

# WETH9 contract
Contract_WETH = Libraries.nodes.Instance_Web3.toChecksumAddress("0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2")
# TokenTransferProxy contract
Contract_TokenTransferProxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0x8da0D80f5007ef1e431DD2127178d224E32C2eF4")
# Exchange contract
Contract_Exchange = Libraries.nodes.Instance_Web3.toChecksumAddress("0x12459c951127e0c374ff9105dda097662a027093")
# ZRX Token Address, putting this here because some things rely on it other than just the zrx market
ZRX_TokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress("0xe41d2489571d322189246dafa5ebde1f4699f498")


def GetDataFor_Deposit_Ether():
    return "0xd0e30db0"


def GetDataFor_Withdraw_Ether(amountForWithdrawal_wei):
    data = "0x2e1a7d4d"
    data += Libraries.core.PadDataParemeter_LeftFillPadding("%x" % int(float(amountForWithdrawal_wei)))
    return data


def GetDataFor_Approve(amount):
    global Contract_TokenTransferProxy

    data = "0x095ea7b3"
    # data += "0000000000000000000000008da0d80f5007ef1e431dd2127178d224e32c2ef4"
    data += Libraries.core.PadDataParemeter_LeftFillPadding(Libraries.core.Remove0XfromHexString(Contract_TokenTransferProxy))
    # Max number
    # data += "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
    data += amount
    return data


def IsValidOrder(orderData):
    PrintAndLog("IsValidOrder: orderData = " + str(orderData))
    # Ensure the order is valid
    if "maker" not in orderData or \
            "taker" not in orderData or \
            "makerFee" not in orderData or \
            "takerFee" not in orderData or \
            "makerTokenAmount" not in orderData or \
            "takerTokenAmount" not in orderData or \
            "makerTokenAddress" not in orderData or \
            "takerTokenAddress" not in orderData or \
            "expirationUnixTimestampSec" not in orderData or \
            "feeRecipient" not in orderData or \
            "salt" not in orderData or \
            "exchangeContractAddress" not in orderData:
        return False

    return True


def IsValidOrder_Price(makerTokenAmount, takerTokenAmount, makerTokenAddress, takerTokenAddress,
                       expectedPrice, decimals_makerToken, decimals_takerToken):
    PrintAndLog("makerTokenAmount = " + str(makerTokenAmount))
    PrintAndLog("takerTokenAmount = " + str(takerTokenAmount))
    PrintAndLog("decimals_makerToken = " + str(decimals_makerToken))
    PrintAndLog("decimals_takerToken = " + str(decimals_takerToken))
    # calculate price given orderData
    makerTokenAmount_ether = Libraries.core.ConvertWeiToEther(makerTokenAmount, decimals_makerToken)
    takerTokenAmount_ether = Libraries.core.ConvertWeiToEther(takerTokenAmount, decimals_takerToken)
    PrintAndLog("makerTokenAmount_ether = " + str(makerTokenAmount_ether))
    PrintAndLog("takerTokenAmount_ether = " + str(takerTokenAmount_ether))

    # Price is always Eth / Tokens, so...
    if makerTokenAddress.lower() == Contract_WETH.lower():
        priceFromOrderData = float(makerTokenAmount_ether) / float(takerTokenAmount_ether)
    elif takerTokenAddress.lower() == Contract_WETH.lower():
        priceFromOrderData = float(takerTokenAmount_ether) / float(makerTokenAmount_ether)
    else:
        raise Exception("WETH not an asset")

    PrintAndLog("priceFromOrderData = " + str(priceFromOrderData))
    PrintAndLog("expectedPrice = " + str(expectedPrice))

    if Libraries.core.IsClose(float(expectedPrice), priceFromOrderData):
        return True
    else:
        return False


def GetDecimalsForMakerAndTakerToken_GivenOrderTypeAndMarket(orderType, marketsDecimals):
    if orderType.lower() == "sell":
        decimals_makerToken = marketsDecimals
        decimals_takerToken = Libraries.core.Ether_Decimals
        return decimals_makerToken, decimals_takerToken

    elif orderType.lower() == "buy":
        decimals_makerToken = Libraries.core.Ether_Decimals
        decimals_takerToken = marketsDecimals
        return decimals_makerToken, decimals_takerToken

    else:
        raise Exception("orderType was not a buy or a sell! orderType = " + str(orderType))


# def GetOrderType_GivenMakerTokenAndTakerToken(makerToken, takerToken):
#     global Contract_WETH
#
#     orderType = None
#     if makerToken.lower() == Contract_WETH.lower() and takerToken.lower() != Contract_WETH.lower():
#         return "buy"
#     elif makerToken.lower() != Contract_WETH.lower() and takerToken.lower() == Contract_WETH.lower():
#         return "sell"
#     else:
#         raise Exception("Neither buy nor sell of WETH.  Is this trading token for token not including WETH?")


def Get_TokenPairs_RadarRelay():
    response = requests.get(URL_RadarRelay_Base + "TOKEN_PAIRS", headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog("Get_TokenPairs_RadarRelay responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("Get_TokenPairs_RadarRelay jData = " + str(jData))

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("Get_TokenPairs_RadarRelay response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def Get_NetworkIdForMainNet_ErcDex():
    jData = Get_Networks_ErcDex()
    for network in jData:
        # PrintAndLog("network = " + str(network))
        if network['label'].lower() == "main":
            return network['id']

    else:
        return None


def Get_Networks_ErcDex():
    response = requests.get(URL_ErcDex_Base + "networks", headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("Get_Networks_ErcDex responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("Get_Networks_ErcDex jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("Get_Networks_ErcDex response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def Get_IsMaintenance_ErcDex():
    response = requests.get(URL_ErcDex_Base + "networks/maintenance", headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("Get_IsMaintenance_ErcDex responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("Get_IsMaintenance_ErcDex jData = " + str(jData))

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("Get_IsMaintenance_ErcDex response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def UpdateOrderWithGetFeesResponse(order, jData):
    # Update the order with the new fee data
    order.feeRecipient = jData['feeRecipient']
    order.makerFee = jData['makerFee']
    order.takerFee = jData['takerFee']


def GetFeesFromRelayerAndUpdateOrder_RadarRelay(order):
    # jData = Post_Fees_Generic(URL_RadarRelay_Base, order)
    # This call is Rate limited, so hard code for now
    jData = {'feeRecipient': '0xa258b39954cef5cb142fd567a46cddb31a670124', 'makerFee': '0', 'takerFee': '0'}
    UpdateOrderWithGetFeesResponse(order, jData)
    return jData


def GetFeesFromRelayerAndUpdateOrder_JoeyZRelay(order):
    jData = Post_Fees_Generic(URL_JoeyZRelay_Base, order)
    UpdateOrderWithGetFeesResponse(order, jData)
    return jData


def GetFeesFromRelayerAndUpdateOrder_ErcDex(order):
    # "https://api.ercdex.com/api/standard/{networkId}/v0/fees"
    networkId = Get_NetworkIdForMainNet_ErcDex()
    jData = Post_Fees_Generic(URL_ErcDex_Base + "standard/" + str(networkId) + "/v0/", order)
    UpdateOrderWithGetFeesResponse(order, jData)
    return jData


def GeneratePayloadForGettingOrderFees(order):
    payload = {
        # You must .lower() all of the addresses
        "exchangeContractAddress": str(order.exchangeContractAddress).lower(),
        "maker": str(order.maker).lower(),
        "taker": str(order.taker).lower(),
        "makerTokenAddress": str(order.makerTokenAddress).lower(),
        "takerTokenAddress": str(order.takerTokenAddress).lower(),
        "makerTokenAmount": str(order.makerTokenAmount),
        "takerTokenAmount": str(order.takerTokenAmount),
        "expirationUnixTimestampSec": str(order.expirationUnixTimestampSec),
        "salt": str(order.salt),
    }
    PrintAndLog("GeneratePayloadForGettingOrderFees payload = " + str(payload))
    return payload


def GeneratePayloadForOrder(order):
    payload = {
        # You must .lower() all of the addresses
        "exchangeContractAddress": str(order.exchangeContractAddress).lower(),
        "maker": str(order.maker).lower(),
        "taker": str(order.taker).lower(),
        "makerTokenAddress": str(order.makerTokenAddress).lower(),
        "takerTokenAddress": str(order.takerTokenAddress).lower(),
        "feeRecipient": str(order.feeRecipient).lower(),
        "makerTokenAmount": str(order.makerTokenAmount).lower(),
        "takerTokenAmount": str(order.takerTokenAmount).lower(),
        "makerFee": str(order.makerFee).lower(),
        "takerFee": str(order.takerFee).lower(),
        "expirationUnixTimestampSec": str(order.expirationUnixTimestampSec).lower(),
        "salt": str(order.salt).lower(),
        "ecSignature": {
            "v": int(order.v),
            "r": str(order.r).lower(),
            "s": str(order.s).lower()
        }
    }
    PrintAndLog("GeneratePayloadForOrder payload = " + str(payload))
    return payload


def Post_Fees_Generic(url, order):
    PrintAndLog("Post_Fees_Generic url = " + url)
    # "https://api.ercdex.com/api/standard/1/v0/fees"
    # "https://api.ercdex.com/api/standard/1/v0/fees"
    payload = GeneratePayloadForGettingOrderFees(order)
    response = requests.post(url + "FEES", data=json.dumps(payload), headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    PrintAndLog("Post_Fees_Generic response = " + str(response))
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("Post_Fees_Generic jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("Post_Fees_Generic response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def Post_Order_RadarRelay(order):
    return Post_Order_Generic(URL_RadarRelay_Base, order)


def Post_Order_JoeyZRelay(order):
    return Post_Order_Generic(URL_JoeyZRelay_Base, order)


def Post_Order_ErcDex(order):
    # https://api.ercdex.com/api/standard/{networkId}/v0/order
    networkId = Get_NetworkIdForMainNet_ErcDex()
    return Post_Order_Generic(URL_ErcDex_Base + "standard/" + str(networkId) + "/v0/", order)


def Post_Order_Generic(url, order):
    payload = GeneratePayloadForOrder(order)
    response = requests.post(url + "ORDER", data=json.dumps(payload), headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    PrintAndLog("Post_Order_Generic response = " + str(response))
    if response.ok:
        PrintAndLog("Post_Order_Generic success!")
        return True

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("Post_Order_Generic response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return False


def API_GetEventLogs_0x_Trade_Infura(fromBlock_int, toBlock_int, url_RemoteNode=None):
    from Libraries.nodes import URL_RemoteNode

    if not url_RemoteNode:
        url_RemoteNode = URL_RemoteNode.infura_lowPriority

    fromBlock_hex = "0x" + "%x" % int(fromBlock_int)
    toBlock_hex = "0x" + "%x" % int(toBlock_int)
    PrintAndLog("API_GetEventLogs_0x_Trade_Infura fromBlock_int: " + str(fromBlock_int) + ", toBlock_int: " + str(toBlock_int))

    payload = {
        "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_getLogs",
        "params": [
            {
                "topics": [
                    # Works, Topic0
                    "0x0d0b9391970d9a25552f37d436d2aae2925e2bfe1b2a923754bada030c498cb3"
                    # Topic1 is the makerAddress
                    # Topic2 is the feeRecipient
                    # Topic3 is the tokens // keccak256(makerToken, takerToken), allows subscribing to a token pair
                ],
                "fromBlock": fromBlock_hex,
                "toBlock": toBlock_hex
            }
        ]
    }
    response = requests.post(Libraries.core.Get_RemoteEthereumNodeToUse(url_RemoteNode), data=json.dumps(payload), headers=Libraries.core.Headers,
                             timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetEventLogs_0x_Trade_Infura jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()
