from Libraries.loggingConfig import PrintAndLogError
import Libraries.core

import json
import requests
import traceback

URL_Base = "https://www.bitstamp.net/api/v2/"


def API_GetTicker(symbol):
    # https://www.bitstamp.net/api/v2/ticker/ethusd/
    url = URL_Base + "ticker/" + symbol + "/"
    # PrintAndLog("url = " + url)
    response = requests.get(url, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTicker jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetCurrentPrice(symbol):
    try:
        jData = API_GetTicker(symbol)
        price = (float(jData['bid']) + float(jData['ask'])) / 2
        return price

    except:
        PrintAndLogError("exception in API_GetCurrentPrice: " + traceback.format_exc())
        pass

    return None
