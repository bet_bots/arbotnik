from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader
import Libraries.core
import Libraries.nodes
import Libraries.cache
import Exchanges.zrxV2

import traceback
import requests
import json
from random import randint

# Ideally I should call the proxy contract and get the kyberNetworkContract address and then use only that.
Contract_KyberNetworkProxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0x818e6fecd516ecc3849daf6845e3ec868087b755")
Contract_KyberNetwork = Libraries.nodes.Instance_Web3.toChecksumAddress("0x65bf64ff5f51272f729bdcd7acfb00677ced86cd")
Contract_KyberReserveExample = Libraries.nodes.Instance_Web3.toChecksumAddress("0x63825c174ab367968ec60f061753d3bbd36a0d8f")

EtherToken = Libraries.nodes.Instance_Web3.toChecksumAddress("0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")

# reserve managers, they don't user trade() to set prices, they use setBaseRate or setCompactRate
# TODO, Figure out why my transactions are all failing...  I'm using exactly 50 Gwei so maybe 50 is to high? Maybe I need to use a number less than the gas?
# TODO, and maybe operators are calling the setBaseRate and setCompactRate functions. a few seconds before I'm calling my trade function?
# TODO, and maybe that get price isn't actually returning me accurate data.
# TODO, maybe I could simulate the transaction to see if it would work.

URL_Kyber_Base = "https://tracker.kyber.network/api/"

MaxDestinationAmount = 69203865833239757421118596509098632427930889272824243351707071692229331386368

KyberFee_percentageOfTokensSpent = 0.00201


def API_GetListOfTokenAddresses():
    jData = API_GetTokens()
    returnList = []
    for tokenData in jData:
        returnList.append(tokenData['contractAddress'])

    return returnList


def API_GetTokens():
    url = URL_Kyber_Base + "tokens/supported"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetTokens responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTokens jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetConvertiblePairs response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def IsAddressEther(address):
    if address and address.lower() == EtherToken.lower():
        return True
    else:
        return False


def GetDataFor_Approve(amount):
    global Contract_KyberNetworkProxy

    data = "0x095ea7b3"
    # data += "0000000000000000000000008da0d80f5007ef1e431dd2127178d224e32c2ef4"
    data += Libraries.core.PadDataParemeter_LeftFillPadding(Libraries.core.Remove0XfromHexString(Contract_KyberNetworkProxy))
    # Max number
    # data += "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
    data += amount
    return data


def API_GetMaxGasPriceValue():
    import Contracts.contracts

    kwargs = {}
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberNetwork.encodeABI('maxGasPrice', kwargs=kwargs),
                "to": Contract_KyberNetwork
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetMaxGasPriceValue Kyber responseData = " + str(responseData))
        jData = json.loads(responseData)
        result_hex = jData['result']
        result_wei = Libraries.core.ConvertHexToInt(result_hex)
        result_gwei = Libraries.core.ConvertWeiToGwei(result_wei)
        PrintAndLog("API_GetMaxGasPriceValue Kyber result_gwei = " + str(result_gwei))
        return result_gwei

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetMaxGasPriceValue response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def ConvertMinConversionRateEtherToWei(minConversionRate_etherUnits):
    # Always use Ether's Decimals when converting for minConversionRate
    minConversionRate_weiUnits = Libraries.core.ConvertEtherToWei(minConversionRate_etherUnits, Libraries.core.Ether_Decimals)
    return minConversionRate_weiUnits


# Kyber devs told me to use this one and call it on the proxy
def API_GetExpectedRate(sourceAddress, destinationAddress, sourceQuantity_etherUnits, decimals_sourceQuantity):
    import Contracts.contracts

    sourceQuantity_weiUnits = Libraries.core.ConvertEtherToWei(sourceQuantity_etherUnits, decimals_sourceQuantity)

    kwargs = {
        'src': sourceAddress,
        'dest': destinationAddress,
        'srcQty': sourceQuantity_weiUnits,
    }
    # PrintAndLog("API_GetExpectedRate: kwargs = " + str(kwargs))
    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberNetworkProxy.encodeABI('getExpectedRate', kwargs=kwargs),
                "to": Contract_KyberNetwork
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetExpectedRate responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetExpectedRate jData = " + str(jData))
        result = jData['result']
        result = result.replace("0x", "")
        splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
        # PrintAndLog("splitArray = " + str(splitArray))
        expectedRate_wei = Libraries.core.ConvertHexToInt(splitArray[0])
        slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
        # PrintAndLog("expectedRate_wei = " + str(expectedRate_wei))
        # PrintAndLog("slippageRate_wei = " + str(slippageRate_wei))

        # TODO, which decimals value do I use here???
        expectedRate_etherUnits = Libraries.core.ConvertWeiToEther(expectedRate_wei, Libraries.core.Ether_Decimals)
        slippageRate_etherUnits = Libraries.core.ConvertWeiToEther(slippageRate_wei, Libraries.core.Ether_Decimals)
        # PrintAndLog("expectedRate_etherUnits = " + str(expectedRate_etherUnits))
        # PrintAndLog("slippageRate_etherUnits = " + str(slippageRate_etherUnits))

        slippagePrice = ConvertRateToPrice_GivenAddresses(sourceAddress, destinationAddress, slippageRate_etherUnits)
        price = ConvertRateToPrice_GivenAddresses(sourceAddress, destinationAddress, expectedRate_etherUnits)
        PrintAndLog("API_GetExpectedRate: price = " + str(price) + ", slippagePrice = " + str(slippagePrice))
        return price, slippagePrice

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetExpectedRate response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_FindBestRate(sourceAddress, destinationAddress, sourceQuantity_etherUnits, decimals_sourceQuantity):
    import Contracts.contracts

    sourceQuantity_weiUnits = Libraries.core.ConvertEtherToWei(sourceQuantity_etherUnits, decimals_sourceQuantity)

    kwargs = {
        'src': sourceAddress,
        'dest': destinationAddress,
        'srcAmount': sourceQuantity_weiUnits,
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberNetwork.encodeABI('findBestRate', kwargs=kwargs),
                "to": Contract_KyberNetwork
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_FindBestRate responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_FindBestRate jData = " + str(jData))
        result = jData['result']
        result = result.replace("0x", "")
        splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
        # PrintAndLog("splitArray = " + str(splitArray))
        # obsolete = Libraries.core.ConvertHexToInt(splitArray[0])
        expectedRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
        # PrintAndLog("expectedRate_wei = " + str(expectedRate_wei))

        # TODO, which decimals value do I use here???
        expectedRate_etherUnits = Libraries.core.ConvertWeiToEther(expectedRate_wei, Libraries.core.Ether_Decimals)
        # PrintAndLog("expectedRate_etherUnits = " + str(expectedRate_etherUnits))

        price = ConvertRateToPrice_GivenAddresses(sourceAddress, destinationAddress, expectedRate_etherUnits)
        PrintAndLog("API_FindBestRate: price = " + str(price))
        return price

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_FindBestRate response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_SearchBestRate(sourceAddress, destinationAddress, sourceQuantity_etherUnits, decimals_sourceQuantity):
    import Contracts.contracts

    sourceQuantity_weiUnits = Libraries.core.ConvertEtherToWei(sourceQuantity_etherUnits, decimals_sourceQuantity)

    kwargs = {
        'src': sourceAddress,
        'dest': destinationAddress,
        'srcAmount': sourceQuantity_weiUnits,
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberNetwork.encodeABI('searchBestRate', kwargs=kwargs),
                "to": Contract_KyberNetwork
            },
            # "latest"
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_SearchBestRate responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_SearchBestRate jData = " + str(jData))
        result = jData['result']
        result = result.replace("0x", "")
        splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
        # PrintAndLog("splitArray = " + str(splitArray))
        # reserveAddress, uint bestRate
        reserveAddress = "0x" + splitArray[0][-Libraries.core.LengthOfPublicAddress_Excludes0x:]
        expectedRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
        # PrintAndLog("expectedRate_wei = " + str(expectedRate_wei))

        # TODO, which decimals value do I use here???
        expectedRate_etherUnits = Libraries.core.ConvertWeiToEther(expectedRate_wei, Libraries.core.Ether_Decimals)
        # PrintAndLog("expectedRate_etherUnits = " + str(expectedRate_etherUnits))

        price = ConvertRateToPrice_GivenAddresses(sourceAddress, destinationAddress, expectedRate_etherUnits)

        PrintAndLog("API_SearchBestRate: price = " + str(price) + ", reserveAddress = " + str(reserveAddress))
        return price, reserveAddress

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_SearchBestRate response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetConversionRate(kyberReserveAddress, src, dest, srcQty, specifiedBlockNumber_int=None):
    import Contracts.contracts

    kwargs = {
        'src': Libraries.nodes.Instance_Web3.toChecksumAddress(src),
        'dest': Libraries.nodes.Instance_Web3.toChecksumAddress(dest),
        'srcQty': int(srcQty),
        'blockNumber': Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int(),
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberReserve.encodeABI('getConversionRate', kwargs=kwargs),
                "to": kyberReserveAddress,
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds,
                                                    None, True, False, False, True, specifiedBlockNumber_int)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetConversionRate responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetConversionRate jData = " + str(jData))
        result_wei = Libraries.core.ConvertHexToInt(jData['result'])
        # PrintAndLog("API_GetConversionRate result_wei = " + str(result_wei))
        return result_wei

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetConversionRate response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetConversionRate_Batched_GivenOneList(kyberReserveAddressList, src, dest, srcQty_weiUnits):
    srcList = []
    destList = []
    srcQtyList_weiUnits = []
    for address in kyberReserveAddressList:
        srcList.append(src)
        destList.append(dest)
        srcQtyList_weiUnits.append(srcQty_weiUnits)

    resultDict = API_GetConversionRate_Batched_GivenAllLists(kyberReserveAddressList, srcList, destList, srcQtyList_weiUnits)

    # Instead of just returning the resultDict from this function call, let's make the response easier to parse

    simplerResultDict = {}
    for kyberReserve in resultDict:
        try:
            list = resultDict[kyberReserve]
            # PrintAndLog("kyberReserve = " + str(kyberReserve) + ", list = " + str(list))
            if list:
                rate = list[0]
                # PrintAndLog("kyberReserve = " + str(kyberReserve) + ", rate = " + str(rate))
                # We're only expecting one single srcQty_weiUnits in srcQtyList_weiUnits,
                # so let's make things easier on the function caller and have one number as the value instead of a list of one item as the value...
                simplerResultDict[kyberReserve] = rate

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("failed to parse kyber rate: exception = " + traceback.format_exc())
            pass

    return simplerResultDict


def API_GetConversionRate_Batched_GivenAllLists(kyberReserveAddressList, srcList, destList, srcQtyList_weiUnits):
    from Libraries.core import SendRequestToAllNodes, Headers, RequestTimeout_long_seconds
    import Contracts.contracts

    # PrintAndLog("API_GetConversionRate_Batched: kyberReserveAddressList = " + str(kyberReserveAddressList) + ", srcList = " + str(
    #     srcList) + ", destList = " + str(destList) + ", srcQtyList_weiUnits = " + str(srcQtyList_weiUnits))

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    blockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    for index, address in enumerate(kyberReserveAddressList):
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        # PrintAndLog("index = " + str(index))
        # PrintAndLog("srcList[index] = " + str(srcList[index]))
        # PrintAndLog("destList[index] = " + str(destList[index]))
        kwargs = {
            'src': Libraries.nodes.Instance_Web3.toChecksumAddress(srcList[index]),
            'dest': Libraries.nodes.Instance_Web3.toChecksumAddress(destList[index]),
            'srcQty': int(srcQtyList_weiUnits[index]),
            'blockNumber': blockNumber,
        }

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_KyberReserve.encodeABI('getConversionRate', kwargs=kwargs),
                    "to": address
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("API_GetConversionRate_Batched: payload_total = " + str(payload_total))
    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_long_seconds, None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetConversionRate_Batched: jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}

        for id in addressPayloadIdDict:
            address = addressPayloadIdDict[id]
            # Init the result to be a list
            resultDict[address] = []
            # PrintAndLog("resultDict should be initialized to empty list: resultDict = " + str(resultDict))

        for id in addressPayloadIdDict:
            address = addressPayloadIdDict[id]

            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    # if 'result' in batchedResult and batchedResult['result'] != '0x':
                    if 'result' in batchedResult:
                        try:
                            # PrintAndLog("API_GetConversionRate_Batched: batchedResult['result'] = " + str(batchedResult['result']))
                            result = Libraries.core.ConvertHexToInt(batchedResult['result'])
                            # PrintAndLog("API_GetConversionRate_Batched: Setting resultDict keyed by " + str(address) + ": result = " + str(result))

                            resultDict[address].append(result)
                            break

                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        for address in resultDict:
            if len(resultDict[address]) != len(kyberReserveAddressList):
                PrintAndLog("API_GetConversionRate_Batched: Length of resultDict did not match length of kyberReserveAddressList")

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetConversionRate_Batched response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetReservesConversionRateContract(kyberReserveAddress):
    import Contracts.contracts

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberReserve.encodeABI('conversionRatesContract', kwargs={}),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserveAddress),
            },
        ]
    }

    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return Libraries.core.GetAddressFromDataProperty(jData['result'])

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetReservesConversionRateContract_Batched(kyberReserveAddressList, resultsDict=None, key=None):
    import Contracts.contracts
    from Libraries.core import SendRequestToAllNodes, Headers, RequestTimeout_long_seconds

    if key:
        resultsDict[key] = None

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for address in kyberReserveAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the address with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_KyberReserve.encodeABI('conversionRatesContract', kwargs={}),
                    "to": Libraries.nodes.Instance_Web3.toChecksumAddress(address),
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_long_seconds,
                                     None, True, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    # address = addressPayloadIdDict[id]
                    result = batchedResult['result']
                    result_address = Libraries.core.GetAddressFromDataProperty(result)
                    PrintAndLog_FuncNameHeader("found Id " + str(id) + " in the response. result = " + str(result))
                    resultList.append(result_address)
                    break

        if len(resultList) != len(kyberReserveAddressList):
            raise Exception("Length of lists did not match, response data is bad?")

        if key:
            resultsDict[key] = resultList

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetReservesConversionRateUpdateBlock(kyberReserveConversionRatesContract, tokenAddress, specifiedBlockNumber_int=None):
    import Contracts.contracts
    PrintAndLog_FuncNameHeader("kyberReserveConversionRatesContract = " + str(kyberReserveConversionRatesContract))

    kwargs = {
        'token': Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress),
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberReserveConversionRates.encodeABI('getRateUpdateBlock', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserveConversionRatesContract),
            },
        ]
    }

    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds,
                                                    None, False, False, False, True, specifiedBlockNumber_int)
    if response.ok:
        responseData = response.content
        PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return Libraries.core.ConvertHexToInt(jData['result'])

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetTradeEnabled(kyberReserveAddress, specifiedBlockNumber_int=None):
    import Contracts.contracts

    kwargs = {}
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberReserve.encodeABI('tradeEnabled', kwargs=kwargs),
                "to": kyberReserveAddress,
            },
        ]
    }
    # PrintAndLog("payload = " + str(payload))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds,
                                                    None, True, False, False, True, specifiedBlockNumber_int)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetTradeEnabled responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTradeEnabled jData = " + str(jData))
        return Libraries.core.ConvertHexToBool(jData['result'])

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetTradeEnabled response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetTradeEnabled_Batched(kyberReserveAddressList):
    from Libraries.core import SendRequestToAllNodes, Headers, RequestTimeout_long_seconds
    import Contracts.contracts

    # PrintAndLog("API_GetTradeEnabled_Batched: kyberReserveAddressList = " + str(kyberReserveAddressList))

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for index, address in enumerate(kyberReserveAddressList):
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        kwargs = {}

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_KyberReserve.encodeABI('tradeEnabled', kwargs=kwargs),
                    "to": address
                },
            ]
        }
        payload_total.append(payload)

    response = SendRequestToAllNodes(payload_total, Headers, RequestTimeout_long_seconds, None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTradeEnabled_Batched: jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = addressPayloadIdDict[id]
                    # if 'result' in batchedResult and batchedResult['result'] != '0x':
                    if 'result' in batchedResult:
                        try:
                            result = Libraries.core.ConvertHexToBool(batchedResult['result'])
                            # PrintAndLog("Setting resultDict keyed by " + str(id) + ": result = " + str(result))
                            resultDict[id] = result
                            break

                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(kyberReserveAddressList):
            PrintAndLog("API_GetTradeEnabled_Batched: Length of resultDict did not match length of kyberReserveAddressList")

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetTradeEnabled_Batched response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetReserves():
    import Contracts.contracts

    kwargs = {}
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_KyberNetwork.encodeABI('getReserves', kwargs=kwargs),
                "to": Contract_KyberNetwork
            },
        ]
    }
    PrintAndLog("API_GetReserves payload = " + str(payload))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetReserves responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetReserves jData = " + str(jData))
        data = jData['result'].replace("0x", "")
        splitArray = Libraries.core.SplitStringIntoChunks(data, Libraries.core.LengthOfDataProperty)

        # Remove the first two items, we don't need them
        splitArray.pop(0)
        splitArray.pop(0)

        addressList = []
        for item in splitArray:
            # PrintAndLog("item = " + str(item))
            address = Libraries.core.GetAddressFromDataProperty(item)
            addressList.append(address)

        return addressList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetReserves response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def GetTradeableReserves():
    tokenList_kyber = Libraries.cache.GetDataFromCache("Exchanges.kyber.API_GetListOfTokenAddresses")
    kyberReserveList = API_GetReserves()
    PrintAndLog("kyberReserveList = " + str(kyberReserveList))
    PrintAndLog("len(kyberReserveList) " + str(len(kyberReserveList)))
    PrintAndLog("len(tokenList_kyber) " + str(len(tokenList_kyber)))

    tradableKyberReserveDict = {}

    for token in tokenList_kyber:
        # PrintAndLog("token = " + str(token))
        # Don't try getting rate for trading ether for ether
        if token.lower() == EtherToken.lower():
            continue

        # Don't try getting rate for trading ether for weth
        if token.lower() == Exchanges.zrxV2.Contract_WETH.lower():
            continue

        srcQty = 1
        resultDict1 = API_GetConversionRate_Batched_GivenOneList(kyberReserveList, token, EtherToken, srcQty)
        resultDict2 = API_GetConversionRate_Batched_GivenOneList(kyberReserveList, EtherToken, token, srcQty)
        # PrintAndLog("resultDict1 = " + str(resultDict1))
        # PrintAndLog("resultDict2 = " + str(resultDict2))

        # We are only interested in the KyberReserves that have a non-zero rate, because that means they are trading it.
        # If they return a zero rate they aren't trading it

        # See if it's in resultDict1 and valid
        for kyberReserve1 in resultDict1:
            # PrintAndLog("kyberReserve1 = " + str(kyberReserve1))
            rate1 = resultDict1[kyberReserve1]
            # PrintAndLog("rate1 = " + str(rate1))
            if rate1 and rate1 > 0:
                # See if it's in resultDict2 and valid
                if kyberReserve1 in resultDict2:
                    rate2 = resultDict2[kyberReserve1]
                    if rate2 and rate2 > 0:
                        if kyberReserve1 not in tradableKyberReserveDict:
                            # Create the empty list of tokens for this kyberReserve
                            tradableKyberReserveDict[kyberReserve1] = {}
                            tradableKyberReserveDict[kyberReserve1]['tokenList'] = []

                        # Add this token to the list of tokens in the kyberReserve
                        if token not in tradableKyberReserveDict[kyberReserve1]:
                            tradableKyberReserveDict[kyberReserve1]['tokenList'].append(token.lower())

        conversionRateContractList = Exchanges.kyber.API_GetReservesConversionRateContract_Batched(kyberReserveList)
        # Set the reserve's conversionRateContract
        for index, kyberReserve1 in enumerate(kyberReserveList):
            # Only do this for the ones we care about
            if kyberReserve1 in tradableKyberReserveDict:
                tradableKyberReserveDict[kyberReserve1]['conversionRateContract'] = conversionRateContractList[index]

        # tokenAddress = '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599'
        # tradableKyberReserveDict = Libraries.cache.GetDataFromCache("Exchanges.kyber.GetTradeableReserves")
        # for kyberReserve in tradableKyberReserveDict:
        #     kyberReserveConversionRatesContract = tradableKyberReserveDict[kyberReserve]['conversionRateContract']
        #     didSucceed = False
        #     try:
        #         blockNumber = Exchanges.kyber.API_GetReservesConversionRateUpdateBlock(kyberReserveConversionRatesContract, tokenAddress)
        #         didSucceed = True
        #
        #     except:
        #         # try again a second time, just in case the call failed due to some fluke internet reason
        #         time.sleep(0.05)
        #         try:
        #             blockNumber = Exchanges.kyber.API_GetReservesConversionRateUpdateBlock(kyberReserveConversionRatesContract, tokenAddress)
        #             didSucceed = True
        #
        #         except:
        #             PrintAndLogError("kyberReserve " + str(kyberReserve) + " does not seem to support the function API_GetReservesConversionRateUpdateBlock. "
        #                                                                    "We cannot use it for this kyber reserve")
        #
        #     if didSucceed:
        #         symbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)
        #         PrintAndLog("kyberReserve " + str(kyberReserve) + " last updated it's rate for " + str(symbol) + " on blockNumber = " + str(blockNumber))

    # PrintAndLog("tradableKyberReserveDict = " + str(tradableKyberReserveDict))
    return tradableKyberReserveDict


def GetAllKyberReservesTradingThisToken(erc20TokenContractAddress):
    # Get the tradableKyberReserveDict so I can figure out what reserves I need to query
    tradableKyberReserveDict = Libraries.cache.GetDataFromCache("Exchanges.kyber.GetTradeableReserves")
    # Search for erc20TokenContractAddress's occurrence in the tradableKyberReserveDict
    # For each occurrence, make calls to GetManyArbitrages for each kyberReserve that's trading this token
    kyberReserveList = []
    for kyberReserve in tradableKyberReserveDict:
        kyberReserveTokenList = tradableKyberReserveDict[kyberReserve]['tokenList']
        # PrintAndLog("kyberReserve " + str(kyberReserve) + " is trading the following tokens: kyberReserveTokenList = " + str(kyberReserveTokenList))
        if erc20TokenContractAddress.lower() in kyberReserveTokenList:
            kyberReserveList.append(kyberReserve)

    # PrintAndLog("kyberReserveList = " + str(kyberReserveList))

    # remove kyberReserves that are banned!
    bannedKyberReserveList = []
    # # Caused me like 10 reverts in a row and I cannot figure out why.
    # bannedKyberReserveList.append('0x7a3370075a54b187d7bd5dcebf0ff2b5552d4f7d')
    # # Giving me fucked up rates that don't make any sense.
    # bannedKyberReserveList.append('0x5b756435bf2c8895bab3e3898dd7ed2ba073d7b9')
    # PrintAndLog("bannedKyberReserveList = " + str(bannedKyberReserveList))

    for bannedKyberReserve in bannedKyberReserveList:
        if bannedKyberReserve.lower() in kyberReserveList:
            kyberReserveList.remove(bannedKyberReserve.lower())

    # PrintAndLog("kyberReserveList after removing banned kyberReserves = " + str(kyberReserveList))

    PrintAndLog("Found " + str(len(kyberReserveList)) + " KyberReserves that are actively trading token " + str(
        erc20TokenContractAddress) + ": kyberReserveList = " + str(kyberReserveList))

    return kyberReserveList


def ConvertRateToPrice(rate, side):
    # When buying tokens on Kyber with Ether
    if Libraries.core.IsBuy(side):
        # rate is inverted of the price
        return 1 / float(rate)
    # When selling tokens on Kyber with Ether
    elif Libraries.core.IsSell(side):
        # rate is the price
        return rate
    else:
        raise Exception("side must be buy or sell!")


def ConvertPriceToRate(price, side):
    # It's the same function!
    return ConvertRateToPrice(price, side)


def ConvertRateToPrice_GivenAddresses(sourceAddress, destinationAddress, rate_etherUnits):
    if rate_etherUnits == 0:
        return None

    # Sometimes you have to take 1 divided by the rate to get the price.
    if sourceAddress.lower() == EtherToken.lower():
        return 1 / float(rate_etherUnits)
    elif destinationAddress.lower() == EtherToken.lower():
        return float(rate_etherUnits)
    else:
        raise Exception("I haven't yet implemented the ability to handle trading tokens directly for tokens.")


def GetWorstRate(ratesArray):
    # For Kyber
    #    The lower the rate the worst it is.
    #    The higher the rate the better it is

    worstRate = ratesArray[0]
    for rate in ratesArray:
        if rate < worstRate:
            worstRate = rate

    return worstRate


def GetBestRate(ratesArray):
    # For Kyber
    #    The lower the rate the worst it is.
    #    The higher the rate the better it is

    bestRate = ratesArray[0]
    for rate in ratesArray:
        if rate > bestRate:
            bestRate = rate

    return bestRate
