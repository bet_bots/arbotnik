from Libraries.loggingConfig import PrintAndLogError, PrintAndLog
import Libraries.core

import traceback
import requests
import json
import time
import hmac
import hashlib
import base64
import threading
import websocket
from enum import Enum

URL_REST_Ocean_Base = "https://api.theocean.trade/v0/"
URL_WS_Ocean_Base = "wss://ws.theocean.trade"
APIKey = "fa0908a99fa6ee2a9bf8528b30b77dd6"
APISecret = "815141bf547d6389d59f4a5f5679b0f854e22ae6fbdac3214578a080fb1eb888"

# URL_REST_Ocean_Base = "https://api.staging.theocean.trade/api/v0/"
# APIKey = "05337da6b5caebb5761b4d681d537e2a"
# APISecret = "41d366d1e78d9d7c38d186825bce6de76369f5e13c19d2120e1e9d84881425db"

WebsocketClient = None


class FeeOption(Enum):
    feeInZrx = "feeInZrx"
    feeInNative = "feeInNative"


def GetAuthenticatedRequest(URL, method, body):
    timestamp = str(int(round(time.time() * 1000)))
    prehash = APIKey + timestamp + method + json.dumps(body, separators=(',', ':'))
    signature = base64.b64encode(hmac.new(APISecret.encode('utf-8'), msg=prehash.encode('utf-8'), digestmod=hashlib.sha256).digest())

    headers = {
        'TOX-ACCESS-KEY': APIKey,
        'TOX-ACCESS-SIGN': signature,
        'TOX-ACCESS-TIMESTAMP': timestamp
    }
    request = requests.request(method, URL, headers=headers, json=body)

    return request


def API_GetTokenPairs():
    method = "token_pairs"
    response = requests.get(URL_REST_Ocean_Base + method, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetTokenPairs responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetTokenPairs jData = " + str(jData))

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetTokenPairs response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetOrderbook(quoteTokenAddress, baseTokenAddress):
    # GET https://api.staging.theocean.trade/api/v0/order_book
    method = "order_book"
    # quoteTokenAddress
    # baseTokenAddress
    url = URL_REST_Ocean_Base + method + "?quoteTokenAddress=" + str(quoteTokenAddress) + "&baseTokenAddress=" + str(baseTokenAddress)
    PrintAndLog("url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetOrderbook responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetOrderbook jData = " + str(jData))

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetOrderbook response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetUserHistory(onlyOpenOrders=False):
    url = URL_REST_Ocean_Base + "user_history"
    if onlyOpenOrders:
        url += "?openAmount=1"
    response = GetAuthenticatedRequest(url, 'GET', {})
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetUserHistory responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetUserHistory jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetUserHistory response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetOpenOrders():
    orderHistory_jData = API_GetUserHistory(True)
    for order in orderHistory_jData:
        PrintAndLog("Found " + order['side'] + " order at " + order['price'] + " with hash = " + order['orderHash'])

    return orderHistory_jData


def API_CancelAllOpenOrders():
    orderHistory_jData = API_GetOpenOrders()
    for order in orderHistory_jData:
        try:
            API_CancelOrder(order['orderHash'])
        except:
            PrintAndLogError("exception in API_CancelAllOpenOrders: " + traceback.format_exc())
            pass


def FormatAddressCorrectly(address):
    return address.lower()


def API_GetAvailableBalance(walletAddress, tokenAddress):
    walletAddress = FormatAddressCorrectly(walletAddress)
    tokenAddress = FormatAddressCorrectly(tokenAddress)

    method = 'available_balance'
    url = URL_REST_Ocean_Base + method + "?walletAddress=" + str(walletAddress) + "&tokenAddress=" + str(tokenAddress)
    PrintAndLog("url = " + str(url))
    response = GetAuthenticatedRequest(url, 'GET', {})
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetAvailableBalance responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetAvailableBalance jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetAvailableBalance response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetUnsignedOrder(walletAddress, quoteTokenAddress, baseTokenAddress, side, orderAmount_baseToken_wei, price, feeOption=FeeOption.feeInZrx):
    walletAddress = FormatAddressCorrectly(walletAddress)
    quoteTokenAddress = FormatAddressCorrectly(quoteTokenAddress)
    baseTokenAddress = FormatAddressCorrectly(baseTokenAddress)

    url = URL_REST_Ocean_Base + "limit_order/reserve"
    body = {
        'walletAddress': str(walletAddress),
        'baseTokenAddress': str(baseTokenAddress),
        'quoteTokenAddress': str(quoteTokenAddress),
        'side': str(side),
        'orderAmount': str(orderAmount_baseToken_wei),
        'price': str(price),
        'feeOption': str(feeOption.value)
    }
    PrintAndLog("API_GetUnsignedOrder: body = " + str(body))
    response = GetAuthenticatedRequest(url, 'POST', body)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetUnsignedOrder responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetUnsignedOrder jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetUnsignedOrder response was not ok response = " + str(response) + ", for trading pair " + str(
            quoteTokenAddress) + "/" + str(baseTokenAddress) + ": " + str(response.content))
        response.raise_for_status()

    return None


def API_PostSignedOrder_MakerAndOrTaker(orderData_maker, orderData_taker, matchingOrderID):
    url = URL_REST_Ocean_Base + "limit_order/place"

    body_maker = {}
    if orderData_maker:
        body_maker = {
            'signedTargetOrder': {
                'maker': orderData_maker.maker.lower(),
                'taker': orderData_maker.taker.lower(),
                'makerTokenAddress': orderData_maker.makerTokenAddress.lower(),
                'takerTokenAddress': orderData_maker.takerTokenAddress.lower(),
                'makerTokenAmount': orderData_maker.makerTokenAmount,
                'takerTokenAmount': orderData_maker.takerTokenAmount,
                'makerFee': orderData_maker.makerFee,
                'takerFee': orderData_maker.takerFee,
                'expirationUnixTimestampSec': orderData_maker.expirationUnixTimestampSec,
                'feeRecipient': orderData_maker.feeRecipient.lower(),
                'salt': orderData_maker.salt,
                'exchangeContractAddress': orderData_maker.exchangeContractAddress,
                'orderHash': orderData_maker.hash,
                'ecSignature': {
                    'v': int(orderData_maker.v),
                    'r': str(orderData_maker.r),
                    's': str(orderData_maker.s)
                }
            }
        }

    body_taker = {}
    if orderData_taker:
        body_taker = {
            'matchingOrderID': matchingOrderID,
            'signedMatchingOrder': {
                'maker': orderData_taker.maker.lower(),
                'taker': orderData_taker.taker.lower(),
                'makerTokenAddress': orderData_taker.makerTokenAddress.lower(),
                'takerTokenAddress': orderData_taker.takerTokenAddress.lower(),
                'makerTokenAmount': orderData_taker.makerTokenAmount,
                'takerTokenAmount': orderData_taker.takerTokenAmount,
                'makerFee': orderData_taker.makerFee,
                'takerFee': orderData_taker.takerFee,
                'expirationUnixTimestampSec': orderData_taker.expirationUnixTimestampSec,
                'feeRecipient': orderData_taker.feeRecipient.lower(),
                'salt': orderData_taker.salt,
                'exchangeContractAddress': orderData_taker.exchangeContractAddress,
                'orderHash': orderData_taker.hash,
                'ecSignature': {
                    'v': int(orderData_taker.v),
                    'r': str(orderData_taker.r),
                    's': str(orderData_taker.s)
                }
            }
        }

    PrintAndLog("API_PostSignedOrder_MakerAndOrTaker: body_maker = " + str(body_maker))
    PrintAndLog("API_PostSignedOrder_MakerAndOrTaker: body_taker = " + str(body_taker))

    # Merge the bodies for the maker and taker data
    body = Libraries.core.MergeDictionaries(body_maker, body_taker)
    PrintAndLog("API_PostSignedOrder_MakerAndOrTaker: body = " + str(body))
    response = GetAuthenticatedRequest(url, 'POST', body)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_PostSignedOrder_MakerAndOrTaker responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_PostSignedOrder_MakerAndOrTaker jData = " + str(jData))

        # return structure: makerOrderSucceeded (bool), takerOrderSucceeded (bool), takerOrderTransactionId (hex string)
        makerOrderSucceeded = None
        takerOrderSucceeded = None
        takerOrderTransactionId = None

        if not body_maker:
            makerOrderSucceeded = None
        elif jData['targetOrder']['orderHash'].lower() == orderData_maker.hash.lower():
            makerOrderSucceeded = True
        else:
            PrintAndLog("API_PostSignedOrder_MakerAndOrTaker: Hashes DO NOT match")
            makerOrderSucceeded = False

        if not body_taker:
            takerOrderSucceeded = None
        elif jData['matchingOrder']['orderHash'].lower() == orderData_taker.hash.lower():
            takerOrderSucceeded = True
            takerOrderTransactionId = jData['matchingOrder']['transactionHash']
        else:
            PrintAndLog("API_PostSignedOrder_Taker: Hashes DO NOT match")
            takerOrderSucceeded = False

        return makerOrderSucceeded, takerOrderSucceeded, takerOrderTransactionId

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_PostSignedOrder_MakerAndOrTaker response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_CancelOrder(orderHash):
    url = URL_REST_Ocean_Base + "order" + "/" + orderHash

    body = {
        "canceledOrder": {
            "orderHash": orderHash,
        }
    }

    PrintAndLog("API_CancelOrder: body = " + str(body))
    response = GetAuthenticatedRequest(url, 'DELETE', body)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_CancelOrder responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_CancelOrder jData = " + str(jData))

        # If the cancel succeeded
        if jData['canceledOrder']['orderHash'].lower() == orderHash.lower():
            return Libraries.core.OrderCanceledBy_OceanAPI
        # Else the cancel failed for some other reason
        else:
            PrintAndLog("API_CancelOrder: Cancel attempt failed, jData = " + str(jData))
            return None

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_CancelOrder response was not ok response = " + str(response) + ", " + str(response.content))
        # Do not raise_for_status here because we need to handle this response in some cases
        responseData = response.content
        jData = json.loads(responseData)

        # If the orderbook claims they aren't aware of the order which we are trying to cancel
        if 'message' in jData and jData['message'].lower() == "order not found":
            # Despite the fact that the cancel failed, i'm going to treat this as a success because in this case we attempted to cancel an order
            # but the orderbook claims they don't know about that order. So we're going to assume it's not on the books and it's either been canceled, expired, or filled
            message = "Order not found on Ocean with hash = " + str(orderHash) + ", check this to see if it's behaving correctly"
            # Libraries.core.API_PostOperatorNotification(message)
            return Libraries.core.OrderNotFoundOn_Ocean

        else:
            response.raise_for_status()

    return None


def API_GetOrderDetails(orderHash):
    # https://api.theocean.trade/v0/order/[orderHash]
    url = URL_REST_Ocean_Base + "order" + "/" + orderHash

    body = {}
    # PrintAndLog("API_GetOrderDetails: body = " + str(body))
    response = GetAuthenticatedRequest(url, 'GET', body)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetOrderDetails responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetOrderDetails jData = " + str(jData))

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetOrderDetails response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def ConnectWebSocketClient():
    global WebsocketClient
    WebsocketClient = WebsocketClientObject()


class WebsocketClientObject(object):
    ws = None

    def __init__(self, interval=10):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        """ Method that runs forever """

        websocket.enableTrace(False)
        PrintAndLog("URL_WS_Ocean_Base = " + str(URL_WS_Ocean_Base))
        websocket.http_proxy_host = URL_WS_Ocean_Base
        self.ws = websocket.WebSocketApp(URL_WS_Ocean_Base,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)
        while True:
            self.ws.run_forever()
            self.ws.run_forever(ping_interval=70, ping_timeout=10)
            PrintAndLog("I was disconnected from websockets!  Sleeping a bit then trying again")
            time.sleep(30)

    def on_message(self, ws, message):
        PrintAndLog("Websockets Ocean, data received: " + message[0:90] + "....." + message[-25:])
        try:
            jData = json.loads(message)
            PrintAndLog("jData = " + str(jData))

        except:
            PrintAndLogError("exception in WebsocketClientObject: " + traceback.format_exc())
            pass

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws):
        message = "Ocean: Disconnected from websockets. Attempting to re-subscribe to all the endpoints"
        PrintAndLogError(message)
        Libraries.core.API_PostOperatorNotification(message)
