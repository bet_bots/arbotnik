import copy
import sys
import traceback
import threading
import datetime
import time

import Libraries.defaults
from Libraries.pricesDict import *
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog_FuncNameHeader, PrintAndLog_Detailed
from Libraries.core import API_PostOperatorNotification
import Libraries.core
import Libraries.node
import Libraries.cache
import Libraries.gasStation
import Libraries.transactions
import Libraries.exceptions
import Libraries.priceOracle
import Libraries.utils
import Libraries.pendingTransactionMonitor
import Libraries.blockNative
import Libraries.signingUtils
import Libraries.ratesGraph
import Exchanges.uniswap
import Exchanges.kyber
import Exchanges.ninja
import Exchanges.keeperDAO
from Libraries.mempool import GetThreadSafeCopyOf_MempoolTxDict, MempoolTx, TxStatus, AddVillainsTo_WatchedVillainAddressDict
from Libraries.executeOnInterval import IsTimeToExecute

# Optimization, i'm doing this once and storing here so I don't have to waste cycles on doing this thousands of times
FunctionSelectorDict_UniswapV2_Router = None
FunctionNameDict_UniswapV2_Router = None


class PendingTradeTx:
    txHash = None
    exchangeName = None
    functionSelector = None
    data = None
    value = None
    specifiedBlockNumber_int_whichThisTxWasFoundToBePendingAndNotYetMinedIn = None
    fromAddress = None
    toAddress = None
    gasPrice_wei = None
    side = None
    tradeFunctionName = None

    deadline = None
    uniswapBalanceExpected_quoteTokens_afterTxMinesIn = None
    uniswapBalanceExpected_baseTokens_afterTxMinesIn = None
    priceExpected_afterTxMinesIn = None
    priceThisPersonExpectsToGet = None
    worstPriceThisPersonAccepts = None

    quoteTokenAmount_etherUnits = None
    baseTokenAmount_etherUnits = None

    def GetQuoteToken(self):
        raise Exception("Not yet implemented")

    def GetBaseToken(self):
        raise Exception("Not yet implemented")

    def GetQuoteTokenQuantity(self):
        raise Exception("Not yet implemented")

    def GetTokensBeingTraded(self):
        return [self.GetQuoteToken(), self.GetBaseToken()]

    def GetPrice_AfterTxMinesIn_GivenQuoteTokenAmount(self, side, quantity_quoteTokens_etherUnits):
        # Note, do not use self.side in this function, we're letting the function caller specify side
        if Libraries.core.IsBuy(side):
            return Exchanges.uniswap.CalculatePriceGivenBalances(
                side, quantity_quoteTokens_etherUnits, self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn,
                self.uniswapBalanceExpected_baseTokens_afterTxMinesIn)
        elif Libraries.core.IsSell(side):
            return Exchanges.uniswap.CalculatePriceGivenBalances_GivenOutputAmount(
                side, quantity_quoteTokens_etherUnits, self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn,
                self.uniswapBalanceExpected_baseTokens_afterTxMinesIn)
        else:
            raise Exception("Not buy or sell?")

    def GetPrice_AfterTxMinesIn_GivenInflowAmount(self, side, quantity_inflowTokens_etherUnits):
        return Exchanges.uniswap.CalculatePriceGivenBalances(
            side, quantity_inflowTokens_etherUnits, self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn,
            self.uniswapBalanceExpected_baseTokens_afterTxMinesIn)

    def GetPricesDictMetaData(self):
        raise Exception("Not yet implemented")


class PendingTradeTx_Uniswap(PendingTradeTx):
    uniswapAddress = None
    uniswapBalance_quoteTokens = None
    uniswapBalance_baseTokens = None
    tokenAddress = None
    tokenSymbol = None
    decimals_token = None

    def __init__(self, _txHash, _exchangeName, _data, _value, _specifiedBlockNumber_int_whichThisTxWasFoundToBePendingAndNotYetMinedIn,
                 _fromAddress, _uniswapAddress, _uniswapBalance_quoteTokens, _uniswapBalance_baseTokens,
                 _gasPrice_wei, _tokenAddress, _tokenSymbol, _decimals_token,
                 _fromAddressBalance_ether, _fromAddressBalance_tokens, _fromAddressTokenAllowance):
        self.txHash = _txHash
        self.exchangeName = _exchangeName
        self.data = _data
        self.value = _value
        self.specifiedBlockNumber_int_whichThisTxWasFoundToBePendingAndNotYetMinedIn = _specifiedBlockNumber_int_whichThisTxWasFoundToBePendingAndNotYetMinedIn
        self.fromAddress = _fromAddress
        self.uniswapAddress = _uniswapAddress
        self.toAddress = _uniswapAddress
        self.uniswapBalance_quoteTokens = _uniswapBalance_quoteTokens
        self.uniswapBalance_baseTokens = _uniswapBalance_baseTokens
        self.gasPrice_wei = _gasPrice_wei
        self.tokenAddress = _tokenAddress
        self.tokenSymbol = _tokenSymbol
        self.decimals_token = _decimals_token
        self.fromAddressBalance_ether = _fromAddressBalance_ether
        self.fromAddressBalance_tokens = _fromAddressBalance_tokens
        self.fromAddressTokenAllowance = _fromAddressTokenAllowance

        self.functionSelector = Libraries.core.GetFirstXCharsInString(self.data, 10).replace("0x", "")

        # PrintAndLog_FuncNameHeader("Check to see if data has matches any of these. functionSelector = " + str(self.functionSelector))
        # TODO, check these over to see if I made any mistakes
        # PrintAndLog_FuncNameHeader("GetMethodHash_ethToTokenSwapInput = " + str(Exchanges.uniswap.GetMethodHash_ethToTokenSwapInput()))
        # PrintAndLog_FuncNameHeader("GetMethodHash_ethToTokenTransferInput = " + str(Exchanges.uniswap.GetMethodHash_ethToTokenTransferInput()))
        # PrintAndLog_FuncNameHeader("GetMethodHash_ethToTokenSwapOutput = " + str(Exchanges.uniswap.GetMethodHash_ethToTokenSwapOutput()))
        # PrintAndLog_FuncNameHeader("GetMethodHash_ethToTokenTransferOutput = " + str(Exchanges.uniswap.GetMethodHash_ethToTokenTransferOutput()))
        # PrintAndLog_FuncNameHeader("GetMethodHash_tokenToEthSwapInput = " + str(Exchanges.uniswap.GetMethodHash_tokenToEthSwapInput()))
        # PrintAndLog_FuncNameHeader("GetMethodHash_tokenToEthTransferInput = " + str(Exchanges.uniswap.GetMethodHash_tokenToEthTransferInput()))
        # PrintAndLog_FuncNameHeader("GetMethodHash_tokenToEthSwapOutput = " + str(Exchanges.uniswap.GetMethodHash_tokenToEthSwapOutput()))
        # PrintAndLog_FuncNameHeader("GetMethodHash_tokenToEthTransferOutput = " + str(Exchanges.uniswap.GetMethodHash_tokenToEthTransferOutput()))

        # We will want to extract the parameters from the data
        data_0xRemoved = Libraries.core.Remove0XfromHexString(self.data)
        lengthOf_data_withoutMethodHash = len(data_0xRemoved) - len(self.functionSelector)
        data_withoutMethodHash = Libraries.core.GetLastXCharsInString(data_0xRemoved, lengthOf_data_withoutMethodHash)

        PrintAndLog_FuncNameHeader("data = " + str(self.data))
        # PrintAndLog_FuncNameHeader("data_0xRemoved = " + str(data_0xRemoved))
        # PrintAndLog_FuncNameHeader("lengthOf_data_withoutMethodHash = " + str(lengthOf_data_withoutMethodHash))
        # PrintAndLog_FuncNameHeader("data_withoutMethodHash = " + str(data_withoutMethodHash))

        if Exchanges.uniswap.GetMethodHash_tokenToEthSwapInput().lower() == self.functionSelector.lower() or \
                Exchanges.uniswap.GetMethodHash_tokenToEthTransferInput().lower() == self.functionSelector.lower():
            self.tradeFunctionName = "tokenToEthSwapInput"
            PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
            dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(data_withoutMethodHash, Libraries.core.LengthOfDataProperty)
            PrintAndLog_FuncNameHeader("dataList = " + str(dataList))

            self.baseTokenAmount_etherUnits = Libraries.core.GetIntFromDataProperty_EtherUnits(dataList[0], self.decimals_token)
            self.quoteTokenAmount_etherUnits = Libraries.core.GetIntFromDataProperty_EtherUnits(dataList[1], Libraries.core.Ether_Decimals)
            self.deadline = Libraries.core.GetIntFromDataProperty_WeiUnits(dataList[2])

            if len(dataList) == 4:
                # recipient could be different than tx fromAddress
                # TODO for now require that fromAddress == recipient to simplify this
                # TODO, I could simply set self.fromAddress = recipient. But i've already calculated fromAddressBalances... so I'll need to move that calculation into here
                recipient = Libraries.core.GetAddressFromDataProperty(dataList[3])
                if recipient.lower() != self.fromAddress.lower():
                    raise Libraries.exceptions.TradeRecipientDiffersFromTransactionBroadcaster_FeatureNotYetImplementedException('txHash = ' + str(self.txHash))

            PrintAndLog_FuncNameHeader("baseTokenAmount_etherUnits = " + str(self.baseTokenAmount_etherUnits))
            PrintAndLog_FuncNameHeader("quoteTokenAmount_etherUnits = " + str(self.quoteTokenAmount_etherUnits))
            PrintAndLog_FuncNameHeader("deadline = " + str(self.deadline))

            self.worstPriceThisPersonAccepts = Libraries.core.GetPriceGivenEtherAndTokens(self.quoteTokenAmount_etherUnits, self.baseTokenAmount_etherUnits)
            PrintAndLog_FuncNameHeader("worse price this person is accepting = " + str(self.worstPriceThisPersonAccepts))

            self.side = "sell"
            PrintAndLog_FuncNameHeader("side = " + str(self.side))
            self.priceThisPersonExpectsToGet = Exchanges.uniswap.CalculatePriceGivenBalances(
                self.side, self.baseTokenAmount_etherUnits, self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens)
            PrintAndLog_FuncNameHeader("priceThisPersonExpectsToGet = " + str(self.priceThisPersonExpectsToGet))

            # Calculate the price we predict the exchange will have after this trade gets mined in
            self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn, self.uniswapBalanceExpected_baseTokens_afterTxMinesIn = \
                Exchanges.uniswap.CalculateNewExchangeBalancesAssumingTheseTradesGetMinedIn(
                    [self.side], [self.quoteTokenAmount_etherUnits], [self.baseTokenAmount_etherUnits], self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens)
            PrintAndLog_FuncNameHeader(
                "uniswapBalanceExpected_quoteTokens_afterTxMinesIn after TX gets mined in = " + str(self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn) + " ETH")
            PrintAndLog_FuncNameHeader(
                "uniswapBalanceExpected_baseTokens_afterTxMinesIn after TX gets mined in = " + str(self.uniswapBalanceExpected_baseTokens_afterTxMinesIn) + " " + str(
                    self.tokenSymbol))

            self.priceExpected_afterTxMinesIn = Exchanges.uniswap.CalculatePriceGivenBalances(
                self.side, self.baseTokenAmount_etherUnits, self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn, self.uniswapBalanceExpected_baseTokens_afterTxMinesIn)
            PrintAndLog_FuncNameHeader("priceExpected_afterTxMinesIn = " + str(self.priceExpected_afterTxMinesIn))

            # Validations
            PrintAndLog_FuncNameHeader("Validating transaction")
            self.Validate_Price()
            self.Validate_Balance_Tokens(self.baseTokenAmount_etherUnits)
            self.Validate_Allowance_Tokens(self.baseTokenAmount_etherUnits)
            if Libraries.defaults.Tailgating_WithHardCodedTxHash:
                PrintAndLog_FuncNameHeader("Not validating deadline because Tailgating_WithHardCodedTxHash is True")
            else:
                self.Validate_Deadline()

            # If we've made it this far without exception, we've passed all validations

            PrintAndLog_FuncNameHeader("MemPool sniffer found a " + str(self.tradeFunctionName) + " on Uniswap protocol with " +
                                       str(self.exchangeName) + " exchange of size " + str(self.quoteTokenAmount_etherUnits) +
                                       " ETH, txHash = " + str(self.txHash))

        elif Exchanges.uniswap.GetMethodHash_ethToTokenSwapInput().lower() == self.functionSelector.lower() or \
                Exchanges.uniswap.GetMethodHash_ethToTokenTransferInput().lower() == self.functionSelector.lower():
            # TODO determine the function name from the hash
            self.tradeFunctionName = "ethToTokenSwapInput"
            PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
            dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(data_withoutMethodHash, Libraries.core.LengthOfDataProperty)
            PrintAndLog_FuncNameHeader("dataList = " + str(dataList))

            self.baseTokenAmount_etherUnits = Libraries.core.GetIntFromDataProperty_EtherUnits(dataList[0], self.decimals_token)
            self.deadline = Libraries.core.GetIntFromDataProperty_WeiUnits(dataList[1])
            if len(dataList) == 3:
                # recipient could be different than tx fromAddress
                # TODO for now require that fromAddress == recipient to simplify this
                # TODO, I could simply set self.fromAddress = recipient. But i've already calculated fromAddressBalances... so I'll need to move that calculation into here
                recipient = Libraries.core.GetAddressFromDataProperty(dataList[2])
                if recipient.lower() != self.fromAddress.lower():
                    raise Libraries.exceptions.TradeRecipientDiffersFromTransactionBroadcaster_FeatureNotYetImplementedException('txHash = ' + str(self.txHash))

            # Ether comes from the transaction value
            self.quoteTokenAmount_etherUnits = Libraries.core.ConvertWeiToEther(self.value, Libraries.core.Ether_Decimals)
            PrintAndLog_FuncNameHeader("baseTokenAmount_etherUnits = " + str(self.baseTokenAmount_etherUnits))
            PrintAndLog_FuncNameHeader("quoteTokenAmount_etherUnits = " + str(self.quoteTokenAmount_etherUnits))
            PrintAndLog_FuncNameHeader("deadline = " + str(self.deadline))

            self.worstPriceThisPersonAccepts = Libraries.core.GetPriceGivenEtherAndTokens(self.quoteTokenAmount_etherUnits, self.baseTokenAmount_etherUnits)
            PrintAndLog_FuncNameHeader("worse price this person is accepting = " + str(self.worstPriceThisPersonAccepts))

            self.side = "buy"
            PrintAndLog_FuncNameHeader("side = " + str(self.side))
            self.priceThisPersonExpectsToGet = Exchanges.uniswap.CalculatePriceGivenBalances(
                self.side, self.quoteTokenAmount_etherUnits, self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens)
            PrintAndLog_FuncNameHeader("priceThisPersonExpectsToGet = " + str(self.priceThisPersonExpectsToGet))

            # Calculate the price we predict the exchange will have after this trade gets mined in
            self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn, self.uniswapBalanceExpected_baseTokens_afterTxMinesIn = \
                Exchanges.uniswap.CalculateNewExchangeBalancesAssumingTheseTradesGetMinedIn(
                    [self.side], [self.quoteTokenAmount_etherUnits], [self.baseTokenAmount_etherUnits], self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens)
            PrintAndLog_FuncNameHeader(
                "self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn after TX gets mined in = " + str(self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn) + " ETH")
            PrintAndLog_FuncNameHeader(
                "uniswapBalanceExpected_baseTokens_afterTxMinesIn after TX gets mined in = " + str(self.uniswapBalanceExpected_baseTokens_afterTxMinesIn) + " " + str(
                    self.tokenSymbol))

            self.priceExpected_afterTxMinesIn = Exchanges.uniswap.CalculatePriceGivenBalances(
                self.side, self.quoteTokenAmount_etherUnits, self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn, self.uniswapBalanceExpected_baseTokens_afterTxMinesIn)
            PrintAndLog_FuncNameHeader("priceExpected_afterTxMinesIn = " + str(self.priceExpected_afterTxMinesIn))

            # Validations
            PrintAndLog_FuncNameHeader("Validating transaction")
            self.Validate_Price()
            self.Validate_Balance_Ether(self.quoteTokenAmount_etherUnits)
            if Libraries.defaults.Tailgating_WithHardCodedTxHash:
                PrintAndLog_FuncNameHeader("Not validating deadline because Tailgating_WithHardCodedTxHash is True")
            else:
                self.Validate_Deadline()

            # If we've made it this far without exception, we've passed all validations

            PrintAndLog_FuncNameHeader("MemPool sniffer found a " + str(self.tradeFunctionName) + " on Uniswap protocol with " +
                                       str(self.exchangeName) + " exchange of size " + str(self.quoteTokenAmount_etherUnits) +
                                       " ETH, txHash = " + str(self.txHash))

        # TODO, handle ethToTokenSwapOutput

        # TODO, handle tokenToEthSwapOutput

        else:
            PrintAndLog_FuncNameHeader("I'm not currently handling this function: hash = " + str(self.functionSelector))
            self.tradeFunctionName = None

    def GetQuoteToken(self):
        return Exchanges.keeperDAO.EthTokenContract

    def GetBaseToken(self):
        return self.tokenAddress

    def GetQuoteTokenQuantity(self):
        return self.quoteTokenAmount_etherUnits

    def GetPricesDictMetaData(self):
        return {
            pdk_exchangeQuoteTokenBalance_etherUnits: self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn
        }

    def Validate_Price(self):
        # Validate that the current trade price is better than worstPriceThisPersonAccepts
        doFailValidation = False

        # For selling, if current price is less than the worst acceptable
        if Libraries.core.IsSell(self.side) and self.priceThisPersonExpectsToGet < self.worstPriceThisPersonAccepts:
            # This will revert
            doFailValidation = True

        # For buying, if current price is greater than the worst acceptable
        if Libraries.core.IsBuy(self.side) and self.priceThisPersonExpectsToGet > self.worstPriceThisPersonAccepts:
            # This will revert
            doFailValidation = True

        if doFailValidation:
            message = "trade price requirement will fail and lead to revert, self.priceThisPersonExpectsToGet = " + str(
                self.priceThisPersonExpectsToGet) + ", self.worstPriceThisPersonAccepts = " + str(self.worstPriceThisPersonAccepts)
            PrintAndLog_FuncNameHeader(message)
            raise Libraries.exceptions.TradePriceRequirementException(message)

    def Validate_Deadline(self):
        # Validate the deadline to make sure it's in the future
        if not IsUniswapDeadlineValid(self.deadline):
            message = "trade deadline " + str(self.deadline) + " has expired, trade will revert if mined in"
            PrintAndLog_FuncNameHeader(message)
            raise Libraries.exceptions.DeadlineExpiredException(message)

    def Validate_Balance_Ether(self, etherQuantityToTrade_etherUnits):
        # Validate that the trader has enough ether in his account to fund the trade
        if etherQuantityToTrade_etherUnits > self.fromAddressBalance_ether:
            message = "trader does not have enough ether to fund the trade. self.fromAddressBalance_ether = " + str(
                self.fromAddressBalance_ether) + " and etherQuantityToTrade_etherUnits + " + str(etherQuantityToTrade_etherUnits)
            PrintAndLog_FuncNameHeader(message)
            raise Libraries.exceptions.TradeNotFundedException(message)

    def Validate_Balance_Tokens(self, tokenQuantityToTrade_etherUnits):
        # Validate that the trader has enough tokens in his account to fund the trade
        if tokenQuantityToTrade_etherUnits > self.fromAddressBalance_tokens:
            message = "trader does not have enough tokens to fund the trade. self.fromAddressBalance_tokens = " + str(
                self.fromAddressBalance_tokens) + " and tokenQuantityToTrade_etherUnits + " + str(tokenQuantityToTrade_etherUnits)
            PrintAndLog_FuncNameHeader(message)
            raise Libraries.exceptions.TradeNotFundedException(message)

    def Validate_Allowance_Tokens(self, tokenQuantityToTrade_etherUnits):
        # Validate that the trader has enough allowance for the trade
        if tokenQuantityToTrade_etherUnits > self.fromAddressTokenAllowance:
            message = "trader does not have enough token allowance for the trade. tokenQuantityToTrade_etherUnits = " + str(
                tokenQuantityToTrade_etherUnits) + ", self.fromAddressTokenAllowance = " + str(self.fromAddressTokenAllowance)
            PrintAndLog_FuncNameHeader(message)
            raise Libraries.exceptions.InsufficientTokenAllowanceException(message)


class PendingTradeTx_UniswapV2(PendingTradeTx):
    uniswapV2Exchange = None
    uniswapBalance_quoteTokens = None
    uniswapBalance_baseTokens = None
    symbol_quoteToken = None
    symbol_baseToken = None
    tokenAddressList = None
    quoteToken = None
    baseToken = None

    inputTokenAmount_etherUnits = None
    outputTokenAmount_etherUnits = None

    def __init__(self, _txHash, _exchangeName, _data, _value, _specifiedBlockNumber_int_whichThisTxWasFoundToBePendingAndNotYetMinedIn,
                 _fromAddress, _uniswapV2Exchange, _tokenAddressList, _uniswapBalances_tokens, _gasPrice_wei,
                 _isFirstTradeLeg, _inputTokenAmount=None):
        # Optimization, I'm caching this info so I dont' have to calculate it for every pending tx because it requires file system access
        MakeSureFunctionSelectorDictsAreCached()

        self.txHash = _txHash
        self.exchangeName = _exchangeName
        self.data = _data
        self.value = _value
        self.specifiedBlockNumber_int_whichThisTxWasFoundToBePendingAndNotYetMinedIn = _specifiedBlockNumber_int_whichThisTxWasFoundToBePendingAndNotYetMinedIn
        self.fromAddress = _fromAddress
        self.uniswapV2Exchange = _uniswapV2Exchange
        self.tokenAddressList = _tokenAddressList
        self.uniswapBalances_tokens = _uniswapBalances_tokens
        self.gasPrice_wei = _gasPrice_wei

        self.functionSelector = Libraries.core.GetFirstXCharsInString(self.data, 10).replace("0x", "")

        self.SetQuoteTokenProperties()
        self.SetBaseTokenProperties()
        self.SetSideAndBalances_BasedOnQuoteAndBaseTokenAndTradePath(_tokenAddressList, _uniswapBalances_tokens)

        PrintAndLog_FuncNameHeader("Check to see if data has matches any of these. functionSelector = " + str(self.functionSelector))
        PrintAndLog_FuncNameHeader("FunctionSelectorDict_UniswapV2_Router = " + str(FunctionSelectorDict_UniswapV2_Router))
        PrintAndLog_FuncNameHeader("self.uniswapV2Exchange = " + str(self.uniswapV2Exchange))

        # We will want to extract the parameters from the data
        data_0xRemoved = Libraries.core.Remove0XfromHexString(self.data)
        lengthOf_data_withoutMethodHash = len(data_0xRemoved) - len(self.functionSelector)
        data_withoutMethodHash = Libraries.core.GetLastXCharsInString(data_0xRemoved, lengthOf_data_withoutMethodHash)
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(data_withoutMethodHash, Libraries.core.LengthOfDataProperty)

        PrintAndLog_FuncNameHeader("data = " + str(self.data))
        # PrintAndLog_FuncNameHeader("data_0xRemoved = " + str(data_0xRemoved))
        # PrintAndLog_FuncNameHeader("lengthOf_data_withoutMethodHash = " + str(lengthOf_data_withoutMethodHash))
        # PrintAndLog_FuncNameHeader("data_withoutMethodHash = " + str(data_withoutMethodHash))
        PrintAndLog_FuncNameHeader("dataList = " + str(dataList))

        if FunctionSelectorDict_UniswapV2_Router['swapExactETHForTokens'].lower() == self.functionSelector.lower():
            self.tradeFunctionName = FunctionNameDict_UniswapV2_Router[self.functionSelector]
            PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
            # function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline)

            self.inputTokenAmount_etherUnits = None
            if _isFirstTradeLeg:
                # input quantity comes from self.value
                self.inputTokenAmount_etherUnits = Libraries.core.ConvertWeiToEther(
                    self.value, Libraries.core.GetDecimalsForTokenContract(_tokenAddressList[0], True))
            else:
                self.inputTokenAmount_etherUnits = _inputTokenAmount

            self.SetOutputTokenAmountAndQuoteAndBaseTokenAmount()

            recipient = Libraries.core.GetAddressFromDataProperty(dataList[2])
            if self.fromAddress.lower() != recipient:
                raise Libraries.exceptions.TradeRecipientDiffersFromTransactionBroadcaster_FeatureNotYetImplementedException('txHash = ' + str(self.txHash))

            self.deadline = Libraries.core.GetIntFromDataProperty_WeiUnits(dataList[3])
            PrintAndLog_FuncNameHeader("deadline = " + str(self.deadline))

            self.worstPriceThisPersonAccepts = Libraries.core.GetPriceGivenEtherAndTokens(self.quoteTokenAmount_etherUnits, self.baseTokenAmount_etherUnits)
            PrintAndLog_FuncNameHeader("worse price this person is accepting = " + str(self.worstPriceThisPersonAccepts))

            self.priceThisPersonExpectsToGet = Exchanges.uniswap.CalculatePriceGivenBalances(
                self.side, self.quoteTokenAmount_etherUnits, self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens)
            PrintAndLog_FuncNameHeader("priceThisPersonExpectsToGet = " + str(self.priceThisPersonExpectsToGet))

            # Calculate the price we predict the exchange will have after this trade gets mined in
            self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn, self.uniswapBalanceExpected_baseTokens_afterTxMinesIn = \
                Exchanges.uniswap.CalculateNewExchangeBalancesAssumingTheseTradesGetMinedIn(
                    [self.side], [self.quoteTokenAmount_etherUnits], [self.baseTokenAmount_etherUnits],
                    self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens)
            PrintAndLog_FuncNameHeader("self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn after TX gets mined in = " + str(
                self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn) + " " + str(self.symbol_quoteToken))
            PrintAndLog_FuncNameHeader("uniswapBalanceExpected_baseTokens_afterTxMinesIn after TX gets mined in = " + str(
                self.uniswapBalanceExpected_baseTokens_afterTxMinesIn) + " " + str(self.symbol_baseToken))

            self.priceExpected_afterTxMinesIn = Exchanges.uniswap.CalculatePriceGivenBalances(
                self.side, self.quoteTokenAmount_etherUnits, self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn,
                self.uniswapBalanceExpected_baseTokens_afterTxMinesIn)
            PrintAndLog_FuncNameHeader("priceExpected_afterTxMinesIn = " + str(self.priceExpected_afterTxMinesIn))

            # Validations
            PrintAndLog_FuncNameHeader("Validating transaction")
            self.Validate_Price()
            # TODO not doing these for now, doing eth_estimateGas instead. Is this the right thing to do?
            # self.Validate_Balance_Ether(self.quoteTokenAmount_etherUnits)
            if Libraries.defaults.Tailgating_WithHardCodedTxHash:
                PrintAndLog_FuncNameHeader("Not validating deadline because Tailgating_WithHardCodedTxHash is True")
            else:
                self.Validate_Deadline()

            # If we've made it this far without exception, we've passed all validations

            PrintAndLog_FuncNameHeader("MemPool sniffer found a " + str(self.tradeFunctionName) + " on UniswapV2 protocol with " +
                                       str(self.exchangeName) + " exchange of size " + str(self.quoteTokenAmount_etherUnits) +
                                       " " + str(self.symbol_quoteToken) + ", txHash = " + str(self.txHash))

        elif FunctionSelectorDict_UniswapV2_Router['swapExactTokensForETH'].lower() == self.functionSelector.lower():
            self.tradeFunctionName = FunctionNameDict_UniswapV2_Router[self.functionSelector]
            PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
            # function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline)

            self.inputTokenAmount_etherUnits = None
            if _isFirstTradeLeg:
                # input quantity comes from dataList[0]
                self.inputTokenAmount_etherUnits = Libraries.core.GetIntFromDataProperty_EtherUnits(
                    dataList[0], Libraries.core.GetDecimalsForTokenContract(_tokenAddressList[0], True))
            else:
                self.inputTokenAmount_etherUnits = _inputTokenAmount

            self.SetOutputTokenAmountAndQuoteAndBaseTokenAmount()

            recipient = Libraries.core.GetAddressFromDataProperty(dataList[3])
            if self.fromAddress.lower() != recipient:
                raise Libraries.exceptions.TradeRecipientDiffersFromTransactionBroadcaster_FeatureNotYetImplementedException('txHash = ' + str(self.txHash))

            self.deadline = Libraries.core.GetIntFromDataProperty_WeiUnits(dataList[4])
            PrintAndLog_FuncNameHeader("deadline = " + str(self.deadline))

            self.worstPriceThisPersonAccepts = Libraries.core.GetPriceGivenEtherAndTokens(self.quoteTokenAmount_etherUnits, self.baseTokenAmount_etherUnits)
            PrintAndLog_FuncNameHeader("worse price this person is accepting = " + str(self.worstPriceThisPersonAccepts))

            self.priceThisPersonExpectsToGet = Exchanges.uniswap.CalculatePriceGivenBalances(
                self.side, self.baseTokenAmount_etherUnits, self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens)
            PrintAndLog_FuncNameHeader("priceThisPersonExpectsToGet = " + str(self.priceThisPersonExpectsToGet))

            # Calculate the price we predict the exchange will have after this trade gets mined in
            self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn, self.uniswapBalanceExpected_baseTokens_afterTxMinesIn = \
                Exchanges.uniswap.CalculateNewExchangeBalancesAssumingTheseTradesGetMinedIn(
                    [self.side], [self.quoteTokenAmount_etherUnits], [self.baseTokenAmount_etherUnits],
                    self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens)
            PrintAndLog_FuncNameHeader("uniswapBalanceExpected_quoteTokens_afterTxMinesIn after TX gets mined in = " + str(
                self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn) + " " + str(self.symbol_quoteToken))
            PrintAndLog_FuncNameHeader("uniswapBalanceExpected_baseTokens_afterTxMinesIn after TX gets mined in = " + str(
                self.uniswapBalanceExpected_baseTokens_afterTxMinesIn) + " " + str(self.symbol_baseToken))

            self.priceExpected_afterTxMinesIn = Exchanges.uniswap.CalculatePriceGivenBalances(
                self.side, self.baseTokenAmount_etherUnits, self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn,
                self.uniswapBalanceExpected_baseTokens_afterTxMinesIn)
            PrintAndLog_FuncNameHeader("priceExpected_afterTxMinesIn = " + str(self.priceExpected_afterTxMinesIn))

            # Validations
            PrintAndLog_FuncNameHeader("Validating transaction")
            self.Validate_Price()
            # TODO not doing these for now, doing eth_estimateGas instead. Is this the right thing to do?
            # self.Validate_Balance_Tokens(self.baseTokenAmount_etherUnits)
            # self.Validate_Allowance_Tokens(self.baseTokenAmount_etherUnits)
            if Libraries.defaults.Tailgating_WithHardCodedTxHash:
                PrintAndLog_FuncNameHeader("Not validating deadline because Tailgating_WithHardCodedTxHash is True")
            else:
                self.Validate_Deadline()

            # If we've made it this far without exception, we've passed all validations

            PrintAndLog_FuncNameHeader("MemPool sniffer found a " + str(self.tradeFunctionName) + " on UniswapV2 protocol with " +
                                       str(self.exchangeName) + " exchange of size " + str(self.quoteTokenAmount_etherUnits) +
                                       " " + str(self.symbol_quoteToken) + ", txHash = " + str(self.txHash))

        # TODO, WETH would be TokenForToken so make sure that works correctly as well
        # TODO
        # elif FunctionSelectorDict_UniswapV2_Router['swapExactTokensForTokens'].lower() == self.functionSelector.lower():
        #     self.tradeFunctionName = FunctionNameDict_UniswapV2_Router[self.functionSelector]
        #     PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
        #     #     function swapExactTokensForTokens(
        #     #         uint amountIn,
        #     #         uint amountOutMin,
        #     #         address[] calldata path,
        #     #         address to,
        #     #         uint deadline
        #     #     ) external returns (uint[] memory amounts);
        # TODO
        # elif FunctionSelectorDict_UniswapV2_Router['swapTokensForExactTokens'].lower() == self.functionSelector.lower():
        #     self.tradeFunctionName = FunctionNameDict_UniswapV2_Router[self.functionSelector]
        #     PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
        #
        #     #     function swapTokensForExactTokens(
        #     #         uint amountOut,
        #     #         uint amountInMax,
        #     #         address[] calldata path,
        #     #         address to,
        #     #         uint deadline
        #     #     ) external returns (uint[] memory amounts);
        #
        # elif FunctionSelectorDict_UniswapV2_Router['swapTokensForExactETH'].lower() == self.functionSelector.lower():
        #     self.tradeFunctionName = FunctionNameDict_UniswapV2_Router[self.functionSelector]
        #     PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
        #     # function swapTokensForExactETH(uint amountOut, uint amountInMax, address[] calldata path, address to, uint deadline)
        #
        # elif FunctionSelectorDict_UniswapV2_Router['swapETHForExactTokens'].lower() == self.functionSelector.lower():
        #     self.tradeFunctionName = FunctionNameDict_UniswapV2_Router[self.functionSelector]
        #     PrintAndLog_FuncNameHeader("This trade matches a " + str(self.tradeFunctionName) + "!")
        #     # function swapETHForExactTokens(uint amountOut, address[] calldata path, address to, uint deadline)

        else:
            PrintAndLog_FuncNameHeader("I'm not currently handling this function: hash = " + str(self.functionSelector) + " name = " + str(
                FunctionNameDict_UniswapV2_Router[self.functionSelector]))
            self.tradeFunctionName = None

    def SetQuoteTokenProperties(self):
        from Exchanges.zrxV2 import Contract_WETH

        PrintAndLog_FuncNameHeader("self.tokenAddressList = " + str(self.tokenAddressList))
        PrintAndLog_FuncNameHeader("self.uniswapBalances_tokens = " + str(self.uniswapBalances_tokens))

        # Set the quoteToken properties
        quoteTokenToUse = None
        possibleQuoteTokens = Exchanges.keeperDAO.GetBorrowableQuoteTokensList(True)
        for quoteToken in possibleQuoteTokens:
            if quoteToken.lower() in self.tokenAddressList:
                quoteTokenToUse = quoteToken.lower()
                # Break in case we see more possible quote tokens later on in the list, we just want to choose the first one
                break

        if not quoteTokenToUse:
            raise Exception("Didn't find a quoteToken. self.tokenAddressList is incorrect or not supported quoteToken in this trade")

        # If it's WETH, we want to treat it like Exchanges.keeperDAO.EthTokenContract for now
        if quoteTokenToUse.lower() == Contract_WETH.lower():
            self.quoteToken = Exchanges.keeperDAO.EthTokenContract.lower()
        else:
            self.quoteToken = quoteTokenToUse.lower()

        self.symbol_quoteToken = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(self.quoteToken)

        PrintAndLog_FuncNameHeader("Setting properties, self.quoteToken = " + str(
            self.quoteToken) + ", self.uniswapBalance_quoteTokens = " + str(self.uniswapBalance_quoteTokens))

    def SetBaseTokenProperties(self):
        from Exchanges.zrxV2 import Contract_WETH

        # Set the baseToken properties
        # Let's just return the first token that's not the quoteToken.
        quoteToken = self.GetQuoteToken()
        if quoteToken.lower() == Exchanges.keeperDAO.EthTokenContract.lower():
            quoteToken = Contract_WETH.lower()

        for index, token in enumerate(self.tokenAddressList):
            if token.lower() != quoteToken.lower():
                self.baseToken = token.lower()
                self.symbol_baseToken = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(self.baseToken)
                PrintAndLog_FuncNameHeader("Setting properties, self.baseToken = " + str(self.baseToken))
                return

        raise Exception("Didn't find a baseToken. self.tokenAddressList is incorrect.")

    def SetSideAndBalances_BasedOnQuoteAndBaseTokenAndTradePath(self, tokenAddressList, tradePathBalances):
        from Libraries.exchanges import EnforceTokenAddressIsERC20Version

        PrintAndLog_FuncNameHeader("tokenAddressList = " + str(tokenAddressList))
        PrintAndLog_FuncNameHeader("tradePathBalances = " + str(tradePathBalances))

        inputToken = tokenAddressList[0]
        outputToken = tokenAddressList[1]
        inputBalance = tradePathBalances[0]
        outputBalance = tradePathBalances[1]

        if EnforceTokenAddressIsERC20Version(inputToken).lower() == EnforceTokenAddressIsERC20Version(self.quoteToken).lower():
            self.side = 'buy'
            self.uniswapBalance_quoteTokens = inputBalance
            self.uniswapBalance_baseTokens = outputBalance
        elif EnforceTokenAddressIsERC20Version(outputToken).lower() == EnforceTokenAddressIsERC20Version(self.quoteToken).lower():
            self.side = 'sell'
            self.uniswapBalance_quoteTokens = outputBalance
            self.uniswapBalance_baseTokens = inputBalance
        else:
            raise Exception("Not buy or sell? tokenAddressList = " + str(tokenAddressList) + ", self.quoteToken = " + str(self.quoteToken))

        PrintAndLog_FuncNameHeader("side = " + str(self.side))
        PrintAndLog_FuncNameHeader("uniswapBalance_quoteTokens = " + str(self.uniswapBalance_quoteTokens))
        PrintAndLog_FuncNameHeader("uniswapBalance_baseTokens = " + str(self.uniswapBalance_baseTokens))

    def SetOutputTokenAmountAndQuoteAndBaseTokenAmount(self):
        if not self.inputTokenAmount_etherUnits:
            raise Exception("self.inputTokenAmount_etherUnits must be set before we can call this function")

        if not self.side:
            raise Exception("self.side must be set before we can call this function")

        PrintAndLog_FuncNameHeader("side = " + str(self.side))
        PrintAndLog_FuncNameHeader("inputTokenAmount_etherUnits = " + str(self.inputTokenAmount_etherUnits))
        PrintAndLog_FuncNameHeader("uniswapBalance_quoteTokens = " + str(self.uniswapBalance_quoteTokens))
        PrintAndLog_FuncNameHeader("uniswapBalance_baseTokens = " + str(self.uniswapBalance_baseTokens))

        price, self.outputTokenAmount_etherUnits = Exchanges.uniswap.CalculatePriceGivenBalances(
            self.side, self.inputTokenAmount_etherUnits,
            self.uniswapBalance_quoteTokens, self.uniswapBalance_baseTokens, True)
        PrintAndLog_FuncNameHeader("inputTokenAmount_etherUnits = " + str(self.inputTokenAmount_etherUnits))
        PrintAndLog_FuncNameHeader("outputTokenAmount_etherUnits = " + str(self.outputTokenAmount_etherUnits))

        if Libraries.core.IsBuy(self.side):
            self.quoteTokenAmount_etherUnits = self.inputTokenAmount_etherUnits
            self.baseTokenAmount_etherUnits = self.outputTokenAmount_etherUnits
        elif Libraries.core.IsSell(self.side):
            self.quoteTokenAmount_etherUnits = self.outputTokenAmount_etherUnits
            self.baseTokenAmount_etherUnits = self.inputTokenAmount_etherUnits
        else:
            raise Exception("not buy or sell?")

        PrintAndLog_FuncNameHeader("baseTokenAmount_etherUnits = " + str(self.baseTokenAmount_etherUnits))
        PrintAndLog_FuncNameHeader("quoteTokenAmount_etherUnits = " + str(self.quoteTokenAmount_etherUnits))

    def GetQuoteToken(self):
        return self.quoteToken

    def GetBaseToken(self):
        return self.baseToken

    def GetQuoteTokenQuantity(self):
        return self.quoteTokenAmount_etherUnits

    def GetPricesDictMetaData(self):
        return {
            pdk_exchangeQuoteTokenBalance_etherUnits: self.uniswapBalanceExpected_quoteTokens_afterTxMinesIn
        }

    def Validate_Price(self):
        # Validate that the current trade price is better than worstPriceThisPersonAccepts
        doFailValidation = False

        # For selling, if current price is less than the worst acceptable
        if Libraries.core.IsSell(self.side) and self.priceThisPersonExpectsToGet < self.worstPriceThisPersonAccepts:
            # This will revert
            doFailValidation = True

        # For buying, if current price is greater than the worst acceptable
        if Libraries.core.IsBuy(self.side) and self.priceThisPersonExpectsToGet > self.worstPriceThisPersonAccepts:
            # This will revert
            doFailValidation = True

        if doFailValidation:
            message = "trade price requirement will fail and lead to revert, self.priceThisPersonExpectsToGet = " + str(
                self.priceThisPersonExpectsToGet) + ", self.worstPriceThisPersonAccepts = " + str(self.worstPriceThisPersonAccepts)
            PrintAndLog_FuncNameHeader(message)
            raise Libraries.exceptions.TradePriceRequirementException(message)

    def Validate_Deadline(self):
        # Validate the deadline to make sure it's in the future
        if not IsUniswapDeadlineValid(self.deadline):
            message = "trade deadline " + str(self.deadline) + " has expired, trade will revert if mined in"
            PrintAndLog_FuncNameHeader(message)
            raise Libraries.exceptions.DeadlineExpiredException(message)

    # def Validate_Balance_Ether(self, etherQuantityToTrade_etherUnits):
    #     # Validate that the trader has enough ether in his account to fund the trade
    #     if etherQuantityToTrade_etherUnits > self.fromAddressBalance_ether:
    #         message = "trader does not have enough ether to fund the trade. self.fromAddressBalance_ether = " + str(
    #             self.fromAddressBalance_ether) + " and etherQuantityToTrade_etherUnits + " + str(etherQuantityToTrade_etherUnits)
    #         PrintAndLog_FuncNameHeader(message)
    #         raise Libraries.exceptions.TradeNotFundedException(message)
    #
    # def Validate_Balance_Tokens(self, tokenQuantityToTrade_etherUnits):
    #     # Validate that the trader has enough tokens in his account to fund the trade
    #     if tokenQuantityToTrade_etherUnits > self.fromAddressBalance_tokens:
    #         message = "trader does not have enough tokens to fund the trade. self.fromAddressBalance_tokens = " + str(
    #             self.fromAddressBalance_tokens) + " and tokenQuantityToTrade_etherUnits + " + str(tokenQuantityToTrade_etherUnits)
    #         PrintAndLog_FuncNameHeader(message)
    #         raise Libraries.exceptions.TradeNotFundedException(message)
    #
    # def Validate_Allowance_Tokens(self, tokenQuantityToTrade_etherUnits):
    #     # Validate that the trader has enough allowance for the trade
    #     if tokenQuantityToTrade_etherUnits > self.fromAddressTokenAllowance:
    #         message = "trader does not have enough token allowance for the trade. tokenQuantityToTrade_etherUnits = " + str(
    #             tokenQuantityToTrade_etherUnits) + ", self.fromAddressTokenAllowance = " + str(self.fromAddressTokenAllowance)
    #         PrintAndLog_FuncNameHeader(message)
    #         raise Libraries.exceptions.InsufficientTokenAllowanceException(message)


def TailgateMemPool():
    from Libraries.exchanges import ExchangeDict

    # Create a dict that associates the exchanges with their mempool watchAddresses
    watchAddressesDict = {}
    for exchangeName in ExchangeDict:
        exchange = ExchangeDict[exchangeName]
        if exchange.CanTailgate():
            watchAddresses = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(exchange.GetTailgatingWatchAddresses())
            if len(watchAddresses) > 0:
                watchAddressesDict[exchangeName] = watchAddresses
                # Add these watchAddresses to the watch list for mempool
                # It's extremely inefficient to watch all txs in the mempool since there are so many, so we only watch ones we care about
                AddVillainsTo_WatchedVillainAddressDict(watchAddresses)
                if Libraries.defaults.EnableMempool_BlockNative and Libraries.defaults.CallBlockNativesAddWatchAddresses:
                    PrintAndLog_FuncNameHeader("Calling API_AddWatchAddresses to blockNative. This may take a while. watchAddresses = " + str(watchAddresses))
                    # Start this in a background thread, because we don't care when it finishes
                    t = threading.Thread(target=Libraries.blockNative.API_AddWatchAddresses, args=(watchAddresses,))
                    t.start()

    PrintAndLog_FuncNameHeader("watchAddressesDict = " + str(watchAddressesDict))

    hardCoded_MempoolTxDict = None

    if Libraries.defaults.Tailgating_WithHardCodedTxHash:
        # hardCodedTx = '0x095d986eca71d0bd7874b94f196d381f9b04ab9b6ebe0b2713f7522728a066d1'  # Uniswap
        # hardCodedTx = '0x94b5ba1ed66f43dd3b91e33eb6955e3eb90bb7ae1db8d17c0d7d696865a625fa'  # UniswapV2
        # hardCodedTx = '0x57b2d48947cb265b756714c6b5516d7dde42e988259787666edbaf5d01a11d73'
        # hardCodedTx = '0xfed3f374220dff0eea4f640b9470139d69c3580d3e904a054a63ddc0b2a1a039'  # UniswapV2
        # hardCodedTx = '0x3f75aac6cc36077e2a4afc311bf29d9c2dd19c2832b604b77fb4a957b1c8cc48'  # UniswapV2
        hardCodedTx = '0x9ece936fd3b34eeb538dd2fdfa6a6da5ff4589300e78d4821051096c0c043207'  # UniswapV2
        hardCodedTx_receipt = Libraries.core.API_GetTransactionReceipt(hardCodedTx)
        hardCodedTx_info = Libraries.core.API_GetTransactionInfo(hardCodedTx)
        PrintAndLog_FuncNameHeader("hardCodedTx_receipt = " + str(hardCodedTx_receipt))
        PrintAndLog_FuncNameHeader("hardCodedTx_info = " + str(hardCodedTx_info))

        gasPrice_wei = Libraries.core.ConvertHexToInt(hardCodedTx_info['gasPrice'])
        gas = Libraries.core.ConvertHexToInt(hardCodedTx_info['gas'])
        value = Libraries.core.ConvertHexToInt(hardCodedTx_info['value'])
        nonce = Libraries.core.ConvertHexToInt(hardCodedTx_info['nonce'])

        mempoolTx = MempoolTx(
            hardCodedTx, hardCodedTx_receipt['to'], hardCodedTx_receipt['from'], hardCodedTx_receipt['to'],
            gasPrice_wei, gas, value, nonce, hardCodedTx_info['input'], TxStatus.Pending, "HardCoded")

        hardCoded_MempoolTxDict = {hardCodedTx: mempoolTx}

    # These two processedTxsDict dicts are to help me loop more efficiently
    # processedTxsDict contains all txs we've processed regardless of result
    # processedTxsDict_watchListed contain only ones we've matched with our watchList. AKA txs we care about tailgating
    # processedTxsDict will get MASSIVE very quickly so we want to clear that out periodically
    # processedTxsDict_watchListed will be very finite and we can just let that get cleared out every time we reboot the bot, no need to manually clear that out
    # We can clear out processedTxsDict occasionally without worrying about double processing a watchList trade
    # because processedTxsDict_watchListed will contain all watchList trades
    processedTxsDict = {}
    processedTxsDict_watchListed = {}
    # Assume the mempool services are running
    while True:
        try:
            PrintAndLog_FuncNameHeader("Begin TailgateMemPool loop")

            # Periodically clear out processedTxsDict because it will get absolutely massive very quickly.
            if IsTimeToExecute("clear_processedTxsDict", 300):
                # PrintAndLog_FuncNameHeader("Clearing processedTxsDict because it's been a while. This should not break anything!")
                processedTxsDict = {}

            PrintAndLog_FuncNameHeader(str(len(processedTxsDict)) + " items in processedTxsDict. This should periodically reset")
            PrintAndLog_FuncNameHeader(str(len(processedTxsDict_watchListed)) + " items in processedTxsDict_watchListed")
            t = threading.Thread(target=Libraries.gasStation.ConsiderGettingRecommendedGasPrices, args=())
            t.start()

            copy_MempoolTxDict = None
            if Libraries.defaults.Tailgating_WithHardCodedTxHash:
                copy_MempoolTxDict = hardCoded_MempoolTxDict
            else:
                copy_MempoolTxDict = GetThreadSafeCopyOf_MempoolTxDict()

            # PrintAndLog_FuncNameHeader("copy_MempoolTxDict (Tailgating_WithHardCodedTxHash " + str(Tailgating_WithHardCodedTxHash) + ") = " + str(copy_MempoolTxDict))

            gasPriceMinimum_wei = Libraries.gasStation.GetGasPrice_SafeLow()
            count_withinGasPriceRange = 0

            for txHash in copy_MempoolTxDict:
                # Skip txs that we've already processed
                if txHash.lower() in processedTxsDict or txHash.lower() in processedTxsDict_watchListed:
                    continue

                try:
                    # Note, I should never see a tx on the watchList processed twice
                    # but occasionally I may see a tx not on the watchList processed twice
                    # this may happen immediately after a processedTxsDict reset

                    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
                    # Only process the txHash to tailgate only if latestBlockNumber has hit our target block number
                    # specified by timeMachine_TravelToBlockNumber
                    if Libraries.defaults.timeMachine_TravelToBlockNumber and latestBlockNumber != Libraries.defaults.timeMachine_TravelToBlockNumber - 1:
                        PrintAndLog_FuncNameHeader("Time machine is enabled and we haven't found the target block of interest, so continue while we wait for it. "
                                                   "latestBlockNumber = " + str(latestBlockNumber) + ", timeMachine_TravelToBlockNumber = " +
                                                   str(Libraries.defaults.timeMachine_TravelToBlockNumber))
                        continue

                    PrintAndLog_FuncNameHeader("Processing txHash " + str(txHash) + ", latestBlockNumber = " + str(latestBlockNumber))
                    # Add this txHash to processedTxsDict so we never process it again
                    processedTxsDict[txHash.lower()] = True

                    mempoolTx = copy_MempoolTxDict[txHash]

                    # TODO put gasPrice_gwei in the mempoolTx object then remove these local variables and just access it from the object
                    gasPrice_wei = mempoolTx.gasPrice_wei
                    # Skip over anything not within our bounds
                    if gasPrice_wei < gasPriceMinimum_wei:
                        PrintAndLog_FuncNameHeader("Breaking because gas price was too low. gasPrice_gwei = " + str(
                            Libraries.core.ConvertWeiToGwei(gasPrice_wei)) + " gwei, gasPriceMinimum_gwei = " + str(
                            Libraries.core.ConvertWeiToGwei(gasPriceMinimum_wei)) + " gwei")
                        continue

                    count_withinGasPriceRange += 1

                    # PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress))
                    # PrintAndLog_FuncNameHeader("toAddress = " + str(toAddress))
                    # PrintAndLog_FuncNameHeader("nonce = " + str(nonce))
                    # PrintAndLog_FuncNameHeader("value = " + str(value))

                    for exchangeName in watchAddressesDict:
                        watchAddressList = watchAddressesDict[exchangeName]
                        # If we care about this tx
                        if mempoolTx.toAddress.lower() in watchAddressList:
                            PrintAndLog_FuncNameHeader("Matched a watch address for " + str(
                                exchangeName) + "! Analyzing this tx for tailgating. txHash = " + str(txHash))

                            # Add this txHash to processedTxsDict_watchListed so we never process it again
                            processedTxsDict_watchListed[txHash.lower()] = True

                            pendingTradeTxList = ExchangeDict[exchangeName].CreatePendingTradeTx(mempoolTx)
                            if not pendingTradeTxList:
                                PrintAndLog_FuncNameHeader("pendingTradeTxList failed validation, not trading. txHash = " + str(txHash))
                            else:
                                PrintAndLog_FuncNameHeader("Calling NinjaTailgateGeneric in a background thread for " +
                                                           str(len(pendingTradeTxList)) + " pendingTradeTxs which have txHash = " +
                                                           str(pendingTradeTxList[0].txHash))

                                threadName = "TailgateMemPool" + "_" + exchangeName
                                IsTimeToExecute(threadName, 0, NinjaTailgateGeneric, True, True, True, pendingTradeTxList)
                                #
                                # This worked, but all the tailgating origin exchange's are logging to the same log file, I'm trying to get them logging to separate log files
                                # t = threading.Thread(
                                #     name=threading.currentThread().getName(),  # Keep the thread name the same to help with logging consistency
                                #     target=NinjaTailgateGeneric,
                                #     args=(pendingTradeTxList,))
                                # t.start()

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    PrintAndLogError("exception in TailgateMemPool when processing txHash " + str(txHash) + ", " + traceback.format_exc())
                    pass

            PrintAndLog_FuncNameHeader("Found " + str(len(copy_MempoolTxDict)) + " transactions in the mempool. " + str(
                count_withinGasPriceRange) + " of them have gasPrice at or above " + str(
                Libraries.core.ConvertWeiToGwei(gasPriceMinimum_wei)) + " gwei")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception in TailgateMemPool, " + traceback.format_exc())
            pass

        # TODO, should I sleep at all here??
        time.sleep(0.04)


def NinjaTailgateGeneric(pendingTradeTxList):
    import Libraries.ninjaOps
    from Libraries.accounts import CreateNewReservableEthereumAccountPoolList
    from Exchanges.ninja_arbitrage import ArbitrageGeneric, TradingTechnique
    from Libraries.exchanges import EnforceTokenAddressIsERC20Version

    # Get the side of trade so I know where to begin the trade path
    # Get the quoteToken amount and match it with a quoteToken amount in the pricesDict
    # Iterate over all exchanges in pricesDict and all possible trading paths against this specific trade (quantity <= this quantity)
    # Calculate profit and determine the max profit opportunity

    txHash = pendingTradeTxList[0].txHash

    header_logging = "NinjaTailgateGeneric: pendingTradeTxList of len " + str(len(pendingTradeTxList)) + " with txHash " + \
                     str(txHash) + "-blockNumberPending"
    functionsStartDateTime = datetime.datetime.now()

    # Get the ratesGraphs so we can analyze trading opportunities with this tailgating tx
    timeout_s = 5
    elapsedTime_s = 0
    sleepTime_s = 0.05
    while True:
        # TODO, should I use latest block number here or specifiedBlockNumber_int_whichThisTxWasFoundToBePendingAndNotYetMinedIn
        #  I'm thinking I should go with the latest block number because it's possible
        #  a new block was just mined in 1 second after this tx hit the mempool and we'd rather use the latest data right?
        blockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

        if Libraries.ratesGraph.IsDataAvailableForBlockNumber(blockNumber) and Exchanges.ninja.GetPricesDict(blockNumber):
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "We have ratesGraph and pricesDict data for blockNumber " + str(blockNumber) + ", breaking from loop")
            break
        else:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "We do NOT have ratesGraph and pricesDict data data for blockNumber " + str(blockNumber) + ", let's wait for it")

        elapsedTime_s += sleepTime_s
        if elapsedTime_s > timeout_s:
            message = "Breaking from loop because we didn't find the ratesGraph data for blockNumber " + str(
                blockNumber) + ",this should never happen! Investigate WTF happened here."
            Libraries.core.API_PostOperatorNotification(message)
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, message)
            raise Exception("message")

        time.sleep(sleepTime_s)

    # Update header_logging now that we have a blockNumber
    header_logging = "NinjaTailgateGeneric: pendingTradeTxList of len " + str(len(pendingTradeTxList)) + " with txHash " + \
                     str(txHash) + "-" + str(blockNumber)

    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Proceeding with tailgate")

    # ********** Begin copy the pricesDict ********** #
    before = datetime.datetime.now()
    forTailgate_pricesDict = copy.deepcopy(Exchanges.ninja.GetPricesDict(blockNumber))
    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "deepcopy of forTailgate_pricesDict completed with duration_s = " + str(duration_s) + " seconds")
    if Libraries.defaults.VerboseLogging_Tailgating_PricesDictAndRatesGraphs:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "forTailgate_pricesDict before = " + str(forTailgate_pricesDict))
    # ********** End copy the pricesDict ********** #

    # ********** Begin copy the ratesGraph ********** #
    # Get the ratesGraphs that we're going to copy
    copy_ratesGraph_Dict = {}
    copy_ratesGraph_Dict_ExchangeNames = {}
    copy_ratesGraph_Dict_QuoteTokens = {}
    copyFromThisOne_ratesGraph_Dict, copyFromThisOne_ratesGraph_Dict_ExchangeNames, copyFromThisOne_ratesGraph_Dict_QuoteTokens = \
        Libraries.ratesGraph.GetRatesGraphDicts(blockNumber, None)

    # Perform the copy
    before = datetime.datetime.now()
    copy_ratesGraph_Dict[blockNumber] = copy.deepcopy(copyFromThisOne_ratesGraph_Dict)
    copy_ratesGraph_Dict_ExchangeNames[blockNumber] = copy.deepcopy(copyFromThisOne_ratesGraph_Dict_ExchangeNames)
    copy_ratesGraph_Dict_QuoteTokens[blockNumber] = copy.deepcopy(copyFromThisOne_ratesGraph_Dict_QuoteTokens)
    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "   deepcopy of ratesGraph_Dict completed with duration_s = " + str(duration_s) + " seconds")
    # ********** End copy the ratesGraph ********** #

    # Below I will want to copy the ratesGraphs so I can modify them
    # But the trick here is that pendingTradeTxList is a list so we may have multiple pendingTradeTx
    # So that means we want to only make the deepcopy one single time and then for every other
    # pendingTradeTx in the pendingTradeTxList we want to refer to that copy and keep updating that new copy
    # Key these local_copy dicts by the index_string
    # value will be copy_ratesGraph_Dict, copy_ratesGraph_Dict_ExchangeNames, and copy_ratesGraph_Dict_QuoteTokens
    local_copy_ratesGraph_Dict = {}
    local_copy_ratesGraph_Dict_ExchangeNames = {}
    local_copy_ratesGraph_Dict_QuoteTokens = {}

    for pendingTradeTx in pendingTradeTxList:
        # TODO for loop goes here??
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pendingTradeTx.GetBaseToken() = " + str(pendingTradeTx.GetBaseToken()))
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pendingTradeTx.GetQuoteToken() = " + str(pendingTradeTx.GetQuoteToken()))

        # Ensure that our ratesGraph contains these tokens
        if pendingTradeTx.GetBaseToken().lower() not in Libraries.ratesGraph.RatesGraphTokenListIndexDict or \
                pendingTradeTx.GetQuoteToken().lower() not in Libraries.ratesGraph.RatesGraphTokenListIndexDict:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "pendingTradeTx token pair not in the ratesGraph. We cannot tailgate this trade. Skipping this internal trade. quoteToken = " +
                                 str(pendingTradeTx.GetQuoteToken()) + ", baseToken = " + str(pendingTradeTx.GetBaseToken()))
            # TODO, use to be a return but should now be continue??
            continue

        pendingTradeTx_quoteTokenQuantity = pendingTradeTx.GetQuoteTokenQuantity()
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pendingTradeTx_quoteTokenQuantity = " + str(pendingTradeTx_quoteTokenQuantity))
        quoteToken_original, baseToken_original = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(pendingTradeTx.GetTokensBeingTraded())
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteToken_original = " + str(quoteToken_original))
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "baseToken_original = " + str(baseToken_original))

        # Iterate over the forTailgate_pricesDict so we can update the price data related to this tailgate
        # Update the ratesGraph for each index_string

        # Iterate twice, inverting the tokens on the second pass
        for i in range(2):
            if i == 0:
                # Using quoteToken as the quoteToken and baseToken as the baseToken
                quoteToken = quoteToken_original
                # The base token must be an ERC20
                baseToken = EnforceTokenAddressIsERC20Version(baseToken_original).lower()
            elif i == 1:
                # Using baseToken as the quoteToken and quoteToken as the baseToken
                quoteToken = baseToken_original
                # The base token must be an ERC20
                baseToken = EnforceTokenAddressIsERC20Version(quoteToken_original).lower()
            else:
                raise Exception("You broke the loop, this should only iterate twice")

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Iterating with quoteToken as " + str(quoteToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Iterating with baseToken as " + str(baseToken))

            if quoteToken.lower() not in forTailgate_pricesDict[pdk_quoteTokens]:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   quoteToken " + str(quoteToken) + " NOT in the forTailgate_pricesDict for " + str(pendingTradeTx.exchangeName))
                continue

            if baseToken.lower() not in forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens]:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   baseToken " + str(baseToken) + " NOT in the forTailgate_pricesDict for " + str(pendingTradeTx.exchangeName))
                continue

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Both quoteToken and baseToken were found in forTailgate_pricesDict")

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   baseToken found in the forTailgate_pricesDict for " + str(pendingTradeTx.exchangeName))
            # Let's just assume that if the first index is valid then we got valid data
            # price_ask_isValid = False
            # price_bid_isValid = False
            forLoopLength = int(len(forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens][baseToken][pdk_prices]))
            for index in range(0, forLoopLength):
                index_string = str(index)
                if index_string in forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens][baseToken][pdk_prices]:
                    quoteTokensToSpend_etherUnits = forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens][baseToken][
                        pdk_prices][index_string][pdk_quoteTokensToSpend_etherUnits]
                    # our tailgating tx should only consider trading less quoteTokens than the pendingTradeTx that we're tailgating traded
                    # It should not be possible to profitably trade any amount greater unless we're tailgating multiple trades at once with the same exact gas price
                    if quoteTokensToSpend_etherUnits <= pendingTradeTx_quoteTokenQuantity:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   considering trading " + str(
                            quoteTokensToSpend_etherUnits) + " quoteTokens because it's less than the pendingTradeTx_quoteTokenQuantity of " + str(
                            pendingTradeTx_quoteTokenQuantity))

                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   pendingTradeTx.priceExpected_afterTxMinesIn = " + str(pendingTradeTx.priceExpected_afterTxMinesIn))
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   pendingTradeTx.side = " + str(pendingTradeTx.side))

                        # Set metaData
                        metaData_pendingTradeTx = pendingTradeTx.GetPricesDictMetaData()
                        forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens][baseToken][pdk_metaData] = \
                            metaData_pendingTradeTx

                        # Set the index_string key (for pendingTradeTx.exchangeName) if it doesn't already exist
                        if index_string not in forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens][baseToken][pdk_prices]:
                            forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string] = {}

                        if Libraries.defaults.VerboseLogging_Tailgating_PricesDictAndRatesGraphs:
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   copy_ratesGraph_Dict[blockNumber][index_string] before of len " +
                                                 str(len(copy_ratesGraph_Dict[blockNumber][index_string])) + " = " +
                                                 str(copy_ratesGraph_Dict[blockNumber][index_string]))
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   copy_ratesGraph_Dict_ExchangeNames[blockNumber][index_string] before of len " +
                                                 str(len(copy_ratesGraph_Dict_ExchangeNames[blockNumber][index_string])) +
                                                 " = " + str(copy_ratesGraph_Dict_ExchangeNames[blockNumber][index_string]))
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   copy_ratesGraph_Dict_QuoteTokens[blockNumber][index_string] before of len " +
                                                 str(len(copy_ratesGraph_Dict_QuoteTokens[blockNumber][index_string])) +
                                                 " = " + str(copy_ratesGraph_Dict_QuoteTokens[blockNumber][index_string]))

                        # Update the ratesGraph and pricesDict
                        price_ask_pendingTradeTx = pendingTradeTx.GetPrice_AfterTxMinesIn_GivenQuoteTokenAmount('buy', quoteTokensToSpend_etherUnits)
                        price_bid_pendingTradeTx = pendingTradeTx.GetPrice_AfterTxMinesIn_GivenQuoteTokenAmount('sell', quoteTokensToSpend_etherUnits)
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   price_ask_pendingTradeTx = " + str(price_ask_pendingTradeTx))
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   price_bid_pendingTradeTx = " + str(price_bid_pendingTradeTx))

                        # Always update the ratesGraph and prices dict no matter what because the goal is to tailgate here and not worry about existing arbitrages
                        price_bid_pendingTradeTx_toUse = None
                        price_ask_pendingTradeTx_toUse = None
                        exchangeName_bid_toUse = None
                        exchangeName_ask_toUse = None
                        if Libraries.core.IsBuy(pendingTradeTx.side):
                            # The user is buying on this exchange, which means I want to tailgate by selling on this exchange
                            # I sell to the bid
                            price_bid_pendingTradeTx_toUse = price_bid_pendingTradeTx
                            exchangeName_bid_toUse = pendingTradeTx.exchangeName
                            # Leave the other one as None because I should not consider arbitraging on that side
                            price_ask_pendingTradeTx_toUse = None
                            exchangeName_ask_toUse = None

                            # Update pricesDict
                            forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][
                                pdk_bid] = price_bid_pendingTradeTx

                        elif Libraries.core.IsSell(pendingTradeTx.side):
                            # The user is selling on this exchange, which means I want to tailgate by buying on this exchange
                            # I want to buy from the ask
                            price_ask_pendingTradeTx_toUse = price_ask_pendingTradeTx
                            exchangeName_ask_toUse = pendingTradeTx.exchangeName
                            # Leave the other one as None because I should not consider arbitraging on that side
                            price_bid_pendingTradeTx_toUse = None
                            exchangeName_bid_toUse = None

                            # Update pricesDict
                            forTailgate_pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][pendingTradeTx.exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][
                                pdk_ask] = price_ask_pendingTradeTx

                        else:
                            raise Exception("not buy or sell?")

                        Libraries.ratesGraph.ForceUpdateNodeInRatesGraph(
                            quoteToken, baseToken, price_bid_pendingTradeTx_toUse, price_ask_pendingTradeTx_toUse,
                            exchangeName_bid_toUse, exchangeName_ask_toUse,
                            copy_ratesGraph_Dict[blockNumber][index_string],
                            copy_ratesGraph_Dict_ExchangeNames[blockNumber][index_string],
                            copy_ratesGraph_Dict_QuoteTokens[blockNumber][index_string])

                        if Libraries.defaults.VerboseLogging_Tailgating_PricesDictAndRatesGraphs:
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   copy_ratesGraph_Dict[blockNumber][index_string] after of len " +
                                                 str(len(copy_ratesGraph_Dict[blockNumber][index_string])) + " = " +
                                                 str(copy_ratesGraph_Dict[blockNumber][index_string]))
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   copy_ratesGraph_Dict_ExchangeNames[blockNumber][index_string] after of len " +
                                                 str(len(copy_ratesGraph_Dict_ExchangeNames[blockNumber][index_string])) +
                                                 " = " + str(copy_ratesGraph_Dict_ExchangeNames[blockNumber][index_string]))
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   copy_ratesGraph_Dict_QuoteTokens[blockNumber][index_string] after of len " +
                                                 str(len(copy_ratesGraph_Dict_QuoteTokens[blockNumber][index_string])) +
                                                 " = " + str(copy_ratesGraph_Dict_QuoteTokens[blockNumber][index_string]))

        if Libraries.defaults.VerboseLogging_Tailgating_PricesDictAndRatesGraphs:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "forTailgate_pricesDict after = " + str(forTailgate_pricesDict))

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Calling ArbitrageGeneric on " + str(len(pendingTradeTxList)) + " internal trades within one tailgating tx")

    configName = "TailgateMemPool"
    reservableEthereumAccountPool = CreateNewReservableEthereumAccountPoolList(
        configName, Libraries.ninjaOps.NinjaOpConfig_NinjaGeneric.ninjaOpList)

    # We can safely assume the gas prices are the same for all internal trades within this tx
    gasPrice_tailgating = pendingTradeTxList[0].gasPrice_wei
    ArbitrageGeneric(header_logging, functionsStartDateTime, forTailgate_pricesDict, gasPrice_tailgating,
                     reservableEthereumAccountPool, blockNumber, [], False, TradingTechnique.Tailgate, pendingTradeTxList,
                     copy_ratesGraph_Dict, copy_ratesGraph_Dict_ExchangeNames, copy_ratesGraph_Dict_QuoteTokens)


def IsUniswapDeadlineValid(deadline):
    deadline_dateTimeUtc = Libraries.core.ConvertUnixEpochToDateTime_GivenUnixEpochInSeconds(deadline)
    # PrintAndLog("IsUniswapDeadlineValid with deadline = " + str(deadline) + ". Comparing deadline_dateTimeUtc = " + str(
    #     deadline_dateTimeUtc) + " with datetime.datetime.utcnow() = " + str(datetime.datetime.utcnow()))

    # If the current time has surpassed the deadline
    if datetime.datetime.utcnow() > deadline_dateTimeUtc:
        return False
    else:
        return True


def MakeSureFunctionSelectorDictsAreCached():
    from Contracts.contracts import Contract_UniswapV2_Router

    global FunctionSelectorDict_UniswapV2_Router
    global FunctionNameDict_UniswapV2_Router

    if not FunctionSelectorDict_UniswapV2_Router or not FunctionNameDict_UniswapV2_Router:
        # Optimization, i'm doing this once and storing here so I don't have to waste cycles on doing this thousands of times
        FunctionSelectorDict_UniswapV2_Router, FunctionNameDict_UniswapV2_Router = \
            Libraries.signingUtils.GetFunctionSelectorsDictForAllFunctionsInContract(Contract_UniswapV2_Router)
        # PrintAndLog_FuncNameHeader("Successfully set FunctionSelectorDict_UniswapV2_Router = " + str(FunctionSelectorDict_UniswapV2_Router))
        # PrintAndLog_FuncNameHeader("Successfully set FunctionNameDict_UniswapV2_Router = " + str(FunctionNameDict_UniswapV2_Router))
