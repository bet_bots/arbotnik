import sys
import traceback
import json
import random
import string
import time
import threading
import datetime
from random import randint
from web3 import Web3
from eth_abi import encode_single
from enum import Enum
from threading import Lock
import copy

from Exchanges.ninja_arbitrage import TradingTechnique
from Libraries.accounts import DedicatedTxPredictionAccount
from Libraries.exchanges import PayloadIdName, GetPayload_Uniswap_TokenBalance, GetPayload_Uniswap_EtherBalance, GetPayload_Kyber_GetExpectedRate, \
    GetPayloadResultDict_FromResultOf_GetManyArbitrages_New, GetValueFromPayloadResultDict_UniswapEtherBalance_Value, \
    GetValueFromPayloadResultDict_UniswapTokenBalance_Value, GetPayload_Kyber_GetConversionRate, GetValueFromPayloadResultDict_UniswapEtherBalance, \
    GetValueFromPayloadResultDict_UniswapTokenBalance, GetValueFromPayloadResultDict_UniswapEtherBalance_QuoteToken, \
    GetValueFromPayloadResultDict_UniswapTokenBalance_QuoteToken, GetValueFromPayloadResultDict_UniswapEtherBalance_BaseToken, \
    GetValueFromPayloadResultDict_UniswapTokenBalance_BaseToken, ExchangeName_Kyber, ExchangeName_Uniswap, ExchangeName_Set, ExchangeName_0xMesh
from Libraries.executeOnInterval import IsTimeToExecute
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog, PrintAndLog_FuncNameHeader
import Libraries.defaults
from Libraries.transactions import TransactionType
from Libraries.transactions import API_SendEther_ToContract
from Libraries.pricesDict import *
from Libraries.readySetGo import ReadySetGoState
import Libraries.core
import Libraries.nodes
import Libraries.gasToken_CHI
import Libraries.utils
import Libraries.priceOracle
import Libraries.gasStation
import Libraries.market
import Libraries.signingUtils
import Libraries.ninjaEncoding
import Libraries.cache
import Libraries.relayProxy

import Exchanges.bancor
import Exchanges.kyber
import Exchanges.set
import Exchanges.zrxV2
import Exchanges.uniswap
import Exchanges.keeperDAO

# V1 Old contract worked well, works only with ninjaop1 account
# Contract_Ninja = Libraries.nodes.Instance_Web3.toChecksumAddress("0x5179bc7b6e90719b17cea1feb302d1c5208a1bfa")
# V2 works with many ninja accounts
# Contract_Ninja = Libraries.nodes.Instance_Web3.toChecksumAddress("0x45f81a44a815e7326db6b9040d27b3a3064bca6b")
# V3 WORKS! In production September 2019
# Contract_Ninja = Libraries.nodes.Instance_Web3.toChecksumAddress("0x1b6810350bfbe11f1fb3c91c3bf4c50807a21b36")
# V4 contract in testing NEVER DEPLOYED
# Contract_Ninja = Libraries.nodes.Instance_Web3.toChecksumAddress("0x3dc85f6244a530f3c358516745861eb4628ee9be")

# Ninja_Set_3
# Contract_Ninja_Set = Libraries.nodes.Instance_Web3.toChecksumAddress("0x7f0dbd886bbe7bb18694cf5e7a8b7911e4cf7435")
# Ninja_Set_4
# Contract_Ninja_Set = Libraries.nodes.Instance_Web3.toChecksumAddress("0x5420cdfd805f52342982867fc2d6c40ff5166bb2")
# # Ninja_Set_8
# Contract_Ninja = Libraries.nodes.Instance_Web3.toChecksumAddress("0x52fa03cd9bd4960bbc09e3c243cd77b17c4a1fde")

# # Ninja_Utility_3  # Had the this.address bug in it. Should have been msg.sender
# Contract_NinjaUtility = Libraries.nodes.Instance_Web3.toChecksumAddress("0x56cdde5d251138d9b37d3f7f0233d2c3743d4e5e")

# Ninja_A_10  # Working
# Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0x60267829fcf7cf880f8bc8eae5f546cdb107ec1a")
# Ninja_A_11  # Deprecated, had bugs
# Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0x6f66bf1390ea7e2659e2ee765b3f73cc56618736")
# # Ninja_A_11  # Testing
# Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0x39b132966938a0d8b95226e473da4f18f2a50c93")
# # Ninja_A_11_Fixing1  # Testing
# Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0x8340013eed0d4809e87cffea95e8fe72fa2f42bc")
# Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0xf5208a0bbe400db3595f67e8b2c3e493394f1b1e")
# Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0xec1a7995867ecdebff7990312c1c048249b6049a")
# # Ninja_A_12  # Working
# Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0x8291f52a87ccb13a8c27f2b1b7c2b0011dc5d5dd")
# Ninja_A_13  # Working
# Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0x6d5fe1342745230063be26117abb4612d9876ba5")
# Ninja_A_14  # Testing
Contract_Ninja_A = Libraries.nodes.Instance_Web3.toChecksumAddress("0x498aa5830da490c85dc4d92b410aab86edb26c17")

# Ninja_B_10  # Working
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x89e99fe0b3b288c3553e3d3f343a3c4a56f1527e")
# Ninja_B_11  # Working with bugs
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0xa92167802c4d41e116339ffcc5cad26e793cb6ea")
# Ninja_B_12  # Testing, found one issue
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x980f15adac0fc8a2010399297f9d90eb28cdf714")
# Ninja_B_12  # Testing, found another issue
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x322c8025cc9225edcfca1516ed1d76474a926ef6")
# Ninja_B_12  # Testing, found one issue with dust
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x28860b1b9b75463e39d8b414f6ce3d8e337e419c")
# Ninja_B_12  # Testing
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x1f6c07360ae001498ba4cef468c6d763080202ca")
# Ninja_B_12  # still having code 5's for 0x-set trades when I shouldn't
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x712a63947fb1ecae4658b3908247debf18c5014a")
# Ninja_B_12  # Testing
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x20c66f6145e922a59f2291eb9e92634e26c47472")
# Ninja_B_13  # Working, but does not have cToken support
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0xc5df5a9f4b275c72f91e0c84ab145e2014fb049d")
# Ninja_B_14  # Working, has some flaws
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x7e29dfe13d6b77fc90a18a89dfd323eaa30af63e")
# Ninja_B_14  # Testing, bug found
# Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0xd49e1117f31aaf20fa7a70e684d32bd54b9e09a2")
# Ninja_B_14  # Testing, now checking for 0x order maker balance, set convert inflowTokenQuantity to shares much more efficient
Contract_Ninja_B = Libraries.nodes.Instance_Web3.toChecksumAddress("0x13baee76236fe29ff843ec96c16a06ef3db9383d")

# Ninja_C_12  # Testing, bugs found
# Contract_Ninja_C = Libraries.nodes.Instance_Web3.toChecksumAddress("0x205f2712060d7daa5c3468d6d3b77b53f7fa1c6e")
# Ninja_C_12  # Testing
Contract_Ninja_C = Libraries.nodes.Instance_Web3.toChecksumAddress("0xb808fdc5b1ed61557fd9167a5b64851016398af5")

# Ninja_Utility_4  # Working
# Contract_NinjaUtility = Libraries.nodes.Instance_Web3.toChecksumAddress("0xff13b099b8e8a3c17f0dd7537593a36de9c35cc4")
# Ninja_Utility_5  # Testing
# Contract_NinjaUtility = Libraries.nodes.Instance_Web3.toChecksumAddress("0xee2a9329ecc07aaabdf34126d72678a41d0417b7")
# Ninja_Utility_6  # Testing
# Contract_NinjaUtility = Libraries.nodes.Instance_Web3.toChecksumAddress("0xc6099178c71ad0ab6ddc4ac826249bfd0ea3de2d")
# Ninja_Utility_7  # Testing
Contract_NinjaUtility = Libraries.nodes.Instance_Web3.toChecksumAddress("0x5307490b81626dba7ccbe0743d0a3dbec5e9c0cf")

NinjaInstance_A = None
NinjaInstance_B = None
NinjaInstance_C = None
NinjaInstance_Generic = None
NinjaObjectsList = []

SupportedQuoteTokensList_Ninja_B = None
SupportedQuoteTokensList_Ninja_C = None

# Destroy 1 contract per 40,787 gas expected to use in the TX
KillOneContractPerThisManyUnitsOfGasExpectedToSpendInTx = 40787

LastDateTime_NinjaTradeReverted = datetime.datetime.now() - datetime.timedelta(days=1)

# LastBlockNumberRan_NinjaBetweenForFunctionsThatOnlyRunWhenNewBlockIsMined = None

# Villain Ninjas
# 0x85c5c26dc2af5546341fc1988b9d178148b4838b
# 0x8018280076d7fa2caa1147e441352e8a89e1ddbe

# Dict of HistoricTrades and keyed by txHash
HistoricTradeDict = {}
Lock_HistoricTradeDict = Lock()

# pricesDicts keyed by block number
PricesDictDict = {}

GasTokenQuantity_etherUnits = None
BurnGasTokenThreshold_wei = None


# TODO REMOVEME
# PriceOracleDict = {}


class NinjaType(Enum):
    A = "A - Uniswap-Kyber, Uniswap-0x, Kyber-0x"
    B = "B - Set, ETH/WETH only as quote token"
    C = "C - Set, any specified ERC20 as quote token"
    Generic = "Generic, accept enabled exchange and token"


class Ninja:
    ninjaType = None
    contractAddress_Ninja = None
    contractObject_Ninja = None
    contractAddress_Utility = None
    contractObject_Utility = None
    supportedQuoteTokensList = None

    # Ninja contract balances
    quoteTokenBalanceDict_etherUnits = None
    # TODO REMOVEME I do not need to lock this
    # lock_quoteTokenBalanceDict_etherUnits = None

    # KeeperDAO LP contract balances
    keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits = None
    # TODO REMOVEME I do not need to lock this
    # lock_keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits = None

    # Rules for Set so this ninja knows which sets it can trade against and which it cannot
    # Set is very complex and ninjas can only trade against sets which it's designed to trade against
    # (example, some require WETH to be apart of the set)
    excludedTokenList_Set = None
    requiredTokenList_Set = None

    def __init__(self, _ninjaType, _contractAddress_Ninja, _contractObject, _contractAddress_Utility,
                 _contractObject_Utility, _supportedQuoteTokensList, _exchangeNamePairList):
        self.ninjaType = _ninjaType
        self.contractAddress_Ninja = _contractAddress_Ninja
        self.contractObject_Ninja = _contractObject
        self.contractAddress_Utility = _contractAddress_Utility
        self.contractObject_Utility = _contractObject_Utility
        self.supportedQuoteTokensList = _supportedQuoteTokensList
        self.exchangeNamePairList = _exchangeNamePairList

        PrintAndLog_FuncNameHeader("self.supportedQuoteTokensList = " + str(self.supportedQuoteTokensList))
        # Make sure the lists within this list are sorted alphabetically
        PrintAndLog_FuncNameHeader("self.exchangeNamePairList before sorting = " + str(self.exchangeNamePairList))
        for sublist in self.exchangeNamePairList:
            sublist.sort()

        PrintAndLog_FuncNameHeader("self.exchangeNamePairList after sorting = " + str(self.exchangeNamePairList))

        self.excludedTokenList_Set = []
        self.requiredTokenList_Set = []

        # Init balances to zero, will get balance and overwrite this late
        self.quoteTokenBalanceDict_etherUnits = {}
        # self.lock_quoteTokenBalanceDict_etherUnits = Lock()

        for quoteToken in self.supportedQuoteTokensList:
            if not quoteToken:
                raise Exception("quoteToken inside of supportedQuoteTokensList for " + str(self.ninjaType) + " was None, was something not initialized?")

            # Init balances to zero, will get balance and overwrite this later
            self.quoteTokenBalanceDict_etherUnits[quoteToken.lower()] = 0

        # Init balances to zero, will get balance and overwrite this later
        self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits = {}
        # self.lock_keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits = Lock()

        for quoteToken in self.supportedQuoteTokensList:
            if not quoteToken:
                raise Exception("quoteToken inside of supportedQuoteTokensList for " + str(self.ninjaType) + " was None, was something not initialized?")

            # Init balances to zero, will get balance and overwrite this later
            self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits[quoteToken.lower()] = 0

    def SetSetsTokenList_RequiredTokenList(self, requiredTokenList):
        self.requiredTokenList_Set = requiredTokenList

    def SetSetsTokenList_ExcludedTokenList(self, excludedTokenList):
        self.excludedTokenList_Set = excludedTokenList

    def UpdateBalances(self):
        # ****** Begin Ninja contract balances ****** #

        # Get the ether balance
        etherBalance_etherUnits = Libraries.core.API_GetEtherBalance(self.contractAddress_Ninja)

        # self.lock_quoteTokenBalanceDict_etherUnits.acquire()
        # try:
        self.quoteTokenBalanceDict_etherUnits[Exchanges.keeperDAO.EthTokenContract.lower()] = etherBalance_etherUnits
        #
        # finally:
        #     self.lock_quoteTokenBalanceDict_etherUnits.release()

        # PrintAndLog_FuncNameHeader("ninjaType = " + str(self.ninjaType) + " and contractAddress_Ninja = " + str(self.contractAddress_Ninja))

        # Update all token balances
        # PrintAndLog_FuncNameHeader("self.quoteTokenBalanceDict_etherUnits = " + str(self.quoteTokenBalanceDict_etherUnits))
        for quoteToken in self.quoteTokenBalanceDict_etherUnits:
            try:
                # PrintAndLog_FuncNameHeader("Getting balance for quoteToken " + str(quoteToken) + " " + str(
                #     Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(quoteToken)))

                decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteToken)))

                if quoteToken.lower() == Exchanges.keeperDAO.EthTokenContract.lower():
                    # Special case
                    tokenBalance_etherUnits = Libraries.core.API_GetEtherBalance(self.contractAddress_Ninja)
                else:
                    # It's a normal token, just get the token balance
                    tokenBalance_etherUnits = Libraries.core.API_GetTokenBalance(self.contractAddress_Ninja, quoteToken, decimals_quoteToken)

                self.quoteTokenBalanceDict_etherUnits[quoteToken] = tokenBalance_etherUnits

                # PrintAndLog_FuncNameHeader("self.quoteTokenBalanceDict_etherUnits's quoteToken " + str(
                #     Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(quoteToken)) + " tokenBalance_etherUnits = " + str(
                #     tokenBalance_etherUnits) + " for ninjaType = " + str(self.ninjaType) + " and contractAddress_Ninja = " + str(self.contractAddress_Ninja))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                message = "exception when getting balance for quoteToken " + str(quoteToken) + " " + str(traceback.format_exc())
                PrintAndLogError(message)
                raise

        # ****** End Ninja contract balances ****** #

        # ****** Begin KeeperDAOLP contract balances ****** #

        # Update all token balances
        # PrintAndLog_FuncNameHeader("self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits = " + str(self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits))
        for quoteToken in self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits:
            try:
                # PrintAndLog_FuncNameHeader("Getting balance for quoteToken " + str(quoteToken) + " " + str(
                #     Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(quoteToken)))

                # Check to see if this quoteToken exists in KeeperDAO
                if quoteToken.lower() not in Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict():
                    PrintAndLogError("Could not find quoteToken " + str(quoteToken) +
                                     " in Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict(). Assuming a zero balance")
                    self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits[quoteToken] = 0
                else:
                    keeperDAOLPContract = Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict()[quoteToken]

                    decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteToken)))

                    # Get the token balance available to be borrowed
                    tokenBalance_etherUnits = Exchanges.keeperDAO.API_GetBorrowableBalance(keeperDAOLPContract, quoteToken)

                    self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits[quoteToken] = tokenBalance_etherUnits

                    # PrintAndLog_FuncNameHeader("self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits's quoteToken " + str(
                    #     Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(quoteToken)) + " tokenBalance_etherUnits = " + str(
                    #     tokenBalance_etherUnits) + " for ninjaType = " + str(self.ninjaType) + " and keeperDAOLPContract.address = " + str(keeperDAOLPContract.address))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                message = "exception when getting balance for quoteToken " + str(quoteToken) + " " + str(traceback.format_exc())
                PrintAndLogError(message)

        # ****** End KeeperDAOLP contract balances ****** #

    def GetQuoteTokenBalance_Ninja(self, quoteToken):
        return self.quoteTokenBalanceDict_etherUnits[quoteToken.lower()]

    def GetQuoteTokenBalance_KeeperDAOLP(self, quoteToken):
        return self.keeperDAOLPBorrowableQuoteTokenBalanceDict_etherUnits[quoteToken.lower()]

    def API_WhitelistUsers(self, fromAddress, fromPrivateKey, userAddressList_toWhitelist, gasPrice, nonce=None):
        import Contracts.contracts

        PrintAndLog("Ninja:API_WhitelistUsers: fromAddress = " + str(fromAddress) + ", userAddressList_toWhitelist = " + str(userAddressList_toWhitelist))

        # function whitelistUsers (
        #         address[] memory users,
        #         bool value
        #         ) public onlyOwner

        kwargs = {
            'users': userAddressList_toWhitelist,
            'value': True,
        }
        PrintAndLog("kwargs = " + str(kwargs))

        data_hex = Contracts.contracts.Contract_Ninja_DiamondProxy.encodeABI('whitelistUsers', kwargs=kwargs)

        toAddress = self.contractAddress_Ninja
        gas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 gas, gasPrice, data_hex, TransactionType.other, nonce, False, True)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_WhitelistUsers: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_WhitelistUsers: no transactionId")

        return transactionId

    def API_BlacklistUsers(self, fromAddress, fromPrivateKey, userAddressList_toBlacklist, gasPrice, nonce=None):
        import Contracts.contracts

        PrintAndLog("Ninja:API_BlacklistUsers: fromAddress = " + str(fromAddress) + ", userAddressList_toBlacklist = " + str(userAddressList_toBlacklist))

        # function whitelistUsers (
        #         address[] memory users,
        #         bool value
        #         ) public onlyOwner

        kwargs = {
            'users': userAddressList_toBlacklist,
            'value': False,
        }
        PrintAndLog("kwargs = " + str(kwargs))

        data_hex = Contracts.contracts.Contract_Ninja_DiamondProxy.encodeABI('whitelistUsers', kwargs=kwargs)

        toAddress = self.contractAddress_Ninja
        gas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 gas, gasPrice, data_hex, TransactionType.other, nonce, False, True)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_BlacklistUsers: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_BlacklistUsers: no transactionId")

        return transactionId

    def API_WhitelistKeeperDAOBorrowProxy(self, fromAddress, fromPrivateKey, lpAddressList_toWhitelist, boolValue, gasPrice, nonce=None):
        import Contracts.contracts

        PrintAndLog("Ninja:API_WhitelistKeeperDAOBorrowProxy: fromAddress = " + str(fromAddress) + ", lpAddressList_toWhitelist = " + str(lpAddressList_toWhitelist))

        # function whitelistKeeperDAOLPPs (
        #         address[] memory users,
        #         bool value
        #         ) public onlyWhitelist
        #     {

        kwargs = {
            'users': lpAddressList_toWhitelist,
            'value': boolValue,
        }
        PrintAndLog("kwargs = " + str(kwargs))

        data_hex = Contracts.contracts.Contract_Ninja_Authentication_KeeperDAOLPPs.encodeABI('whitelistKeeperDAOLPPs', kwargs=kwargs)

        toAddress = self.contractAddress_Ninja
        gas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 gas, gasPrice, data_hex, TransactionType.other, nonce, False, True)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_WhitelistKeeperDAOBorrowProxy: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_WhitelistKeeperDAOBorrowProxy: no transactionId")

        return transactionId

    def API_BlacklistKeeperDAOLPs(self, fromAddress, fromPrivateKey, lpAddressList_toBlacklist, gasPrice, nonce=None):
        import Contracts.contracts

        PrintAndLog("Ninja:API_BlacklistKeeperDAOLPs: fromAddress = " + str(fromAddress) + ", lpAddressList_toBlacklist = " + str(lpAddressList_toBlacklist))

        # function whitelistKeeperDAOLPPs (
        #         address[] memory users,
        #         bool value
        #         ) public onlyWhitelist
        #     {

        kwargs = {
            'users': lpAddressList_toBlacklist,
            'value': False,
        }
        PrintAndLog("kwargs = " + str(kwargs))

        data_hex = Contracts.contracts.Contract_Ninja_DiamondProxy.encodeABI('whitelistKeeperDAOLPPs', kwargs=kwargs)

        toAddress = self.contractAddress_Ninja
        gas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 gas, gasPrice, data_hex, TransactionType.other, nonce, False, True)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_BlacklistKeeperDAOLPs: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_BlacklistKeeperDAOLPs: no transactionId")

        return transactionId

    def API_WithdrawEther(self, fromAddress, fromPrivateKey, amount_etherUnits, gas, gasPrice):
        import Contracts.contracts

        amount_weiUnits = Libraries.core.ConvertEtherToWei(amount_etherUnits, Libraries.core.Ether_Decimals)
        PrintAndLog("Ninja:API_WithdrawEther: fromAddress = " + str(fromAddress) + ", amount_weiUnits = " + str(amount_weiUnits))

        kwargs = {
            'amount': int(amount_weiUnits),
        }
        PrintAndLog("kwargs = " + str(kwargs))

        data_hex = Contracts.contracts.Contract_Ninja_AssetManager.encodeABI('withdrawEther', kwargs=kwargs)
        toAddress = self.contractAddress_Ninja
        estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
        PrintAndLog("estimatedGas = " + str(estimatedGas))

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 gas, gasPrice, data_hex, TransactionType.withdraw, None, False, True)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_WithdrawEther: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_WithdrawEther: no transactionId")

        return transactionId

    def API_WithdrawAllEther(self, fromAddress, fromPrivateKey, gas, gasPrice):
        import Contracts.contracts

        PrintAndLog("Ninja:API_WithdrawAllEther: fromAddress = " + str(fromAddress))
        data_hex = Contracts.contracts.Contract_Ninja_AssetManager.encodeABI('withdrawAllEther', kwargs={})
        toAddress = self.contractAddress_Ninja
        estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
        PrintAndLog("estimatedGas = " + str(estimatedGas))

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 gas, gasPrice, data_hex, TransactionType.withdraw, None, False, True)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_WithdrawAllEther: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_WithdrawAllEther: no transactionId")

        return transactionId

    def API_WithdrawAllTokens(self, fromAddress, fromPrivateKey, tokenAddress, gasPrice, nonce=None):
        # Get token balance, then withdraw all of it
        quantity_weiUnits = Libraries.core.API_GetTokenBalance(
            self.contractAddress_Ninja, tokenAddress, float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))),
            None, None, None, Libraries.core.RequestTimeout_seconds, None, Libraries.core.Units.Wei)
        # PrintAndLog("quantity_weiUnits = " + str(quantity_weiUnits))
        return self.API_WithdrawTokens(fromAddress, fromPrivateKey, tokenAddress, quantity_weiUnits, gasPrice, nonce)

    def API_WithdrawTokens(self, fromAddress, fromPrivateKey, tokenAddress, amount_weiUnits, gasPrice, nonce=None):
        import Contracts.contracts

        toAddress = self.contractAddress_Ninja
        PrintAndLog("Ninja:API_WithdrawTokens: fromAddress = " + str(fromAddress) + " toAddress = " + str(toAddress))
        kwargs = {
            'token': str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
            'amount': int(amount_weiUnits),
        }
        PrintAndLog("kwargs = " + str(kwargs))

        data_hex = Contracts.contracts.Contract_Ninja_AssetManager.encodeABI('withdrawToken', kwargs=kwargs)
        gas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
        PrintAndLog("Ninja:API_WithdrawTokens estimatedGas = " + str(gas))

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 gas, gasPrice, data_hex, TransactionType.withdraw, nonce, False, True)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_WithdrawTokens: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_WithdrawTokens: no transactionId")

        return transactionId

    def API_GetProxyOwner(self):
        import Contracts.contracts

        payload = {
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Ninja_DiamondProxy.encodeABI(
                        'owner_Proxy_ThisNameMustBeUniqueBecauseInTheoryItCouldGetOverridenByALogicContractHavingTheSameFunctionSelector', kwargs={}),
                    "to": Libraries.nodes.Instance_Web3.toChecksumAddress(self.contractAddress_Ninja),
                },
            ]
        }
        # PrintAndLog_FuncNameHeader("payload = " + str(payload))
        response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
        if response.ok:
            responseData = response.content
            jData = json.loads(responseData)
            # PrintAndLog_FuncNameHeader("jData = " + str(jData))
            return Libraries.core.GetAddressFromDataProperty(jData['result'])

        else:
            # If response code is not ok (200), print the resulting http error code with description
            PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
            response.raise_for_status()

    def API_SetAddresses(self, fromAddress, fromPrivateKey, gasPrice, nonce=None):
        import Contracts.contracts

        #     function setAddresses(
        #         address[] memory addressArray
        #         ) public onlyWhitelist
        #     {
        #         DiamondStorage_Properties storage ds = diamondStorage_Properties();
        #         uint256 i = 0;
        #
        #         ds.kyberProxyContract = addressArray[i++];
        #         ds.wethContract = addressArray[i++];
        #         ds.zrxV3ExchangeContract = addressArray[i++];
        #         ds.kyberEthTokenContract = addressArray[i++];
        #         ds.keeperDaoEthTokenContract = addressArray[i++];
        #         ds.keeperDaoLppContract_Eth = address(uint160(addressArray[i++]));
        #         ds.ninjaProxyContract = addressArray[i++];
        #     }

        addressArray = [
            Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
            Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.zrxV2.Contract_WETH),
            Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.zrxV2.Contract_Exchange),
            Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
            Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.keeperDAO.EthTokenContract),
            Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.keeperDAO.GetLiquidityProviderPoolContractDict()[Exchanges.keeperDAO.EthTokenContract.lower()].address),
            Libraries.nodes.Instance_Web3.toChecksumAddress(Contracts.contracts.Contract_Ninja_DiamondProxy.address),
        ]

        kwargs = {
            'addressArray': addressArray,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = Contracts.contracts.Contract_Ninja_Properties.encodeABI('setAddresses', kwargs=kwargs)
        # PrintAndLog("data_hex: " + str(data_hex))
        toAddress = self.contractAddress_Ninja
        estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 estimatedGas, gasPrice, data_hex, TransactionType.approve, nonce)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_TestProxy_LogicContract_SetAddresses: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_TestProxy_LogicContract_SetAddresses: no transactionId")

        return transactionId

    def API_SetInts(self, fromAddress, fromPrivateKey, gasPrice, nonce=None):
        import Contracts.contracts

        # function setInts(
        #         uint256[] memory intArray
        #         ) public onlyWhitelist
        #     {
        #         DiamondStorage_Properties storage ds = diamondStorage_Properties();
        #         uint256 i = 0;
        #
        #         ds.doLogEvents = intArray[i++];
        #         ds.killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx = intArray[i++];
        #         ds.gasPriceThresholdForSpendingGasTokens = intArray[i++];
        #         ds.minQuoteTokenAllowedToSpendDivider = intArray[i++];
        #         ds.divider_profitToShareWithKeeperDAO = intArray[i++];
        #     }

        doLogEvents = 1
        killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx = 36622
        # gasPriceThresholdForSpendingGasTokens = Libraries.gasStation.ConvertGweiToWei(2.44)
        # gasPriceThresholdForSpendingGasTokens = Libraries.gasStation.ConvertGweiToWei(5.04)
        gasPriceThresholdForSpendingGasTokens = Libraries.gasStation.ConvertGweiToWei(600)
        minQuoteTokenAllowedToSpendDivider = 3

        intArray = [
            int(doLogEvents),
            int(killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx),
            int(gasPriceThresholdForSpendingGasTokens),
            int(minQuoteTokenAllowedToSpendDivider),
            int(Exchanges.keeperDAO.Divider_profitToShareWithKeeperDAO),
        ]

        kwargs = {
            'intArray': intArray,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = Contracts.contracts.Contract_Ninja_Properties.encodeABI('setInts', kwargs=kwargs)
        toAddress = self.contractAddress_Ninja
        estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 estimatedGas, gasPrice, data_hex, TransactionType.approve, nonce)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_TestProxy_LogicContract_SetInts: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_TestProxy_LogicContract_SetInts: no transactionId")

        return transactionId

    def API_GetAddresses(self):
        import Contracts.contracts

        # function getAddresses(
        #         ) public view returns (address[] memory addressArray)

        payload = {
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Ninja_Properties.encodeABI('getAddresses', kwargs={}),
                    "to": Libraries.nodes.Instance_Web3.toChecksumAddress(self.contractAddress_Ninja),
                },
            ]
        }
        # PrintAndLog_FuncNameHeader("payload = " + str(payload))
        response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
        if response.ok:
            responseData = response.content
            jData = json.loads(responseData)
            # PrintAndLog_FuncNameHeader("jData = " + str(jData))

            dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(
                Libraries.core.Remove0XfromHexString(jData['result']), Libraries.core.LengthOfDataProperty)
            # We dont' care about the first two items
            dataList.pop(0)
            dataList.pop(0)
            return dataList

        else:
            # If response code is not ok (200), print the resulting http error code with description
            PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
            response.raise_for_status()

    def API_GetInts(self):
        import Contracts.contracts

        # function getInts(
        #     ) public view returns (uint256[] memory intArray)

        payload = {
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Ninja_Properties.encodeABI('getInts', kwargs={}),
                    "to": Libraries.nodes.Instance_Web3.toChecksumAddress(self.contractAddress_Ninja),
                },
            ]
        }
        # PrintAndLog_FuncNameHeader("payload = " + str(payload))
        response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
        if response.ok:
            responseData = response.content
            jData = json.loads(responseData)
            # PrintAndLog_FuncNameHeader("jData = " + str(jData))
            dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(jData['result']),
                                                                                 Libraries.core.LengthOfDataProperty)
            # We dont' care about the first two items
            dataList.pop(0)
            dataList.pop(0)
            return dataList

        else:
            # If response code is not ok (200), print the resulting http error code with description
            PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
            response.raise_for_status()

    def GetAddressPropertyNameList(self):
        return [
            'kyberProxyContract',
            'wethContract',
            'zrxV3ExchangeContract',
            'kyberEthTokenContract',
            'keeperDaoEthTokenContract',
            'keeperDaoLppContract_Eth',
            'ninjaProxyContract',
        ]

    def GetIntPropertyNameList(self):
        return [
            'doLogEvents',
            'killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx',
            'gasPriceThresholdForSpendingGasTokens',
            'minQuoteTokenAllowedToSpendDivider',
            'divider_profitToShareWithKeeperDAO',
        ]

    # def API_SetIntProperties(self, fromAddress, fromPrivateKey,
    #                          killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx,
    #                          gasPriceThresholdForSpendingGasTokens, maxTradeLossAcceptable_etherUnits,
    #                          minEtherToSpendAllowedDivider, gasPrice):
    #     PrintAndLog("Ninja:API_SetIntProperties: fromAddress = " + str(fromAddress))
    #
    #     # function setInts(
    #     #         uint _killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx,
    #     #         uint _gasPriceThresholdForSpendingGasTokens,
    #     #         uint _maxTradeLossAcceptable,
    #     #         uint _minEtherToSpendAllowedDivider
    #     #         ) public onlyOwnerOrWhitelist
    #
    #     maxTradeLossAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxTradeLossAcceptable_etherUnits, Libraries.core.Ether_Decimals)
    #
    #     kwargs = {
    #         '_killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx': int(killOneContractPerThisManyUnitsOfGasExpectedToSpendInTx),
    #         '_gasPriceThresholdForSpendingGasTokens': int(gasPriceThresholdForSpendingGasTokens),
    #         '_maxTradeLossAcceptable': int(maxTradeLossAcceptable_weiUnits),
    #         '_minEtherToSpendAllowedDivider': int(minEtherToSpendAllowedDivider),
    #     }
    #     PrintAndLog("kwargs = " + str(kwargs))
    #     # Build binary representation of the function call with arguments
    #     # Convert this hex string to a hex byte array
    #     data_hex = self.contractObject_Ninja.encodeABI('setInts', kwargs=kwargs)
    #     # PrintAndLog("data_hex: " + str(data_hex))
    #     toAddress = self.contractAddress_Ninja
    #     gas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    #
    #     transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
    #                                              gas, gasPrice, data_hex, TransactionType.other)
    #     # Trash the fromPrivateKey in memory
    #     fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))
    #
    #     if transactionId:
    #         message = "Ninja:API_SetIntProperties: transactionId: " + transactionId
    #         PrintAndLog(message)
    #     else:
    #         PrintAndLog("Failed to Ninja:API_SetIntProperties: no transactionId")
    #
    #     return transactionId

    def API_GetGasTokenQuantity(self):
        gasTokenContract = Libraries.gasToken_CHI.Contract_CHI
        return Libraries.core.API_GetTokenBalance(
            self.contractAddress_Ninja, gasTokenContract,
            "1e" + str(Libraries.core.GetDecimalsForTokenContract(gasTokenContract)))

    def API_ConvertInflowTokenQuantityToSharesQuantity(self, setTokenData, inflowTokensToSpend_weiUnits):
        PrintAndLog_FuncNameHeader("with set = " + str(setTokenData.setTokenKey) + " and inflowTokensToSpend_weiUnits = " + str(inflowTokensToSpend_weiUnits))
        # function convertInflowTokenQuantityToSharesQuantity(
        #         bool useCTokenBidderContract,
        #         address auctionModule,
        #         address setTokenAddress,
        #         uint256 inflowTokensToSpend,
        #         uint256 minBidSize
        #         ) public view returns (uint256)
        #     {

        # baseTokenAddress, useSetCTokenBidderContract = setTokenData.GetBaseTokenAddress()

        # kwargs = {
        #     'useCTokenBidderContract': useSetCTokenBidderContract,
        #     'auctionModule': setTokenData.DetermineRebalanceAuctionModule(),
        #     'setTokenAddress': setTokenData.setAddress,
        #     'inflowTokensToSpend': int(inflowTokensToSpend_weiUnits),
        #     'minBidSize': int(setTokenData.minBidSize_weiUnits),
        # }

        # function convertInflowTokenQuantityToSharesQuantity(
        #         uint256 inflowTokensToSpend,
        #         uint256 magicConverter_inflowTokensToShares
        #         ) public pure returns (uint256 shares)

        kwargs = {
            'inflowTokensToSpend': int(inflowTokensToSpend_weiUnits),
            'minBidSize': int(setTokenData.minBidSize_weiUnits),
            'magicConverter_inflowTokensToShares': int(setTokenData.magicConverter_inflowTokensToShares_weiUnits),
        }
        PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))

        payload = {
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": self.contractObject_Ninja.encodeABI('convertInflowTokenQuantityToSharesQuantity', kwargs=kwargs),
                    "to": self.contractAddress_Ninja
                },
            ]
        }
        response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
        if response.ok:
            responseData = response.content
            jData = json.loads(responseData)
            PrintAndLog_FuncNameHeader("jData = " + str(jData))
            return Libraries.core.ConvertHexToInt(jData['result'])

        else:
            # If response code is not ok (200), print the resulting http error code with description
            PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
            response.raise_for_status()

    def PrintProperties(self):
        from Libraries.accounts import NinjaOpAccountDict

        self.UpdateBalances()
        intsList = self.API_GetInts()
        addressesList = self.API_GetAddresses()
        gasTokenQuantity = self.API_GetGasTokenQuantity()

        # Show relevant token balances
        relevantTokenList = [
            '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2',
            '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48',
            '0x6b175474e89094c44da98b954eedeac495271d0f',
            '0xdac17f958d2ee523a2206206994597c13d831ec7',
            '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599',
            '0xeb4c2781e4eba804ce9a9803c67d0893436bb27d',
        ]
        tokenTupleList = []

        for tokenAddress in relevantTokenList:
            tokenSymbol = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress)
            tokenBalance = Libraries.core.API_GetTokenBalance(
                self.contractAddress_Ninja, tokenAddress, "1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress)))
            PrintAndLog_FuncNameHeader("tokenBalance = " + str(tokenBalance) + " for tokenAddress = " + str(tokenAddress))
            tokenTupleList.append((tokenSymbol, tokenBalance))

        PrintAndLog("-------------------------- Ninja " + str(self.contractAddress_Ninja) + " --------------------------")
        PrintAndLog("-------------------------- Proxy Owner = " + str(self.API_GetProxyOwner()) + " --------------------------")

        totalEtherBalanceAcrossAllAccounts = 0
        etherBalance = self.GetQuoteTokenBalance_Ninja(Exchanges.keeperDAO.EthTokenContract)
        totalEtherBalanceAcrossAllAccounts += etherBalance
        PrintAndLog("   Ether balance = " + str(round(etherBalance, 5)) + " ETH")

        for tokenTuple in tokenTupleList:
            tokenSymbol, tokenBalance = tokenTuple
            PrintAndLog("   Token balance = " + str(round(tokenBalance, 5)) + " " + str(tokenSymbol))
            if tokenSymbol.lower() == 'weth':
                totalEtherBalanceAcrossAllAccounts += tokenBalance

        PrintAndLog("   Gas token quantity = " + str(gasTokenQuantity))

        for index, propertyName in enumerate(self.GetAddressPropertyNameList()):
            PrintAndLog("   " + str(propertyName) + " = " + str(Libraries.core.GetAddressFromDataProperty(addressesList[index])))

        for index, propertyName in enumerate(self.GetIntPropertyNameList()):
            value = Libraries.core.ConvertHexToInt(intsList[index])
            if propertyName.lower() == 'gasPriceThresholdForSpendingGasTokens'.lower():
                value = Libraries.core.ConvertWeiToGwei(value)
                PrintAndLog("   " + str(propertyName) + " = " + str(value) + " gwei")
            else:
                PrintAndLog("   " + str(propertyName) + " = " + str(value))

        PrintAndLog("-------------------------- Ninja Operators --------------------------")

        # Create a list of accounts we're displaying
        accountList = []
        accountList += list(NinjaOpAccountDict.values())
        accountList.append(DedicatedTxPredictionAccount)

        # Get their addresses
        accountAddressList = []
        for account in accountList:
            accountAddressList.append(account.publicAddress)

        # Get their balances
        balanceList = Libraries.core.API_GetEtherBalance_Batched_Safe(accountAddressList)

        # Print to screen
        index = 0
        for account in accountList:
            PrintAndLog(account.marketName + ": " + str(round(balanceList[index], 3)) + " ETH: " + str(account.publicAddress))

            totalEtherBalanceAcrossAllAccounts += balanceList[index]
            index += 1

        PrintAndLog("totalEtherBalanceAcrossAllAccounts = " + str(totalEtherBalanceAcrossAllAccounts) + " ETH (and WETH)")

    def API_EnsureAllowance(self, fromAddress, fromPrivateKey, _token, _spender, _value, gas, gasPrice,
                            nonce=None, forceSendRegardlessOfCurrentlyPendingForThisAddress=False):
        import Contracts.contracts

        PrintAndLog("Ninja:API_EnsureAllowance: fromAddress = " + str(fromAddress) + ", _token = " + str(
            _token) + ", _spender = " + str(_spender) + ", _value = " + str(_value))

        kwargs = {
            '_token': str(Libraries.nodes.Instance_Web3.toChecksumAddress(_token)),
            '_spender': str(Libraries.nodes.Instance_Web3.toChecksumAddress(_spender)),
            '_value': int(_value),
        }
        # PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = Contracts.contracts.Contract_Ninja_AssetManager.encodeABI('ensureAllowance', kwargs=kwargs)
        # PrintAndLog("data_hex: " + str(data_hex))

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, self.contractAddress_Ninja, 0,
                                                 gas, gasPrice, data_hex, TransactionType.approve, nonce, forceSendRegardlessOfCurrentlyPendingForThisAddress)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_EnsureAllowance: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_EnsureAllowance: no transactionId")

        return transactionId

    # TODO duplicate function need to merge with the one in Libraries.exchanges
    def ApproveContractForTokenTransfer(self, fromAddress, fromPrivateKey, tokenAddressToApprove, spenderContractToApprove,
                                        nonce=None, forceSendRegardlessOfCurrentlyPendingForThisAddress=False, doIssueApproveTransactions=True,
                                        gas=None, gasPrice=None):
        decimals = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddressToApprove)))
        # Get the allowance for the Ninja contract, not fromAddress
        approvedTokenAmount_ether, approvedTokenAmount_weiUnits = \
            Libraries.core.API_GetTokenAllowance(self.contractAddress_Ninja, tokenAddressToApprove, spenderContractToApprove, decimals)
        PrintAndLog("ApproveContractForTokenTransfer: approvedTokenAmount_ether = " + str(
            approvedTokenAmount_ether) + ", approvedTokenAmount_weiUnits = " + str(approvedTokenAmount_weiUnits))
        # Check to see if our approved amount is getting low
        if approvedTokenAmount_weiUnits > Libraries.core.DataF_Int_uint96 - 10:
            PrintAndLog("ApproveContractForTokenTransfer: we do NOT need to approve " + str(self.contractAddress_Ninja) + " for contract " + str(
                spenderContractToApprove) + " to trade tokens " + str(
                tokenAddressToApprove) + " on our behalf, because it's already approved for a large amount")

        else:
            PrintAndLog("ApproveContractForTokenTransfer: we need to approve " + str(self.contractAddress_Ninja) + " for contract " + str(
                spenderContractToApprove) + " to trade tokens " + str(tokenAddressToApprove) + " on our behalf")

            # Approve a large amount 0xFFFFFFFF...
            # approveAmount_wei = 792089237316195423570985008687907853269984665640564039457584007913129639935
            approveAmount_wei = Libraries.core.DataF_Int
            if not gas:
                gas = Libraries.gasStation.GetGasLimit_Other()

            if not gasPrice:
                # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
                gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
                # gasPrice = Libraries.gasStation.GetGasPrice_Fast()
                # gasPrice = Libraries.core.ConvertGweiToWei(79)

            if not doIssueApproveTransactions:
                message = "Ninja has discovered that it's not approved to trade " + str(tokenAddressToApprove) + ": " + str(
                    spenderContractToApprove) + ", but doIssueApproveTransactions is set to " + str(
                    doIssueApproveTransactions) + ". Please approve it to trade it ASAP so I can begin slinging it around the network."
                Libraries.core.API_PostOperatorNotification(message)
            else:
                PrintAndLog("ApproveContractForTokenTransfer: with tokenAddressToApprove = " + str(tokenAddressToApprove) + ", spenderContractToApprove = " + str(
                    spenderContractToApprove) + ", approveAmount_wei = " + str(approveAmount_wei) + ", nonce = " + str(nonce))

                transactionId = self.API_EnsureAllowance(fromAddress, fromPrivateKey, tokenAddressToApprove, spenderContractToApprove, approveAmount_wei,
                                                         gas, gasPrice, nonce, forceSendRegardlessOfCurrentlyPendingForThisAddress)
                return transactionId

    def API_GetVersion_Ninja(self):
        return API_GetVersion(self.contractAddress_Ninja, self.contractObject_Ninja)

    def API_GetVersion_Utility(self):
        return API_GetVersion(self.contractAddress_Utility, self.contractObject_Utility)

    def API_MintGasTokens(self, fromAddress, fromPrivateKey, quantity, gasPrice, nonce=None):
        import Contracts.contracts

        PrintAndLog("Minting " + str(quantity) + " gas tokens to " + str(self.contractAddress_Ninja))

        quantityNew = Libraries.gasToken_CHI.EnforceSafeQuantityOfGasTokensToMint(quantity)
        if quantityNew != quantity:
            PrintAndLog("API_MintGasTokens: Overriding minting quantity from " + str(quantity) + " to " + str(
                quantityNew) + " because quantity was outside our safe max")
            quantity = quantityNew

        PrintAndLog("quantity = " + str(quantity))

        # function mintGasTokens(
        #         uint gasTokens
        #         ) public

        kwargs = {
            'gasTokens': int(quantity),
        }
        PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = Contracts.contracts.Contract_Ninja_GasTokens.encodeABI('mintGasTokens', kwargs=kwargs)
        # PrintAndLog("data_hex: " + str(data_hex))

        toAddress = self.contractAddress_Ninja
        gas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
        PrintAndLog("gas: " + str(gas))

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 gas, gasPrice, data_hex, TransactionType.other, nonce, True)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_MintGasTokens: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_MintGasTokens: no transactionId")

        return transactionId

    def API_ShareProfitWithKeeperDAO(self, fromAddress, fromPrivateKey, profitToShare_etherUnits, token, gasPrice, nonce=None):
        import Contracts.contracts

        #     function shareProfitWithKeeperDAO(
        #         uint256 profitToShare,
        #         address token
        #         ) external onlyWhitelist
        #     {

        profitToShare_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(profitToShare_etherUnits, token)

        kwargs = {
            'profitToShare': int(profitToShare_weiUnits),
            'token': Libraries.nodes.Instance_Web3.toChecksumAddress(token),
        }
        PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = Contracts.contracts.Contract_Ninja_Trade_3.encodeABI('shareProfitWithKeeperDAO', kwargs=kwargs)
        toAddress = self.contractAddress_Ninja
        estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)

        transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                 estimatedGas, gasPrice, data_hex, TransactionType.approve, nonce)
        # Trash the fromPrivateKey in memory
        fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        if transactionId:
            message = "Ninja:API_ShareProfitWithKeeperDAO: transactionId: " + transactionId
            PrintAndLog(message)
        else:
            PrintAndLog("Failed to Ninja:API_ShareProfitWithKeeperDAO: no transactionId")

        return transactionId

    def API_TradeSimpleRequirements(self, ninjaAccount_toUse, quoteTokenAddress,
                                    expectedProfit_quoteTokens, quoteTokensToSpendArray,
                                    maxQuoteTokenTradeLossAcceptable_etherUnits, doTradeWithinFlashLoan,
                                    checkCallDataArrays_encoded, tradeCallDataArray_encoded, gasPrice, gas,
                                    nonce=None, ethCallIdString=None, message_tradeSubmittedSuccessfully=None,
                                    postMortemTokenString=None, doRequireTxMinePredictionToBeSuccessful=True,
                                    tradingTechnique=TradingTechnique.Arbitrage,
                                    callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade=False,
                                    readySetGoCommunicationDict=None, key_readySetGoCommunicationDict=None,
                                    immediatelyReturnAfterTxMinePrediction=False):
        import Contracts.contracts

        try:
            PrintAndLog_FuncNameHeader("for contractAddress_Ninja " + str(self.contractAddress_Ninja))

            fromAddress = ninjaAccount_toUse.publicAddress

            # # If we're supposed to communicate via readySetGoCommunicationDict but we're not in the correct state
            # if callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade and readySetGoCommunicationDict[key_readySetGoCommunicationDict] != ReadySetGoState.Ready:
            #     PrintAndLog_FuncNameHeader("Forcing callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade to be False "
            #                                "because this key_readySetGoCommunicationDict was not in the Ready State, "
            #                                "we must have re-entered this function with an arbitrageOpportunity we already processed. "
            #                                "Most likely just increasing the gas or retrying something. "
            #                                "key_readySetGoCommunicationDict = " + str(key_readySetGoCommunicationDict))
            #     callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade = False

            #     function tradeSimpleRequirements(
            #         uint256[] calldata quoteTokensToSpendArray,
            #         uint256 quoteToken_encoded,
            #         uint256[][] calldata checkCallDataArrays_encoded,
            #         uint256[] calldata tradeCallDataArray_encoded,
            #         bool doTradeWithinFlashLoan
            #         ) external onlyWhitelist returns (uint256 profit_quoteTokens)
            #     {
            #         // quoteTokensToSpendArray should have 1 less item than checkCallDataArrays_encoded
            #         // For a 2 leg trade
            #         // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade
            #
            #         // For a 3 leg trade
            #         // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade
            #         // quoteTokensToSpendArray[1] = quoteTokensToSpend for second trade
            #
            #         // For a 4 leg trade
            #         // quoteTokensToSpendArray[0] = quoteTokensToSpend for first and last trade
            #         // quoteTokensToSpendArray[1] = quoteTokensToSpend for second trade
            #         // quoteTokensToSpendArray[2] = quoteTokensToSpend for third trade

            # quoteTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(
            #     quoteTokensToSpend_etherUnits, quoteTokenAddress)
            maxQuoteTokenTradeLossAcceptable_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(
                maxQuoteTokenTradeLossAcceptable_etherUnits, quoteTokenAddress)

            # PrintAndLog_FuncNameHeader("maxQuoteTokenTradeLossAcceptable_etherUnits " + str(maxQuoteTokenTradeLossAcceptable_etherUnits))
            # PrintAndLog_FuncNameHeader("maxQuoteTokenTradeLossAcceptable_weiUnits " + str(maxQuoteTokenTradeLossAcceptable_weiUnits))
            kwargs = {
                'quoteTokensToSpendArray': quoteTokensToSpendArray,
                'quoteToken_encoded': Libraries.ninjaEncoding.PackAddress(Libraries.nodes.Instance_Web3.toChecksumAddress(quoteTokenAddress)),
                'checkCallDataArrays_encoded': checkCallDataArrays_encoded,
                'tradeCallDataArray_encoded': tradeCallDataArray_encoded,
                'doTradeWithinFlashLoan': doTradeWithinFlashLoan,
            }
            PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
            # Build binary representation of the function call with arguments
            # Convert this hex string to a hex byte array
            data_hex = Contracts.contracts.Contract_Ninja_Trade_3.encodeABI('tradeSimpleRequirements', kwargs=kwargs)
            # PrintAndLog_FuncNameHeader("data_hex: " + str(data_hex))

            toAddress = self.contractAddress_Ninja
            if Libraries.defaults.EnableNinja_TradeUsingRelayProxy:
                toAddress = Libraries.relayProxy.RelayerProxyContract
                PrintAndLog_FuncNameHeader("EnableNinja_TradeUsingRelayProxy was set so trading through a relay proxy: " + str(toAddress))

            estimatedGas = "dontCare"
            # If we require that we must predict the tx to mine in successfully, via either eth_estimateGas or eth_call or something similar
            # If this is set and our prediction fails we want to NOT broadcast the tx because it will likely revert
            if doRequireTxMinePredictionToBeSuccessful:
                # Note eth_call seems to return me the reason for failure but it's hard to tell that it fails.
                # I think the only wait I can tell that it's going to fail from an eth_call is by saying "If return type i string" but that's hairy
                # My function doesn't return anything, so I could just say if return type is contains more than 1 byte...
                PrintAndLog_FuncNameHeader("Calling API_EthCall and API_EstimateGas with ethCallIdString = " + str(ethCallIdString))
                try:
                    before = datetime.datetime.now()
                    threads = []
                    resultDict = {}
                    lock_resultDict = Lock()

                    resultKey_API_EthCall = "API_EthCall"
                    t_API_EthCall = threading.Thread(target=Libraries.core.API_EthCall, args=(
                        toAddress, fromAddress, data_hex, 0, Libraries.core.RequestTimeout_short_seconds,
                        resultDict, resultKey_API_EthCall, lock_resultDict,))
                    threads.append(t_API_EthCall)
                    t_API_EthCall.start()

                    resultKey_API_EstimateGas = "API_EstimateGas"
                    t_API_EstimateGas = threading.Thread(target=Libraries.core.API_EstimateGas, args=(
                        toAddress, fromAddress, data_hex, 0, True, 1.03,
                        Libraries.core.RequestTimeout_short_seconds, None,
                        resultDict, resultKey_API_EstimateGas, lock_resultDict,))
                    threads.append(t_API_EstimateGas)
                    t_API_EstimateGas.start()

                    PrintAndLog_FuncNameHeader("Waiting for threads to finish")
                    for thread in threads:
                        thread.join()

                    result_ethCall = resultDict[resultKey_API_EthCall]
                    estimatedGas = resultDict[resultKey_API_EstimateGas]

                    duration_s = (datetime.datetime.now() - before).total_seconds()
                    PrintAndLog_FuncNameHeader("eth_call & eth_estimateGas succeeded in " + str(duration_s) + " seconds")

                    PrintAndLog_FuncNameHeader("result_ethCall = " + str(result_ethCall))
                    PrintAndLog_FuncNameHeader("estimatedGas = " + str(estimatedGas) + " for ethCallIdString " + str(ethCallIdString))

                    # TODO callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade
                    #  if callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade == True
                    #     anytime we 'return None, False' we must tell the callback that we failed and we can exit this trade loop
                    #     What's trick is normally we have a retry system where we would retry up to ~4 times
                    #     But there's no point in keeping that because we don't want the callback logic in ArbitrageGeneric to have to wait for 4 retries that would take too long
                    #     readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

                    if Libraries.defaults.CallEthCallBeforeTradingForSafety:
                        # result_ethCall = Libraries.core.API_EthCall(toAddress, fromAddress, data_hex)
                        PrintAndLog_FuncNameHeader("result_ethCall succeeded, result = " + str(result_ethCall))
                        actualProfit_quoteTokens = Libraries.core.ConvertWeiToEther_GivenTokenAddress(
                            Libraries.core.ConvertHexToInt(result_ethCall), quoteTokenAddress)
                        PrintAndLog_FuncNameHeader("actualProfit_quoteTokens = " + str(actualProfit_quoteTokens) + " " + str(quoteTokenAddress))
                        PrintAndLog_FuncNameHeader("expectedProfit_quoteTokens = " + str(expectedProfit_quoteTokens) + " " + str(quoteTokenAddress))
                        profitRequirementThreshold = expectedProfit_quoteTokens * Libraries.defaults.ActualProfitMinPercentageOfExpectedProfit
                        PrintAndLog_FuncNameHeader("profitRequirementThreshold = " + str(profitRequirementThreshold) + " " + str(quoteTokenAddress))
                        # If we're not going to make as much profit as we are expecting to make
                        if actualProfit_quoteTokens < profitRequirementThreshold:
                            PrintAndLog_FuncNameHeader("Return None and False saying that our tx mine prediction failed because the trade was not profitable based on eth_call")
                            # if callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade and readySetGoCommunicationDict and key_readySetGoCommunicationDict:
                            #     # Tell the readySetGoCommunicationDict we failed
                            #     readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

                            return None, False

                    if Libraries.defaults.CallEstimateGasBeforeTradingForSafety:
                        # estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
                        PrintAndLog_FuncNameHeader("estimatedGas succeeded, estimatedGas = " + str(estimatedGas) + " for ethCallIdString " + str(ethCallIdString))
                        # Require that the gas limit we're providing is <= estimatedGas.
                        # I've seen some cases where teh estimatedGas is incredibly high adn then I get an out of gas error and spew ETH.. I need to avoid this at all costs
                        # Send a notification to the operator too so we can fix the issue.
                        # I cannot just blindly increase the gas limit for the tx to accommodate the
                        # estimatedGas because that will increase the cost of the tx and make it less profitable

                        # I'm getting this message sometimes when I dont' think I should be, so I'm adding some padding to this.
                        # I've seen eth_estimateGas being wrong before on gas estimates
                        gasLimitPadding = 0
                        if estimatedGas > gas + gasLimitPadding:
                            message = "estimatedGas exceeded our provided gas limit. Rejecting the trade opportunity. " \
                                      "Is my gas limit estimation incorrect? estimatedGas = " + str(estimatedGas) + ", gas = " + \
                                      str(gas) + ", gasLimitPadding = " + str(gasLimitPadding)
                            PrintAndLog_FuncNameHeader(message)
                            Libraries.core.API_PostOperatorNotification(message)
                            PrintAndLog_FuncNameHeader("Return None and False saying that our tx mine prediction failed which means we predict the call will revert due to out of gas")
                            # if callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade and readySetGoCommunicationDict and key_readySetGoCommunicationDict:
                            #     # Tell the readySetGoCommunicationDict we failed
                            #     readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

                            return None, False

                        # I've noticed that sometimes my estimateGas will come back with only like 50k or 60k, meaning the trade will miss.
                        # When this happens there's a great chance the trade will not even succeed so may as well not even broadcast it
                        # But there's also a chance this could be preventing successful trades from going through since I've found eth_estimateGas to be unreliable anyways
                        # Only enforce this for tradingTechnique == TradingTechnique.Arbitrage
                        if tradingTechnique == TradingTechnique.Arbitrage and \
                                Libraries.defaults.MinGasRequired_ToAssumeTradeWillSucceed and \
                                estimatedGas < Libraries.defaults.MinGasRequired_ToAssumeTradeWillSucceed:
                            PrintAndLog_FuncNameHeader("Return None and False saying that our tx mine prediction failed which means we predict the trade will miss")
                            # if callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade and readySetGoCommunicationDict and key_readySetGoCommunicationDict:
                            #     # Tell the readySetGoCommunicationDict we failed
                            #     readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

                            return None, False

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    if Libraries.defaults.CallEstimateGasBeforeTradingForSafety or Libraries.defaults.CallEthCallBeforeTradingForSafety:
                        message = "estimatedGas or ethCall FAILED for ethCallIdString " + str(ethCallIdString) + ", " + str(traceback.format_exc())
                        PrintAndLog_FuncNameHeader(message)
                        PrintAndLogError(message)
                        PrintAndLog_FuncNameHeader("Return None and False saying that our tx mine prediction failed which means we predict the call will revert")
                        # if callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade and readySetGoCommunicationDict and key_readySetGoCommunicationDict:
                        #     # Tell the readySetGoCommunicationDict we failed
                        #     readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

                        return None, False

                    else:
                        # We must have turned off our before trade safety checks, so let's keep going regardless if eth_estimateGas or eth_call failed
                        pass

            # # TODO callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade
            # #  IF we've made it this far, we can safely assume the trade will succeed
            # #  We need to make a callback to ArbitrageGeneric telling it GO
            # # Only do this if doRequireTxMinePredictionToBeSuccessful = True because we do not want to run this logic when we're just incrementing gas price or front running
            # PrintAndLog_FuncNameHeader("JOEYZ DEBUG READYSETGO. key_readySetGoCommunicationDict = " + str(key_readySetGoCommunicationDict))
            # if doRequireTxMinePredictionToBeSuccessful and \
            #         callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade and \
            #         readySetGoCommunicationDict and \
            #         key_readySetGoCommunicationDict:
            #     readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Set
            #     sleepTime_seconds = 1.0
            #     while True:
            #         PrintAndLog_FuncNameHeader("TODO Make READYSETGO callback to ArbitrageGeneric telling it we are ready to trade. ethCallIdString = " + str(ethCallIdString))
            #         PrintAndLog_FuncNameHeader("Waiting for readySetGoCommunicationDict[" + str(key_readySetGoCommunicationDict) +
            #                                    "] to = go, it's currently = " + str(readySetGoCommunicationDict[key_readySetGoCommunicationDict]))
            #         if readySetGoCommunicationDict[key_readySetGoCommunicationDict] != ReadySetGoState.Set:
            #             PrintAndLog_FuncNameHeader("Breaking from loop because our state has changed: key_readySetGoCommunicationDict = " + str(key_readySetGoCommunicationDict))
            #             break
            #
            #         time.sleep(sleepTime_seconds)
            #
            #     if readySetGoCommunicationDict[key_readySetGoCommunicationDict] == ReadySetGoState.Go:
            #         PrintAndLog_FuncNameHeader("We've been given the GO signal to pursue the trade: "
            #                                    "key_readySetGoCommunicationDict = " + str(key_readySetGoCommunicationDict))
            #         pass
            #     elif readySetGoCommunicationDict[key_readySetGoCommunicationDict] == ReadySetGoState.Fail:
            #         message = "We've been told to abandon the trade, most likely because of a new block being mined in: " \
            #                   "key_readySetGoCommunicationDict = " + str(key_readySetGoCommunicationDict)
            #         PrintAndLog_FuncNameHeader(message)
            #         # Raise an exception that we can catch to gracefully exit
            #         raise ArbitrageOpportunityAbandoned(message)
            
            # If we've made it this far, we can safely assume the trade will succeed
            if immediatelyReturnAfterTxMinePrediction:
                # Don't actually continue processing the trade and signing a transaction
                # We just want to know if the txMinePrediction succeeded
                # We don't have a transactionId but we the txMinePrediction was True
                return None, True

            # Consider not trading due to debug flags
            if not Libraries.defaults.EnableNinja_TradeExecution:
                message = "EnableNinja_TradeExecution was disabled. We are in debugging mode and no Ninja trades will be executed. estimatedGas = " + str(estimatedGas)
                if IsTimeToExecute(message, 60, None, True, True):
                    Libraries.core.API_PostOperatorNotification(message)

                PrintAndLog_FuncNameHeader("Return None and False because EnableNinja_TradeExecution was False")
                return None, False

            fromPrivateKey = ninjaAccount_toUse.DecryptPK()
            transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                                     gas, gasPrice, data_hex, TransactionType.other, nonce, True)
            # Trash the fromPrivateKey in memory
            fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

            if transactionId:
                message = "Associate estimatedGas = " + str(estimatedGas) + ", transactionId: " + str(transactionId)
                PrintAndLog_FuncNameHeader(message)

            else:
                PrintAndLog_FuncNameHeader("Failed: no transactionId")

            PrintAndLog_FuncNameHeader("JOEYZ DEBUGGING:")
            PrintAndLog_FuncNameHeader("   message_tradeSubmittedSuccessfully = " + str(message_tradeSubmittedSuccessfully))
            PrintAndLog_FuncNameHeader("   transactionId = " + str(transactionId))
            PrintAndLog_FuncNameHeader("   postMortemTokenString = " + str(postMortemTokenString))
            if message_tradeSubmittedSuccessfully:
                if transactionId and postMortemTokenString:
                    message_tradeSubmittedSuccessfully += " transactionId trade post mortem string = " + str(transactionId) + " " + str(postMortemTokenString)
                Libraries.core.API_PostOperatorNotification(message_tradeSubmittedSuccessfully)

            PrintAndLog_FuncNameHeader("Return transactionId and True saying that our tx mine prediction succeeded which "
                                       "should mean this call will succeed as long as I don't get front run")
            return transactionId, True

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            message = "exception in API_TradeSimpleRequirements, " + traceback.format_exc()
            PrintAndLogError(message)
            PrintAndLog_FuncNameHeader(message)
            # if callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade and readySetGoCommunicationDict and key_readySetGoCommunicationDict:
            #     # Tell the readySetGoCommunicationDict we failed
            #     readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

            raise

    def API_TradeTokensOnKyber(self, fromAddress, fromPrivateKey, msgValue_weiUnits,
                               src, sourceAmount_weiUnits, dest, maxDestAmount, minConversionRate_etherUnits, walletId,
                               gas, gasPrice):
        return API_TradeTokensOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                      fromAddress, fromPrivateKey, msgValue_weiUnits,
                                      src, sourceAmount_weiUnits, dest, maxDestAmount, minConversionRate_etherUnits, walletId,
                                      gas, gasPrice)

    def API_TradeTokensOnSet(self, fromAddress, fromPrivateKey, inflowTokensToSpend_weiUnits, minBidSize_weiUnits,
                             rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress, gas, gasPrice):
        return API_TradeTokensOnSet(self.contractAddress_Ninja, self.contractObject_Ninja,
                                    fromAddress, fromPrivateKey, inflowTokensToSpend_weiUnits, minBidSize_weiUnits,
                                    rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress, gas, gasPrice)

    def API_TradeTokensOnBancor(self, fromAddress, fromPrivateKey, msgValue_weiUnits,
                                bancorConverterContract, pathList, sourceAmount_weiUnits, _minReturn, _block, _v, _r, _s,
                                gas, gasPrice):
        return API_TradeTokensOnBancor(self.contractAddress_Ninja, self.contractObject_Ninja,
                                       fromAddress, fromPrivateKey, msgValue_weiUnits,
                                       bancorConverterContract, pathList, sourceAmount_weiUnits, _minReturn, _block, _v, _r, _s,
                                       gas, gasPrice)

    def API_Trade_BuyOnSetSellOnKyber(self, fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                      inflowTokensToSpend_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                      inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice):
        return API_Trade_BuyOnSetSellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                             fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                             inflowTokensToSpend_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                             inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice)

    def API_Trade_BuyOnKyberSellOnSet(self, fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                      amountToConvert_Kyber_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                      inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice):
        return API_Trade_BuyOnKyberSellOnSet(self.contractAddress_Ninja, self.contractObject_Ninja,
                                             fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                             amountToConvert_Kyber_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                             inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice)

    def API_TradeSafe_BuyOnSetSellOnKyber(self, fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                          inflowTokensToSpend_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress,
                                          setTokenAddress, inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice, numOfTimesToTrade=1):
        maxNumOfTimesToTrade = 4
        if numOfTimesToTrade > maxNumOfTimesToTrade:
            Libraries.core.API_PostOperatorNotification("I haven't tested numOfTimesToTrade this high yet! Overriding to something safer")
            numOfTimesToTrade = maxNumOfTimesToTrade

        transactionIdList = []
        for i in range(numOfTimesToTrade):
            transactionCount = Libraries.core.API_GetTransactionCount(fromAddress)
            nonceToUse = transactionCount + i

            transactionId = API_TradeSafe_BuyOnSetSellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                              fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                                              inflowTokensToSpend_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                                              inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice, nonceToUse)
            transactionIdList.append(transactionId)

        return transactionIdList

    def API_TradeSafe_BuyOnKyberSellOnSet(self, fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                          amountToConvert_Kyber_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                          inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice, numOfTimesToTrade=1):
        maxNumOfTimesToTrade = 4
        if numOfTimesToTrade > maxNumOfTimesToTrade:
            Libraries.core.API_PostOperatorNotification("I haven't tested numOfTimesToTrade this high yet! Overriding to something safer")
            numOfTimesToTrade = maxNumOfTimesToTrade

        transactionIdList = []
        for i in range(numOfTimesToTrade):
            transactionCount = Libraries.core.API_GetTransactionCount(fromAddress)
            nonceToUse = transactionCount + i

            transactionId = API_TradeSafe_BuyOnKyberSellOnSet(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                              fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                                              amountToConvert_Kyber_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                                              inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice, nonceToUse)
            transactionIdList.append(transactionId)

        return transactionIdList

    def API_TradeSafe_BuyOn0xv2SellOnBancor(self, fromAddress, fromPrivateKey, tokenContractAddress,
                                            bancorConverterContract, bancorTokenConverterContract,
                                            pathList_Bancor, order, amountToConvert_0xv2_weiUnits,
                                            gas, gasPrice):
        return API_TradeSafe_BuyOn0xv2SellOnBancor(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                   fromAddress, fromPrivateKey, tokenContractAddress,
                                                   bancorConverterContract, bancorTokenConverterContract,
                                                   pathList_Bancor, order, amountToConvert_0xv2_weiUnits,
                                                   gas, gasPrice)

    def API_TradeSafe_BuyOnBancorSellOn0xv2(self, fromAddress, fromPrivateKey, tokenContractAddress,
                                            bancorConverterContract, bancorTokenConverterContract,
                                            pathList_Bancor, order, amountToConvert_Bancor_weiUnits,
                                            gas, gasPrice):
        return API_TradeSafe_BuyOnBancorSellOn0xv2(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                   fromAddress, fromPrivateKey, tokenContractAddress,
                                                   bancorConverterContract, bancorTokenConverterContract,
                                                   pathList_Bancor, order, amountToConvert_Bancor_weiUnits,
                                                   gas, gasPrice)

    def API_Trade_BuyOnBancorSellOnKyber(self, fromAddress, fromPrivateKey,
                                         bancorConverterContract, kyberProxyContract,
                                         pathList_Bancor, pathList_Kyber, amountToConvert_Bancor_weiUnits,
                                         gas, gasPrice):
        return API_Trade_BuyOnBancorSellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                fromAddress, fromPrivateKey,
                                                bancorConverterContract, kyberProxyContract,
                                                pathList_Bancor, pathList_Kyber, amountToConvert_Bancor_weiUnits,
                                                gas, gasPrice)

    def API_Trade_BuyOnKyberSellOnBancor(self, fromAddress, fromPrivateKey,
                                         bancorConverterContract, kyberProxyContract,
                                         pathList_Bancor, pathList_Kyber, amountToConvert_Kyber_weiUnits,
                                         gas, gasPrice):
        return API_Trade_BuyOnKyberSellOnBancor(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                fromAddress, fromPrivateKey,
                                                bancorConverterContract, kyberProxyContract,
                                                pathList_Bancor, pathList_Kyber, amountToConvert_Kyber_weiUnits,
                                                gas, gasPrice)

    def API_TradeSafe_BuyOnBancorSellOnKyber(self, fromAddress, fromPrivateKey,
                                             bancorConverterContract, bancorTokenConverterContract, kyberProxyContract,
                                             pathList_Bancor, pathList_Kyber, amountToConvert_Bancor_weiUnits,
                                             gas, gasPrice):
        return API_TradeSafe_BuyOnBancorSellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                    fromAddress, fromPrivateKey,
                                                    bancorConverterContract, bancorTokenConverterContract, kyberProxyContract,
                                                    pathList_Bancor, pathList_Kyber, amountToConvert_Bancor_weiUnits,
                                                    gas, gasPrice)

    def API_TradeSafe_BuyOnKyberSellOnBancor(self, fromAddress, fromPrivateKey,
                                             bancorConverterContract, bancorTokenConverterContract, kyberProxyContract,
                                             pathList_Bancor, pathList_Kyber, amountToConvert_Kyber_weiUnits,
                                             gas, gasPrice):
        return API_TradeSafe_BuyOnKyberSellOnBancor(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                    fromAddress, fromPrivateKey,
                                                    bancorConverterContract, bancorTokenConverterContract, kyberProxyContract,
                                                    pathList_Bancor, pathList_Kyber, amountToConvert_Kyber_weiUnits,
                                                    gas, gasPrice)

    def API_Trade_BuyOnSetSellOn0xv2(self, fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                     rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                     order, inflowTokensToSpend_Set_weiUnits, gas, gasPrice):
        return API_Trade_BuyOnSetSellOn0xv2(self.contractAddress_Ninja, self.contractObject_Ninja,
                                            fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                            rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                            order, inflowTokensToSpend_Set_weiUnits, gas, gasPrice)

    def API_Trade_BuyOn0xv2SellOnSet(self, fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                     rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                     order, amountToConvert_0xv2_weiUnits, gas, gasPrice):
        return API_Trade_BuyOn0xv2SellOnSet(self.contractAddress_Ninja, self.contractObject_Ninja,
                                            fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                            rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                            order, amountToConvert_0xv2_weiUnits, gas, gasPrice)

    def API_TradeSafe_BuyOnSetSellOn0xv2(self, fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                         rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                         order, inflowTokensToSpend_Set_weiUnits, gas, gasPrice, nonceToUse=None):
        return API_TradeSafe_BuyOnSetSellOn0xv2(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                                rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                                order, inflowTokensToSpend_Set_weiUnits, gas, gasPrice, nonceToUse)

    def API_TradeSafe_BuyOn0xv2SellOnSet(self, fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                         rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                         order, amountToConvert_0xv2_weiUnits, gas, gasPrice, nonceToUse=None):
        return API_TradeSafe_BuyOn0xv2SellOnSet(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                                rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                                order, amountToConvert_0xv2_weiUnits, gas, gasPrice, nonceToUse)

    def API_Trade_BuyOn0xv2SellOnKyber(self, fromAddress, fromPrivateKey, kyberProxyContract,
                                       pathList_Kyber, order, amountToConvert_0xv2_weiUnits,
                                       gas, gasPrice):
        return API_Trade_BuyOn0xv2SellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                              fromAddress, fromPrivateKey, kyberProxyContract,
                                              pathList_Kyber, order, amountToConvert_0xv2_weiUnits,
                                              gas, gasPrice)

    def API_Trade_BuyOnKyberSellOn0xv2(self, fromAddress, fromPrivateKey, kyberProxyContract,
                                       pathList_Kyber, order, amountToConvert_Kyber_weiUnits,
                                       gas, gasPrice):
        return API_Trade_BuyOnKyberSellOn0xv2(self.contractAddress_Ninja, self.contractObject_Ninja,
                                              fromAddress, fromPrivateKey, kyberProxyContract,
                                              pathList_Kyber, order, amountToConvert_Kyber_weiUnits,
                                              gas, gasPrice)

    def API_TradeSafe_BuyOn0xv2SellOnKyber(self, fromAddress, fromPrivateKey, token, kyberProxyContract,
                                           pathList_Kyber, order, amountToConvert_0xv2_weiUnits,
                                           gas, gasPrice):
        return API_TradeSafe_BuyOn0xv2SellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                  fromAddress, fromPrivateKey, token, kyberProxyContract,
                                                  pathList_Kyber, order, amountToConvert_0xv2_weiUnits,
                                                  gas, gasPrice)

    def API_TradeSafe_BuyOnKyberSellOn0xv2(self, fromAddress, fromPrivateKey, token, kyberProxyContract,
                                           pathList_Kyber, order, amountToConvert_Kyber_weiUnits,
                                           gas, gasPrice):
        return API_TradeSafe_BuyOnKyberSellOn0xv2(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                  fromAddress, fromPrivateKey, token, kyberProxyContract,
                                                  pathList_Kyber, order, amountToConvert_Kyber_weiUnits,
                                                  gas, gasPrice)

    def API_TradeSimpleRequirements_BuyOnKyberSellOnBancor(self, fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                           tokenAddress, minBancorBntQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
                                                           bancorConverter_payingWithSpecificTokenOnly, bancorRelayerToken, kyberReserve,
                                                           gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnKyberSellOnBancor(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                  fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                                  tokenAddress, minBancorBntQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
                                                                  bancorConverter_payingWithSpecificTokenOnly, bancorRelayerToken, kyberReserve,
                                                                  gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnBancorSellOnKyber(self, fromAddress, fromPrivateKey, etherToSpend_etherUnits, expectedTokensToTrade_etherUnits,
                                                           tokenAddress, decimals_token, maxBancorBntQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
                                                           bancorConverter_payingWithEtherOnly, bancorConverter_payingWithSpecificTokenOnly,
                                                           bancorRelayerToken, kyberReserve, gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnBancorSellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                  fromAddress, fromPrivateKey, etherToSpend_etherUnits, expectedTokensToTrade_etherUnits,
                                                                  tokenAddress, decimals_token, maxBancorBntQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
                                                                  bancorConverter_payingWithEtherOnly, bancorConverter_payingWithSpecificTokenOnly,
                                                                  bancorRelayerToken, kyberReserve, gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnUniswapSellOnKyber(self, fromAddress, fromPrivateKey, etherToSpend_etherUnits, expectedTokensToTrade_etherUnits,
                                                            tokenAddress, decimals_token, maxUniswapEthQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
                                                            kyberReserve, gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnUniswapSellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                   fromAddress, fromPrivateKey, etherToSpend_etherUnits, expectedTokensToTrade_etherUnits,
                                                                   tokenAddress, decimals_token, maxUniswapEthQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
                                                                   kyberReserve, gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap(self, fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                            tokenAddress, minUniswapEthQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
                                                            kyberReserve, gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                   fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                                   tokenAddress, minUniswapEthQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
                                                                   kyberReserve, gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnUniswapSellOn0xv3(self, fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                           tokenAddress, maxUniswapEthQuantityAcceptable_etherUnits, order,
                                                           gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnUniswapSellOn0xv3(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                  fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                                  tokenAddress, maxUniswapEthQuantityAcceptable_etherUnits, order,
                                                                  gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOn0xv3SellOnUniswap(self, fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                           tokenAddress, minUniswapEthQuantityAcceptable_etherUnits,
                                                           order, gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOn0xv3SellOnUniswap(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                  fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                                  tokenAddress, minUniswapEthQuantityAcceptable_etherUnits,
                                                                  order, gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnSetSellOnKyber(self, reservableEthereumAccountPool, reserveAccountName, etherToSpend_etherUnits,
                                                        expectedTokensToTrade_etherUnits,
                                                        tokenAddress, decimals_token, setTokenData, useCTokenBidderContract, minKyberPriceAcceptable,
                                                        kyberReserve, gas, gasPrice, numOfTimesToTrade=1, message_tradeSubmittedSuccessfully=None):
        ninjaAccount_toUse, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(reserveAccountName)

        fromAddress = ninjaAccount_toUse.publicAddress
        fromPrivateKey = ninjaAccount_toUse.DecryptPK()

        transactionIdList = []
        try:
            maxNumOfTimesToTrade = 4
            if numOfTimesToTrade > maxNumOfTimesToTrade:
                Libraries.core.API_PostOperatorNotification("I haven't tested numOfTimesToTrade this high yet! Overriding to something safer")
                numOfTimesToTrade = maxNumOfTimesToTrade

            for i in range(numOfTimesToTrade):
                transactionCount = Libraries.core.API_GetTransactionCount(fromAddress)
                nonceToUse = transactionCount + i
                transactionId = API_TradeSimpleRequirements_BuyOnSetSellOnKyber(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                                fromAddress, fromPrivateKey, etherToSpend_etherUnits, expectedTokensToTrade_etherUnits,
                                                                                tokenAddress, decimals_token, setTokenData, useCTokenBidderContract,
                                                                                minKyberPriceAcceptable,
                                                                                kyberReserve, gas, gasPrice, nonceToUse)
                transactionIdList.append(transactionId)

        except:
            # Release the account since the trade failed
            reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)
            raise

        if len(transactionIdList) > 0:
            for transactionId in transactionIdList:
                # Associate the transactionId with the account
                reservableEthereumAccountPool.AssociateAccountWithNonce(ninjaAccount_toUse, transactionId)

            if message_tradeSubmittedSuccessfully:
                # Append the transactionId because the caller does not know it!
                message_tradeSubmittedSuccessfully += ", transactionIdList = " + str(transactionIdList)
                Libraries.core.API_PostOperatorNotification(message_tradeSubmittedSuccessfully)

        else:
            # Release the account since the trade failed
            reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)

            PrintAndLog("Ninja(Set-Kyber) failed to trade via " + str(tokenAddress) + " BuyOnSetSellOnKyber")

    def API_TradeSimpleRequirements_BuyOnKyberSellOnSet(self, reservableEthereumAccountPool, reserveAccountName, etherToSpend_etherUnits,
                                                        tokenAddress, setTokenData, useCTokenBidderContract, maxKyberPriceAcceptable,
                                                        kyberReserve, gas, gasPrice, numOfTimesToTrade=1, message_tradeSubmittedSuccessfully=None):
        ninjaAccount_toUse, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(reserveAccountName)

        fromAddress = ninjaAccount_toUse.publicAddress
        fromPrivateKey = ninjaAccount_toUse.DecryptPK()

        transactionIdList = []
        try:
            maxNumOfTimesToTrade = 4
            if numOfTimesToTrade > maxNumOfTimesToTrade:
                Libraries.core.API_PostOperatorNotification("I haven't tested numOfTimesToTrade this high yet! Overriding to something safer")
                numOfTimesToTrade = maxNumOfTimesToTrade

            for i in range(numOfTimesToTrade):
                transactionCount = Libraries.core.API_GetTransactionCount(fromAddress)
                nonceToUse = transactionCount + i
                transactionId = API_TradeSimpleRequirements_BuyOnKyberSellOnSet(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                                fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                                                tokenAddress, setTokenData, useCTokenBidderContract, maxKyberPriceAcceptable,
                                                                                kyberReserve, gas, gasPrice, nonceToUse)
                transactionIdList.append(transactionId)

        except:
            # Release the account since the trade failed
            reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)
            raise

        if len(transactionIdList) > 0:
            for transactionId in transactionIdList:
                # Associate the transactionId with the account
                reservableEthereumAccountPool.AssociateAccountWithNonce(ninjaAccount_toUse, transactionId)

            if message_tradeSubmittedSuccessfully:
                # Append the transactionId because the caller does not know it!
                message_tradeSubmittedSuccessfully += ", transactionIdList = " + str(transactionIdList)
                Libraries.core.API_PostOperatorNotification(message_tradeSubmittedSuccessfully)

        else:
            # Release the account since the trade failed
            reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)

            PrintAndLog("Ninja(Set-Kyber) failed to trade via " + str(tokenAddress) + " BuyOnKyberSellOnSet")

    def API_TradeSimpleRequirements_BuyOnSetSellOn0xv3(self, reservableEthereumAccountPool, reserveAccountName, etherToSpend_etherUnits,
                                                       order, setTokenData, useCTokenBidderContract, gas, gasPrice,
                                                       nonceToUse=None, message_tradeSubmittedSuccessfully=None):
        from Libraries.trade import AssociateAnOrderHashWithATxHash

        ninjaAccount_toUse, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(reserveAccountName)

        transactionIdList = None
        try:
            transactionIdList = API_TradeSimpleRequirements_BuyOnSetSellOn0xv3(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                               ninjaAccount_toUse.publicAddress, ninjaAccount_toUse.DecryptPK(), etherToSpend_etherUnits,
                                                                               order, setTokenData, useCTokenBidderContract, gas, gasPrice, nonceToUse)

        except:
            # Release the account since the trade failed
            reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)
            raise

        if len(transactionIdList) > 0:
            for transactionId in transactionIdList:
                # Associate the transactionId with the order.hash in case I need to blacklist it
                AssociateAnOrderHashWithATxHash(transactionId, order.hash)

                # Associate the transactionId with the account
                reservableEthereumAccountPool.AssociateAccountWithNonce(ninjaAccount_toUse, transactionId)

            if message_tradeSubmittedSuccessfully:
                # Append the transactionIdList because the caller does not know it!
                message_tradeSubmittedSuccessfully += ", transactionIdList = " + str(transactionIdList)
                Libraries.core.API_PostOperatorNotification(message_tradeSubmittedSuccessfully)

        else:
            # Release the account since the trade failed
            reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)

            PrintAndLog("Ninja(Set-0x) failed to trade via " + setTokenData.setTokenKey + " BuyOnSetSellOn0xv2")

    def API_TradeSimpleRequirements_BuyOn0xv3SellOnSet(self, reservableEthereumAccountPool, reserveAccountName, etherToSpend_etherUnits,
                                                       order, setTokenData, useCTokenBidderContract, gas, gasPrice,
                                                       nonceToUse=None, message_tradeSubmittedSuccessfully=None):
        from Libraries.trade import AssociateAnOrderHashWithATxHash

        ninjaAccount_toUse, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(reserveAccountName)

        transactionIdList = None
        try:
            transactionIdList = API_TradeSimpleRequirements_BuyOn0xv3SellOnSet(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                               ninjaAccount_toUse.publicAddress, ninjaAccount_toUse.DecryptPK(), etherToSpend_etherUnits,
                                                                               order, setTokenData, useCTokenBidderContract, gas, gasPrice, nonceToUse)

        except:
            # Release the account since the trade failed
            reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)
            raise

        if len(transactionIdList) > 0:
            for transactionId in transactionIdList:
                # Associate the transactionId with the order.hash in case I need to blacklist it
                AssociateAnOrderHashWithATxHash(transactionId, order.hash)

                # Associate the transactionId with the account
                reservableEthereumAccountPool.AssociateAccountWithNonce(ninjaAccount_toUse, transactionId)

            if message_tradeSubmittedSuccessfully:
                # Append the transactionIdList because the caller does not know it!
                message_tradeSubmittedSuccessfully += ", transactionIdList = " + str(transactionIdList)
                Libraries.core.API_PostOperatorNotification(message_tradeSubmittedSuccessfully)

        else:
            # Release the account since the trade failed
            reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)

            PrintAndLog("Ninja(Set-0x) failed to trade via " + setTokenData.setTokenKey + " BuyOn0xv2SellOnSet")

    def API_TradeSimpleRequirements_BuyOnSetSellOnUniswap(self, fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                          tokenAddress, minUniswapEthQuantityAcceptable_etherUnits,
                                                          setTokenData, gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnSetSellOnUniswap(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                 fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                                 tokenAddress, minUniswapEthQuantityAcceptable_etherUnits,
                                                                 setTokenData, gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnUniswapSellOnSet(self, fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                          tokenAddress, maxUniswapEthQuantityAcceptable_etherUnits,
                                                          setTokenData, gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnUniswapSellOnSet(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                 fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                                 tokenAddress, maxUniswapEthQuantityAcceptable_etherUnits,
                                                                 setTokenData, gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnSetSellOnKyber_TokenForToken(self, fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                      quoteTokenAddress, baseTokenAddress, setTokenData,
                                                                      gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnSetSellOnKyber_TokenForToken(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                             fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                             quoteTokenAddress, baseTokenAddress, setTokenData,
                                                                             gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnKyberSellOnSet_TokenForToken(self, fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                      quoteTokenAddress, baseTokenAddress, setTokenData,
                                                                      gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnKyberSellOnSet_TokenForToken(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                             fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                             quoteTokenAddress, baseTokenAddress, setTokenData,
                                                                             gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnUniswapSellOnSet_TokenForToken(self, fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                        quoteTokenAddress, baseTokenAddress,
                                                                        setTokenData, gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnUniswapSellOnSet_TokenForToken(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                               fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                               quoteTokenAddress, baseTokenAddress,
                                                                               setTokenData, gas, gasPrice, nonceToUse)

    def API_TradeSimpleRequirements_BuyOnSetSellOnUniswap_TokenForToken(self, fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                        quoteTokenAddress, baseTokenAddress, expectedBaseTokensToTrade_etherUnits,
                                                                        setTokenData, gas, gasPrice, nonceToUse=None):
        return API_TradeSimpleRequirements_BuyOnSetSellOnUniswap_TokenForToken(self.contractAddress_Ninja, self.contractObject_Ninja,
                                                                               fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                               quoteTokenAddress, baseTokenAddress, expectedBaseTokensToTrade_etherUnits,
                                                                               setTokenData, gas, gasPrice, nonceToUse)

    def API_GetManyArbitrages_BuyOnKyberSellOnSet(self, token, setTokenAddress, amountsToConvert_Kyber_weiUnits, minBidSize_weiUnits):
        return API_GetManyArbitrages_BuyOnKyberSellOnSet(self.contractAddress_Utility, self.contractObject_Utility,
                                                         token, setTokenAddress, amountsToConvert_Kyber_weiUnits, minBidSize_weiUnits)

    def API_GetManyArbitrages_BuyOnSetSellOnKyber(self, token, setTokenAddress, inflowTokensToSpend_Set_weiUnits, minBidSize_weiUnits):
        return API_GetManyArbitrages_BuyOnSetSellOnKyber(self.contractAddress_Utility, self.contractObject_Utility,
                                                         token, setTokenAddress, inflowTokensToSpend_Set_weiUnits, minBidSize_weiUnits)

    def API_GetManyArbitrages_BuyOnKyberSellOnBancor(self, token, amountsToConvert_Kyber):
        return API_GetManyArbitrages_BuyOnKyberSellOnBancor(self.contractAddress_Utility, self.contractObject_Utility,
                                                            token, amountsToConvert_Kyber)

    def API_GetManyArbitrages_BuyOnBancorSellOnKyber(self, token, amountsToConvert_Bancor):
        return API_GetManyArbitrages_BuyOnBancorSellOnKyber(self.contractAddress_Utility, self.contractObject_Utility,
                                                            token, amountsToConvert_Bancor)

    def API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve(self, token, amountsToConvert_Kyber, kyberReserve):
        return API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched(self.contractAddress_Utility, self.contractObject_Utility,
                                                                                 token, amountsToConvert_Kyber, kyberReserve)
        # return API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve(self.contractAddress_Utility, self.contractObject_Utility,
        #                                                                       token, amountsToConvert_Kyber, kyberReserve)

    def API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve(self, token, amountsToConvert_Bancor, kyberReserve):
        return API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched(self.contractAddress_Utility, self.contractObject_Utility,
                                                                                 token, amountsToConvert_Bancor, kyberReserve)
        # return API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve(self.contractAddress_Utility, self.contractObject_Utility,
        #                                                                       token, amountsToConvert_Bancor, kyberReserve)

    def API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve(self, token, amountsToConvert_Kyber, kyberReserve):
        return API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched(self.contractAddress_Utility, self.contractObject_Utility,
                                                                                  token, amountsToConvert_Kyber, kyberReserve)

    def API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve(self, token, amountsToConvert_Uniswap, kyberReserve):
        return API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched(self.contractAddress_Utility, self.contractObject_Utility,
                                                                                  token, amountsToConvert_Uniswap, kyberReserve)

    # def API_GetManyArbitrages_BuyOn0xv2SellOnBancor(self, token, amountsToConvert_0xv2, order):
    #     return API_GetManyArbitrages_BuyOn0xv2SellOnBancor(self.contractAddress_Utility, self.contractObject_Utility,
    #                                                        token, amountsToConvert_0xv2, order)
    #
    # def API_GetManyArbitrages_BuyOnBancorSellOn0xv2(self, token, amountsToConvert_Bancor, order):
    #     return API_GetManyArbitrages_BuyOnBancorSellOn0xv2(self.contractAddress_Utility, self.contractObject_Utility,
    #                                                        token, amountsToConvert_Bancor, order)
    #
    # def API_GetManyArbitrages_BuyOn0xv2SellOnKyber(self, token, amountsToConvert_0xv2, order):
    #     return API_GetManyArbitrages_BuyOn0xv2SellOnKyber(self.contractAddress_Utility, self.contractObject_Utility,
    #                                                       token, amountsToConvert_0xv2, order)
    #
    # def API_GetManyArbitrages_BuyOnKyberSellOn0xv2(self, token, amountsToConvert_Kyber, order):
    #     return API_GetManyArbitrages_BuyOnKyberSellOn0xv2(self.contractAddress_Utility, self.contractObject_Utility,
    #                                                       token, amountsToConvert_Kyber, order)

    @staticmethod
    def ConsiderOverridingNumOfTimesToTrade_BasedOnTradeQuantityAndSetTokenData(
            numOfTimesToTrade, quoteTokensToSpend_etherUnits, maxQuoteTokenAvailableInSetToken, expectedProfit_eth=None):

        threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft = 2
        threshold_profit_eth = 0.1

        # If it's trying to trade near the max amount of quantity that's remaining in the setTokenData
        if (maxQuoteTokenAvailableInSetToken * 0.66) < quoteTokensToSpend_etherUnits:
            # Lower numOfTimesToTrade to just 1
            PrintAndLog_FuncNameHeader("Reducing numOfTimesToTrade to 1 because quoteTokensToSpend_etherUnits is close to exceeding the maxQuoteTokenAvailableInSetToken")
            return 1

        # Decide if we'll allow this to trade more than a couple times
        if numOfTimesToTrade > threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft:
            # We will only consider trading more than a couple times if there's a lot of assets left in the set
            if maxQuoteTokenAvailableInSetToken > (quoteTokensToSpend_etherUnits * (threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft + 2)):
                # Optionally enforce expectedProfit_eth
                # If there's NOT a lot of profit to be made, no point in queueing up a ton of trades since we're walking a tight line
                if (not expectedProfit_eth) or (expectedProfit_eth and expectedProfit_eth > threshold_profit_eth):
                    PrintAndLog_FuncNameHeader("Returning original numOfTimesToTrade because there's enough depth on set")
                    return numOfTimesToTrade

                else:
                    PrintAndLog_FuncNameHeader(
                        "Returning threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft there's enough depth on set but our expectedProfit_eth (" + str(
                            expectedProfit_eth) + ") isn't high enough compared to our threshold_profit_eth (" + str(threshold_profit_eth) + ")")
                    return threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft

            else:
                PrintAndLog_FuncNameHeader("Returning threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft there's not enough depth on set to go any higher")
                return threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft

        PrintAndLog_FuncNameHeader("quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
        PrintAndLog_FuncNameHeader("maxQuoteTokenAvailableInSetToken = " + str(maxQuoteTokenAvailableInSetToken))
        PrintAndLog_FuncNameHeader("numOfTimesToTrade = " + str(numOfTimesToTrade))
        PrintAndLog_FuncNameHeader("threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft = " + str(threshold_numOfTimesToTrade_whenSetHasALotOfAssetsLeft))
        PrintAndLog_FuncNameHeader("expectedProfit_eth = " + str(expectedProfit_eth))
        message = "Should not have made it this far. A bug must have occurred. Returning 1 just to be safe so it doesn't crash but this needs investigated!"
        PrintAndLog_FuncNameHeader(message)
        Libraries.core.API_PostOperatorNotification(message)
        return 1


class HistoricTrade:
    txHash = None
    inputData = None
    toAddress = None
    gasPrice_gwei = None

    quoteToken = None
    internalTx_receive_quoteToken = None
    internalTx_send_quoteToken = None
    internalTx_receive_baseToken = None
    internalTx_send_baseToken = None

    gasUsed = None
    gasPrice = None
    txReceiptBlockStatus = None

    def __init__(self, _txHash, _inputData, _toAddress, _gasPrice_gwei):
        self.txHash = _txHash
        self.inputData = _inputData
        self.toAddress = _toAddress
        self.gasPrice_gwei = _gasPrice_gwei

        self.quoteToken = None
        self.internalTx_receive_quoteToken = 0
        self.internalTx_send_quoteToken = 0
        self.internalTx_receive_baseToken = 0
        self.internalTx_send_baseToken = 0

        self.gasUsed = None
        self.gasPrice = None
        self.txReceiptBlockStatus = None

    def CalculateProfit(self):
        profit = 0

        # I received ETH only if the tx did not revert
        if self.DidTxSucceed() and self.internalTx_receive_quoteToken:
            profit += self.internalTx_receive_quoteToken

        # I sent ETH only if the tx did not revert
        if self.DidTxSucceed() and self.internalTx_send_quoteToken:
            profit -= self.internalTx_send_quoteToken

        # I always pay gas if the tx mined in
        profit -= self.CalculateEthPaidForGas()

        return profit

    def CalculateEthPaidForGas(self):
        return Libraries.core.Calculate_GasUsedOnTransaction_Ether_GivenGasPriceGweiAndGasUsedGwei(
            self.gasPrice, self.gasUsed)

    def DidTxSucceed(self):
        return self.txReceiptBlockStatus == Libraries.transactions.TxReceiptBlockStatus.Succeeded

    def IsHit(self):
        # This trade is considered a hit if the tx was successfully mined in and we traded
        if self.DidTxSucceed() and self.internalTx_send_quoteToken > 0 and self.internalTx_receive_quoteToken > 0:
            return True
        # Otherwise it's considered a miss
        else:
            return False


class HistoricTrade_EtherScan(HistoricTrade):
    pass


def CreateNinjaObject(ninjaType):
    global Contract_Ninja_A
    global Contract_Ninja_B
    global Contract_Ninja_C
    global SupportedQuoteTokensList_Ninja_B
    global SupportedQuoteTokensList_Ninja_C
    global Contract_NinjaUtility
    global NinjaObjectsList

    import Contracts.contracts

    ninjaObject = None

    if ninjaType == NinjaType.A:
        supportedQuoteTokensList = [Exchanges.keeperDAO.EthTokenContract.lower()]

        exchangeNamePairList = [
            [ExchangeName_Uniswap, ExchangeName_Kyber],
        ]

        ninjaObject = Ninja(ninjaType, Contracts.contracts.Contract_Ninja_DiamondProxy.address, Contracts.contracts.Contract_Ninja_Trade_A,
                            None, None, supportedQuoteTokensList, exchangeNamePairList)

    elif ninjaType == NinjaType.B:
        supportedQuoteTokensList = [Exchanges.keeperDAO.EthTokenContract.lower()]

        exchangeNamePairList = [
            [ExchangeName_Kyber, ExchangeName_Set],
            [ExchangeName_Set, ExchangeName_0xMesh],
        ]

        ninjaObject = Ninja(ninjaType, Contracts.contracts.Contract_Ninja_DiamondProxy.address, Contracts.contracts.Contract_Ninja_Trade_B,
                            None, None, supportedQuoteTokensList, exchangeNamePairList)

        # Setting restrictions on which sets this can trade against
        ninjaObject.SetSetsTokenList_RequiredTokenList([Exchanges.set.Contract_WETH.lower()])

    elif ninjaType == NinjaType.C:
        supportedQuoteTokensList = [
            Exchanges.set.Contract_USDC.lower(),
            Exchanges.set.Contract_WBTC.lower(),
        ]

        exchangeNamePairList = [
            [ExchangeName_Kyber, ExchangeName_Set],
            [ExchangeName_Set, ExchangeName_Uniswap],
        ]

        ninjaObject = Ninja(ninjaType, Contracts.contracts.Contract_Ninja_DiamondProxy.address, Contracts.contracts.Contract_Ninja_Trade_C,
                            None, None, supportedQuoteTokensList, exchangeNamePairList)

        # Setting restrictions on which sets this can trade against
        ninjaObject.SetSetsTokenList_RequiredTokenList(supportedQuoteTokensList)
        # All sets that include WETH are more optimally traded via NinjaType.B because
        # token to token trades are more expensive which means the profit margins are smaller
        # So I'm only using NinjaType.C for sets that do not include WETH at all
        ninjaObject.SetSetsTokenList_ExcludedTokenList([Exchanges.set.Contract_WETH.lower()])

    elif ninjaType == NinjaType.Generic:
        # supportedQuoteTokensList = [Exchanges.keeperDAO.EthTokenContract.lower()]
        supportedQuoteTokensList = Exchanges.keeperDAO.GetBorrowableAssetsList()

        ninjaObject = Ninja(ninjaType, Contracts.contracts.Contract_Ninja_DiamondProxy.address, Contracts.contracts.Contract_Ninja_Trade_3,
                            None, None, supportedQuoteTokensList, [])

    else:
        raise Exception("ninjaType = " + str(ninjaType) + " is not valid")

    NinjaObjectsList.append(ninjaObject)
    return ninjaObject


def GetNinjaObjectGivenTokens(quoteToken, baseToken):
    global NinjaObjectsList

    # Find the corresponding ninjaObject
    for ninjaObject in NinjaObjectsList:
        # To get the correct NinjaObject:
        # 1.) I must find the quoteToken in the requiredTokenList_Set
        # 2.) Neither quoteToken nor baseToken can be in the excludedTokenList_Set
        if quoteToken.lower() in ninjaObject.requiredTokenList_Set and \
                quoteToken.lower() not in ninjaObject.excludedTokenList_Set and \
                baseToken.lower() not in ninjaObject.excludedTokenList_Set:
            return ninjaObject

    raise Exception("Could not find corresponding ninjaObject to match quoteToken = " + str(quoteToken) + " and baseToken = " + str(baseToken))


# def GetNinjaObjectGivenExchangeNamePairList(quoteToken_toMatch, exchangeNamePairList_toMatch):
#     global NinjaObjectsList
#
#     # Make sure the list is alphabetized
#     exchangeNamePairList_toMatch.sort()
#
#     # Find the corresponding ninjaObject
#     for ninjaObject in NinjaObjectsList:
#         PrintAndLog_FuncNameHeader("Comparing quoteToken_toMatch " + str(quoteToken_toMatch) + " with ninjaObject.supportedQuoteTokensList " + str(
#             ninjaObject.supportedQuoteTokensList))
#         if quoteToken_toMatch.lower() in ninjaObject.supportedQuoteTokensList:
#             PrintAndLog_FuncNameHeader("Matched quoteToken to ninjaObject " + str(ninjaObject.ninjaType))
#             for exchangeNamePair in ninjaObject.exchangeNamePairList:
#                 PrintAndLog_FuncNameHeader("Comparing exchangeNamePair " + str(exchangeNamePair) + " with exchangeNamePairList_toMatch " + str(exchangeNamePairList_toMatch))
#                 if exchangeNamePair == exchangeNamePairList_toMatch:
#                     PrintAndLog_FuncNameHeader("Matched exchangeNamePair to ninjaObject " + str(ninjaObject.ninjaType))
#                     return ninjaObject


def API_GetManyArbitrages_BuyOnBancorSellOnKyber(contractAddress, contractObject,
                                                 token, amountsToConvert_Bancor):
    # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber with token.bancorTokenConverterContract = " + str(token.bancorTokenConverterContract))
    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
    ]

    kwargs = {
        'addressArray': addressArray,
        'amountsToConvert_Bancor': amountsToConvert_Bancor,
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": contractObject.encodeABI('getManyArbitrages_buyOnBancorSellOnKyber', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
            },
        ]
    }
    # before = datetime.datetime.now()
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber duration_s = " + str(round(duration_s, 1)))
        responseData = response.content
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber dataList = " + str(dataList))
        returnList = ParseResponseData_GetManyArbitrages(dataList)
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber returnList = " + str(returnList))
        return returnList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_GetManyArbitrages_BuyOnKyberSellOnBancor(contractAddress, contractObject,
                                                 token, amountsToConvert_Kyber):
    # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor with token.bancorTokenConverterContract = " + str(token.bancorTokenConverterContract))
    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
    ]

    kwargs = {
        'addressArray': addressArray,
        'amountsToConvert_Kyber': amountsToConvert_Kyber,
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": contractObject.encodeABI('getManyArbitrages_buyOnKyberSellOnBancor', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor dataList = " + str(dataList))
        returnList = ParseResponseData_GetManyArbitrages(dataList)
        # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor returnList = " + str(returnList))
        return returnList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve(contractAddress, contractObject,
                                                                   token, amountsToConvert_Bancor, kyberReserve):
    # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve with token.bancorTokenConverterContract = " + str(token.bancorTokenConverterContract))
    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
    ]

    kwargs = {
        'addressArray': addressArray,
        'amountsToConvert_Bancor': amountsToConvert_Bancor,
    }
    # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": contractObject.encodeABI('getManyArbitrages_buyOnBancorSellOnKyber_usingKyberReserve', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
            },
        ]
    }

    # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve: payload = " + str(payload))
    before = datetime.datetime.now()
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve duration_s = " + str(round(duration_s, 1)))
        responseData = response.content
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve dataList = " + str(dataList))
        returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt = ParseResponseData_GetManyArbitrages_UsingKyberReserve(
            dataList, len(amountsToConvert_Bancor), token.decimals)
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve returnList_Rate = " + str(returnList_Rate))
        # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve quantityBnt = " + str(quantityBnt))
        return returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber_UsingKyberReserve response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched(contractAddress, contractObject,
                                                                      token, amountsToConvert_Bancor, kyberReserve):
    # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched with token.bancorTokenConverterContract = " + str(token.bancorTokenConverterContract))
    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
    ]

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for amount in amountsToConvert_Bancor:
        # Increment the id each time around
        payloadId += 1

        # Pair the amount with the Id so I can know which is which in the response
        payloadIdDict[payloadId] = amount

        kwargs = {
            'addressArray': addressArray,
            'amountsToConvert_Bancor': [amount],
        }

        # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched kwargs = " + str(kwargs))
        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": contractObject.encodeABI('getManyArbitrages_buyOnBancorSellOnKyber_usingKyberReserve', kwargs=kwargs),
                    "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, True, False, True, False)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched responseData = " + str(responseData))
        jData = json.loads(responseData)

        returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt = \
            ParseBatchedResponseData_GetManyArbitrages_UsingKyberReserve(jData, payloadIdDict, token.decimals, Exchanges.bancor.Decimals_bnt)
        # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched returnList_Rate = " + str(returnList_Rate))
        # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched quantityBnt = " + str(quantityBnt))
        return returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetArbitrage_BuyOnBancorSellOnKyber_UsingKyberReserve_Batched response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve(contractAddress, contractObject,
                                                                   token, amountsToConvert_Kyber, kyberReserve):
    # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve with token.bancorTokenConverterContract = " + str(token.bancorTokenConverterContract))
    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
    ]

    kwargs = {
        'addressArray': addressArray,
        'amountsToConvert_Kyber': amountsToConvert_Kyber,
    }
    PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": contractObject.encodeABI('getManyArbitrages_buyOnKyberSellOnBancor_usingKyberReserve', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve dataList = " + str(dataList))
        returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt = ParseResponseData_GetManyArbitrages_UsingKyberReserve(
            dataList, len(amountsToConvert_Kyber), token.decimals)
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve returnList_Eth = " + str(returnList_Eth))
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve returnList_Tokens = " + str(returnList_Tokens))
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve returnList_Rate = " + str(returnList_Rate))
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve quantityBnt = " + str(quantityBnt))
        return returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnBancor_UsingKyberReserve response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched(contractAddress, contractObject,
                                                                      token, amountsToConvert_Kyber, kyberReserve):
    # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched with token.bancorTokenConverterContract = " + str(token.bancorTokenConverterContract))
    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
    ]

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for amount in amountsToConvert_Kyber:
        # Increment the id each time around
        payloadId += 1

        # Pair the amount with the Id so I can know which is which in the response
        payloadIdDict[payloadId] = amount

        kwargs = {
            'addressArray': addressArray,
            'amountsToConvert_Kyber': [amount],
        }

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": contractObject.encodeABI('getManyArbitrages_buyOnKyberSellOnBancor_usingKyberReserve', kwargs=kwargs),
                    "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
                },
            ]
        }
        payload_total.append(payload)

    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, True, False, True, False)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched responseData = " + str(responseData))
        jData = json.loads(responseData)

        returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt = \
            ParseBatchedResponseData_GetManyArbitrages_UsingKyberReserve(jData, payloadIdDict, token.decimals, Exchanges.bancor.Decimals_bnt)
        PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched returnList_Eth = " + str(returnList_Eth))
        PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched returnList_Tokens = " + str(returnList_Tokens))
        PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched returnList_Rate = " + str(returnList_Rate))
        PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched quantityBnt = " + str(quantityBnt))
        return returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnBancor_UsingKyberReserve_Batched response was not ok response = " + str(response))
        response.raise_for_status()


# def API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve(contractAddress, contractObject,
#                                                                     token, amountsToConvert_Uniswap, kyberReserve):
#     # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve with kyberReserve = " + str(kyberReserve))
#
#     # function getManyArbitrages_buyOnUniswapSellOnKyber_usingKyberReserve(
#     #         address[] memory addressArray, uint[] memory amountsToConvert_Uniswap
#     #         ) public view returns (uint[] memory, uint[] memory, uint[] memory, uint)
#     #     {
#     #         // addressArray[0] is uniswapContract
#     #         // addressArray[1] is tokenToTrade
#     #         // addressArray[2] is kyberReserve
#
#     uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])
#
#     addressArray = [
#         Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
#     ]
#
#     kwargs = {
#         'addressArray': addressArray,
#         'amountsToConvert_Uniswap': amountsToConvert_Uniswap,
#     }
#     PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve: kwargs = " + str(kwargs))
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": contractObject.encodeABI('getManyArbitrages_buyOnUniswapSellOnKyber_usingKyberReserve', kwargs=kwargs),
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
#             },
#         ]
#     }
#     # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve: payload = " + str(payload))
#     # before = datetime.datetime.now()
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         # duration_s = (datetime.datetime.now() - before).total_seconds()
#         # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve duration_s = " + str(round(duration_s, 1)))
#         responseData = response.content
#         # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve jData = " + str(jData))
#         data = jData['result']
#         dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve dataList = " + str(dataList))
#         returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth = ParseResponseData_GetManyArbitrages_UsingKyberReserve(
#             dataList, len(amountsToConvert_Uniswap), token.decimals)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve returnList_Eth = " + str(returnList_Eth))
#         # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve returnList_Tokens = " + str(returnList_Tokens))
#         # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve returnList_Rate = " + str(returnList_Rate))
#         # PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve quantityEth = " + str(quantityEth))
#         return returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetManyArbitrages_BuyOnUniswapSellOnKyber_UsingKyberReserve response was not ok response = " + str(response))
#         response.raise_for_status()
#
#     return None


def API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched(contractAddress, contractObject, token,
                                                                       amountsToConvert_Uniswap, kyberReserve):
    # PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched with kyberReserve = " + str(kyberReserve))

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
    ]

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for amount in amountsToConvert_Uniswap:
        # Increment the id each time around
        payloadId += 1

        # Pair the amount with the Id so I can know which is which in the response
        payloadIdDict[payloadId] = amount

        kwargs = {
            'addressArray': addressArray,
            'amountsToConvert_Uniswap': [amount],
        }

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": contractObject.encodeABI('getManyArbitrages_buyOnUniswapSellOnKyber_usingKyberReserve', kwargs=kwargs),
                    "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched: payload_total = " + str(payload_total))
    # before = datetime.datetime.now()
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, True, False, True, False)
    if response.ok:
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched duration_s = " + str(round(duration_s, 1)))
        responseData = response.content
        # PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched responseData = " + str(responseData))
        jData = json.loads(responseData)

        returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth = \
            ParseBatchedResponseData_GetManyArbitrages_UsingKyberReserve(jData, payloadIdDict, token.decimals, Libraries.core.Ether_Decimals)

        # PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched returnList_Rate = " + str(returnList_Rate))
        # PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched quantityEth = " + str(quantityEth))
        return returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetArbitrage_BuyOnUniswapSellOnKyber_UsingKyberReserve_Batched response was not ok response = " + str(response))
        response.raise_for_status()


# def API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve(contractAddress, contractObject,
#                                                                     token, amountsToConvert_Kyber, kyberReserve):
#     # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve with kyberReserve = " + str(kyberReserve))
#
#     # function getManyArbitrages_buyOnKyberSellOnUniswap_usingKyberReserve(
#     #         address[] memory addressArray, uint[] memory amountsToConvert_Kyber
#     #         ) public view returns (uint[] memory, uint[] memory, uint[] memory, uint)
#     #     {
#     #         // addressArray[0] is uniswapContract
#     #         // addressArray[1] is tokenToTrade
#     #         // addressArray[2] is kyberReserve
#
#     uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])
#
#     addressArray = [
#         Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
#     ]
#
#     kwargs = {
#         'addressArray': addressArray,
#         'amountsToConvert_Kyber': amountsToConvert_Kyber,
#     }
#     PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve: kwargs = " + str(kwargs))
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": contractObject.encodeABI('getManyArbitrages_buyOnKyberSellOnUniswap_usingKyberReserve', kwargs=kwargs),
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
#             },
#         ]
#     }
#     # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve: payload = " + str(payload))
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         responseData = response.content
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve jData = " + str(jData))
#         data = jData['result']
#         dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve dataList = " + str(dataList))
#         returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth = ParseResponseData_GetManyArbitrages_UsingKyberReserve(
#             dataList, len(amountsToConvert_Kyber), token.decimals)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve returnList_Eth = " + str(returnList_Eth))
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve returnList_Tokens = " + str(returnList_Tokens))
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve returnList_Rate = " + str(returnList_Rate))
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve quantityEth = " + str(quantityEth))
#         return returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnUniswap_UsingKyberReserve response was not ok response = " + str(response))
#         response.raise_for_status()
#
#     return None


def API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched(contractAddress, contractObject,
                                                                       token, amountsToConvert_Kyber, kyberReserve):
    # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched with kyberReserve = " + str(kyberReserve))

    # function getManyArbitrages_buyOnKyberSellOnUniswap_usingKyberReserve(
    #         address[] memory addressArray, uint[] memory amountsToConvert_Kyber
    #         ) public view returns (uint[] memory, uint[] memory, uint[] memory, uint)
    #     {
    #         // addressArray[0] is uniswapContract
    #         // addressArray[1] is tokenToTrade
    #         // addressArray[2] is kyberReserve

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy),
    ]

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for amount in amountsToConvert_Kyber:
        # Increment the id each time around
        payloadId += 1

        # Pair the amount with the Id so I can know which is which in the response
        payloadIdDict[payloadId] = amount

        kwargs = {
            'addressArray': addressArray,
            'amountsToConvert_Kyber': [amount],
        }

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": contractObject.encodeABI('getManyArbitrages_buyOnKyberSellOnUniswap_usingKyberReserve', kwargs=kwargs),
                    "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched: payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, True, False, True, False)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched responseData = " + str(responseData))
        jData = json.loads(responseData)

        returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth = \
            ParseBatchedResponseData_GetManyArbitrages_UsingKyberReserve(jData, payloadIdDict, token.decimals, Libraries.core.Ether_Decimals)

        # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched returnList_Rate = " + str(returnList_Rate))
        # PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched quantityEth = " + str(quantityEth))
        return returnList_Eth, returnList_Tokens, returnList_Rate, quantityEth

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetArbitrage_BuyOnKyberSellOnUniswap_UsingKyberReserve_Batched response was not ok response = " + str(response))
        response.raise_for_status()


# def API_GetArbitrage_BuyOn0xv2SellOnBancor(token, amountToConvert_0xv2, order):
#     import Contracts.contracts
#
#     addressArray = [
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
#         # Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[token.tokenName.upper()]),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
#     ]
#
#     # PrintAndLog("order.takerTokenAddress = " + str(order.takerTokenAddress))
#     # PrintAndLog("order.makerTokenAddress = " + str(order.makerTokenAddress))
#     fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
#     toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))
#
#     orderTuple = order.GenerateOrderTupleForTransaction()
#
#     params = 'address[],bytes,bytes,uint256,' + Exchanges.zrxV2.OrderTupleString
#     # PrintAndLog("params = " + str(params))
#     method_signature = Web3.sha3(text=f"getArbitrage_buyOn0xv2SellOnBancor({params})")[0:4]
#     # PrintAndLog("API_GetArbitrage_BuyOn0xv2SellOnBancor method_signature = " + str(method_signature))
#     PrintAndLog("method_parameters before encoding fromAssetData = " + str(fromAssetData) + ", toAssetData = " + str(toAssetData) + ", orderTuple = " + str(
#         orderTuple) + ", amountToConvert_0xv2 = " + str(amountToConvert_0xv2))
#     method_parameters = encode_single(f"({params})", [
#         addressArray,
#         fromAssetData,
#         toAssetData,
#         int(amountToConvert_0xv2),
#         orderTuple
#     ])
#     PrintAndLog("API_GetArbitrage_BuyOn0xv2SellOnBancor method_parameters = " + str(method_parameters))
#     data_hex = '0x' + (method_signature + method_parameters).hex()
#     # PrintAndLog("data_hex: " + str(data_hex))
#
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": data_hex,
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(Contract_Ninja)
#             },
#         ]
#     }
#     # PrintAndLog("API_GetArbitrage_BuyOn0xv2SellOnBancor payload = " + str(payload))
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         responseData = response.content
#         PrintAndLog("API_GetArbitrage_BuyOn0xv2SellOnBancor responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         PrintAndLog("API_GetArbitrage_BuyOn0xv2SellOnBancor jData = " + str(jData))
#         # result_wei = Libraries.core.ConvertHexToInt(jData['result'])
#         # PrintAndLog("API_GetArbitrage_BuyOn0xv2SellOnBancor result_wei = " + str(result_wei))
#         # return result_wei
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetArbitrage_BuyOn0xv2SellOnBancor response was not ok response = " + str(response))
#         response.raise_for_status()


# def API_GetArbitrage_BuyOnBancorSellOn0xv2(token, amountToConvert_Bancor, order):
#     import Contracts.contracts
#
#     addressArray = [
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
#         # Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[token.tokenName.upper()]),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
#     ]
#
#     # PrintAndLog("order.takerTokenAddress = " + str(order.takerTokenAddress))
#     # PrintAndLog("order.makerTokenAddress = " + str(order.makerTokenAddress))
#     fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
#     toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))
#
#     orderTuple = order.GenerateOrderTupleForTransaction()
#
#     params = 'address[],bytes,bytes,uint256,' + Exchanges.zrxV2.OrderTupleString
#     PrintAndLog("params = " + str(params))
#     method_signature = Web3.sha3(text=f"getArbitrage_buyOnBancorSellOn0xv2({params})")[0:4]
#     PrintAndLog("API_GetArbitrage_BuyOnBancorSellOn0xv2 method_signature = " + str(method_signature))
#     PrintAndLog("method_parameters before encoding fromAssetData = " + str(fromAssetData) + ", toAssetData = " + str(toAssetData) + ", orderTuple = " + str(
#         orderTuple) + ", amountToConvert_Bancor = " + str(amountToConvert_Bancor))
#     method_parameters = encode_single(f"({params})", [
#         addressArray,
#         fromAssetData,
#         toAssetData,
#         amountToConvert_Bancor,
#         orderTuple
#     ])
#     # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOn0xv2 method_parameters = " + str(method_parameters))
#     data_hex = '0x' + (method_signature + method_parameters).hex()
#     # PrintAndLog("data_hex: " + str(data_hex))
#
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": data_hex,
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(Contract_Ninja)
#             },
#         ]
#     }
#     # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOn0xv2 payload: " + str(payload))
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         responseData = response.content
#         # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOn0xv2 responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         PrintAndLog("API_GetArbitrage_BuyOnBancorSellOn0xv2 jData = " + str(jData))
#         # data = jData['result']
#         # dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
#         # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOn0xv2 dataList = " + str(dataList))
#         # returnList = ParseResponseData_GetManyArbitrages(dataList)
#         # PrintAndLog("API_GetArbitrage_BuyOnBancorSellOn0xv2 returnList = " + str(returnList))
#         # return returnList
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetArbitrage_BuyOnBancorSellOn0xv2 response was not ok response = " + str(response))
#         response.raise_for_status()


# def API_GetManyArbitrages_BuyOn0xv2SellOnBancor(contractAddress, contractObject, token, amountsToConvert_0xv2, order):
#     addressArray = [
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
#         # Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[token.tokenName.upper()]),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
#     ]
#
#     # PrintAndLog("order.takerTokenAddress = " + str(order.takerTokenAddress))
#     # PrintAndLog("order.makerTokenAddress = " + str(order.makerTokenAddress))
#     fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
#     toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))
#
#     orderTuple = order.GenerateOrderTupleForTransaction()
#
#     params = 'address[],bytes,bytes,uint256[],' + Exchanges.zrxV2.OrderTupleString
#     PrintAndLog("params = " + str(params))
#     method_signature = Web3.sha3(text=f"getManyArbitrages_buyOn0xv2SellOnBancor({params})")[0:4]
#     PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnBancor method_signature = " + str(method_signature))
#     PrintAndLog("method_parameters before encoding addressArray = " + str(addressArray) + ", fromAssetData = " + str(fromAssetData) + ", toAssetData = " + str(
#         toAssetData) + ", amountsToConvert_0xv2 = " + str(amountsToConvert_0xv2) + ", orderTuple = " + str(orderTuple))
#     method_parameters = encode_single(f"({params})", [
#         addressArray,
#         fromAssetData,
#         toAssetData,
#         amountsToConvert_0xv2,
#         orderTuple
#     ])
#     # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnBancor method_parameters = " + str(method_parameters))
#     data_hex = '0x' + (method_signature + method_parameters).hex()
#     # PrintAndLog("data_hex: " + str(data_hex))
#
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": data_hex,
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
#             },
#         ]
#     }
#     # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnBancor payload: " + str(payload))
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         responseData = response.content
#         # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnBancor responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnBancor jData = " + str(jData))
#         data = jData['result']
#         dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
#         # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnBancor dataList = " + str(dataList))
#         returnList = ParseResponseData_GetManyArbitrages(dataList)
#         PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnBancor returnList = " + str(returnList))
#         return returnList
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnBancor response was not ok response = " + str(response))
#         response.raise_for_status()


# def API_GetManyArbitrages_BuyOnBancorSellOn0xv2(contractAddress, contractObject, token, amountsToConvert_Bancor, order):
#     addressArray = [
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
#         # Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ContractDict_Converter[token.tokenName.upper()]),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.bancorTokenConverterContract),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
#     ]
#
#     # PrintAndLog("order.takerTokenAddress = " + str(order.takerTokenAddress))
#     # PrintAndLog("order.makerTokenAddress = " + str(order.makerTokenAddress))
#     fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
#     toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))
#
#     orderTuple = order.GenerateOrderTupleForTransaction()
#
#     params = 'address[],bytes,bytes,uint256[],' + Exchanges.zrxV2.OrderTupleString
#     PrintAndLog("params = " + str(params))
#     method_signature = Web3.sha3(text=f"getManyArbitrages_buyOnBancorSellOn0xv2({params})")[0:4]
#     PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOn0xv2 method_signature = " + str(method_signature))
#     PrintAndLog("method_parameters before encoding addressArray = " + str(addressArray) + ", fromAssetData = " + str(fromAssetData) + ", toAssetData = " + str(
#         toAssetData) + ", amountsToConvert_Bancor = " + str(amountsToConvert_Bancor) + ", orderTuple = " + str(orderTuple))
#     method_parameters = encode_single(f"({params})", [
#         addressArray,
#         fromAssetData,
#         toAssetData,
#         amountsToConvert_Bancor,
#         orderTuple
#     ])
#     # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOn0xv2 method_parameters = " + str(method_parameters))
#     data_hex = '0x' + (method_signature + method_parameters).hex()
#     # PrintAndLog("data_hex: " + str(data_hex))
#
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": data_hex,
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
#             },
#         ]
#     }
#     # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOn0xv2 payload: " + str(payload))
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         responseData = response.content
#         # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOn0xv2 responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOn0xv2 jData = " + str(jData))
#         data = jData['result']
#         dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOn0xv2 dataList = " + str(dataList))
#         returnList = ParseResponseData_GetManyArbitrages(dataList)
#         PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOn0xv2 returnList = " + str(returnList))
#         return returnList
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOn0xv2 response was not ok response = " + str(response))
#         response.raise_for_status()


# def API_GetManyArbitrages_BuyOnKyberSellOn0xv2(contractAddress, contractObject, token, amountsToConvert_Kyber, order):
#     # function getManyArbitrages_buyOnKyberSellOn0xv2(
#     #     address[] memory addressArray,
#     #     bytes memory fromAssetData,
#     #     bytes memory toAssetData,
#     #     uint[] memory amountsToConvert_Kyber,
#     #     Order memory order
#
#     #   // addressArray[0] is tokenToTrade
#     #   // addressArray[1] is kybersEtherToken
#     #   // addressArray[2] is kyberProxyContract
#     addressArray = [
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
#     ]
#
#     # PrintAndLog("order.takerTokenAddress = " + str(order.takerTokenAddress))
#     # PrintAndLog("order.makerTokenAddress = " + str(order.makerTokenAddress))
#     fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
#     toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))
#
#     orderTuple = order.GenerateOrderTupleForTransaction()
#
#     params = 'address[],bytes,bytes,uint256[],' + Exchanges.zrxV2.OrderTupleString
#     PrintAndLog("params = " + str(params))
#     method_signature = Web3.sha3(text=f"getManyArbitrages_buyOnKyberSellOn0xv2({params})")[0:4]
#     PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOn0xv2 method_signature = " + str(method_signature))
#     PrintAndLog("method_parameters before encoding addressArray = " + str(addressArray) + ", fromAssetData = " + str(fromAssetData) + ", toAssetData = " + str(
#         toAssetData) + ", amountsToConvert_Kyber = " + str(amountsToConvert_Kyber) + ", orderTuple = " + str(orderTuple))
#     method_parameters = encode_single(f"({params})", [
#         addressArray,
#         fromAssetData,
#         toAssetData,
#         amountsToConvert_Kyber,
#         orderTuple
#     ])
#     # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOn0xv2 method_parameters = " + str(method_parameters))
#     data_hex = '0x' + (method_signature + method_parameters).hex()
#     # PrintAndLog("data_hex: " + str(data_hex))
#
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": data_hex,
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
#             },
#         ]
#     }
#     # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOn0xv2 payload: " + str(payload))
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         responseData = response.content
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOn0xv2 responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOn0xv2 jData = " + str(jData))
#         data = jData['result']
#         dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
#         # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOn0xv2 dataList = " + str(dataList))
#         returnList = ParseResponseData_GetManyArbitrages(dataList)
#         PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOn0xv2 returnList = " + str(returnList))
#         return returnList
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOn0xv2 response was not ok response = " + str(response))
#         response.raise_for_status()


# def API_GetManyArbitrages_BuyOn0xv2SellOnKyber(contractAddress, contractObject, token, amountsToConvert_0xv2, order):
#     # function getManyArbitrages_buyOn0xv2SellOnKyber(
#     #     address[] memory addressArray,
#     #     bytes memory fromAssetData,
#     #     bytes memory toAssetData,
#     #     uint[] memory amountsToConvert_0xv2,
#     #     Order memory order
#
#     # // addressArray[0] is tokenToTrade
#     # // addressArray[1] is kybersEtherToken
#     # // addressArray[2] is kyberProxyContract
#
#     addressArray = [
#         Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
#         Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
#     ]
#
#     # PrintAndLog("order.takerTokenAddress = " + str(order.takerTokenAddress))
#     # PrintAndLog("order.makerTokenAddress = " + str(order.makerTokenAddress))
#     fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
#     toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))
#
#     orderTuple = order.GenerateOrderTupleForTransaction()
#
#     params = 'address[],bytes,bytes,uint256[],' + Exchanges.zrxV2.OrderTupleString
#     PrintAndLog("params = " + str(params))
#     method_signature = Web3.sha3(text=f"getManyArbitrages_buyOn0xv2SellOnKyber({params})")[0:4]
#     PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnKyber method_signature = " + str(method_signature))
#     PrintAndLog("method_parameters before encoding addressArray = " + str(addressArray) + ", fromAssetData = " + str(fromAssetData) + ", toAssetData = " + str(
#         toAssetData) + ", amountsToConvert_0xv2 = " + str(amountsToConvert_0xv2) + ", orderTuple = " + str(orderTuple))
#     method_parameters = encode_single(f"({params})", [
#         addressArray,
#         fromAssetData,
#         toAssetData,
#         amountsToConvert_0xv2,
#         orderTuple
#     ])
#     # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnKyber method_parameters = " + str(method_parameters))
#     data_hex = '0x' + (method_signature + method_parameters).hex()
#     # PrintAndLog("data_hex: " + str(data_hex))
#
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": data_hex,
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
#             },
#         ]
#     }
#     # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnKyber payload: " + str(payload))
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         responseData = response.content
#         # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnKyber responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnKyber jData = " + str(jData))
#         data = jData['result']
#         dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
#         # PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnKyber dataList = " + str(dataList))
#         returnList = ParseResponseData_GetManyArbitrages(dataList)
#         PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnKyber returnList = " + str(returnList))
#         return returnList
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetManyArbitrages_BuyOn0xv2SellOnKyber response was not ok response = " + str(response))
#         response.raise_for_status()


def ParseResponseData_GetManyArbitrages(dataList, decimals=None):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))

    if not decimals:
        decimals = Libraries.core.Ether_Decimals

    returnList = []
    for data in dataList:
        value_base = Libraries.core.ConvertHexToInt(data)
        value_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(value_base), decimals)
        # PrintAndLog("value_ether = " + str(value_ether) + ", value_base = " + str(value_base))
        returnList.append(value_ether)

    return returnList


def ParseResponseData_GetManyArbitrages_UsingKyberReserve(dataList, dataLength, decimals_tokens):
    # [
    #   '0000000000000000000000000000000000000000000000000000000000000080',
    #   '0000000000000000000000000000000000000000000000000000000000000100',
    #   '0000000000000000000000000000000000000000000000000000000000000180',
    #   '000000000000000000000000000000000000000000004ab888601ae6ea156204',
    #   '0000000000000000000000000000000000000000000000000000000000000003',
    #   '00000000000000000000000000000000000000000000000000000000000f170e',
    #   '000000000000000000000000000000000000000000000000004644257460b83b',
    #   '00000000000000000000000000000000000000000000000028f66b3e256a4cf4',
    #   '0000000000000000000000000000000000000000000000000000000000000003',
    #   '0000000000000000000000000000000000000000000000000000000008b213ee',
    #   '000000000000000000000000000000000000000000000000287da0bc091a2674',
    #   '0000000000000000000000000000000000000000000000179ac6558bb3083f5f',
    #   '0000000000000000000000000000000000000000000000000000000000000003',
    #   '00000000000000000000000000000000000000000000000000181541243a37ae',
    #   '00000000000000000000000000000000000000000000000000181541243a37ae',
    #   '00000000000000000000000000000000000000000000000000181541243a37ae'
    # ]

    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    dataList.pop(0)

    # This value is not an array
    quantityBnt = Libraries.core.GetIntFromDataProperty_EtherUnits(dataList[0], Exchanges.bancor.Decimals_bnt)
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))

    # if not decimals_tokens:
    #     decimals_tokens = Libraries.core.Ether_Decimals

    returnList_Eth = []
    returnList_Tokens = []
    returnList_Rate = []
    for index, data in enumerate(dataList):
        # PrintAndLog("ParseResponseData_GetManyArbitrages_UsingKyberReserve: index = " + str(index) + ", data = " + str(data))

        # if index == (dataLength - 1):
        #     PrintAndLog("ParseResponseData_GetManyArbitrages_UsingKyberReserve: skipping this one because this isn't data, it's array/tuple structure that we don't need")
        #     continue

        if index <= (dataLength - 1):
            # PrintAndLog("ParseResponseData_GetManyArbitrages_UsingKyberReserve: this data is ETHER data")
            value_base = Libraries.core.ConvertHexToInt(data)
            value_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(value_base), Libraries.core.Ether_Decimals)
            # PrintAndLog("value_ether = " + str(value_ether) + ", value_base = " + str(value_base))
            returnList_Eth.append(value_ether)
        elif index > dataLength and index <= (2 * dataLength):
            # PrintAndLog("ParseResponseData_GetManyArbitrages_UsingKyberReserve: this data is TOKEN data")
            value_base = Libraries.core.ConvertHexToInt(data)
            value_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(value_base), decimals_tokens)
            # PrintAndLog("value_ether = " + str(value_ether) + ", value_base = " + str(value_base))
            returnList_Tokens.append(value_ether)
        elif index > ((2 * dataLength) + 1):
            # PrintAndLog("ParseResponseData_GetManyArbitrages_UsingKyberReserve: this data is RATE data")
            value_base = Libraries.core.ConvertHexToInt(data)
            value_ether = Libraries.core.ConvertBaseAmountToEtherAmount(float(value_base), Libraries.core.Ether_Decimals)
            # PrintAndLog("value_ether = " + str(value_ether) + ", value_base = " + str(value_base))
            returnList_Rate.append(value_ether)
        # else:
        #     PrintAndLog("ParseResponseData_GetManyArbitrages_UsingKyberReserve: skipping this one because this isn't data, it's array/tuple structure that we don't need")

    return returnList_Eth, returnList_Tokens, returnList_Rate, quantityBnt


def ParseBatchedResponseData_GetManyArbitrages_UsingKyberReserve(jData, payloadIdDict, token_decimals, quantity_decimals):
    # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
    returnList_Eth = []
    returnList_Tokens = []
    returnList_Rate = []
    quantity = None

    for id in payloadIdDict:
        for batchedResult in jData:
            if int(batchedResult['id']) == id:
                try:
                    if 'error' in batchedResult:
                        # Use zero for values like ether received so that this clearly shows if you try to trade based on these params you get nothing.
                        returnList_Eth.append(0)
                        returnList_Tokens.append(0)
                        # Use None for things like rate, because you shouldn't even attempt to judge rate if the trade is not profitable or possible.
                        returnList_Rate.append(None)

                    else:
                        result = batchedResult['result']
                        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(result),
                                                                                             Libraries.core.LengthOfDataProperty)
                        # PrintAndLog("found Id " + str(id) + " in the response. result = " + str(result))
                        # PrintAndLog("dataList = " + str(dataList))
                        quantity = Libraries.core.GetIntFromDataProperty_EtherUnits(dataList[3], quantity_decimals)
                        returnList_Eth.append(Libraries.core.ConvertBaseAmountToEtherAmount(Libraries.core.ConvertHexToInt(dataList[5]), Libraries.core.Ether_Decimals))
                        returnList_Tokens.append(Libraries.core.ConvertBaseAmountToEtherAmount(Libraries.core.ConvertHexToInt(dataList[7]), token_decimals))
                        returnList_Rate.append(Libraries.core.ConvertBaseAmountToEtherAmount(Libraries.core.ConvertHexToInt(dataList[9]), Libraries.core.Ether_Decimals))

                except:
                    message = "ParseResponseData_GetManyArbitrages_UsingKyberReserve: exception  " + traceback.format_exc()
                    PrintAndLogError(message)
                    pass

                break

    return returnList_Eth, returnList_Tokens, returnList_Rate, quantity


# def API_GetReturn_0xv2(amountToConvert_weiUnits, order, fromAssetData, toAssetData):
#     import Contracts.contracts
#
#     # function getReturn_0xv2(
#     #     uint amountToConvert, Order memory order,
#     #     bytes memory fromAssetData, bytes memory toAssetData
#     #     ) public view returns (uint expectedReturn_Tokens)
#     # {
#
#     orderTuple = order.GenerateOrderTupleForTransaction()
#
#     params = 'uint256,' + Exchanges.zrxV2.OrderTupleString + ',bytes,bytes'
#     # PrintAndLog("params = " + str(params))
#     method_signature = Web3.sha3(text=f"getReturn_0xv2({params})")[0:4]
#     # PrintAndLog("API_GetReturn_0xv2 method_signature = " + str(method_signature))
#     PrintAndLog("method_parameters before encoding fromAssetData = " + str(fromAssetData) + ", toAssetData = " + str(
#         toAssetData) + ", orderTuple = " + str(orderTuple) + ", amountToConvert_weiUnits = " + str(amountToConvert_weiUnits))
#     method_parameters = encode_single(f"({params})", [
#         int(amountToConvert_weiUnits),
#         orderTuple,
#         Web3.toBytes(hexstr=str(fromAssetData)),
#         Web3.toBytes(hexstr=str(toAssetData))
#     ])
#     # PrintAndLog("API_GetReturn_0xv2 method_parameters = " + str(method_parameters))
#     data_hex = '0x' + (method_signature + method_parameters).hex()
#     # PrintAndLog("data_hex: " + str(data_hex))
#
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": data_hex,
#                 "to": Libraries.nodes.Instance_Web3.toChecksumAddress(Contract_Ninja)
#             },
#         ]
#     }
#     PrintAndLog("API_GetReturn_0xv2 payload = " + str(payload))
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
#     if response.ok:
#         responseData = response.content
#         # PrintAndLog("API_GetReturn_0xv2 responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         PrintAndLog("API_GetReturn_0xv2 jData = " + str(jData))
#         result_wei = Libraries.core.ConvertHexToInt(jData['result'])
#         PrintAndLog("API_GetReturn_0xv2 result_wei = " + str(result_wei))
#         return result_wei
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog("API_GetReturn_0xv2 response was not ok response = " + str(response))
#         response.raise_for_status()


def API_TradeTokensOnKyber(contractAddress, contractObject,
                           fromAddress, fromPrivateKey, msgValue_weiUnits,
                           src, sourceAmount_weiUnits, dest, maxDestAmount, minConversionRate_etherUnits, walletId,
                           gas, gasPrice):
    PrintAndLog("Ninja:API_TradeTokensOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    PrintAndLog("minConversionRate_etherUnits = " + str(minConversionRate_etherUnits))
    minConversionRate_weiUnits = Exchanges.kyber.ConvertMinConversionRateEtherToWei(minConversionRate_etherUnits)
    PrintAndLog("minConversionRate_weiUnits = " + str(minConversionRate_weiUnits))
    # function tradeTokensOnKyber(
    # 	    uint256 msgValue,
    # 	    address src,
    # 	    uint srcAmount,
    # 	    address dest,
    # 	    uint maxDestAmount,
    # 	    uint minConversionRate,
    # 	    address walletId
    # 	    ) public onlyOwnerOrWhitelist returns (uint)
    kwargs = {
        'msgValue': int(msgValue_weiUnits),
        'src': str(Libraries.nodes.Instance_Web3.toChecksumAddress(src)),
        'srcAmount': int(sourceAmount_weiUnits),
        'dest': str(Libraries.nodes.Instance_Web3.toChecksumAddress(dest)),
        'maxDestAmount': int(maxDestAmount),
        'minConversionRate': int(minConversionRate_weiUnits),
        'walletId': str(Libraries.nodes.Instance_Web3.toChecksumAddress(walletId)),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeTokensOnKyber', kwargs=kwargs)
    # PrintAndLog("data_hex: " + str(data_hex))

    # This function assumes that ether you'd like to use to trade is already deposited to the contract
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeTokensOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeTokensOnKyber: no transactionId")

    return transactionId


def API_TradeTokensOnSet(contractAddress, contractObject,
                         fromAddress, fromPrivateKey, inflowTokensToSpend_weiUnits, minBidSize_weiUnits,
                         rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress, gas, gasPrice):
    PrintAndLog("Ninja:API_TradeTokensOnSet: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress))
    ]

    kwargs = {
        'addressArray_Set': addressArray_Set_toUse,
        'inflowTokensToSpend': int(inflowTokensToSpend_weiUnits),
        'minBidSize': int(minBidSize_weiUnits),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeTokensOnSet', kwargs=kwargs)
    PrintAndLog("data_hex: " + str(data_hex))

    # This function assumes that ether you'd like to use to trade is already deposited to the contract
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeTokensOnSet: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeTokensOnSet: no transactionId")

    return transactionId


def API_TradeTokensOnBancor(contractAddress, contractObject,
                            fromAddress, fromPrivateKey, msgValue_weiUnits,
                            bancorConverterContract, pathList, sourceAmount_weiUnits, _minReturn, _block, _v, _r, _s,
                            gas, gasPrice):
    PrintAndLog("Ninja:API_TradeTokensOnBancor: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    pathList_toUse = []
    for address in pathList:
        pathList_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    kwargs = {
        'msgValue': int(msgValue_weiUnits),
        'bancorConverterContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverterContract)),
        '_path': pathList_toUse,
        '_amount': int(sourceAmount_weiUnits),
        '_minReturn': int(_minReturn),
        '_block': int(_block),
        '_v': int(_v),
        '_r': Web3.toBytes(hexstr=str(_r)),
        '_s': Web3.toBytes(hexstr=str(_s)),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeTokensOnBancor', kwargs=kwargs)
    PrintAndLog("data_hex: " + str(data_hex))

    # This function assumes that ether you'd like to use to trade is already deposited to the contract
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeTokensOnBancor: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeTokensOnBancor: no transactionId")

    return transactionId


def API_Trade_BuyOnSetSellOnKyber(contractAddress, contractObject,
                                  fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                  inflowTokensToSpend_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                  inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice):
    PrintAndLog("Ninja:API_Trade_BuyOnSetSellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContractAddress))
    ]

    kwargs = {
        'addressArray_Set': addressArray_Set_toUse,
        'inflowTokensToSpend_Set': int(inflowTokensToSpend_weiUnits),
        'minBidSize_Set': int(minBidSize_weiUnits),
        'kyberProxyContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        'path_Kyber': pathList_Kyber_toUse,
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('trade_buyOnSetSellOnKyber', kwargs=kwargs)
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_Trade_BuyOnSetSellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_Trade_BuyOnSetSellOnKyber: no transactionId")

    return transactionId


def API_Trade_BuyOnKyberSellOnSet(contractAddress, contractObject,
                                  fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                  amountToConvert_Kyber_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                  inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice):
    PrintAndLog("Ninja:API_Trade_BuyOnKyberSellOnSet: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContractAddress))
    ]

    kwargs = {
        'addressArray_Set': addressArray_Set_toUse,
        'amountToConvert_Kyber': int(amountToConvert_Kyber_weiUnits),
        'minBidSize_Set': int(minBidSize_weiUnits),
        'kyberProxyContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        'path_Kyber': pathList_Kyber_toUse,
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('trade_buyOnKyberSellOnSet', kwargs=kwargs)
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_Trade_BuyOnKyberSellOnSet: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_Trade_BuyOnKyberSellOnSet: no transactionId")

    return transactionId


def API_Trade_BuyOnSetSellOn0xv2(contractAddress, contractObject,
                                 fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                 rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                 order, inflowTokensToSpend_Set_weiUnits, gas, gasPrice):
    PrintAndLog("Ninja:API_Trade_BuyOnSetSellOn0xv2: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    #     function trade_buyOnSetSellOn0xv2(
    #         uint inflowTokensToSpend_Set,
    #     address[] memory addressArray_Set,
    #         uint256 minBidSize_Set,
    #         address zrxV2ExchangeContract,
    #     address wethContract,
    #         Order memory order,
    #     bytes memory signature
    #         ) public onlyOwnerOrWhitelist
    #     {

    orderTuple = order.GenerateOrderTupleForTransaction()

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract))
    ]

    params = 'uint256,address[],uint256,address,address,' + Exchanges.zrxV2.OrderTupleString + ',bytes'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"trade_buyOnSetSellOn0xv2({params})")[0:4]
    PrintAndLog("API_Trade_BuyOnSetSellOn0xv2 method_signature = " + str(method_signature))
    method_parameters = encode_single(f"({params})", [
        int(inflowTokensToSpend_Set_weiUnits),
        addressArray_Set_toUse,
        int(minBidSize_weiUnits),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract)),
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature))
    ])
    PrintAndLog("API_Trade_BuyOnSetSellOn0xv2 method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_Trade_BuyOnSetSellOn0xv2: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_Trade_BuyOnSetSellOn0xv2: no transactionId")

    return transactionId


def API_Trade_BuyOn0xv2SellOnSet(contractAddress, contractObject,
                                 fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                 rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                 order, amountToConvert_0xv2_weiUnits, gas, gasPrice):
    PrintAndLog("Ninja:API_Trade_BuyOn0xv2SellOnSet: fromAddress = " + str(fromAddress) + ", amountToConvert_0xv2_weiUnits = " + str(
        amountToConvert_0xv2_weiUnits) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    # function trade_buyOn0xv2SellOnSet(
    #     address zrxV2ExchangeContract,
    #     address wethContract,
    #     Order memory order,
    #     bytes memory signature,
    #     uint amountToConvert_0xv2,
    #     address[] memory addressArray_Set,
    #     uint256 minBidSize_Set
    #     )

    orderTuple = order.GenerateOrderTupleForTransaction()

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract))
    ]

    params = 'address,address,' + Exchanges.zrxV2.OrderTupleString + ',bytes,uint256,address[],uint256'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"trade_buyOn0xv2SellOnSet({params})")[0:4]
    PrintAndLog("API_Trade_BuyOn0xv2SellOnSet method_signature = " + str(method_signature))
    method_parameters = encode_single(f"({params})", [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract)),
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        int(amountToConvert_0xv2_weiUnits),
        addressArray_Set_toUse,
        int(minBidSize_weiUnits)
    ])
    PrintAndLog("API_Trade_BuyOn0xv2SellOnSet method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_Trade_BuyOn0xv2SellOnSet: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_Trade_BuyOn0xv2SellOnSet: no transactionId")

    return transactionId


def API_Trade_BuyOnKyberSellOnBancor(contractAddress, contractObject,
                                     fromAddress, fromPrivateKey,
                                     bancorConverterContract, kyberProxyContract,
                                     pathList_Bancor, pathList_Kyber, amountToConvert_Kyber_weiUnits,
                                     gas, gasPrice):
    PrintAndLog("Ninja:API_Trade_BuyOnKyberSellOnBancor: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    pathList_Bancor_toUse = []
    for address in pathList_Bancor:
        pathList_Bancor_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    kwargs = {
        'bancorConverterContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverterContract)),
        'kyberProxyContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        'path_Bancor': pathList_Bancor_toUse,
        'path_Kyber': pathList_Kyber_toUse,
        'amountToConvert_Kyber': int(amountToConvert_Kyber_weiUnits),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('trade_buyOnKyberSellOnBancor', kwargs=kwargs)
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_Trade_BuyOnKyberSellOnBancor: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_Trade_BuyOnKyberSellOnBancor: no transactionId")

    return transactionId


def API_Trade_BuyOnBancorSellOnKyber(contractAddress, contractObject,
                                     fromAddress, fromPrivateKey,
                                     bancorConverterContract, kyberProxyContract,
                                     pathList_Bancor, pathList_Kyber, amountToConvert_Bancor_weiUnits,
                                     gas, gasPrice):
    PrintAndLog("Ninja:API_Trade_BuyOnBancorSellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    pathList_Bancor_toUse = []
    for address in pathList_Bancor:
        pathList_Bancor_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    kwargs = {
        'bancorConverterContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverterContract)),
        'kyberProxyContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        'path_Bancor': pathList_Bancor_toUse,
        'path_Kyber': pathList_Kyber_toUse,
        'amountToConvert_Bancor': int(amountToConvert_Bancor_weiUnits),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('trade_buyOnBancorSellOnKyber', kwargs=kwargs)
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_Trade_BuyOnBancorSellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_Trade_BuyOnBancorSellOnKyber: no transactionId")

    return transactionId


def API_Trade_BuyOn0xv2SellOnKyber(contractAddress, contractObject,
                                   fromAddress, fromPrivateKey, kyberProxyContract,
                                   pathList_Kyber, order, amountToConvert_0xv2_weiUnits,
                                   gas, gasPrice):
    PrintAndLog("Ninja:API_Trade_BuyOn0xv2SellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    # function trade_buyOn0xv2SellOnKyber(
    #         address zrxV2ExchangeContract,
    #     address wethContract,
    #     address kyberProxyContract,
    #         address[] memory path_Kyber,
    #     Order memory order,
    #     bytes memory signature,
    #         uint amountToConvert_0xv2
    #         ) public onlyOwnerOrWhitelist

    orderTuple = order.GenerateOrderTupleForTransaction()

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    params = 'address,address,address,address[],' + Exchanges.zrxV2.OrderTupleString + ',bytes,uint256'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"trade_buyOn0xv2SellOnKyber({params})")[0:4]
    PrintAndLog("API_Trade_BuyOn0xv2SellOnKyber method_signature = " + str(method_signature))
    PrintAndLog("method_parameters before encoding kyberProxyContract = " + str(kyberProxyContract) + ", zrxV2ExchangeContract = " + str(
        zrxV2ExchangeContract) + ", wethContract = " + str(wethContract) + ", pathList_Kyber_toUse = " + str(pathList_Kyber_toUse) + ", orderTuple = " + str(
        orderTuple) + ", amountToConvert_0xv2_weiUnits = " + str(amountToConvert_0xv2_weiUnits) + ", order.signature = " + str(order.signature))
    method_parameters = encode_single(f"({params})", [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        pathList_Kyber_toUse,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        int(amountToConvert_0xv2_weiUnits)
    ])
    PrintAndLog("API_Trade_BuyOn0xv2SellOnKyber method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_Trade_BuyOn0xv2SellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_Trade_BuyOn0xv2SellOnKyber: no transactionId")

    return transactionId


def API_Trade_BuyOnKyberSellOn0xv2(contractAddress, contractObject,
                                   fromAddress, fromPrivateKey, kyberProxyContract,
                                   pathList_Kyber, order, amountToConvert_Kyber_weiUnits,
                                   gas, gasPrice):
    PrintAndLog("Ninja:API_Trade_BuyOnKyberSellOn0xv2: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    # function trade_buyOnKyberSellOn0xv2(
    #     address zrxV2ExchangeContract,
    #     address wethContract,
    #     address kyberProxyContract,
    #     address[] memory path_Kyber,
    #     Order memory order,
    #     bytes memory signature,
    #     uint amountToConvert_Kyber
    #     ) public onlyOwnerOrWhitelist

    orderTuple = order.GenerateOrderTupleForTransaction()

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    params = 'address,address,address,address[],' + Exchanges.zrxV2.OrderTupleString + ',bytes,uint256'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"trade_buyOnKyberSellOn0xv2({params})")[0:4]
    PrintAndLog("API_Trade_BuyOnKyberSellOn0xv2 method_signature = " + str(method_signature))
    PrintAndLog("method_parameters before encoding kyberProxyContract = " + str(kyberProxyContract) + ", zrxV2ExchangeContract = " + str(
        zrxV2ExchangeContract) + ", wethContract = " + str(wethContract) + ", pathList_Kyber_toUse = " + str(pathList_Kyber_toUse) + ", orderTuple = " + str(
        orderTuple) + ", amountToConvert_Kyber_weiUnits = " + str(amountToConvert_Kyber_weiUnits) + ", order.signature = " + str(order.signature))
    method_parameters = encode_single(f"({params})", [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        pathList_Kyber_toUse,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        int(amountToConvert_Kyber_weiUnits)
    ])
    PrintAndLog("API_Trade_BuyOnKyberSellOn0xv2 method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_Trade_BuyOnKyberSellOn0xv2: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_Trade_BuyOnKyberSellOn0xv2: no transactionId")

    return transactionId


def API_TradeSafe_BuyOn0xv2SellOnKyber(contractAddress, contractObject,
                                       fromAddress, fromPrivateKey, token, kyberProxyContract,
                                       pathList_Kyber, order, amountToConvert_0xv2_weiUnits,
                                       gas, gasPrice):
    PrintAndLog("Ninja:API_TradeSafe_BuyOn0xv2SellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    # // Trades only when profitable
    #     function tradeSafe_buyOn0xv2SellOnKyber(
    #         address zrxV2ExchangeContract,
    #     address wethContract,
    #     address kyberProxyContract,
    #         address[] memory addressArray,
    #     bytes memory fromAssetData,
    #     bytes memory toAssetData,
    #         address[] memory path_Kyber,
    #     Order memory order,
    #     bytes memory signature,
    #     uint256 amountToConvert_0xv2

    #     // addressArray[0] is tokenToTrade
    #     // addressArray[1] is kybersEtherToken
    #     // addressArray[2] is kyberProxyContract

    orderTuple = order.GenerateOrderTupleForTransaction()
    # TODO, remove
    # orderTuple = (str(order.maker), str(order.taker), str(order.feeRecipient), str(order.senderAddress), int(order.makerTokenAmount),
    #               int(order.takerTokenAmount), int(order.makerFee), int(order.takerFee), int(order.expirationUnixTimestampSec), int(order.salt),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.makerTokenAddress)))),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.takerTokenAddress)))))

    fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
    toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    addressArray_Kyber_toUse = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
    ]

    params = 'address,address,address,address[],bytes,bytes,address[],' + Exchanges.zrxV2.OrderTupleString + ',bytes,uint256'
    # TODO, remove
    # params = 'address,address,address,address[],bytes,bytes,address[],(address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes),bytes,uint256'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeSafe_buyOn0xv2SellOnKyber({params})")[0:4]
    # PrintAndLog("API_TradeSafe_BuyOn0xv2SellOnKyber method_signature = " + str(method_signature))
    PrintAndLog("method_parameters before encoding kyberProxyContract = " + str(kyberProxyContract) + ", zrxV2ExchangeContract = " + str(
        zrxV2ExchangeContract) + ", wethContract = " + str(wethContract) + ", pathList_Kyber_toUse = " + str(pathList_Kyber_toUse) + ", orderTuple = " + str(
        orderTuple) + ", amountToConvert_0xv2_weiUnits = " + str(amountToConvert_0xv2_weiUnits) + ", order.signature = " + str(order.signature))
    method_parameters = encode_single(f"({params})", [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        addressArray_Kyber_toUse,
        fromAssetData,
        toAssetData,
        pathList_Kyber_toUse,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        int(amountToConvert_0xv2_weiUnits)
    ])
    # PrintAndLog("API_TradeSafe_BuyOn0xv2SellOnKyber method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOn0xv2SellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOn0xv2SellOnKyber: no transactionId")

    return transactionId


def API_TradeSafe_BuyOnKyberSellOn0xv2(contractAddress, contractObject,
                                       fromAddress, fromPrivateKey, token, kyberProxyContract,
                                       pathList_Kyber, order, amountToConvert_Kyber_weiUnits,
                                       gas, gasPrice):
    PrintAndLog("Ninja:API_TradeSafe_BuyOnKyberSellOn0xv2: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    #     function tradeSafe_buyOnKyberSellOn0xv2(
    #         address zrxV2ExchangeContract,
    #     address wethContract,
    #     address kyberProxyContract,
    #         address[] memory addressArray,
    #     bytes memory fromAssetData,
    #     bytes memory toAssetData,
    #         address[] memory path_Kyber,
    #     Order memory order,
    #     bytes memory signature,
    #     uint256 amountToConvert_Kyber

    #     // addressArray[0] is tokenToTrade
    #     // addressArray[1] is kybersEtherToken
    #     // addressArray[2] is kyberProxyContract

    orderTuple = order.GenerateOrderTupleForTransaction()
    # TODO, remove
    # orderTuple = (str(order.maker), str(order.taker), str(order.feeRecipient), str(order.senderAddress), int(order.makerTokenAmount),
    #               int(order.takerTokenAmount), int(order.makerFee), int(order.takerFee), int(order.expirationUnixTimestampSec), int(order.salt),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.makerTokenAddress)))),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.takerTokenAddress)))))

    fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
    toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    addressArray_Kyber_toUse = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
    ]

    params = 'address,address,address,address[],bytes,bytes,address[],' + Exchanges.zrxV2.OrderTupleString + ',bytes,uint256'
    # TODO, remove
    # params = 'address,address,address,address[],bytes,bytes,address[],(address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes),bytes,uint256'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeSafe_buyOnKyberSellOn0xv2({params})")[0:4]
    # PrintAndLog("API_TradeSafe_BuyOnKyberSellOn0xv2 method_signature = " + str(method_signature))
    PrintAndLog("method_parameters before encoding kyberProxyContract = " + str(kyberProxyContract) + ", zrxV2ExchangeContract = " + str(
        zrxV2ExchangeContract) + ", wethContract = " + str(wethContract) + ", pathList_Kyber_toUse = " + str(pathList_Kyber_toUse) + ", orderTuple = " + str(
        orderTuple) + ", amountToConvert_Kyber_weiUnits = " + str(amountToConvert_Kyber_weiUnits) + ", order.signature = " + str(order.signature))
    method_parameters = encode_single(f"({params})", [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        addressArray_Kyber_toUse,
        fromAssetData,
        toAssetData,
        pathList_Kyber_toUse,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        int(amountToConvert_Kyber_weiUnits)
    ])
    # PrintAndLog("API_TradeSafe_BuyOnKyberSellOn0xv2 method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOnKyberSellOn0xv2: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOnKyberSellOn0xv2: no transactionId")

    return transactionId


def API_TradeSafe_BuyOnSetSellOn0xv2(contractAddress, contractObject,
                                     fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                     rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                     order, inflowTokensToSpend_Set_weiUnits, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSafe_BuyOnSetSellOn0xv2: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    #     function tradeSafe_buyOnSetSellOn0xv2(
    #         address[] memory addressArray_Set,
    #     uint256 inflowTokensToSpend_Set,
    #     uint256 minBidSize_Set,
    #         address zrxV2ExchangeContract,
    #     address wethContract,
    #         bytes memory fromAssetData,
    #     bytes memory toAssetData,
    #     Order memory order,
    #     bytes memory signature
    #         )

    orderTuple = order.GenerateOrderTupleForTransaction()
    # TODO, remove
    # orderTuple = (str(order.maker), str(order.taker), str(order.feeRecipient), str(order.senderAddress), int(order.makerTokenAmount),
    #               int(order.takerTokenAmount), int(order.makerFee), int(order.takerFee), int(order.expirationUnixTimestampSec), int(order.salt),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.makerTokenAddress)))),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.takerTokenAddress)))))

    fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
    toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract))
    ]

    params = 'address[],uint256,uint256,address,address,bytes,bytes,' + Exchanges.zrxV2.OrderTupleString + ',bytes'
    # TODO, remove
    # params = 'address[],uint256,uint256,address,address,bytes,bytes,(address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes),bytes'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeSafe_buyOnSetSellOn0xv2({params})")[0:4]
    # PrintAndLog("API_TradeSafe_BuyOnSetSellOn0xv2 method_signature = " + str(method_signature))
    method_parameters = encode_single(f"({params})", [
        addressArray_Set_toUse,
        int(inflowTokensToSpend_Set_weiUnits),
        int(minBidSize_weiUnits),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract)),
        fromAssetData,
        toAssetData,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature))
    ])
    # PrintAndLog("API_TradeSafe_BuyOnSetSellOn0xv2 method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOnSetSellOn0xv2: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOnSetSellOn0xv2: no transactionId")

    return transactionId


def API_TradeSafe_BuyOn0xv2SellOnSet(contractAddress, contractObject,
                                     fromAddress, fromPrivateKey, minBidSize_weiUnits,
                                     rebalanceAuctionModuleAddress, setTokenAddress, inflowTokenAddress, outflowTokenAddress,
                                     order, amountToConvert_0xv2_weiUnits, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSafe_BuyOn0xv2SellOnSet: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    #     function tradeSafe_buyOn0xv2SellOnSet(
    #         address zrxV2ExchangeContract,
    #     address wethContract,
    #     uint amountToConvert_0xv2,
    #         bytes memory fromAssetData,
    #     bytes memory toAssetData,
    #     Order memory order,
    #     bytes memory signature,
    #         address[] memory addressArray_Set,
    #     uint256 minBidSize_Set
    #         )

    orderTuple = order.GenerateOrderTupleForTransaction()
    # TODO, remove
    # orderTuple = (str(order.maker), str(order.taker), str(order.feeRecipient), str(order.senderAddress), int(order.makerTokenAmount),
    #               int(order.takerTokenAmount), int(order.makerFee), int(order.takerFee), int(order.expirationUnixTimestampSec), int(order.salt),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.makerTokenAddress)))),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.takerTokenAddress)))))

    fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
    toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract))
    ]

    params = 'address,address,uint256,bytes,bytes,' + Exchanges.zrxV2.OrderTupleString + ',bytes,address[],uint256'
    # TODO, remove
    # params = 'address,address,uint256,bytes,bytes,(address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes),bytes,address[],uint256'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeSafe_buyOn0xv2SellOnSet({params})")[0:4]
    # PrintAndLog("API_TradeSafe_BuyOn0xv2SellOnSet method_signature = " + str(method_signature))
    method_parameters = encode_single(f"({params})", [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContract)),
        int(amountToConvert_0xv2_weiUnits),
        fromAssetData,
        toAssetData,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        addressArray_Set_toUse,
        int(minBidSize_weiUnits)
    ])
    # PrintAndLog("API_TradeSafe_BuyOn0xv2SellOnSet method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOn0xv2SellOnSet: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOn0xv2SellOnSet: no transactionId")

    return transactionId


def API_TradeSafe_BuyOnSetSellOnKyber(contractAddress, contractObject,
                                      fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                      inflowTokensToSpend_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                      inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSafe_BuyOnSetSellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContractAddress))
    ]

    addressArray_Kyber_toUse = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)
    ]
    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    kwargs = {
        'addressArray_Set': addressArray_Set_toUse,
        'addressArray_Kyber': addressArray_Kyber_toUse,
        'path_Kyber': pathList_Kyber_toUse,
        'inflowTokensToSpend_Set': int(inflowTokensToSpend_weiUnits),
        'minBidSize_Set': int(minBidSize_weiUnits),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSafe_buyOnSetSellOnKyber', kwargs=kwargs)
    # PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOnSetSellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOnSetSellOnKyber: no transactionId")

    return transactionId


def API_TradeSafe_BuyOnKyberSellOnSet(contractAddress, contractObject,
                                      fromAddress, fromPrivateKey, kyberProxyContract, pathList_Kyber,
                                      amountToConvert_Kyber_weiUnits, minBidSize_weiUnits, rebalanceAuctionModuleAddress, setTokenAddress,
                                      inflowTokenAddress, outflowTokenAddress, wethContractAddress, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSafe_BuyOnKyberSellOnSet: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    addressArray_Set_toUse = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(rebalanceAuctionModuleAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(outflowTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(wethContractAddress))
    ]

    addressArray_Kyber_toUse = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(inflowTokenAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)
    ]
    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    kwargs = {
        'addressArray_Set': addressArray_Set_toUse,
        'addressArray_Kyber': addressArray_Kyber_toUse,
        'path_Kyber': pathList_Kyber_toUse,
        'amountToConvert_Kyber': int(amountToConvert_Kyber_weiUnits),
        'minBidSize_Set': int(minBidSize_weiUnits),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSafe_buyOnKyberSellOnSet', kwargs=kwargs)
    # PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOnKyberSellOnSet: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOnKyberSellOnSet: no transactionId")

    return transactionId


def API_TradeSafe_BuyOn0xv2SellOnBancor(contractAddress, contractObject,
                                        fromAddress, fromPrivateKey, tokenContractAddress,
                                        bancorConverterContract, bancorTokenConverterContract,
                                        pathList_Bancor, order, amountToConvert_0xv2_weiUnits,
                                        gas, gasPrice):
    PrintAndLog("Ninja:API_TradeSafe_BuyOn0xv2SellOnBancor: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    orderTuple = order.GenerateOrderTupleForTransaction()
    # TODO, remove
    # orderTuple = (str(order.maker), str(order.taker), str(order.feeRecipient), str(order.senderAddress), int(order.makerTokenAmount),
    #               int(order.takerTokenAmount), int(order.makerFee), int(order.takerFee), int(order.expirationUnixTimestampSec), int(order.salt),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.makerTokenAddress)))),
    #               Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(str(order.takerTokenAddress)))))

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        bancorTokenConverterContract,
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(tokenContractAddress),
    ]

    fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
    toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))

    pathList_Bancor_toUse = []
    for address in pathList_Bancor:
        pathList_Bancor_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    params = 'address,address,address,address[],bytes,bytes,address[],' + Exchanges.zrxV2.OrderTupleString + ',bytes,uint256'
    # TODO, remove
    # params = 'address,address,address,address[],bytes,bytes,address[],(address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes),bytes,uint256'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeSafe_buyOn0xv2SellOnBancor({params})")[0:4]
    PrintAndLog("API_TradeSafe_BuyOn0xv2SellOnBancor method_signature = " + str(method_signature))
    PrintAndLog("method_parameters before encoding bancorConverterContract = " + str(bancorConverterContract) + ", zrxV2ExchangeContract = " + str(
        zrxV2ExchangeContract) + ", wethContract = " + str(wethContract) + ", addressArray = " + str(addressArray) + ", pathList_Bancor_toUse = " + str(
        pathList_Bancor_toUse) + ", orderTuple = " + str(orderTuple) + ", amountToConvert_0xv2_weiUnits = " + str(amountToConvert_0xv2_weiUnits))
    method_parameters = encode_single(f"({params})", [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverterContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        wethContract,
        addressArray,
        fromAssetData,
        toAssetData,
        pathList_Bancor_toUse,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        int(amountToConvert_0xv2_weiUnits)
    ])
    PrintAndLog("API_TradeSafe_BuyOn0xv2SellOnBancor method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOn0xv2SellOnBancor: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOn0xv2SellOnBancor: no transactionId")

    return transactionId


def API_TradeSafe_BuyOnBancorSellOn0xv2(contractAddress, contractObject,
                                        fromAddress, fromPrivateKey, tokenContractAddress,
                                        bancorConverterContract, bancorTokenConverterContract,
                                        pathList_Bancor, order, amountToConvert_Bancor_weiUnits,
                                        gas, gasPrice):
    PrintAndLog("Ninja:API_TradeSafe_BuyOnBancorSellOn0xv2: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    orderTuple = order.GenerateOrderTupleForTransaction()

    wethContract = Exchanges.zrxV2.Contract_WETH
    zrxV2ExchangeContract = Exchanges.zrxV2.Contract_Exchange

    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        bancorTokenConverterContract,
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(tokenContractAddress),
    ]

    fromAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)))
    toAssetData = Web3.toBytes(hexstr=str(Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)))

    pathList_Bancor_toUse = []
    for address in pathList_Bancor:
        pathList_Bancor_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    params = 'address,address,address,address[],bytes,bytes,address[],' + Exchanges.zrxV2.OrderTupleString + ',bytes,uint256'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeSafe_buyOnBancorSellOn0xv2({params})")[0:4]
    PrintAndLog("API_TradeSafe_BuyOnBancorSellOn0xv2 method_signature = " + str(method_signature))
    PrintAndLog("method_parameters before encoding bancorConverterContract = " + str(bancorConverterContract) + ", zrxV2ExchangeContract = " + str(
        zrxV2ExchangeContract) + ", wethContract = " + str(wethContract) + ", addressArray = " + str(addressArray) + ", pathList_Bancor_toUse = " + str(
        pathList_Bancor_toUse) + ", orderTuple = " + str(orderTuple) + ", amountToConvert_Bancor_weiUnits = " + str(amountToConvert_Bancor_weiUnits))
    method_parameters = encode_single(f"({params})", [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverterContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(zrxV2ExchangeContract)),
        wethContract,
        addressArray,
        fromAssetData,
        toAssetData,
        pathList_Bancor_toUse,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        int(amountToConvert_Bancor_weiUnits)
    ])
    PrintAndLog("API_TradeSafe_BuyOnBancorSellOn0xv2 method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOnBancorSellOn0xv2: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOnBancorSellOn0xv2: no transactionId")

    return transactionId


def API_TradeSafe_BuyOnKyberSellOnBancor(contractAddress, contractObject,
                                         fromAddress, fromPrivateKey,
                                         bancorConverterContract, bancorTokenConverterContract, kyberProxyContract,
                                         pathList_Bancor, pathList_Kyber, amountToConvert_Kyber_weiUnits,
                                         gas, gasPrice):
    PrintAndLog("Ninja:API_TradeSafe_BuyOnKyberSellOnBancor: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    pathList_Bancor_toUse = []
    for address in pathList_Bancor:
        pathList_Bancor_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    # populate addressArray
    #    // addressArray[0] is converterContract_BNT
    #    // addressArray[1] is converterContract_Token
    #    // addressArray[2] is bancorsEtherToken
    #    // addressArray[3] is bancorsBNTToken
    #    // addressArray[4] is tokenToTrade
    #    // addressArray[5] is kybersEtherToken
    #    // addressArray[6] is kyberProxyContract
    tokenContractAddress = pathList_Kyber[1]
    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        Libraries.nodes.Instance_Web3.toChecksumAddress(bancorTokenConverterContract),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(tokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
    ]

    kwargs = {
        'bancorConverterContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverterContract)),
        'kyberProxyContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        'addressArray': addressArray,
        'path_Bancor': pathList_Bancor_toUse,
        'path_Kyber': pathList_Kyber_toUse,
        'amountToConvert_Kyber': int(amountToConvert_Kyber_weiUnits),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSafe_buyOnKyberSellOnBancor', kwargs=kwargs)
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOnKyberSellOnBancor: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOnKyberSellOnBancor: no transactionId")

    return transactionId


def API_TradeSafe_BuyOnBancorSellOnKyber(contractAddress, contractObject,
                                         fromAddress, fromPrivateKey,
                                         bancorConverterContract, bancorTokenConverterContract, kyberProxyContract,
                                         pathList_Bancor, pathList_Kyber, amountToConvert_Bancor_weiUnits,
                                         gas, gasPrice):
    PrintAndLog("Ninja:API_TradeSafe_BuyOnBancorSellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    pathList_Bancor_toUse = []
    for address in pathList_Bancor:
        pathList_Bancor_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    pathList_Kyber_toUse = []
    for address in pathList_Kyber:
        pathList_Kyber_toUse.append(str(Libraries.nodes.Instance_Web3.toChecksumAddress(address)))

    # populate addressArray
    #    // addressArray[0] is converterContract_BNT
    #    // addressArray[1] is converterContract_Token
    #    // addressArray[2] is bancorsEtherToken
    #    // addressArray[3] is bancorsBNTToken
    #    // addressArray[4] is tokenToTrade
    #    // addressArray[5] is kybersEtherToken
    #    // addressArray[6] is kyberProxyContract
    tokenContractAddress = pathList_Kyber[0]
    addressArray = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.ConverterContractForBuyingTokens),
        bancorTokenConverterContract,
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorWrappedETH),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.bancor.BancorTokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(tokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
    ]

    kwargs = {
        'bancorConverterContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverterContract)),
        'kyberProxyContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberProxyContract)),
        'addressArray': addressArray,
        'path_Bancor': pathList_Bancor_toUse,
        'path_Kyber': pathList_Kyber_toUse,
        'amountToConvert_Bancor': int(amountToConvert_Bancor_weiUnits),
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSafe_buyOnBancorSellOnKyber', kwargs=kwargs)
    PrintAndLog("data_hex: " + str(data_hex))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSafe_BuyOnBancorSellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSafe_BuyOnBancorSellOnKyber: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnBancorSellOnKyber(contractAddress, contractObject,
                                                       fromAddress, fromPrivateKey, etherToSpend_etherUnits, expectedTokensToTrade_etherUnits,
                                                       tokenAddress, decimals_token, maxBancorBntQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
                                                       bancorConverter_payingWithEtherOnly, bancorConverter_payingWithSpecificTokenOnly,
                                                       bancorRelayerToken, kyberReserve,
                                                       gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnBancorSellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     function tradeSimpleRequirements_buyOnBancorSellOnKyber(
    #         uint256 etherToSpend,
    #         uint256 expectedTokensToTrade,
    #         uint256 maxBancorBntQuantityAcceptable,
    #         uint256 minKyberRateAcceptable,
    #         address[] memory addressArray
    #         ) public onlyOwnerOrWhitelist
    #     {
    # // addressArray[0] = address tokenAddress,
    # // addressArray[1] = address bancorConverter_payingWithEtherOnly,
    # // addressArray[2] = address bancorConverter_payingWithSpecificTokenOnly,
    # // addressArray[3] = address bancorRelayerToken, example: GNO Smart Token Relay (GNOBNT) 0xd7eb9db184da9f099b84e2f86b1da1fe6b305b3dT
    # // addressArray[4] = address kyberReserve,

    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverter_payingWithEtherOnly)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverter_payingWithSpecificTokenOnly)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorRelayerToken)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve)),
    ]

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    expectedTokensToTrade_weiUnits = Libraries.core.ConvertEtherToWei(expectedTokensToTrade_etherUnits, decimals_token)
    minKyberRateAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minKyberPriceAcceptable, Libraries.core.Ether_Decimals)
    maxBancorBntQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxBancorBntQuantityAcceptable_etherUnits, Exchanges.bancor.Decimals_bnt)

    kwargs = {
        'etherToSpend': int(etherToSpend_weiUnits),
        'expectedTokensToTrade': int(expectedTokensToTrade_weiUnits),
        'maxBancorBntQuantityAcceptable': int(maxBancorBntQuantityAcceptable_weiUnits),
        'minKyberRateAcceptable': int(minKyberRateAcceptable_weiUnits),
        'addressArray': addressArray
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array

    data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnBancorSellOnKyber', kwargs=kwargs)
    # PrintAndLog("data_hex: " + str(data_hex))
    value_wei_int = 0

    # try:
    #     estimatedGas = Libraries.core.API_EstimateGas(contractAddress, fromAddress, data_hex, value_wei_int,
    #                                                   True, 1.10, Libraries.core.RequestTimeout_wickedShort_seconds)
    #     Libraries.core.API_PostOperatorNotification("Ninja:API_TradeSimpleRequirements_BuyOnBancorSellOnKyber: estimatedGas = " + str(estimatedGas))
    #
    # except (KeyboardInterrupt, SystemExit):
    #     print('\nkeyboard interrupt caught')
    #     print('\n...Program Stopped Manually!')
    #     raise
    #
    # except:
    #     message = "Ninja:API_TradeSimpleRequirements_BuyOnBancorSellOnKyber: exception when calling API_EstimateGas " + traceback.format_exc()
    #     Libraries.core.API_PostOperatorNotification(message)

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnBancorSellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnBancorSellOnKyber: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnKyberSellOnBancor(contractAddress, contractObject,
                                                       fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                       tokenAddress, minBancorBntQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
                                                       bancorConverter_payingWithSpecificTokenOnly, bancorRelayerToken, kyberReserve,
                                                       gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnBancor: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    # function tradeSimpleRequirements_buyOnKyberSellOnBancor(
    #         uint256 etherToSpend,
    #         uint256 minBancorBntQuantityAcceptable,
    #         uint256 minKyberRateAcceptable,
    #         address[] memory addressArray
    #         ) public onlyOwnerOrWhitelist

    #     // addressArray[0] = address tokenAddress,
    #     // addressArray[1] = address bancorConverter_payingWithSpecificTokenOnly,
    #     // addressArray[2] = address bancorRelayerToken,
    #     // addressArray[3] = address kyberReserve,

    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorConverter_payingWithSpecificTokenOnly)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(bancorRelayerToken)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve)),
    ]

    # When buying tokens on Kyber I need to invert the price
    minKyberRateAcceptable_etherUnits = 1 / float(maxKyberPriceAcceptable)

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    minKyberRateAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minKyberRateAcceptable_etherUnits, Libraries.core.Ether_Decimals)
    minBancorBntQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minBancorBntQuantityAcceptable_etherUnits, Exchanges.bancor.Decimals_bnt)

    kwargs = {
        'etherToSpend': int(etherToSpend_weiUnits),
        'minBancorBntQuantityAcceptable': int(minBancorBntQuantityAcceptable_weiUnits),
        'minKyberRateAcceptable': int(minKyberRateAcceptable_weiUnits),
        'addressArray': addressArray,
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnKyberSellOnBancor', kwargs=kwargs)
    # PrintAndLog("data_hex: " + str(data_hex))
    value_wei_int = 0

    # try:
    #     estimatedGas = Libraries.core.API_EstimateGas(contractAddress, fromAddress, data_hex, value_wei_int,
    #                                                   True, 1.10, Libraries.core.RequestTimeout_wickedShort_seconds)
    #     Libraries.core.API_PostOperatorNotification("Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnBancor: estimatedGas = " + str(estimatedGas))
    #
    # except (KeyboardInterrupt, SystemExit):
    #     print('\nkeyboard interrupt caught')
    #     print('\n...Program Stopped Manually!')
    #     raise
    #
    # except:
    #     message = "Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnBancor: exception when calling API_EstimateGas " + traceback.format_exc()
    #     Libraries.core.API_PostOperatorNotification(message)

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnBancor: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnBancor: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnUniswapSellOnKyber(contractAddress, contractObject,
                                                        fromAddress, fromPrivateKey, etherToSpend_etherUnits, expectedTokensToTrade_etherUnits,
                                                        tokenAddress, decimals_token, maxUniswapEthQuantityAcceptable_etherUnits, minKyberPriceAcceptable,
                                                        kyberReserve, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    # function tradeSimpleRequirements_encoded_buyOnUniswapSellOnKyber(
    #         uint256 etherToSpend_encoded,
    #         uint256[] memory intArray,
    #         uint256[] memory addressArray_encoded,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public
    #     {
    #         // addressArray[0] = address tokenAddress,
    #         // addressArray[1] = address uniswapContract,
    #         // addressArray[2] = address kyberReserve,
    #
    #         // intArray[0] = uint256 expectedTokensToTrade,
    #         // intArray[1] = uint256 maxUniswapEthQuantityAcceptable,
    #         // intArray[2] = uint256 minKyberRateAcceptable,
    #
    #         // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

    # function tradeWithinFlashLoan_buyOnUniswapSellOnKyber(
    #         uint256 etherToSpend,
    #         address uniswapContract,
    #         address tokenAddress
    #         ) public onlyWhitelistedKeeperDAOLPPs

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[tokenAddress.lower()]['exchange'])

    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve)),
    ]

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    expectedTokensToTrade_weiUnits = Libraries.core.ConvertEtherToWei(expectedTokensToTrade_etherUnits, decimals_token)
    minKyberRateAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minKyberPriceAcceptable, Libraries.core.Ether_Decimals)
    maxUniswapEthQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxUniswapEthQuantityAcceptable_etherUnits, Libraries.core.Ether_Decimals)

    intArray = [
        int(expectedTokensToTrade_weiUnits),
        int(maxUniswapEthQuantityAcceptable_weiUnits),
        int(minKyberRateAcceptable_weiUnits),
    ]

    # Determine the prePackedTradeCallDataArray_encoded
    # Prep the calldata and function selector for the appropriate tradeWithinFlashLoan call
    kwargs = {
        # This will get replaced within the smart contract, so just set it as 0xFFFF...FFFF for now
        'borrowedQuoteTokens': int(Libraries.core.DataF_Int),
        'uniswapContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
        'tokenAddress': str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
    }
    prePackedTradeCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, contractObject, 'tradeWithinFlashLoan_buyOnUniswapSellOnKyber')
    PrintAndLog("prePackedTradeCallDataArray_encoded = " + str(prePackedTradeCallDataArray_encoded))

    doEncode = True
    data_hex = None
    if doEncode:
        kwargs = {
            'etherToSpend_encoded': Libraries.ninjaEncoding.PackInt(int(etherToSpend_weiUnits)),
            'intArray': intArray,
            'addressArray_encoded': Libraries.ninjaEncoding.PackAddressList(addressArray),
            'prePackedTradeCallDataArray_encoded': prePackedTradeCallDataArray_encoded,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        data_hex = contractObject.encodeABI('tradeSimpleRequirements_encoded_buyOnUniswapSellOnKyber', kwargs=kwargs)
    else:
        kwargs = {
            'etherToSpend': int(etherToSpend_weiUnits),
            'intArray': intArray,
            'addressArray': addressArray,
            'prePackedTradeCallDataArray_encoded': prePackedTradeCallDataArray_encoded,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnUniswapSellOnKyber', kwargs=kwargs)

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOnKyber: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap(contractAddress, contractObject,
                                                        fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                        tokenAddress, minUniswapEthQuantityAcceptable_etherUnits, maxKyberPriceAcceptable,
                                                        kyberReserve, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))

    # // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_encoded_buyOnKyberSellOnUniswap(
    #         uint256 etherToSpend_encoded,
    #         uint256 minUniswapEthQuantityAcceptable,
    #         uint256 minKyberRateAcceptable,
    #         uint256[] memory addressArray_encoded,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public

    #         // addressArray[0] = address tokenAddress,
    #         // addressArray[1] = address uniswapContract
    #         // addressArray[2] = address kyberReserve,
    #
    #         // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

    # function tradeWithinFlashLoan_buyOnKyberSellOnUniswap(
    #         uint256 etherToSpend,
    #         address uniswapContract,
    #         address tokenAddress
    #         ) public onlyWhitelistedKeeperDAOLPPs

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[tokenAddress.lower()]['exchange'])

    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve)),
    ]

    # When buying tokens on Kyber I need to invert the price
    minKyberRateAcceptable_etherUnits = 1 / float(maxKyberPriceAcceptable)

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    minKyberRateAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minKyberRateAcceptable_etherUnits, Libraries.core.Ether_Decimals)
    minUniswapEthQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minUniswapEthQuantityAcceptable_etherUnits, Libraries.core.Ether_Decimals)

    # Determine the prePackedTradeCallDataArray_encoded
    # Prep the calldata and function selector for the appropriate tradeWithinFlashLoan call
    kwargs = {
        # This will get replaced within the smart contract, so just set it as 0xFFFF...FFFF for now
        'borrowedQuoteTokens': int(Libraries.core.DataF_Int),
        'uniswapContract': str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
        'tokenAddress': str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
    }
    prePackedTradeCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, contractObject, 'tradeWithinFlashLoan_buyOnKyberSellOnUniswap')
    PrintAndLog("prePackedTradeCallDataArray_encoded = " + str(prePackedTradeCallDataArray_encoded))

    doEncode = True
    data_hex = None
    if doEncode:
        kwargs = {
            'etherToSpend_encoded': Libraries.ninjaEncoding.PackInt(int(etherToSpend_weiUnits)),
            'minUniswapEthQuantityAcceptable': int(minUniswapEthQuantityAcceptable_weiUnits),
            'minKyberRateAcceptable': int(minKyberRateAcceptable_weiUnits),
            'addressArray_encoded': Libraries.ninjaEncoding.PackAddressList(addressArray),
            'prePackedTradeCallDataArray_encoded': prePackedTradeCallDataArray_encoded,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        data_hex = contractObject.encodeABI('tradeSimpleRequirements_encoded_buyOnKyberSellOnUniswap', kwargs=kwargs)
    else:
        kwargs = {
            'etherToSpend': int(etherToSpend_weiUnits),
            'minUniswapEthQuantityAcceptable': int(minUniswapEthQuantityAcceptable_weiUnits),
            'minKyberRateAcceptable': int(minKyberRateAcceptable_weiUnits),
            'addressArray': addressArray,
            'prePackedTradeCallDataArray_encoded': prePackedTradeCallDataArray_encoded,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnKyberSellOnUniswap', kwargs=kwargs)

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnUniswap: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnUniswapSellOn0xv3(contractAddress, contractObject,
                                                       fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                       tokenAddress, maxUniswapEthQuantityAcceptable_etherUnits, order,
                                                       gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOn0xv3: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     function tradeSimpleRequirements_buyOnUniswapSellOn0xv3(
    #         uint256 etherToSpend,
    #         uint256 maxUniswapEthQuantityAcceptable,
    #         Order memory order,
    #         bytes memory signature,
    #         address[] memory addressArray
    #         ) public onlyOwnerOrWhitelist
    #     {
    #         // addressArray[0] = address uniswapContract,

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[tokenAddress.lower()]['exchange'])
    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
    ]

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    maxUniswapEthQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxUniswapEthQuantityAcceptable_etherUnits, Libraries.core.Ether_Decimals)
    PrintAndLog("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

    orderTuple = order.GenerateOrderTupleForTransaction()

    params = 'uint256,uint256,' + Exchanges.zrxV2.OrderTupleString + ',bytes,address[]'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeSimpleRequirements_buyOnUniswapSellOn0xv3({params})")[0:4]
    method_parameters = encode_single(f"({params})", [
        int(etherToSpend_weiUnits),
        int(maxUniswapEthQuantityAcceptable_weiUnits),
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        addressArray,
    ])
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))

    value_wei_int = 0
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOn0xv3: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOn0xv3: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOn0xv3SellOnUniswap(contractAddress, contractObject,
                                                       fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                       tokenAddress, minUniswapEthQuantityAcceptable_etherUnits,
                                                       order, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOn0xv3SellOnUniswap: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    # function tradeSimpleRequirements_buyOn0xv3SellOnUniswap(
    #         uint256 etherToSpend,
    #         uint256 minUniswapEthQuantityAcceptable,
    #         Order memory order,
    #         bytes memory signature,
    #         address[] memory addressArray
    #         ) public onlyOwnerOrWhitelist
    #     {
    #         // addressArray[0] = address uniswapContract

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[tokenAddress.lower()]['exchange'])
    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
    ]

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    minUniswapEthQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minUniswapEthQuantityAcceptable_etherUnits, Libraries.core.Ether_Decimals)

    orderTuple = order.GenerateOrderTupleForTransaction()

    params = 'uint256,uint256,' + Exchanges.zrxV2.OrderTupleString + ',bytes,address[]'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeSimpleRequirements_buyOn0xv3SellOnUniswap({params})")[0:4]
    method_parameters = encode_single(f"({params})", [
        int(etherToSpend_weiUnits),
        int(minUniswapEthQuantityAcceptable_weiUnits),
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        addressArray,
    ])
    data_hex = '0x' + (method_signature + method_parameters).hex()
    PrintAndLog("data_hex: " + str(data_hex))
    value_wei_int = 0
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOn0xv3SellOnUniswap: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOn0xv3SellOnUniswap: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnSetSellOnKyber(contractAddress, contractObject,
                                                    fromAddress, fromPrivateKey, etherToSpend_etherUnits, expectedTokensToTrade_etherUnits,
                                                    tokenAddress, decimals_token, setTokenData, useCTokenBidderContract, minKyberPriceAcceptable,
                                                    kyberReserve, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnSetSellOnKyber: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_encoded_buyOnSetSellOnKyber(
    #         uint256 etherToSpend,
    #         uint256[] memory intArray,
    #         uint256[] memory addressArray_Kyber_encoded,
    #         uint256[] memory addressArray_Set_encoded,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public
    #     {
    #         tradeSimpleRequirements_buyOnSetSellOnKyber(
    #             etherToSpend, intArray,
    #             decodeAddressArray(addressArray_Kyber_encoded),
    #             decodeAddressArray(addressArray_Set_encoded),
    #             prePackedTradeCallDataArray_encoded);
    #     }
    #
    #     // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_buyOnSetSellOnKyber(
    #         uint256 etherToSpend,
    #         uint256[] memory intArray,
    #         address[] memory addressArray_Kyber,
    #         address[] memory addressArray_Set,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public onlyWhitelist
    #     {
    #         // intArray[0] = minBidSize
    #         // intArray[1] = minInflowTokenQuantity
    #         // intArray[2] = minOutflowTokenQuantity
    #         // intArray[3] = expectedTokensToTrade,
    #         // intArray[4] = minKyberRateAcceptable,
    #         // intArray[5] = magicConverter_inflowTokensToShares,
    #
    #         // addressArray_Kyber[0] = tokenAddress
    #         // addressArray_Kyber[1] = kyberReserve
    #
    #         // addressArray_Set[0] = rebalanceAuctionModuleAddress
    #         // addressArray_Set[1] = setTokenAddress
    #         // addressArray_Set[2] = inflowTokenAddress
    #         // addressArray_Set[3] = outflowTokenAddress
    #
    #         // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

    #     function tradeWithinFlashLoan_buyOnSetSellOnKyber(
    #         uint256 borrowedQuoteTokens,
    #         uint256 magicConverter_inflowTokensToShares,
    #         uint256[] memory intArray,
    #         address[] memory addressArray_Kyber,
    #         address[] memory addressArray_Set
    #         ) public onlyWhitelistedKeeperDAOTransferProxys

    addressArray_Kyber = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve)),
    ]

    addressArray_Set = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetInflowTokenAddressForTrading())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetOutflowTokenAddressForTrading())),
    ]

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    expectedTokensToTrade_weiUnits = Libraries.core.ConvertEtherToWei(expectedTokensToTrade_etherUnits, decimals_token)
    minKyberRateAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minKyberPriceAcceptable, Libraries.core.Ether_Decimals)

    dontCare1, priceChangeDueOnNextMinedBlock, magicConverter_inflowTokensToShares_onNextMinedBlock = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture(True)
    if priceChangeDueOnNextMinedBlock:
        message = "We're shipping with a future magicConverter. Check this over to make sure it's working properly!"
        Libraries.core.API_PostOperatorNotification(message)

    intArray = [
        int(setTokenData.minBidSize_weiUnits),
        int(setTokenData.minInflowTokenQuantity_weiUnits),
        int(setTokenData.minOutflowTokenQuantity_weiUnits),
        int(expectedTokensToTrade_weiUnits),
        int(minKyberRateAcceptable_weiUnits),
        int(magicConverter_inflowTokensToShares_onNextMinedBlock),
    ]

    # Determine the prePackedTradeCallDataArray_encoded
    # Prep the calldata and function selector for the appropriate tradeWithinFlashLoan call
    kwargs = {
        # # This will get replaced within the smart contract, so just set it as 0xFFFF...FFFF for now
        'borrowedQuoteTokens': int(Libraries.core.DataF_Int),
        'magicConverter_inflowTokensToShares': int(magicConverter_inflowTokensToShares_onNextMinedBlock),
        'intArray': intArray,
        'addressArray_Kyber': addressArray_Kyber,
        'addressArray_Set': addressArray_Set,
    }
    prePackedTradeCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, contractObject, 'tradeWithinFlashLoan_buyOnSetSellOnKyber')
    PrintAndLog("prePackedTradeCallDataArray_encoded = " + str(prePackedTradeCallDataArray_encoded))

    data_hex = None
    doEncode = True
    if doEncode:
        kwargs = {
            'etherToSpend': int(etherToSpend_weiUnits),
            'intArray': intArray,
            'addressArray_Kyber_encoded': Libraries.ninjaEncoding.PackAddressList(addressArray_Kyber),
            'addressArray_Set_encoded': Libraries.ninjaEncoding.PackAddressList(addressArray_Set),
            'prePackedTradeCallDataArray_encoded': prePackedTradeCallDataArray_encoded,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = contractObject.encodeABI('tradeSimpleRequirements_encoded_buyOnSetSellOnKyber', kwargs=kwargs)

    else:
        kwargs = {
            'etherToSpend': int(etherToSpend_weiUnits),
            'intArray': intArray,
            'addressArray_Kyber': addressArray_Kyber,
            'addressArray_Set': addressArray_Set,
            'prePackedTradeCallDataArray_encoded': prePackedTradeCallDataArray_encoded,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnSetSellOnKyber', kwargs=kwargs)

    value_wei_int = 0
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnSetSellOnKyber: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnSetSellOnKyber: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnKyberSellOnSet(contractAddress, contractObject,
                                                    fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                    tokenAddress, setTokenData, useCTokenBidderContract, maxKyberPriceAcceptable,
                                                    kyberReserve, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnSet: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_encoded_buyOnKyberSellOnSet(
    #         uint256 etherToSpend,
    #         uint256[] memory intArray,
    #         uint256[] memory addressArray_Kyber_encoded,
    #         uint256[] memory addressArray_Set_encoded,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public
    #     {
    #         tradeSimpleRequirements_buyOnKyberSellOnSet(
    #             etherToSpend, intArray,
    #             decodeAddressArray(addressArray_Kyber_encoded),
    #             decodeAddressArray(addressArray_Set_encoded),
    #             prePackedTradeCallDataArray_encoded);
    #     }
    #
    #     // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_buyOnKyberSellOnSet(
    #         uint256 etherToSpend,
    #         uint256[] memory intArray,
    #         address[] memory addressArray_Kyber,
    #         address[] memory addressArray_Set,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public onlyWhitelist
    #     {
    #         // intArray[0] = minBidSize
    #         // intArray[1] = minInflowTokenQuantity
    #         // intArray[2] = minOutflowTokenQuantity
    #         // intArray[3] = minKyberRateAcceptable,
    #         // intArray[4] = magicConverter_inflowTokensToShares,
    #
    #         // addressArray_Kyber[0] = tokenAddress
    #         // addressArray_Kyber[1] = kyberReserve
    #
    #         // addressArray_Set[0] = rebalanceAuctionModuleAddress
    #         // addressArray_Set[1] = setTokenAddress
    #         // addressArray_Set[2] = inflowTokenAddress
    #         // addressArray_Set[3] = outflowTokenAddress
    #
    #         // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

    #     function tradeWithinFlashLoan_buyOnKyberSellOnSet(
    #         uint256 borrowedQuoteTokens, 
    #         uint256[] memory intArray,
    #         address[] memory addressArray_Kyber,
    #         address[] memory addressArray_Set
    #         ) public onlyWhitelistedKeeperDAOTransferProxys

    addressArray_Kyber = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(kyberReserve)),
    ]

    addressArray_Set = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetInflowTokenAddressForTrading())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetOutflowTokenAddressForTrading())),
    ]

    # When buying tokens on Kyber I need to invert the price
    minKyberRateAcceptable_etherUnits = 1 / float(maxKyberPriceAcceptable)
    minKyberRateAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minKyberRateAcceptable_etherUnits, Libraries.core.Ether_Decimals)
    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)

    dontCare1, priceChangeDueOnNextMinedBlock, magicConverter_inflowTokensToShares_onNextMinedBlock = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture(True)
    if priceChangeDueOnNextMinedBlock:
        message = "We're shipping with a future magicConverter. Check this over to make sure it's working properly!"
        Libraries.core.API_PostOperatorNotification(message)

    intArray = [
        int(setTokenData.minBidSize_weiUnits),
        int(setTokenData.minInflowTokenQuantity_weiUnits),
        int(setTokenData.minOutflowTokenQuantity_weiUnits),
        int(minKyberRateAcceptable_weiUnits),
        int(magicConverter_inflowTokensToShares_onNextMinedBlock),
    ]

    # Determine the prePackedTradeCallDataArray_encoded
    # Prep the calldata and function selector for the appropriate tradeWithinFlashLoan call
    kwargs = {
        # This will get replaced within the smart contract, so just set it as 0xFFFF...FFFF for now
        'borrowedQuoteTokens': int(Libraries.core.DataF_Int),
        'intArray': intArray,
        'addressArray_Kyber': addressArray_Kyber,
        'addressArray_Set': addressArray_Set,
    }
    prePackedTradeCallDataArray_encoded = PreparePackedTradeCallData_GivenContractData(kwargs, contractObject, 'tradeWithinFlashLoan_buyOnKyberSellOnSet')
    PrintAndLog("prePackedTradeCallDataArray_encoded = " + str(prePackedTradeCallDataArray_encoded))

    data_hex = None
    doEncode = True
    if doEncode:
        kwargs = {
            'etherToSpend': int(etherToSpend_weiUnits),
            'intArray': intArray,
            'addressArray_Kyber_encoded': Libraries.ninjaEncoding.PackAddressList(addressArray_Kyber),
            'addressArray_Set_encoded': Libraries.ninjaEncoding.PackAddressList(addressArray_Set),
            'prePackedTradeCallDataArray_encoded': prePackedTradeCallDataArray_encoded,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = contractObject.encodeABI('tradeSimpleRequirements_encoded_buyOnKyberSellOnSet', kwargs=kwargs)

    else:
        kwargs = {
            'etherToSpend': int(etherToSpend_weiUnits),
            'intArray': intArray,
            'addressArray_Kyber': addressArray_Kyber,
            'addressArray_Set': addressArray_Set,
            'prePackedTradeCallDataArray_encoded': prePackedTradeCallDataArray_encoded,
        }
        PrintAndLog("kwargs = " + str(kwargs))
        # Build binary representation of the function call with arguments
        # Convert this hex string to a hex byte array
        data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnKyberSellOnSet', kwargs=kwargs)

    value_wei_int = 0
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnSet: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnKyberSellOnSet: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOn0xv3SellOnSet(contractAddress, contractObject,
                                                   fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                   order, setTokenData, useCTokenBidderContract,
                                                   gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOn0xv3SellOnSet: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_encoded_buyOn0xv3SellOnSet(
    #         uint256 etherToSpend,
    #         uint256[] memory intArray,
    #         Order_Encoded memory order_encoded,
    #         uint256[] memory addressArray_Set_encoded,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public
    #     {
    #         tradeSimpleRequirements_buyOn0xv3SellOnSet(
    #             etherToSpend, intArray,
    #             decodeOrder(order_encoded),
    #             decodeAddressArray(addressArray_Set_encoded),
    #             prePackedTradeCallDataArray_encoded);
    #     }
    #
    #     // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_buyOn0xv3SellOnSet(
    #         uint256 etherToSpend,
    #         uint256[] memory intArray,
    #         Order memory order,
    #         address[] memory addressArray_Set,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public onlyWhitelist
    #     {
    #         // intArray[0] = minBidSize
    #         // intArray[1] = etherToSpendSafetiplier
    #         // intArray[2] = magicConverter_inflowTokensToShares
    #         // intArray[3] = minInflowTokenQuantity
    #         // intArray[4] = minOutflowTokenQuantity
    #
    #         // addressArray_Set[0] = rebalanceAuctionModuleAddress
    #         // addressArray_Set[1] = setTokenAddress
    #         // addressArray_Set[2] = inflowTokenAddress
    #         // addressArray_Set[3] = outflowTokenAddress
    #
    #         // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

    #     function tradeWithinFlashLoan_buyOn0xv3SellOnSet(
    #         uint256 borrowedQuoteTokens,
    #         uint256[] memory intArray,
    #         Order memory order,
    #         bytes memory signature,
    #         address[] memory addressArray_Set
    #         ) public onlyWhitelistedKeeperDAOTransferProxys

    addressArray_Set = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetInflowTokenAddressForTrading())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetOutflowTokenAddressForTrading())),
    ]

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    etherToSpendSafetiplier_weiUnits = GetEtherToSpendSafetiplier_SetAnd0x(setTokenData, order)

    dontCare1, priceChangeDueOnNextMinedBlock, magicConverter_inflowTokensToShares_onNextMinedBlock = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture(True)
    if priceChangeDueOnNextMinedBlock:
        message = "We're shipping with a future magicConverter. Check this over to make sure it's working properly!"
        Libraries.core.API_PostOperatorNotification(message)

    intArray = [
        int(setTokenData.minBidSize_weiUnits),
        int(etherToSpendSafetiplier_weiUnits),
        int(magicConverter_inflowTokensToShares_onNextMinedBlock),
        int(setTokenData.minInflowTokenQuantity_weiUnits),
        int(setTokenData.minOutflowTokenQuantity_weiUnits),
    ]

    # Because of complexities with logic, get the orderTuple unencoded here, then later we can get it encoded if we want to encode
    orderTuple = order.GenerateOrderTupleForTransaction()
    PrintAndLog("orderTuple = " + str(orderTuple))

    # Determine the prePackedTradeCallDataArray_encoded
    # Prep the calldata and function selector for the appropriate tradeWithinFlashLoan call
    params = 'uint256,uint256[],' + Exchanges.zrxV2.OrderTupleString + ',bytes,address[]'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeWithinFlashLoan_buyOn0xv3SellOnSet({params})")[0:4]
    method_parameters = encode_single(f"({params})", [
        # This will get replaced within the smart contract, so just set it as 0xFFFF...FFFF for now
        int(Libraries.core.DataF_Int),
        intArray,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        addressArray_Set,
    ])
    data_hex = '0x' + (method_signature + method_parameters).hex()
    prePackedTradeCallDataArray_encoded = PreparePackedTradeCallData_GivenCallData_Hex(data_hex)
    PrintAndLog("prePackedTradeCallDataArray_encoded = " + str(prePackedTradeCallDataArray_encoded))

    doEncode = True
    data_hex = None
    if doEncode:
        orderTuple_encoded = order.GenerateOrderTupleForTransaction(True)
        PrintAndLog("orderTuple_encoded = " + str(orderTuple_encoded))

        params = 'uint256,uint256[],' + Exchanges.zrxV2.OrderTupleString_Encoded + ',uint256[],uint256[]'
        PrintAndLog("params = " + str(params))
        method_signature = Web3.sha3(text=f"tradeSimpleRequirements_encoded_buyOn0xv3SellOnSet({params})")[0:4]
        method_parameters = encode_single(f"({params})", [
            int(etherToSpend_weiUnits),
            intArray,
            orderTuple_encoded,
            Libraries.ninjaEncoding.PackAddressList(addressArray_Set),
            prePackedTradeCallDataArray_encoded,
        ])
        data_hex = '0x' + (method_signature + method_parameters).hex()

    else:
        params = 'uint256,uint256[],' + Exchanges.zrxV2.OrderTupleString + ',address[],uint256[]'
        PrintAndLog("params = " + str(params))
        method_signature = Web3.sha3(text=f"tradeSimpleRequirements_buyOn0xv3SellOnSet({params})")[0:4]
        method_parameters = encode_single(f"({params})", [
            int(etherToSpend_weiUnits),
            intArray,
            orderTuple,
            addressArray_Set,
            prePackedTradeCallDataArray_encoded,
        ])
        data_hex = '0x' + (method_signature + method_parameters).hex()

    value_wei_int = 0
    transactionIdList = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                                 gas, gasPrice, data_hex, TransactionType.trade, nonceToUse,
                                                 True, True, Libraries.defaults.AutoGasPriceIncrementIterations_NinjaBetweenSetAnd0x)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionIdList:
        message = "Ninja:API_TradeSimpleRequirements_BuyOn0xv3SellOnSet: transactionIdList: " + str(transactionIdList)
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOn0xv3SellOnSet: no transactionIdList")

    return transactionIdList


def API_TradeSimpleRequirements_BuyOnSetSellOn0xv3(contractAddress, contractObject,
                                                   fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                   order, setTokenData, useCTokenBidderContract,
                                                   gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnSetSellOn0xv3: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_encoded_buyOnSetSellOn0xv3(
    #         uint256 etherToSpend,
    #         uint256[] memory intArray,
    #         Order_Encoded memory order_encoded,
    #         uint256[] memory addressArray_Set_encoded,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public
    #     {
    #         tradeSimpleRequirements_buyOnSetSellOn0xv3(
    #             etherToSpend, intArray,
    #             decodeOrder(order_encoded),
    #             decodeAddressArray(addressArray_Set_encoded),
    #             prePackedTradeCallDataArray_encoded);
    #     }
    #
    #     // Trades if state meets simple requirements
    #     function tradeSimpleRequirements_buyOnSetSellOn0xv3(
    #         uint256 etherToSpend,
    #         uint256[] memory intArray,
    #         Order memory order,
    #         address[] memory addressArray_Set,
    #         uint256[] memory prePackedTradeCallDataArray_encoded
    #         ) public onlyWhitelist
    #     {
    #         // intArray[0] = minBidSize
    #         // intArray[1] = etherToSpendSafetiplier
    #         // intArray[2] = magicConverter_inflowTokensToShares
    #         // intArray[3] = minInflowTokenQuantity
    #         // intArray[4] = minOutflowTokenQuantity
    #
    #         // addressArray_Set[0] = rebalanceAuctionModuleAddress
    #         // addressArray_Set[1] = setTokenAddress
    #         // addressArray_Set[2] = inflowTokenAddress
    #         // addressArray_Set[3] = outflowTokenAddress
    #
    #         // prePackedTradeCallDataArray_encoded = encoded appropriate tradeWithinFlashLoan function calldata

    #     function tradeWithinFlashLoan_buyOnSetSellOn0xv3(
    #         uint256 borrowedQuoteTokens,
    #         uint256[] memory intArray,
    #         Order memory order,
    #         bytes memory signature,
    #         address[] memory addressArray_Set
    #         ) public onlyWhitelistedKeeperDAOTransferProxys

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    etherToSpendSafetiplier_weiUnits = GetEtherToSpendSafetiplier_SetAnd0x(setTokenData, order)

    dontCare1, priceChangeDueOnNextMinedBlock, magicConverter_inflowTokensToShares_onNextMinedBlock = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture(True)
    if priceChangeDueOnNextMinedBlock:
        message = "We're shipping with a future magicConverter. Check this over to make sure it's working properly!"
        Libraries.core.API_PostOperatorNotification(message)

    intArray = [
        int(setTokenData.minBidSize_weiUnits),
        int(etherToSpendSafetiplier_weiUnits),
        int(magicConverter_inflowTokensToShares_onNextMinedBlock),
        int(setTokenData.minInflowTokenQuantity_weiUnits),
        int(setTokenData.minOutflowTokenQuantity_weiUnits),
    ]

    addressArray_Set = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetInflowTokenAddressForTrading())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetOutflowTokenAddressForTrading())),
    ]

    # Because of complexities with logic, get the orderTuple unencoded here, then later we can get it encoded if we want to encode
    orderTuple = order.GenerateOrderTupleForTransaction()
    PrintAndLog("orderTuple = " + str(orderTuple))

    # Determine the prePackedTradeCallDataArray_encoded
    # Prep the calldata and function selector for the appropriate tradeWithinFlashLoan call
    params = 'uint256,uint256[],' + Exchanges.zrxV2.OrderTupleString + ',bytes,address[]'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"tradeWithinFlashLoan_buyOnSetSellOn0xv3({params})")[0:4]
    method_parameters = encode_single(f"({params})", [
        # This will get replaced within the smart contract, so just set it as 0xFFFF...FFFF for now
        int(Libraries.core.DataF_Int),
        intArray,
        orderTuple,
        Web3.toBytes(hexstr=str(order.signature)),
        addressArray_Set,
    ])
    data_hex = '0x' + (method_signature + method_parameters).hex()
    prePackedTradeCallDataArray_encoded = PreparePackedTradeCallData_GivenCallData_Hex(data_hex)
    PrintAndLog("prePackedTradeCallDataArray_encoded = " + str(prePackedTradeCallDataArray_encoded))

    doEncode = True
    data_hex = None
    if doEncode:
        orderTuple_encoded = order.GenerateOrderTupleForTransaction(True)
        PrintAndLog("orderTuple_encoded = " + str(orderTuple_encoded))

        params = 'uint256,uint256[],' + Exchanges.zrxV2.OrderTupleString_Encoded + ',uint256[],uint256[]'
        PrintAndLog("params = " + str(params))
        method_signature = Web3.sha3(text=f"tradeSimpleRequirements_encoded_buyOnSetSellOn0xv3({params})")[0:4]
        method_parameters = encode_single(f"({params})", [
            int(etherToSpend_weiUnits),
            intArray,
            orderTuple_encoded,
            Libraries.ninjaEncoding.PackAddressList(addressArray_Set),
            prePackedTradeCallDataArray_encoded,
        ])
        data_hex = '0x' + (method_signature + method_parameters).hex()

    else:
        params = 'uint256,uint256[],' + Exchanges.zrxV2.OrderTupleString + ',address[],uint256[]'
        PrintAndLog("params = " + str(params))
        method_signature = Web3.sha3(text=f"tradeSimpleRequirements_buyOnSetSellOn0xv3({params})")[0:4]
        method_parameters = encode_single(f"({params})", [
            int(etherToSpend_weiUnits),
            intArray,
            orderTuple,
            addressArray_Set,
            prePackedTradeCallDataArray_encoded,
        ])
        data_hex = '0x' + (method_signature + method_parameters).hex()

    value_wei_int = 0
    transactionIdList = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                                 gas, gasPrice, data_hex, TransactionType.trade, nonceToUse,
                                                 True, True, Libraries.defaults.AutoGasPriceIncrementIterations_NinjaBetweenSetAnd0x)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionIdList:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnSetSellOn0xv3: transactionIdList: " + str(transactionIdList)
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnSetSellOn0xv3: no transactionIdList")

    return transactionIdList


def API_TradeSimpleRequirements_BuyOnUniswapSellOnSet(contractAddress, contractObject,
                                                      fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                      tokenAddress, maxUniswapEthQuantityAcceptable_etherUnits,
                                                      setTokenData, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOnSet: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    # function tradeSimpleRequirements_buyOnUniswapSellOnSet(
    #         uint256 etherToSpend,
    #         uint256 maxUniswapEthQuantityAcceptable,
    #         uint256 minBidSize_Set,
    #         address[] memory addressArray_Uniswap,
    #         address[] memory addressArray_Set
    #         ) public onlyOwnerOrWhitelist
    #     {
    #         // addressArray_Uniswap[0] = uniswapContract,
    #
    #         // addressArray_Set[0] = rebalanceAuctionModuleAddress
    #         // addressArray_Set[1] = setTokenAddress
    #         // addressArray_Set[2] = inflowTokenAddress
    #         // addressArray_Set[3] = outflowTokenAddress

    addressArray_Set = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetInflowTokenAddressForTrading())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetOutflowTokenAddressForTrading())),
    ]

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[tokenAddress.lower()]['exchange'])
    addressArray_Uniswap = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
    ]

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    maxUniswapEthQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(maxUniswapEthQuantityAcceptable_etherUnits, Libraries.core.Ether_Decimals)

    kwargs = {
        'etherToSpend': int(etherToSpend_weiUnits),
        'maxUniswapEthQuantityAcceptable': int(maxUniswapEthQuantityAcceptable_weiUnits),
        'minBidSize_Set': int(setTokenData.minBidSize_weiUnits),
        'addressArray_Uniswap': addressArray_Uniswap,
        'addressArray_Set': addressArray_Set,
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnUniswapSellOnSet', kwargs=kwargs)
    # PrintAndLog("data_hex: " + str(data_hex))

    value_wei_int = 0
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOnSet: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnUniswapSellOnSet: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnSetSellOnUniswap(contractAddress, contractObject,
                                                      fromAddress, fromPrivateKey, etherToSpend_etherUnits,
                                                      tokenAddress, minUniswapEthQuantityAcceptable_etherUnits,
                                                      setTokenData, gas, gasPrice, nonceToUse=None):
    PrintAndLog("Ninja:API_TradeSimpleRequirements_BuyOnSetSellOnUniswap: fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    # function tradeSimpleRequirements_buyOnSetSellOnUniswap(
    #         uint256 etherToSpend,
    #         uint256 minUniswapEthQuantityAcceptable,
    #         uint256 minBidSize_Set,
    #         address[] memory addressArray_Uniswap,
    #         address[] memory addressArray_Set
    #         ) public onlyOwnerOrWhitelist
    #     {
    #         // addressArray_Uniswap[0] = uniswapContract,
    #
    #         // addressArray_Set[0] = rebalanceAuctionModuleAddress
    #         // addressArray_Set[1] = setTokenAddress
    #         // addressArray_Set[2] = inflowTokenAddress
    #         // addressArray_Set[3] = outflowTokenAddress

    addressArray_Set = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetInflowTokenAddressForTrading())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.GetOutflowTokenAddressForTrading())),
    ]

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[tokenAddress.lower()]['exchange'])
    addressArray_Uniswap = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)),
    ]

    etherToSpend_weiUnits = Libraries.core.ConvertEtherToWei(etherToSpend_etherUnits, Libraries.core.Ether_Decimals)
    minUniswapEthQuantityAcceptable_weiUnits = Libraries.core.ConvertEtherToWei(minUniswapEthQuantityAcceptable_etherUnits, Libraries.core.Ether_Decimals)

    kwargs = {
        'etherToSpend': int(etherToSpend_weiUnits),
        'minUniswapEthQuantityAcceptable': int(minUniswapEthQuantityAcceptable_weiUnits),
        'minBidSize_Set': int(setTokenData.minBidSize_weiUnits),
        'addressArray_Uniswap': addressArray_Uniswap,
        'addressArray_Set': addressArray_Set,
    }
    PrintAndLog("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnSetSellOnUniswap', kwargs=kwargs)
    # PrintAndLog("data_hex: " + str(data_hex))
    value_wei_int = 0
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, value_wei_int,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Ninja:API_TradeSimpleRequirements_BuyOnSetSellOnUniswap: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to Ninja:API_TradeSimpleRequirements_BuyOnSetSellOnUniswap: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnSetSellOnKyber_TokenForToken(contractAddress, contractObject,
                                                                  fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                  quoteTokenAddress, baseTokenAddress, setTokenData,
                                                                  gas, gasPrice, nonceToUse=None):
    PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     function tradeSimpleRequirements_buyOnSetSellOnKyber(
    #         uint256 quoteTokensToSpend,
    #         uint256[] memory intArray,
    #         address[] memory addressArray
    #         ) public onlyWhitelist
    #     {
    #         // intArray[0] = minBidSize
    #         // intArray[1] = magicConverter_inflowTokensToShares
    #         // intArray[2] = minInflowTokenQuantity
    #         // intArray[3] = minOutflowTokenQuantity
    #         // intArray[4] = quoteTokenDecimals
    #         // intArray[5] = baseTokenDecimals
    #
    #         // addressArray[0] = quoteTokenAddress WBTC
    #         // addressArray[1] = baseTokenAddress USDC (or other ERC20 token)
    #         // addressArray[2] = rebalanceAuctionModuleAddress
    #         // addressArray[3] = setTokenAddress

    decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteTokenAddress)))
    decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseTokenAddress)))
    quoteTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei(quoteTokensToSpend_etherUnits, decimals_quoteToken)

    dontCare1, priceChangeDueOnNextMinedBlock, magicConverter_inflowTokensToShares_onNextMinedBlock = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture(True)

    intArray = [
        int(setTokenData.minBidSize_weiUnits),
        int(magicConverter_inflowTokensToShares_onNextMinedBlock),
        int(setTokenData.minInflowTokenQuantity_weiUnits),
        int(setTokenData.minOutflowTokenQuantity_weiUnits),
        int(decimals_quoteToken),
        int(decimals_baseToken),
    ]

    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(quoteTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(baseTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
    ]

    kwargs = {
        'quoteTokensToSpend': int(quoteTokensToSpend_weiUnits),
        'intArray': intArray,
        'addressArray': addressArray,
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnSetSellOnKyber', kwargs=kwargs)
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        PrintAndLog_FuncNameHeader("transactionId: " + transactionId)
    else:
        PrintAndLog_FuncNameHeader("Failed: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnKyberSellOnSet_TokenForToken(contractAddress, contractObject,
                                                                  fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                  quoteTokenAddress, baseTokenAddress, setTokenData,
                                                                  gas, gasPrice, nonceToUse=None):
    PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     function tradeSimpleRequirements_buyOnKyberSellOnSet(
    #         uint256 quoteTokensToSpend,
    #         uint256[] memory intArray,
    #         address[] memory addressArray
    #         ) public onlyWhitelist
    #     {
    #         // intArray[0] = minBidSize
    #         // intArray[1] = magicConverter_inflowTokensToShares
    #         // intArray[2] = minInflowTokenQuantity
    #         // intArray[3] = minOutflowTokenQuantity
    #         // intArray[4] = quoteTokenDecimals
    #         // intArray[5] = baseTokenDecimals
    #
    #         // addressArray[0] = quoteTokenAddress WBTC
    #         // addressArray[1] = baseTokenAddress USDC (or other ERC20 token)
    #         // addressArray[2] = rebalanceAuctionModuleAddress
    #         // addressArray[3] = setTokenAddress

    decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteTokenAddress)))
    decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseTokenAddress)))
    quoteTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei(quoteTokensToSpend_etherUnits, decimals_quoteToken)

    dontCare1, priceChangeDueOnNextMinedBlock, magicConverter_inflowTokensToShares_onNextMinedBlock = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture(True)

    intArray = [
        int(setTokenData.minBidSize_weiUnits),
        int(magicConverter_inflowTokensToShares_onNextMinedBlock),
        int(setTokenData.minInflowTokenQuantity_weiUnits),
        int(setTokenData.minOutflowTokenQuantity_weiUnits),
        int(decimals_quoteToken),
        int(decimals_baseToken),
    ]

    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(quoteTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(baseTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
    ]

    kwargs = {
        'quoteTokensToSpend': int(quoteTokensToSpend_weiUnits),
        'intArray': intArray,
        'addressArray': addressArray,
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array

    data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnKyberSellOnSet', kwargs=kwargs)
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        PrintAndLog_FuncNameHeader("transactionId: " + transactionId)
    else:
        PrintAndLog_FuncNameHeader("Failed: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnUniswapSellOnSet_TokenForToken(contractAddress, contractObject,
                                                                    fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                    quoteTokenAddress, baseTokenAddress,
                                                                    setTokenData, gas, gasPrice, nonceToUse=None):
    PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     function tradeSimpleRequirements_buyOnUniswapSellOnSet(
    #         uint256 quoteTokensToSpend,
    #         address[] memory addressArray,
    #         uint256[] memory intArray
    #         ) public onlyWhitelist
    #     {
    #         // addressArray[0] = quoteTokenAddress WBTC
    #         // addressArray[1] = baseTokenAddress USDC (or other ERC20 token)
    #         // addressArray[2] = rebalanceAuctionModuleAddress
    #         // addressArray[3] = setTokenAddress
    #         // addressArray[4] = uniswapContract_quoteToken
    #         // addressArray[5] = uniswapContract_baseToken
    #
    #         // intArray[0] = minBidSize
    #         // intArray[1] = quoteTokenDecimals
    #         // intArray[2] = baseTokenDecimals
    #         // intArray[3] = expectedBaseTokensToTrade
    #         // intArray[4] = magicConverter_inflowTokensToShares
    #         // intArray[5] = minInflowTokenQuantity
    #         // intArray[6] = minOutflowTokenQuantity

    decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteTokenAddress)))
    decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseTokenAddress)))

    dontCare1, priceChangeDueOnNextMinedBlock, magicConverter_inflowTokensToShares_onNextMinedBlock = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture(True)

    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(quoteTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(baseTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[quoteTokenAddress.lower()]['exchange'])),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[baseTokenAddress.lower()]['exchange'])),
    ]

    intArray = [
        int(setTokenData.minBidSize_weiUnits),
        int(decimals_quoteToken),
        int(decimals_baseToken),
        int(0),  # This isn't being used for this specific function call
        int(magicConverter_inflowTokensToShares_onNextMinedBlock),
        int(setTokenData.minInflowTokenQuantity_weiUnits),
        int(setTokenData.minOutflowTokenQuantity_weiUnits),
    ]

    quoteTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei(quoteTokensToSpend_etherUnits, decimals_quoteToken)

    kwargs = {
        'quoteTokensToSpend': int(quoteTokensToSpend_weiUnits),
        'addressArray': addressArray,
        'intArray': intArray,
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnUniswapSellOnSet', kwargs=kwargs)
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        PrintAndLog_FuncNameHeader("transactionId: " + transactionId)
    else:
        PrintAndLog_FuncNameHeader("Failed: no transactionId")

    return transactionId


def API_TradeSimpleRequirements_BuyOnSetSellOnUniswap_TokenForToken(contractAddress, contractObject,
                                                                    fromAddress, fromPrivateKey, quoteTokensToSpend_etherUnits,
                                                                    quoteTokenAddress, baseTokenAddress, expectedBaseTokensToTrade_etherUnits,
                                                                    setTokenData, gas, gasPrice, nonceToUse=None):
    PrintAndLog_FuncNameHeader("fromAddress = " + str(fromAddress) + ", gas = " + str(gas) + ", gasPrice = " + str(gasPrice))
    #     function tradeSimpleRequirements_buyOnSetSellOnUniswap(
    #         uint256 quoteTokensToSpend,
    #         address[] memory addressArray,
    #         uint256[] memory intArray
    #         ) public onlyWhitelist
    #     {
    #         // addressArray[0] = quoteTokenAddress WBTC
    #         // addressArray[1] = baseTokenAddress USDC (or other ERC20 token)
    #         // addressArray[2] = rebalanceAuctionModuleAddress
    #         // addressArray[3] = setTokenAddress
    #         // addressArray[4] = uniswapContract_quoteToken
    #         // addressArray[5] = uniswapContract_baseToken
    #
    #         // intArray[0] = minBidSize
    #         // intArray[1] = quoteTokenDecimals
    #         // intArray[2] = baseTokenDecimals
    #         // intArray[3] = expectedBaseTokensToTrade
    #         // intArray[4] = magicConverter_inflowTokensToShares
    #         // intArray[5] = minInflowTokenQuantity
    #         // intArray[6] = minOutflowTokenQuantity

    decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteTokenAddress)))
    decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseTokenAddress)))

    expectedBaseTokensToTrade_weiUnits = Libraries.core.ConvertEtherToWei(expectedBaseTokensToTrade_etherUnits, decimals_baseToken)

    dontCare1, priceChangeDueOnNextMinedBlock, magicConverter_inflowTokensToShares_onNextMinedBlock = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture(True)

    addressArray = [
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(quoteTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(baseTokenAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.DetermineRebalanceAuctionModule())),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenData.setAddress)),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[quoteTokenAddress.lower()]['exchange'])),
        str(Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[baseTokenAddress.lower()]['exchange'])),
    ]

    intArray = [
        int(setTokenData.minBidSize_weiUnits),
        int(decimals_quoteToken),
        int(decimals_baseToken),
        int(expectedBaseTokensToTrade_weiUnits),
        int(magicConverter_inflowTokensToShares_onNextMinedBlock),
        int(setTokenData.minInflowTokenQuantity_weiUnits),
        int(setTokenData.minOutflowTokenQuantity_weiUnits),
    ]

    quoteTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei(quoteTokensToSpend_etherUnits, decimals_quoteToken)

    kwargs = {
        'quoteTokensToSpend': int(quoteTokensToSpend_weiUnits),
        'addressArray': addressArray,
        'intArray': intArray,
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = contractObject.encodeABI('tradeSimpleRequirements_buyOnSetSellOnUniswap', kwargs=kwargs)
    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, contractAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.trade, nonceToUse, True, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        PrintAndLog_FuncNameHeader("transactionId: " + transactionId)
    else:
        PrintAndLog_FuncNameHeader("Failed: no transactionId")

    return transactionId


def API_GetManyArbitrages_BuyOnKyberSellOnSet(contractAddress, contractObject,
                                              token, setTokenAddress, amountsToConvert_Kyber_weiUnits, minBidSize_weiUnits):
    # function getManyArbitrages_buyOnKyberSellOnSet(
    # address[] memory addressArray_Kyber, address[] memory addressArray_Set,
    # uint[] memory amountsToConvert_Kyber, uint256 minBidSize_Set
    # ) public view returns (uint[] memory)

    addressArray_Kyber = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
    ]

    addressArray_Set = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.set.Contract_RebalanceAuctionModule),
        Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)
    ]

    kwargs = {
        'addressArray_Kyber': addressArray_Kyber,
        'addressArray_Set': addressArray_Set,
        'amountsToConvert_Kyber': amountsToConvert_Kyber_weiUnits,
        'minBidSize_Set': int(minBidSize_weiUnits),
    }
    PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnSet: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": contractObject.encodeABI('getManyArbitrages_buyOnKyberSellOnSet', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
            },
        ]
    }
    PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnSet: payload = " + str(payload))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnSet responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnSet jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnSet dataList = " + str(dataList))
        returnList = ParseResponseData_GetManyArbitrages(dataList)
        # PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnSet returnList = " + str(returnList))
        return returnList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_BuyOnKyberSellOnSet response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_GetManyArbitrages_BuyOnSetSellOnKyber(contractAddress, contractObject,
                                              token, setTokenAddress, inflowTokensToSpend_Set_weiUnits, minBidSize_weiUnits):
    # function getManyArbitrages_buyOnSetSellOnKyber(
    # address[] memory addressArray_Kyber, address[] memory addressArray_Set,
    # uint[] memory inflowTokensToSpend_Set, uint256 minBidSize_Set
    # ) public view returns (uint[] memory)

    addressArray_Kyber = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(token.erc20TokenContractAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.EtherToken),
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.kyber.Contract_KyberNetworkProxy)
    ]

    addressArray_Set = [
        Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.set.Contract_RebalanceAuctionModule),
        Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress)
    ]

    kwargs = {
        'addressArray_Kyber': addressArray_Kyber,
        'addressArray_Set': addressArray_Set,
        'inflowTokensToSpend_Set': inflowTokensToSpend_Set_weiUnits,
        'minBidSize_Set': int(minBidSize_weiUnits),
    }
    PrintAndLog("API_GetManyArbitrages_BuyOnSetSellOnKyber: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": contractObject.encodeABI('getManyArbitrages_buyOnSetSellOnKyber', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
            },
        ]
    }
    PrintAndLog("API_GetManyArbitrages_BuyOnSetSellOnKyber: payload = " + str(kwargs))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetManyArbitrages_BuyOnSetSellOnKyber responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyArbitrages_BuyOnSetSellOnKyber jData = " + str(jData))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetManyArbitrages_BuyOnSetSellOnKyber dataList = " + str(dataList))
        returnList = ParseResponseData_GetManyArbitrages(dataList)
        # PrintAndLog("API_GetManyArbitrages_BuyOnSetSellOnKyber returnList = " + str(returnList))
        return returnList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_BuyOnSetSellOnKyber response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_GetVersion(contractAddress, contractObject):
    kwargs = {}
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": contractObject.encodeABI('Version', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(contractAddress)
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetVersion jData = " + str(jData))
        version = Libraries.core.ConvertResponseDataToReadableString(jData['result'])
        # PrintAndLog("API_GetVersion version = " + str(version))
        return version

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_BuyOnBancorSellOnKyber response was not ok response = " + str(response))
        response.raise_for_status()


def SetLastDateTime_NinjaTradeReverted():
    global LastDateTime_NinjaTradeReverted
    LastDateTime_NinjaTradeReverted = datetime.datetime.now()


def API_GetPositiveArbitrageTokens_New_BuyOnKyberSellOnUniswap_Batched(tokenList, etherToSpendList_weiUnits, resultDict):
    returnDict_returnList_Eth, returnDict_returnList_Tokens, returnDict_returnList_Rate, returnDict_balanceEther_UniswapContract_etherUnits = \
        API_GetManyArbitrages_New_BuyOnKyberSellOnUniswap_Batched(tokenList, etherToSpendList_weiUnits, resultDict)

    profitableTokensList = []
    for index, token in enumerate(tokenList):
        if token not in returnDict_returnList_Eth:
            PrintAndLog_FuncNameHeader("No profit was found for " + str(token.tokenName) + ", the call failed?")
            continue

        returnList_Eth = returnDict_returnList_Eth[token]
        for index_2, etherToReceive_etherUnits in enumerate(returnList_Eth):
            etherToSpend_weiUnits = etherToSpendList_weiUnits[index_2]
            etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
            if etherToReceive_etherUnits > etherToSpend_etherUnits:
                PrintAndLog_FuncNameHeader("Profit was found for " + str(token.tokenName) + ", etherToSpend_etherUnits = " + str(
                    etherToSpend_etherUnits) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits))
                profitableTokensList.append(token)
                # Go to the next token
                break
            else:
                pass
                # PrintAndLog_FuncNameHeader("No profit was found for " + str(token.tokenName) + ", etherToSpend_etherUnits = " + str(
                #     etherToSpend_etherUnits) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits))

    PrintAndLog_FuncNameHeader("profit was found in " + str(len(profitableTokensList)) + " out of " + str(
        len(tokenList)) + " tokens. Profitable tokens are = " + str(Libraries.market.GetListOfTokenNamesFromTokenList(profitableTokensList)))

    # Store the result in resultDict in case the function caller calls from within a thread
    resultDict['profitableTokensList'] = profitableTokensList
    return profitableTokensList


def API_GetManyArbitrages_New_BuyOnKyberSellOnUniswap_Batched(tokenList, etherToSpendList_weiUnits, resultDict):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    for index_token, token in enumerate(tokenList):
        # PrintAndLog_FuncNameHeader("Creating payload_total for " + str(token.tokenName) + " " + str(index_token))

        side_kyber = 'buy'
        side_uniswap = 'sell'
        src_kyber = Exchanges.kyber.EtherToken
        dest_kyber = token.erc20TokenContractAddress

        uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

        payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Token.value)] = payloadId
        payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract, token.erc20TokenContractAddress, payloadId))
        payloadId += 1

        payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Ether.value)] = payloadId
        payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract, payloadId))
        payloadId += 1

        # Get the expectedRate for kyber itself
        payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)] = payloadId
        for etherToSpend_weiUnits in etherToSpendList_weiUnits:
            payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, etherToSpend_weiUnits, payloadId))
            payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    # before = datetime.datetime.now()
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog_FuncNameHeader("duration_s = " + str(duration_s))
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        # Keyed by token
        returnDict_returnList_Eth = {}
        returnDict_returnList_Tokens = {}
        returnDict_returnList_Rate = {}
        returnDict_balanceEther_UniswapContract_etherUnits = {}

        for index_token, token in enumerate(tokenList):
            try:
                # PrintAndLog_FuncNameHeader("Parsing payloadResultDict for " + str(token.tokenName) + " " + str(index_token))

                balanceEther_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance_Value(payloadResultDict, index_token)
                balanceTokens_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance_Value(payloadResultDict, index_token, token.decimals)

                returnList_Eth = []
                returnList_Tokens = []
                returnList_Rate = []
                # Determine results
                for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
                    # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
                    etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
                    # PrintAndLog_FuncNameHeader("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")

                    # PrintAndLog_FuncNameHeader("payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)] has len of " + str(
                    #     len(payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)])))
                    # Get the rate from the KyberProxy
                    result = payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)][index]
                    result = result.replace("0x", "")
                    splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
                    # PrintAndLog_FuncNameHeader("splitArray = " + str(splitArray))
                    # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
                    # PrintAndLog_FuncNameHeader("Libraries.core.ConvertHexToInt(splitArray[0]) + " + str(Libraries.core.ConvertHexToInt(splitArray[0])))
                    rate_kyberProxy = Libraries.core.ConvertWeiToEther(Libraries.core.ConvertHexToInt(splitArray[0]), Libraries.core.Ether_Decimals)
                    # PrintAndLog_FuncNameHeader("rate_kyberProxy = " + str(rate_kyberProxy))

                    if rate_kyberProxy == 0:
                        # If rate is zero, the return on the trade is zero
                        returnList_Eth.append(0)
                        returnList_Tokens.append(0)
                    else:
                        price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberProxy, side_kyber)
                        # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberProxy = " + str(rate_kyberProxy))

                        tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_kyber)
                        # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
                        price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances(
                            side_uniswap, tokenQuantity_etherUnits, balanceEther_UniswapContract_etherUnits, balanceTokens_UniswapContract_etherUnits)
                        # PrintAndLog_FuncNameHeader("price_uniswap = " + str(price_uniswap))

                        etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_kyber, price_uniswap)
                        profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
                        # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

                        returnList_Eth.append(etherToReceive_etherUnits)
                        returnList_Tokens.append(tokenQuantity_etherUnits)

                    returnList_Rate.append(rate_kyberProxy)

                # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
                # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
                # PrintAndLog_FuncNameHeader("returnList_Rate = " + str(returnList_Rate))
                # PrintAndLog_FuncNameHeader("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))

                returnDict_returnList_Eth[token] = returnList_Eth
                returnDict_returnList_Tokens[token] = returnList_Tokens
                returnDict_returnList_Rate[token] = returnList_Rate
                returnDict_balanceEther_UniswapContract_etherUnits[token] = balanceEther_UniswapContract_etherUnits

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception = " + traceback.format_exc())
                continue

        # Set the resultDict for callers who are using a thread and cannot access the return value
        resultDict['returnDict_returnList_Eth'] = returnDict_returnList_Eth
        resultDict['returnDict_returnList_Tokens'] = returnDict_returnList_Tokens
        resultDict['returnDict_returnList_Rate'] = returnDict_returnList_Rate
        resultDict['returnDict_balanceEther_UniswapContract_etherUnits'] = returnDict_balanceEther_UniswapContract_etherUnits

        return returnDict_returnList_Eth, returnDict_returnList_Tokens, returnDict_returnList_Rate, returnDict_balanceEther_UniswapContract_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetPositiveArbitrageTokens_New_BuyOnUniswapSellOnKyber_Batched(tokenList, etherToSpendList_weiUnits, resultDict):
    returnDict_returnList_Eth, returnDict_returnList_Tokens, returnDict_returnList_Rate, returnDict_balanceEther_UniswapContract_etherUnits = \
        API_GetManyArbitrages_New_BuyOnUniswapSellOnKyber_Batched(tokenList, etherToSpendList_weiUnits, resultDict)

    profitableTokensList = []
    for index, token in enumerate(tokenList):
        if token not in returnDict_returnList_Eth:
            PrintAndLog_FuncNameHeader("No profit was found for " + str(token.tokenName) + ", the call failed?")
            continue

        returnList_Eth = returnDict_returnList_Eth[token]
        for index_2, etherToReceive_etherUnits in enumerate(returnList_Eth):
            etherToSpend_weiUnits = etherToSpendList_weiUnits[index_2]
            etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
            if etherToReceive_etherUnits > etherToSpend_etherUnits:
                PrintAndLog_FuncNameHeader("Profit was found for " + str(token.tokenName) + ", etherToSpend_etherUnits = " + str(
                    etherToSpend_etherUnits) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits))
                profitableTokensList.append(token)
                # Go to the next token
                break
            else:
                pass
                # PrintAndLog_FuncNameHeader("No profit was found for " + str(token.tokenName) + ", etherToSpend_etherUnits = " + str(
                #     etherToSpend_etherUnits) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits))

    PrintAndLog_FuncNameHeader("profit was found in " + str(len(profitableTokensList)) + " out of " + str(
        len(tokenList)) + " tokens. Profitable tokens are = " + str(Libraries.market.GetListOfTokenNamesFromTokenList(profitableTokensList)))

    # Store the result in resultDict in case the function caller calls from within a thread
    resultDict['profitableTokensList'] = profitableTokensList
    return profitableTokensList


# TODO REMOVEME DEPRECATED
# def API_GetManyArbitrages_New_BuyOnUniswapSellOnKyber_Batched(tokenList, etherToSpendList_weiUnits, resultDict):
#     # Use this blockNumber_int for all calls by batching them together and specifying this
#     blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
#
#     payload_total = []
#     payloadIdDict = {}
#     payloadId = randint(0, 99999999999999)
#
#     for index_token, token in enumerate(tokenList):
#         # PrintAndLog_FuncNameHeader("Creating payload_total for " + str(token.tokenName) + " " + str(index_token))
#         uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])
#
#         # NOTE:  Calculating priceForConversion based on GetOnChainPrice_FromCache is safe to do when estimating,
#         #         but it's not safe to do when actually triggering a trade.
#         #         Right now this is fine because i'm only calling this batched version of this function
#         #         when i'm seeing what markets have profit, then I call it again, non batched on the markets I care about
#
#         priceForConversion = None
#         tokensToSpendList_weiUnits = None
#         try:
#             # We're selling on Kyber and kyber expects srcQty as input, but etherToSpendList_weiUnits is currently in ETH not tokens...
#             # Convert etherToSpendList_weiUnits to tokensToSpendList_weiUnits
#             priceForConversion = Libraries.priceOracle.GetOnChainPrice_FromCache(token.erc20TokenContractAddress)
#             tokensToSpendList_weiUnits = Libraries.utils.ConvertEtherListToTokensList(etherToSpendList_weiUnits, priceForConversion, token.decimals)
#             # PrintAndLog_FuncNameHeader("priceForConversion = " + str(priceForConversion) + ", token = " + str(token.tokenName))
#             # PrintAndLog_FuncNameHeader("etherToSpendList_weiUnits = " + str(etherToSpendList_weiUnits))
#             # PrintAndLog_FuncNameHeader("tokensToSpendList_weiUnits = " + str(tokensToSpendList_weiUnits))
#
#         except (KeyboardInterrupt, SystemExit):
#             print('\nkeyboard interrupt caught')
#             print('\n...Program Stopped Manually!')
#             raise
#
#         except:
#             PrintAndLogError("exception = " + traceback.format_exc())
#             continue
#
#         side_kyber = 'sell'
#         side_uniswap = 'buy'
#         src_kyber = token.erc20TokenContractAddress
#         dest_kyber = Exchanges.kyber.EtherToken
#
#         payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Token.value)] = payloadId
#         payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract, token.erc20TokenContractAddress, payloadId))
#         payloadId += 1
#
#         payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Ether.value)] = payloadId
#         payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract, payloadId))
#         payloadId += 1
#
#         # Get the expectedRate for kyber itself
#         payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)] = payloadId
#         for tokensToSpend_weiUnits in tokensToSpendList_weiUnits:
#             payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, tokensToSpend_weiUnits, payloadId))
#             payloadId += 1
#
#     # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
#     # before = datetime.datetime.now()
#     response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
#                                                     None, False, False, True, False, blockNumber_int)
#     if response.ok:
#         responseData = response.content
#         jDataList = json.loads(responseData)
#         # duration_s = (datetime.datetime.now() - before).total_seconds()
#         # PrintAndLog_FuncNameHeader("duration_s = " + str(duration_s))
#         # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))
#
#         # parse the results based on the ids in payloadIdDict
#         payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
#         # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))
#
#         # Keyed by token
#         returnDict_returnList_Eth = {}
#         returnDict_returnList_Tokens = {}
#         returnDict_returnList_Rate = {}
#         returnDict_balanceEther_UniswapContract_etherUnits = {}
#
#         for index_token, token in enumerate(tokenList):
#             try:
#                 # PrintAndLog_FuncNameHeader("Parsing payloadResultDict for " + str(token.tokenName) + " " + str(index_token))
#
#                 balanceEther_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance_Value(payloadResultDict, index_token)
#                 balanceTokens_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance_Value(payloadResultDict, index_token, token.decimals)
#
#                 returnList_Eth = []
#                 returnList_Tokens = []
#                 returnList_Rate = []
#                 # Determine results
#                 for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
#                     # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
#                     etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
#                     # PrintAndLog_FuncNameHeader("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")
#                     price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances(
#                         side_uniswap, etherToSpend_etherUnits, balanceEther_UniswapContract_etherUnits, balanceTokens_UniswapContract_etherUnits)
#                     # PrintAndLog_FuncNameHeader("price_uniswap = " + str(price_uniswap))
#
#                     tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_uniswap)
#                     # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
#
#                     # Get the rate from the KyberProxy
#                     result = payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)][index]
#                     result = result.replace("0x", "")
#                     splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
#                     # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
#                     rate_kyberProxy = Libraries.core.ConvertWeiToEther(Libraries.core.ConvertHexToInt(splitArray[0]), Libraries.core.Ether_Decimals)
#                     # PrintAndLog_FuncNameHeader("rate_kyberProxy = " + str(rate_kyberProxy))
#
#                     if rate_kyberProxy == 0:
#                         # If rate is zero, the return on the trade is zero
#                         returnList_Eth.append(0)
#                         returnList_Tokens.append(0)
#                     else:
#                         price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberProxy, side_kyber)
#                         # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberProxy = " + str(rate_kyberProxy))
#
#                         etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_uniswap, price_kyber)
#                         profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
#                         # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))
#
#                         returnList_Eth.append(etherToReceive_etherUnits)
#                         returnList_Tokens.append(tokenQuantity_etherUnits)
#
#                     returnList_Rate.append(rate_kyberProxy)
#
#             except (KeyboardInterrupt, SystemExit):
#                 print('\nkeyboard interrupt caught')
#                 print('\n...Program Stopped Manually!')
#                 raise
#
#             except:
#                 PrintAndLogError("exception = " + traceback.format_exc())
#                 continue
#
#             returnDict_returnList_Eth[token] = returnList_Eth
#             returnDict_returnList_Tokens[token] = returnList_Tokens
#             returnDict_returnList_Rate[token] = returnList_Rate
#             returnDict_balanceEther_UniswapContract_etherUnits[token] = balanceEther_UniswapContract_etherUnits
#
#         # Set the resultDict for callers who are using a thread and cannot access the return value
#         resultDict['returnDict_returnList_Eth'] = returnDict_returnList_Eth
#         resultDict['returnDict_returnList_Tokens'] = returnDict_returnList_Tokens
#         resultDict['returnDict_returnList_Rate'] = returnDict_returnList_Rate
#         resultDict['returnDict_balanceEther_UniswapContract_etherUnits'] = returnDict_balanceEther_UniswapContract_etherUnits
#
#         return returnDict_returnList_Eth, returnDict_returnList_Tokens, returnDict_returnList_Rate, returnDict_balanceEther_UniswapContract_etherUnits
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
#         response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnUniswapSellOnKyber_Batched(tokenList, etherToSpendList_weiUnits, resultDict):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    # Make a batched call to get all the uniswap balances we'll
    tokenAddressList = []
    for index_token, token in enumerate(tokenList):
        tokenAddressList.append(token.erc20TokenContractAddress.lower())

    # PrintAndLog_FuncNameHeader("Calling Exchanges.uniswap.API_GetExchangeBalances_Batched with tokenAddressList of len " + str(
    #     len(tokenAddressList)) + " = " + str(tokenAddressList))
    balanceDict_ether, balanceDict_tokens = Exchanges.uniswap.API_GetExchangeBalances_Batched(tokenAddressList)
    # PrintAndLog_FuncNameHeader("balanceDict_ether = " + str(balanceDict_ether))
    # PrintAndLog_FuncNameHeader("balanceDict_tokens = " + str(balanceDict_tokens))

    for index_token, token in enumerate(tokenList):
        # PrintAndLog_FuncNameHeader("Creating payload_total for " + str(token.tokenName) + " " + str(index_token))

        side_kyber = 'sell'
        side_uniswap = 'buy'
        src_kyber = token.erc20TokenContractAddress
        dest_kyber = Exchanges.kyber.EtherToken

        uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

        tokensToSpendList_weiUnits = None
        try:
            # I must set tokensToSpendList_weiUnits based on the actual uniswap prices
            # I must make the call to get uniswap ether/token balances BEFORE I can even make the call to get kyber reserve prices

            balanceEther_UniswapContract_etherUnits = balanceDict_ether[index_token]
            balanceTokens_UniswapContract_etherUnits = balanceDict_tokens[index_token]

            # PrintAndLog_FuncNameHeader("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))
            # PrintAndLog_FuncNameHeader("balanceTokens_UniswapContract_etherUnits = " + str(balanceTokens_UniswapContract_etherUnits))

            # We're selling on Kyber and kyber expects srcQty as input, but etherToSpendList_weiUnits is currently in ETH not tokens...
            # Convert etherToSpendList_weiUnits to tokensToSpendList_weiUnits
            etherToSpendList_etherUnits = Libraries.core.ConvertListOfWeisToEthers(etherToSpendList_weiUnits, Libraries.core.Ether_Decimals)

            returnPriceDict = Exchanges.uniswap.API_GetManyTradePrices(
                side_uniswap, [uniswapContract], [etherToSpendList_etherUnits],
                [balanceEther_UniswapContract_etherUnits], [balanceTokens_UniswapContract_etherUnits])

            # PrintAndLog_FuncNameHeader("returnPriceDict = " + str(returnPriceDict))
            priceList_uniswap = returnPriceDict[uniswapContract]
            # PrintAndLog_FuncNameHeader("priceList_uniswap = " + str(priceList_uniswap))
            # Now that we have all the prices, create a tokensToSpendList_weiUnits
            tokensToSpendList_weiUnits = Libraries.utils.ConvertQuoteListToBaseList_GivenPriceList(etherToSpendList_weiUnits, priceList_uniswap, token.decimals)
            # PrintAndLog_FuncNameHeader("etherToSpendList_weiUnits = " + str(etherToSpendList_weiUnits))
            # PrintAndLog_FuncNameHeader("tokensToSpendList_weiUnits = " + str(tokensToSpendList_weiUnits))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception = " + traceback.format_exc())
            continue

        payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Token.value)] = payloadId
        payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract, token.erc20TokenContractAddress, payloadId))
        payloadId += 1

        payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Uniswap_Balance_Ether.value)] = payloadId
        payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract, payloadId))
        payloadId += 1

        # Get the expectedRate for kyber itself
        payloadIdDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)] = payloadId
        for tokensToSpend_weiUnits in tokensToSpendList_weiUnits:
            payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, tokensToSpend_weiUnits, payloadId))
            payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    # before = datetime.datetime.now()
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog_FuncNameHeader("duration_s = " + str(duration_s))
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        # Keyed by token
        returnDict_returnList_Eth = {}
        returnDict_returnList_Tokens = {}
        returnDict_returnList_Rate = {}
        returnDict_balanceEther_UniswapContract_etherUnits = {}

        for index_token, token in enumerate(tokenList):
            try:
                # PrintAndLog_FuncNameHeader("Parsing payloadResultDict for " + str(token.tokenName) + " " + str(index_token))

                balanceEther_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance_Value(payloadResultDict, index_token)
                balanceTokens_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance_Value(payloadResultDict, index_token, token.decimals)

                returnList_Eth = []
                returnList_Tokens = []
                returnList_Rate = []
                # Determine results
                for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
                    # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
                    etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
                    # PrintAndLog_FuncNameHeader("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(
                    #     etherToSpend_weiUnits) + " wei")
                    price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances(
                        side_uniswap, etherToSpend_etherUnits, balanceEther_UniswapContract_etherUnits, balanceTokens_UniswapContract_etherUnits)
                    # PrintAndLog_FuncNameHeader("price_uniswap = " + str(price_uniswap))

                    tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_uniswap)
                    # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))

                    # Get the rate from the KyberProxy
                    result = payloadResultDict[Libraries.utils.GetIdBasedOnIndexNumber(index_token, PayloadIdName.Kyber_GetExpectedRate_Range.value)][index]
                    result = result.replace("0x", "")
                    splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
                    # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
                    rate_kyberProxy = Libraries.core.ConvertWeiToEther(Libraries.core.ConvertHexToInt(splitArray[0]), Libraries.core.Ether_Decimals)
                    # PrintAndLog_FuncNameHeader("rate_kyberProxy = " + str(rate_kyberProxy))

                    if rate_kyberProxy == 0:
                        # If rate is zero, the return on the trade is zero
                        returnList_Eth.append(0)
                        returnList_Tokens.append(0)
                    else:
                        price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberProxy, side_kyber)
                        # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberProxy = " + str(rate_kyberProxy))

                        etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_uniswap, price_kyber)
                        profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
                        # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(
                        #     etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

                        returnList_Eth.append(etherToReceive_etherUnits)
                        returnList_Tokens.append(tokenQuantity_etherUnits)

                    returnList_Rate.append(rate_kyberProxy)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception = " + traceback.format_exc())
                continue

            returnDict_returnList_Eth[token] = returnList_Eth
            returnDict_returnList_Tokens[token] = returnList_Tokens
            returnDict_returnList_Rate[token] = returnList_Rate
            returnDict_balanceEther_UniswapContract_etherUnits[token] = balanceEther_UniswapContract_etherUnits

        # Set the resultDict for callers who are using a thread and cannot access the return value
        resultDict['returnDict_returnList_Eth'] = returnDict_returnList_Eth
        resultDict['returnDict_returnList_Tokens'] = returnDict_returnList_Tokens
        resultDict['returnDict_returnList_Rate'] = returnDict_returnList_Rate
        resultDict['returnDict_balanceEther_UniswapContract_etherUnits'] = returnDict_balanceEther_UniswapContract_etherUnits

        return returnDict_returnList_Eth, returnDict_returnList_Tokens, returnDict_returnList_Rate, returnDict_balanceEther_UniswapContract_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnKyberSellOnUniswap(token, etherToSpendList_weiUnits, kyberReserve, resultDict):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

    side_kyber = 'buy'
    side_uniswap = 'sell'
    src_kyber = Exchanges.kyber.EtherToken
    dest_kyber = token.erc20TokenContractAddress

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Uniswap_Balance_Token] = payloadId
    payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract, token.erc20TokenContractAddress, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_Balance_Ether] = payloadId
    payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract, payloadId))
    payloadId += 1

    # Get conversion rate for the kyber reserve
    payloadIdDict[PayloadIdName.Kyber_GetConversionRate_Range] = payloadId
    for etherToSpend_weiUnits in etherToSpendList_weiUnits:
        payload_total.append(GetPayload_Kyber_GetConversionRate(kyberReserve, src_kyber, dest_kyber, etherToSpend_weiUnits, blockNumber_int, payloadId))
        payloadId += 1

    # Also get the expectedRate for kyber itself
    # I have to do this because I occasionally see cases where kyber proxy's trade function does not honor the conversion rate offered up by a kyber reserve.
    # I think somehow a kyber reserve is shutting down while their conversion rate remains despite not being tradable.
    payloadIdDict[PayloadIdName.Kyber_GetExpectedRate_Range] = payloadId
    for etherToSpend_weiUnits in etherToSpendList_weiUnits:
        payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, etherToSpend_weiUnits, payloadId))
        payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    # before = datetime.datetime.now()
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog_FuncNameHeader("duration_s = " + str(duration_s))
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        balanceEther_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance(payloadResultDict)
        balanceTokens_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance(payloadResultDict, token.decimals)

        returnList_Eth = []
        returnList_Tokens = []
        returnList_Rate = []
        # Determine results
        for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
            # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
            etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
            # PrintAndLog_FuncNameHeader("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")
            rate_kyberReserve = Libraries.core.ConvertWeiToEther(
                Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Kyber_GetConversionRate_Range][index]), Libraries.core.Ether_Decimals)
            # PrintAndLog_FuncNameHeader("rate_kyberReserve = " + str(rate_kyberReserve))

            # Get the rate from the KyberProxy
            result = payloadResultDict[PayloadIdName.Kyber_GetExpectedRate_Range][index]
            result = result.replace("0x", "")
            splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
            # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
            rate_kyberProxy = Libraries.core.ConvertWeiToEther(Libraries.core.ConvertHexToInt(splitArray[0]), Libraries.core.Ether_Decimals)
            # PrintAndLog_FuncNameHeader("rate_kyberProxy = " + str(rate_kyberProxy))
            # Ensure that Kyber is honoring this reserve's rate taking the worst rate between rate_kyberReserve and rate_kyberProxy
            # In other words, if kyber proxy's rate is WORSE than the kyber reserve's rate, that means this kyber reserve's rate is not valid!
            # I've asked Kyber about this... I'm not sure why a kyber reserve's rate would not be honored...
            rate_kyberReserve = Exchanges.kyber.GetWorstRate([rate_kyberReserve, rate_kyberProxy])
            # PrintAndLog_FuncNameHeader("worst rate between KyberReserve and KyberProxy = " + str(rate_kyberReserve))

            if rate_kyberReserve == 0:
                # If rate is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberReserve, side_kyber)
                # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberReserve = " + str(rate_kyberReserve))

                tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_kyber)
                # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
                price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances(
                    side_uniswap, tokenQuantity_etherUnits, balanceEther_UniswapContract_etherUnits, balanceTokens_UniswapContract_etherUnits)
                # PrintAndLog_FuncNameHeader("price_uniswap = " + str(price_uniswap))

                etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_kyber, price_uniswap)
                profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
                # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

                returnList_Eth.append(etherToReceive_etherUnits)
                returnList_Tokens.append(tokenQuantity_etherUnits)

            returnList_Rate.append(rate_kyberReserve)

        # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog_FuncNameHeader("returnList_Rate = " + str(returnList_Rate))
        # PrintAndLog_FuncNameHeader("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))

        # Set the resultDict for callers who are using a thread and cannot access the return value
        resultDict['returnList_Eth'] = returnList_Eth
        resultDict['returnList_Tokens'] = returnList_Tokens
        resultDict['returnList_Rate'] = returnList_Rate
        resultDict['balanceEther_UniswapContract_etherUnits'] = balanceEther_UniswapContract_etherUnits
        return returnList_Eth, returnList_Tokens, returnList_Rate, balanceEther_UniswapContract_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnUniswapSellOnKyber(token, etherToSpendList_weiUnits, kyberReserve, resultDict):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    side_kyber = 'sell'
    side_uniswap = 'buy'
    src_kyber = token.erc20TokenContractAddress
    dest_kyber = Exchanges.kyber.EtherToken

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

    # TODO, do not do this priceForConversion based on GetOnChainPrice_FromCache here...
    # I must set tokensToSpendList_weiUnits based on the actual uniswap prices
    # Which means I must make the call to get uniswap ether/token balances BEFORE I can even make the call to get kyber reserve prices

    balanceEther_UniswapContract_etherUnits, balanceTokens_UniswapContract_etherUnits = \
        Exchanges.uniswap.API_GetExchangeBalances(token.erc20TokenContractAddress.lower())

    # PrintAndLog("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))
    # PrintAndLog("balanceTokens_UniswapContract_etherUnits = " + str(balanceTokens_UniswapContract_etherUnits))

    # We're selling on Kyber and kyber expects srcQty as input, but etherToSpendList_weiUnits is currently in ETH not tokens...
    # Convert etherToSpendList_weiUnits to tokensToSpendList_weiUnits
    etherToSpendList_etherUnits = Libraries.core.ConvertListOfWeisToEthers(etherToSpendList_weiUnits, Libraries.core.Ether_Decimals)

    returnPriceDict = Exchanges.uniswap.API_GetManyTradePrices(
        side_uniswap, [uniswapContract], [etherToSpendList_etherUnits],
        [balanceEther_UniswapContract_etherUnits], [balanceTokens_UniswapContract_etherUnits])

    # PrintAndLog("returnPriceDict = " + str(returnPriceDict))
    priceList_uniswap = returnPriceDict[uniswapContract]
    # PrintAndLog("priceList_uniswap = " + str(priceList_uniswap))
    # Now that we have all the prices, create a tokensToSpendList_weiUnits
    tokensToSpendList_weiUnits = Libraries.utils.ConvertQuoteListToBaseList_GivenPriceList(etherToSpendList_weiUnits, priceList_uniswap, token.decimals)
    # PrintAndLog_FuncNameHeader("etherToSpendList_weiUnits = " + str(etherToSpendList_weiUnits))
    # PrintAndLog_FuncNameHeader("tokensToSpendList_weiUnits = " + str(tokensToSpendList_weiUnits))

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    # payloadIdDict[PayloadIdName.Uniswap_Balance_Token] = payloadId
    # payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract, token.erc20TokenContractAddress, payloadId))
    # payloadId += 1
    #
    # payloadIdDict[PayloadIdName.Uniswap_Balance_Ether] = payloadId
    # payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract, payloadId))
    # payloadId += 1

    # Get conversion rate for the kyber reserve
    payloadIdDict[PayloadIdName.Kyber_GetConversionRate_Range] = payloadId
    for tokensToSpend_weiUnits in tokensToSpendList_weiUnits:
        payload_total.append(GetPayload_Kyber_GetConversionRate(kyberReserve, src_kyber, dest_kyber, tokensToSpend_weiUnits, blockNumber_int, payloadId))
        payloadId += 1

    # Also get the expectedRate for kyber itself
    # I have to do this because I occasionally see cases where kyber proxy's trade function does not honor the conversion rate offered up by a kyber reserve.
    # I think somehow a kyber reserve is shutting down while their conversion rate remains despite not being tradable.
    payloadIdDict[PayloadIdName.Kyber_GetExpectedRate_Range] = payloadId
    for tokensToSpend_weiUnits in tokensToSpendList_weiUnits:
        payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, tokensToSpend_weiUnits, payloadId))
        payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    # before = datetime.datetime.now()
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # PrintAndLog_FuncNameHeader("duration_s = " + str(duration_s))
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        returnList_Eth = []
        returnList_Tokens = []
        returnList_Rate = []
        # Determine results
        for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
            # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
            etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
            # PrintAndLog_FuncNameHeader("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")
            price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances(
                side_uniswap, etherToSpend_etherUnits, balanceEther_UniswapContract_etherUnits, balanceTokens_UniswapContract_etherUnits)
            # PrintAndLog_FuncNameHeader("price_uniswap = " + str(price_uniswap))

            tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_uniswap)
            # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
            # Get the rate from the KyberReserve
            rate_kyberReserve = Libraries.core.ConvertWeiToEther(
                Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Kyber_GetConversionRate_Range][index]), Libraries.core.Ether_Decimals)
            # PrintAndLog_FuncNameHeader("rate_kyberReserve = " + str(rate_kyberReserve))

            # Get the rate from the KyberProxy
            result = payloadResultDict[PayloadIdName.Kyber_GetExpectedRate_Range][index]
            result = result.replace("0x", "")
            splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
            # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
            rate_kyberProxy = Libraries.core.ConvertWeiToEther(Libraries.core.ConvertHexToInt(splitArray[0]), Libraries.core.Ether_Decimals)
            # PrintAndLog_FuncNameHeader("rate_kyberProxy = " + str(rate_kyberProxy))
            # Ensure that Kyber is honoring this reserve's rate taking the worst rate between rate_kyberReserve and rate_kyberProxy
            # In other words, if kyber proxy's rate is WORSE than the kyber reserve's rate, that means this kyber reserve's rate is not valid!
            # I've asked Kyber about this... I'm not sure why a kyber reserve's rate would not be honored...
            rate_kyberReserve = Exchanges.kyber.GetWorstRate([rate_kyberReserve, rate_kyberProxy])
            # PrintAndLog_FuncNameHeader("worst rate between KyberReserve and KyberProxy = " + str(rate_kyberReserve))

            if rate_kyberReserve == 0:
                # If rate is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberReserve, side_kyber)
                # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberReserve = " + str(rate_kyberReserve))

                etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_uniswap, price_kyber)
                profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
                # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

                returnList_Eth.append(etherToReceive_etherUnits)
                returnList_Tokens.append(tokenQuantity_etherUnits)

            returnList_Rate.append(rate_kyberReserve)

        # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog_FuncNameHeader("returnList_Rate = " + str(returnList_Rate))
        # PrintAndLog_FuncNameHeader("balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))

        # Set the resultDict for callers who are using a thread and cannot access the return value
        resultDict['returnList_Eth'] = returnList_Eth
        resultDict['returnList_Tokens'] = returnList_Tokens
        resultDict['returnList_Rate'] = returnList_Rate
        resultDict['balanceEther_UniswapContract_etherUnits'] = balanceEther_UniswapContract_etherUnits
        return returnList_Eth, returnList_Tokens, returnList_Rate, balanceEther_UniswapContract_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnKyberSellOnSet(token, etherToSpendList_weiUnits, setKey, kyberReserve):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]
    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog_FuncNameHeader("price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    side_kyber = 'buy'
    side_set = setTokenData.side
    if side_set == side_kyber:
        raise Exception("Sides should not be the same!")

    src_kyber = Exchanges.kyber.EtherToken
    dest_kyber = token.erc20TokenContractAddress

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Kyber_GetConversionRate_Range] = payloadId
    for etherToSpend_weiUnits in etherToSpendList_weiUnits:
        payload_total.append(GetPayload_Kyber_GetConversionRate(kyberReserve, src_kyber, dest_kyber, etherToSpend_weiUnits, blockNumber_int, payloadId))
        payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        returnList_Eth = []
        returnList_Tokens = []
        returnList_Rate = []
        # Determine results
        for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
            # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
            etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
            # PrintAndLog_FuncNameHeader("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")
            rate_kyber = Libraries.core.ConvertWeiToEther(
                Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Kyber_GetConversionRate_Range][index]), Libraries.core.Ether_Decimals)
            if rate_kyber == 0:
                # If rate is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyber, side_kyber)
                # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyber = " + str(rate_kyber))

                tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_kyber)
                # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
                # PrintAndLog_FuncNameHeader("price_set = " + str(price_set))

                etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_kyber, price_set)
                profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
                # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

                returnList_Eth.append(etherToReceive_etherUnits)
                returnList_Tokens.append(tokenQuantity_etherUnits)

            returnList_Rate.append(rate_kyber)

        # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog_FuncNameHeader("returnList_Rate = " + str(returnList_Rate))
        return returnList_Eth, returnList_Tokens, returnList_Rate

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnSetSellOnKyber(token, etherToSpendList_weiUnits, setKey, kyberReserve):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]
    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog_FuncNameHeader("price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    # We're selling on Kyber and kyber expects srcQty as input, but etherToSpendList_weiUnits is currently in ETH not tokens...
    # Convert etherToSpendList_weiUnits to tokensToSpendList_weiUnits
    priceForConversion = price_set
    tokensToSpendList_weiUnits = Libraries.utils.ConvertEtherListToTokensList(
        etherToSpendList_weiUnits, priceForConversion, token.decimals)
    # PrintAndLog_FuncNameHeader("priceForConversion = " + str(priceForConversion) + ", token = " + str(token.tokenName))
    # PrintAndLog_FuncNameHeader("etherToSpendList_weiUnits = " + str(etherToSpendList_weiUnits))
    # PrintAndLog_FuncNameHeader("tokensToSpendList_weiUnits = " + str(tokensToSpendList_weiUnits))

    side_kyber = 'sell'
    side_set = 'buy'
    if side_set == side_kyber:
        raise Exception("Sides should not be the same!")

    src_kyber = token.erc20TokenContractAddress
    dest_kyber = Exchanges.kyber.EtherToken

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Kyber_GetConversionRate_Range] = payloadId
    for tokensToSpend_weiUnits in tokensToSpendList_weiUnits:
        payload_total.append(GetPayload_Kyber_GetConversionRate(kyberReserve, src_kyber, dest_kyber, tokensToSpend_weiUnits, blockNumber_int, payloadId))
        payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        returnList_Eth = []
        returnList_Tokens = []
        returnList_Rate = []
        # Determine results
        for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
            # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
            etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
            # PrintAndLog_FuncNameHeader("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")

            tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_set)
            # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
            rate_kyber = Libraries.core.ConvertWeiToEther(
                Libraries.core.ConvertHexToInt(payloadResultDict[PayloadIdName.Kyber_GetConversionRate_Range][index]), Libraries.core.Ether_Decimals)
            if rate_kyber == 0:
                # If rate is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyber, side_kyber)
                # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyber = " + str(rate_kyber))

                etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_set, price_kyber)
                profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
                # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(
                #     etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

                returnList_Eth.append(etherToReceive_etherUnits)
                returnList_Tokens.append(tokenQuantity_etherUnits)

            returnList_Rate.append(rate_kyber)

        # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog_FuncNameHeader("returnList_Rate = " + str(returnList_Rate))
        return returnList_Eth, returnList_Tokens, returnList_Rate

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOn0xv3SellOnSet(etherToSpendList_weiUnits, setKey, order):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    # PrintAndLog("API_GetManyArbitrages_New_BuyOn0xv3SellOnSet blockNumber_int = " + str(blockNumber_int))

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]
    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog("API_GetManyArbitrages_New_BuyOn0xv3SellOnSet price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    side_0x = Libraries.core.InvertOrderType(order.GetOrderType())
    side_set = setTokenData.side
    if not Libraries.core.IsBuy(side_0x):
        raise Exception("I'm expecting side to be something else. Calling the wrong function, or sending wrong parameter, or bug?")
    if not Libraries.core.IsSell(side_set):
        raise Exception("I'm expecting side to be something else. Calling the wrong function, or sending wrong parameter, or bug?")

    price_0x = order.GetPrice()
    PrintAndLog("API_GetManyArbitrages_New_BuyOn0xv3SellOnSet price_0x = " + str(price_0x))

    returnList_Eth = []
    returnList_Tokens = []
    # Determine results
    for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
        # PrintAndLog("------ index = " + str(index) + " -----------------------------------------------------")
        etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
        # PrintAndLog("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")

        tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_0x)
        # PrintAndLog("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))

        etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_0x, price_set)
        profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
        # PrintAndLog("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

        returnList_Eth.append(etherToReceive_etherUnits)
        returnList_Tokens.append(tokenQuantity_etherUnits)

    # PrintAndLog("API_GetManyArbitrages_New_BuyOn0xv3SellOnSet returnList_Eth = " + str(returnList_Eth))
    # PrintAndLog("API_GetManyArbitrages_New_BuyOn0xv3SellOnSet returnList_Tokens = " + str(returnList_Tokens))
    return returnList_Eth, returnList_Tokens


def API_GetManyArbitrages_New_BuyOnSetSellOn0xv3(etherToSpendList_weiUnits, setKey, order):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]
    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOn0xv3 price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    side_0x = Libraries.core.InvertOrderType(order.GetOrderType())
    side_set = setTokenData.side
    if not Libraries.core.IsBuy(side_set):
        raise Exception("I'm expecting side to be something else. Calling the wrong function, or sending wrong parameter, or bug?")
    if not Libraries.core.IsSell(side_0x):
        raise Exception("I'm expecting side to be something else. Calling the wrong function, or sending wrong parameter, or bug?")

    price_0x = order.GetPrice()
    PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOn0xv3 price_0x = " + str(price_0x))

    returnList_Eth = []
    returnList_Tokens = []
    # Determine results
    for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
        # PrintAndLog("------ index = " + str(index) + " -----------------------------------------------------")
        etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
        # PrintAndLog("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")

        tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_0x)
        # PrintAndLog("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))

        etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_set, price_0x)
        profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
        # PrintAndLog("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

        returnList_Eth.append(etherToReceive_etherUnits)
        returnList_Tokens.append(tokenQuantity_etherUnits)

    # PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOn0xv3 returnList_Eth = " + str(returnList_Eth))
    # PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOn0xv3 returnList_Tokens = " + str(returnList_Tokens))
    return returnList_Eth, returnList_Tokens


def API_GetManyArbitrages_New_BuyOnUniswapSellOnSet(token, etherToSpendList_weiUnits, setKey):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]
    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog("API_GetManyArbitrages_New_BuyOnUniswapSellOnSet price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    side_uniswap = 'buy'
    side_set = setTokenData.side
    if side_set == side_uniswap:
        raise Exception("Sides should not be the same!")

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Uniswap_Balance_Token] = payloadId
    payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract, token.erc20TokenContractAddress, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_Balance_Ether] = payloadId
    payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract, payloadId))
    payloadId += 1

    # PrintAndLog("API_GetManyArbitrages_New_BuyOnUniswapSellOnSet: payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog("API_GetManyArbitrages_New_BuyOnUniswapSellOnSet jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
        # PrintAndLog("payloadResultDict = " + str(payloadResultDict))

        balanceEther_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance(payloadResultDict)
        balanceTokens_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance(payloadResultDict, token.decimals)

        returnList_Eth = []
        returnList_Tokens = []
        # Determine results
        for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
            # PrintAndLog("------ index = " + str(index) + " -----------------------------------------------------")
            etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
            # PrintAndLog("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")
            price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances(
                side_uniswap, etherToSpend_etherUnits, balanceEther_UniswapContract_etherUnits, balanceTokens_UniswapContract_etherUnits)
            # PrintAndLog("price_uniswap = " + str(price_uniswap))
            if price_uniswap == 0:
                # If price is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_uniswap)
                # PrintAndLog("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
                # PrintAndLog("price_set = " + str(price_set))

                etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_uniswap, price_set)
                profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
                # PrintAndLog("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

                returnList_Eth.append(etherToReceive_etherUnits)
                returnList_Tokens.append(tokenQuantity_etherUnits)

        # PrintAndLog("API_GetManyArbitrages_New_BuyOnUniswapSellOnSet returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog("API_GetManyArbitrages_New_BuyOnUniswapSellOnSet returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog("API_GetManyArbitrages_New_BuyOnUniswapSellOnSet balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))
        return returnList_Eth, returnList_Tokens, balanceEther_UniswapContract_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_New_BuyOnUniswapSellOnSet response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnSetSellOnUniswap(token, etherToSpendList_weiUnits, setKey):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[token.erc20TokenContractAddress.lower()]['exchange'])

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]
    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOnUniswap price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    side_uniswap = 'sell'
    side_set = setTokenData.side
    if side_set == side_uniswap:
        raise Exception("Sides should not be the same!")

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Uniswap_Balance_Token] = payloadId
    payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract, token.erc20TokenContractAddress, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_Balance_Ether] = payloadId
    payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract, payloadId))
    payloadId += 1

    # PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOnUniswap: payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOnUniswap jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, etherToSpendList_weiUnits)
        # PrintAndLog("payloadResultDict = " + str(payloadResultDict))

        balanceEther_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance(payloadResultDict)
        balanceTokens_UniswapContract_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance(payloadResultDict, token.decimals)

        returnList_Eth = []
        returnList_Tokens = []
        # Determine results
        for index, etherToSpend_weiUnits in enumerate(etherToSpendList_weiUnits):
            # PrintAndLog("------ index = " + str(index) + " -----------------------------------------------------")
            etherToSpend_etherUnits = Libraries.core.ConvertWeiToEther(etherToSpend_weiUnits, Libraries.core.Ether_Decimals)
            # PrintAndLog("etherToSpend_etherUnits = " + str(etherToSpend_etherUnits) + " ETH, etherToSpend_weiUnits = " + str(etherToSpend_weiUnits) + " wei")

            tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(etherToSpend_etherUnits, price_set)
            # PrintAndLog("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
            price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances(
                side_uniswap, tokenQuantity_etherUnits, balanceEther_UniswapContract_etherUnits, balanceTokens_UniswapContract_etherUnits)
            # PrintAndLog("price_uniswap = " + str(price_uniswap))
            if price_uniswap == 0:
                # If price is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(etherToSpend_etherUnits, price_set, price_uniswap)
                profitEth = etherToReceive_etherUnits - etherToSpend_etherUnits
                # PrintAndLog("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(etherToReceive_etherUnits) + ", etherToSpend_etherUnits = " + str(etherToSpend_etherUnits))

                returnList_Eth.append(etherToReceive_etherUnits)
                returnList_Tokens.append(tokenQuantity_etherUnits)

        # PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOnUniswap returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOnUniswap returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOnUniswap balanceEther_UniswapContract_etherUnits = " + str(balanceEther_UniswapContract_etherUnits))
        return returnList_Eth, returnList_Tokens, balanceEther_UniswapContract_etherUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyArbitrages_New_BuyOnSetSellOnUniswap response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnUniswapSellOnSet_TokenForToken(quoteToken, baseToken, quoteTokenQuantityToSpendList_weiUnits, setKey):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    uniswapContract_quoteToken = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[quoteToken.lower()]['exchange'])
    uniswapContract_baseToken = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[baseToken.lower()]['exchange'])

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]

    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog_FuncNameHeader("price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    side_uniswap = 'buy'
    side_set = setTokenData.side
    if side_set == side_uniswap:
        raise Exception("Sides should not be the same!")

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Uniswap_QuoteToken_Balance_Token] = payloadId
    payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract_quoteToken, quoteToken, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_QuoteToken_Balance_Ether] = payloadId
    payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract_quoteToken, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_BaseToken_Balance_Token] = payloadId
    payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract_baseToken, baseToken, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_BaseToken_Balance_Ether] = payloadId
    payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract_baseToken, payloadId))
    payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, quoteTokenQuantityToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteToken)))
        decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseToken)))

        balanceEther_UniswapContract_quoteToken_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance_QuoteToken(payloadResultDict)
        balanceTokens_UniswapContract_quoteToken_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance_QuoteToken(payloadResultDict, decimals_quoteToken)
        balanceEther_UniswapContract_baseToken_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance_BaseToken(payloadResultDict)
        balanceTokens_UniswapContract_baseToken_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance_BaseToken(payloadResultDict, decimals_baseToken)

        returnList_Eth = []
        returnList_Tokens = []
        # Determine results
        for index, quoteTokenQuantityToSpend_weiUnits in enumerate(quoteTokenQuantityToSpendList_weiUnits):
            # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
            quoteTokenQuantityToSpend_etherUnits = Libraries.core.ConvertWeiToEther(quoteTokenQuantityToSpend_weiUnits, decimals_quoteToken)
            # PrintAndLog_FuncNameHeader("quoteTokenQuantityToSpend_etherUnits = " + str(
            #     quoteTokenQuantityToSpend_etherUnits) + " QuoteTokens, quoteTokenQuantityToSpend_weiUnits = " + str(quoteTokenQuantityToSpend_weiUnits) + " wei")

            price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances_TokenForToken(
                side_uniswap, quoteTokenQuantityToSpend_etherUnits,
                balanceEther_UniswapContract_quoteToken_etherUnits, balanceTokens_UniswapContract_quoteToken_etherUnits,
                balanceEther_UniswapContract_baseToken_etherUnits, balanceTokens_UniswapContract_baseToken_etherUnits)
            # PrintAndLog_FuncNameHeader("price_uniswap = " + str(price_uniswap))
            if price_uniswap == 0:
                # If price is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                baseTokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(quoteTokenQuantityToSpend_etherUnits, price_uniswap)
                # PrintAndLog_FuncNameHeader("baseTokenQuantity_etherUnits = " + str(baseTokenQuantity_etherUnits))
                # PrintAndLog_FuncNameHeader("price_set = " + str(price_set))

                quoteTokensToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(quoteTokenQuantityToSpend_etherUnits, price_uniswap, price_set)
                profitQuoteToken = quoteTokensToReceive_etherUnits - quoteTokenQuantityToSpend_etherUnits
                # PrintAndLog_FuncNameHeader("profitQuoteToken = " + str(profitQuoteToken) + ", quoteTokensToReceive_etherUnits = " + str(quoteTokensToReceive_etherUnits) + ", quoteTokenQuantityToSpend_etherUnits = " + str(quoteTokenQuantityToSpend_etherUnits))

                returnList_Eth.append(quoteTokensToReceive_etherUnits)
                returnList_Tokens.append(baseTokenQuantity_etherUnits)

        # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
        return returnList_Eth, returnList_Tokens

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnSetSellOnUniswap_TokenForToken(quoteToken, baseToken, quoteTokenQuantityToSpendList_weiUnits, setKey):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    uniswapContract_quoteToken = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[quoteToken.lower()]['exchange'])
    uniswapContract_baseToken = Libraries.nodes.Instance_Web3.toChecksumAddress(Exchanges.uniswap.ContractDict_Exchange[baseToken.lower()]['exchange'])

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]

    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog_FuncNameHeader("price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    side_uniswap = 'sell'
    side_set = setTokenData.side
    if side_set == side_uniswap:
        raise Exception("Sides should not be the same!")

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Uniswap_QuoteToken_Balance_Token] = payloadId
    payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract_quoteToken, quoteToken, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_QuoteToken_Balance_Ether] = payloadId
    payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract_quoteToken, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_BaseToken_Balance_Token] = payloadId
    payload_total.append(GetPayload_Uniswap_TokenBalance(uniswapContract_baseToken, baseToken, payloadId))
    payloadId += 1

    payloadIdDict[PayloadIdName.Uniswap_BaseToken_Balance_Ether] = payloadId
    payload_total.append(GetPayload_Uniswap_EtherBalance(uniswapContract_baseToken, payloadId))
    payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, quoteTokenQuantityToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteToken)))
        decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseToken)))

        balanceEther_UniswapContract_quoteToken_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance_QuoteToken(payloadResultDict)
        balanceTokens_UniswapContract_quoteToken_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance_QuoteToken(payloadResultDict, decimals_quoteToken)
        balanceEther_UniswapContract_baseToken_etherUnits = GetValueFromPayloadResultDict_UniswapEtherBalance_BaseToken(payloadResultDict)
        balanceTokens_UniswapContract_baseToken_etherUnits = GetValueFromPayloadResultDict_UniswapTokenBalance_BaseToken(payloadResultDict, decimals_baseToken)

        returnList_Eth = []
        returnList_Tokens = []
        # Determine results
        for index, quoteTokenQuantityToSpend_weiUnits in enumerate(quoteTokenQuantityToSpendList_weiUnits):
            # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
            quoteTokenQuantityToSpend_etherUnits = Libraries.core.ConvertWeiToEther(quoteTokenQuantityToSpend_weiUnits, decimals_quoteToken)
            # PrintAndLog_FuncNameHeader("quoteTokenQuantityToSpend_etherUnits = " + str(quoteTokenQuantityToSpend_etherUnits) + " ETH, quoteTokenQuantityToSpend_weiUnits = " + str(quoteTokenQuantityToSpend_weiUnits) + " wei")

            baseTokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(quoteTokenQuantityToSpend_etherUnits, price_set)
            # PrintAndLog_FuncNameHeader("baseTokenQuantity_etherUnits = " + str(baseTokenQuantity_etherUnits))

            price_uniswap = Exchanges.uniswap.CalculatePriceGivenBalances_TokenForToken(
                side_uniswap, baseTokenQuantity_etherUnits,
                balanceEther_UniswapContract_quoteToken_etherUnits, balanceTokens_UniswapContract_quoteToken_etherUnits,
                balanceEther_UniswapContract_baseToken_etherUnits, balanceTokens_UniswapContract_baseToken_etherUnits)
            # PrintAndLog_FuncNameHeader("price_uniswap = " + str(price_uniswap))
            if price_uniswap == 0:
                # If price is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                quoteTokensToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(quoteTokenQuantityToSpend_etherUnits, price_set, price_uniswap)
                profitQuoteToken = quoteTokensToReceive_etherUnits - quoteTokenQuantityToSpend_etherUnits
                # PrintAndLog_FuncNameHeader("profitQuoteToken = " + str(profitQuoteToken) + ", quoteTokensToReceive_etherUnits = " + str(quoteTokensToReceive_etherUnits) + ", quoteTokenQuantityToSpend_etherUnits = " + str(quoteTokenQuantityToSpend_etherUnits))

                returnList_Eth.append(quoteTokensToReceive_etherUnits)
                returnList_Tokens.append(baseTokenQuantity_etherUnits)

        # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
        return returnList_Eth, returnList_Tokens

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnKyberSellOnSet_TokenForToken(quoteToken, baseToken, quoteTokenQuantityToSpendList_weiUnits, setKey):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]

    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog_FuncNameHeader("price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    side_kyber = 'buy'
    side_set = setTokenData.side
    if side_set == side_kyber:
        raise Exception("Sides should not be the same!")

    src_kyber = quoteToken
    dest_kyber = baseToken

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Kyber_GetExpectedRate_Range] = payloadId
    for quoteTokenQuantityToSpend_weiUnits in quoteTokenQuantityToSpendList_weiUnits:
        payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, quoteTokenQuantityToSpend_weiUnits, payloadId))
        payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, quoteTokenQuantityToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteToken)))
        decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseToken)))

        returnList_Eth = []
        returnList_Tokens = []
        returnList_Rate = []
        # Determine results
        for index, quoteTokenQuantityToSpend_weiUnits in enumerate(quoteTokenQuantityToSpendList_weiUnits):
            # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
            quoteTokenQuantityToSpend_etherUnits = Libraries.core.ConvertWeiToEther(quoteTokenQuantityToSpend_weiUnits, decimals_quoteToken)
            # PrintAndLog_FuncNameHeader("quoteTokenQuantityToSpend_etherUnits = " + str(
            #     quoteTokenQuantityToSpend_etherUnits) + " QuoteTokens, quoteTokenQuantityToSpend_weiUnits = " + str(quoteTokenQuantityToSpend_weiUnits) + " wei")

            result = payloadResultDict[PayloadIdName.Kyber_GetExpectedRate_Range][index]
            result = result.replace("0x", "")
            splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
            # PrintAndLog_FuncNameHeader("splitArray = " + str(splitArray))
            rate_kyberProxy_weiUnits = Libraries.core.ConvertHexToInt(splitArray[0])
            # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
            rate_kyberProxy = Libraries.core.ConvertWeiToEther(rate_kyberProxy_weiUnits, Libraries.core.Ether_Decimals)
            if rate_kyberProxy == 0:
                # If rate is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberProxy, side_kyber)
                # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberProxy = " + str(rate_kyberProxy))

                tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(quoteTokenQuantityToSpend_etherUnits, price_kyber)
                # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))
                # PrintAndLog_FuncNameHeader("price_set = " + str(price_set))

                etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(quoteTokenQuantityToSpend_etherUnits, price_kyber, price_set)
                profitEth = etherToReceive_etherUnits - quoteTokenQuantityToSpend_etherUnits
                # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(
                #     etherToReceive_etherUnits) + ", quoteTokenQuantityToSpend_etherUnits = " + str(quoteTokenQuantityToSpend_etherUnits))

                returnList_Eth.append(etherToReceive_etherUnits)
                returnList_Tokens.append(tokenQuantity_etherUnits)

            returnList_Rate.append(rate_kyberProxy)

        # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog_FuncNameHeader("returnList_Rate = " + str(returnList_Rate))
        return returnList_Eth, returnList_Tokens, returnList_Rate

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyArbitrages_New_BuyOnSetSellOnKyber_TokenForToken(quoteToken, baseToken, quoteTokenQuantityToSpendList_weiUnits, setKey):
    # Use this blockNumber_int for all calls by batching them together and specifying this
    blockNumber_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    setTokenData = Exchanges.set.SetTokenDataDict[setKey]
    # Set the price_set based on a future predicted price based on the price curve
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = setTokenData.GetExpectedPriceXsecondsInTheFuture()
    price_set = setPrice_onNextMinedBlock
    PrintAndLog_FuncNameHeader("price_set = " + str(price_set) + " AFTER calculating future predicted price based on curve")

    # We're selling on Kyber and kyber expects srcQty as input, but quoteTokenQuantityToSpendList_weiUnits is currently in ETH not tokens...
    # Convert quoteTokenQuantityToSpendList_weiUnits to baseTokenQuantityToSpendList_weiUnits
    decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseToken)))
    priceForConversion = price_set
    baseTokenQuantityToSpendList_weiUnits = Libraries.utils.ConvertQuoteListToBaseList(
        quoteTokenQuantityToSpendList_weiUnits, priceForConversion, decimals_baseToken)
    # PrintAndLog_FuncNameHeader("priceForConversion = " + str(priceForConversion) + ", decimals_baseToken = " + str(decimals_baseToken))
    # PrintAndLog_FuncNameHeader("quoteTokenQuantityToSpendList_weiUnits = " + str(quoteTokenQuantityToSpendList_weiUnits))
    # PrintAndLog_FuncNameHeader("baseTokenQuantityToSpendList_weiUnits = " + str(baseTokenQuantityToSpendList_weiUnits))

    side_kyber = 'sell'
    side_set = setTokenData.side
    if side_set == side_kyber:
        raise Exception("Sides should not be the same!")

    src_kyber = baseToken
    dest_kyber = quoteToken

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[PayloadIdName.Kyber_GetExpectedRate_Range] = payloadId
    for baseTokenQuantityToSpend_weiUnits in baseTokenQuantityToSpendList_weiUnits:
        payload_total.append(GetPayload_Kyber_GetExpectedRate(src_kyber, dest_kyber, baseTokenQuantityToSpend_weiUnits, payloadId))
        payloadId += 1

    # PrintAndLog_FuncNameHeader("API_GetManyArbitrages_New_BuyOnSetSellOnKyber: payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, blockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("API_GetManyArbitrages_New_BuyOnSetSellOnKyber jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, quoteTokenQuantityToSpendList_weiUnits)
        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        decimals_quoteToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(quoteToken)))
        decimals_baseToken = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(baseToken)))

        returnList_Eth = []
        returnList_Tokens = []
        returnList_Rate = []
        # Determine results
        for index, quoteTokenQuantityToSpend_weiUnits in enumerate(quoteTokenQuantityToSpendList_weiUnits):
            # PrintAndLog_FuncNameHeader("------ index = " + str(index) + " -----------------------------------------------------")
            quoteTokenQuantityToSpend_etherUnits = Libraries.core.ConvertWeiToEther(quoteTokenQuantityToSpend_weiUnits, decimals_quoteToken)
            # PrintAndLog_FuncNameHeader("quoteTokenQuantityToSpend_etherUnits = " + str(
            #     quoteTokenQuantityToSpend_etherUnits) + " QuoteTokens, quoteTokenQuantityToSpend_weiUnits = " + str(quoteTokenQuantityToSpend_weiUnits) + " wei")

            tokenQuantity_etherUnits = Libraries.core.ConvertEtherToTokens(quoteTokenQuantityToSpend_etherUnits, price_set)
            # PrintAndLog_FuncNameHeader("tokenQuantity_etherUnits = " + str(tokenQuantity_etherUnits))

            result = payloadResultDict[PayloadIdName.Kyber_GetExpectedRate_Range][index]
            result = result.replace("0x", "")
            splitArray = Libraries.core.SplitStringIntoChunks(result, Libraries.core.LengthOfDataProperty)
            # PrintAndLog_FuncNameHeader("splitArray = " + str(splitArray))
            rate_kyberProxy_weiUnits = Libraries.core.ConvertHexToInt(splitArray[0])
            # slippageRate_wei = Libraries.core.ConvertHexToInt(splitArray[1])
            rate_kyberProxy = Libraries.core.ConvertWeiToEther(rate_kyberProxy_weiUnits, Libraries.core.Ether_Decimals)
            if rate_kyberProxy == 0:
                # If rate is zero, the return on the trade is zero
                returnList_Eth.append(0)
                returnList_Tokens.append(0)
            else:
                price_kyber = Exchanges.kyber.ConvertRateToPrice(rate_kyberProxy, side_kyber)
                # PrintAndLog_FuncNameHeader("price_kyber = " + str(price_kyber) + ", rate_kyberProxy = " + str(rate_kyberProxy))

                etherToReceive_etherUnits = Libraries.core.GetReturnAmountGivenTwoTradePrices(quoteTokenQuantityToSpend_etherUnits, price_set, price_kyber)
                profitEth = etherToReceive_etherUnits - quoteTokenQuantityToSpend_etherUnits
                # PrintAndLog_FuncNameHeader("profitEth = " + str(profitEth) + ", etherToReceive_etherUnits = " + str(
                #     etherToReceive_etherUnits) + ", quoteTokenQuantityToSpend_etherUnits = " + str(quoteTokenQuantityToSpend_etherUnits))

                returnList_Eth.append(etherToReceive_etherUnits)
                returnList_Tokens.append(tokenQuantity_etherUnits)

            returnList_Rate.append(rate_kyberProxy)

        # PrintAndLog_FuncNameHeader("returnList_Eth = " + str(returnList_Eth))
        # PrintAndLog_FuncNameHeader("returnList_Tokens = " + str(returnList_Tokens))
        # PrintAndLog_FuncNameHeader("returnList_Rate = " + str(returnList_Rate))
        return returnList_Eth, returnList_Tokens, returnList_Rate

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def GetEtherToSpendSafetiplier_SetAnd0x(setTokenData, order):
    setPrice_onNextMinedBlock, priceChangeDueOnNextMinedBlock, dontCare = \
        setTokenData.GetExpectedPriceXsecondsInTheFuture()
    return GetEtherToSpendSafetiplier_FromTwoPrices(order.GetPrice(), setPrice_onNextMinedBlock)


def GetEtherToSpendSafetiplier_FromTwoPrices(price1, price2):
    # Safetiplier is calculated based on a profit percentage
    # Often times the easiest way to generate this is from trading prices
    # PrintAndLog("price1 = " + str(price1))
    # PrintAndLog("price2 = " + str(price2))
    smallerPrice = min(price1, price2)
    largerPrice = max(price1, price2)
    profitPercentage = 1.0 - (smallerPrice / largerPrice)
    # PrintAndLog("profitPercentage = " + str(profitPercentage))
    # Assume it's slightly better because the tx will get mined into a block in the future
    profitPercentage_toUse = 0.01 + profitPercentage * 1.5
    # PrintAndLog("profitPercentage_toUse = " + str(profitPercentage_toUse))
    return MatchProfitPercentageToSafetiplier(profitPercentage_toUse)


def MatchProfitPercentageToSafetiplier(profitPercentage):
    # Safetiplier is calculated based on a profit percentage
    maxSafetiplier = 100
    for x in range(1, maxSafetiplier):
        factor = float(maxSafetiplier - x)
        # PrintAndLog("factor = " + str(factor))
        resultingProfitPercentage = 1.0 - (factor / (factor + 1))
        # PrintAndLog("resultingProfitPercentage = " + str(resultingProfitPercentage))
        if resultingProfitPercentage > profitPercentage:
            # We need this to be an int so we can send it to a smart contract
            factor = int(factor)
            # PrintAndLog_FuncNameHeader("resultingProfitPercentage has exceeded profitPercentage. factor to use = " + str(factor))
            return factor

    # if you get to the end, just return 1
    # The safetify function wasn't really meant go below 1.  I could do fractions but not in solidity, I would have to chang ethe function entirely
    PrintAndLog_FuncNameHeader("returning 1 because we've exceeded our functions capability. profitPercentage = " + str(profitPercentage))
    return 1


def GetOrCreateHistoricTrade(txHash, inputData, toAddress, gasPrice_gwei):
    global HistoricTradeDict
    global Lock_HistoricTradeDict

    if txHash.lower() not in HistoricTradeDict:
        Lock_HistoricTradeDict.acquire()
        try:
            HistoricTradeDict[txHash.lower()] = HistoricTrade_EtherScan(txHash.lower(), inputData, toAddress, gasPrice_gwei)
        finally:
            Lock_HistoricTradeDict.release()

    return HistoricTradeDict[txHash.lower()]


def ParseHistoricTrades_InternalTxs_EtherScan(
        address, txHash, quoteToken, jData_internalTx_EtherScan, jData_txInfo, tradeLogicContractList):
    # PrintAndLog_FuncNameHeader("txHash = " + str(txHash))
    # PrintAndLog_FuncNameHeader("jData_internalTx_EtherScan = " + str(jData_internalTx_EtherScan))
    # PrintAndLog_FuncNameHeader("jData_txInfo = " + str(jData_txInfo))
    #     #   {
    #     #     'blockNumber': '9733551',
    #     #     'timeStamp': '1585044233',
    #     #     'hash': '0x2a42dc505b67e093e94434c9d30cabdc162d7de877e9cf006123ca4014dd8a37',
    #     #     'from': '0x498aa5830da490c85dc4d92b410aab86edb26c17',
    #     #     'to': '0xa825cae02b310e9901b4776806ce25db520c8642',
    #     #     'value': '3503864765135826944',
    #     #     'contractAddress': '',
    #     #     'input': '',
    #     #     'type': 'call',
    #     #     'gas': '1234338',
    #     #     'gasUsed': '46931',
    #     #     'traceId': '1',
    #     #     'isError': '0',
    #     #     'errCode': ''
    #     #   },
    #     pass

    if quoteToken.lower() != Libraries.core.GetEtherContractAddress().lower():
        raise Exception("Not Yet Implemented with any ERC20 token as the quoteToken")

    inputData = jData_txInfo['input'].lower().replace("0x", "")
    toAddress = jData_txInfo['to'].lower()
    gasPrice_gwei = Libraries.core.ConvertWeiToGwei(Libraries.core.ConvertHexToInt(jData_txInfo['gasPrice']))
    if IsTxInputDataThatOfANinjaTrade(inputData, tradeLogicContractList):
        historicTrade = GetOrCreateHistoricTrade(txHash, inputData, toAddress, gasPrice_gwei)

        # Find the internal transactions we care about from within the jData_internalTx_EtherScan
        # Find internal transactions going TO and FROM my address
        # We must know whether or not the quoteToken is ether or an ERC20 token
        # The baseToken may be unknown or it may be known. We may have dust baseTokens left over after the trade, or we may not. The baseToken part will be tricky

        # PrintAndLog_FuncNameHeader("txHash = " + str(txHash))
        # PrintAndLog_FuncNameHeader("jData_internalTx_EtherScan = " + str(jData_internalTx_EtherScan))
        if jData_internalTx_EtherScan['hash'].lower() == txHash.lower():
            # address paying ether
            if jData_internalTx_EtherScan['from'].lower() == address.lower() and jData_internalTx_EtherScan['to'].lower() != address.lower():
                historicTrade.internalTx_send_quoteToken += Libraries.core.ConvertWeiToEther(jData_internalTx_EtherScan['value'], Libraries.core.Ether_Decimals)
            # address receiving ether
            elif jData_internalTx_EtherScan['to'].lower() == address.lower() and jData_internalTx_EtherScan['from'].lower() != address.lower():
                historicTrade.internalTx_receive_quoteToken += Libraries.core.ConvertWeiToEther(jData_internalTx_EtherScan['value'], Libraries.core.Ether_Decimals)


def ParseHistoricTrades_Txs_EtherScan(txHash, jData_tx_EtherScan, tradeLogicContractList):
    # PrintAndLog_FuncNameHeader("jData_tx_EtherScan = " + str(jData_tx_EtherScan))
    inputData = jData_tx_EtherScan['input'].lower().replace("0x", "")
    toAddress = jData_tx_EtherScan['to'].lower()
    gasPrice_gwei = Libraries.core.ConvertWeiToGwei(int(jData_tx_EtherScan['gasPrice']))
    if IsTxInputDataThatOfANinjaTrade(inputData, tradeLogicContractList):
        # Create a historicTrade for this tx because it was a trade attempt
        # PrintAndLog_FuncNameHeader("Creating a historicTrade for txHash because it was a function call to trade on the ninja! txHash = " + str(txHash))
        GetOrCreateHistoricTrade(txHash, inputData, toAddress, gasPrice_gwei)


def IsTxInputDataThatOfANinjaTrade(inputData, tradeLogicContractList):
    # Get methodSignatures of all trade functions in all ninjas
    functionSelectorList = Libraries.utils.GetMethodSignaturesOfAllContractFunctions(tradeLogicContractList, 'trade')
    # PrintAndLog_FuncNameHeader("functionSelectorList = " + str(functionSelectorList))

    for methodSignature in functionSelectorList:
        if inputData.startswith(methodSignature.lower()):
            return True

    return False


def UpdateHistoricTradeData():
    global HistoricTradeDict

    responseList_txInfo = Libraries.core.API_GetTransactionInfo_Batched_Safe(list(HistoricTradeDict.keys()))
    responseList_txReceipt = Libraries.core.API_GetTransactionReceipt_Batched_Safe(list(HistoricTradeDict.keys()))
    # PrintAndLog_FuncNameHeader("responseList_txInfo = " + str(responseList_txInfo))
    # PrintAndLog_FuncNameHeader("responseList_txReceipt = " + str(responseList_txReceipt))

    for txHash in HistoricTradeDict:
        historicTrade = HistoricTradeDict[txHash]
        for response in responseList_txInfo:
            # Find the matching txHash
            if response['hash'].lower() == txHash.lower():
                # Set the gasPrice
                historicTrade.gasPrice = Libraries.core.ConvertHexToInt(response['gasPrice'])

        for response in responseList_txReceipt:
            # Find the matching txHash
            if response['transactionHash'].lower() == txHash.lower():
                # Set the gasUsed
                historicTrade.gasUsed = Libraries.core.ConvertHexToInt(response['gasUsed'])
                # We need to know if the tx was successful or reverted
                historicTrade.txReceiptBlockStatus = Libraries.transactions.GetTxReceiptBlockStatus_GivenStatusFromTxReceipt(
                    Libraries.core.ConvertHexToInt(response['status']))


def RemoveTradesWithGasPriceLowerThanThreshold(threshold_gasPrice, mathSymbol):
    global HistoricTradeDict

    copy_HistoricTradeDict = copy.deepcopy(HistoricTradeDict)
    for key in HistoricTradeDict:
        historicTrade = HistoricTradeDict[key]
        # Delete all historicTrades that have less gas than our threshold_gasPrice
        if threshold_gasPrice:
            if (mathSymbol == Libraries.utils.MathSymbol.GreaterThan and historicTrade.gasPrice_gwei < threshold_gasPrice) or \
                    (mathSymbol == Libraries.utils.MathSymbol.LessThan and historicTrade.gasPrice_gwei > threshold_gasPrice):
                del copy_HistoricTradeDict[key]

    # Set HistoricTradeDict
    HistoricTradeDict = copy_HistoricTradeDict


def PrintAllHistoricTrades():
    global HistoricTradeDict

    roundNum_eth = 5
    historicTradesByNinjaAddressDict = GetHistoricTradesByNinjaAddress()

    # Keep track of trading volume
    tradingVolume_eth = 0
    # Keep track of total profit
    totalProfitEth = 0
    # Keep track of profits per ninja
    sumProfitEthDict_byNinjaAddress = {}
    # init the sumProfitEthDict keys and values
    for key in historicTradesByNinjaAddressDict:
        sumProfitEthDict_byNinjaAddress[key] = 0

    # Keep track of total hits
    totalHits = 0
    # Keep track of hits per ninja
    sumHitsDict_byNinjaAddress = {}
    # init the sumProfitEthDict keys and values
    for key in historicTradesByNinjaAddressDict:
        sumHitsDict_byNinjaAddress[key] = 0

    tradeCount = len(HistoricTradeDict)

    for txHash in HistoricTradeDict:
        historicTrade = HistoricTradeDict[txHash]

        PrintAndLog_FuncNameHeader("-------------------- trade to ninja " + str(historicTrade.toAddress) + " txHash = " + str(txHash) + " --------------------")
        if historicTrade.DidTxSucceed():
            PrintAndLog_FuncNameHeader("internalTx_send_quoteToken = " + str(historicTrade.internalTx_send_quoteToken))
            PrintAndLog_FuncNameHeader("internalTx_receive_quoteToken = " + str(historicTrade.internalTx_receive_quoteToken))
        else:
            PrintAndLog_FuncNameHeader("tx reverted, so no ether/token transfer occurred")

        if historicTrade.IsHit():
            # Keep track of total hits
            totalHits += 1
            # Keep track of hits by ninja
            sumHitsDict_byNinjaAddress[historicTrade.toAddress.lower()] += 1
            # Keep track of trading volume
            tradingVolume_eth += historicTrade.internalTx_send_quoteToken + historicTrade.internalTx_receive_quoteToken

        PrintAndLog_FuncNameHeader("gas fee paid = " + str(round(historicTrade.CalculateEthPaidForGas(), roundNum_eth)) + " ETH")
        PrintAndLog_FuncNameHeader("gasPrice = " + str(round(historicTrade.gasPrice_gwei, 1)) + " Gwei")

        profit_eth = historicTrade.CalculateProfit()
        PrintAndLog_FuncNameHeader("profit = " + str(round(profit_eth, roundNum_eth)) + " ETH")

        # Keep track of total profit
        totalProfitEth += profit_eth
        # Keep track of profit by ninja
        sumProfitEthDict_byNinjaAddress[historicTrade.toAddress.lower()] += profit_eth

        if profit_eth < -1.0:
            PrintAndLog_FuncNameHeader("profit_eth was very NEGATIVE. Is there a bug here??")

        if profit_eth > 0.3:
            PrintAndLog_FuncNameHeader("profit_eth was very LARGE. Is there a bug here??")

        PrintAndLog_FuncNameHeader(
            "--------------------------------------------------------------------------------------------------------------------------------------------")

    PrintAndLog_FuncNameHeader("num of trades = " + str(tradeCount))
    for ninjaAddress in historicTradesByNinjaAddressDict:
        historicTradesByNinjaAddress = historicTradesByNinjaAddressDict[ninjaAddress]
        PrintAndLog_FuncNameHeader("   Ninja " + str(ninjaAddress))
        PrintAndLog_FuncNameHeader("      " + str(len(historicTradesByNinjaAddress)) + " trades with profit = " + str(
            round(sumProfitEthDict_byNinjaAddress[ninjaAddress], roundNum_eth)) + " ETH")
        percentageOfTrades = len(historicTradesByNinjaAddress) / float(tradeCount)
        percentageOfProfits = sumProfitEthDict_byNinjaAddress[ninjaAddress] / float(totalProfitEth)
        hits = sumHitsDict_byNinjaAddress[ninjaAddress]
        misses = len(historicTradesByNinjaAddress) - hits
        hitsPercentage = hits / float(len(historicTradesByNinjaAddress))
        missPercentage = misses / float(len(historicTradesByNinjaAddress))
        PrintAndLog_FuncNameHeader(
            "      percentageOfTrades = " + str(round(percentageOfTrades * 100, 2)) + " %, percentageOfProfits = " + str(round(percentageOfProfits * 100, 2)) + " %")
        PrintAndLog_FuncNameHeader("      hits = " + str(hits) + ", misses = " + str(misses))
        PrintAndLog_FuncNameHeader("      hitsPercentage = " + str(round(hitsPercentage * 100, 2)) + " %, missPercentage = " + str(round(missPercentage * 100, 2)) + " %")
        PrintAndLog_FuncNameHeader("")

    PrintAndLog_FuncNameHeader("totalProfitEth = " + str(totalProfitEth) + " ETH")

    PrintAndLog_FuncNameHeader("tradingVolume_eth = " + str(tradingVolume_eth) + " ETH")
    price_usdc = Libraries.priceOracle.GetOnChainPrice_FromCache("0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48")
    tradingVolume_usd = Libraries.core.ConvertQuoteTokensToBaseTokens(tradingVolume_eth, price_usdc)
    PrintAndLog_FuncNameHeader("tradingVolume_usd = $" + str(tradingVolume_usd))

    PrintAndLog_FuncNameHeader("sumProfitEthDict_byNinjaAddress = " + str(sumProfitEthDict_byNinjaAddress))
    profit_eth_perTrade = None
    profitString = None
    if tradeCount > 0:
        profit_eth_perTrade = totalProfitEth / float(tradeCount)
        profitString = round(profit_eth_perTrade, roundNum_eth)

    PrintAndLog_FuncNameHeader("profit_eth per trade = " + str(profitString) + " ETH")


def GetHistoricTradesByNinjaAddress():
    returnDict = {}
    for key in HistoricTradeDict:
        historicTrade = HistoricTradeDict[key]
        # Populate returnDict so that the ninja address is the key and the value is a list of historicTrades
        if historicTrade.toAddress.lower() not in returnDict:
            returnDict[historicTrade.toAddress.lower()] = []

        returnDict[historicTrade.toAddress.lower()].append(historicTrade)

    return returnDict


def PreparePackedTradeCallData_GivenContractData(kwargs, contractObject, functionName, doEncode=True, doRemoveFirstParameter=False):
    # PrintAndLog_FuncNameHeader("contractObject = " + str(contractObject))
    # PrintAndLog_FuncNameHeader("contractObject.address = " + str(contractObject.address))
    # PrintAndLog_FuncNameHeader("contractObject.abi = " + str(contractObject.abi))
    # PrintAndLog_FuncNameHeader("Calling functionName = " + str(functionName) + " on contractObject.address " + str(contractObject.address) + " with kwargs " + str(kwargs))
    data_hex = contractObject.encodeABI(functionName, kwargs=kwargs)
    # PrintAndLog_FuncNameHeader("data_hex = " + str(data_hex))
    return PreparePackedTradeCallData_GivenCallData_Hex(data_hex, doEncode, doRemoveFirstParameter)


def PreparePackedTradeCallData_GivenCallData_Hex(data_hex, doEncode=True, doRemoveFirstParameter=False):
    # PrintAndLog_FuncNameHeader("data_hex = " + str(data_hex))
    if doRemoveFirstParameter:
        data_hex = Libraries.core.RemoveFirstParameterFromCallDataString(data_hex)
        # PrintAndLog_FuncNameHeader("data_hex after removing first param = " + str(data_hex))

    functionSelector, functionInputData = Libraries.core.GetFunctionSelectorFromCallData(data_hex)
    # PrintAndLog_FuncNameHeader("functionSelector = " + str(functionSelector))
    # PrintAndLog_FuncNameHeader("functionInputData = " + str(functionInputData))

    encodedList_functionSelector = Libraries.ninjaEncoding.PackBytes(functionSelector, doEncode)
    encodedList_functionInputData = Libraries.ninjaEncoding.PackBytes(functionInputData, doEncode)
    # PrintAndLog_FuncNameHeader("encodedList_functionSelector = " + str(encodedList_functionSelector))
    # PrintAndLog_FuncNameHeader("encodedList_functionInputData = " + str(encodedList_functionInputData))

    prePackedTradeCallDataArray_encoded = encodedList_functionSelector + encodedList_functionInputData
    return prePackedTradeCallDataArray_encoded


def UpdatePricesDict(pricesDict, blockNumber):
    global PricesDictDict

    if blockNumber in PricesDictDict:
        Libraries.core.API_PostOperatorNotification(
            "Called UpdatePricesDict with a blockNumber that was already in PricesDictDict. "
            "Should this be allowed to happen? I'm not yet handling re-orgs properly and "
            "I'm not yet handling updating prices efficiently for in-efficient exchanges")

    PricesDictDict[blockNumber] = pricesDict


def GetPricesDict(blockNumber):
    global PricesDictDict

    if blockNumber not in PricesDictDict:
        return None
    else:
        return PricesDictDict[blockNumber]


def RemoveOldPricesDicts():
    global PricesDictDict

    PrintAndLog_FuncNameHeader("PricesDictDict len = " + str(len(PricesDictDict)))
    # Remove all old block numbers
    # Old is defined as, for example, 10 blocks or more old
    blockNumberAgeThreshold = Libraries.defaults.BlockAge_blocks_WhenABlockIsConsideredOldForDataCleanUpPurposes
    blockNumber_threshold = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() - blockNumberAgeThreshold
    blockNumbers = list(PricesDictDict.keys()).copy()

    PrintAndLog_FuncNameHeader(str(blockNumbers) + " pricesDicts in PricesDictDict BEFORE removing old ones")
    oldestBlockNumber = None

    for blockNumber in blockNumbers:
        # Maintain oldestBlockNumber
        if not oldestBlockNumber or blockNumber < oldestBlockNumber:
            oldestBlockNumber = blockNumber

        if blockNumber < blockNumber_threshold:
            try:
                # PrintAndLog_FuncNameHeader("Reference count: PricesDictDict[blockNumber] = " + str(sys.getrefcount(PricesDictDict[blockNumber])))
                # PrintAndLog_FuncNameHeader("Reference count: PricesDictDict = " + str(sys.getrefcount(PricesDictDict)))
                # Release this dict from memory
                PricesDictDict[blockNumber].clear()
                del PricesDictDict[blockNumber]

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in RemoveOldPricesDicts when removing pricesDict from PricesDictDict for "
                                 "blockNumber " + str(blockNumber) + ", exception = " + str(traceback.format_exc()))

    blockNumbers_after = copy.deepcopy(list(PricesDictDict.keys()))
    PrintAndLog_FuncNameHeader(str(blockNumbers_after) + " pricesDicts in PricesDictDict AFTER removing old ones. "
                                                         "oldestBlockNumber = " + str(oldestBlockNumber))


def UpdateGasTokenProperties():
    global NinjaInstance_Generic
    global GasTokenQuantity_etherUnits
    global BurnGasTokenThreshold_wei

    intsList = NinjaInstance_Generic.API_GetInts()
    BurnGasTokenThreshold_wei = Libraries.core.ConvertHexToInt(intsList[NinjaInstance_Generic.GetIntPropertyNameList().index('gasPriceThresholdForSpendingGasTokens')])
    GasTokenQuantity_etherUnits = NinjaInstance_Generic.API_GetGasTokenQuantity()
    # PrintAndLog_FuncNameHeader("BurnGasTokenThreshold_wei = " + str(BurnGasTokenThreshold_wei))
    # PrintAndLog_FuncNameHeader("GasTokenQuantity_etherUnits = " + str(GasTokenQuantity_etherUnits))

# TODO REMOVEME
# def UpdatePriceOracle():
#     from Libraries.trade import IsValidPrice
#
#     global PriceOracleDict
#
#     # Wait for the pricesDict to be available with the latest block number
#     blockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
#     sleepTime_s = 0.02
#     elapsedTime_s = 0
#     timeout_s = 10
#     # PrintAndLog_FuncNameHeader("Waiting for pricesDict for blockNumber " + str(blockNumber))
#     while True:
#         pricesDict = GetPricesDict(blockNumber)
#         if pricesDict:
#             # PrintAndLog_FuncNameHeader("Found pricesDict for blockNumber " + str(blockNumber) + ", breaking from loop")
#             break
#
#         time.sleep(sleepTime_s)
#         elapsedTime_s += sleepTime_s
#         if elapsedTime_s > timeout_s:
#             PrintAndLog_FuncNameHeader("Breaking from loop and returning because we've waited too long. "
#                                        "Was there a bug getting pricesDict or did it take too long for somme reason?")
#             return
#
#     # We should now have the pricesDict
#
#     # Iterate over tokensList and find the average prices for each token WRT ETH
#     # TODO, instead of getting average, when I have lot of exchange integrations I can remove outliers and take average of non-outlier prices
#     tokensList = Exchanges.keeperDAO.GetBorrowableQuoteTokensList(True, True)
#     tokensList = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(tokensList)
#     # PrintAndLog_FuncNameHeader("tokensList = " + str(tokensList))
#     # Store result in dict keyed by token
#     resultsDict = {}
#     for token in tokensList:
#         # Grab the first value for EthTokenContract and use that to determine prices for the other tokens WRT ETH
#         firstEthAmount = Exchanges.keeperDAO.GetManuallyAddedTradeAmountArray(Exchanges.keeperDAO.EthTokenContract.lower())[
#             len(Exchanges.keeperDAO.GetManuallyAddedTradeAmountArray(Exchanges.keeperDAO.EthTokenContract.lower())) - 1]
#
#         sum_price_ask = 0
#         sum_price_bid = 0
#         count_price_ask = 0
#         count_price_bid = 0
#
#         # PriceOracleDict currently only works with ETH as the quoteToken
#         quoteToken = Exchanges.keeperDAO.EthTokenContract.lower()
#         for exchangeName in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges]:
#             baseToken = token
#
#             if not Libraries.exchanges.ExchangeDict[exchangeName].IsAvailableToTrade() or \
#                     not Libraries.exchanges.ExchangeDict[exchangeName].IncomingTradesAffectThePriceOfTheExchange():
#                 # PrintAndLog_FuncNameHeader("Skipping exchange " + str(exchangeName) + " because we feel it's not qualified as an oracle")
#                 continue
#
#             # PrintAndLog_FuncNameHeader("Iterating over " + str(exchangeName) + "'s quoteToken = " + str(quoteToken) + ", baseToken = " + str(baseToken) + ", " + str(
#             #     len(pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens])) + " baseTokens on block number = " + str(blockNumber))
#             # for baseToken in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens]:
#             if baseToken in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens]:
#                 # Let's just assume that if the first index is valid then we got valid data
#                 price_ask_isValid = False
#                 price_bid_isValid = False
#                 forLoopLength = int(len(pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices]))
#                 for index in range(0, forLoopLength):
#                     index_string = str(index)
#                     if index_string in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices]:
#                         quoteTokensToSpend = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][
#                             pdk_quoteTokensToSpend_etherUnits]
#
#                         if quoteTokensToSpend == firstEthAmount:
#                             # PrintAndLog_FuncNameHeader("matched quoteTokensToSpend with firstEthAmount. quoteTokensToSpend = " + str(
#                             #     quoteTokensToSpend) + ", firstEthAmount = " + str(firstEthAmount))
#                             price_ask = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][pdk_ask]
#                             price_bid = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][pdk_bid]
#                             # PrintAndLog_FuncNameHeader("price_ask = " + str(price_ask))
#                             # PrintAndLog_FuncNameHeader("price_bid = " + str(price_bid))
#
#                             # Verify that they are valid
#                             if IsValidPrice(price_ask):
#                                 # PrintAndLog_FuncNameHeader("price_ask is valid")
#                                 count_price_ask += 1
#                                 sum_price_ask += price_ask
#
#                             if IsValidPrice(price_bid):
#                                 # PrintAndLog_FuncNameHeader("price_bid is valid")
#                                 count_price_bid += 1
#                                 sum_price_bid += price_bid
#
#         if count_price_ask == 0:
#             averagePrice_ask = None
#         else:
#             averagePrice_ask = sum_price_ask / count_price_ask
#
#         if count_price_bid == 0:
#             averagePrice_bid = None
#         else:
#             averagePrice_bid = sum_price_bid / count_price_bid
#
#         # PrintAndLog_FuncNameHeader("averagePrice_ask = " + str(averagePrice_ask) + " for token " + str(token))
#         # PrintAndLog_FuncNameHeader("averagePrice_bid = " + str(averagePrice_bid) + " for token " + str(token))
#         resultsDict[token] = averagePrice_bid, averagePrice_ask
#
#     PrintAndLog_FuncNameHeader("resultsDict = " + str(resultsDict))
#     PriceOracleDict = resultsDict
#
#
# def GetPriceFromPriceOracleDict_QuoteToken_Eth(token):
#     global PriceOracleDict
#
#     # if the function caller passes in the ether address, the price of ether with respect to ether is always 1.0
#     if Libraries.utils.IsTokenAKnownEtherToken(token):
#         return 1.0
#
#     if token.lower() not in PriceOracleDict:
#         raise Exception("Token " + str(token) + " not in PriceOracleDict")
#
#     price_bid, price_ask = PriceOracleDict[token.lower()]
#     price = (price_bid + price_ask) / 2.0
#     return price
