import json
import traceback
from random import randint

import requests

import Libraries.core
import Libraries.nodes
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog

Contract_Exchange = Libraries.nodes.Instance_Web3.toChecksumAddress("0x8d12a197cb00d4747a1fe03395095ce2a5cc6819")


def API_GetEventLogs_EtherDelta_Trade_Infura(fromBlock_int, toBlock_int, url_RemoteNode=None):
    from Libraries.nodes import URL_RemoteNode

    if not url_RemoteNode:
        url_RemoteNode = URL_RemoteNode.infura_lowPriority

    fromBlock_hex = "0x" + "%x" % int(fromBlock_int)
    toBlock_hex = "0x" + "%x" % int(toBlock_int)
    PrintAndLog("API_GetEventLogs_EtherDelta_Trade_Infura fromBlock_int: " + str(fromBlock_int) + ", toBlock_int: " + str(toBlock_int))

    payload = {
        "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_getLogs",
        "params": [
            {
                "topics": ["0x6effdda786735d5033bfad5f53e5131abcced9e52be6c507b62d639685fbed6d"],
                "fromBlock": fromBlock_hex,
                "toBlock": toBlock_hex
            }
        ]
    }
    response = requests.post(Libraries.core.Get_RemoteEthereumNodeToUse(url_RemoteNode), data=json.dumps(payload), headers=Libraries.core.Headers,
                             timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetEventLogs_EtherDelta_Trade_Infura jData = " + str(jData))
        return jData['result']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()
