import json
import threading
from random import randint

from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader
import Libraries.core
import Libraries.signingUtils
import Libraries.exchanges
import Libraries.nodes

Contract_Factory = Libraries.nodes.Instance_Web3.toChecksumAddress("0xc0a47dFe034B400B47bDaD5FecDa2621de6c4d95")

# This gets set based on an API call to the blockchain which discovers Contracts, keyed by the contract address
ContractDict_Exchange = {}


def API_GetTokenWithId(id):
    import Contracts.contracts

    kwargs = {
        'token_id': int(id),
    }
    PrintAndLog("API_GetTokenWithId: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_UniswapFactory.encodeABI('getTokenWithId', kwargs=kwargs),
                "to": Contract_Factory
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("API_GetTokenWithId jData = " + str(jData))
        return Libraries.core.GetAddressFromDataProperty(jData['result'])

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetTokenWithId response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetAllTokensFromFactory():
    chunkSize = 50

    tokenAddressList_combined = []
    fromId = 1
    while True:
        toId = fromId + chunkSize

        tokenIdList = range(fromId, toId + 1)
        PrintAndLog_FuncNameHeader("Calling API_GetManyTokensWithIds with fromId = " + str(fromId) + ", toId = " + str(toId) + ", tokenIdList = " + str(tokenIdList))
        tokenAddressList_combined += API_GetManyTokensWithIds(tokenIdList)

        # If we've reached the end of valid exchanges, we'll get a Null Address as a response
        if Libraries.core.GetNullAddress() in tokenAddressList_combined or Libraries.core.GetNullAddress().lower() in tokenAddressList_combined:
            PrintAndLog_FuncNameHeader("Found a null address in our response")
            tokenAddressList_combined = list(filter((Libraries.core.GetNullAddress()).__ne__, tokenAddressList_combined))
            tokenAddressList_combined = list(filter((Libraries.core.GetNullAddress().lower()).__ne__, tokenAddressList_combined))
            break

        fromId = toId + 1

    return tokenAddressList_combined


def API_GetExchangeDataDict():
    global ContractDict_Exchange

    tokenAddressList = API_GetAllTokensFromFactory()
    exchangeDataDict = API_GetManyExchanges(tokenAddressList)

    # Build an address list from exchangeDataDict and get the ether balance of each exchange
    exchangeAddressList = []
    for tokenAddress in exchangeDataDict:
        exchangeAddressList.append(exchangeDataDict[tokenAddress]['exchange'])

    # PrintAndLog("API_GetExchangeDataDict: exchangeAddressList = " + str(exchangeAddressList))
    balancesList = Libraries.core.API_GetEtherBalance_Batched_Safe(exchangeAddressList)
    # PrintAndLog("API_GetExchangeDataDict: balancesList = " + str(balancesList))

    # Update exchangeDataDict with the balances
    for tokenAddress in exchangeDataDict:
        for index, exchangeAddress in enumerate(exchangeAddressList):
            if exchangeDataDict[tokenAddress]['exchange'] == exchangeAddress:
                exchangeDataDict[tokenAddress]['balance_ether'] = balancesList[index]
                break

    # Update exchangeDataDict with the token symbol
    symbolDict = Libraries.core.API_GetERC20Symbol_Batched(tokenAddressList)
    # PrintAndLog("API_GetExchangeDataDict: tokenAddressList = " + str(tokenAddressList))
    # PrintAndLog("API_GetExchangeDataDict: symbolDict = " + str(symbolDict))

    for index, tokenAddress in enumerate(symbolDict):
        symbol = symbolDict[tokenAddress]
        exchangeDataDict[tokenAddress]['symbol'] = symbol
        # PrintAndLog("API_GetExchangeDataDict: Attempting set symbol " + str(symbol) + " with data " + str(exchangeDataDict[tokenAddress]))

    # Update exchangeDataDict with the decimals
    decimalsDict = Libraries.core.API_ERC20Contract_GetDecimals_Batched(tokenAddressList)
    # PrintAndLog("API_GetExchangeDataDict: tokenAddressList = " + str(tokenAddressList))
    # PrintAndLog("API_GetExchangeDataDict: decimalsDict = " + str(decimalsDict))

    for index, tokenAddress in enumerate(decimalsDict):
        decimals = decimalsDict[tokenAddress]
        exchangeDataDict[tokenAddress]['decimals'] = decimals
        # PrintAndLog("API_GetExchangeDataDict: Attempting set decimals " + str(decimals) + " with data " + str(exchangeDataDict[tokenAddress]))

    # Update the cache in memory
    ContractDict_Exchange = exchangeDataDict
    # Return the updated exchangeDataDict
    return exchangeDataDict


def API_GetManyTokensWithIds(tokenIdList):
    import Contracts.contracts

    payload_total = []
    tokenIdPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for tokenId in tokenIdList:
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenId with the Id so I can know which is which in the response
        tokenIdPayloadIdDict[payloadId] = tokenId

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_UniswapFactory.encodeABI('getTokenWithId', kwargs={'token_id': int(tokenId)}),
                    "to": Contract_Factory
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, True, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetManyTokensWithIds jData = " + str(jData))
        # return Libraries.core.GetAddressFromDataProperty(jData['result'])

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in tokenIdPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = tokenIdPayloadIdDict[id]
                    result = Libraries.core.GetAddressFromDataProperty(batchedResult['result'])
                    # PrintAndLog("found Id " + str(id) + " in the response. result = " + str(result))
                    resultList.append(result)
                    break

        if len(resultList) != len(tokenIdList):
            raise Exception("Length of lists did not match, response data is bad?")

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManyTokensWithIds response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetExchange(tokenAddress):
    import Contracts.contracts

    kwargs = {
        'token': Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress),
    }
    # PrintAndLog("API_GetExchange: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_UniswapFactory.encodeABI('getExchange', kwargs=kwargs),
                "to": Contract_Factory
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetExchange jData = " + str(jData))
        return Libraries.core.GetAddressFromDataProperty(jData['result'])

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetExchange response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyExchanges(tokenAddressList):
    import Contracts.contracts

    payload_total = []
    tokenAddressIdDict = {}
    payloadId = randint(0, 99999999999999)
    for tokenAddress in tokenAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        tokenAddressIdDict[payloadId] = tokenAddress

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_UniswapFactory.encodeABI(
                        'getExchange', kwargs={'token': Libraries.nodes.Instance_Web3.toChecksumAddress(tokenAddress)}),
                    "to": Contract_Factory
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, True, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetExchange jData = " + str(jData))
        # return Libraries.core.GetAddressFromDataProperty(jData['result'])

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        uniswapDataDict = {}
        for id in tokenAddressIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    tokenAddress = tokenAddressIdDict[id]
                    exchangeAddress = Libraries.core.GetAddressFromDataProperty(batchedResult['result'])
                    # PrintAndLog("found Id " + str(id) + " in the response. Using tokenAddress " + str(tokenAddress) + ", and exchangeAddress = " + str(exchangeAddress))
                    uniswapDataDict[tokenAddress.lower()] = {
                        "exchange": exchangeAddress.lower(),
                        "tokenAddress": tokenAddress.lower(),
                    }
                    # uniswapDataDict[tokenAddress.lower()] = exchangeAddress.lower()
                    break

        if len(tokenAddressList) != len(uniswapDataDict):
            raise Exception("Length of lists did not match, response data is bad?")

        # PrintAndLog("API_GetManyExchanges: Uniswap, uniswapDataDict = " + str(uniswapDataDict))
        return uniswapDataDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetExchange response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetEthToTokenInputPrice(erc20TokenContractAddress):
    import Contracts.contracts
    global ContractDict_Exchange

    uniswapContract = ContractDict_Exchange[erc20TokenContractAddress.lower()]['exchange']
    PrintAndLog("API_GetEthToTokenInputPrice with uniswapContract = " + str(uniswapContract))

    #  {
    #   "name": "getEthToTokenInputPrice",
    #   "outputs": [
    #     {
    #       "type": "uint256",
    #       "name": "out"
    #     }
    #   ],
    #   "inputs": [
    #     {
    #       "type": "uint256",
    #       "name": "eth_sold"
    #     }
    #   ],
    #   "constant": true,
    #   "payable": false,
    #   "type": "function",
    #   "gas": 5542
    # },

    kwargs = {
        'eth_sold': int(1000000000000000000),
    }
    PrintAndLog("API_GetEthToTokenInputPrice: kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_UniswapExchange_dai.encodeABI('getEthToTokenInputPrice', kwargs=kwargs),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(uniswapContract)
            },
        ]
    }
    PrintAndLog("API_GetEthToTokenInputPrice: payload = " + str(payload))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("API_GetEthToTokenInputPrice jData = " + str(jData))
        # return Libraries.core.GetAddressFromDataProperty(jData['result'])

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetEthToTokenInputPrice response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyTradePrices(side, uniswapContractList, sourceTokenQuantityListOfLists_etherUnits, balanceEtherList_UniswapContract, balanceTokensList_UniswapContract):
    # PrintAndLog("API_GetManyTradePrices with balanceEtherList_UniswapContract = " + str(
    #     balanceEtherList_UniswapContract) + ", balanceTokensList_UniswapContract = " + str(balanceTokensList_UniswapContract))

    if len(uniswapContractList) == len(sourceTokenQuantityListOfLists_etherUnits) == len(balanceEtherList_UniswapContract) == len(balanceTokensList_UniswapContract):
        pass
    else:
        raise Exception("Length of all list parameters must be equal!")

    # Key this by the uniswapContract
    returnPriceDict = {}

    for index, sourceTokenQuantityList_etherUnits in enumerate(sourceTokenQuantityListOfLists_etherUnits):
        # PrintAndLog("API_GetManyTradePrices index = " + str(index) + " ------------------------------------------------- ")
        uniswapContract = uniswapContractList[index]
        balanceEther_UniswapContract = balanceEtherList_UniswapContract[index]
        balanceTokens_UniswapContract = balanceTokensList_UniswapContract[index]
        # PrintAndLog("API_GetManyTradePrices uniswapContract = " + str(uniswapContract))
        # PrintAndLog("API_GetManyTradePrices balanceEther_UniswapContract = " + str(balanceEther_UniswapContract))
        # PrintAndLog("API_GetManyTradePrices balanceTokens_UniswapContract = " + str(balanceTokens_UniswapContract))

        returnPriceList = []
        for sourceTokenQuantity_etherUnits in sourceTokenQuantityList_etherUnits:
            # PrintAndLog("API_GetManyTradePrices using sourceTokenQuantity_etherUnits = " + str(sourceTokenQuantity_etherUnits))

            # Refer to https://docs.uniswap.io/frontend-integration/swap#eth-erc20-calculations

            # // Sell ETH for ERC20
            # const inputAmount = userInputEthValue
            # const inputReserve = web3.eth.getBalance(exchangeAddress)
            # const outputReserve = tokenContract.methods.balanceOf(exchangeAddress)
            #
            # // Sell ERC20 for ETH
            # const inputAmount = userInputTokenValue
            # const inputReserve = tokenContract.methods.balanceOf(exchangeAddress)
            # const outputReserve = web3.eth.getBalance(exchangeAddress)
            #
            # // Output amount bought
            # const numerator = inputAmount * outputReserve * 997
            # const denominator = inputReserve * 1000 + inputAmount * 997
            # const outputAmount = numerator / denominator

            # inputAmount = None
            # inputReserve = None
            # outputReserve = None
            # if Libraries.core.IsBuy(side):
            #     inputAmount = sourceTokenQuantity_etherUnits
            #     inputReserve = balanceEther_UniswapContract
            #     outputReserve = balanceTokens_UniswapContract
            # elif Libraries.core.IsSell(side):
            #     inputAmount = sourceTokenQuantity_etherUnits
            #     inputReserve = balanceTokens_UniswapContract
            #     outputReserve = balanceEther_UniswapContract
            #
            # else:
            #     raise Exception("not buy or sell")
            #
            # numerator = inputAmount * outputReserve * 997
            # denominator = inputReserve * 1000 + inputAmount * 997
            # outputAmount = numerator / denominator
            #
            # price = None
            # if Libraries.core.IsBuy(side):
            #     price = inputAmount / outputAmount
            # elif Libraries.core.IsSell(side):
            #     price = outputAmount / inputAmount
            # else:
            #     raise Exception("not buy or sell")

            price = CalculatePriceGivenBalances(side, sourceTokenQuantity_etherUnits, balanceEther_UniswapContract, balanceTokens_UniswapContract)

            # PrintAndLog("API_GetManyTradePrices outputAmount = " + str(outputAmount))
            # PrintAndLog("API_GetManyTradePrices price = " + str(price))
            returnPriceList.append(price)

        returnPriceDict[uniswapContract] = returnPriceList

    return returnPriceDict


def CalculatePriceGivenBalances(side, sourceTokenQuantity_etherUnits, balanceQuoteToken_UniswapContract_etherUnits,
                                balanceBaseToken_UniswapContract_etherUnits, doReturnOutputAmount=False):
    # https://uniswap.org/docs/v1/frontend-integration/trade-tokens/
    inputAmount = None
    inputReserve = None
    outputReserve = None
    if Libraries.core.IsBuy(side):
        inputAmount = sourceTokenQuantity_etherUnits
        inputReserve = balanceQuoteToken_UniswapContract_etherUnits
        outputReserve = balanceBaseToken_UniswapContract_etherUnits
    elif Libraries.core.IsSell(side):
        inputAmount = sourceTokenQuantity_etherUnits
        inputReserve = balanceBaseToken_UniswapContract_etherUnits
        outputReserve = balanceQuoteToken_UniswapContract_etherUnits
    else:
        raise Exception("not buy or sell")

    numerator = inputAmount * outputReserve * 997
    denominator = inputReserve * 1000 + inputAmount * 997
    outputAmount = numerator / denominator

    price = None
    if Libraries.core.IsBuy(side):
        price = inputAmount / outputAmount
    elif Libraries.core.IsSell(side):
        price = outputAmount / inputAmount
    else:
        raise Exception("not buy or sell")

    if doReturnOutputAmount:
        return price, outputAmount
    else:
        return price


def CalculatePriceGivenBalances_GivenOutputAmount(side, destinationTokenAmount_etherUnits, balanceQuoteToken_UniswapContract_etherUnits,
                                                  balanceBaseToken_UniswapContract_etherUnits, doReturnInputAmount=False):
    # https://uniswap.org/docs/v1/frontend-integration/trade-tokens/
    outputAmount = None
    inputReserve = None
    outputReserve = None
    if Libraries.core.IsBuy(side):
        outputAmount = destinationTokenAmount_etherUnits
        inputReserve = balanceQuoteToken_UniswapContract_etherUnits
        outputReserve = balanceBaseToken_UniswapContract_etherUnits
    elif Libraries.core.IsSell(side):
        outputAmount = destinationTokenAmount_etherUnits
        inputReserve = balanceBaseToken_UniswapContract_etherUnits
        outputReserve = balanceQuoteToken_UniswapContract_etherUnits
    else:
        raise Exception("not buy or sell")

    numerator = outputAmount * inputReserve * 1000
    denominator = (outputReserve - outputAmount) * 997
    inputAmount = numerator / (denominator + 1)

    price = None
    if Libraries.core.IsBuy(side):
        price = inputAmount / outputAmount
    elif Libraries.core.IsSell(side):
        price = outputAmount / inputAmount
    else:
        raise Exception("not buy or sell")

    if doReturnInputAmount:
        return price, inputAmount
    else:
        return price


def CalculatePriceGivenBalances_TokenForToken(side, sourceTokenQuantity_etherUnits,
                                              balanceEther_UniswapContract_quoteToken_etherUnits, balanceTokens_UniswapContract_quoteToken_etherUnits,
                                              balanceEther_UniswapContract_baseToken_etherUnits, balanceTokens_UniswapContract_baseToken_etherUnits):
    if Libraries.core.IsBuy(side):
        # Sell quote tokens for ether
        quoteTokensToSpend = sourceTokenQuantity_etherUnits
        # PrintAndLog("quoteTokensToSpend = " + str(quoteTokensToSpend))
        price_quoteTokensWithRespectToEther = CalculatePriceGivenBalances(
            Libraries.core.InvertOrderType(side), quoteTokensToSpend,
            balanceEther_UniswapContract_quoteToken_etherUnits, balanceTokens_UniswapContract_quoteToken_etherUnits)
        # PrintAndLog("price_quoteTokensWithRespectToEther = " + str(price_quoteTokensWithRespectToEther))
        # Determine how much ether we receive
        etherReceived = Libraries.core.ConvertTokensToEther(quoteTokensToSpend, price_quoteTokensWithRespectToEther)
        # PrintAndLog("etherReceived = " + str(etherReceived))

        # Buy base tokens with ether
        price_baseTokensWithRespectToEther = CalculatePriceGivenBalances(
            side, etherReceived,
            balanceEther_UniswapContract_baseToken_etherUnits, balanceTokens_UniswapContract_baseToken_etherUnits)
        # PrintAndLog("price_baseTokensWithRespectToEther = " + str(price_baseTokensWithRespectToEther))
        # Determine how much base tokens we receive
        baseTokensReceived = Libraries.core.ConvertEtherToTokens(etherReceived, price_baseTokensWithRespectToEther)
        # PrintAndLog("baseTokensReceived = " + str(baseTokensReceived))

        # Use the information we calculated to determine token to token price
        price_quoteTokensWithRespectToBaseTokens = quoteTokensToSpend / baseTokensReceived
        # PrintAndLog("price_quoteTokensWithRespectToBaseTokens = " + str(price_quoteTokensWithRespectToBaseTokens))
        return price_quoteTokensWithRespectToBaseTokens
        pass

    elif Libraries.core.IsSell(side):
        # Sell base tokens for ether
        baseTokensToSpend = sourceTokenQuantity_etherUnits
        # PrintAndLog("baseTokensToSpend = " + str(baseTokensToSpend))
        price_baseTokensWithRespectToEther = CalculatePriceGivenBalances(
            side, baseTokensToSpend,
            balanceEther_UniswapContract_baseToken_etherUnits, balanceTokens_UniswapContract_baseToken_etherUnits)
        # PrintAndLog("price_baseTokensWithRespectToEther = " + str(price_baseTokensWithRespectToEther))
        # Determine how much ether we receive
        etherReceived = Libraries.core.ConvertTokensToEther(baseTokensToSpend, price_baseTokensWithRespectToEther)
        # PrintAndLog("etherReceived = " + str(etherReceived))

        # Buy quote tokens with ether
        price_quoteTokensWithRespectToEther = CalculatePriceGivenBalances(
            Libraries.core.InvertOrderType(side), etherReceived,
            balanceEther_UniswapContract_quoteToken_etherUnits, balanceTokens_UniswapContract_quoteToken_etherUnits)
        # PrintAndLog("price_quoteTokensWithRespectToEther = " + str(price_quoteTokensWithRespectToEther))
        # Determine how much quote tokens we receive
        quoteTokensReceived = Libraries.core.ConvertEtherToTokens(etherReceived, price_quoteTokensWithRespectToEther)
        # PrintAndLog("quoteTokensReceived = " + str(quoteTokensReceived))

        # Use the information we calculated to determine token to token price
        price_quoteTokensWithRespectToBaseTokens = quoteTokensReceived / baseTokensToSpend
        # PrintAndLog("price_quoteTokensWithRespectToBaseTokens = " + str(price_quoteTokensWithRespectToBaseTokens))
        return price_quoteTokensWithRespectToBaseTokens

    else:
        raise Exception("Side must be buy or sell")


# TODO, check these over to see if I made any mistakes
def GetMethodHash_ethToTokenSwapInput():
    return Libraries.signingUtils.GetMethodSignature('ethToTokenSwapInput', 'uint256,uint256')


def GetMethodHash_ethToTokenTransferInput():
    return Libraries.signingUtils.GetMethodSignature('ethToTokenTransferInput', 'uint256,uint256,address')


def GetMethodHash_ethToTokenSwapOutput():
    return Libraries.signingUtils.GetMethodSignature('ethToTokenSwapOutput', 'uint256,uint256')


def GetMethodHash_ethToTokenTransferOutput():
    return Libraries.signingUtils.GetMethodSignature('ethToTokenTransferOutput', 'uint256,uint256,address')


def GetMethodHash_tokenToEthSwapInput():
    return Libraries.signingUtils.GetMethodSignature('tokenToEthSwapInput', 'uint256,uint256,uint256')


def GetMethodHash_tokenToEthTransferInput():
    return Libraries.signingUtils.GetMethodSignature('tokenToEthTransferInput', 'uint256,uint256,uint256,address')


def GetMethodHash_tokenToEthSwapOutput():
    return Libraries.signingUtils.GetMethodSignature('tokenToEthSwapOutput', 'uint256,uint256,uint256')


def GetMethodHash_tokenToEthTransferOutput():
    return Libraries.signingUtils.GetMethodSignature('tokenToEthTransferOutput', 'uint256,uint256,uint256,address')


def GetMethodHash_tokenToTokenSwapInput():
    return Libraries.signingUtils.GetMethodSignature('tokenToTokenSwapInput', 'uint256,uint256,uint256,uint256,address')


def GetMethodHash_tokenToTokenTransferInput():
    return Libraries.signingUtils.GetMethodSignature('tokenToTokenTransferInput', 'uint256,uint256,uint256,uint256,address,address')


def GetMethodHash_tokenToTokenTransferOutput():
    return Libraries.signingUtils.GetMethodSignature('tokenToTokenTransferOutput', 'uint256,uint256,uint256,uint256,address,address')


def GetMethodHash_tokenToExchangeSwapInput():
    return Libraries.signingUtils.GetMethodSignature('tokenToExchangeSwapInput', 'uint256,uint256,uint256,uint256,address')


def GetMethodHash_tokenToExchangeTransferInput():
    return Libraries.signingUtils.GetMethodSignature('tokenToExchangeTransferInput', 'uint256,uint256,uint256,uint256,address,address')


def GetMethodHash_tokenToExchangeSwapOutput():
    return Libraries.signingUtils.GetMethodSignature('tokenToExchangeSwapOutput', 'uint256,uint256,uint256,uint256,address')


def GetMethodHash_tokenToExchangeTransferOutput():
    return Libraries.signingUtils.GetMethodSignature('tokenToExchangeTransferOutput', 'uint256,uint256,uint256,uint256,address,address')


# Calculate the price we predict the exchange will have after this trade gets mined in
def CalculateNewExchangeBalancesAssumingTheseTradesGetMinedIn(sideList, etherQuantityList_etherUnits, tokensQuantityList_etherUnits,
                                                              exchangeBalance_ether, exchangeBalance_token):
    if not (len(sideList) == len(etherQuantityList_etherUnits) == len(tokensQuantityList_etherUnits)):
        raise Exception("All lists should be of equal length!")

    for index, side in enumerate(sideList):
        etherQuantity = etherQuantityList_etherUnits[index]
        tokensQuantity = tokensQuantityList_etherUnits[index]
        # If user is selling tokens to Uniswap
        if Libraries.core.IsSell(side):
            # Token quantity in Uniswap changes
            exchangeBalance_token += tokensQuantity
            # Ether quantity in Uniswap changes
            exchangeBalance_ether -= etherQuantity
        # If user is buying tokens from Uniswap
        elif Libraries.core.IsBuy(side):
            # Token quantity in Uniswap changes
            exchangeBalance_token -= tokensQuantity
            # Ether quantity in Uniswap changes
            exchangeBalance_ether += etherQuantity
        else:
            raise Exception("side must be buy or sell")

        if exchangeBalance_token < 0 or exchangeBalance_ether < 0:
            raise Exception("Uniswap balance can not go negative, something bad happened! exchangeBalance_ether = " + str(
                exchangeBalance_ether) + ", exchangeBalance_token = " + str(exchangeBalance_token) + ", etherQuantity = " + str(
                etherQuantity) + ", tokensQuantity = " + str(tokensQuantity))

    return exchangeBalance_ether, exchangeBalance_token


def API_GetExchangeBalances(erc20TokenContractAddress, specifiedBlockNumber_int=None):
    global ContractDict_Exchange

    uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(ContractDict_Exchange[erc20TokenContractAddress.lower()]['exchange'])

    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)

    payloadIdDict[Libraries.exchanges.PayloadIdName.Uniswap_Balance_Token] = payloadId
    payload_total.append(Libraries.exchanges.GetPayload_Uniswap_TokenBalance(uniswapContract, erc20TokenContractAddress, payloadId))
    payloadId += 1

    payloadIdDict[Libraries.exchanges.PayloadIdName.Uniswap_Balance_Ether] = payloadId
    payload_total.append(Libraries.exchanges.GetPayload_Uniswap_EtherBalance(uniswapContract, payloadId))
    payloadId += 1

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, False, False, True, False, specifiedBlockNumber_int)
    if response.ok:
        responseData = response.content
        jDataList = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jDataList = " + str(jDataList))

        # parse the results based on the ids in payloadIdDict
        payloadResultDict = Libraries.exchanges.GetPayloadResultDict_FromResultOf_GetManyArbitrages_New(payloadIdDict, jDataList, None)

        # PrintAndLog_FuncNameHeader("payloadResultDict = " + str(payloadResultDict))

        balance_ether = Libraries.exchanges.GetValueFromPayloadResultDict_UniswapEtherBalance(payloadResultDict)
        token_decimals = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(erc20TokenContractAddress)))
        balance_tokens = Libraries.exchanges.GetValueFromPayloadResultDict_UniswapTokenBalance(payloadResultDict, token_decimals)

        return balance_ether, balance_tokens

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("API_GetExchange response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetExchangeBalances_Batched(tokenAddressList, blockNumber=None):
    global ContractDict_Exchange

    uniswapContractList = []
    decimalsList = []
    for tokenAddress in tokenAddressList:
        if tokenAddress.lower() not in ContractDict_Exchange:
            uniswapContractList.append(None)
            decimalsList.append(None)
        else:
            uniswapContract = Libraries.nodes.Instance_Web3.toChecksumAddress(ContractDict_Exchange[tokenAddress.lower()]['exchange'])
            uniswapContractList.append(uniswapContract)
            decimals_token = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress)))
            decimalsList.append(decimals_token)

    threads = []
    resultsDict = {}

    key_etherBalance = "etherBalance"
    t = threading.Thread(target=Libraries.core.API_GetEtherBalance_Batched_Safe, args=(uniswapContractList, resultsDict, key_etherBalance, blockNumber))
    threads.append(t)
    t.start()

    key_tokensBalance = "tokensBalance"
    t = threading.Thread(target=Libraries.core.API_GetTokenBalance_Batched_Safe, args=(uniswapContractList, tokenAddressList, decimalsList, resultsDict, key_tokensBalance, blockNumber))
    threads.append(t)
    t.start()

    for thread in threads:
        thread.join()

    balanceList_ether = resultsDict[key_etherBalance]
    balanceList_tokens = resultsDict[key_tokensBalance]

    return balanceList_ether, balanceList_tokens


def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance():
    global ContractDict_Exchange

    # The purpose of this function is to get a list of tokens on exchange
    # that have an exchange and that have some kind of balance on them that's tradeable.
    # So not dust and greater than zero.
    etherBalanceThreshold = 1
    allTokenList = list(ContractDict_Exchange.keys())
    tokenListToReturn = []
    for tokenAddress in allTokenList:
        if float(ContractDict_Exchange[tokenAddress]['balance_ether']) > etherBalanceThreshold:
            tokenListToReturn.append(tokenAddress)

    PrintAndLog_FuncNameHeader("Uniswap: of the " + str(len(allTokenList)) + " tokens on uniswap, " + str(
        len(tokenListToReturn)) + " of them have a balance greater than " + str(etherBalanceThreshold) + " ETH")
    return tokenListToReturn
