import datetime
import json
import random
import string
from random import randint
from enum import Enum
from eth_abi import encode_single
from web3 import Web3

from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader
import Libraries.core
import Libraries.nodes
import Libraries.network
import Libraries.signingUtils

MAINNET_Contract_WETH = Libraries.nodes.Instance_Web3.toChecksumAddress("0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2")
MAINNET_ZRX_TokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress("0xe41d2489571d322189246dafa5ebde1f4699f498")

MAINNET_Contract_ExchangeProxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0xdef1c0ded9bec7f1a1670819833240f027b25eff")

OrderTupleString_RFQ_Contents = 'address,address,uint128,uint128,address,address,address,bytes32,uint64,uint256'
OrderTupleString_RFQ = '(' + OrderTupleString_RFQ_Contents + ')'
OrderTupleStringArray_RFQ = '(' + OrderTupleString_RFQ_Contents + ')[]'

SignatureTupleString_RFQ_Contents = 'uint8,uint8,bytes32,bytes32'
SignatureTupleString_RFQ = '(' + SignatureTupleString_RFQ_Contents + ')'

# Set these by calling SetNetwork
Network = None
Contract_WETH = None
Contract_Exchange = None
ZRX_TokenAddress = None

ProtocolFeeMultiplier_weiUnits = 70000

# ZrXStakingPool_KeeperDAO = 45
ZrXStakingPool_KeeperDAO = "0x000000000000000000000000000000000000000000000000000000000000002d"


class OrderObjectType(Enum):
    Class = "Class"
    JsonDict = "JsonDict"


class OrderState(Enum):
    Open = 1
    # TODO, others of interest??


OrderParameters_0xv4 = [
    'chainId',
    'makerToken',
    'takerToken',
    'makerAmount',
    'takerAmount',
    'maker',
    'taker',
    'txOrigin',
    'pool',
    'expiry',
    'salt',
    'signature',
]


def SetNetwork(networkToSet):
    global Network
    global Contract_WETH
    global Contract_Exchange
    global ZRX_TokenAddress

    global MAINNET_Contract_WETH
    global MAINNET_Contract_ExchangeProxy
    global MAINNET_ZRX_TokenAddress

    Network = networkToSet
    if Network == Libraries.network.Network.mainnet:
        Contract_WETH = MAINNET_Contract_WETH
        Contract_Exchange = MAINNET_Contract_ExchangeProxy
        ZRX_TokenAddress = MAINNET_ZRX_TokenAddress

    PrintAndLog_FuncNameHeader("Now using " + str(Network.value))
    PrintAndLog_FuncNameHeader("Contract_WETH set to: " + str(Contract_WETH))
    PrintAndLog_FuncNameHeader("Contract_Exchange set to: " + str(Contract_Exchange))
    PrintAndLog_FuncNameHeader("ZRX_TokenAddress set to: " + str(ZRX_TokenAddress))


def API_GetOrderInfo_Batched_Safe(orderList, orderObjectType=OrderObjectType.Class, resultKey=None, resultsDict=None):
    before = datetime.datetime.now()
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 60  # 100 works, 130 breaks, use less than 100 to be safe

    orderList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(orderList, chunkSize)

    concatenatedList = []
    for i in range(len(orderList_chunked)):
        concatenatedList += API_GetOrderInfo_Batched(orderList_chunked[i], orderObjectType)

    if resultKey:
        resultsDict[resultKey] = concatenatedList

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Completed in " + str(duration_s) + " seconds")
    return concatenatedList


def API_GetOrderInfo_Batched(orderList, orderObjectType=OrderObjectType.Class):
    # PrintAndLog_FuncNameHeader("orderList = " + str(orderList))
    # PrintAndLog_FuncNameHeader("orderObjectType = " + str(orderObjectType))
    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for order in orderList:
        # Increment the id each time around
        payloadId += 1

        # Pair the value with the Id so I can know which is which in the response
        payloadIdDict[payloadId] = order

        method_signature = Web3.sha3(text=f"getRfqOrderInfo({OrderTupleString_RFQ})")[0:4]

        if orderObjectType == OrderObjectType.Class:
            method_parameters = encode_single(f"({OrderTupleString_RFQ})", [order.GenerateOrderTupleForTransaction()])
        elif orderObjectType == OrderObjectType.JsonDict:
            method_parameters = encode_single(f"({OrderTupleString_RFQ})", [GenerateOrderTupleForTransaction_JSON_0xv4(order)])
        else:
            raise Exception("orderObjectType " + str(orderObjectType) + " not yet implemented")

        data = '0x' + (method_signature + method_parameters).hex()

        # PrintAndLog_FuncNameHeader("method_signature = " + str(method_signature))
        # PrintAndLog_FuncNameHeader("method_parameters = " + str(method_parameters))
        # PrintAndLog_FuncNameHeader("data = " + str(data))

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": data,
                    "to": Contract_Exchange
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds,
                                                    None, True, False, True, True)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        PrintAndLog_FuncNameHeader("Exchange_0xv4 response duration = " + str(response.elapsed.total_seconds()))
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        returnList = []
        for id in payloadIdDict:
            # PrintAndLog_FuncNameHeader("id = " + str(id))
            for batchedResult in jData:
                # PrintAndLog_FuncNameHeader("searching batchedResult to find id " + str(id) + ", batchedResult = " + str(batchedResult))
                if int(batchedResult['id']) == id:
                    order = payloadIdDict[id]
                    if 'result' in batchedResult:
                        # PrintAndLog_FuncNameHeader("batchedResult['result'] for order " + str(order) + " = " + str(batchedResult['result']))
                        orderInfoResult = batchedResult['result']
                        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(orderInfoResult),
                                                                                             Libraries.core.LengthOfDataProperty)
                        # PrintAndLog_FuncNameHeader("dataList = " + str(dataList))
                        orderState = int(dataList[1])
                        orderHash = '0x' + str(dataList[0])
                        orderTakerAmountFilled_weiUnits = Libraries.core.ConvertHexToInt(dataList[2])
                        # PrintAndLog_FuncNameHeader("id = " + str(id) + ", orderState = " + str(orderState) + ", orderTakerAmountFilled_weiUnits = " + str(
                        #     orderTakerAmountFilled_weiUnits) + ", orderHash = " + str(orderHash))
                        returnList.append((orderState, orderHash, orderTakerAmountFilled_weiUnits))
                        break

        # PrintAndLog_FuncNameHeader("returnList of len " + str(len(returnList)) + " = " + str(returnList))
        # PrintAndLog_FuncNameHeader("orderList of len " + str(len(orderList)) + " = " + str(orderList))
        if len(returnList) != len(orderList):
            PrintAndLog_FuncNameHeader("Length of returnList did not match length of orderList.")

        return returnList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_IsValidHashSignature(order):
    #     function isValidHashSignature(
    #         bytes32 hash,
    #         address signerAddress,
    #         bytes memory signature
    #     )
    #         public
    #         view
    #         returns (bool isValid)
    #     {

    PrintAndLog_FuncNameHeader("order.hash = " + str(order.hash))
    PrintAndLog_FuncNameHeader("order.maker = " + str(order.maker))
    PrintAndLog_FuncNameHeader("order.v, r, s = " + str(order.v) + ", " + str(order.r) + ", " + str(order.s))
    data_hex = Libraries.signingUtils.EncodeABI(
        'isValidHashSignature',
        'bytes32,address,' + SignatureTupleString_RFQ,
        [
            Web3.toBytes(hexstr=str(order.hash)),
            Libraries.nodes.Instance_Web3.toChecksumAddress(order.maker),
            order.GenerateSignatureTupleForTransaction(),
        ])
    PrintAndLog_FuncNameHeader("data_hex = " + str(data_hex))

    # data_hex = Libraries.core.RemoveFirstXCharsInString(data_hex, 10)
    # data_hex = '0x8171c407' + data_hex
    # PrintAndLog_FuncNameHeader("data_hex = " + str(data_hex))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": data_hex,
                "to": Contract_Exchange
            },
        ]
    }
    PrintAndLog_FuncNameHeader("payload = " + str(payload))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


#   {
#     "maker": "0xca77dc47eec9e1c46c9f541ba0f222e741d6236b",
#     "taker": "0x0000000000000000000000000000000000000000",
#     "makerAmount": "250000000",
#     "takerAmount": "469905000000000000",
#     "makerToken": "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48",
#     "takerToken": "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2",
#     "salt": "1610058646000",
#     "expiry": "1610059646",
#     "chainId": 1,
#     "txOrigin": "0xBd49A97300E10325c78D6b4EC864Af31623Bb5dD",
#     "pool": "0x0000000000000000000000000000000000000000000000000000000000000017",
#     "verifyingContract": "0xDef1C0ded9bec7F1a1670819833240f027b25EfF",
#     "signature": {
#       "signatureType": 3,
#       "v": 28,
#       "r": "0xd82639dc69faa16ed02c6af334fa6180f2fb8e8e338a2cbdb51f1f031562fd35",
#       "s": "0x7a1a340150671ce1820b48b47d8fd6a62319cc33771eb577d5146321a8da15f2"
#     }
#   }

def GenerateOrderTupleForTransaction_JSON_0xv4(order):
    orderTuple = (
        Libraries.nodes.Instance_Web3.toChecksumAddress(order['makerToken']),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order['takerToken']),
        int(order['makerAmount']),
        int(order['takerAmount']),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order['maker']),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order['taker']),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order['txOrigin']),
        Web3.toBytes(hexstr=str(str(order['pool']))),
        int(order['expiry']),
        int(order['salt']))

    PrintAndLog_FuncNameHeader("orderTuple = " + str(orderTuple))
    return orderTuple


def GenerateOrderTupleForTransaction(order):
    orderTuple = (
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.makerTokenAddress),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.takerTokenAddress),
        int(order.makerTokenAmount),
        int(order.takerTokenAmount),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.maker),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.taker),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.txOrigin),
        Web3.toBytes(hexstr=str(order.pool)),
        int(order.expirationUnixTimestampSec),
        int(order.salt))

    return orderTuple


def GenerateSignatureTupleForTransaction(order):
    #       "signatureType": 3,
    #       "v": 28,
    #       "r": "0xd82639dc69faa16ed02c6af334fa6180f2fb8e8e338a2cbdb51f1f031562fd35",
    #       "s": "0x7a1a340150671ce1820b48b47d8fd6a62319cc33771eb577d5146321a8da15f2"
    orderTuple = (
        int(order.signatureType),
        int(order.v),
        Web3.toBytes(hexstr=str(order.r)),
        Web3.toBytes(hexstr=str(order.s)))

    return orderTuple


# TODO??
# def IsValidHashSignature(orderHash, signerAddress, signature):
#     import Contracts.contracts
#
#     # PrintAndLog_FuncNameHeader("IsValidHashSignature: orderHash = " + str(orderHash))
#     # PrintAndLog_FuncNameHeader("IsValidHashSignature: signerAddress = " + str(signerAddress))
#     # PrintAndLog_FuncNameHeader(]"IsValidHashSignature: signature = " + str(signature))
#     kwargs = {
#         'hash': Web3.toBytes(hexstr=str(orderHash)),
#         'signerAddress': Libraries.nodes.Instance_Web3.toChecksumAddress(signerAddress),
#         'signature': Web3.toBytes(hexstr=str(signature)),
#     }
#
#     payload = {
#         "jsonrpc": "2.0",
#         "method": "eth_call",
#         "params": [
#             {
#                 "data": Contracts.contracts.Contract_0xv3.encodeABI('isValidHashSignature', kwargs=kwargs),
#                 "to": Exchanges.zrxV2.Contract_Exchange
#             },
#         ]
#     }
#     response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
#     if response.ok:
#         responseData = response.content
#         # PrintAndLog_FuncNameHeader("Exchange_0xv2 responseData = " + str(responseData))
#         jData = json.loads(responseData)
#         # PrintAndLog_FuncNameHeader("Exchange_0xv2 jData = " + str(jData))
#         return Libraries.core.ConvertHexToBool(jData['result'])
#
#     else:
#         # If response code is not ok (200), print the resulting http error code with description
#         PrintAndLog_FuncNameHeader("Exchange_0xv2 response was not ok response = " + str(response))
#         response.raise_for_status()


def API_VerifyValidityOfOrders(orderList):
    returnList = API_GetOrderInfo_Batched_Safe(orderList)
    # PrintAndLog_FuncNameHeader("Iterate over returnList = " + str(returnList))

    returnOrderList = []
    for index, resultTuple in enumerate(returnList):
        (orderState, orderHash, orderTakerAmountFilled_weiUnits) = resultTuple
        # TODO, also verify orderTakerAmountFilled_weiUnits to make sure it means our requirements
        if orderState == OrderState.Open.value:
            # PrintAndLog_FuncNameHeader("Appending order to returnOrderList because it was found to still be fillable. orderHash = " + str(orderHash))
            returnOrderList.append(orderList[index])
        else:
            pass
            # PrintAndLog_FuncNameHeader("NOT appending order to returnOrderList because orderState = " + str(
            #     orderState) + ". orderHash = " + str(orderHash))

    # PrintAndLog_FuncNameHeader("Found that " + str(len(returnOrderList)) + " orders of the original " + str(
    #     len(orderList)) + " are still valid and fillable")
    return returnOrderList


def API_RegisterAllowedRfqOrigins(fromAddress, fromPrivateKey, txOriginAddressList, isAllowed, gasPrice, nonce=None):
    import Contracts.contracts
    from Libraries.transactions import API_SendEther_ToContract, TransactionType

    new_txOriginAddressList = []
    for address in txOriginAddressList:
        new_txOriginAddressList.append(Libraries.nodes.Instance_Web3.toChecksumAddress(address))

    # registerAllowedRfqOrigins(address[] origins, bool allowed)
    kwargs = {
        'origins': new_txOriginAddressList,
        'allowed': isAllowed,
    }
    PrintAndLog_FuncNameHeader("kwargs = " + str(kwargs))
    # Build binary representation of the function call with arguments
    # Convert this hex string to a hex byte array
    data_hex = Contracts.contracts.Contract_0xv4.encodeABI('registerAllowedRfqOrigins', kwargs=kwargs)
    toAddress = Contract_Exchange
    estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    PrintAndLog_FuncNameHeader("estimatedGas = " + str(estimatedGas))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                             estimatedGas, gasPrice, data_hex, TransactionType.other, nonce)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))
    if transactionId:
        PrintAndLog_FuncNameHeader("transactionId: " + transactionId)
    else:
        PrintAndLog_FuncNameHeader("Failed: no transactionId")

    return transactionId


def API_CancelOrder_RFQ(fromAddress, fromPrivateKey, order, gas, gasPrice):
    from Libraries.transactions import API_SendEther_ToContract, TransactionType

    PrintAndLog_FuncNameHeader("Canceling order " + str(order.PrintOrderDetails()))

    orderTuple = order.GenerateOrderTupleForTransaction()

    method_signature = Web3.sha3(text=f"cancelRfqOrder({OrderTupleString_RFQ})")[0:4]
    method_parameters = encode_single(f"({OrderTupleString_RFQ})", [orderTuple])
    data_hex = '0x' + (method_signature + method_parameters).hex()

    PrintAndLog_FuncNameHeader("method_signature = " + str(method_signature))
    PrintAndLog_FuncNameHeader("method_parameters = " + str(method_parameters))
    PrintAndLog_FuncNameHeader("data_hex = " + str(data_hex))

    # When canceling an order, make sure to send zero as the value
    transactionId = API_SendEther_ToContract(Libraries.nodes.Instance_Web3.toChecksumAddress(fromAddress), fromPrivateKey,
                                             Contract_Exchange, 0, gas, gasPrice, data_hex, TransactionType.cancelOrder, None)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "CancelOrder " + order.PrintOrderDetails() + ". transactionId: " + transactionId
        PrintAndLog_FuncNameHeader(message)
    else:
        PrintAndLog_FuncNameHeader("Failed, no transactionId")

    return transactionId


def API_BatchCancelPair_RFQ(fromAddress, fromPrivateKey, makerTokens, takerTokens, minValidSalts, gas, gasPrice):
    from Libraries.transactions import API_SendEther_ToContract, TransactionType

    PrintAndLog_FuncNameHeader("Batch canceling pair: makerTokens = " + str(makerTokens) + ", takerTokens + " +
                               str(takerTokens) + ", minValidSalts = " + str(minValidSalts))

    makerTokens_toUse = []
    for makerToken in makerTokens:
        makerTokens_toUse.append(Libraries.nodes.Instance_Web3.toChecksumAddress(makerToken))

    takerTokens_toUse = []
    for takerToken in takerTokens:
        takerTokens_toUse.append(Libraries.nodes.Instance_Web3.toChecksumAddress(takerToken))

    #     function batchCancelPairRfqOrders(
    #         address[] memory makerTokens,
    #         address[] memory takerTokens,
    #         uint256[] memory minValidSalts
    #     )
    params = 'address[],address[],uint256[]'
    PrintAndLog("params = " + str(params))
    method_signature = Web3.sha3(text=f"batchCancelPairRfqOrders({params})")[0:4]
    method_parameters = encode_single(f"({params})", [
        makerTokens_toUse,
        takerTokens_toUse,
        minValidSalts,
    ])
    data_hex = '0x' + (method_signature + method_parameters).hex()

    PrintAndLog_FuncNameHeader("method_signature = " + str(method_signature))
    PrintAndLog_FuncNameHeader("method_parameters = " + str(method_parameters))
    PrintAndLog_FuncNameHeader("data_hex = " + str(data_hex))

    # When canceling an order, make sure to send zero as the value
    transactionId = API_SendEther_ToContract(Libraries.nodes.Instance_Web3.toChecksumAddress(fromAddress), fromPrivateKey,
                                             Contract_Exchange, 0, gas, gasPrice, data_hex, TransactionType.cancelOrder, None)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "Batch canceling pair, transactionId: " + transactionId
        PrintAndLog_FuncNameHeader(message)
    else:
        PrintAndLog_FuncNameHeader("Failed, no transactionId")

    return transactionId
