import json
import threading
import traceback
from random import randint
import requests

import Libraries.core
import Libraries.cache
import Libraries.nodes
import Exchanges.keeperDAO

from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader, PrintAndLogError
from Libraries.utils import ConvertListOfStringsToLowercaseListOfStrings

ContractDict_Exchange = {}

SwapTupleString_Contents = 'address,uint256,uint256,uint256'
SwapTupleString = '(' + SwapTupleString_Contents + ')'
SwapTupleStringArray = '(' + SwapTupleString_Contents + ')[]'


class Swap:
    pool = None
    tokenInParam = None  # tokenInAmount / maxAmountIn / limitAmountIn
    tokenOutParam = None  # minAmountOut / tokenAmountOut / limitAmountOut
    maxPrice = None

    def __init__(self, _pool, _tokenInParam, _tokenOutParam, _maxPrice):
        self.pool = _pool
        self.tokenInParam = _tokenInParam
        self.tokenOutParam = _tokenOutParam
        self.maxPrice = _maxPrice

    def GenerateSwapTuple(self):
        return (
            Libraries.nodes.Instance_Web3.toChecksumAddress(self.pool),
            int(self.tokenInParam),
            int(self.tokenOutParam),
            int(self.maxPrice),
        )


def API_GetAllBalancerPools():
    from Contracts.contracts import Contract_Balancer_Factory
    from Libraries.topics import BuildTopicsArray_Balancer_NewPool

    # TODO put this top line back in when done testing
    fromBlock_int = 9560480  # This is a little before the first pool was created on Balancer
    toBlock_int = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

    resultsList = Libraries.core.API_GetLogs_Safe(Contract_Balancer_Factory.address, BuildTopicsArray_Balancer_NewPool(), fromBlock_int, toBlock_int)
    # PrintAndLog_FuncNameHeader("found " + str(len(resultsList)) + " pools in Balancer's factory")
    poolAddressList = []
    for result in resultsList:
        poolAddress = Libraries.core.GetAddressFromDataProperty(result['topics'][2])
        PrintAndLog_FuncNameHeader("Found poolAddress " + str(poolAddress))
        poolAddressList.append(poolAddress)
        # poolTokens = API_GetPoolsTokens(poolAddress)
        # PrintAndLog_FuncNameHeader("   poolTokens = " + str(poolTokens))

    PrintAndLog_FuncNameHeader("poolAddressList = " + str(poolAddressList))
    return poolAddressList


def API_GetExchangeDataDict():
    global ContractDict_Exchange

    poolAddressList = API_GetAllBalancerPools()
    PrintAndLog_FuncNameHeader("poolAddressList of len " + str(len(poolAddressList)) + " = " + str(poolAddressList))

    poolTokensDict = API_GetPoolsTokens_Batched(poolAddressList)
    # PrintAndLog_FuncNameHeader("poolTokensDict of len " + str(len(poolTokensDict)) + " = " + str(poolTokensDict))

    poolFeeDict = API_GetSwapFee_Batched_Safe(poolAddressList)
    # PrintAndLog_FuncNameHeader("poolFeeDict of len " + str(len(poolFeeDict)) + " = " + str(poolFeeDict))

    isFinalizedDict = API_IsFinalized_Batched_Safe(poolAddressList)
    # PrintAndLog_FuncNameHeader("isFinalizedDict of len " + str(len(isFinalizedDict)) + " = " + str(isFinalizedDict))
    finalizedCount = 0
    for poolAddress in isFinalizedDict:
        if isFinalizedDict[poolAddress]:
            finalizedCount += 1

    PrintAndLog_FuncNameHeader("of " + str(len(poolAddressList)) + " balancer pools, " + str(finalizedCount) + " of them are finalized and immutable")

    exchangeDataDict = {}
    for poolAddress in poolAddressList:
        if not isFinalizedDict[poolAddress]:
            PrintAndLog_FuncNameHeader("Skipping poolAddress " + str(poolAddress) + " because it's not finalized and immutable")
            continue

        tokensList = []
        if poolAddress in poolTokensDict:
            tokensList = poolTokensDict[poolAddress]

        if len(tokensList) <= 0:
            PrintAndLog_FuncNameHeader("Skipping poolAddress " + str(poolAddress) + " because tokensList was empty")
            continue

        key = poolAddress.lower()

        try:
            exchangeDataDict[key] = {}

            # PrintAndLog_FuncNameHeader("tokensList for poolAddress " + str(poolAddress) + " = " + str(tokensList))

            exchangeDataDict[key]['exchange'] = poolAddress
            exchangeDataDict[key]['tokensList'] = tokensList

            tokenDecimalsList = []
            tokenDecimalsList_forGetTokenBalanceCall = []
            for tokenAddress in tokensList:
                tokenDecimalsList.append(Libraries.core.GetDecimalsForTokenContract(tokenAddress))
                tokenDecimalsList_forGetTokenBalanceCall.append(float("1e" + str(Libraries.core.GetDecimalsForTokenContract(tokenAddress))))

            poolAddressList_forGetTokenBalanceCall = []
            for dontCare in tokensList:
                poolAddressList_forGetTokenBalanceCall.append(poolAddress)

            # PrintAndLog_FuncNameHeader("poolAddressList_forGetTokenBalanceCall of len " + str(
            #     len(poolAddressList_forGetTokenBalanceCall)) + " = " + str(poolAddressList_forGetTokenBalanceCall))
            # PrintAndLog_FuncNameHeader("tokensList of len " + str(
            #     len(tokensList)) + " = " + str(tokensList))
            # PrintAndLog_FuncNameHeader("tokenDecimalsList_forGetTokenBalanceCall of len " + str(
            #     len(tokenDecimalsList_forGetTokenBalanceCall)) + " = " + str(tokenDecimalsList_forGetTokenBalanceCall))

            tokenBalancesList = []
            atLeastOneBalanceIsNonZero = False
            if len(tokensList) > 0:
                tokenBalancesList = Libraries.core.API_GetTokenBalance_Batched_Safe(
                    poolAddressList_forGetTokenBalanceCall, tokensList, tokenDecimalsList_forGetTokenBalanceCall)
                for balance in tokenBalancesList:
                    if balance > 0:
                        atLeastOneBalanceIsNonZero = True

            if not atLeastOneBalanceIsNonZero:
                PrintAndLog_FuncNameHeader("Skipping poolAddress " + str(poolAddress) + " because all token balances were zero")
                del exchangeDataDict[key]
                continue

            tokenWeightsList = []
            if len(tokensList) > 0:
                tokenWeightsDict = API_GetNormalizedWeight_Batched(poolAddress, tokensList)
                for token in tokensList:
                    tokenWeightsList.append(tokenWeightsDict[token])

            exchangeDataDict[key]['tokenBalancesList'] = tokenBalancesList
            exchangeDataDict[key]['tokenWeightsList'] = tokenWeightsList

            tokenSymbolsList = []
            for tokenAddress in tokensList:
                tokenSymbolsList.append(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(tokenAddress))

            exchangeDataDict[key]['tokenSymbolsList'] = tokenSymbolsList
            exchangeDataDict[key]['tokenDecimalsList'] = tokenDecimalsList
            exchangeDataDict[key]['fee'] = poolFeeDict[poolAddress.lower()]

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception with key " + str(key) + ", " + traceback.format_exc())
            # Delete this key and assume something is wrong with this token
            del exchangeDataDict[key]

    PrintAndLog_FuncNameHeader("exchangeDataDict = " + str(exchangeDataDict))
    # Update the cache in memory
    ContractDict_Exchange = exchangeDataDict
    # Return the updated exchangeDataDict
    return exchangeDataDict


def API_GetPoolsTokens(poolAddress):
    from Contracts.contracts import Contract_Balancer_Pool

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contract_Balancer_Pool.encodeABI('getCurrentTokens', kwargs={}),
                "to": poolAddress
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData['result'] = " + str(jData['result']))
        return ParseResponseData_API_GetPoolsTokens(jData['result'])

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def ParseResponseData_API_GetPoolsTokens(data):
    dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)

    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)

    addressList = []
    for data in dataList:
        addressList.append(Libraries.core.GetAddressFromDataProperty(data))

    return addressList


def API_GetPoolsTokens_Batched_Safe(poolAddressList):
    # Make the function call with a large number of items,
    # chunk it into multiple calls so we don't over load the node
    chunkSize = 50
    poolAddressList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(poolAddressList, chunkSize)

    resultDict = {}
    for poolAddressList in poolAddressList_chunked:
        local_resultDict = API_GetPoolsTokens_Batched(poolAddressList)
        resultDict = Libraries.core.MergeDictionaries(resultDict, local_resultDict)

    return resultDict


def API_GetPoolsTokens_Batched(poolAddressList):
    from Contracts.contracts import Contract_Balancer_Pool

    # PrintAndLog_FuncNameHeader("poolAddressList = " + str(poolAddressList))

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for address in poolAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contract_Balancer_Pool.encodeABI('getFinalTokens', kwargs={}),
                    "to": address
                },
            ]
        }

        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers,
                                                    Libraries.core.RequestTimeout_veryLong_seconds,
                                                    None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = addressPayloadIdDict[id]
                    if 'result' in batchedResult:
                        try:
                            resultDict[id] = ParseResponseData_API_GetPoolsTokens(batchedResult['result'])
                            break
                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(poolAddressList):
            PrintAndLog_FuncNameHeader("Length of resultDict did not match length of poolAddressList.")

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetNormalizedWeight_Batched(poolAddress, tokenAddressList):
    from Contracts.contracts import Contract_Balancer_Pool

    # PrintAndLog_FuncNameHeader("poolAddress = " + str(poolAddress) + ", tokenAddressList = " + str(tokenAddressList))

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for address in tokenAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        kwargs = {
            'token': Libraries.nodes.Instance_Web3.toChecksumAddress(address),
        }

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contract_Balancer_Pool.encodeABI('getNormalizedWeight', kwargs=kwargs),
                    "to": poolAddress
                },
            ]
        }

        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers,
                                                    Libraries.core.RequestTimeout_veryLong_seconds,
                                                    None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = addressPayloadIdDict[id]
                    if 'result' in batchedResult:
                        try:
                            result_weiUnits = Libraries.core.ConvertHexToInt(batchedResult['result'])
                            result_etherUnits = Libraries.core.ConvertWeiToEther(result_weiUnits, Libraries.core.Ether_Decimals)
                            resultDict[id] = result_etherUnits
                            break
                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(tokenAddressList):
            PrintAndLog_FuncNameHeader("Length of resultDict did not match length of tokenAddressList.")

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetSwapFee_Batched_Safe(poolAddressList):
    # Make the function call with a large number of items,
    # chunk it into multiple calls so we don't over load the node
    chunkSize = 50
    poolAddressList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(poolAddressList, chunkSize)

    resultDict = {}
    for poolAddressList in poolAddressList_chunked:
        local_resultDict = API_GetSwapFee_Batched(poolAddressList)
        resultDict = Libraries.core.MergeDictionaries(resultDict, local_resultDict)

    return resultDict


def API_GetSwapFee_Batched(poolAddressList):
    from Contracts.contracts import Contract_Balancer_Pool

    # PrintAndLog_FuncNameHeader("poolAddressList = " + str(poolAddressList))

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for address in poolAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contract_Balancer_Pool.encodeABI('getSwapFee', kwargs={}),
                    "to": address
                },
            ]
        }

        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers,
                                                    Libraries.core.RequestTimeout_veryLong_seconds,
                                                    None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = addressPayloadIdDict[id]
                    if 'result' in batchedResult:
                        try:
                            result_weiUnits = Libraries.core.ConvertHexToInt(batchedResult['result'])
                            result_etherUnits = Libraries.core.ConvertWeiToEther(result_weiUnits, Libraries.core.Ether_Decimals)
                            resultDict[id] = result_etherUnits
                            break
                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(poolAddressList):
            PrintAndLog_FuncNameHeader("Length of resultDict did not match length of poolAddressList.")

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_IsFinalized_Batched_Safe(poolAddressList):
    # Make the function call with a large number of items,
    # chunk it into multiple calls so we don't over load the node
    chunkSize = 50
    poolAddressList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(poolAddressList, chunkSize)

    resultDict = {}
    for poolAddressList in poolAddressList_chunked:
        local_resultDict = API_IsFinalized_Batched(poolAddressList)
        resultDict = Libraries.core.MergeDictionaries(resultDict, local_resultDict)

    return resultDict


def API_IsFinalized_Batched(poolAddressList):
    from Contracts.contracts import Contract_Balancer_Pool

    # PrintAndLog_FuncNameHeader("poolAddressList = " + str(poolAddressList))

    payload_total = []
    addressPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for address in poolAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the tokenAddress with the Id so I can know which is which in the response
        addressPayloadIdDict[payloadId] = address

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contract_Balancer_Pool.encodeABI('isFinalized', kwargs={}),
                    "to": address
                },
            ]
        }

        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers,
                                                    Libraries.core.RequestTimeout_veryLong_seconds,
                                                    None, False, False, True, False)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultDict = {}
        for id in addressPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    id = addressPayloadIdDict[id]
                    if 'result' in batchedResult:
                        try:
                            resultDict[id] = Libraries.core.ConvertHexToBool(batchedResult['result'])
                            break
                        except:
                            PrintAndLogError("exception = " + traceback.format_exc())
                            pass

        if len(resultDict) != len(poolAddressList):
            PrintAndLog_FuncNameHeader("Length of resultDict did not match length of poolAddressList.")

        return resultDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def GetAllExchangesContainingTheseTokens(tokensList_toMatch, exchangeMustContainAllTokensInTokenList):
    global ContractDict_Exchange

    exchangesList = []
    exchangesTokens2dList = []
    for balancerExchange in ContractDict_Exchange:
        exchangesTokens = ContractDict_Exchange[balancerExchange]['tokensList']
        exchangesTokens = ConvertListOfStringsToLowercaseListOfStrings(exchangesTokens)

        matchCountRequirement = 1
        if exchangeMustContainAllTokensInTokenList:
            matchCountRequirement = len(tokensList_toMatch)

        matchCount = 0
        for tokens_toMatch in tokensList_toMatch:
            if tokens_toMatch.lower() in exchangesTokens:
                matchCount += 1

        if matchCount >= matchCountRequirement:
            exchangesList.append(balancerExchange.lower())
            exchangesTokens2dList.append(exchangesTokens)

    return exchangesList, exchangesTokens2dList


def GetTokenWeights(exchange_toMatch):
    global ContractDict_Exchange

    for balancerExchange in ContractDict_Exchange:
        if balancerExchange.lower() == exchange_toMatch.lower():
            tokenWeightsList = ContractDict_Exchange[balancerExchange]['tokenWeightsList']
            tokensList = ContractDict_Exchange[balancerExchange]['tokensList']

            tokenWeightsDict = {}
            for index, token in enumerate(tokensList):
                tokenWeightsDict[token] = tokenWeightsList[index]

            return tokenWeightsDict

    raise Exception("Could not find weights for exchange_toMatch = " + str(exchange_toMatch))


def GetFee(exchange_toMatch):
    global ContractDict_Exchange

    for balancerExchange in ContractDict_Exchange:
        if balancerExchange.lower() == exchange_toMatch.lower():
            return ContractDict_Exchange[balancerExchange]['fee']

    raise Exception("Could not find fee for exchange_toMatch = " + str(exchange_toMatch))


def GetManyTradePrices(side, balancerExchangesList, spendTokenAmountList,
                       exchangeBalanceQuoteTokensList, exchangeBalanceBaseTokensList,
                       quoteTokensList, baseTokensList):
    if len(balancerExchangesList) != len(exchangeBalanceQuoteTokensList) != len(exchangeBalanceBaseTokensList):
        raise Exception("All exchange lists must have the same length")

    if len(spendTokenAmountList) != len(quoteTokensList) != len(baseTokensList):
        raise Exception("All token lists must have the same length")

    # PrintAndLog_FuncNameHeader("balancerExchangesList of len " + str(len(balancerExchangesList)) + " = " + str(balancerExchangesList))
    # PrintAndLog_FuncNameHeader("exchangeBalanceQuoteTokensList of len " + str(len(exchangeBalanceQuoteTokensList)) + " = " + str(exchangeBalanceQuoteTokensList))
    # PrintAndLog_FuncNameHeader("exchangeBalanceBaseTokensList of len " + str(len(exchangeBalanceBaseTokensList)) + " = " + str(exchangeBalanceBaseTokensList))
    #
    # PrintAndLog_FuncNameHeader("spendTokenAmountList of len " + str(len(spendTokenAmountList)) + " = " + str(spendTokenAmountList))
    # PrintAndLog_FuncNameHeader("quoteTokensList of len " + str(len(quoteTokensList)) + " = " + str(quoteTokensList))
    # PrintAndLog_FuncNameHeader("baseTokensList of len " + str(len(baseTokensList)) + " = " + str(baseTokensList))

    exchangeBalanceSpendTokensList = None
    exchangeBalanceReceiveTokensList = None
    spendTokenList = None
    receiveTokenList = None
    if Libraries.core.IsBuy(side):
        exchangeBalanceSpendTokensList = exchangeBalanceQuoteTokensList
        exchangeBalanceReceiveTokensList = exchangeBalanceBaseTokensList
        spendTokenList = quoteTokensList
        receiveTokenList = baseTokensList
    elif Libraries.core.IsSell(side):
        exchangeBalanceSpendTokensList = exchangeBalanceBaseTokensList
        exchangeBalanceReceiveTokensList = exchangeBalanceQuoteTokensList
        spendTokenList = baseTokensList
        receiveTokenList = quoteTokensList
    else:
        raise Exception("Not buy or sell")

    # keyed by balancerExchange, the value is a pricesList
    resultDict = {}
    for index_exchange, balancerExchange in enumerate(balancerExchangesList):
        try:
            pricesList = []
            for index_token, spendTokenAmount_etherUnits in enumerate(spendTokenAmountList):
                try:
                    # # PrintAndLog_FuncNameHeader("side " + str(side) + " on balancerExchange = " + str(balancerExchange))
                    # spendToken = spendTokenList[index_token].lower()
                    # receiveToken = receiveTokenList[index_token].lower()
                    #
                    # # spendTokenAmount_etherUnits = spendTokenAmountList[index_token]
                    # spendTokenAmount_weiUnits = Libraries.core.ConvertEtherToWei(
                    #     spendTokenAmount_etherUnits, Libraries.core.GetDecimalsForTokenContract(spendToken, True))
                    # # PrintAndLog_FuncNameHeader("   spendTokenAmount_etherUnits = " + str(spendTokenAmount_etherUnits))
                    # # PrintAndLog_FuncNameHeader("   spendTokenAmount_weiUnits = " + str(spendTokenAmount_weiUnits))
                    #
                    # exchangeBalanceSpendTokens_etherUnits = exchangeBalanceSpendTokensList[index_exchange]
                    # exchangeBalanceSpendTokens_weiUnits = Libraries.core.ConvertEtherToWei(exchangeBalanceSpendTokens_etherUnits,
                    #                                                                        Libraries.core.GetDecimalsForTokenContract(spendToken, True))
                    # # PrintAndLog_FuncNameHeader("   exchangeBalanceSpendTokens_etherUnits = " + str(exchangeBalanceSpendTokens_etherUnits))
                    # # PrintAndLog_FuncNameHeader("   exchangeBalanceSpendTokens_weiUnits = " + str(exchangeBalanceSpendTokens_weiUnits))
                    #
                    # exchangeBalanceReceiveTokens_etherUnits = exchangeBalanceReceiveTokensList[index_exchange]
                    # exchangeBalanceReceiveTokens_weiUnits = Libraries.core.ConvertEtherToWei(exchangeBalanceReceiveTokens_etherUnits,
                    #                                                                          Libraries.core.GetDecimalsForTokenContract(receiveToken, True))
                    # # PrintAndLog_FuncNameHeader("   exchangeBalanceReceiveTokens_etherUnits = " + str(exchangeBalanceReceiveTokens_etherUnits))
                    # # PrintAndLog_FuncNameHeader("   exchangeBalanceReceiveTokens_weiUnits = " + str(exchangeBalanceReceiveTokens_weiUnits))
                    #
                    # weightSpendToken = GetTokenWeights(balancerExchange)[spendToken]
                    # weightReceiveToken = GetTokenWeights(balancerExchange)[receiveToken]
                    # fee = GetFee(balancerExchange)
                    #
                    # # PrintAndLog_FuncNameHeader("   spendTokenList[index_token] = " + str(spendTokenList[index_token]))
                    # # PrintAndLog_FuncNameHeader("   receiveTokenList[index_token] = " + str(receiveTokenList[index_token]))
                    # # PrintAndLog_FuncNameHeader("   weightSpendToken = " + str(weightSpendToken))
                    # # PrintAndLog_FuncNameHeader("   weightReceiveToken = " + str(weightReceiveToken))
                    # # PrintAndLog_FuncNameHeader("   fee = " + str(fee))
                    #
                    # # https://docs.balancer.finance/protocol/index#out-given-in
                    # # receiveTokenAmount = \
                    # #     exchangeBalanceReceiveTokens * \
                    # #     (1 - (exchangeBalanceSpendTokens / (exchangeBalanceSpendTokens + (spendTokenAmount * (1 - fee)))) ** (weightSpendToken / weightReceiveToken))
                    #
                    # bottomSection = exchangeBalanceSpendTokens_weiUnits + (spendTokenAmount_weiUnits * (1 - fee))
                    # middleSection = exchangeBalanceSpendTokens_weiUnits / bottomSection
                    # powerSection = weightSpendToken / weightReceiveToken
                    #
                    # # PrintAndLog_FuncNameHeader("   bottomSection = " + str(bottomSection))
                    # # PrintAndLog_FuncNameHeader("   middleSection = " + str(middleSection))
                    # # PrintAndLog_FuncNameHeader("   powerSection = " + str(powerSection))
                    #
                    # receiveTokenAmount_weiUnits = exchangeBalanceReceiveTokens_weiUnits * (1 - (middleSection ** powerSection))
                    # receiveTokenAmount_etherUnits = Libraries.core.ConvertWeiToEther(
                    #     receiveTokenAmount_weiUnits, Libraries.core.GetDecimalsForTokenContract(receiveToken, True))
                    # # PrintAndLog_FuncNameHeader("   receiveTokenAmount_weiUnits = " + str(receiveTokenAmount_weiUnits))
                    # # PrintAndLog_FuncNameHeader("   receiveTokenAmount_etherUnits = " + str(receiveTokenAmount_etherUnits))
                    #
                    # price = None
                    # if Libraries.core.IsBuy(side):
                    #     if receiveTokenAmount_etherUnits == 0:
                    #         price = None
                    #     else:
                    #         price = spendTokenAmount_etherUnits / receiveTokenAmount_etherUnits
                    # elif Libraries.core.IsSell(side):
                    #     if spendTokenAmount_etherUnits == 0:
                    #         price = None
                    #     else:
                    #         price = receiveTokenAmount_etherUnits / spendTokenAmount_etherUnits
                    # else:
                    #     raise Exception("Not buy or sell")

                    price = CalculateTradePrice(
                        side, balancerExchange, spendTokenList[index_token],
                        receiveTokenList[index_token], spendTokenAmount_etherUnits,
                        exchangeBalanceSpendTokensList[index_exchange], exchangeBalanceReceiveTokensList[index_exchange])
                    # PrintAndLog_FuncNameHeader("   price = " + str(price))
                    pricesList.append(price)

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    PrintAndLogError("exception when calculating price, " + traceback.format_exc())
                    pricesList.append(None)

            resultDict[balancerExchange] = pricesList

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when calculating price, " + traceback.format_exc())

    return resultDict


def CalculateTradePrice(side, balancerExchange, spendToken, receiveToken, spendTokenAmount_etherUnits,
                        exchangeBalanceSpendTokens_etherUnits, exchangeBalanceReceiveTokens_etherUnits):
    # doPrintDebug = False
    # if balancerExchange.lower() == '0xc0b2b0c5376cb2e6f73b473a7caa341542f707ce'.lower():
    #     PrintAndLog_FuncNameHeader("Setting doPrintDebug = True")
    #     doPrintDebug = True

    # if doPrintDebug:
    #     PrintAndLog_FuncNameHeader("side " + str(side) + " on balancerExchange = " + str(balancerExchange))
    spendToken = spendToken.lower()
    receiveToken = receiveToken.lower()

    spendTokenAmount_weiUnits = Libraries.core.ConvertEtherToWei(
        spendTokenAmount_etherUnits, Libraries.core.GetDecimalsForTokenContract(spendToken, True))
    # if doPrintDebug:
    #     PrintAndLog_FuncNameHeader("   spendTokenAmount_etherUnits = " + str(spendTokenAmount_etherUnits))
    #     PrintAndLog_FuncNameHeader("   spendTokenAmount_weiUnits = " + str(spendTokenAmount_weiUnits))

    exchangeBalanceSpendTokens_weiUnits = Libraries.core.ConvertEtherToWei(
        exchangeBalanceSpendTokens_etherUnits, Libraries.core.GetDecimalsForTokenContract(spendToken, True))
    # if doPrintDebug:
    #     PrintAndLog_FuncNameHeader("   exchangeBalanceSpendTokens_etherUnits = " + str(exchangeBalanceSpendTokens_etherUnits))
    #     PrintAndLog_FuncNameHeader("   exchangeBalanceSpendTokens_weiUnits = " + str(exchangeBalanceSpendTokens_weiUnits))

    exchangeBalanceReceiveTokens_weiUnits = Libraries.core.ConvertEtherToWei(
        exchangeBalanceReceiveTokens_etherUnits, Libraries.core.GetDecimalsForTokenContract(receiveToken, True))
    # if doPrintDebug:
    #     PrintAndLog_FuncNameHeader("   exchangeBalanceReceiveTokens_etherUnits = " + str(exchangeBalanceReceiveTokens_etherUnits))
    #     PrintAndLog_FuncNameHeader("   exchangeBalanceReceiveTokens_weiUnits = " + str(exchangeBalanceReceiveTokens_weiUnits))

    weightSpendToken = GetTokenWeights(balancerExchange)[spendToken]
    weightReceiveToken = GetTokenWeights(balancerExchange)[receiveToken]
    fee = GetFee(balancerExchange)

    # if doPrintDebug:
    #     PrintAndLog_FuncNameHeader("   spendToken = " + str(spendToken))
    #     PrintAndLog_FuncNameHeader("   receiveToken = " + str(receiveToken))
    #     PrintAndLog_FuncNameHeader("   weightSpendToken = " + str(weightSpendToken))
    #     PrintAndLog_FuncNameHeader("   weightReceiveToken = " + str(weightReceiveToken))
    #     PrintAndLog_FuncNameHeader("   fee = " + str(fee))

    # https://docs.balancer.finance/protocol/index#out-given-in
    bottomSection = exchangeBalanceSpendTokens_weiUnits + (spendTokenAmount_weiUnits * (1 - fee))
    middleSection = exchangeBalanceSpendTokens_weiUnits / bottomSection
    powerSection = weightSpendToken / weightReceiveToken

    # if doPrintDebug:
    #     PrintAndLog_FuncNameHeader("   bottomSection = " + str(bottomSection))
    #     PrintAndLog_FuncNameHeader("   middleSection = " + str(middleSection))
    #     PrintAndLog_FuncNameHeader("   powerSection = " + str(powerSection))

    receiveTokenAmount_weiUnits = exchangeBalanceReceiveTokens_weiUnits * (1 - (middleSection ** powerSection))
    receiveTokenAmount_etherUnits = Libraries.core.ConvertWeiToEther(
        receiveTokenAmount_weiUnits, Libraries.core.GetDecimalsForTokenContract(receiveToken, True))
    # if doPrintDebug:
    #     PrintAndLog_FuncNameHeader("   receiveTokenAmount_weiUnits = " + str(receiveTokenAmount_weiUnits))
    #     PrintAndLog_FuncNameHeader("   receiveTokenAmount_etherUnits = " + str(receiveTokenAmount_etherUnits))

    price = None
    if Libraries.core.IsBuy(side):
        if receiveTokenAmount_etherUnits == 0:
            price = None
        else:
            price = spendTokenAmount_etherUnits / receiveTokenAmount_etherUnits
    elif Libraries.core.IsSell(side):
        if spendTokenAmount_etherUnits == 0:
            price = None
        else:
            price = receiveTokenAmount_etherUnits / spendTokenAmount_etherUnits
    else:
        raise Exception("Not buy or sell")

    return price


def GetExchangesThatHaveTheseTokens(tokensList_toMatch):
    global ContractDict_Exchange

    exchangesList = []
    for exchange in ContractDict_Exchange:
        tokensList = ContractDict_Exchange[exchange]['tokensList']
        allTokensToMatchAreFound = True
        for token_toMatch in tokensList_toMatch:
            if token_toMatch.lower() not in tokensList:
                allTokensToMatchAreFound = False
                # We want to match all tokens in tokensList_toMatch so as soon as we don't find one, break from the loop
                break

        # If we've made it this far and allTokensToMatchAreFound is still True, add it to the list
        if allTokensToMatchAreFound:
            exchangesList.append(exchange.lower())

    return exchangesList


def GetExchangesTokenBalance(exchange, token_toMatch):
    global ContractDict_Exchange

    key = exchange.lower()
    tokenBalancesList = ContractDict_Exchange[key]['tokenBalancesList']
    tokensList = ContractDict_Exchange[key]['tokensList']

    # Find token_toMatch's balance
    indexOfToken = tokensList.index(token_toMatch.lower())
    tokenBalance = tokenBalancesList[indexOfToken]
    return tokenBalance


def GetExchangesTokenList(exchange):
    global ContractDict_Exchange

    key = exchange.lower()
    tokensList = ContractDict_Exchange[key]['tokensList']
    return tokensList


def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(quoteTokenList):
    global ContractDict_Exchange

    from Libraries.exchanges import EnforceTokenAddressIsERC20Version, ExchangeName_Balancer

    # The purpose of this function is to get a list of tokens on exchange
    # that have an exchange and that have some kind of balance on them that's tradeable.
    # So not dust and greater than zero.

    # Start by getting all the possible tokens
    allTokenList = []
    for exchange in ContractDict_Exchange:
        tokensList = ContractDict_Exchange[exchange]['tokensList']

        for token in tokensList:
            if token.lower() not in allTokenList:
                allTokenList.append(token.lower())

    # PrintAndLog_FuncNameHeader("quoteTokenList has " + str(len(quoteTokenList)) + " items. quoteTokenList = " + str(quoteTokenList))
    # PrintAndLog_FuncNameHeader("allTokenList has " + str(len(allTokenList)) + " items. allTokenList = " + str(allTokenList))

    tokenListToReturn = []
    # Iterate over all the quoteTokens in quoteTokenList
    for quoteToken in quoteTokenList:
        quoteToken_toUseForThisExchange = EnforceTokenAddressIsERC20Version(quoteToken)

        for baseToken in allTokenList:
            # Skip tokens already added to the list
            if baseToken.lower() in tokenListToReturn:
                continue

            # cannot trade the same token with itself
            if baseToken.lower() == quoteToken_toUseForThisExchange.lower():
                continue

            # Find at least one exchange whose quoteTokenBalance exceeds the quoteTokenBalanceThreshold
            # If we find that, add the baseToken to tokenListToReturn
            exchangesList = GetExchangesThatHaveTheseTokens([quoteToken_toUseForThisExchange, baseToken])
            for exchange in exchangesList:
                quoteTokenBalance = GetExchangesTokenBalance(exchange, quoteToken_toUseForThisExchange)

                # Require that the quoteTokenBalance exceeds the threshold so we know this token is worth trading
                if Exchanges.keeperDAO.DoesThisQuoteTokenHaveEnoughBalanceWorthTradingFor(quoteToken, float(quoteTokenBalance), ExchangeName_Balancer):
                    # it's worth trading so add to the list
                    tokenListToReturn.append(baseToken.lower())
                    # PrintAndLog_FuncNameHeader("baseToken " + str(
                    #     baseToken) + " has sufficient balance for quoteToken_toUseForThisExchange " + str(
                    #     quoteToken_toUseForThisExchange) + " in exchange " + str(exchange))
                    # Break because we don't need to look at any more exchanges for this baseToken
                    break
                else:
                    pass
                    # PrintAndLog_FuncNameHeader("baseToken " + str(
                    #     baseToken) + " does not have sufficient balance for quoteToken_toUseForThisExchange " + str(
                    #     quoteToken_toUseForThisExchange) + " in exchange " + str(exchange))

    PrintAndLog_FuncNameHeader("Balancer: of the " + str(len(allTokenList)) + " tokens on Balancer, " + str(
        len(tokenListToReturn)) + " of them have a balance greater than the respective quoteTokenBalanceThreshold")
    return tokenListToReturn
