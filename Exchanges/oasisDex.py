import bisect
import copy
import datetime
import json
import traceback
from threading import Lock

import Exchanges.zrx
import Exchanges.zrxV2
import Libraries.arbyUtility
import Libraries.core
import Libraries.nodes
from Libraries.loggingConfig import PrintAndLogError, PrintAndLog

# Old
from Libraries.orders import Order_CX

Contract_Exchange_Old1 = Libraries.nodes.Instance_Web3.toChecksumAddress("0x14fbca95be7e99c15cc2996c6c9d841e54b79425")
Contract_Exchange_Old2 = Libraries.nodes.Instance_Web3.toChecksumAddress("0xb7ac09c2c0217b07d7c103029b4918a2c401eecb")

# Current
Contract_Exchange = Libraries.nodes.Instance_Web3.toChecksumAddress("0x39755357759ce0d7f32dc8dc45414cca409ae24e")

Contract_WETH = Libraries.nodes.Instance_Web3.toChecksumAddress("0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2")
Contract_DAI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359")

# This is basically a cache of the results of a call to the OasisDex contract
# TODO, this is a temporary work around so I don't have to spam the network with calls. I need to find a better way to do this long term
GetPriceDetailsResultDict_DAIsellDetails = {}
GetPriceDetailsResultLock_DAIsellDetails = Lock()
GetPriceDetailsResultDict_WETHsellDetails = {}
GetPriceDetailsResultLock_WETHsellDetails = Lock()

# Interval in which to check DAI prices
# IntervalToCheckDAIprices_seconds = 35
IntervalToCheckDAIprices_seconds = 9


class GetPriceDetailsResult:
    dateTimeSet = None
    amount = None
    result1 = None
    result2 = None

    def __init__(self, _amount, _result1, _result2):
        self.amount = _amount
        self.dateTimeSet = datetime.datetime.now()
        self.result1 = _result1
        self.result2 = _result2

    def PrintDetails(self):
        return "GetPriceDetailsResult: " + str(self.amount) + ", " + str(self.result1) + ", " + str(self.result2)


def SetGetPriceDetailsResult(dictObject, lockObject, amount, result1, result2):
    global IntervalToCheckDAIprices_seconds

    lockObject.acquire()
    try:
        dictObject[amount] = GetPriceDetailsResult(amount, result1, result2)

        # Consider deleting old items
        for resultKey in copy.deepcopy(dictObject):
            # If this item in the dict is old
            if (datetime.datetime.now() - dictObject[resultKey].dateTimeSet).total_seconds() > IntervalToCheckDAIprices_seconds:
                # Delete this item from the dict because it's old
                del dictObject[resultKey]

    finally:
        lockObject.release()


def CheckForValidResultFromDict(dictObject, lockObject, amount):
    global IntervalToCheckDAIprices_seconds

    lockObject.acquire()
    try:
        if amount in dictObject:
            if (datetime.datetime.now() - dictObject[amount].dateTimeSet).total_seconds() < IntervalToCheckDAIprices_seconds:
                return dictObject[amount].result1, dictObject[amount].result2

    finally:
        lockObject.release()

    return None


def API_GetDAIsellDetails(receiveAmount_weth_etherUnits, sendTokenAddress, alwaysGetPriceFromCache=False):
    global GetPriceDetailsResultDict_DAIsellDetails
    global GetPriceDetailsResultLock_DAIsellDetails

    # Before making the call, check to see if we have a cached value that we can return.  If so, return it.  If not, get the new value
    cacheResult = CheckForValidResultFromDict(GetPriceDetailsResultDict_DAIsellDetails, GetPriceDetailsResultLock_DAIsellDetails, receiveAmount_weth_etherUnits)
    if cacheResult:
        PrintAndLog("API_GetDAIsellDetails: returning cached value: receiveAmount_weth_etherUnits = " + str(
            receiveAmount_weth_etherUnits) + ", price(cacheResult) = " + str(cacheResult))
        return cacheResult
    # return None if we're specifically told to ony get data from the cache, but we don't have any cached data.  If we did have cached data, it would have returned already above this
    elif alwaysGetPriceFromCache:
        return None
    # else, get new data and store in cache
    else:
        # Sending DAI, receiving WETH
        receiveTokenAddress = Exchanges.oasisDex.Contract_WETH
        decimals_weth = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(receiveTokenAddress)))
        payAmount_dai_etherUnits = API_GetPayAmount(sendTokenAddress, receiveTokenAddress, receiveAmount_weth_etherUnits, decimals_weth)
        price = receiveAmount_weth_etherUnits / payAmount_dai_etherUnits
        # Store the result in the cache
        SetGetPriceDetailsResult(GetPriceDetailsResultDict_DAIsellDetails, GetPriceDetailsResultLock_DAIsellDetails,
                                 receiveAmount_weth_etherUnits, price, payAmount_dai_etherUnits)
        # PrintAndLog("Set cache GetPriceDetailsResultDict_DAIsellDetails items = " + str(len(GetPriceDetailsResultDict_DAIsellDetails)))
        PrintAndLog("API_GetDAIsellDetails: returning new value: receiveAmount_weth_etherUnits = " + str(
            receiveAmount_weth_etherUnits) + ", payAmount_dai_etherUnits = " + str(payAmount_dai_etherUnits) + ", price = " + str(price))
        return price, payAmount_dai_etherUnits


def API_GetDAIbuyDetails(receiveAmount_token_etherUnits, receiveTokenAddress):
    # Sending WETH, receiving DAI
    sendTokenAddress = Exchanges.oasisDex.Contract_WETH
    decimals_dai = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(receiveTokenAddress)))
    payAmount_weth_etherUnits = API_GetPayAmount(sendTokenAddress, receiveTokenAddress, receiveAmount_token_etherUnits, decimals_dai)
    price = payAmount_weth_etherUnits / receiveAmount_token_etherUnits
    PrintAndLog("API_GetDAIbuyDetails: returning new value: receiveAmount_token_etherUnits = " + str(
        receiveAmount_token_etherUnits) + ", payAmount_weth_etherUnits = " + str(payAmount_weth_etherUnits) + ", price = " + str(price))
    return price, payAmount_weth_etherUnits


def API_GetWETHsellDetails(sendAmount_weth_etherUnits, receiveTokenAddress, alwaysGetPriceFromCache=False):
    global GetPriceDetailsResultLock_WETHsellDetails
    global GetPriceDetailsResultDict_WETHsellDetails

    # Before making the call, check to see if we have a cached value that we can return.  If so, return it.  If not, get the new value
    cacheResult = CheckForValidResultFromDict(GetPriceDetailsResultDict_WETHsellDetails, GetPriceDetailsResultLock_WETHsellDetails, sendAmount_weth_etherUnits)
    if cacheResult:
        PrintAndLog("API_GetWETHsellDetails: returning cached value: sendAmount_weth_etherUnits = " + str(
            sendAmount_weth_etherUnits) + ", price(cacheResult) = " + str(cacheResult))
        return cacheResult
    # return None if we're specifically told to ony get data from the cache, but we don't have any cached data
    elif alwaysGetPriceFromCache:
        return None
    # else, get new data and store in cache
    else:
        # Sending WETH, receiving DAI
        sendTokenAddress = Exchanges.oasisDex.Contract_WETH
        decimals_dai = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(receiveTokenAddress)))
        receiveAmount_dai_etherUnits = API_GetBuyAmount(sendTokenAddress, receiveTokenAddress, sendAmount_weth_etherUnits, decimals_dai)
        price = sendAmount_weth_etherUnits / receiveAmount_dai_etherUnits
        # Store the result in the cache
        SetGetPriceDetailsResult(GetPriceDetailsResultDict_WETHsellDetails, GetPriceDetailsResultLock_WETHsellDetails,
                                 sendAmount_weth_etherUnits, price, receiveAmount_dai_etherUnits)
        # PrintAndLog("Set cache GetPriceDetailsResultDict_WETHsellDetails items = " + str(len(GetPriceDetailsResultDict_WETHsellDetails)))
        PrintAndLog("API_GetWETHsellDetails: returning new value: sendAmount_weth_etherUnits = " + str(
            sendAmount_weth_etherUnits) + ", receiveAmount_dai_etherUnits = " + str(receiveAmount_dai_etherUnits) + ", price = " + str(price))
        return price, receiveAmount_dai_etherUnits


def API_GetWETHbuyDetails(sendAmount_dai_etherUnits, sendTokenAddress):
    # Sending DAI, receiving WETH
    receiveTokenAddress = Exchanges.oasisDex.Contract_WETH
    decimals_weth = float("1e" + str(Libraries.core.GetDecimalsForTokenContract(receiveTokenAddress)))
    receiveAmount_weth_etherUnits = API_GetBuyAmount(sendTokenAddress, receiveTokenAddress, sendAmount_dai_etherUnits, decimals_weth)
    price = receiveAmount_weth_etherUnits / sendAmount_dai_etherUnits
    PrintAndLog("API_GetWETHsellDetails: returning new value: sendAmount_dai_etherUnits = " + str(
        sendAmount_dai_etherUnits) + ", receiveAmount_weth_etherUnits = " + str(receiveAmount_weth_etherUnits) + ", price = " + str(price))
    return price, receiveAmount_weth_etherUnits


def API_GetPayAmount(sendTokenAddress, receiveTokenAddress, receiveAmount_etherUnits, decimals_receiveToken):
    import Contracts.contracts
    global Contract_Exchange

    receiveAmount_weiUnits = Libraries.core.ConvertEtherToWei(receiveAmount_etherUnits, decimals_receiveToken)

    kwargs = {
        'pay_gem': sendTokenAddress,
        'buy_gem': receiveTokenAddress,
        'buy_amt': receiveAmount_weiUnits,
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_OasisDex.encodeABI('getPayAmount', kwargs=kwargs),
                "to": Contract_Exchange
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog("API_GetPayAmount responseData = " + str(responseData))
        jData = json.loads(responseData)
        result_hex = jData['result']
        result_wei = Libraries.core.ConvertHexToInt(result_hex)
        result_ether = Libraries.core.ConvertWeiToEther(result_wei, decimals_receiveToken)
        # PrintAndLog("result_ether = " + str(result_ether))
        return result_ether

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetPayAmount response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetBuyAmount(sendTokenAddress, receiveTokenAddress, sendAmount_etherUnits, decimals_sendToken):
    import Contracts.contracts
    global Contract_Exchange

    sendAmount_weiUnits = Libraries.core.ConvertEtherToWei(sendAmount_etherUnits, decimals_sendToken)

    kwargs = {
        'buy_gem': receiveTokenAddress,
        'pay_gem': sendTokenAddress,
        'pay_amt': sendAmount_weiUnits,
    }
    # PrintAndLog("kwargs = " + str(kwargs))
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_OasisDex.encodeABI('getBuyAmount', kwargs=kwargs),
                "to": Contract_Exchange
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog("API_GetBuyAmount responseData = " + str(responseData))
        jData = json.loads(responseData)
        result_hex = jData['result']
        result_wei = Libraries.core.ConvertHexToInt(result_hex)
        result_ether = Libraries.core.ConvertWeiToEther(result_wei, decimals_sendToken)
        # PrintAndLog("result_ether = " + str(result_ether))
        return result_ether

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetBuyAmount response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


# def GetOasisDexOrderbook():
#     from Libraries.accounts import MarketDict
#     from Libraries.exchanges import ExchangeName_OasisDex
#
#     # list all the markets we want to get data for and populate orderbooks for
#     marketNameList = []
#     marketNameList.append('dai-eth')
#     # marketNameList.append('mkr-eth')
#
#     # For each market, get data and populate orderbooks
#     for marketName in marketNameList:
#         try:
#             market = MarketDict[marketName]
#             if not market.marketIsEnabled:
#                 PrintAndLog("market was disabled for marketName = " + str(marketName) + ", skipping it")
#                 continue
#
#             if ExchangeName_OasisDex not in market.ExchangeDict:
#                 PrintAndLog("ExchangeName_OasisDex was not found in market.ExchangeDict for marketName = " + str(marketName) + ", skipping it")
#                 continue
#
#             exchangeDX = market.ExchangeDict[ExchangeName_OasisDex]
#             PrintAndLog("GetOasisDexOrderbook for " + str(marketName) + ", exchangeDX = " + str(exchangeDX.exchangeName))
#
#             maxNumOfOrdersToGet = 100
#             weth = Exchanges.zrxV2.Contract_WETH
#             token = exchangeDX.market.erc20TokenContractAddress
#
#             # Get sell orders
#             buyOrdersList = Libraries.arbyUtility.API_GetOasisDexOrderbook(weth, token, maxNumOfOrdersToGet)
#             sellOrdersList = Libraries.arbyUtility.API_GetOasisDexOrderbook(token, weth, maxNumOfOrdersToGet)
#
#             # PrintAndLog("GetOasisDexOrderbook: for " + str(marketName) + ", sellOrdersList of len " + str(len(sellOrdersList)) + " = " + str(sellOrdersList))
#             # PrintAndLog("GetOasisDexOrderbook: for " + str(marketName) + ", buyOrdersList of len " + str(len(buyOrdersList)) + " = " + str(buyOrdersList))
#
#             # Clear the lists now that we have new data
#             exchangeDX.OrdersList_Buys = []
#             exchangeDX.OrdersList_Sells = []
#
#             for orderTuple in buyOrdersList:
#                 order = CreateOrderFromOasisDexOrderTuple(orderTuple, exchangeDX.exchangeName)
#                 # insert the orders sorted into the list
#                 bisect.insort_left(exchangeDX.OrdersList_Buys, order)
#
#             for orderTuple in sellOrdersList:
#                 order = CreateOrderFromOasisDexOrderTuple(orderTuple, exchangeDX.exchangeName)
#                 # insert the orders sorted into the list
#                 bisect.insort_left(exchangeDX.OrdersList_Sells, order)
#
#             exchangeDX.PrintExchangeInfo(10, True, None)
#
#         except:
#             PrintAndLogError("exception in GetOasisDexOrderbook with " + str(marketName) + ": " + traceback.format_exc())
#             pass


def CreateOrderFromOasisDexOrderTuple(orderTuple, exchangeName):
    # Each item has 4 sets of data
    # uint     pay_amt;
    # Token    pay_gem;
    # uint     buy_amt;
    # Token    buy_gem;

    makerTokenAmount_etherUnits = orderTuple[0]
    makerToken = orderTuple[1]
    takerTokenAmount_etherUnits = orderTuple[2]
    takerToken = orderTuple[3]

    orderType = Libraries.core.GetOrderType_GivenMakerTokenAndTakerToken(makerToken, takerToken)
    # quantity is in tokens
    tokensQuantity = None
    etherQuantity = None
    price = None
    if Libraries.core.IsBuy(orderType):
        tokensQuantity = takerTokenAmount_etherUnits
        etherQuantity = makerTokenAmount_etherUnits
    elif Libraries.core.IsSell(orderType):
        tokensQuantity = makerTokenAmount_etherUnits
        etherQuantity = takerTokenAmount_etherUnits
    else:
        raise Exception("Trading tokens for tokens is not yet implemented, must trade Ether.")

    price = Libraries.core.GetPriceGivenEtherAndTokens(etherQuantity, tokensQuantity)

    # PrintAndLog("CreateOrderFromOasisDexOrderTuple: orderTuple = " + str(orderTuple))
    # PrintAndLog("CreateOrderFromOasisDexOrderTuple: orderType = " + str(orderType))
    # PrintAndLog("CreateOrderFromOasisDexOrderTuple: tokensQuantity = " + str(tokensQuantity))
    # PrintAndLog("CreateOrderFromOasisDexOrderTuple: etherQuantity = " + str(etherQuantity))
    # PrintAndLog("CreateOrderFromOasisDexOrderTuple: price = " + str(price))

    id = "_" + str(orderType)

    return Order_CX(id, tokensQuantity, price, exchangeName)
