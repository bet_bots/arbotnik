import json
import threading
import time
import traceback
from enum import Enum
from random import randint

import jsonpickle
import requests
import websocket

import Exchanges.zrxV2
import Libraries.core
import Libraries.exchanges
import Libraries.executeOnInterval
import Libraries.orderAggregator
import Libraries.signingUtils
from Libraries.core import API_PostOperatorNotification
from Libraries.loggingConfig import PrintAndLog, PrintAndLogError, PrintAndLog_Websockets

# Kovan Alpha
# URL_REST_RadarRelay_Base = "https://api.alpha-da24f79881b8c.radarrelay.com/v2/"
# Mainnet Staging
# URL_REST_RadarRelay_Base = "https://api.rc-da24f79881b8c.radarrelay.com/v2/"
# Mainnet Production
# URL_REST_RadarRelay_Base = "https://api.radarrelay.com/v2/"
# URL_WS_RadarRelay_Base = "wss://ws.radarrelay.com/v2"
# Mainnet production 0xv3
URL_REST_RadarRelay_Base = "https://api-v3.radarrelay.com/v3/"
URL_WS_RadarRelay_Base = "wss://ws-v3.radarrelay.com/v3"

# Mainnet
# https://api-v3.radarrelay.com/v3
# wss://ws-v3.radarrelay.com/v3
#
# Kovan testnet
# https://api.kovan.radarrelay.com/v3
# wss://ws.kovan.radarrelay.com/v3

WebsocketClient = None

# Keep track of what i'm subscribing to so that in the case of a disconnect-reconnect, i can just re-subscribe to stuff.
WebsocketSubscriptionsList = []

Function_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses = None


class WebsocketRequestTopic(Enum):
    BOOK = 'BOOK'
    TICKER = 'TICKER'
    CANDLE = 'CANDLE'


class WebsocketRequestType(Enum):
    SUBSCRIBE = 'SUBSCRIBE'
    UNSUBSCRIBE = 'UNSUBSCRIBE'


class RadarOrderType(Enum):
    BID = 'BID'
    ASK = 'ASK'


class RadarOrderState(Enum):
    OPEN = 'OPEN'
    FILLED = 'FILLED'
    CANCELED = 'CANCELED'
    EXPIRED = 'EXPIRED'
    UNFUNDED = 'UNFUNDED'


class WebsocketAction(Enum):
    FILL = 'FILL'
    NEW = 'NEW'
    CANCEL = 'CANCEL'
    REMOVE = 'REMOVE'


def API_GetListOfTokenAddresses():
    jData = API_GetMarkets()

    tokenAddressList = []
    wethAddress = Exchanges.zrxV2.Contract_WETH
    # PrintAndLog("wethAddress =" + str(wethAddress))

    for marketData in jData:
        # PrintAndLog("marketData = " + str(marketData))
        quoteTokenAddress = marketData['quoteTokenAddress']
        baseTokenAddress = marketData['baseTokenAddress']
        # PrintAndLog("quoteTokenAddress = " + str(quoteTokenAddress))
        # PrintAndLog("baseTokenAddress = " + str(baseTokenAddress))

        tokenAddress = None
        if quoteTokenAddress.lower() != wethAddress.lower() and baseTokenAddress.lower() == wethAddress.lower():
            tokenAddress = quoteTokenAddress
        elif baseTokenAddress.lower() != wethAddress.lower() and quoteTokenAddress.lower() == wethAddress.lower():
            tokenAddress = baseTokenAddress

        if tokenAddress:
            # PrintAndLog("Adding " + str(tokenAddress) + " to array")
            tokenAddressList.append(tokenAddress)
        # else:
        #     PrintAndLog("NOT Adding tokenAddress to array because we couldn't find a match")

    return tokenAddressList


def API_GetMarkets():
    url = URL_REST_RadarRelay_Base + "markets"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetMarkets responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetMarkets jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetMarkets response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetOrders(quoteTokenSymbol, baseTokenSymbol):
    quoteTokenSymbol = Libraries.core.ReplaceETHwithWETH(quoteTokenSymbol)
    baseTokenSymbol = Libraries.core.ReplaceETHwithWETH(baseTokenSymbol)
    marketName = baseTokenSymbol.upper() + "-" + quoteTokenSymbol.upper()

    response = requests.get(URL_REST_RadarRelay_Base + "markets/" + str(marketName) + "/book", headers=Libraries.core.Headers,
                            timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetOrders responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetOrders jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetOrders response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetUnsignedLimitOrder(quoteTokenSymbol, baseTokenSymbol, orderType, quantity, price, expiration):
    quoteTokenSymbol = Libraries.core.ReplaceETHwithWETH(quoteTokenSymbol)
    baseTokenSymbol = Libraries.core.ReplaceETHwithWETH(baseTokenSymbol)
    marketName = baseTokenSymbol.upper() + "-" + quoteTokenSymbol.upper()

    payload = {
        'type': orderType.upper(),
        'quantity': str(quantity),
        'price': str(price),
        'expiration': str(expiration),
    }
    url = URL_REST_RadarRelay_Base + "markets/" + marketName + "/order/limit"
    # PrintAndLog("API_GetUnsignedLimitOrder RadarRelay2 url = " + str(url))
    PrintAndLog("API_GetUnsignedLimitOrder RadarRelay2 payload = " + str(payload))
    response = requests.post(url, data=json.dumps(payload), headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetUnsignedLimitOrder responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetUnsignedLimitOrder jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetUnsignedLimitOrder response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_PostOrder(payload):
    url = URL_REST_RadarRelay_Base + "orders"
    PrintAndLog("API_PostOrder RadarRelay2 url = " + str(url))
    PrintAndLog("API_PostOrder RadarRelay2 payload = " + str(payload))
    response = requests.post(url, data=json.dumps(payload), headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_PostOrder responseData = " + str(responseData))
        return True

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_PostOrder response was not ok response = " + str(response) + ", " + str(response.content) + ". payload = " + str(payload))
        response.raise_for_status()

    return False


def Set_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses(function_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses):
    global Function_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses
    Function_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses = function_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses


def ConnectWebSocketClient(exchangeName):
    global WebsocketClient
    WebsocketClient = WebsocketClientObject(exchangeName)


class WebsocketClientObject(object):
    ws = None
    exchangeName = None

    def __init__(self, _exchangeName):
        self.exchangeName = _exchangeName

        # p = Process(target=self.run, args=())
        # p.start()

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        websocket.enableTrace(False)
        websocket.http_proxy_host = URL_WS_RadarRelay_Base
        self.ws = websocket.WebSocketApp(URL_WS_RadarRelay_Base,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)

        while True:
            try:
                # Subscribe to events
                Libraries.executeOnInterval.IsTimeToExecute("SubscribeWebsockets_AllEvents", 0, SubscribeWebsockets_AllEvents, True)

                Libraries.core.API_PostOperatorNotification("Connecting to RadarRelay2 websockets")
                self.ws.run_forever(ping_interval=30, ping_timeout=10)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in RadarRelay2 websockets = " + traceback.format_exc())
                pass

            PrintAndLog("RadarRelay2 websockets disconnected! Sleeping a bit then trying again")
            time.sleep(30)

    def on_message(self, ws, message):
        global Function_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses

        printMessage = "RadarRelay2: Websockets: data received: " + message[0:90] + "....." + message[-25:]
        PrintAndLog_Websockets(printMessage)
        # Periodically tell me that websockets are working
        if Libraries.executeOnInterval.IsTimeToExecute("RadarRelay2: Websockets: data received", 14400):
            API_PostOperatorNotification(printMessage)

        try:
            jData = json.loads(message)
            # PrintAndLog_Websockets("jData = " + str(jData))
            if 'action' in jData:
                # New order
                if jData['action'] == WebsocketAction.NEW.value:
                    event = jData['event']
                    order = event['order']
                    orderHash = order['orderHash']
                    PrintAndLog_Websockets("RadarRelay2: Websockets: order update! hash = " + str(orderHash))

                    # Get the orderAggregator matching these order addresses
                    orderAggregator = Function_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses(
                        self.exchangeName, event['quoteTokenAddress'], event['baseTokenAddress'])

                    if not orderAggregator:
                        PrintAndLog_Websockets("No orderAggregator was found for exchange " + str(
                            self.exchangeName) + " where quoteTokenAddress = " + str(
                            event['quoteTokenAddress']) + " and baseTokenAddress = " + str(event['baseTokenAddress']))
                        return

                    # TODO, I need to refactor the orderAggregator to support any token as the quoteToken, right now it requires ETH
                    if event['quoteTokenAddress'].lower() != Exchanges.zrxV2.Contract_WETH.lower() and event['baseTokenAddress'].lower() != Exchanges.zrxV2.Contract_WETH.lower():
                        PrintAndLog_Websockets("WETH must be either the quoteToken or the baseToken, not both and not neither. "
                                               "TODO add support for any token to be quoteToken")
                        return

                    # Save the order in the orderAggregator
                    orderAggregator.UpdateOrdersToCache([order], self.exchangeName)

                # Removed order
                elif jData['action'] == WebsocketAction.REMOVE.value or jData['action'] == WebsocketAction.CANCEL.value:
                    event = jData['event']
                    orderHash = event['orderHash']
                    if 'reason' in event:
                        reason = event['reason']
                    elif jData['action'] == WebsocketAction.CANCEL.value:
                        reason = WebsocketAction.CANCEL.value
                    else:
                        reason = "Could not find reason"

                    PrintAndLog_Websockets("RadarRelay2: Websockets: An order has been removed due to '" + str(reason) + "', hash = " + str(orderHash))

                    # Get the orderAggregator matching these order addresses
                    orderAggregator = Function_GetOrderAggregator_GivenQuoteAndBaseTokenAddresses(
                        self.exchangeName, event['quoteTokenAddress'], event['baseTokenAddress'])

                    if not orderAggregator:
                        PrintAndLog_Websockets("No orderAggregator was found for exchange " + str(
                            self.exchangeName) + " where quoteTokenAddress = " + str(
                            event['quoteTokenAddress']) + " and baseTokenAddress = " + str(event['baseTokenAddress']))
                        return

                    # Remove order from orderAggregator
                    orderAggregator.RemoveOrdersFromCache([orderHash])

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when parsing message in WebsocketClientObject: " + traceback.format_exc() + ", message = " + str(message))
            pass

    def on_error(self, ws, error):
        PrintAndLogError(str(error))

    def on_close(self, ws):
        message = "RadarRelay2: Disconnected from websockets."
        PrintAndLogError(message)
        Libraries.core.API_PostOperatorNotification(message)


def SubscribeWebsockets_AllEvents(doSleepFirst=True):
    from Libraries.accounts import TokenDict_Ninja

    if doSleepFirst:
        # Give websockets time to connect
        time.sleep(4)

    Libraries.core.API_PostOperatorNotification("TODO, need to subscribe to websocket events based on tokens that the Ninja is using")
    SubscribeWebsockets_OrderEvents(GetListOfMarketStringsForWSsubscription_Token(TokenDict_Ninja))


def SubscribeWebsockets_OrderEvents(marketStringList):
    ClearWebsocketSubscriptionsList()

    # Subscribe to order change events
    for marketString in marketStringList:
        PrintAndLog("Subscribing to order events for " + marketString)
        payload = {
            'type': WebsocketRequestType.SUBSCRIBE.value,
            'topic': WebsocketRequestTopic.BOOK.value,
            'market': marketString,
            'requestId': randint(0, 99999999999999),
        }
        SubscribeWebsocketsToEndpoint(payload)
        AddSubscriptionPayloadTo_WebsocketSubscriptionsList(payload)


def SubscribeWebsocketsToEndpoint(subscriptionPayload):
    global WebsocketClient
    PrintAndLog("Subscribing to subscriptionPayload = " + str(subscriptionPayload))
    WebsocketClient.ws.send(jsonpickle.encode(subscriptionPayload))


def AddSubscriptionPayloadTo_WebsocketSubscriptionsList(subscriptionPayload):
    global WebsocketSubscriptionsList
    WebsocketSubscriptionsList.append(subscriptionPayload)
    # PrintAndLog("AddSubscriptionPayloadTo_WebsocketSubscriptionsList: WebsocketSubscriptionsList = " + str(WebsocketSubscriptionsList))


def ClearWebsocketSubscriptionsList():
    global WebsocketSubscriptionsList
    WebsocketSubscriptionsList = []


def ResubscribeWebsocketsToAllEndpoints():
    global WebsocketSubscriptionsList

    PrintAndLog("ResubscribeWebsocketsToAllEndpoints: resubscribing to " + str(len(WebsocketSubscriptionsList)) + " websocket endpoints")
    for subscriptionPayload in WebsocketSubscriptionsList:
        SubscribeWebsocketsToEndpoint(subscriptionPayload)


def GetListOfMarketStringsForWSsubscription_Token(customTokenDict):
    returnList = []

    for tokenName in customTokenDict:
        if customTokenDict[tokenName].tokenIsEnabled_betweenBancorAnd0xv2:
            # TODO deprecated baseTokenSymbol_0xMarketPair and quoteTokenSymbol_0xMarketPair
            marketString = customTokenDict[tokenName].baseTokenSymbol_0xMarketPair.upper() + "-" + customTokenDict[tokenName].quoteTokenSymbol_0xMarketPair.upper()
            returnList.append(marketString)

    PrintAndLog("GetListOfMarketStringsForWSsubscription_Token: returnList = " + str(returnList))

    return returnList
