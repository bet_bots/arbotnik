import copy
import datetime
import json
import random
import string
import threading
import time
import traceback
from enum import Enum
from threading import Lock

import jsonpickle
import requests
from eth_abi import encode_single
from web3 import Web3

import Exchanges.compound
import Libraries.cache
import Libraries.core

import Libraries.network
import Libraries.customDicts
import Libraries.executeOnInterval
import Libraries.gasStation
import Libraries.nodes
import Libraries.priceOracle
import Libraries.utils
from Libraries.loggingConfig import InitLogging, PrintAndLog_Detailed, PrintAndLog, PrintAndLogError, PrintAndLog_FuncNameHeader
import Libraries.defaults

# Set these by calling SetNetwork
from Libraries.trade import GetArbitrageProfitPercentageMultiplier_GivenMarket
from Libraries.transactions import API_SendEther_ToContract, TransactionType

Network = None
Contract_TransferProxy = None
Contract_RebalanceAuctionModule = None
Contract_SetCTokenBidderContract = None
Contract_SetTokenManager_ETH20SMACO_HardCodedSoICanLoadInitialContract = None
Contract_WETH = None
Contract_WBTC = None
Contract_USDC = None
Contract_DAI = None

SetTokenDataDict = {}

CTokenListSubstitutions = []

StateResultDict = {}
Lock_StateResultDict = Lock()

Account_KovanTestnet_FromAddress = "0x30937F96903E547aB630920622c77B4b1629439c"
Account_KovanTestnet_FromPrivateKey = "TODO"

Decimals_TokenSet = Libraries.core.Ether_Decimals

# https://fc9e2e276de78afd4806f40ae53dc656.tokensets.com/public/v1/rebalancing_sets
URL_Base_Mainnet_Production = "https://api.tokensets.com/"
URL_Base_Mainnet_Staging = "https://fc9e2e276de78afd4806f40ae53dc656.tokensets.com/"

MaxLength_HistoryDataPoints = 9
MinLengthRequiredToPredictFutureValue_HistoryDataPoints = 4
if MinLengthRequiredToPredictFutureValue_HistoryDataPoints >= MaxLength_HistoryDataPoints:
    raise Exception("MinLengthRequiredToPredictFutureValue_HistoryDataPoints must be at least 1 less than MaxLength_HistoryDataPoints "
                    "because we may not be able to trust the first data point, so i'm always disregarding it")

# So it turns out, calling API_GetBidPrice with the minBidSize as the quantity is a bad idea because it can cause tokens like WBTC give us inaccurate numbers
# I need to use a much larger number so that the price result and the inflowTokenQuantity_etherUnits/outflowTokenQuantity_etherUnits values have more precision
# But if I do this, then I can't just use inflowTokenQuantity_etherUnits/outflowTokenQuantity_etherUnits as the
# minOutflowTokenQuantity_etherUnits/minInflowTokenQuantity_etherUnits below
# So say I pass in a quantity of minBidSize * 10000 (precision multiplier).
# The resulting inflowTokenQuantity_etherUnits/outflowTokenQuantity_etherUnits I get from the function would need to be divided by that precision multiplier
PricePrecisionMultiplier = 100000000

PriceHistoryDataPointsDict = Libraries.customDicts.DictWithValueAndAge()


class RebalancingState(Enum):
    Default = 0
    Proposal = 1
    Rebalance = 2
    Drawdown = 3


class TokenDirection(Enum):
    InflowToken = "inflowToken"
    OutflowToken = "outflowToken"


class TradingStrategy(Enum):
    MovingAverages = "moving_averages"
    UserDefined = "user_defined"
    InverseBenchmark = "inverse_benchmark"
    BuyTheDip = "buy_the_dip"
    BuyAndHold = "buy_and_hold"


class SetTokenData:
    setTokenKey = None
    name = None
    setAddress = None
    managerAddress = None
    tokenComponents = None
    quoteTokenAddress = None
    marketCap = None
    strategy = None
    priceHistoryDataPoints = None
    magicConverterHistoryDataPoints = None

    state = None
    minBidSize_etherUnits = None
    minBidSize_weiUnits = None
    minInflowTokenQuantity_etherUnits = None
    minInflowTokenQuantity_weiUnits = None
    minOutflowTokenQuantity_etherUnits = None
    minOutflowTokenQuantity_weiUnits = None
    remainingCurrentUnits_weiUnits = None
    remainingCurrentShares_weiUnits = None
    outflowTokenAddress = None
    inflowTokenAddress = None
    quoteTokensTradeDirection = None
    price = None
    side = None
    decimals_inflowToken = None
    decimals_outflowToken = None
    ratioOfMinBidSizeToInFlowTokens_weiUnits = None
    ratioOfMinBidSizeToInFlowTokens_etherUnits = None
    maxInflowTokenQuantity_weiUnits = None
    maxInflowTokenQuantity_etherUnits = None
    maxOutflowTokenQuantity_weiUnits = None
    maxOutflowTokenQuantity_etherUnits = None
    magicConverter_inflowTokensToShares_weiUnits = None
    magicConverter_inflowTokensToShares_etherUnits = None

    def __init__(self, _setTokenKey, _name, _setAddress, _managerAddress, _tokenComponents, _quoteTokenAddress, _marketCap, _strategy):
        self.setTokenKey = _setTokenKey
        self.name = _name
        self.setAddress = _setAddress
        self.managerAddress = _managerAddress
        self.tokenComponents = _tokenComponents
        self.quoteTokenAddress = _quoteTokenAddress
        self.marketCap = _marketCap
        self.strategy = TradingStrategy(_strategy)
        self.priceHistoryDataPoints = []
        self.magicConverterHistoryDataPoints = []

    def ResetVariables(self):
        # Do not reset self.setTokenKey or self.state
        # I'm calling this function when I want to reset all of these variables because I don't want a previous rebalance's variables to carry over to a new rebalance
        self.minBidSize_etherUnits = None
        self.minBidSize_weiUnits = None
        self.minInflowTokenQuantity_etherUnits = None
        self.minInflowTokenQuantity_weiUnits = None
        self.minOutflowTokenQuantity_etherUnits = None
        self.minOutflowTokenQuantity_weiUnits = None
        self.remainingCurrentUnits_weiUnits = None
        self.remainingCurrentShares_weiUnits = None
        self.outflowTokenAddress = None
        self.inflowTokenAddress = None
        self.quoteTokensTradeDirection = None
        self.price = None
        self.side = None
        self.decimals_inflowToken = None
        self.decimals_outflowToken = None
        self.ratioOfMinBidSizeToInFlowTokens_weiUnits = None
        self.ratioOfMinBidSizeToInFlowTokens_etherUnits = None
        self.maxInflowTokenQuantity_weiUnits = None
        self.maxInflowTokenQuantity_etherUnits = None
        self.maxOutflowTokenQuantity_weiUnits = None
        self.maxOutflowTokenQuantity_etherUnits = None
        self.magicConverter_inflowTokensToShares_weiUnits = None
        self.magicConverter_inflowTokensToShares_etherUnits = None
        self.priceHistoryDataPoints = []
        self.magicConverterHistoryDataPoints = []

    def UpdateSetTokenData(self):
        global SetTokenDataDict

        try:
            PrintAndLog("----------------- Begin UpdateSetTokenData " + str(self.setTokenKey) + " -----------------")

            header = "UpdateSetTokenData (" + str(self.setTokenKey) + "):"
            beforeDateTime = datetime.datetime.now()

            # Specify a block number so we can record it's corresponding unixEpochTime in StoreHistoricalData functions
            blockNumber_forApiCalls = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
            setTokenContract = SetTokenDataDict[self.setTokenKey].setAddress

            # There are several API calls that are unrelated that I can make in parallel threads right away to save time.
            # Make these calls in parallel
            threads = []

            resultDict = {}
            resultKey_state = "state"
            resultKey_biddingParameters = "biddingParameters"
            resultKey_block = "block"
            lock_resultDict = Lock()

            t = threading.Thread(
                name=threading.currentThread().getName(),  # Keep the thread name the same to help with logging consistency
                target=API_GetRebalanceState,
                args=(setTokenContract, resultDict, resultKey_state, lock_resultDict,))
            threads.append(t)
            t.start()

            t = threading.Thread(
                name=threading.currentThread().getName(),  # Keep the thread name the same to help with logging consistency
                target=API_GetBiddingParameters,
                args=(setTokenContract, resultDict, resultKey_biddingParameters, lock_resultDict,))
            threads.append(t)
            t.start()

            t = threading.Thread(
                name=threading.currentThread().getName(),  # Keep the thread name the same to help with logging consistency
                target=Libraries.core.API_GetBlock,
                args=(blockNumber_forApiCalls, True, resultDict, resultKey_block, lock_resultDict,))
            threads.append(t)
            t.start()

            for thread in threads:
                thread.join()

            # extract the results from the resultDict
            self.state = resultDict[resultKey_state]
            self.minBidSize_weiUnits, self.remainingCurrentUnits_weiUnits = resultDict[resultKey_biddingParameters]
            blockData = resultDict[resultKey_block]

            # self.state = API_GetRebalanceState(setTokenContract)
            if self.state == RebalancingState.Rebalance:
                # Continue with the logic below this if statement
                PrintAndLog_Detailed(header, beforeDateTime,
                                     str(self.setTokenKey) + " is in " + str(self.state) + " mode. Unleash Ninja for rebalance trading!")
            else:
                # Do not attempt to update all the below logic, because the calls to the smart contract will fail.
                # Reset all these values to some initial state
                self.ResetVariables()
                PrintAndLog_Detailed(header, beforeDateTime,
                                     str(self.setTokenKey) + " is in " + str(self.state) + " mode, so there's nothing to rebalance at this time.")
                return

            # self.minBidSize_weiUnits, self.remainingCurrentUnits_weiUnits = API_GetBiddingParameters(setTokenContract)
            self.minBidSize_etherUnits = Libraries.core.ConvertWeiToEther(self.minBidSize_weiUnits, Decimals_TokenSet)
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.minBidSize_weiUnits = " + str(self.minBidSize_weiUnits) + ", self.minBidSize_etherUnits = " + str(self.minBidSize_etherUnits))
            self.remainingCurrentShares_weiUnits = float(self.remainingCurrentUnits_weiUnits) / float(self.minBidSize_weiUnits)
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.remainingCurrentShares_weiUnits = " + str(self.remainingCurrentShares_weiUnits))

            # Multiply by the PricePrecisionMultiplier to get a higher price precision
            quantityToUse = PricePrecisionMultiplier * self.minBidSize_etherUnits

            # Get the block data so we can get the exact timestamp of the block
            unixEpochTime_forApiCalls = Libraries.core.ConvertHexToInt(blockData['timestamp'])
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "blockNumber_forApiCalls = " + str(blockNumber_forApiCalls))
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "unixEpochTime_forApiCalls = " + str(unixEpochTime_forApiCalls))

            # Determine if we need to use the CTokenBidderContract
            dontCare, useSetCTokenBidderContract = self.GetBaseTokenAddress()

            inflowTokenQuantity_weiUnits = None
            outflowTokenQuantity_weiUnits = None

            if useSetCTokenBidderContract:
                self.inflowTokenAddress, self.outflowTokenAddress, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits = \
                    API_GetAddressAndBidPriceArray(setTokenContract, quantityToUse, blockNumber_forApiCalls)

            else:
                # I must call API_GetCombinedTokenArray to determine what tokens this set is trading
                token1, token2 = API_GetCombinedTokenArray(setTokenContract)
                self.inflowTokenAddress, self.outflowTokenAddress = API_DetermineFlow(setTokenContract, self.minBidSize_etherUnits, token1, token2)

            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.outflowTokenAddress = " + str(self.outflowTokenAddress))
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.inflowTokenAddress = " + str(self.inflowTokenAddress))
            self.decimals_outflowToken = "1e" + str(Libraries.core.GetDecimalsForTokenContract(self.outflowTokenAddress))
            self.decimals_inflowToken = "1e" + str(Libraries.core.GetDecimalsForTokenContract(self.inflowTokenAddress))
            # PrintAndLog_Detailed(header, beforeDateTime,
            #                      "self.decimals_outflowToken = " + str(self.decimals_outflowToken))
            # PrintAndLog_Detailed(header, beforeDateTime,
            #                      "self.decimals_inflowToken = " + str(self.decimals_inflowToken))

            if useSetCTokenBidderContract:
                # PricePrecisionMultiplier is being used so we get better precision
                inflowTokenQuantity_weiUnits /= PricePrecisionMultiplier
                outflowTokenQuantity_weiUnits /= PricePrecisionMultiplier

                inflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(inflowTokenQuantity_weiUnits, self.decimals_inflowToken)
                outflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(outflowTokenQuantity_weiUnits, self.decimals_outflowToken)

            self.quoteTokensTradeDirection = None
            if self.inflowTokenAddress.lower() == self.quoteTokenAddress.lower():
                self.quoteTokensTradeDirection = TokenDirection.InflowToken
            elif self.outflowTokenAddress.lower() == self.quoteTokenAddress.lower():
                self.quoteTokensTradeDirection = TokenDirection.OutflowToken
            else:
                raise Exception("Neither inflow nor outflow were the quote token? Something bad happened!")

            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.quoteTokensTradeDirection = " + str(self.quoteTokensTradeDirection))

            if useSetCTokenBidderContract:
                self.side = None
                self.price = None
                if self.quoteTokensTradeDirection == TokenDirection.InflowToken:
                    self.price = inflowTokenQuantity_etherUnits / outflowTokenQuantity_etherUnits
                    self.side = 'buy'
                elif self.quoteTokensTradeDirection == TokenDirection.OutflowToken:
                    self.price = outflowTokenQuantity_etherUnits / inflowTokenQuantity_etherUnits
                    self.side = 'sell'
                else:
                    raise Exception("Not a valid TokenDirection")

                PrintAndLog_Detailed(header, beforeDateTime,
                                     "self.price = " + str(self.price))
                PrintAndLog_Detailed(header, beforeDateTime,
                                     "self.side = " + str(self.side))

            else:
                self.price, self.side, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits = \
                    API_GetBidPrice(setTokenContract, quantityToUse, self.quoteTokensTradeDirection,
                                    self.decimals_outflowToken, self.decimals_inflowToken, blockNumber_forApiCalls)

                # PricePrecisionMultiplier is being used so we get better precision
                inflowTokenQuantity_weiUnits /= PricePrecisionMultiplier
                outflowTokenQuantity_weiUnits /= PricePrecisionMultiplier

                inflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(inflowTokenQuantity_weiUnits, self.decimals_inflowToken)
                outflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(outflowTokenQuantity_weiUnits, self.decimals_outflowToken)

            # TODO< this all needs called later at the end of the function
            # self.StoreHistoricalData_Price(unixEpochTime_forApiCalls, blockNumber_forApiCalls)
            # self.StoreHistoricalData_MagicConvertor_inflowTokensToShares(unixEpochTime_forApiCalls, blockNumber_forApiCalls)
            #
            # PrintAndLog_Detailed(header, beforeDateTime,
            #                      "self.price = " + str(self.price) + ", self.side = " + str(self.side))
            #
            # # Call this to see it in the logs
            # self.GetExpectedPriceXsecondsInTheFuture()

            self.minInflowTokenQuantity_etherUnits = inflowTokenQuantity_etherUnits
            self.minInflowTokenQuantity_weiUnits = Libraries.core.ConvertEtherToWei(inflowTokenQuantity_etherUnits, self.decimals_inflowToken)
            self.minOutflowTokenQuantity_etherUnits = outflowTokenQuantity_etherUnits
            self.minOutflowTokenQuantity_weiUnits = Libraries.core.ConvertEtherToWei(outflowTokenQuantity_etherUnits, self.decimals_outflowToken)
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.minInflowTokenQuantity_etherUnits = " + str(self.minInflowTokenQuantity_etherUnits))
            # PrintAndLog_Detailed(header, beforeDateTime,
            #                      "self.minInflowTokenQuantity_weiUnits = " + str(self.minInflowTokenQuantity_weiUnits))
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.minOutflowTokenQuantity_etherUnits = " + str(self.minOutflowTokenQuantity_etherUnits))
            # PrintAndLog_Detailed(header, beforeDateTime,
            #                      "self.minOutflowTokenQuantity_weiUnits = " + str(self.minOutflowTokenQuantity_weiUnits))

            # Calculate how many inflow tokens are remaining and fillable based on remainingCurrentShares_weiUnits
            # It's one share (remainingCurrentShares_weiUnits) per minInflowTokenQuantity_weiUnits
            # Cut off the decimal by rounding to an int since they do not allow trading a fraction of a share
            self.maxInflowTokenQuantity_weiUnits = int(int(self.remainingCurrentShares_weiUnits) * self.minInflowTokenQuantity_weiUnits)
            self.maxInflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(self.maxInflowTokenQuantity_weiUnits, self.decimals_inflowToken)
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.maxInflowTokenQuantity_weiUnits = " + str(self.maxInflowTokenQuantity_weiUnits))
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.maxInflowTokenQuantity_etherUnits = " + str(self.maxInflowTokenQuantity_etherUnits))
            self.maxOutflowTokenQuantity_weiUnits = int(int(self.remainingCurrentShares_weiUnits) * self.minOutflowTokenQuantity_weiUnits)
            self.maxOutflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(self.maxOutflowTokenQuantity_weiUnits, self.decimals_outflowToken)
            # PrintAndLog_Detailed(header, beforeDateTime,
            #                      "self.maxOutflowTokenQuantity_weiUnits = " + str(self.maxOutflowTokenQuantity_weiUnits))
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.maxOutflowTokenQuantity_etherUnits = " + str(self.maxOutflowTokenQuantity_etherUnits))

            # I need to know the ratio of tokens per min bid size
            # I can use this later to calculate the shares based on what I want to pay
            # Using etherUnits
            self.ratioOfMinBidSizeToInFlowTokens_etherUnits = self.minBidSize_etherUnits / inflowTokenQuantity_etherUnits
            # Using weiUnits
            # inflowTokenQuantity_weiUnits = Libraries.core.ConvertEtherToWei(inflowTokenQuantity_etherUnits, self.decimals_inflowToken)
            self.ratioOfMinBidSizeToInFlowTokens_weiUnits = self.minBidSize_weiUnits / inflowTokenQuantity_weiUnits
            # PrintAndLog_Detailed(header, beforeDateTime,
            #                      "self.ratioOfMinBidSizeToInFlowTokens_etherUnits = " + str(self.ratioOfMinBidSizeToInFlowTokens_etherUnits))
            # PrintAndLog_Detailed(header, beforeDateTime,
            #                      "self.ratioOfMinBidSizeToInFlowTokens_weiUnits = " + str(self.ratioOfMinBidSizeToInFlowTokens_weiUnits))

            # # Formula: shares = inflowTokensToSpend_weiUnits / magicNumber_weiUnits
            # # Formula solidity: shares = safeDiv(safeMul(inflowTokensToSpend_weiUnits, 1000000000000000000), magicNumber_weiUnits)
            testInflowTokensToSpend_weiUnits = 6421223000000000000  # Shouldn't matter what I put in here as long as it's big enough, just don't put in like 5.  Give it at least 18 digits
            testShares = self.GetSharesQuantityForSetTrade_GivenInflowTokenQuantityToSpend_WeiUnits(testInflowTokensToSpend_weiUnits)
            self.magicConverter_inflowTokensToShares_etherUnits = testInflowTokensToSpend_weiUnits / testShares
            self.magicConverter_inflowTokensToShares_weiUnits = Libraries.core.ConvertEtherToWei(self.magicConverter_inflowTokensToShares_etherUnits,
                                                                                                 Libraries.core.Ether_Decimals)
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.magicConverter_inflowTokensToShares_etherUnits = " + str(self.magicConverter_inflowTokensToShares_etherUnits))
            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.magicConverter_inflowTokensToShares_weiUnits = " + str(self.magicConverter_inflowTokensToShares_weiUnits))

            self.StoreHistoricalData_Price(unixEpochTime_forApiCalls, blockNumber_forApiCalls)
            self.StoreHistoricalData_MagicConvertor_inflowTokensToShares(unixEpochTime_forApiCalls, blockNumber_forApiCalls)

            PrintAndLog_Detailed(header, beforeDateTime,
                                 "self.price = " + str(self.price) + ", self.side = " + str(self.side))

            # Call this to see it in the logs
            self.GetExpectedPriceXsecondsInTheFuture()

            PrintAndLog("----------------- End UpdateSetTokenData " + str(self.setTokenKey) + " -----------------")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception in UpdateSetTokenData with key " + str(self.setTokenKey) + " " + str(traceback.format_exc()))
            raise

    def GetSharesQuantityForSetTrade_GivenInflowTokenQuantityToSpend_WeiUnits(self, inflowTokensToSpend_weiUnits):
        # PrintAndLog_FuncNameHeader("self.ratioOfMinBidSizeToInFlowTokens_weiUnits = " + str(self.ratioOfMinBidSizeToInFlowTokens_weiUnits))
        # PrintAndLog_FuncNameHeader("inflowTokensToSpend_weiUnits = " + str(inflowTokensToSpend_weiUnits))
        sharesQuantityForTrade_weiUnits = self.ratioOfMinBidSizeToInFlowTokens_weiUnits * inflowTokensToSpend_weiUnits
        # PrintAndLog_FuncNameHeader("sharesQuantityForTrade_weiUnits = " + str(sharesQuantityForTrade_weiUnits))
        # Make sure we don't accidentally overfill, apply safetiplier
        sharesQuantityForTrade_weiUnits = Libraries.utils.SafetifyThisNumber_int(sharesQuantityForTrade_weiUnits, 99999)
        # PrintAndLog_FuncNameHeader("sharesQuantityForTrade_weiUnits after applying safetiplier = " + str(sharesQuantityForTrade_weiUnits))
        # PrintAndLog_FuncNameHeader("sharesQuantityForTrade_weiUnits BEFORE making multiple of minBidSize_weiUnits = " + str(sharesQuantityForTrade_weiUnits))
        # sharesQuantityForTrade_weiUnits must be multiple of minBidSize_weiUnits
        # Make sure you're dividing by an int here, and not a float or else you won't get the desired result
        sharesQuantityForTrade_weiUnits = int(int(sharesQuantityForTrade_weiUnits / int(self.minBidSize_weiUnits)) * int(self.minBidSize_weiUnits))
        # PrintAndLog_FuncNameHeader("sharesQuantityForTrade_weiUnits AFTER making multiple of minBidSize_weiUnits = " + str(sharesQuantityForTrade_weiUnits))
        return sharesQuantityForTrade_weiUnits

    def GetPrintFriendlyComponents(self):
        returnString = ''
        for component in self.tokenComponents:
            returnString += component['symbol'] + ', '

        return returnString

    def GetBaseTokenAddress(self):
        global CTokenListSubstitutions

        quoteTokenIsInComponents = False
        baseTokenAddress = None
        for component in self.tokenComponents:
            if component['address'].lower() == self.quoteTokenAddress.lower():
                quoteTokenIsInComponents = True
            else:
                # Save the tokenAddress of the base token since that's what this function wants
                baseTokenAddress = component['address'].lower()

        if not quoteTokenIsInComponents:
            message = "Set integration currently does not support this token pair! Please add support for a new quote token"
            if Libraries.executeOnInterval.IsTimeToExecute(message, 6000):
                Libraries.core.API_PostOperatorNotification(message)

            raise Exception(message)

        if not baseTokenAddress:
            raise Exception("baseTokenAddress was not found. This is bad!")

        for pair in CTokenListSubstitutions:
            originalTokenAddress = pair[0]
            compoundTokenAddress = pair[1]
            if baseTokenAddress.lower() == compoundTokenAddress.lower():
                # PrintAndLog("GetBaseTokenAddress: Returning originalTokenAddress " + str(
                #     originalTokenAddress) + " instead of compoundTokenAddress " + str(
                #     compoundTokenAddress) + ". We must call bidAndWithdraw on the SetCTokenBidderContract")
                return originalTokenAddress, True

        # PrintAndLog("GetBaseTokenAddress: Returning a normal token, no compound token complexities. baseTokenAddress = " + str(baseTokenAddress))
        return baseTokenAddress, False

    def GetBaseTokenInSetTokenPair(self, tokenDict_Ninja):
        baseTokenAddress, dontCare = self.GetBaseTokenAddress()
        # Find the token object in the tokenDict_Ninja based on the baseTokenAddress
        for symbol in tokenDict_Ninja:
            if tokenDict_Ninja[symbol].erc20TokenContractAddress.lower() == baseTokenAddress.lower():
                return tokenDict_Ninja[symbol]

        # We should never get this far without returning a token
        raise Exception("Could not find baseTokenAddress = " + str(baseTokenAddress) + " in tokenDict_Ninja")

    def GetMaxQuoteTokenQuantityRemaining_etherUnits(self, quoteToken):
        if quoteToken.lower() == self.inflowTokenAddress.lower():
            return self.maxInflowTokenQuantity_etherUnits
        elif quoteToken.lower() == self.outflowTokenAddress.lower():
            return self.maxOutflowTokenQuantity_etherUnits
        else:
            raise Exception("Not buy or sell")

    def CalculateArbys_ArbitrageProfitPercentage_MinimumRequiredToTrade(self, market, quoteTokenAddress, maxEffectiveQuoteTokenTradable):
        import Libraries.defaults
        # TODO base this on USD marketcap rather than based on maxEffectiveQuoteTokenTradable since that would make more sense going forward to the future
        # TODO, to achieve this, just convert maxEffectiveQuoteTokenTradable to USD, which means I would take the quoteTokenAddress and get the current market price of it
        maxQuoteTokenAvailableInSet = self.GetMaxQuoteTokenQuantityRemaining_etherUnits(quoteTokenAddress)
        PrintAndLog_FuncNameHeader(
            "maxQuoteTokenAvailableInSet = " + str(maxQuoteTokenAvailableInSet) + ", maxEffectiveQuoteTokenTradable = " + str(maxEffectiveQuoteTokenTradable))

        try:
            # Get the price of the quoteTokenAddress against USDC where USDC is the quoteToken and quoteTokenAddress is the baseToken
            price_withRespectToUsd = Libraries.priceOracle.GetOnChainPrice_FromCache_TokenForToken(Exchanges.set.Contract_USDC, quoteTokenAddress)
            PrintAndLog_FuncNameHeader("price_withRespectToUsd = " + str(price_withRespectToUsd))

            setMarketCap = maxQuoteTokenAvailableInSet * price_withRespectToUsd
            PrintAndLog_FuncNameHeader("setMarketCap for " + str(self.setTokenKey) + " = " + str(
                setMarketCap) + ", do confirm that this is correct before I plug it in and use it! Is this marketcap correct?")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception = " + traceback.format_exc())

        # maxQuoteTokenAvailableInSet must be WAY bigger than maxEffectiveQuoteTokenTradable for us to use the greedier profit percentage
        scaleMultiplier_deepLiquidity = 8
        scaleMultiplier_shallowLiquidity = 2
        arbitrageProfitPercentage_MinimumRequiredToTrade = None

        if maxQuoteTokenAvailableInSet > (maxEffectiveQuoteTokenTradable * scaleMultiplier_deepLiquidity):
            arbitrageProfitPercentage_MinimumRequiredToTrade = Libraries.defaults.ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly_DeepLiquidity
            PrintAndLog_FuncNameHeader("Using a larger arbitrage profit percentage because this set is extremely deep WRT my maxEffectiveQuoteTokenTradable. "
                                       "arbitrageProfitPercentage_MinimumRequiredToTrade = " + str(arbitrageProfitPercentage_MinimumRequiredToTrade))
        elif maxQuoteTokenAvailableInSet <= (maxEffectiveQuoteTokenTradable * scaleMultiplier_shallowLiquidity):
            arbitrageProfitPercentage_MinimumRequiredToTrade = Libraries.defaults.ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly_ShallowLiquidity
            PrintAndLog_FuncNameHeader("Using a smaller arbitrage profit percentage because this set is extremely shallow WRT my maxEffectiveQuoteTokenTradable. "
                                       "arbitrageProfitPercentage_MinimumRequiredToTrade = " + str(arbitrageProfitPercentage_MinimumRequiredToTrade))
        else:
            arbitrageProfitPercentage_MinimumRequiredToTrade = Libraries.defaults.ArbitrageProfitPercentage_MinimumRequiredToTrade_SetOnly
            PrintAndLog_FuncNameHeader("Using a standard arbitrage profit percentage. arbitrageProfitPercentage_MinimumRequiredToTrade = " + str(
                arbitrageProfitPercentage_MinimumRequiredToTrade))

        multiplier = GetArbitrageProfitPercentageMultiplier_GivenMarket(market)
        arbitrageProfitPercentage_MinimumRequiredToTrade *= multiplier
        PrintAndLog_FuncNameHeader("arbitrageProfitPercentage_MinimumRequiredToTrade after applying a multiplier of " + str(
            multiplier) + " based on market " + str(market.marketName) + " = " + str(arbitrageProfitPercentage_MinimumRequiredToTrade))
        return arbitrageProfitPercentage_MinimumRequiredToTrade

    def StoreHistoricalData_Price(self, unixEpochTime, blockNumber):
        price = self.price
        # PrintAndLog_FuncNameHeader("unixEpochTime = " + str(unixEpochTime) + ", price = " + str(price))
        # Maintain a list of the most recent data points for price and time
        # Remove old items and just keep a rolling few
        # X axis data is time in epoch seconds
        # Y axis data is price
        # if unixEpochTime is not already in the list
        itemAlreadyInList = False
        for dataPoint in self.priceHistoryDataPoints:
            # If this unixEpochTime is already in there or the price is already in there
            if dataPoint[0] == unixEpochTime or dataPoint[1] == price or dataPoint[2] == blockNumber:
                # This isn't a new price
                itemAlreadyInList = True
                break

        if not itemAlreadyInList:
            # PrintAndLog_FuncNameHeader("inserting unixEpochTime = " + str(unixEpochTime) + " and price = " + str(price))
            # Add it to the list
            self.priceHistoryDataPoints.insert(0, (unixEpochTime, price, blockNumber))
            if len(self.priceHistoryDataPoints) > MaxLength_HistoryDataPoints:
                self.priceHistoryDataPoints.pop()

        else:
            PrintAndLog_FuncNameHeader("not adding this item to the list because it's already there. It must be along time between blocks")

        PrintAndLog_FuncNameHeader("self.priceHistoryDataPoints = " + str(self.priceHistoryDataPoints))

    def StoreHistoricalData_MagicConvertor_inflowTokensToShares(self, unixEpochTime, blockNumber):
        magicConverter = self.magicConverter_inflowTokensToShares_weiUnits
        # if the data's not ready yet, ignore it
        if not magicConverter:
            PrintAndLog_FuncNameHeader("magicConverter was None, returning and skipping this one")
            return

        # PrintAndLog_FuncNameHeader("unixEpochTime = " + str(unixEpochTime) + ", magicConverter = " + str(magicConverter))
        # Maintain a list of the most recent data points for magicConverter and time
        # Remove old items and just keep a rolling few
        # X axis data is time in epoch seconds
        # Y axis data is magicConverter
        # if unixEpochTime is not already in the list
        itemAlreadyInList = False
        for dataPoint in self.magicConverterHistoryDataPoints:
            # If this unixEpochTime is already in there or the magicConverter is already in there
            if dataPoint[0] == unixEpochTime or dataPoint[1] == magicConverter or dataPoint[2] == blockNumber:
                # This isn't a new magicConverter
                itemAlreadyInList = True
                break

        if not itemAlreadyInList:
            # PrintAndLog_FuncNameHeader("inserting unixEpochTime = " + str(unixEpochTime) + " and magicConverter = " + str(magicConverter))
            # Add it to the list
            self.magicConverterHistoryDataPoints.insert(0, (unixEpochTime, magicConverter, blockNumber))
            if len(self.magicConverterHistoryDataPoints) > MaxLength_HistoryDataPoints:
                self.magicConverterHistoryDataPoints.pop()

        else:
            PrintAndLog_FuncNameHeader("not adding this item to the list because it's already there. It must be along time between blocks")

        PrintAndLog_FuncNameHeader("self.magicConverterHistoryDataPoints = " + str(self.magicConverterHistoryDataPoints))

    def GetReadOnlyHistoryDataPoints_Price(self, doExcludeOldestDataPoint=True):
        # This function is needed because we cannot always trust the oldest data point since this application isn't guaranteed to start at the beginning of a block
        # I could remove this problem by making a call to get the block data and use the timestamp from the block data itself, but I feel that's not necessary
        # and I'd prefer to not make that call to my node unless I really need to
        listToReturn = copy.deepcopy(self.priceHistoryDataPoints)

        if doExcludeOldestDataPoint and len(listToReturn) > 0:
            # Remove last item
            listToReturn.pop()

        return listToReturn

    def GetReadOnlyHistoryDataPoints_MagicConvertor(self, doExcludeOldestDataPoint=True):
        # This function is needed because we cannot always trust the oldest data point since this application isn't guaranteed to start at the beginning of a block
        # I could remove this problem by making a call to get the block data and use the timestamp from the block data itself, but I feel that's not necessary
        # and I'd prefer to not make that call to my node unless I really need to
        listToReturn = copy.deepcopy(self.magicConverterHistoryDataPoints)

        if doExcludeOldestDataPoint and len(listToReturn) > 0:
            # Remove last item
            listToReturn.pop()

        return listToReturn

    def GetExpectedPriceXsecondsInTheFuture(self, doPrintDebug=True):
        global PriceHistoryDataPointsDict

        if not Libraries.defaults.SetAllowedToPredictFuturePrice:
            # Return the flag False saying that a new price is NOT due on the next mined block
            return self.price, False, self.magicConverter_inflowTokensToShares_weiUnits

        # This function assumes self.priceHistoryDataPoints has already been populated
        # These set rebalances change price over time. I'm assuming linear function for now
        # Take points from history, make a function given data from historical values and use it to predict the future

        # X axis data is time in epoch seconds
        # Y axis data is price

        # Call GetReadOnlyHistoryDataPoints_Price rather than directly using self.priceHistoryDataPoints because we may not be able to trust the oldest data point
        priceHistoryDataPoints_toUse = self.GetReadOnlyHistoryDataPoints_Price()
        magicConverterHistoryDataPoints_toUse = self.GetReadOnlyHistoryDataPoints_MagicConvertor()
        # magicConverterHistoryDataPoints_toUse = None

        # Get the priceHistoryDataPoints_toUse from the PriceHistoryDataPointsDict
        returnValue = PriceHistoryDataPointsDict.GetValue(self.setTokenKey)
        if returnValue:
            secondsPerPriceChange = returnValue[0]
            dataAge_seconds = returnValue[1]

            # if the data is too old, something must be wrong let's just ignore the data and do not predict future price based on it
            if dataAge_seconds > 300:
                if Libraries.executeOnInterval.IsTimeToExecute("PriceHistoryDataPointsDict Data age is too old", 1000):
                    message = 'PriceHistoryDataPointsDict Data age is too old (' + str(dataAge_seconds) + ' seconds. Why did this happen?'
                    Libraries.core.API_PostOperatorNotification(message)

                # Return the flag False saying that a new price is NOT due on the next mined block
                return self.price, False, self.magicConverter_inflowTokensToShares_weiUnits
        else:
            PrintAndLog_FuncNameHeader("we don't yet have any data from PriceHistoryDataPointsDict, so let's just returning the current values")
            # Return the flag False saying that a new price is NOT due on the next mined block
            return self.price, False, self.magicConverter_inflowTokensToShares_weiUnits

        if doPrintDebug:
            PrintAndLog_FuncNameHeader("secondsPerPriceChange = " + str(secondsPerPriceChange) + " seconds")

        # These lists should have the same length. If they do not, either something bad happened or maybe on initialization one wasn't ready when the other was
        if doPrintDebug:
            PrintAndLog_FuncNameHeader("priceHistoryDataPoints_toUse of len " + str(
                len(priceHistoryDataPoints_toUse)) + " = " + str(priceHistoryDataPoints_toUse))
            PrintAndLog_FuncNameHeader("magicConverterHistoryDataPoints_toUse of len " + str(
                len(magicConverterHistoryDataPoints_toUse)) + " = " + str(magicConverterHistoryDataPoints_toUse))

        # If we don't yet have any historical data to work with
        if len(priceHistoryDataPoints_toUse) == 0:
            # Just return the current price
            # Return the flag False saying that a new price is NOT due on the next mined block
            return self.price, False, self.magicConverter_inflowTokensToShares_weiUnits

        # If the current price is super far away from fair value, like not even close, don't try to predict future price.
        # Some sets start rebalancing extremely early and way far away from fair value.
        # I found some weird behaviors and bugs where future predicted price will actually end up being negative because of this
        fairValuePrice_onChain = Libraries.priceOracle.GetOnChainPrice_FromCache_TokenForToken(self.quoteTokenAddress, self.GetBaseTokenAddress()[0])
        if doPrintDebug:
            PrintAndLog_FuncNameHeader("fairValuePrice_onChain = " + str(fairValuePrice_onChain) + ", self.price = " + str(self.price))

        # Find the price difference between the fair value price and the current price
        priceDifferencePercentage = abs(Libraries.core.CalculateTradeProfitPercentage(fairValuePrice_onChain, self.price))
        # 0.20 = 20%, which means if the prices differ by more than 20% than do not consider predicting a future price because set's price curve may be far from linear!
        threshold_priceDifferencePercentage = 0.25
        if doPrintDebug:
            PrintAndLog_FuncNameHeader(
                "priceDifferencePercentage = " + str(priceDifferencePercentage) + ", threshold_priceDifferencePercentage = " + str(threshold_priceDifferencePercentage))

        if priceDifferencePercentage > threshold_priceDifferencePercentage:
            if doPrintDebug:
                PrintAndLog_FuncNameHeader("Not able to predict future price because current price is so insanely far away from fair value "
                                           "that we don't want to try and apply a price curve to this yet. "
                                           "self.price = " + str(self.price) + ", fairValuePrice_onChain = " + str(fairValuePrice_onChain))
            # Return the flag False saying that a new price is NOT due on the next mined block
            return self.price, False, self.magicConverter_inflowTokensToShares_weiUnits

        # TODO, refactor this function and only use this futurePrice if we predict that the price will change
        # The price only changes periodically. In some cases it's one change every ~160 seconds. Other cases it's every ~30 seconds.
        # It depends on how they configured the set
        # There's one item in priceHistoryDataPoints for each price change.
        if len(priceHistoryDataPoints_toUse) >= MinLengthRequiredToPredictFutureValue_HistoryDataPoints:
            if doPrintDebug:
                PrintAndLog_FuncNameHeader("Eligible to predict future price. But we need too check to see if a future price is within range, "
                                           "or if the next mined block will have the same price priceHistoryDataPoints_toUse = " + str(priceHistoryDataPoints_toUse))

            # The predicted time stamp of the next block is always the unix epoch time that the last block when it was mined
            predictedTimeStampOfNextBlock_unixEpoch = Libraries.core.GetThreadSafeCopyOf_LatestUnixEpochTime_int()
            # Predict the price
            futurePrice = Libraries.utils.GetLinearFunctionGivenDataPoints(priceHistoryDataPoints_toUse, predictedTimeStampOfNextBlock_unixEpoch)
            # Predict the magicConverter
            futureMagicConverter = int(Libraries.utils.GetLinearFunctionGivenDataPoints(magicConverterHistoryDataPoints_toUse, predictedTimeStampOfNextBlock_unixEpoch))
            # # TODO, should I predict a future magic converter just like I am predicting a future price? I'm taking a short cut for now because that's a lot of work...
            # futureMagicConverter = self.magicConverter_inflowTokensToShares_weiUnits
            if doPrintDebug:
                PrintAndLog_FuncNameHeader("predictedTimeStampOfNextBlock_unixEpoch = " + str(predictedTimeStampOfNextBlock_unixEpoch) + ", futurePrice = " + str(
                    futurePrice) + ", priceHistoryDataPoints_toUse = " + str(priceHistoryDataPoints_toUse))
                PrintAndLog_FuncNameHeader("comparing self.price = " + str(self.price) + " and futurePrice = " + str(futurePrice))
                PrintAndLog_FuncNameHeader(
                    "predictedTimeStampOfNextBlock_unixEpoch = " + str(predictedTimeStampOfNextBlock_unixEpoch) + ", futureMagicConverter = " + str(
                        futureMagicConverter) + ", magicConverterHistoryDataPoints_toUse = " + str(magicConverterHistoryDataPoints_toUse))
                PrintAndLog_FuncNameHeader("comparing self.magicConverter_inflowTokensToShares_weiUnits = " + str(
                    self.magicConverter_inflowTokensToShares_weiUnits) + " and futureMagicConverter = " + str(futureMagicConverter))

            newestDataPoint_unixEpochSeconds = priceHistoryDataPoints_toUse[0][0]
            newestDataPoint_blockNumber = priceHistoryDataPoints_toUse[0][2]

            if doPrintDebug:
                PrintAndLog_FuncNameHeader("secondsPerPriceChange = " + str(secondsPerPriceChange))
                PrintAndLog_FuncNameHeader("newestDataPoint_unixEpochSeconds = " + str(newestDataPoint_unixEpochSeconds))
                PrintAndLog_FuncNameHeader("newestDataPoint_blockNumber = " + str(newestDataPoint_blockNumber) + " and it was discovered at " + str(
                    Libraries.core.BlockDiscoveryTimeStampDict[newestDataPoint_blockNumber]) + " seconds")
                PrintAndLog_FuncNameHeader("time.time() = " + str(time.time()))
                PrintAndLog_FuncNameHeader("predictedTimeStampOfNextBlock_unixEpoch = " + str(predictedTimeStampOfNextBlock_unixEpoch))

            # Calculate the timeSinceLastPriceUpdate so we can determine when the price should change again
            # timeSinceLastPriceUpdate = time.time() - Libraries.core.BlockDiscoveryTimeStampDict[newestDataPoint_blockNumber]
            timeSinceLastPriceUpdate = Libraries.core.GetTimeSinceBlockWasDiscovered(newestDataPoint_blockNumber)

            if doPrintDebug:
                PrintAndLog_FuncNameHeader("timeSinceLastPriceUpdate = " + str(timeSinceLastPriceUpdate) + " seconds")

            # Playing it safe and adding an adder/multiplier
            multiplier = 1.1
            adder = 5
            # Determine what threshold to use, apply an adder and multiplier to make sure we don't accidentally
            threshold_secondsPerPriceChange = (secondsPerPriceChange * multiplier) + adder

            if doPrintDebug:
                PrintAndLog_FuncNameHeader("threshold_secondsPerPriceChange = " + str(
                    threshold_secondsPerPriceChange) + " which was set from secondsPerPriceChange = " + str(secondsPerPriceChange) + " and an adder/multiplier")

            if timeSinceLastPriceUpdate >= threshold_secondsPerPriceChange:
                # Note, I'm having a problem with my futurePrice.  For some reason I'm occasionally off and it's causing me to trigger trades when the price hasn't changed yet
                # This is an ongoing investigation.
                # For now, I'm applying this little hack that basically says pick a price somewhere between the futurePrice I just calculated and the current price that I know is safe
                # From what i've seen, my futurePrice is very close but often times is off by a very small amount.  ANd that has been just enough to cost me a lot of ETH
                # So this little hack is going to save me a lot of money and still give me a small advantage over other trades while I try to address this problem another way
                percentageTowardsValue1 = 0.75
                # 88 percent worked for today's whoops, I'm thinking 80% should be fine, but better to be safe here since this has proven to be an expensive bug
                safetifiedFuturePrice = Libraries.utils.GetValueBetweenTwoValuesBasedOnPercentage(
                    futurePrice, self.price, percentageTowardsValue1)
                safetifiedMagicConverter = int(Libraries.utils.GetValueBetweenTwoValuesBasedOnPercentage(
                    futureMagicConverter, self.magicConverter_inflowTokensToShares_weiUnits, percentageTowardsValue1))

                if doPrintDebug:
                    PrintAndLog_FuncNameHeader("self.price = " + str(self.price))
                    PrintAndLog_FuncNameHeader("safetifiedFuturePrice = " + str(safetifiedFuturePrice))
                    PrintAndLog_FuncNameHeader("futurePrice = " + str(futurePrice))

                    PrintAndLog_FuncNameHeader("self.magicConverter_inflowTokensToShares_weiUnits = " + str(self.magicConverter_inflowTokensToShares_weiUnits))
                    PrintAndLog_FuncNameHeader("safetifiedMagicConverter = " + str(safetifiedMagicConverter))
                    PrintAndLog_FuncNameHeader("futureMagicConverter = " + str(futureMagicConverter))

                if doPrintDebug:
                    PrintAndLog_FuncNameHeader("Returning safetifiedFuturePrice of " + str(safetifiedFuturePrice) + " as opposed to current price which is " + str(
                        self.price) + " because it's been " + str(
                        timeSinceLastPriceUpdate) + " seconds which is greater than our threshold_secondsPerPriceChange of " + str(
                        threshold_secondsPerPriceChange) + " seconds. safetifiedFuturePrice was calculated from futurePrice which = " + str(
                        futurePrice) + ". safetifiedFuturePrice should be somewhere between self.price and futurePrice to help prevent bugs")

                    PrintAndLog_FuncNameHeader(
                        "Also returning safetifiedMagicConverter of " + str(safetifiedMagicConverter) + " as opposed to current magicConverter which is " + str(
                            self.magicConverter_inflowTokensToShares_weiUnits) + " because it's been " + str(
                            timeSinceLastPriceUpdate) + " seconds which is greater than our threshold_secondsPerPriceChange of " + str(
                            threshold_secondsPerPriceChange) + " seconds. safetifiedMagicConverter was calculated from futureMagicConverter which = " + str(
                            futureMagicConverter) + ". safetifiedMagicConverter should be somewhere between self.magicConverter_inflowTokensToShares_weiUnits and futureMagicConverter to help prevent bugs")

                # Return the flag True saying that a new price is due on the next mined block
                return safetifiedFuturePrice, True, safetifiedMagicConverter

            else:
                if doPrintDebug:
                    PrintAndLog_FuncNameHeader("Returning self.price of " + str(self.price) + " because not enough time has passed since the last price change. " + str(
                        round(timeSinceLastPriceUpdate,
                              1)) + " seconds have passed since our last price change which is less than our threshold_secondsPerPriceChange of " + str(
                        threshold_secondsPerPriceChange) + " seconds")

                    PrintAndLog_FuncNameHeader("Also returning self.magicConverter_inflowTokensToShares_weiUnits of " + str(
                        self.magicConverter_inflowTokensToShares_weiUnits) + " because not enough time has passed since the last price change. " + str(
                        round(timeSinceLastPriceUpdate,
                              1)) + " seconds have passed since our last price change which is less than our threshold_secondsPerPriceChange of " + str(
                        threshold_secondsPerPriceChange) + " seconds")

                # Return the flag False saying that a new price is NOT due on the next mined block
                return self.price, False, self.magicConverter_inflowTokensToShares_weiUnits

        else:
            if doPrintDebug:
                PrintAndLog_FuncNameHeader("Not able to predict future price because we don't have enough data. Returning current price. "
                                           "len(priceHistoryDataPoints_toUse) = " + str(len(priceHistoryDataPoints_toUse)))

            # Return the flag False saying that a new price is NOT due on the next mined block
            return self.price, False, self.magicConverter_inflowTokensToShares_weiUnits

    def DetermineRebalanceAuctionModule(self):
        global Contract_RebalanceAuctionModule
        global Contract_SetCTokenBidderContract

        dontCare, useSetCTokenBidderContract = self.GetBaseTokenAddress()
        if useSetCTokenBidderContract:
            return Contract_SetCTokenBidderContract
        else:
            return Contract_RebalanceAuctionModule

    def GetInflowTokenAddressForTrading(self):
        return self.GetPossibleCompoundTokensOriginalTokenAddress(self.inflowTokenAddress)

    def GetOutflowTokenAddressForTrading(self):
        return self.GetPossibleCompoundTokensOriginalTokenAddress(self.outflowTokenAddress)

    def GetPossibleCompoundTokensOriginalTokenAddress(self, possibleCompoundToken):
        for pair in CTokenListSubstitutions:
            originalTokenAddress = pair[0]
            compoundTokenAddress = pair[1]
            if possibleCompoundToken.lower() == compoundTokenAddress.lower():
                return originalTokenAddress

        # If we didn't return yet, just return the param passed into the function
        return possibleCompoundToken


def GetUrlBase():
    if Network == Libraries.network.Network.mainnet:
        if Libraries.defaults.Set_UseMainnetStagingContracts:
            return URL_Base_Mainnet_Staging
        else:
            return URL_Base_Mainnet_Production

    elif Network == Libraries.network.Network.kovanTestnet:
        raise Exception("Network not yet implemented")

    else:
        raise Exception("Network not yet implemented")


def GetSetKey_MatchingSetAddress(setTokenAddress_toMatch):
    global SetTokenDataDict

    for key, value in SetTokenDataDict.items():
        if value.setAddress == setTokenAddress_toMatch:
            return key


def PopulateSetTokenDataDict():
    global SetTokenDataDict

    jData_rebalancing_sets = Libraries.cache.GetDataFromCache("Set_RebalancingSets")

    # Create my custom dictionary of data out of this response because i don't like how it's formatted
    SetTokenDataDict = {}
    for setData in jData_rebalancing_sets:
        # PrintAndLog("GetAndCache_Set_RebalancingSets: setData = " + str(setData))
        setKey = setData['id']

        try:
            quoteTokenAddress = DetermineQuoteTokenGivenTokenComponents(setKey, setData['components'])

            SetTokenDataDict[setKey] = SetTokenData(
                setKey,
                setData['name'],
                Libraries.nodes.Instance_Web3.toChecksumAddress(setData['address']),
                Libraries.nodes.Instance_Web3.toChecksumAddress(setData['manager']),
                setData['components'],
                quoteTokenAddress,
                float(setData['market_cap']),
                setData['strategy'])

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            message = "exception when creating SetTokenData for " + str(setKey) + ", exception = " + traceback.format_exc()
            PrintAndLogError(message)
            Libraries.core.API_PostOperatorNotification(message)
            pass

    PrintAndLog("PopulateSetTokenDataDict: SetTokenDataDict len = " + str(len(SetTokenDataDict)))

    # For debug purposes, I can delete all set tokens except for one so that my logs are cleaner when debugging
    if Libraries.defaults.Set_DeleteAllSetTokensExceptForOne:
        # setTokenToKeep = "ethbtcrsi7030"
        # setTokenToKeep = "joeyethdai"
        setTokenToKeep = "jethcusdc"
        # setTokenToKeep = "rbtccusdc"
        # setTokenToKeep = "wbtcusdcv5"
        # setTokenToKeep = "jlinkusdc"

        PrintAndLog("SetTokenDataDict before deleting all but one, len = " + str(len(SetTokenDataDict)))
        for setKey in SetTokenDataDict.copy():
            if setKey != setTokenToKeep:
                del SetTokenDataDict[setKey]

        PrintAndLog("PopulateSetTokenDataDict: SetTokenDataDict after deleting all but one, len = " + str(len(SetTokenDataDict)))


def CanNinjaObjectTradeOnThisSetTokenData(ninjaObject, setTokenData):
    # PrintAndLog_FuncNameHeader("ninjaObject.ninjaType = " + str(ninjaObject.ninjaType) + ", setTokenData.tokenComponents = " + str(setTokenData.tokenComponents))
    # PrintAndLog_FuncNameHeader("ninjaObject.requiredTokenList_Set = " + str(ninjaObject.requiredTokenList_Set))
    # PrintAndLog_FuncNameHeader("ninjaObject.excludedTokenList_Set = " + str(ninjaObject.excludedTokenList_Set))

    # Require that it has at least one of the tokens in ninjaObject.requiredTokenList_Set
    hasAtLeastOneRequiredToken = False
    for tokenAddress in ninjaObject.requiredTokenList_Set:
        if IsTokenAddressInComponents(tokenAddress, setTokenData.tokenComponents):
            hasAtLeastOneRequiredToken = True

    # PrintAndLog_FuncNameHeader("hasAtLeastOneRequiredToken = " + str(hasAtLeastOneRequiredToken))

    # Require that it has NONE of the tokens in ninjaObject.excludedTokenList_Set
    for sub_tokenAddress in ninjaObject.excludedTokenList_Set:
        if IsTokenAddressInComponents(sub_tokenAddress, setTokenData.tokenComponents):
            # PrintAndLog_FuncNameHeader("returning False. Failed in excludedTokenList_Set")
            # We cannot trade if it includes a token that we are excluding
            return False

    returnValue = hasAtLeastOneRequiredToken
    # PrintAndLog_FuncNameHeader("returning " + str(returnValue))
    return returnValue


def DetermineQuoteTokenGivenTokenComponents(setKey, tokenComponents):
    # Add to this list as I need more quote tokens
    # NOTE: Order is critical here. The first one in the list is highest priority.
    # If that one is not found in the set's tokenComponents then it moves on to the next address in the list. So on and so forth.
    # When you support a new quote token, add it here.
    possibleQuoteTokenList = []
    possibleQuoteTokenList.append(Contract_WETH)
    possibleQuoteTokenList.append(Contract_WBTC)
    possibleQuoteTokenList.append(Contract_USDC)

    for possibleQuoteToken in possibleQuoteTokenList:
        if IsTokenAddressInComponents(possibleQuoteToken, tokenComponents):
            return possibleQuoteToken

    # If we have not already returned a quoteToken, this means that none of the tokens found in possibleQuoteTokenList are in this set
    # So we have to pick another token as the quoteToken.  For now, let's just fall back to setting the quote token to the first token we see
    # Just fall back to the first one

    message = "Went through all the possible tokens in the possibleQuoteTokenList and we weren't able to match any to this set! setKey = " + str(
        setKey) + ". possibleQuoteTokenList = " + str(possibleQuoteTokenList) + " we are returning the quote token as the first token found in the set components"
    # Periodically send this message via API_PostOperatorNotification
    if Libraries.executeOnInterval.IsTimeToExecute(message, 6000):
        Libraries.core.API_PostOperatorNotification(message)

    return tokenComponents[0]['address'].lower()


def API_GetRebalancingSets():
    # Example for staging
    # https://fc9e2e276de78afd4806f40ae53dc656.tokensets.com/public/v1/rebalancing_sets
    url = GetUrlBase() + "public/v1/" + "rebalancing_sets"
    PrintAndLog_FuncNameHeader("url = " + str(url))
    response = requests.get(url, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("API_GetRebalancingSets jData = " + str(jData))

        # Set was kind enough to make me some sets I can manually test with on their staging environment.
        # But for some reason they occasionally remove them from the API.
        # I've hard coded the data from the API into my code so I do not need them to return the addresses from the API for my own debugging since the
        # smart contracts still exist on the blockchain
        if Libraries.defaults.Set_UseMainnetStagingContracts and Libraries.defaults.Set_AddMyHardCodedStagingContractsToSetData:
            filepath = Libraries.defaults.Directory_Exchanges + "/" + "set_stagingSets.json"
            # PrintAndLog_FuncNameHeader("filepath = " + str(filepath))
            file = open(filepath, "r")

            testStagingSetDataList = jsonpickle.decode(file.read())
            file.close()

            for testStagingSetData in testStagingSetDataList:
                # PrintAndLog_FuncNameHeader("found test set: " + str(testStagingSetData['id']))
                if testStagingSetData['id'] not in jData['rebalancing_sets']:
                    # PrintAndLog_FuncNameHeader("   test set: " + str(testStagingSetData['id']) + " is NOT in API_GetRebalancingSets's response! "
                    #                                                                              "I'm going to add it because Set_AddMyHardCodedStagingContractsToSetData was set")
                    # PrintAndLog_FuncNameHeader("jData BEFORE = " + str(jData))
                    jData['rebalancing_sets'].append(testStagingSetData)
                    # PrintAndLog_FuncNameHeader("jData AFTER = " + str(jData))

                # else:
                #     PrintAndLog_FuncNameHeader("   test set: " + str(testStagingSetData['id']) + " was already found in API_GetRebalancingSets's response! All Set")

        return jData['rebalancing_sets']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetRebalancingSet(setName, jData_rebalancingSets=None):
    # Get the jData_rebalancingSets if we haven't already
    if not jData_rebalancingSets:
        jData_rebalancingSets = API_GetRebalancingSets()

    listOfMatchingSets = []
    for rebalancing_set in jData_rebalancingSets:
        # This logic no longer works
        # if setName.lower() in rebalancing_set['id'].lower():
        # if rebalancing_set['id'].lower().startswith(setName.lower()):
        if rebalancing_set['id'].lower() == setName.lower():
            listOfMatchingSets.append(rebalancing_set)

    if len(listOfMatchingSets) > 1:
        raise Exception("Found more than one match! This should never happen!")
    elif len(listOfMatchingSets) <= 0:
        raise Exception("Found invalid number of matches! This should never happen!")
    else:
        return listOfMatchingSets[0]


def API_GetRebalancingSetDetails(setName):
    # Example for staging
    # https://fc9e2e276de78afd4806f40ae53dc656.tokensets.com/v1/rebalancing_sets/btceth7525-3
    url = GetUrlBase() + "v1/" + "rebalancing_sets/" + str(setName)
    # PrintAndLog("API_GetRebalancingSetDetails url = " + str(url))
    response = requests.get(url, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetRebalancingSetDetails jData = " + str(jData))
        return jData['rebalancing_set']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        response.raise_for_status()


def API_GetRebalancingSetState_Custom(setKey, jData_rebalancingSets=None):
    # TODO Why am I calling the same functions twice/three times???
    # TODO I should already have all the data for the sets especially since i'm passing in jData_rebalancingSets
    # TODO So why am I calling API_GetRebalancingSet then also API_GetRebalancingSetDetails???

    jData_rebalancingSet = API_GetRebalancingSet(setKey, jData_rebalancingSets)
    PrintAndLog("API_GetRebalancingSetState_Custom: jData_rebalancingSet for setKey " + str(setKey) + " = " + str(jData_rebalancingSet))

    # Get the actual setId from the response.  Sometimes they add a "-1" to the end of the id
    setId = jData_rebalancingSet['id']

    # Try to find the data in jData_rebalancingSets before I make the call
    rebalancingSetDataDetails = None
    for rebalancing_set in jData_rebalancingSets:
        if rebalancing_set['id'].lower() == setId.lower():
            rebalancingSetDataDetails = rebalancing_set

    # Do I still need to make the call to get the data?
    if not rebalancingSetDataDetails:
        rebalancingSetDataDetails = API_GetRebalancingSetDetails(setId, jData_rebalancingSets)

    # PrintAndLog("rebalancingSetDataDetails for " + str(setKey) + " = " + str(rebalancingSetDataDetails))
    status = rebalancingSetDataDetails['status']
    rebalanceCriteria = rebalancingSetDataDetails['rebalance_criteria']
    components = rebalancingSetDataDetails['components']
    # PrintAndLog("status = " + str(status))
    # PrintAndLog("rebalanceCriteria = " + str(rebalanceCriteria))
    # PrintAndLog("components = " + str(components))
    return status, rebalanceCriteria, components


def UpdateAllSetTokenData_OnlySetsCurrentlyInRebalanceMode():
    before = datetime.datetime.now()
    updateCount = UpdateAllSetTokenData(False)
    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("updated " + str(updateCount) + " sets in = " + str(duration_s) + " seconds")


# This call has been killing my nodes bogging them down really hard since there are so many sets now
# I need to call UpdateSetTokenData in series instead of parallel
# def UpdateAllSetTokenData(doUpdateRegardlessOfMode=True):
#     global SetTokenDataDict
#
#     # Call UpdateSetTokenData on all sets in parallel threads
#     threads = []
#     updateCount = 0
#     for key in SetTokenDataDict:
#         try:
#             # Note, if doUpdateRegardlessOfMode is False then something else better be calling this with doUpdateRegardlessOfMode = True so that the state is updating!
#             if doUpdateRegardlessOfMode or SetTokenDataDict[key].state == RebalancingState.Rebalance:
#                 threadName = Libraries.loggingConfig.GetLoggerName("Set_" + key)
#                 Libraries.loggingConfig.RegisterLogger(threadName, Directory_Logs_Set)
#
#                 t = threading.Thread(
#                     name=threadName,
#                     target=SetTokenDataDict[key].UpdateSetTokenData,
#                     args=())
#                 threads.append(t)
#                 t.start()
#
#                 # SetTokenDataDict[key].UpdateSetTokenData()
#                 updateCount += 1
#
#             else:
#                 pass
#                 # PrintAndLog("----------------- Not updating " + str(key) + " because doUpdateRegardlessOfMode == False and it's not currently in rebalance mode -----------------")
#
#         except (KeyboardInterrupt, SystemExit):
#             print('\nkeyboard interrupt caught')
#             print('\n...Program Stopped Manually!')
#             raise
#
#         except:
#             PrintAndLogError("exception when trying to UpdateAllSetTokenData " + str(key) + ": " + traceback.format_exc())
#             pass
#
#     for thread in threads:
#         thread.join()
#
#     return updateCount


def UpdateAllSetTokenData(doUpdateRegardlessOfMode=True):
    global SetTokenDataDict

    before = datetime.datetime.now()
    updateCount = 0
    for key in SetTokenDataDict:
        try:
            # Note, if doUpdateRegardlessOfMode is False then something else better be calling this with doUpdateRegardlessOfMode = True so that the state is updating!
            if doUpdateRegardlessOfMode or SetTokenDataDict[key].state == RebalancingState.Rebalance:
                threadName = Libraries.loggingConfig.GetLoggerName("Set_" + key)
                Libraries.loggingConfig.RegisterLogger(threadName, Libraries.defaults.Directory_Logs_Set)

                SetTokenDataDict[key].UpdateSetTokenData()
                updateCount += 1

            else:
                pass
                # PrintAndLog("----------------- Not updating " + str(key) + " because doUpdateRegardlessOfMode == False and it's not currently in rebalance mode -----------------")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when trying to UpdateAllSetTokenData " + str(key) + ": " + traceback.format_exc())
            pass

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("duration_s = " + str(duration_s))

    return updateCount

# This call has been killing my nodes bogging them down really hard since there are so many sets now
# I need to call UpdateSetTokenData in series instead of parallel
# def CalculateAllSetsAverageTimeBetweenPriceChanges():
#     global SetTokenDataDict
#
#     # Leaving this here as an option for the future, but I should never need to set this to True
#     doUpdateRegardlessOfMode = False
#
#     # Call UpdateSetTokenData on all sets in parallel threads
#     threads = []
#     updateCount = 0
#     for key in SetTokenDataDict:
#         try:
#             # Note, if doUpdateRegardlessOfMode is False then something else better be calling this with doUpdateRegardlessOfMode = True so that the state is updating!
#             if doUpdateRegardlessOfMode or SetTokenDataDict[key].state == RebalancingState.Rebalance:
#                 threadName = Libraries.loggingConfig.GetLoggerName("Set_" + key)
#                 Libraries.loggingConfig.RegisterLogger(threadName, Directory_Logs_Set)
#
#                 t = threading.Thread(
#                     name=threadName,
#                     target=CalculateSetsAverageTimeBetweenPriceChanges,
#                     args=(SetTokenDataDict[key],))
#                 threads.append(t)
#                 t.start()
#
#                 updateCount += 1
#
#             else:
#                 pass
#                 # PrintAndLog("----------------- Not updating " + str(key) + " because doUpdateRegardlessOfMode == False and it's not currently in rebalance mode -----------------")
#
#         except (KeyboardInterrupt, SystemExit):
#             print('\nkeyboard interrupt caught')
#             print('\n...Program Stopped Manually!')
#             raise
#
#         except:
#             PrintAndLogError("exception when trying to UpdateAllSetTokenData " + str(key) + ": " + traceback.format_exc())
#             pass
#
#     for thread in threads:
#         thread.join()
#
#     return updateCount


def CalculateAllSetsAverageTimeBetweenPriceChanges():
    global SetTokenDataDict

    # Leaving this here as an option for the future, but I should never need to set this to True
    doUpdateRegardlessOfMode = False

    before = datetime.datetime.now()
    updateCount = 0
    for key in SetTokenDataDict:
        try:
            # Note, if doUpdateRegardlessOfMode is False then something else better be calling this with doUpdateRegardlessOfMode = True so that the state is updating!
            if doUpdateRegardlessOfMode or SetTokenDataDict[key].state == RebalancingState.Rebalance:
                threadName = Libraries.loggingConfig.GetLoggerName("Set_" + key)
                Libraries.loggingConfig.RegisterLogger(threadName, Libraries.defaults.Directory_Logs_Set)

                CalculateSetsAverageTimeBetweenPriceChanges(SetTokenDataDict[key])
                updateCount += 1

            else:
                pass
                # PrintAndLog("----------------- Not updating " + str(key) + " because doUpdateRegardlessOfMode == False and it's not currently in rebalance mode -----------------")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when trying to CalculateAllSetsAverageTimeBetweenPriceChanges " + str(key) + ": " + traceback.format_exc())
            pass

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("duration_s = " + str(duration_s))

    return updateCount


def SetNetwork(networkToSet):
    global Network
    global Contract_TransferProxy
    global Contract_RebalanceAuctionModule
    global Contract_SetCTokenBidderContract
    global Contract_WETH
    global Contract_USDC
    global Contract_DAI
    global Contract_WBTC
    global CTokenListSubstitutions

    from Exchanges.zrxV2 import Contract_WETH as Contract_WETH_zrxV2

    Network = networkToSet
    if Network == Libraries.network.Network.mainnet:
        Contract_WETH = Contract_WETH_zrxV2
        Contract_USDC = Libraries.nodes.Instance_Web3.toChecksumAddress("0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48")
        Contract_DAI = Libraries.nodes.Instance_Web3.toChecksumAddress("0x6b175474e89094c44da98b954eedeac495271d0f")
        Contract_WBTC = Libraries.nodes.Instance_Web3.toChecksumAddress("0x2260fac5e5542a773aa44fbcfedf7c193bc2c599")

        # List each compound asset pair we support.
        # By support I mean which token we are using the SetCTokenBidderContract with
        CTokenListSubstitutions.append((Contract_USDC, Exchanges.compound.Contract_cUSDC))
        CTokenListSubstitutions.append((Contract_DAI, Exchanges.compound.Contract_cDAI))

        if Libraries.defaults.Set_UseMainnetStagingContracts:
            # Deployed staging mainnet addresses: https://gist.github.com/richardliang/9be1f867217dbd5f851a32e11e71a5b8
            Contract_RebalanceAuctionModule = Libraries.nodes.Instance_Web3.toChecksumAddress("0x56dB0438B1341e81e4C6E62F875fC3607FD1b911")
            Contract_TransferProxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0x5a1E0Fd3Fc829E893d4A158CC32ae68b0C0B92C2")
            Contract_SetCTokenBidderContract = Libraries.nodes.Instance_Web3.toChecksumAddress('0x47b560a4a34f8383b94f2c9212ccf6568d12e492')

        else:
            # Deployed mainnet addresses: https://github.com/SetProtocol/set-protocol-contracts/wiki/Deployed-Addresses-(MainNet---Production)
            Contract_RebalanceAuctionModule = Libraries.nodes.Instance_Web3.toChecksumAddress("0xe23FB31dD2edacEbF7d92720358bB92445F47fDB")
            Contract_TransferProxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0x882d80D3a191859d64477eb78Cca46599307ec1C")
            Contract_SetCTokenBidderContract = Libraries.nodes.Instance_Web3.toChecksumAddress('0xdfe0f6d42433dcdd534bf736452db61335a72c28')

    elif Network == Libraries.network.Network.kovanTestnet:
        Contract_RebalanceAuctionModule = Libraries.nodes.Instance_Web3.toChecksumAddress("0xea510e982c92620a19475f8dc777baaa3c2a00f5")
        Contract_TransferProxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0x61d264865756751392C0f00357Cc26ea70D98E3B")

        Contract_WETH = Libraries.nodes.Instance_Web3.toChecksumAddress("0x8a18c7034acefd1748199a58683ee5f22e2d4e45")
        Contract_USDC = Libraries.nodes.Instance_Web3.toChecksumAddress("0x15758350decea0e5a96cfe9024e3f352d039905a")

    else:
        raise Exception("Network not yet implemented")

    PrintAndLog("Now using " + str(Network.value))
    PrintAndLog("Contract_TransferProxy set to: " + str(Contract_TransferProxy))
    PrintAndLog("Contract_RebalanceAuctionModule set to: " + str(Contract_RebalanceAuctionModule))
    PrintAndLog("Contract_SetCTokenBidderContract set to: " + str(Contract_SetCTokenBidderContract))
    PrintAndLog("Contract_WETH set to: " + str(Contract_WETH))
    PrintAndLog("Contract_USDC set to: " + str(Contract_USDC))
    PrintAndLog("Contract_DAI set to: " + str(Contract_DAI))
    PrintAndLog("Contract_WBTC set to: " + str(Contract_WBTC))


def API_GetRebalanceState(setTokenContract, resultDict=None, resultKey=None, lock_resultDict=None):
    import Contracts.contracts
    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)
    # PrintAndLog("API_GetRebalanceState with set name = " + str(dictKey) + ", setTokenContract = " + str(setTokenContract))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_SetToken.encodeABI('rebalanceState', kwargs={}),
                "to": setTokenContract
            },
        ]
    }
    # PrintAndLog("API_GetRebalanceState payload = " + str(payload))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetRebalanceState jData = " + str(jData) + " for " + str(dictKey))
        stateNumber = Libraries.core.ConvertHexToInt(jData['result'])
        # PrintAndLog("API_GetRebalanceState stateNumber = " + str(stateNumber) + " for " + str(dictKey))
        stateObject = RebalancingState(stateNumber)
        # PrintAndLog("API_GetRebalanceState stateObject = " + str(stateObject) + " for " + str(dictKey))
        Libraries.utils.ConsiderSettingResultDict(stateObject, resultDict, resultKey, lock_resultDict)
        return stateObject

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetRebalanceState response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def API_GetManager(setTokenContract):
    import Contracts.contracts
    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)
    # PrintAndLog("API_GetManager with set name = " + str(dictKey) + ", setTokenContract = " + str(setTokenContract))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_SetToken.encodeABI('manager', kwargs={}),
                "to": setTokenContract
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("API_GetManager jData = " + str(jData) + " for " + str(dictKey))
        managerAddress = Libraries.core.GetAddressFromDataProperty(jData['result'])
        return managerAddress

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetManager response was not ok response = " + str(response))
        response.raise_for_status()


def API_DetermineFlow(setTokenContract, inputAmount_etherUnits, tokenAddress_1, tokenAddress_2):
    import Contracts.contracts
    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)
    PrintAndLog("API_DetermineFlow with set name = " + str(dictKey) + ", setTokenContract = " + str(setTokenContract))

    inputAmount_weiUnits = Libraries.core.ConvertEtherToWei(inputAmount_etherUnits, Libraries.core.Ether_Decimals)
    kwargs = {
        '_quantity': int(inputAmount_weiUnits)
    }
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_SetToken.encodeABI('getBidPrice', kwargs=kwargs),
                "to": setTokenContract
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        PrintAndLog("API_DetermineFlow jData = " + str(jData) + " for " + str(dictKey))
        # data = jData['result']
        firstArray, secondArray = GetAmountArraysFromGetBidPriceResult(jData['result'])
        # dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_DetermineFlow dataList = " + str(dataList))

        #     - [ ] SupaCoolD00d, [Jan 14, 2020 at 12:22:10 PM]:
        #         - [ ] So then how do I determine which is inflow and which is outflow?
        #     - [ ] Brian Weickmann, [Jan 14, 2020 at 12:22:31 PM]:
        #         - [ ] You match the combinedTokenArray to the outflow array
        #         - [ ] Whichever slot in there isn't 0 that's an outflow. And then do the same for the inflow array
        #     - [ ] SupaCoolD00d, [Jan 14, 2020 at 12:23:02 PM]:
        #         - [ ] Okay so you're saying I have to call getBidPrice in order to determine it.  And depending on how that array is formatted, that tells me which is which right?

        if IsFirstArrayInflow(firstArray, secondArray):
            return tokenAddress_1, tokenAddress_2
        elif IsSecondArrayInflow(firstArray, secondArray):
            return tokenAddress_2, tokenAddress_1
        else:
            message = "Failed to determine flow for " + str(dictKey) + ". Is there a new set with a new feature I'm not handling?"
            if Libraries.executeOnInterval.IsTimeToExecute(message, 1200):
                Libraries.core.API_PostOperatorNotification(message)

            raise Exception(message)

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_DetermineFlow response was not ok response = " + str(response))
        response.raise_for_status()


def IsFirstArrayInflow(firstArray, secondArray):
    if Libraries.core.ConvertHexToInt(secondArray[0]) == 0 and \
            Libraries.core.ConvertHexToInt(firstArray[1]) == 0 and \
            Libraries.core.ConvertHexToInt(firstArray[0]) != 0 and \
            Libraries.core.ConvertHexToInt(secondArray[1]) != 0:
        return True
    else:
        return False


def IsSecondArrayInflow(firstArray, secondArray):
    if Libraries.core.ConvertHexToInt(firstArray[0]) == 0 and \
            Libraries.core.ConvertHexToInt(secondArray[1]) == 0 and \
            Libraries.core.ConvertHexToInt(secondArray[0]) != 0 and \
            Libraries.core.ConvertHexToInt(firstArray[1]) != 0:
        return True
    else:
        return False


def GetAmountArraysFromGetBidPriceResult(result):
    dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(result), Libraries.core.LengthOfDataProperty)
    # PrintAndLog("GetAmountArraysFromGetBidPriceResult dataList = " + str(dataList))
    firstArray = [dataList[3], dataList[4]]
    secondArray = [dataList[6], dataList[7]]
    # PrintAndLog("GetAmountArraysFromGetBidPriceResult firstArray = " + str(firstArray))
    # PrintAndLog("GetAmountArraysFromGetBidPriceResult secondArray = " + str(secondArray))
    return firstArray, secondArray


def API_GetBidPrice(setTokenContract, quantity_etherUnits, quoteTokensTradeDirection, decimals_outflowToken, decimals_inflowToken, blockNumber_int=None):
    import Contracts.contracts
    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)
    # PrintAndLog("API_GetBidPrice with set name = " + str(dictKey) + ", setTokenContract = " + str(setTokenContract) + ", quantity_etherUnits = " + str(quantity_etherUnits))

    quantity_weiUnits = Libraries.core.ConvertEtherToWei(quantity_etherUnits, Libraries.core.Ether_Decimals)
    kwargs = {
        '_quantity': int(quantity_weiUnits)
    }
    # PrintAndLog("kwargs = " + str(kwargs))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_SetToken.encodeABI('getBidPrice', kwargs=kwargs),
                "to": setTokenContract
            },
        ]
    }

    # PrintAndLog("payload = " + str(payload))
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, True, False, False, True, blockNumber_int)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetBidPrice jData = " + str(jData) + " for " + str(dictKey))
        price, side, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits = \
            ParseResponseData_GetBidPrice(jData['result'], quoteTokensTradeDirection, decimals_outflowToken, decimals_inflowToken)
        return price, side, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetBidPrice response was not ok response = " + str(response))
        response.raise_for_status()


def ParseResponseData_GetBidPrice(result, quoteTokensTradeDirection, decimals_outflowToken, decimals_inflowToken):
    dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(result), Libraries.core.LengthOfDataProperty)
    firstArray, secondArray = GetAmountArraysFromGetBidPriceResult(result)

    # PrintAndLog("ParseResponseData_GetBidPrice: firstArray = " + str(firstArray))
    # PrintAndLog("ParseResponseData_GetBidPrice: secondArray = " + str(secondArray))
    # PrintAndLog("ParseResponseData_GetBidPrice: decimals_outflowToken = " + str(decimals_outflowToken))
    # PrintAndLog("ParseResponseData_GetBidPrice: decimals_inflowToken = " + str(decimals_inflowToken))

    # This logic is tricky
    # inflow is always the first array
    # outflow is always the second array
    # But each array has 2 values in it.  The one we want is always non zero.  The one that's zero is the one we do not want
    # And they alternate.  So if index 0 of the first array is zero, then index 1 of the second array will be zero. And visa versa
    #                      So if index 1 of the first array is zero, hen index 0 of the second array will be zero.
    # The non zero one will be the value we care about
    if IsFirstArrayInflow(firstArray, secondArray):
        # PrintAndLog("First part of if statement")
        inflowTokenQuantity_weiUnits = Libraries.core.ConvertHexToInt(firstArray[0])
        outflowTokenQuantity_weiUnits = Libraries.core.ConvertHexToInt(secondArray[1])
    elif IsSecondArrayInflow(firstArray, secondArray):
        # PrintAndLog("Second part of if statement")
        inflowTokenQuantity_weiUnits = Libraries.core.ConvertHexToInt(firstArray[1])
        outflowTokenQuantity_weiUnits = Libraries.core.ConvertHexToInt(secondArray[0])
    else:
        message = "Failed to determine flow for result =" + str(
            result) + ". Is there a new set with a new feature I'm not handling? Or is this rebalance not yet tradeable?"
        if Libraries.executeOnInterval.IsTimeToExecute(message, 1200):
            Libraries.core.API_PostOperatorNotification(message)

        raise Exception(message)

    # PrintAndLog("ParseResponseData_GetBidPrice: inflowTokenQuantity_weiUnits = " + str(inflowTokenQuantity_weiUnits))
    # PrintAndLog("ParseResponseData_GetBidPrice: outflowTokenQuantity_weiUnits = " + str(outflowTokenQuantity_weiUnits))

    # Which ever array has a non-zero number at the zero index, is inflow.  The other is outflow

    # getBidPrice function returns two arrays:  inflowUnitArray, outflowUnitArray
    # PrintAndLog("dataList = " + str(dataList))

    # inflowTokenQuantity_weiUnits = Libraries.core.ConvertHexToInt(dataList[4])  # This works for moving_averages strategy but not buy_the_dip... why?
    # outflowTokenQuantity_weiUnits = Libraries.core.ConvertHexToInt(dataList[6])  # This works for moving_averages strategy but not buy_the_dip... why?
    # inflowTokenQuantity_weiUnits = Libraries.core.ConvertHexToInt(dataList[7])  # This works for buy_the_dip strategy but not moving_averages  ... why?
    # outflowTokenQuantity_weiUnits = Libraries.core.ConvertHexToInt(dataList[3])  # This works for buy_the_dip strategy but not moving_averages  ... why?

    # PrintAndLog("inflowTokenQuantity_weiUnits = " + str(inflowTokenQuantity_weiUnits))
    # PrintAndLog("outflowTokenQuantity_weiUnits = " + str(outflowTokenQuantity_weiUnits))
    inflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(inflowTokenQuantity_weiUnits, decimals_inflowToken)
    outflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(outflowTokenQuantity_weiUnits, decimals_outflowToken)
    # PrintAndLog("inflowTokenQuantity_etherUnits = " + str(inflowTokenQuantity_etherUnits))
    # PrintAndLog("outflowTokenQuantity_etherUnits = " + str(outflowTokenQuantity_etherUnits))

    side = None
    price = None
    # TODO, this is a hack and needs removed for when I enable trading any token for any token
    # TODO, setting this based on WETH for now. So buying and selling tokens where WETH is the quote token
    if quoteTokensTradeDirection == TokenDirection.InflowToken:
        price = inflowTokenQuantity_etherUnits / outflowTokenQuantity_etherUnits
        side = 'buy'
    elif quoteTokensTradeDirection == TokenDirection.OutflowToken:
        price = outflowTokenQuantity_etherUnits / inflowTokenQuantity_etherUnits
        side = 'sell'
    else:
        raise Exception("Not a valid TokenDirection")

    return price, side, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits


def API_GetCombinedTokenArray(setTokenContract):
    import Contracts.contracts
    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)
    # PrintAndLog("API_GetCombinedTokenArray with set name = " + str(dictKey) + ", setTokenContract = " + str(setTokenContract))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_SetToken.encodeABI('getCombinedTokenArray', kwargs={}),
                "to": setTokenContract
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetCombinedTokenArray jData = " + str(jData) + " for " + str(dictKey))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        outflowTokenAddress, inflowTokenAddress = ParseResponseData_GetCombinedTokenArray(dataList)
        return outflowTokenAddress, inflowTokenAddress

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetCombinedTokenArray response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def ParseResponseData_GetCombinedTokenArray(dataList):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))
    outflowTokenAddress = Libraries.core.GetAddressFromDataProperty(dataList[0])
    inflowTokenAddress = Libraries.core.GetAddressFromDataProperty(dataList[1])
    return outflowTokenAddress, inflowTokenAddress


def API_GetBiddingParameters(setTokenContract, resultDict=None, resultKey=None, lock_resultDict=None):
    import Contracts.contracts
    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)
    PrintAndLog("API_GetBiddingParameters with set name = " + str(dictKey) + ", setTokenContract = " + str(setTokenContract))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_SetToken.encodeABI('getBiddingParameters', kwargs={}),
                "to": setTokenContract
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetBiddingParameters jData = " + str(jData) + " for " + str(dictKey))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        minBidSize, remainingCurrentUnits = ParseResponseData_GetBiddingParameters(dataList)
        returnValue = (int(minBidSize), int(remainingCurrentUnits))
        Libraries.utils.ConsiderSettingResultDict(returnValue, resultDict, resultKey, lock_resultDict)
        return returnValue

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetBiddingParameters response was not ok response = " + str(response))
        response.raise_for_status()


def ParseResponseData_GetBiddingParameters(dataList):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))
    # minimum bid size and remaining current units
    minBidSize = float(Libraries.core.ConvertHexToInt(Libraries.core.GetAddressFromDataProperty(dataList[0])))
    remainingCurrentUnits = float(Libraries.core.ConvertHexToInt(Libraries.core.GetAddressFromDataProperty(dataList[1])))
    # PrintAndLog("minBidSize = " + str(minBidSize))
    # PrintAndLog("remainingCurrentUnits = " + str(remainingCurrentUnits))
    return minBidSize, remainingCurrentUnits


def ParseResponseData_GetAddressAndBidPriceArray(dataList):
    # [
    #   '0000000000000000000000000000000000000000000000000000000000000060',
    #   '00000000000000000000000000000000000000000000000000000000000000c0',
    #   '0000000000000000000000000000000000000000000000000000000000000120',
    #   '0000000000000000000000000000000000000000000000000000000000000002',
    #   '000000000000000000000000a0b86991c6218b36c1d19d4a2e9eb0ce3606eb48',
    #   '000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc2',
    #   '0000000000000000000000000000000000000000000000000000000000000002',
    #   '0000000000000000000000000000000000000000000000000000000000000000',
    #   '000000000000000000000000000000000000000000000000069bf419c61fe7f7',
    #   '0000000000000000000000000000000000000000000000000000000000000002',
    #   '0000000000000000000000000000000000000000000000000000000005fdbbb1',
    #   '0000000000000000000000000000000000000000000000000000000000000000'
    # ]
    dataList.pop(0)
    dataList.pop(0)
    dataList.pop(0)
    dataList.pop(0)

    address_A = Libraries.core.GetAddressFromDataProperty(dataList[0])
    address_B = Libraries.core.GetAddressFromDataProperty(dataList[1])
    # inflowUnitsArray
    inflowUnitsArray_A = Libraries.core.ConvertHexToInt(dataList[3])
    inflowUnitsArray_B = Libraries.core.ConvertHexToInt(dataList[4])
    # outflowUnitsArray
    outflowUnitsArray_A = Libraries.core.ConvertHexToInt(dataList[6])
    outflowUnitsArray_B = Libraries.core.ConvertHexToInt(dataList[7])

    inflowToken = None
    outflowToken = None
    inflowTokenQuantity_weiUnits = None
    outflowTokenQuantity_weiUnits = None

    if inflowUnitsArray_A == 0 and outflowUnitsArray_B == 0:
        inflowToken = address_B
        outflowToken = address_A
        inflowTokenQuantity_weiUnits = inflowUnitsArray_B
        outflowTokenQuantity_weiUnits = outflowUnitsArray_A
    elif inflowUnitsArray_B == 0 and outflowUnitsArray_A == 0:
        inflowToken = address_A
        outflowToken = address_B
        inflowTokenQuantity_weiUnits = inflowUnitsArray_A
        outflowTokenQuantity_weiUnits = outflowUnitsArray_B
    else:
        raise Exception("ParseResponseData_GetAddressAndBidPriceArray: invalid response")

    # PrintAndLog_FuncNameHeader("inflowToken = " + str(inflowToken) + ": " + str(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(inflowToken)))
    # PrintAndLog_FuncNameHeader("outflowToken = " + str(outflowToken) + ": " + str(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(outflowToken)))
    # PrintAndLog_FuncNameHeader("inflowTokenQuantity_weiUnits = " + str(inflowTokenQuantity_weiUnits))
    # PrintAndLog_FuncNameHeader("outflowTokenQuantity_weiUnits = " + str(outflowTokenQuantity_weiUnits))
    return inflowToken, outflowToken, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits


def API_GetAddressAndBidPriceArray(setTokenContract, inputAmount_etherUnits, blockNumber_int=None):
    #     /*
    #      * Get token inflows and outflows and combined token array denominated in underlying required
    #      * for bid for a given rebalancing Set token.
    #      *
    #      * @param _rebalancingSetToken    The rebalancing Set Token instance
    #      * @param _quantity               The amount of currentSet to be rebalanced
    #      * @return combinedTokenArray     Array of token addresses
    #      * @return inflowUnitsArray       Array of amount of tokens inserted into system in bid
    #      * @return outflowUnitsArray      Array of amount of tokens returned from system in bid
    #      */
    #     function getAddressAndBidPriceArray(
    #         IRebalancingSetToken _rebalancingSetToken,
    #         uint256 _quantity
    #     )
    #         external
    #         view
    #         returns (address[] memory, uint256[] memory, uint256[] memory)
    #     {
    import Contracts.contracts
    global Contract_SetCTokenBidderContract

    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)
    PrintAndLog("API_GetAddressAndBidPriceArray with set name = " + str(dictKey) + ", setTokenContract = " + str(setTokenContract))

    inputAmount_weiUnits = Libraries.core.ConvertEtherToWei(inputAmount_etherUnits, Libraries.core.Ether_Decimals)

    kwargs = {
        '_rebalancingSetToken': str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenContract)),
        '_quantity': int(inputAmount_weiUnits),
    }

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_Set_CTokenBidderContract.encodeABI('getAddressAndBidPriceArray', kwargs=kwargs),
                "to": Contract_SetCTokenBidderContract
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds,
                                                    None, True, False, False, True, blockNumber_int)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog("API_GetAddressAndBidPriceArray jData = " + str(jData) + " for " + str(dictKey))
        data = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(data), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("API_GetAddressAndBidPriceArray dataList = " + str(dataList))
        inflowToken, outflowToken, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits = ParseResponseData_GetAddressAndBidPriceArray(dataList)
        return inflowToken, outflowToken, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetAddressAndBidPriceArray response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetProposalPeriod(setTokenContract):
    return API_GetProperty_SetToken(setTokenContract, 'proposalPeriod')


def API_GetProposalStartTime(setTokenContract):
    return API_GetProperty_SetToken(setTokenContract, 'proposalStartTime')


def API_GetProperty_SetToken(setTokenContract, propertyName):
    import Contracts.contracts
    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)
    # PrintAndLog("API_GetProperty_SetToken with set name = " + str(dictKey) + ", setTokenContract = " + str(setTokenContract))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_SetToken.encodeABI(propertyName, kwargs={}),
                "to": setTokenContract
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        result = Libraries.core.ConvertHexToInt(jData['result'])
        return result

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetProperty_SetToken response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def Trade(fromAddress, fromPrivateKey, setKey, inflowTokensToSpend_weiUnits, gas=None, gasPrice=None, nonce=None):
    global SetTokenDataDict

    # I have to convert the inflowTokensToSpend_weiUnits to 'shares' which is Set protocol's unit of measure for trade
    sharesQuantityForTrade_weiUnits = SetTokenDataDict[setKey].GetSharesQuantityForSetTrade_GivenInflowTokenQuantityToSpend_WeiUnits(inflowTokensToSpend_weiUnits)
    PrintAndLog_FuncNameHeader("inflowTokensToSpend_weiUnits = " + str(
        inflowTokensToSpend_weiUnits) + ", which is equivalent to sharesQuantityForTrade_weiUnits = " + str(sharesQuantityForTrade_weiUnits))
    return BidAndWithdraw(fromAddress, fromPrivateKey, setKey, sharesQuantityForTrade_weiUnits, gas, gasPrice, nonce)


def BidAndWithdraw(fromAddress, fromPrivateKey, setKey, sharesQuantityForTrade_weiUnits, gas=None, gasPrice=None, nonce=None):
    global Contract_RebalanceAuctionModule
    global Contract_SetCTokenBidderContract
    global SetTokenDataDict

    setTokenData = SetTokenDataDict[setKey]
    setTokenContract = setTokenData.setAddress

    #     function bidAndWithdraw(
    #         address _rebalancingSetToken,
    #         uint256 _quantity,
    #         bool _allowPartialFill
    #     )
    dictKey = GetSetKey_MatchingSetAddress(setTokenContract)

    PrintAndLog_FuncNameHeader("set name = " + str(dictKey) + ", setTokenContract = " + str(
        setTokenContract) + ", sharesQuantityForTrade_weiUnits = " + str(sharesQuantityForTrade_weiUnits))
    allowPartialFill = True

    params = 'address,uint256,bool'
    PrintAndLog_FuncNameHeader("params = " + str(params))
    method_signature = Web3.sha3(text=f"bidAndWithdraw({params})")[0:4]
    # PrintAndLog_FuncNameHeader("BidAndWithdraw method_signature = " + str(method_signature))
    method_parameters = encode_single(f"({params})",
                                      [str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenContract)), int(sharesQuantityForTrade_weiUnits), allowPartialFill])
    # PrintAndLog_FuncNameHeader("BidAndWithdraw method_parameters = " + str(method_parameters))
    data_hex = '0x' + (method_signature + method_parameters).hex()
    # PrintAndLog_FuncNameHeader("data_hex: " + str(data_hex))

    if not gas:
        gas = Libraries.gasStation.GetGasLimit_Trade_Set()

    if not gasPrice:
        gasPrice = Libraries.gasStation.GetGasPrice_Cheap()

    transactionId = API_SendEther_ToContract(Libraries.nodes.Instance_Web3.toChecksumAddress(fromAddress), fromPrivateKey,
                                             setTokenData.DetermineRebalanceAuctionModule(), 0, gas, gasPrice, data_hex,
                                             TransactionType.trade, nonce, True)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "BidAndWithdraw " + str(dictKey) + " transactionId: " + transactionId
        PrintAndLog_FuncNameHeader(message)
    else:
        PrintAndLog_FuncNameHeader("Failed to BidAndWithdraw " + str(dictKey) + ", no transactionId")

    return transactionId


# Monitor the Set protocol rebalance state and send a notification if the state changes
def MonitorSetRebalanceStates():
    global StateResultDict
    global Lock_StateResultDict
    global SetTokenDataDict

    try:
        Lock_StateResultDict.acquire()
        try:
            nonDefaultStateCount = 0
            for key in SetTokenDataDict:
                try:
                    setTokenContract = SetTokenDataDict[key].setAddress
                    state = API_GetRebalanceState(setTokenContract)

                    # Set previousState
                    previousState = "Unknown"
                    if key in StateResultDict:
                        previousState = StateResultDict[key]

                    # If the state does not match previous state
                    PrintAndLog("Comparing state = " + str(state) + " with previousState = " + str(previousState))
                    if state != previousState:
                        # State changed!
                        message = "Set protocol state monitor: " + str(key) + "'s state has changed! state = " + str(state) + " with previousState = " + str(
                            previousState)
                        PrintAndLog(message)

                        # Only call API_PostOperatorNotification when there's a rebalance
                        if state == RebalancingState.Proposal or state == RebalancingState.Rebalance:
                            Libraries.core.API_PostOperatorNotification(message)

                            # TODO, only go further if it's in rebalance mode once I thoroughly test this phone call thing ot make sure it's working properly

                            # If the set has gone to rebalance mode and the market cap exceeds our threshold
                            if state == RebalancingState.Rebalance and SetTokenDataDict[key].marketCap > Libraries.defaults.SetPhoneCallAlertMarketCapThreshold_usd:
                                # Determine if I want to make a phone call notification regarding this set rebalance
                                # But this call will fail because this information hasn't been obtained yet. I need to wait like 1 or 2 blocks to be safe.
                                # So what I should do here as a work around is start a thread, then sleep for like 60 seconds, then run the following code
                                t = threading.Thread(target=ConsiderSoundingDelayedAlarmTellingMeASetRebalanceIsOccurring, args=(key, state))
                                t.start()

                    else:
                        # State has not changed, so don't need to do anything
                        message = "Set protocol state monitor: " + str(key) + "'s state has not changed. state = " + str(state) + " with previousState = " + str(
                            previousState)
                        PrintAndLog(message)

                    # Store the new state
                    StateResultDict[key] = state

                    if state != RebalancingState.Default:
                        nonDefaultStateCount += 1

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    PrintAndLogError("exception = " + traceback.format_exc())
                    pass

            # PrintAndLog("StateResultDict = " + str(StateResultDict))
            # PrintAndLog("nonDefaultStateCount = " + str(nonDefaultStateCount))

        finally:
            Lock_StateResultDict.release()

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception = " + traceback.format_exc())
        return Libraries.gasStation.GasPrice_StaleOrderFromOrderBook_Min


def API_Propose(fromAddress, fromPrivateKey, setTokenManagerAddress, setTokenAddress, gasPrice=None):
    import Contracts.contracts

    kwargs = {
        '_rebalancingSetTokenAddress': Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenAddress),
    }

    PrintAndLog("API_Propose: setTokenManagerAddress " + str(setTokenManagerAddress))
    data_hex = Contracts.contracts.Contract_Set_BTCDaiRebalancingManager.encodeABI('propose', kwargs=kwargs)
    toAddress = setTokenManagerAddress
    estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    PrintAndLog("API_Propose: estimatedGas = " + str(estimatedGas))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                             estimatedGas, gasPrice, data_hex, TransactionType.other)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "API_Propose: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to API_Propose: no transactionId")

    return transactionId


def API_InitialPropose(fromAddress, fromPrivateKey, setTokenManagerAddress, gasPrice=None):
    import Contracts.contracts

    PrintAndLog("API_InitialPropose to setTokenManagerAddress " + str(setTokenManagerAddress))
    data_hex = Contracts.contracts.Contract_Set_MACOStrategyManager.encodeABI('initialPropose', kwargs={})
    toAddress = setTokenManagerAddress
    estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    PrintAndLog("API_InitialPropose: estimatedGas = " + str(estimatedGas))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                             estimatedGas, gasPrice, data_hex, TransactionType.other)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "API_InitialPropose: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to API_InitialPropose: no transactionId")

    return transactionId


def API_ConfirmPropose(fromAddress, fromPrivateKey, setTokenManagerAddress, gasPrice=None):
    import Contracts.contracts

    PrintAndLog("API_ConfirmPropose to setTokenManagerAddress " + str(setTokenManagerAddress))
    data_hex = Contracts.contracts.Contract_Set_MACOStrategyManager.encodeABI('confirmPropose', kwargs={})
    toAddress = setTokenManagerAddress
    estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    PrintAndLog("API_ConfirmPropose: estimatedGas = " + str(estimatedGas))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                             estimatedGas, gasPrice, data_hex, TransactionType.other)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "API_ConfirmPropose: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to API_ConfirmPropose: no transactionId")

    return transactionId


def API_StartRebalance(fromAddress, fromPrivateKey, setTokenContract, gas=None, gasPrice=None):
    import Contracts.contracts

    PrintAndLog("API_StartRebalance to " + str(setTokenContract))
    data_hex = Contracts.contracts.Contract_SetToken.encodeABI('startRebalance', kwargs={})
    toAddress = setTokenContract
    # estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    # PrintAndLog("API_StartRebalance: estimatedGas = " + str(estimatedGas))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.other)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "API_StartRebalance: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to API_StartRebalance: no transactionId")

    return transactionId


def API_SettleRebalance(fromAddress, fromPrivateKey, setTokenContract, gas=None, gasPrice=None):
    import Contracts.contracts

    PrintAndLog("API_SettleRebalance to " + str(setTokenContract))
    data_hex = Contracts.contracts.Contract_SetToken.encodeABI('settleRebalance', kwargs={})
    toAddress = setTokenContract
    # estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    # PrintAndLog("API_SettleRebalance: estimatedGas = " + str(estimatedGas))

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                             gas, gasPrice, data_hex, TransactionType.other)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "API_SettleRebalance: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to API_SettleRebalance: no transactionId")

    return transactionId


def API_UpdateAllocation(fromAddress, fromPrivateKey, setTokenManagerAddress, setTokenContract, newAllocationPercentage,
                         gasPrice=None, nonce=None):
    import Contracts.contracts
    PrintAndLog("API_UpdateAllocation to setTokenManagerAddress " + str(setTokenManagerAddress))

    #   {
    #     "constant": false,
    #     "inputs": [
    #       {
    #         "name": "_tradingPool",
    #         "type": "address"
    #       },
    #       {
    #         "name": "_newAllocation",
    #         "type": "uint256"
    #       },
    #       {
    #         "name": "_liquidatorData",
    #         "type": "bytes"
    #       }
    #     ],
    #     "name": "updateAllocation",
    #     "outputs": [
    #
    #     ],
    #     "payable": false,
    #     "stateMutability": "nonpayable",
    #     "type": "function"
    #   },

    if newAllocationPercentage < 0 or newAllocationPercentage > 1.0:
        raise Exception("newAllocationPercentage was not within proper boundaries. Must in range (0 - 1.0)")

    newAllocation_weiUnits = Libraries.core.ConvertEtherToWei(newAllocationPercentage, Libraries.core.Ether_Decimals)

    kwargs = {
        '_tradingPool': str(Libraries.nodes.Instance_Web3.toChecksumAddress(setTokenContract)),
        '_newAllocation': int(newAllocation_weiUnits),
        '_liquidatorData': Web3.toBytes(hexstr=str('0x0')),
    }
    PrintAndLog("API_UpdateAllocation: kwargs = " + str(kwargs))

    data_hex = Contracts.contracts.Contract_Set_SocialTradingManager.encodeABI('updateAllocation', kwargs=kwargs)
    toAddress = setTokenManagerAddress
    estimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data_hex)
    PrintAndLog("API_UpdateAllocation: estimatedGas = " + str(estimatedGas))
    # estimatedGas = 3500000

    transactionId = API_SendEther_ToContract(fromAddress, fromPrivateKey, toAddress, 0,
                                             estimatedGas, gasPrice, data_hex, TransactionType.other, nonce)
    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    if transactionId:
        message = "API_UpdateAllocation: transactionId: " + transactionId
        PrintAndLog(message)
    else:
        PrintAndLog("Failed to API_UpdateAllocation: no transactionId")

    return transactionId


def IsTokenAddressInComponents(tokenAddress, componentsList):
    # [
    #   {
    #     'address': '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
    #     'id': 'usdc',
    #     'name': 'USD',
    #     'quantity': '0',
    #     'symbol': 'USDC',
    #     'units': '0'
    #   },
    #   {
    #     'address': '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2',
    #     'id': 'wrapped_eth',
    #     'name': 'Wrapped Eth',
    #     'quantity': '1.083727',
    #     'symbol': 'WETH',
    #     'units': '1000000'
    #   }
    # ]
    for component in componentsList:
        if tokenAddress.lower() == component['address'].lower():
            return True

    return False


def GetListOfTradableTokens(ninjaObject):
    global SetTokenDataDict

    # Make a list of all unique tokenAddresses in all token sets
    tokenAddressList = []
    for setKey in SetTokenDataDict:
        setTokenData = SetTokenDataDict[setKey]

        if ninjaObject and not CanNinjaObjectTradeOnThisSetTokenData(ninjaObject, setTokenData):
            continue

        for component in setTokenData.tokenComponents:
            if component['address'].lower() not in tokenAddressList:
                tokenAddressList.append(component['address'].lower())

    localNinjaType = None
    if ninjaObject:
        localNinjaType = ninjaObject.ninjaType

    PrintAndLog("GetListOfTradableTokens: for localNinjaType " + str(localNinjaType) + ", tokenAddressList = " + str(tokenAddressList))
    return tokenAddressList


def DoesSetStrategyUseInitialProposeAndConfirmProposal(strategy):
    if strategy == TradingStrategy.InverseBenchmark or \
            strategy == TradingStrategy.MovingAverages:
        return True
    else:
        return False


# def CalculateSetsAverageTimeBetweenPriceChanges(setTokenData):
#     global PriceHistoryDataPointsDict
#
#     # NOTE, this function must be called in a background thread, it's incredibly time consuming to call
#     # This does not need to be called frequently.  Once very couple minutes should be fine
#
#     # I want to get a lot of blocks in case this rebalance doesn't change price very often
#     # My nodes don't tend to have 150 blocks worth of recent data.
#     # 30 blocks may be enough for most sets, but might not be enough for some of the really old sets whose price don't change often, so go bigger than that
#     # I wrote this function to try until it succeeds and it should be able to handle
#     # really large numbers (it's just a waste of time and network calls until it finds valid blocks from the node)
#     maxBlocks = 90
#     endBlock = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
#     startBlock = endBlock - maxBlocks
#
#     minRequiredSuccesses = 40
#     dontCare, useSetCTokenBidderContract = setTokenData.GetBaseTokenAddress()
#
#     # Batch a call to get all block data to make this go a bit quicker
#     blockNumberList = []
#     for blockNumber in range(startBlock, endBlock + 1):
#         blockNumberList.append(blockNumber)
#
#     PrintAndLog_FuncNameHeader("Calling API_GetBlock_Batched")
#     blockDataList = Libraries.core.API_GetBlock_Batched(blockNumberList)
#     PrintAndLog_FuncNameHeader("blockDataList of len " + str(len(blockDataList)) + " = " + str(blockDataList))
#
#     successCount = 0
#     resultList_price_usingCTokenBidderContract = []
#     resultList_price_usingAPI_GetBidPrice = []
#     resultList_timestampUnixEpoch_seconds = []
#     resultList_blockNumber = []
#     minValidBlocksOfDataBeforeWeStartRecordingData = 10
#     # Only start recording data when we get x valid blocks in a row. Let's make sure we don't start recording and then a node wipes out a block because it's old
#     index = 0
#     for blockNumber in range(startBlock, endBlock + 1):
#         blockData = blockDataList[index]
#         # try:
#         #     blockData = Libraries.core.API_GetBlock(blockNumber)
#         #
#         # except (KeyboardInterrupt, SystemExit):
#         #     print('\nkeyboard interrupt caught')
#         #     print('\n...Program Stopped Manually!')
#         #     raise
#
#         # except:
#         #     PrintAndLogError("exception = " + traceback.format_exc())
#         #     continue
#
#         try:
#             timestampUnixEpoch_seconds = Libraries.core.ConvertHexToInt(blockData['timestamp'])
#
#             # Multiply by the PricePrecisionMultiplier to get a higher price precision
#             quantityToUse = PricePrecisionMultiplier * setTokenData.minBidSize_etherUnits
#
#             # ********** Begin getting price via the cTokenBidderContract ********** #
#             price_usingCTokenBidderContract = None
#             if useSetCTokenBidderContract:
#
#                 setTokenData.inflowTokenAddress, setTokenData.outflowTokenAddress, inflowTokenQuantity_weiUnits, outflowTokenQuantity_weiUnits = \
#                     API_GetAddressAndBidPriceArray(setTokenData.setAddress, quantityToUse, blockNumber)
#
#                 # PricePrecisionMultiplier is being used so we get better precision
#                 inflowTokenQuantity_weiUnits /= PricePrecisionMultiplier
#                 outflowTokenQuantity_weiUnits /= PricePrecisionMultiplier
#
#                 inflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(inflowTokenQuantity_weiUnits, setTokenData.decimals_inflowToken)
#                 outflowTokenQuantity_etherUnits = Libraries.core.ConvertWeiToEther(outflowTokenQuantity_weiUnits, setTokenData.decimals_outflowToken)
#
#                 if setTokenData.quoteTokensTradeDirection == TokenDirection.InflowToken:
#                     price_usingCTokenBidderContract = inflowTokenQuantity_etherUnits / outflowTokenQuantity_etherUnits
#                 elif setTokenData.quoteTokensTradeDirection == TokenDirection.OutflowToken:
#                     price_usingCTokenBidderContract = outflowTokenQuantity_etherUnits / inflowTokenQuantity_etherUnits
#                 else:
#                     raise Exception("Not a valid TokenDirection")
#
#             # ********** End getting price via the cTokenBidderContract ********** #
#
#             # ********** Begin getting price via API_GetBidPrice ********** #
#             price_usingAPI_GetBidPrice, dontCare1, dontCare2, dontCare3 = API_GetBidPrice(
#                 setTokenData.setAddress, quantityToUse, setTokenData.quoteTokensTradeDirection,
#                 setTokenData.decimals_outflowToken, setTokenData.decimals_inflowToken, blockNumber)
#
#             # ********** End getting price via API_GetBidPrice ********** #
#
#             PrintAndLog_FuncNameHeader("At blockNumber " + str(blockNumber) + "-" + str(timestampUnixEpoch_seconds) + ", price_usingAPI_GetBidPrice = " + str(
#                 price_usingAPI_GetBidPrice) + ", price_usingCTokenBidderContract = " + str(price_usingCTokenBidderContract))
#
#             # If we've made it this far, we've found a valid block of data
#             # so let's compare successCount with minValidBlocksOfDataBeforeWeStartRecordingData to see if we can start recording data
#             if successCount >= minValidBlocksOfDataBeforeWeStartRecordingData:
#                 # If this is a new price we haven't seen before (AKA the price changed)
#                 # We only want to track time changes based on API_GetBidPrice
#                 # Do not track time changes based on usingCTokenBidderContract, just have this price tag along for the ride because we'll need it
#                 if price_usingAPI_GetBidPrice not in resultList_price_usingAPI_GetBidPrice:
#                     resultList_price_usingAPI_GetBidPrice.append(price_usingAPI_GetBidPrice)
#                     resultList_price_usingCTokenBidderContract.append(price_usingCTokenBidderContract)
#                     resultList_timestampUnixEpoch_seconds.append(timestampUnixEpoch_seconds)
#                     resultList_blockNumber.append(blockNumber)
#             else:
#                 pass
#                 # PrintAndLog_FuncNameHeader("Found valid data for block " + str(blockNumber) + " but we're not using it just because we want to see a string of "
#                 #                                                                "valid blocks before we accept this just in case the node wipes the "
#                 #                                                                "next block's data out within the next second or so")
#
#         except (KeyboardInterrupt, SystemExit):
#             print('\nkeyboard interrupt caught')
#             print('\n...Program Stopped Manually!')
#             raise
#
#         except:
#             PrintAndLogError("exception in CalculateSetsAverageTimeBetweenPriceChanges = " + traceback.format_exc())
#             continue
#
#         # Keep track of how many calls succeeded in case for some reason I'm not getting enough
#         successCount += 1
#         index += 1
#
#     # If we didn't get very many successes, we cannot use this data
#     if successCount < minRequiredSuccesses:
#         message = "We didn't get very many successful blocks. Is something wrong with my nodes? " + str(
#             setTokenData.setTokenKey) + " has only successCount = " + str(successCount)
#         Libraries.core.API_PostOperatorNotification(message)
#         raise Exception(message)
#
#     resultList_timeSincePreviousPriceChange = []
#     previousTimestamp = None
#     for index, timestamp in enumerate(resultList_timestampUnixEpoch_seconds):
#         if index != 0:
#             resultList_timeSincePreviousPriceChange.append(timestamp - previousTimestamp)
#
#         previousTimestamp = timestamp
#
#     # We need to remove some items from the lists. We must ignore the first/second data points before we can trust the data to predict the future with it
#     # PrintAndLog_FuncNameHeader("resultList_blockNumber before the pop = " + str(resultList_blockNumber))
#
#     # Pop the first item in these lists
#     resultList_price_usingAPI_GetBidPrice.pop(0)
#     resultList_price_usingCTokenBidderContract.pop(0)
#     resultList_blockNumber.pop(0)
#     resultList_timestampUnixEpoch_seconds.pop(0)
#
#     # Pop the first item in all these lists including resultList_timeSincePreviousPriceChange
#     resultList_price_usingAPI_GetBidPrice.pop(0)
#     resultList_price_usingCTokenBidderContract.pop(0)
#     resultList_blockNumber.pop(0)
#     resultList_timestampUnixEpoch_seconds.pop(0)
#     resultList_timeSincePreviousPriceChange.pop(0)
#
#     PrintAndLog_FuncNameHeader("resultList_blockNumber after the pop = " + str(resultList_blockNumber))
#     PrintAndLog_FuncNameHeader("resultList_price_usingCTokenBidderContract len = " + str(len(resultList_price_usingCTokenBidderContract)))
#     PrintAndLog_FuncNameHeader("resultList_price_usingAPI_GetBidPrice len = " + str(len(resultList_price_usingAPI_GetBidPrice)))
#     PrintAndLog_FuncNameHeader("resultList_blockNumber len = " + str(len(resultList_blockNumber)))
#     PrintAndLog_FuncNameHeader("resultList_timestampUnixEpoch_seconds len = " + str(len(resultList_timestampUnixEpoch_seconds)))
#     PrintAndLog_FuncNameHeader("resultList_timeSincePreviousPriceChange len = " + str(len(resultList_timeSincePreviousPriceChange)))
#     # resultList_timeSincePreviousPriceChange should have one less item than the other lists,
#     # so only remove the last item from this array so no need to remove the first item from it
#
#     PrintAndLog_FuncNameHeader("Price changes over this timespan after removing the first and last values")
#     sum_timeDifference = 0
#     priceHistoryDataPoints_toUse = []
#     for index, price in enumerate(resultList_price_usingAPI_GetBidPrice):
#         price_usingCTokenBidderContract = resultList_price_usingCTokenBidderContract[index]
#         price_usingAPI_GetBidPrice = resultList_price_usingAPI_GetBidPrice[index]
#         PrintAndLog_FuncNameHeader("At blockNumber " + str(resultList_blockNumber[index]) + "-" + str(
#             resultList_timestampUnixEpoch_seconds[index]) + ", price_usingAPI_GetBidPrice = " + str(
#             price_usingAPI_GetBidPrice) + ", price_usingCTokenBidderContract = " + str(
#             price_usingCTokenBidderContract) + ", time diff between this and previous price change = " + str(
#             round(resultList_timeSincePreviousPriceChange[index], 1)) + " seconds")
#
#         sum_timeDifference += resultList_timeSincePreviousPriceChange[index]
#
#         # Update the the array with the timestamp and the price that we care about
#         priceWeCareAbout = None
#         if useSetCTokenBidderContract:
#             priceWeCareAbout = price_usingCTokenBidderContract
#         else:
#             priceWeCareAbout = price_usingAPI_GetBidPrice
#
#         # We want the list to be ordered newest data first
#         priceHistoryDataPoints_toUse.insert(0, (resultList_timestampUnixEpoch_seconds[index], priceWeCareAbout))
#
#     averageTimeDifferenceBetweenPriceChanges_seconds = sum_timeDifference / len(resultList_timeSincePreviousPriceChange)
#     PrintAndLog_FuncNameHeader("averageTimeDifferenceBetweenPriceChanges_seconds = " + str(averageTimeDifferenceBetweenPriceChanges_seconds))
#     PrintAndLog_FuncNameHeader("priceHistoryDataPoints_toUse = " + str(priceHistoryDataPoints_toUse))
#
#     # Store the values we care about in the PriceHistoryDataPointsDict
#     value = (priceHistoryDataPoints_toUse, averageTimeDifferenceBetweenPriceChanges_seconds)
#     PriceHistoryDataPointsDict.SetValue(setTokenData.setTokenKey, value)


def CalculateSetsAverageTimeBetweenPriceChanges(setTokenData):
    global PriceHistoryDataPointsDict

    # NOTE, this function must be called in a background thread, it's incredibly time consuming to call
    # This does not need to be called frequently.  Once very couple minutes should be fine

    # I want to get a lot of blocks in case this rebalance doesn't change price very often
    # My nodes don't tend to have 150 blocks worth of recent data.
    # 30 blocks may be enough for most sets, but might not be enough for some of the really old sets whose price don't change often, so go bigger than that
    # I wrote this function to try until it succeeds and it should be able to handle
    # really large numbers (it's just a waste of time and network calls until it finds valid blocks from the node)
    maxBlocks = 90
    endBlock = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    startBlock = endBlock - maxBlocks

    minRequiredSuccesses = 40

    # Batch a call to get all block data to make this go a bit quicker
    blockNumberList = []
    for blockNumber in range(startBlock, endBlock + 1):
        blockNumberList.append(blockNumber)

    blockDataList = Libraries.core.API_GetBlock_Batched(blockNumberList)
    # PrintAndLog_FuncNameHeader("Called API_GetBlock_Batched blockDataList of len " + str(len(blockDataList)) + " = " + str(blockDataList))
    PrintAndLog_FuncNameHeader("Called API_GetBlock_Batched blockDataList of len " + str(len(blockDataList)))

    successCount = 0
    resultList_price_usingAPI_GetBidPrice = []
    resultList_timestampUnixEpoch_seconds = []
    resultList_blockNumber = []
    minValidBlocksOfDataBeforeWeStartRecordingData = 10
    # Only start recording data when we get x valid blocks in a row. Let's make sure we don't start recording and then a node wipes out a block because it's old
    index = 0
    for blockNumber in range(startBlock, endBlock + 1):
        blockData = blockDataList[index]
        try:
            timestampUnixEpoch_seconds = Libraries.core.ConvertHexToInt(blockData['timestamp'])

            # Multiply by the PricePrecisionMultiplier to get a higher price precision
            quantityToUse = PricePrecisionMultiplier * setTokenData.minBidSize_etherUnits

            price_usingAPI_GetBidPrice, dontCare1, dontCare2, dontCare3 = API_GetBidPrice(
                setTokenData.setAddress, quantityToUse, setTokenData.quoteTokensTradeDirection,
                setTokenData.decimals_outflowToken, setTokenData.decimals_inflowToken, blockNumber)

            PrintAndLog_FuncNameHeader("At blockNumber " + str(blockNumber) + "-" + str(timestampUnixEpoch_seconds) + ", price_usingAPI_GetBidPrice = " + str(
                price_usingAPI_GetBidPrice))

            # If we've made it this far, we've found a valid block of data
            # so let's compare successCount with minValidBlocksOfDataBeforeWeStartRecordingData to see if we can start recording data
            if successCount >= minValidBlocksOfDataBeforeWeStartRecordingData:
                # If this is a new price we haven't seen before (AKA the price changed)
                if price_usingAPI_GetBidPrice not in resultList_price_usingAPI_GetBidPrice:
                    resultList_price_usingAPI_GetBidPrice.append(price_usingAPI_GetBidPrice)
                    resultList_timestampUnixEpoch_seconds.append(timestampUnixEpoch_seconds)
                    resultList_blockNumber.append(blockNumber)
            else:
                pass
                # PrintAndLog_FuncNameHeader("Found valid data for block " + str(blockNumber) + " but we're not using it just because we want to see a string of "
                #                                                                "valid blocks before we accept this just in case the node wipes the "
                #                                                                "next block's data out within the next second or so")

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception in CalculateSetsAverageTimeBetweenPriceChanges = " + traceback.format_exc())
            continue

        # Keep track of how many calls succeeded in case for some reason I'm not getting enough
        successCount += 1
        index += 1

    # If we didn't get very many successes, we cannot use this data
    if successCount < minRequiredSuccesses:
        message = "We didn't get very many successful blocks. Is something wrong with my nodes? " + str(
            setTokenData.setTokenKey) + " has only successCount = " + str(successCount)
        Libraries.core.API_PostOperatorNotification(message)
        raise Exception(message)

    resultList_timeSincePreviousPriceChange = []
    previousTimestamp = None
    for index, timestamp in enumerate(resultList_timestampUnixEpoch_seconds):
        if index != 0:
            resultList_timeSincePreviousPriceChange.append(timestamp - previousTimestamp)

        previousTimestamp = timestamp

    # We need to remove some items from the lists. We must ignore the first/second data points before we can trust the data to predict the future with it
    # PrintAndLog_FuncNameHeader("resultList_blockNumber before the pop = " + str(resultList_blockNumber))

    # Pop the first item in these lists
    resultList_price_usingAPI_GetBidPrice.pop(0)
    resultList_blockNumber.pop(0)
    resultList_timestampUnixEpoch_seconds.pop(0)

    # Pop the first item in all these lists including resultList_timeSincePreviousPriceChange
    resultList_price_usingAPI_GetBidPrice.pop(0)
    resultList_blockNumber.pop(0)
    resultList_timestampUnixEpoch_seconds.pop(0)
    resultList_timeSincePreviousPriceChange.pop(0)

    PrintAndLog_FuncNameHeader("resultList_blockNumber after the pop = " + str(resultList_blockNumber))
    PrintAndLog_FuncNameHeader("resultList_price_usingAPI_GetBidPrice len = " + str(len(resultList_price_usingAPI_GetBidPrice)))
    PrintAndLog_FuncNameHeader("resultList_blockNumber len = " + str(len(resultList_blockNumber)))
    PrintAndLog_FuncNameHeader("resultList_timestampUnixEpoch_seconds len = " + str(len(resultList_timestampUnixEpoch_seconds)))
    PrintAndLog_FuncNameHeader("resultList_timeSincePreviousPriceChange len = " + str(len(resultList_timeSincePreviousPriceChange)))
    # resultList_timeSincePreviousPriceChange should have one less item than the other lists,
    # so only remove the last item from this array so no need to remove the first item from it

    PrintAndLog_FuncNameHeader("Price changes over this timespan after removing the first and last values")
    sum_timeDifference = 0
    priceHistoryDataPoints_toUse = []
    for index, price in enumerate(resultList_price_usingAPI_GetBidPrice):
        price_usingAPI_GetBidPrice = resultList_price_usingAPI_GetBidPrice[index]
        PrintAndLog_FuncNameHeader("At blockNumber " + str(resultList_blockNumber[index]) + "-" + str(
            resultList_timestampUnixEpoch_seconds[index]) + ", price_usingAPI_GetBidPrice = " + str(
            price_usingAPI_GetBidPrice) + ", time diff between this and previous price change = " + str(
            round(resultList_timeSincePreviousPriceChange[index], 1)) + " seconds")

        sum_timeDifference += resultList_timeSincePreviousPriceChange[index]

        # Update the the array with the timestamp and the price that we care about
        priceWeCareAbout = price_usingAPI_GetBidPrice

        # We want the list to be ordered newest data first
        priceHistoryDataPoints_toUse.insert(0, (resultList_timestampUnixEpoch_seconds[index], priceWeCareAbout))

    averageTimeDifferenceBetweenPriceChanges_seconds = sum_timeDifference / len(resultList_timeSincePreviousPriceChange)
    PrintAndLog_FuncNameHeader("averageTimeDifferenceBetweenPriceChanges_seconds = " + str(averageTimeDifferenceBetweenPriceChanges_seconds))
    PrintAndLog_FuncNameHeader("priceHistoryDataPoints_toUse = " + str(priceHistoryDataPoints_toUse))

    # Store the value we care about in the PriceHistoryDataPointsDict
    PriceHistoryDataPointsDict.SetValue(setTokenData.setTokenKey, averageTimeDifferenceBetweenPriceChanges_seconds)


def ConsiderSoundingDelayedAlarmTellingMeASetRebalanceIsOccurring(key, state):
    # SetTokenDataDict doesn't yet have the data we need to process required logic!
    # Sleep a few blocks before we consider making the phone call
    currentBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    blockNumberToMakePhoneCall = currentBlockNumber + 8
    # timeout after some large number of seconds
    before = datetime.datetime.now()
    timeout_seconds = 500
    while currentBlockNumber < blockNumberToMakePhoneCall:
        currentBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

        duration_seconds = (datetime.datetime.now() - before).total_seconds()

        if duration_seconds > timeout_seconds:
            message = "ConsiderSoundingDelayedAlarmTellingMeASetRebalanceIsOccurring: This loop timed out, this should never happen!"
            Libraries.core.API_PostOperatorNotification(message)
            PrintAndLogError(message)
            raise Exception(message)

        time.sleep(Libraries.core.AverageBlockTime_seconds)

    # Once we get here, we should be able to count on SetTokenDataDict having all the data we need to process the below logic

    tokenSymbolsString = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(SetTokenDataDict[key].inflowTokenAddress).upper() + " " + \
                         Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(SetTokenDataDict[key].outflowTokenAddress)
    message = "Hey baby, wanna make some money? " + str(key) + " is in " + str(state) + " mode with a market cap of " + str(
        SetTokenDataDict[key].marketCap) + ". Tokens are " + str(tokenSymbolsString)
    PrintAndLog(message)

    # Only consider making a phone call alert based on script name

    if Libraries.utils.ActiveScriptName == Libraries.utils.ScriptName.Arby:
        # Do not make a phone call alert if this script just started within the last few minutes
        # If I'm debugging, starting and stopping the script repeatedly, I do not want to get this phone call alert every time i restart the script
        if Libraries.utils.GetTimeSinceNinjaLaunched_seconds() < 120:
            Libraries.core.API_PostOperatorNotification(message, True, Libraries.core.OperatorNotificationKey.ArbyCallUsPhone)
        else:
            PrintAndLog_FuncNameHeader("Not making phone call alert because the script recently started")

    else:
        PrintAndLog_FuncNameHeader("Not making phone call alert because the Libraries.utils.ActiveScriptName isn't set to one we "
                                   "want to alert for. Libraries.utils.ActiveScriptName = " + str(Libraries.utils.ActiveScriptName))