import json
import threading
import time
import traceback
import jsonpickle
import requests
import websocket
from urllib.parse import urlencode

from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError
import Libraries.core
import Libraries.executeOnInterval
import Exchanges.zrxV4
import Libraries.signingUtils_0xv4
import Libraries.signingUtils_0xv3
from Libraries.orders import MyOrder_0xv3, MyOrder_0xv4
import Libraries.orderAggregator
import Libraries.utils

Host_Local = '127.0.0.1'
Host_Production = 'hidingbook.keeperdao.com'

Url_Base = Host_Local
# Url_Base = Host_Production

Port_http_Local = 3000
Port_ws_Local = 4000
Port_http_Production = 80
Port_ws_Production = 80

Port_http = Port_http_Local
Port_ws = Port_ws_Local

# TODO, make this into a config so I can change the IP address based on a config param
# https://hidingbook.keeperdao.com/api/v1/info
# wss://hidingbook.keeperdao.com/ws/

APIVersion = 1
# WSVersion = 1

HidingGameRfqOriginsAccount = '0xBd49A97300E10325c78D6b4EC864Af31623Bb5dD'

WebsocketClient = None

# The HTTP server expects the IP address to be injected by cloudflare.
# API calls coming from my python code do not add that, Cloudflare does.
# And in order to rate limit properly I need the IP address
# So when making calls locally, we have to fake an IP address because there's no Cloudflare to inject it
# And when we're on production, we do not want an IP address because Cloudflare adds it
Headers_Local = {
    'content-type': 'application/json',
    'Cf-Connecting-Ip': '82.5.151.34',
}

Headers_Production = {
    'content-type': 'application/json',
}

# Headers = Headers_Production
Headers = Headers_Local


def GetHeaders():
    return Headers


def GetUrl_Http():
    protocol = 'http'
    if Url_Base != Host_Local:
        protocol += 's'

    protocol += '://' + Url_Base
    if Port_http != 80:
        protocol += ':' + str(Port_http)

    protocol += '/api/v' + str(APIVersion) + '/'
    return protocol


def GetUrl_Ws():
    protocol = 'ws'
    if Url_Base != Host_Local:
        protocol += 's'

    # TODO add version to WS protocol
    protocol += '://' + Url_Base
    if Port_ws != 80:
        protocol += ':' + str(Port_ws)

    protocol += '/ws/'
    return protocol


def API_GetInfo():
    url = GetUrl_Http() + "info"
    PrintAndLog_FuncNameHeader("url = " + str(url))
    response = requests.get(url, headers=GetHeaders(), timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetListOfTokenAddresses():
    jData = API_GetInfo()
    tokenObjectList = jData['result']['tokenList']['tokens']
    tokenList = []
    for tokenObject in tokenObjectList:
        tokenList.append(tokenObject['address'].lower())

    return tokenList


def ConnectWebSocketClient(exchangeName):
    global WebsocketClient
    WebsocketClient = WebsocketClientObject(exchangeName)


class WebsocketClientObject:
    ws = None
    exchangeName = None

    def __init__(self, _exchangeName):
        self.exchangeName = _exchangeName

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        websocket.enableTrace(False)
        PrintAndLog_FuncNameHeader("GetUrl_Ws() = " + str(GetUrl_Ws()))
        websocket.http_proxy_host = GetUrl_Ws()
        self.ws = websocket.WebSocketApp(GetUrl_Ws(),
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)

        while True:
            try:
                # Subscribe to events
                Libraries.executeOnInterval.IsTimeToExecute("SubscribeToOrders", 0, SubscribeToOrders, True)

                Libraries.core.API_PostOperatorNotification("Connecting to HidingBook websockets")
                self.ws.run_forever(ping_interval=30, ping_timeout=10)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in HidingBook websockets = " + traceback.format_exc())

            PrintAndLog_FuncNameHeader("HidingBook websockets disconnected! Sleeping a bit then trying again")
            time.sleep(30)

    def on_message(self, ws, message):
        printMessage = "HidingBook: Websockets: data received: " + message[0:90] + "....." + message[-25:]
        PrintAndLog_FuncNameHeader(printMessage)
        # Periodically tell me that websockets are working
        if Libraries.executeOnInterval.IsTimeToExecute("HidingBook: Websockets: data received", 14400):
            Libraries.core.API_PostOperatorNotification(printMessage)

        try:
            jData = json.loads(message)
            # PrintAndLog_FuncNameHeader("HidingBook: Websockets: jData = " + str(jData))
            if 'orderData' in jData:
                Libraries.orderAggregator.HandleNewOrderDatas_0xv4([jData['orderData']], self.exchangeName, True)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            errorMessage = "exception when parsing message in WebsocketClientObject: " + traceback.format_exc() + ", message = " + str(message)
            PrintAndLog_FuncNameHeader(errorMessage)
            PrintAndLogError(errorMessage)

    def on_error(self, ws, error):
        PrintAndLogError(str(error))

    def on_close(self, ws):
        message = "HidingBook: Disconnected from websockets."
        PrintAndLogError(message)
        Libraries.core.API_PostOperatorNotification(message)


def SendTestMessage(message):
    global WebsocketClient

    # WebsocketClient.ws.send(jsonpickle.encode(subscriptionPayload))
    WebsocketClient.ws.send(message)


def SubscribeToOrders(doSleepFirst=True):
    global WebsocketClient

    # Give websockets time to connect
    # PrintAndLog_FuncNameHeader("testOrderDuration_s = " + str(testOrderDuration_s))

    # currentTime_s = time.time()
    # PrintAndLog_FuncNameHeader("currentTime_s = " + str(currentTime_s))
    if doSleepFirst:
        time.sleep(4)

    WebsocketClient.ws.send(jsonpickle.encode({'subscribe': 'order'}))


def Sign0xV4Order(publicAddress, privateKey, quoteToken, baseToken,
                  side, amount_baseToken_etherUnits, price, orderDuration_seconds,
                  doWriteOrderToLogsForTesting=False):
    amount_quoteToken_etherUnits = Libraries.core.ConvertTokensToEther(amount_baseToken_etherUnits, price)
    # Convert etherUnits to weiUnits
    amount_quoteToken_weiUnits = Libraries.core.ConvertEtherAmountToBaseAmount(amount_quoteToken_etherUnits, Libraries.core.GetDecimalsForTokenContract(quoteToken, True))
    amount_baseToken_weiUnits = Libraries.core.ConvertEtherAmountToBaseAmount(amount_baseToken_etherUnits, Libraries.core.GetDecimalsForTokenContract(baseToken, True))

    exchangeContractAddress = Exchanges.zrxV4.Contract_Exchange.lower()
    txOrigin = HidingGameRfqOriginsAccount
    zrxStakingPool = Exchanges.zrxV4.ZrXStakingPool_KeeperDAO
    maker = publicAddress.lower()
    taker = Libraries.core.GetNullAddress()

    if Libraries.core.IsBuy(side):
        makerTokenAddress = quoteToken.lower()
        takerTokenAddress = baseToken.lower()
        makerTokenAmount = str(amount_quoteToken_weiUnits)
        takerTokenAmount = str(amount_baseToken_weiUnits)
    elif Libraries.core.IsSell(side):
        makerTokenAddress = baseToken.lower()
        takerTokenAddress = quoteToken.lower()
        makerTokenAmount = str(amount_baseToken_weiUnits)
        takerTokenAmount = str(amount_quoteToken_weiUnits)
    else:
        raise Exception("Not buy or sell?")

    expirationUnixTimestampSec = str(int(time.time() + orderDuration_seconds))
    # Set the salt based on unix epoch time in milliseconds instead of seconds.
    salt = str(int(time.time()) * 1000)

    order = MyOrder_0xv4("_" + side, exchangeContractAddress, maker, taker, makerTokenAddress, takerTokenAddress,
                         makerTokenAmount, takerTokenAmount, expirationUnixTimestampSec,
                         orderDuration_seconds, salt, txOrigin, zrxStakingPool)
    Libraries.signingUtils_0xv4.SignOrder_0xv4(order, privateKey)

    if doWriteOrderToLogsForTesting:
        # Write the order to a file so we can keep track of it even if the bot crashes or stops
        serializedOrderObject = jsonpickle.encode(order)
        PrintAndLog_FuncNameHeader("serializedOrderObject = " + str(serializedOrderObject))

        if Libraries.core.IsBuy(side):
            file = open("testLocalOrder_0x_buy", "w")
            file.write(serializedOrderObject)
            file.close()
        elif Libraries.core.IsSell(side):
            file = open("testLocalOrder_0x_sell", "w")
            file.write(serializedOrderObject)
            file.close()
        else:
            raise Exception("Not buy or sell?")

    return order


def API_PostOrders_0xV4(orders):
    payload = []
    for order in orders:
        # 0xv4
        jData_order = {
            "maker": order.maker.lower(),
            "taker": order.taker.lower(),
            "makerAmount": str(order.makerTokenAmount).lower(),
            "takerAmount": str(order.takerTokenAmount).lower(),
            "makerToken": order.makerTokenAddress.lower(),
            "takerToken": order.takerTokenAddress.lower(),
            "salt": str(order.salt).lower(),
            "expiry": order.expirationUnixTimestampSec,
            "chainId": 1,
            "txOrigin": order.txOrigin.lower(),
            "pool": Exchanges.zrxV4.ZrXStakingPool_KeeperDAO.lower(),
            "verifyingContract": Exchanges.zrxV4.Contract_Exchange.lower(),
            "signature": {
                "signatureType": 3,
                "v": order.v,
                "r": order.r,
                "s": order.s
            }
        }
        payload.append(jData_order)

    url = GetUrl_Http() + "orders"
    PrintAndLog_FuncNameHeader("url = " + str(url))
    PrintAndLog_FuncNameHeader("payload = " + str(payload))
    response = requests.post(url, data=json.dumps(payload), headers=GetHeaders(), timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog_FuncNameHeader("responseData = " + str(responseData) + ", call took " + str(response.elapsed.total_seconds()) + " seconds")
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLogError("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetOrders(openOnly=True, makerAddress=None):
    # orders?maker=0xca77dc47eec9e1c46c9f541ba0f222e741d6236b
    url = GetUrl_Http() + "orders"

    urlDict = {}
    if openOnly:
        urlDict['open'] = openOnly

    if makerAddress:
        if not Libraries.core.IsValidAddress(makerAddress):
            raise Exception("makerAddress is not valid: makerAddress = " + str(makerAddress))

        urlDict['maker'] = makerAddress

    if len(urlDict) > 0:
        url += "?" + urlencode(urlDict)

    PrintAndLog_FuncNameHeader("url = " + str(url))
    response = requests.get(url, headers=GetHeaders(), timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def GenerateTokenList():
    from Libraries.accounts import TokenDict_Ninja

    tokenListDict = {}
    tokenListDict['name'] = 'Hiding Game'
    # TODO generate time stamp YEAR-MONTH-DAYTHOUR:MINUTE:SECOND+00:00
    tokenListDict['timestamp'] = '2020-12-07T10:35:15+00:00'
    tokenListDict['version'] = {
        'major': 1,
        'minor': 0,
        'patch': 0,
    }
    tokenListDict['keywords'] = ['keeperdao', 'hiding', 'game', 'rook', 'list', 'mev']
    tokenListDict['tokens'] = []
    tokenListDict['logoURI'] = "https://raw.githubusercontent.com/keeperdao/assets/master/logos/KeeperDAO_Logo_Monogram_Black.svg"

    tokenListDict['tokens'] = []

    PrintAndLog_FuncNameHeader("Updating " + str(len(TokenDict_Ninja)) + " token")
    for symbol in TokenDict_Ninja:
        try:
            token = TokenDict_Ninja[symbol]

            if Libraries.utils.IsTokenAKnownEtherToken(token.erc20TokenContractAddress):
                PrintAndLog_FuncNameHeader("Skipping this ether token since we can only trade ERC20s: " + str(token.erc20TokenContractAddress))
                continue

            tokenDict = {}
            tokenDict['address'] = token.erc20TokenContractAddress.lower()
            tokenDict['chainId'] = 1
            # tokenDict['name'] = None
            tokenDict['symbol'] = token.tokenName.upper()
            tokenDict['decimals'] = Libraries.core.ConvertUsableDecimalsToDecimals(int(token.decimals))
            # tokenDict['logoURI'] = None

            tokenListDict['tokens'].append(tokenDict)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception with symbol " + str(symbol) + ": " + traceback.format_exc())

    #     {
    #       "address": "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48",
    #       "chainId": 1,
    #       "name": "USDCoin",
    #       "symbol": "USDC",
    #       "decimals": 6,
    #       "logoURI": "https://tokens.1inch.exchange/0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48.png"
    #     },

    PrintAndLog_FuncNameHeader("tokenListDict = " + str(tokenListDict))
    return tokenListDict
