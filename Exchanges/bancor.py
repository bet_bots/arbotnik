import datetime
import json
from enum import Enum
from threading import Lock

import requests

import Libraries.arbyUtility
import Libraries.core
import Libraries.gasStation
import Libraries.nodes
from Libraries.loggingConfig import PrintAndLog

URL_Bancor_Base = "https://api.bancor.network/0.1/"

# TODO, hard code this at 5 for now, but in the future min it to 5
Validation_DoEnforceGasPriceMax = True

# Verification_GasPriceTarget = 5
# Validation_DoValidateGasPrices = False

# Bancor Rate limit: max 6 calls per second.  I'll be nice to them :-)
RateLimit_AllowedCallsPerSecond = 3
SleepPerAPICall_s = 1 / float(RateLimit_AllowedCallsPerSecond)

ErrorCountSinceLastDateTime = 0
DateTimeOfLastErrorPostMessage = None

BancorWrappedETH = Libraries.nodes.Instance_Web3.toChecksumAddress("0xc0829421c1d260bd3cb3e0f06cfe2d52db2ce315")
Contract_BancorNetwork = Libraries.nodes.Instance_Web3.toChecksumAddress("0xf20b9e713a33f61fa38792d2afaf1cd30339126a")

# TODO, this address is also in ninja.py, i should only have it in one single location.
BancorTokenContractAddress = "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c".lower()
Decimals_bnt = 18

# We must make a call to the Contract_BancorNetwork in order to get the Contract_BancorGasPriceLimit
Contract_BancorGasPriceLimit = None
Lock_Contract_BancorGasPriceLimit = Lock()

# So it seems there are a bunch of contracts. MANA has two contracts I interact with to buy and sell
# Only one of them contains the tokens (MANA for the MANA converter, and EOS for the EOS converter, etc).
# WHen you want to make the call to getReturn to estimate the trade price, you must call function on the one that contains the tokens.
# Take a look at these two for example:
# this  https://etherscan.io/tx/0x2aba44a141a35cc6101bd02f5520d7dd93690fa43eeacd36a9295de1466cdcb8
# and   https://etherscan.io/tx/0xd5f02e2898746c5fa240bda877a598b2f753fddb9fd8c52a5423f651b6456ec0
ContractDict_Converter = {
    "BuyingAddress1": Libraries.nodes.Instance_Web3.toChecksumAddress("0xb626a5facc4de1c813f5293ec3be31979f1d1c78"),
    "BuyingAddress2": Libraries.nodes.Instance_Web3.toChecksumAddress("0xc6725ae749677f21e4d8f85f41cfb6de49b9db29"),
    "BuyingAddress3": Libraries.nodes.Instance_Web3.toChecksumAddress("0x3839416bd0095d97be9b354cbfb0f6807d4d609e"),
    "BuyingAddress4": Libraries.nodes.Instance_Web3.toChecksumAddress("0xb89570f6ad742cb1fd440a930d6c2a2ea29c51ee"),
    "BuyingAddress5": Libraries.nodes.Instance_Web3.toChecksumAddress("0xcbc6a023eb975a1e2630223a7959988948e664f3"),
    "BuyingAddress6": Libraries.nodes.Instance_Web3.toChecksumAddress("0x9c248517b92ae226b88a0a0c28de02b9b7b039d3"),
    "BuyingAddress7": Libraries.nodes.Instance_Web3.toChecksumAddress("0x0e936b11c2e7b601055e58c7e32417187af4de4a"),
    "MANA": Libraries.nodes.Instance_Web3.toChecksumAddress("0x967f1c667fc490ddd2fb941e3a461223c03d40e9"),
    "AION": Libraries.nodes.Instance_Web3.toChecksumAddress("0xdd9b82c59aa260b2a834ec67c472f43b40a2e6f1"),
    "TRX": Libraries.nodes.Instance_Web3.toChecksumAddress("0x58C46d06cFDCf9cA2CB590d7B79B12ffA8aDCe04"),
    "SNT": Libraries.nodes.Instance_Web3.toChecksumAddress("0x599485dc0f3d8b308b973b2db5cd44bae46d31c4"),
    "POWR": Libraries.nodes.Instance_Web3.toChecksumAddress("0x8fd5bfbc2f61a450400ae275e64d1e171c05b639"),
    "OMG": Libraries.nodes.Instance_Web3.toChecksumAddress("0x11614c5f1eb215ecffe657da56d3dd12df395dc8"),
    "BNB": Libraries.nodes.Instance_Web3.toChecksumAddress("0x751b934e7496e437503d74d0679a45e49c0b7071"),
    "BAT": Libraries.nodes.Instance_Web3.toChecksumAddress("0x46ffcdc6d8e6ed69f124d944bbfe0ac74f8fcf7f"),
    "ENJ": Libraries.nodes.Instance_Web3.toChecksumAddress("0xf3ed5b15618494ddbd0a57b3bca8b2686ac0bc04"),
    "REQ": Libraries.nodes.Instance_Web3.toChecksumAddress("0x37c88474b5d6c593bbd2e4ce16635c08f8215b1e"),
    "KNC": Libraries.nodes.Instance_Web3.toChecksumAddress("0xcde79f10b689a716029d0edb54de78b1bbc14957"),
    "STORM": Libraries.nodes.Instance_Web3.toChecksumAddress("0x58b249b613ce917b6ccc2f66787856ef39f4f0b6"),
    "ELF": Libraries.nodes.Instance_Web3.toChecksumAddress("0x645a3f2fa86be27a4d9a3cc93a73f27b33df766f"),
    "GTO": Libraries.nodes.Instance_Web3.toChecksumAddress("0xe88d6d63389d5c91e6348e379913f330739ad2c4"),
    "RLC": Libraries.nodes.Instance_Web3.toChecksumAddress("0x8b30e174bddb3c0376e666afb8a4196e2f53182d"),
    "VIB": Libraries.nodes.Instance_Web3.toChecksumAddress("0xbe1daf05bf9e054b3e28b7e9c318819ef5dacb58"),
    "SRN": Libraries.nodes.Instance_Web3.toChecksumAddress("0xd3a3bace3d61f6f5d16a9b415d51813cd2ea3887"),
    "UP": Libraries.nodes.Instance_Web3.toChecksumAddress("0xa7a402266ceea0652ea8eafd919d619d16bee134"),
    "VEE": Libraries.nodes.Instance_Web3.toChecksumAddress("0x3b42239a8bc2f07bb16b17578fe44ff2422c16f6"),
    "WAX": Libraries.nodes.Instance_Web3.toChecksumAddress("0x7bac8115f3789f4d7a3bfe241eb1bcb4d7f71665"),
    "WINGS": Libraries.nodes.Instance_Web3.toChecksumAddress("0x0cfbed1bd80bd8a740f24ec5fca8e8d1a9f87052"),
    "POA20": Libraries.nodes.Instance_Web3.toChecksumAddress("0x6758b7d441a9739b98552b373703d8d3d14f9e62"),
    "DAI": Libraries.nodes.Instance_Web3.toChecksumAddress("0x587044b74004e3d5ef2d453b7f8d198d9e4cb558"),
    "DATA": Libraries.nodes.Instance_Web3.toChecksumAddress("0x8658863984d116d4b3a0a5af45979eceac8a62f1"),
    "POE": Libraries.nodes.Instance_Web3.toChecksumAddress("0x8c2036ce61648fcddffb06d6d11fe0b479ed63fe"),
    "NPXS": Libraries.nodes.Instance_Web3.toChecksumAddress("0x2d56d1904bb750675c0a55ca7339f971f48d9dda"),
    "RCN": Libraries.nodes.Instance_Web3.toChecksumAddress("0x8e7fc617e87b39bd5fe1767a95afa53d2c79f147"),
    "MFT": Libraries.nodes.Instance_Web3.toChecksumAddress("0x9a3487c0d300c4d3a7b3ff38d7a18c53c66f1c49"),
    "MTL": Libraries.nodes.Instance_Web3.toChecksumAddress("0xe0569fd1c3f0affd7e08131a16c06f3381c9355a"),
    "DGD": Libraries.nodes.Instance_Web3.toChecksumAddress("0x9b42a6dde041bd3b812e4dde32ad2887fb9d08da"),
    "MKR": Libraries.nodes.Instance_Web3.toChecksumAddress("0xfdbb3b3cfd6fcc0dd5c1b5bff05bffac1db42258"),
}
# Force all addresses in dict to be lowercase
Libraries.core.ForceAllValuesInDictLowercase(ContractDict_Converter)

ConverterContractForBuyingTokens = ContractDict_Converter['BuyingAddress7']

# Add the BNT address to the ContractDict_Converter
ContractDict_Converter["BNT"] = ConverterContractForBuyingTokens

CurrencyIdDict = {
    "ETH": "5937d635231e97001f744267",
    "BNT": "594bb7e468a95e00203b048d",
    "MANA": "5a2cfacad0129700019a7270",
    "EOS": "5a1eb3753203d200012b8b75",
    "AION": "5a36828a9416220001fa9d94",
    "TRX": "5a69a9cb1b67798dab40b998",
    "SNT": "5a2d307c47bbf500018ecc6e",
    "POWR": "5a12993ac1a3da0001a37aa6",
    "OMG": "5a086f93875e890001605abc",
    "BNB": "5a1fb3df634e000001878563",
    "BAT": "5a1fd517634e000001878565",
    "ENJ": "5a174c5145a97200011ad30a",
    "REQ": "5a69a9cd1b67798dab40b9a1",
    "KNC": "5a69a9cb1b67798dab40b997",
    "STORM": "5a3800604b02a6ad9f85324f",
    "ELF": "5a722b4cec75fc00012d4b8c",
    "GTO": "5a806bfda47fc50001d5bd5c",
    "ENJBNT": "5a1ace8a967e9c6a2a5ac385",
    "RLC": "5a54b9cfb6b5870001b890e0",
    "VIB": "5a69a9d11b67798dab40b9b8",
    "SRN": "5a548d3244680b000198e7f3",
    "UP": "5aa61b9bd5a17c4dc8b098ac",
    "VEE": "5a5eeb3fc5e04600015b40f1",
    "WAX": "5a37e92fed8a500001de70da",
    "WINGS": "5a1ea4d8634e00000187855c",
    "POA20": "5af31eb33a695919b3e7c2c3",
    "DAI": "5a604b62c5e04600015b72d5",
    "DATA": "5af19e5820e434fb44cdeaed",
    "POE": "5a69a9d11b67798dab40b9bd",
    "NPXS": "5a8b1f1db2b342000166f58d",
    "RCN": "5a92b443ef48d60001102970",
    "MFT": "5b3e433ba5281a534e93166f",
    "MTL": "5a69a9cc1b67798dab40b99b",
    "DGD": "5a2d64e647bbf500018ecf25",
    "MKR": "5a72f33c6ce1040001bb5d56",
}

CurrentPrice_BNT_PriceToBuyBNTonBancor = None
CurrentPrice_BNT_PriceToSellBNTonBancor = None
Lock_CurrentPrice_BNT = Lock()
CurrentPrice_BNT_EtherQuantityArray = [0.5, 1, 1.5, 2, 3, 4, 5, 6, 8, 10, 12, 14, 18, 22, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100]


class OrderDataType(Enum):
    Approve = "ApproveOrderData"
    QuickConvert = "QuickConvertOrderData"


def IsKnownConverterContract(address):
    global ContractDict_Converter

    PrintAndLog("IsKnownConverterContract, address = " + str(address))
    return Libraries.core.FindValueInDict(ContractDict_Converter, address)


def IsTransactionDataPrefixedWithBancorQuickConvertHash(hexString):
    # So far there are two functions that I know of, a quickConvert and a quickConvertPrioritized
    isDataBancorQuickConvert = \
        "0xf0843ba9".lower() in str(hexString).lower() or \
        "0x22742564".lower() in str(hexString).lower() or \
        "0x14c9035e".lower() in str(hexString).lower()
    return isDataBancorQuickConvert


def GetDataFor_Approve(contractAddress, amount):
    global ContractDict_Converter

    data = "0x095ea7b3"
    data += Libraries.core.PadDataParemeter_LeftFillPadding(Libraries.core.Remove0XfromHexString(contractAddress))
    # Max number
    # data += "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
    data += amount
    return data


def API_GetConvertiblePairs():
    url = URL_Bancor_Base + "currencies/convertiblePairs"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetConvertiblePairs responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetConvertiblePairs jData = " + str(jData))
        thingy = jData['data']
        return thingy

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetConvertiblePairs response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetAllCurrencies():
    index = 0
    numPerCall = 100
    totalNumOfMarketsToAttemptToGet = 1000
    returnList = []
    while index < totalNumOfMarketsToAttemptToGet:
        returnList = returnList + API_GetCurrencies(index, numPerCall)
        index += numPerCall

    return returnList


def API_GetCurrencies(skip=0, limit=50):
    # "https://api.bancor.network/0.1/currencies"       # Forces limit to 10
    # "https://api.bancor.network/0.1/currencies?limit=100"
    url = URL_Bancor_Base + "currencies?limit=" + str(limit) + "&skip=" + str(skip)
    # url = URL_Bancor_Base + "currencies?limit=100&skip=200"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetCurrencies responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetCurrencies jData = " + str(jData))
        return jData['data']['currencies']['page']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetConvertiblePairs response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def ConvertSymbolToBancorSymbol(symbol):
    if symbol.lower() == "poa":
        symbol = symbol + "20"
    return symbol


def API_GetTicker(baseSymbol, quoteSymbol):
    # Need to convert symbols like POA to POA20
    baseSymbol = ConvertSymbolToBancorSymbol(baseSymbol)
    quoteSymbol = ConvertSymbolToBancorSymbol(quoteSymbol)

    # "https://api.bancor.network/0.1/currencies/GNO/ticker?fromCurrencyCode=BNT"
    # "https://api.bancor.network/0.1/currencies/BNT/ticker?fromCurrencyCode=ETH"
    url = URL_Bancor_Base + "currencies/" + quoteSymbol + "/ticker?fromCurrencyCode=" + baseSymbol
    # PrintAndLog("API_GetTicker: url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetTicker responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetTicker jData = " + str(jData))
        return jData['data']

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetTicker response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_PostConvert(fromCurrency, toCurrency, ownerAddress, amountToUse, minimumReturn, tokenContractAddress_forValidation=None):
    PrintAndLog("API_PostConvert fromCurrency = " + str(fromCurrency) + ", toCurrency = " + str(toCurrency) + ", ownerAddress = " + str(ownerAddress) + ", amountToUse = " + str(
        amountToUse) + ", minimumReturn = " + str(minimumReturn) + ", tokenContractAddress_forValidation = " + str(tokenContractAddress_forValidation))
    # "https://api.bancor.network/0.1/currencies/convert"
    url = URL_Bancor_Base + "currencies/convert"

    # TODO, NOTE:  This APi has a quirk, if you make this call with "minimumReturn" based on a worstPriceIllAccept that's not possible (because the market isn't good enough for that price)
    # then this call will timeout and not respond.  For now, I'm going to shorten the timeout.  And when i'm testing this

    # Need to convert symbols like POA to POA20
    fromCurrency = ConvertSymbolToBancorSymbol(fromCurrency)
    toCurrency = ConvertSymbolToBancorSymbol(toCurrency)

    payload = {
        "blockchainType": "ethereum",
        "fromCurrencyId": CurrencyIdDict[fromCurrency.upper()],
        "toCurrencyId": CurrencyIdDict[toCurrency.upper()],
        "amount": str(amountToUse),
        "minimumReturn": str(minimumReturn),
        "ownerAddress": ownerAddress
    }
    PrintAndLog("payload = " + str(payload))
    response = requests.post(url, data=json.dumps(payload), headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetConvertiblePairs responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetConvertiblePairs jData = " + str(jData))
        if "errorCode" in jData:
            message = "Error in API_PostConvert with " + str(fromCurrency) + "-" + str(toCurrency) + ": " + str(jData['errorCode'])
            ConsiderPostingMessageSayingWeHadAnError(message)
            # PrintAndLog(message)
            # Libraries.core.API_PostOperatorNotification(message)
            return []

        else:
            dataList = jData['data']
            # if IsValidConvertData(ownerAddress, data_quickConvert, data_approve, tokenContractAddress_forValidation):
            if IsValidConvertData(ownerAddress, dataList, tokenContractAddress_forValidation):
                return dataList
            else:
                message = "Response was NOT valid!!!!!!!!!!!!!"
                PrintAndLog(message + ": dataList = " + str(dataList))
                return message

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetConvertiblePairs response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return []


def IsValidConvertData(ownerAddress, dataList, tokenContractAddress_forValidation=None):
    # global Validation_DoValidateGasPrices
    global Validation_DoEnforceGasPriceMax

    # Here are the scenarios i'm aware of so far
    # 1.) Buying tokens, they send me one dataset, which is calling the Convert function
    # 2.) Selling tokens, they send me two datasets, which are 1) Approve, and 2) Convert
    # 3.) Selling tokens, they send me three datasets, which are 1) Approve reset with value zero, 2) Approve with correct value, and 3) Convert
    # PrintAndLog("Data length = " + str(len(jData['data'])))
    # data_approve = None
    # data_quickConvert = None
    # dataList = jData['data']
    # if len(dataList) == 1:
    #     data_quickConvert = dataList[0]
    # elif len(dataList) == 2:
    #     data_approve = dataList[0]
    #     data_quickConvert = dataList[1]
    #
    # PrintAndLog("data_approve = " + str(data_approve))
    # PrintAndLog("data_quickConvert = " + str(data_quickConvert))

    # jData_quickConvert, jData_approve = None

    PrintAndLog("IsValidConvertData with ownerAddress = " + ownerAddress + ", tokenContractAddress_forValidation = " + tokenContractAddress_forValidation)
    # Validate the data here, do a sanity check and make sure something wasn't manipulated

    for jData in dataList:
        # Begin verify the from address
        if ownerAddress.lower() != jData['from'].lower():
            message = "IsValidConvertData Failed validation: from contract address not matching the owner address"
            PrintAndLog(message)
            Libraries.core.API_PostOperatorNotification(message)
            return False
        # End verify the from address

        # Begin verify the to address
        # If this data is an approve, we want to check to see if the to address goes to the correct token.
        isDataAnApprove = Libraries.core.IsTransactionDataPrefixedWithApproveHash(jData)
        PrintAndLog("isDataAnApprove = " + str(isDataAnApprove))
        if isDataAnApprove:
            if tokenContractAddress_forValidation and tokenContractAddress_forValidation.lower() != jData['to'].lower():
                message = "IsValidConvertData Failed validation due to approve to address not matching the tokenContractAddress: tokenContractAddress_forValidation = " + str(
                    tokenContractAddress_forValidation)
                PrintAndLog(message)
                Libraries.core.API_PostOperatorNotification(message)
                return False

        # Else, it's not an approve so for now we'll assume it's a convert and that it must go to a Bancor contract (they have many contracts...)
        else:
            if not IsKnownConverterContract(jData['to'].lower()):
                message = "IsValidConvertData Failed validation due to quickConvert contract address not matching the to address"
                PrintAndLog(message)
                Libraries.core.API_PostOperatorNotification(message)
                return False

                # Begin verify the price of the convert to make sure it abides by my price constraint I passed into the convert API
                # TODO
                # End verify the price of the convert to make sure it abides by my price constraint I passed into the convert API
        # End verify the to address

        # Begin verify the gas price to make sure it's not some crazy number like 1000 Gwei
        if Validation_DoEnforceGasPriceMax:
            if not isDataAnApprove:
                gasPrice_hex = jData['gasPrice']
                PrintAndLog("gasPrice_hex = " + str(gasPrice_hex))
                gasPrice_gwei = Libraries.core.ConvertWeiToGwei(Libraries.core.ConvertHexToInt(jData['gasPrice']))
                PrintAndLog("IsValidConvertData: gasPrice_gwei = " + str(gasPrice_gwei))
                gasPrice_maxAllowance = Libraries.gasStation.GetGasPrice_StaleOrderFromOrderBook_Min()
                if gasPrice_gwei > gasPrice_maxAllowance:
                    # Update the jData with a valid gas price
                    message = "IsValidConvertData Failed validation due to gasPrice_gwei " + str(gasPrice_gwei) + " exceeding our max allowance of " + str(gasPrice_maxAllowance)
                    PrintAndLog(message)
                    Libraries.core.API_PostOperatorNotification(message)
                    return False
                    # End verify the gas price to make sure it's not some crazy number like 1000 Gwei

                    # # Begin verify that the gas price is exactly a value
                    # if Validation_DoValidateGasPrices:
                    #     if not isDataAnApprove:
                    #         gasPrice_hex = jData['gasPrice']
                    #         PrintAndLog("gasPrice_hex = " + str(gasPrice_hex))
                    #         gasPrice_gwei = Libraries.core.ConvertWeiToGwei(Libraries.core.ConvertHexToInt(jData['gasPrice']))
                    #         PrintAndLog("IsValidConvertData: gasPrice_gwei = " + str(gasPrice_gwei))
                    #         if gasPrice_gwei != Verification_GasPriceTarget:
                    #             # Update the jData with a valid gas price
                    #             jData['gasPrice'] = "0x" + Libraries.core.ConvertIntToHex(Libraries.core.ConvertGweiToWei(Verification_GasPriceTarget))
                    #             message = "IsValidConvertData detected a bad gas price delivered via the Bancor API. I'm manually enforcing a good gas price. bad gas price: " + str(
                    #                 gasPrice_gwei) + ", i'm overriding to: " + str(Verification_GasPriceTarget)
                    #             PrintAndLog(message)
                    #             Libraries.core.API_PostOperatorNotification(message)
                    # # End verify that the gas price is exactly a value

    return True


def GetManyMarketPrices_ConvertTokensToEther_GivenMarket(market, price_bnteth, tokensAmountList_ether):
    # PrintAndLog("GetManyMarketPrices_ConvertTokensToEther_GivenMarket: token.decimals = " + str(market.decimals))
    return GetManyMarketPrices_ConvertTokensToEther(market.GetSecondaryTokenName(), market.erc20TokenContractAddress, market.decimals, price_bnteth, tokensAmountList_ether)


def GetManyMarketPrices_ConvertTokensToEther_GivenToken(token, price_bnteth, tokensAmountList_ether):
    # PrintAndLog("GetManyMarketPrices_ConvertTokensToEther_GivenToken: token.decimals = " + str(token.decimals))
    return GetManyMarketPrices_ConvertTokensToEther(token.tokenName, token.erc20TokenContractAddress, token.decimals, price_bnteth, tokensAmountList_ether)


def GetManyMarketPrices_ConvertTokensToEther(symbol, erc20TokenContractAddress, decimals, price_bnteth, tokensAmountList_ether):
    global ContractDict_Converter
    global BancorTokenContractAddress
    global BancorWrappedETH

    # HACK, this isn't ideal...
    # This handles the special case for BNT when i want to convert from ETH to BNT.  This conversion only requires one conversion,
    # all other conversions (form token to token) require multiple and are more complicated
    if symbol.lower() == "bnt":
        isConvertingBNTwithETH = True
    else:
        isConvertingBNTwithETH = False

    if isConvertingBNTwithETH:
        toAddress = BancorWrappedETH
    else:
        toAddress = BancorTokenContractAddress

    # PrintAndLog("GetManyMarketPrices_ConvertTokensToEther price_bnteth = " + str(price_bnteth) + ", tokensAmountList_ether = " + str(
    #     tokensAmountList_ether) + ", using toAddress = " + str(toAddress))

    # Convert TOKEN to BNT
    returnBNTlist_ether = Libraries.arbyUtility.API_ConverterContract_GetManyReturns(ContractDict_Converter[symbol.upper()],
                                                                                     erc20TokenContractAddress, toAddress, tokensAmountList_ether,
                                                                                     decimals, Decimals_bnt)

    # PrintAndLog("returnBNTlist_ether = " + str(returnBNTlist_ether))
    # Calculate trade price
    returnETHList_ether = []
    tradePriceList = []
    for i, returnBNT_ether in enumerate(returnBNTlist_ether):
        if isConvertingBNTwithETH:
            returnETH_ether = returnBNT_ether
        else:
            returnETH_ether = returnBNT_ether * price_bnteth

        # PrintAndLog("returnETH_ether = " + str(returnETH_ether))
        tradePrice = returnETH_ether / tokensAmountList_ether[i]
        # PrintAndLog("tradePrice to sell " + str(tokensAmountList_ether[i]) + " " + symbol.upper() + " on Bancor = " + str(tradePrice))
        returnETHList_ether.append(returnETH_ether)
        tradePriceList.append(tradePrice)

    return tradePriceList, returnETHList_ether


def GetManyMarketPrices_ConvertEtherToTokens_GivenMarket(market, price_bnteth, etherListAmount_ether):
    # PrintAndLog("GetManyMarketPrices_ConvertEtherToTokens_GivenToken: market.decimals = " + str(market.decimals))
    return GetManyMarketPrices_ConvertEtherToTokens(market.GetSecondaryTokenName(), market.erc20TokenContractAddress, market.decimals, price_bnteth, etherListAmount_ether)


def GetManyMarketPrices_ConvertEtherToTokens_GivenToken(token, price_bnteth, etherListAmount_ether):
    # PrintAndLog("GetManyMarketPrices_ConvertEtherToTokens_GivenToken: token.decimals = " + str(token.decimals))
    return GetManyMarketPrices_ConvertEtherToTokens(token.tokenName, token.erc20TokenContractAddress, token.decimals, price_bnteth, etherListAmount_ether)


def GetManyMarketPrices_ConvertEtherToTokens(symbol, erc20TokenContractAddress, decimals, price_bnteth, etherListAmount_ether):
    global ContractDict_Converter
    global BancorTokenContractAddress

    # HACK, this isn't ideal...
    # This handles the special case for BNT when i want to convert from ETH to BNT.  This conversion only requires one conversion,
    # all other conversions (form token to token) require multiple and are more complicated
    if symbol.lower() == "bnt":
        isConvertingBNTwithETH = True
    else:
        isConvertingBNTwithETH = False

    if isConvertingBNTwithETH:
        fromAddress = BancorWrappedETH

    else:
        fromAddress = BancorTokenContractAddress

    # PrintAndLog("GetManyMarketPrices_ConvertEtherToTokens price_bnteth = " + str(price_bnteth) + ", etherListAmount_ether = " + str(
    #     etherListAmount_ether) + ", using fromAddress = " + str(fromAddress))

    # Convert ETH to BNT
    bntAmountList_ether = []
    for etherAmount_ether in etherListAmount_ether:
        if isConvertingBNTwithETH:
            bntAmount_ether = etherAmount_ether
        else:
            bntAmount_ether = etherAmount_ether / price_bnteth

        bntAmountList_ether.append(bntAmount_ether)

    # Call getReturn for converting BNT to TOKEN
    returnTokensList_ether = Libraries.arbyUtility.API_ConverterContract_GetManyReturns(ContractDict_Converter[symbol.upper()],
                                                                                        fromAddress, erc20TokenContractAddress, bntAmountList_ether,
                                                                                        Decimals_bnt, decimals)

    # PrintAndLog("returnTokensList_ether = " + str(returnTokensList_ether))
    # Calculate trade price
    tradePriceList = []
    for i, returnTokens_ether in enumerate(returnTokensList_ether):
        tradePrice = etherListAmount_ether[i] / returnTokens_ether
        # PrintAndLog("tradePrice to buy " + str(returnTokens_ether) + " " + symbol.upper() + " on Bancor = " + str(tradePrice))
        tradePriceList.append(tradePrice)

    return tradePriceList, returnTokensList_ether


def API_ConverterContract_GetPurchaseReturn(bancorConverterContractAddress, connectorToken, amount_ether, decimals):
    global Decimals_bnt

    # PrintAndLog("API_ConverterContract_GetPurchaseReturn")
    import Contracts.contracts

    amount_wei = Libraries.core.ConvertEtherToWei(amount_ether, decimals)
    kwargs = {
        # For some reason it expects an empty string as the label here...
        '_connectorToken': connectorToken,
        '_depositAmount': int(amount_wei),
    }

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_BancorConverter_ethbnt.encodeABI('getPurchaseReturn', kwargs=kwargs),
                "to": bancorConverterContractAddress
            },
            # "latest"
        ]
    }
    # response = requests.post(Libraries.core.Get_RemoteEthereumNodeToUse(Libraries.nodes.URL_RemoteNode.infura_lowPriority), data=json.dumps(payload),
    #                          headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        PrintAndLog("API_ConverterContract_GetPurchaseReturn responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_ConverterContract_GetPurchaseReturn jData = " + str(jData))
        result = jData['result']
        if result == "0x":
            result = "0x0"
        returnValue_ether = int(result, 16) / float(Decimals_bnt)
        PrintAndLog("API_ConverterContract_GetPurchaseReturn returnValue_ether = " + str(returnValue_ether))
        return returnValue_ether

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_ConverterContract_GetPurchaseReturn response was not ok response = " + str(response))
        response.raise_for_status()

    return None


def ConsiderPostingMessageSayingWeHadAnError(stackTraceString):
    global DateTimeOfLastErrorPostMessage
    global ErrorCountSinceLastDateTime

    # Increment the number of occurrences
    # Also keep track of how many occurred
    ErrorCountSinceLastDateTime += 1

    if not DateTimeOfLastErrorPostMessage or ((datetime.datetime.now() - DateTimeOfLastErrorPostMessage).total_seconds() > 1400):
        DateTimeOfLastErrorPostMessage = datetime.datetime.now()

        message = "Error (" + str(ErrorCountSinceLastDateTime) + " errors since last report): " + stackTraceString
        Libraries.core.API_PostOperatorNotification(message)

        # Reset this to zero since we sent the message.
        ErrorCountSinceLastDateTime = 0


def API_GetBancorGasPriceLimitContract():
    global Lock_Contract_BancorGasPriceLimit
    global Contract_BancorGasPriceLimit

    import Contracts.contracts

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_BancorNetwork.encodeABI('gasPriceLimit', kwargs={}),
                "to": Contract_BancorNetwork,
            },
            # "latest"
        ]
    }
    # response = requests.post(Libraries.core.Get_RemoteEthereumNodeToUse(Libraries.nodes.URL_RemoteNode.infura_lowPriority), data=json.dumps(payload),
    #                          headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetBancorGasPriceLimitContract responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetBancorGasPriceLimitContract jData = " + str(jData))

        Lock_Contract_BancorGasPriceLimit.acquire()
        try:
            Contract_BancorGasPriceLimit = Libraries.nodes.Instance_Web3.toChecksumAddress(jData['result'][-Libraries.core.LengthOfPublicAddress_Excludes0x:])
            PrintAndLog("Setting Contract_BancorGasPriceLimit to " + str(Contract_BancorGasPriceLimit))

        finally:
            Lock_Contract_BancorGasPriceLimit.release()

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetBancorGasPriceLimitContract response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetBancorMaxGasPriceLimit():
    global Contract_BancorGasPriceLimit

    import Contracts.contracts

    if not Contract_BancorGasPriceLimit:
        raise Exception("Contract_BancorGasPriceLimit was not set")

    payload = {
        # "id": randint(0, 99999999999999),
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_BancorGasPriceLimit.encodeABI('gasPrice', kwargs={}),
                "to": Contract_BancorGasPriceLimit,
            },
            # "latest"
        ]
    }
    # response = requests.post(Libraries.core.Get_RemoteEthereumNodeToUse(Libraries.nodes.URL_RemoteNode.infura_lowPriority), data=json.dumps(payload),
    #                          headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetBancorMaxGasPriceLimit responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetBancorMaxGasPriceLimit jData = " + str(jData))
        gasPrice_wei = Libraries.core.ConvertHexToInt(jData['result'])
        gasPrice_gwei = Libraries.core.ConvertWeiToGwei(gasPrice_wei)
        PrintAndLog("API_GetBancorMaxGasPriceLimit gasPrice_gwei = " + str(gasPrice_gwei
                                                                           ))
        return gasPrice_gwei

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetBancorMaxGasPriceLimit response was not ok response = " + str(response))
        response.raise_for_status()


def SetBancorBNTprices():
    global CurrentPrice_BNT_PriceToBuyBNTonBancor
    global CurrentPrice_BNT_PriceToSellBNTonBancor
    global Lock_CurrentPrice_BNT
    global CurrentPrice_BNT_EtherQuantityArray

    # TODO, what to use here???
    # valueToUse_ether_etherUnits = 5
    # PrintAndLog("SetBancorBNTprices: valueToUse_ether_etherUnits = " + str(valueToUse_ether_etherUnits))

    etherArrayToSend = CurrentPrice_BNT_EtherQuantityArray
    returnTokensList_etherUnits = GetManyMarketPrices_ConvertEtherToTokens(
        "bnt", BancorTokenContractAddress, Decimals_bnt, None, etherArrayToSend)
    # PrintAndLog("SetBancorBNTprices: returnTokensList_etherUnits = " + str(returnTokensList_etherUnits))

    Lock_CurrentPrice_BNT.acquire()
    try:
        CurrentPrice_BNT_PriceToBuyBNTonBancor = returnTokensList_etherUnits[0]
        # PrintAndLog("CurrentPrice_BNT_PriceToBuyBNTonBancor = " + str(CurrentPrice_BNT_PriceToBuyBNTonBancor))
    finally:
        Lock_CurrentPrice_BNT.release()  # release lock, no matter what

    # I'm going to send an array of tokens equal to the ones I would get in return for the above conversion.
    tokensArrayToSend = returnTokensList_etherUnits[1]
    returnEtherList_etherUnits = GetManyMarketPrices_ConvertTokensToEther(
        "bnt", BancorTokenContractAddress, Decimals_bnt, None, tokensArrayToSend)

    Lock_CurrentPrice_BNT.acquire()
    try:
        CurrentPrice_BNT_PriceToSellBNTonBancor = returnEtherList_etherUnits[0]
        # PrintAndLog("CurrentPrice_BNT_PriceToSellBNTonBancor = " + str(CurrentPrice_BNT_PriceToSellBNTonBancor))
    finally:
        Lock_CurrentPrice_BNT.release()  # release lock, no matter what


def GetBancorBNTprices():
    global CurrentPrice_BNT_PriceToBuyBNTonBancor
    global CurrentPrice_BNT_PriceToSellBNTonBancor
    global Lock_CurrentPrice_BNT

    # This function assumes that SetBancorBNTprices is being called periodically

    local_CurrentPrice_BNT_PriceToBuyBNTonBancor = None
    local_CurrentPrice_BNT_PriceToSellBNTonBancor = None

    Lock_CurrentPrice_BNT.acquire()
    try:
        local_CurrentPrice_BNT_PriceToBuyBNTonBancor = CurrentPrice_BNT_PriceToBuyBNTonBancor
        local_CurrentPrice_BNT_PriceToSellBNTonBancor = CurrentPrice_BNT_PriceToSellBNTonBancor

    finally:
        Lock_CurrentPrice_BNT.release()

    return local_CurrentPrice_BNT_PriceToBuyBNTonBancor, local_CurrentPrice_BNT_PriceToSellBNTonBancor
