import requests
import json
import traceback
from random import randint
import websocket
import time
import threading
import jsonpickle
from enum import Enum

from Libraries.loggingConfig import PrintAndLog, PrintAndLogError, PrintAndLog_Websockets, PrintAndLog_FuncNameHeader
from Libraries.core import API_PostOperatorNotification
import Libraries.core
import Libraries.signingUtils
import Libraries.orderAggregator
import Libraries.executeOnInterval
import Exchanges.zrxV2

URL_REST_Base = "https://api.0x.org/"
URL_WS_Base = "wss://api.0x.org/sra/v3"

# Use excludedSources to exclude things like Kyber and Uniswap if I want:
# /swap/v0/quote?buyToken=DAI&sellToken=ETH&buyAmount=100000000&excludedSources=0x,Kyber

WebsocketClient = None


class WebsocketRequestType(Enum):
    Subscribe = 'subscribe'
    Unsubscribe = 'unsubscribe'


class WebsocketRequestChannel(Enum):
    Orders = 'orders'


def API_GetTokens():
    url = URL_REST_Base + "swap/v0/tokens"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetListOfTokenAddresses():
    jData_tokens = API_GetTokens()
    tokenAddressList = []
    for record in jData_tokens['records']:
        tokenAddress = record['address']
        tokenAddressList.append(tokenAddress.lower())

    return tokenAddressList


def API_GetQuote(buyToken, sellToken, buyAmount_weiUnits, sellAmount_weiUnits):
    amountsAreValid = True
    if not buyAmount_weiUnits and not sellAmount_weiUnits:
        amountsAreValid = False
    elif buyAmount_weiUnits and sellAmount_weiUnits:
        amountsAreValid = False

    if not amountsAreValid:
        raise Exception("Caller must specify exactly one of buyAmount_weiUnits and sellAmount_weiUnits")

    url = URL_REST_Base + "swap/v0/quote?buyToken=" + str(buyToken.lower()) + "&sellToken=" + str(
        sellToken.lower())

    if buyAmount_weiUnits:
        url += "&buyAmount=" + str(buyAmount_weiUnits)
    elif sellAmount_weiUnits:
        url += "&sellAmount=" + str(sellAmount_weiUnits)

    # PrintAndLog_FuncNameHeader("url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetAssetPairs(tokenAddress):
    # /sra/v3/asset_pairs?assetDataA=0xf47261b...1f4699f498
    url = URL_REST_Base + "sra/v3/asset_pairs?assetDataA=" + str(Exchanges.zrxV2.EncodeERC20AssetData(tokenAddress))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_GetOrderbook(baseToken, quoteToken, page=1, perPage=300):
    # /sra/v3/orderbook?baseAssetData=0xf472...6cc2&quoteAssetData=0xf472...71d0f
    url = URL_REST_Base + "sra/v3/orderbook?baseAssetData=" + str(Exchanges.zrxV2.EncodeERC20AssetData(baseToken)) + "&quoteAssetData=" + str(
        Exchanges.zrxV2.EncodeERC20AssetData(quoteToken)) + "&page=" + str(page) + "&perPage=" + str(perPage)

    PrintAndLog_FuncNameHeader("baseToken = " + str(baseToken) + ", quoteToken = " + str(quoteToken))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()


def API_PostOrder(order):
    url = URL_REST_Base + "sra/v3/orders"

    payload = [
        {
            "signature": order.signature,
            "senderAddress": order.senderAddress,
            "makerAddress": order.maker,
            "takerAddress": order.taker,
            "makerFee": order.makerFee,
            "takerFee": order.takerFee,
            "makerAssetAmount": str(order.makerTokenAmount),
            "takerAssetAmount": str(order.takerTokenAmount),
            "makerAssetData": Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress),
            "takerAssetData": Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress),
            "salt": str(order.salt),
            "exchangeAddress": order.exchangeContractAddress,
            "feeRecipientAddress": order.feeRecipient,
            "expirationTimeSeconds": order.expirationUnixTimestampSec,
            "makerFeeAssetData": order.makerFeeTokenAddress,
            "takerFeeAssetData": order.takerFeeTokenAddress,
            "chainId": 1,
        },
    ]

    PrintAndLog_FuncNameHeader("posting order to 0xAPI, orderHash = " + str(order.hash))
    response = requests.post(url, data=json.dumps(payload), headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        # responseData = response.content
        # PrintAndLog_FuncNameHeader("responseData = " + str(responseData))
        return True

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response) + ", " + str(response.content) + ". payload = " + str(payload))
        response.raise_for_status()

    return False


def ConnectWebSocketClient(exchangeName):
    global WebsocketClient
    WebsocketClient = WebsocketClientObject(exchangeName)


class WebsocketClientObject:
    ws = None
    exchangeName = None

    def __init__(self, _exchangeName):
        self.exchangeName = _exchangeName

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        websocket.enableTrace(False)
        websocket.http_proxy_host = URL_WS_Base
        self.ws = websocket.WebSocketApp(URL_WS_Base,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)

        while True:
            try:
                # Subscribe to events
                Libraries.executeOnInterval.IsTimeToExecute("SubscribeWebsockets_OrderEvents", 0, SubscribeWebsockets_OrderEvents, True)

                Libraries.core.API_PostOperatorNotification("Connecting to 0xAPI websockets")
                self.ws.run_forever(ping_interval=30, ping_timeout=10)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception in 0xAPI websockets = " + traceback.format_exc())

            PrintAndLog("0xAPI websockets disconnected! Sleeping a bit then trying again")
            time.sleep(30)

    def on_message(self, ws, message):
        printMessage = "0xAPI: Websockets: data received: " + message[0:90] + "....." + message[-25:]
        PrintAndLog_Websockets(printMessage)
        # Periodically tell me that websockets are working
        if Libraries.executeOnInterval.IsTimeToExecute("0xAPI: Websockets: data received", 14400):
            API_PostOperatorNotification(printMessage)

        try:
            jData = json.loads(message)
            # PrintAndLog_Websockets("0xAPI: Websockets: jData = " + str(jData))
            if jData['channel'] == 'orders' and jData['type'] == 'update':
                # # Assume payload can be an array of orders, although I've never seen more than one order in here
                Libraries.orderAggregator.HandleNewOrderDatas_0xv3(jData['payload'], self.exchangeName, True)

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            errorMessage = "exception when parsing message in WebsocketClientObject: " + traceback.format_exc() + ", message = " + str(message)
            PrintAndLog_Websockets(errorMessage)
            PrintAndLogError(errorMessage)

    def on_error(self, ws, error):
        PrintAndLogError(str(error))

    def on_close(self, ws):
        message = "0xAPI: Disconnected from websockets."
        PrintAndLogError(message)
        Libraries.core.API_PostOperatorNotification(message)


def SubscribeWebsockets_OrderEvents(doSleepFirst=True):
    # Give websockets time to connect
    if doSleepFirst:
        time.sleep(4)

    # Subscribe to order change events
    # PrintAndLog("Subscribing to order events")
    payload = {
        'type': WebsocketRequestType.Subscribe.value,
        'channel': WebsocketRequestChannel.Orders.value,
        'requestId': str(randint(0, 99999999999999)),
    }
    SubscribeWebsocketsToEndpoint(payload)


def SubscribeWebsocketsToEndpoint(subscriptionPayload):
    global WebsocketClient
    PrintAndLog("Subscribing to subscriptionPayload = " + str(subscriptionPayload))
    WebsocketClient.ws.send(jsonpickle.encode(subscriptionPayload))
