import requests
import json
from enum import Enum

from Libraries.loggingConfig import PrintAndLog
import Libraries.core
import Libraries.nodes

URL_REST_DDEX_Base = 'https://api.ddex.io/v3/'
URL_WS_DDEX_Base = 'wss://ws.ddex.io/v3/'

Contract_Exchange = Libraries.nodes.Instance_Web3.toChecksumAddress("0x09cabec1ead1c0ba254b09efb3ee13841712be14")


def API_GetMarkets():
    url = URL_REST_DDEX_Base + "markets"
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetMarkets responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetMarkets jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetMarkets response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


class OrderbookOption(Enum):
    Option1 = 1
    Option2 = 2
    Option3 = 3
    # 1	The best bid and ask prices
    # 2	The top 50 bids and asks, with orders at the same price aggregated together
    # 3	Returns the full orderbook with no aggregation


def API_GetOrderbook(exchange_DDEX):
    orderbookOption = OrderbookOption.Option3

    ddexMarketName = GetDDEXMarketName_GivenMarket(exchange_DDEX)
    url = URL_REST_DDEX_Base + "markets/" + str(ddexMarketName) + "/orderbook?level=" + str(orderbookOption.value)

    PrintAndLog("API_GetOrderbook url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetOrderbook responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetOrderbook jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetOrderbook response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def GetDDEXMarketName_GivenMarket(exchange_DDEX):
    if exchange_DDEX.DoIFlipBaseAndQuote():
        baseTokenSymbol = exchange_DDEX.market.GetPrimaryTokenName().upper()
        quoteTokenSymbol = exchange_DDEX.market.GetSecondaryTokenName().upper()
    else:
        baseTokenSymbol = exchange_DDEX.market.GetSecondaryTokenName().upper()
        quoteTokenSymbol = exchange_DDEX.market.GetPrimaryTokenName().upper()

    baseTokenSymbol = baseTokenSymbol.replace("ETH", "WETH")
    quoteTokenSymbol = quoteTokenSymbol.replace("ETH", "WETH")

    return baseTokenSymbol + "-" + quoteTokenSymbol
