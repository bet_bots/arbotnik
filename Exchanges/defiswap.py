import json
import sys
import threading
import traceback
from random import randint

import Libraries.core
import Libraries.nodes
import Libraries.cache
import Exchanges.zrxV2
import Exchanges.keeperDAO
from Libraries.loggingConfig import PrintAndLog_FuncNameHeader, PrintAndLogError
import Libraries.defaults

ContractDict_Exchange = {}

Separator = "_"


def API_GetFactory():
    import Contracts.contracts

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_Defiswap_Router.encodeABI('factory', kwargs={}),
                "to": Contracts.contracts.Contract_Defiswap_Router.address
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        factoryContract = Libraries.core.GetAddressFromDataProperty(jData['result'])
        # PrintAndLog_FuncNameHeader("factoryContract = " + str(factoryContract))
        return factoryContract

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetAllPairsLength():
    import Contracts.contracts

    factoryContract = API_GetFactory()
    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_Defiswap_Factory.encodeABI('allPairsLength', kwargs={}),
                "to": factoryContract
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        allPairsLength = Libraries.core.ConvertHexToInt(jData['result'])
        # PrintAndLog_FuncNameHeader("allPairsLength = " + str(allPairsLength))
        return allPairsLength

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetAllExchangesFromFactory():
    chunkSize = 50

    allPairsLength = API_GetAllPairsLength()
    PrintAndLog_FuncNameHeader("allPairsLength = " + str(allPairsLength))
    if Libraries.defaults.UniswapV2_UseOnlyTheFirstFewExchanges:
        PrintAndLog_FuncNameHeader("UniswapV2_UseOnlyTheFirstFewExchanges is set to True, so we're hard coding allPairsLength to something small while we debug")
        allPairsLength = 5

    maxId = allPairsLength - 1
    PrintAndLog_FuncNameHeader("maxId = " + str(maxId))

    exchangeAddressList_combined = []
    fromId = 0
    while True:
        toId = fromId + chunkSize

        # Enforce the maxId
        if toId > maxId:
            toId = maxId

        tokenIdList = range(fromId, toId + 1)
        PrintAndLog_FuncNameHeader("Calling API_GetManyExchangesWithIds_Batched with fromId = " + str(fromId) + ", toId = " + str(toId) + ", tokenIdList = " + str(tokenIdList))
        exchangeAddressList_combined += API_GetManyExchangesWithIds_Batched(tokenIdList)

        # If we've reached the end of valid exchanges, we'll get a Null Address as a response
        if toId == maxId:
            # if Libraries.core.GetNullAddress() in exchangeAddressList_combined or Libraries.core.GetNullAddress().lower() in exchangeAddressList_combined:
            PrintAndLog_FuncNameHeader("We've reached the end of the Ids. maxId = " + str(maxId))

            exchangeAddressList_combined = list(filter((Libraries.core.GetNullAddress()).__ne__, exchangeAddressList_combined))
            exchangeAddressList_combined = list(filter((Libraries.core.GetNullAddress().lower()).__ne__, exchangeAddressList_combined))
            break

        fromId = toId + 1

    return exchangeAddressList_combined


def API_GetExchangeDataDict():
    global ContractDict_Exchange

    exchangeAddressList = API_GetAllExchangesFromFactory()
    exchangeDataDict_token0 = API_GetManyTokens_Batched_Safe(exchangeAddressList, 'token0')
    # PrintAndLog_FuncNameHeader("exchangeDataDict_token0 = " + str(exchangeDataDict_token0))
    exchangeDataDict_token1 = API_GetManyTokens_Batched_Safe(exchangeAddressList, 'token1')
    # PrintAndLog_FuncNameHeader("exchangeDataDict_token1 = " + str(exchangeDataDict_token1))

    exchangeDataDict = {}
    # {"balance_ether": 0.0009, "decimals": 8, "exchange": "0x56d407f84b04af2b7c659460cb415a40166a6962", "symbol": "BTG", "tokenAddress": "0xffee3b942dfdcbc51007bffffc2a735b0483878f"}
    for exchangeAddress in exchangeAddressList:
        tokenAddress0 = exchangeDataDict_token0[exchangeAddress.lower()]['tokenAddress'].lower()
        tokenAddress1 = exchangeDataDict_token1[exchangeAddress.lower()]['tokenAddress'].lower()

        # We must use something unique for the key, and since token addresses can have multiple exchanges in uniswapV2 we can no longer use the token address.
        # But we can use the combination of token addresses
        key = tokenAddress0 + Separator + tokenAddress1

        try:
            exchangeDataDict[key] = {}
            # PrintAndLog_FuncNameHeader("exchangeDataDict = " + str(exchangeDataDict))
            exchangeDataDict[key]['exchange'] = exchangeAddress.lower()
            exchangeDataDict[key]['tokenAddress0'] = tokenAddress0
            exchangeDataDict[key]['tokenAddress1'] = tokenAddress1
            exchangeDataDict[key]['decimals0'] = Libraries.core.GetDecimalsForTokenContract(exchangeDataDict_token0[exchangeAddress]['tokenAddress'])
            exchangeDataDict[key]['decimals1'] = Libraries.core.GetDecimalsForTokenContract(exchangeDataDict_token1[exchangeAddress]['tokenAddress'])

            exchangeDataDict[key]['symbol0'] = ''
            exchangeDataDict[key]['symbol1'] = ''
            try:
                exchangeDataDict[key]['symbol0'] = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(exchangeDataDict_token0[exchangeAddress]['tokenAddress'])
                exchangeDataDict[key]['symbol1'] = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(exchangeDataDict_token1[exchangeAddress]['tokenAddress'])

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception when getting symbol, " + traceback.format_exc())

            exchangeDataDict[key]['symbolPair'] = exchangeDataDict[key]['symbol0'] + Separator + exchangeDataDict[key]['symbol1']

            # PrintAndLog_FuncNameHeader("exchangeDataDict = " + str(exchangeDataDict))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception with key " + str(key) + ", " + traceback.format_exc())
            # Delete this key and assume something is wrong with this token
            del exchangeDataDict[key]

    # Build an address list from exchangeDataDict and get the ether balance of each exchange
    # exchangeAddressList = list(exchangeDataDict.keys())
    exchangeAddressList = []
    for key in exchangeDataDict:
        exchangeAddressList.append(exchangeDataDict[key]['exchange'])

    tokenAddressList_token0 = []
    tokenAddressList_token1 = []
    decimalsList_token0 = []
    decimalsList_token1 = []
    for key in exchangeDataDict:
        tokenAddressList_token0.append(exchangeDataDict[key]['tokenAddress0'])
        tokenAddressList_token1.append(exchangeDataDict[key]['tokenAddress1'])
        decimalsList_token0.append(float("1e" + str(Libraries.core.GetDecimalsForTokenContract(exchangeDataDict[key]['tokenAddress0']))))
        decimalsList_token1.append(float("1e" + str(Libraries.core.GetDecimalsForTokenContract(exchangeDataDict[key]['tokenAddress1']))))

    tokenBalancesList_token0 = Libraries.core.API_GetTokenBalance_Batched_Safe(exchangeAddressList, tokenAddressList_token0, decimalsList_token0)
    tokenBalancesList_token1 = Libraries.core.API_GetTokenBalance_Batched_Safe(exchangeAddressList, tokenAddressList_token1, decimalsList_token1)
    # PrintAndLog_FuncNameHeader("tokenBalancesList_token0 = " + str(tokenBalancesList_token0))
    # PrintAndLog_FuncNameHeader("tokenBalancesList_token1 = " + str(tokenBalancesList_token1))

    for index, exchangeAddress in enumerate(exchangeAddressList):
        # Find this exchangeAddress in the exchangeDataDict so we can update it's values
        for key in exchangeDataDict:
            if exchangeDataDict[key]['exchange'].lower() == exchangeAddress.lower():
                exchangeDataDict[key]['balance_token0'] = tokenBalancesList_token0[index]
                exchangeDataDict[key]['balance_token1'] = tokenBalancesList_token1[index]
                break

    PrintAndLog_FuncNameHeader("exchangeDataDict = " + str(exchangeDataDict))
    # Update the cache in memory
    ContractDict_Exchange = exchangeDataDict
    # Return the updated exchangeDataDict
    return exchangeDataDict


def API_GetManyExchangesWithIds_Batched(exchangeIdList):
    import Contracts.contracts

    factoryContract = API_GetFactory()

    payload_total = []
    exchangeIdPayloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for exchangeId in exchangeIdList:
        # Increment the id each time around
        payloadId += 1

        # Pair the exchangeId with the Id so I can know which is which in the response
        exchangeIdPayloadIdDict[payloadId] = exchangeId

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_Defiswap_Factory.encodeABI('allPairs', kwargs={'': int(exchangeId)}),
                    "to": factoryContract
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, False, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        resultList = []
        for id in exchangeIdPayloadIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    # id = exchangeIdPayloadIdDict[id]
                    result = Libraries.core.GetAddressFromDataProperty(batchedResult['result'])
                    # PrintAndLog_FuncNameHeader("found Id " + str(id) + " in the response. result = " + str(result))
                    resultList.append(result)
                    break

        if len(resultList) != len(exchangeIdList):
            raise Exception("Length of lists did not match, response data is bad?")

        return resultList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetManyTokens_Batched_Safe(exchangeAddressList, functionName):
    # Make the function call with a large number of items,
    # chunk it into multiple calls so we don't over load the node
    chunkSize = 100
    exchangeAddressList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(exchangeAddressList, chunkSize)

    resultDict = {}
    for exchangeAddressList in exchangeAddressList_chunked:
        local_resultDict = API_GetManyTokens_Batched(exchangeAddressList, functionName)
        resultDict = Libraries.core.MergeDictionaries(resultDict, local_resultDict)

    return resultDict


def API_GetManyTokens_Batched(exchangeAddressList, functionName):
    import Contracts.contracts

    payload_total = []
    exchangeAddressIdDict = {}
    payloadId = randint(0, 99999999999999)
    for exchangeAddress in exchangeAddressList:
        # Increment the id each time around
        payloadId += 1

        # Pair the exchangeAddress with the Id so I can know which is which in the response
        exchangeAddressIdDict[payloadId] = exchangeAddress

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": Contracts.contracts.Contract_UniswapV2_Pair.encodeABI(functionName, kwargs={}),
                    "to": exchangeAddress
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog_FuncNameHeader("payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, True, False, True)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        # PrintAndLog_FuncNameHeader("jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        uniswapV2DataDict = {}
        for id in exchangeAddressIdDict:
            for batchedResult in jData:
                if int(batchedResult['id']) == id:
                    exchangeAddress = exchangeAddressIdDict[id]
                    tokenAddress = Libraries.core.GetAddressFromDataProperty(batchedResult['result'])
                    # PrintAndLog_FuncNameHeader("found Id " + str(id) + " in the response. Using exchangeAddress " + str(
                    #     exchangeAddress) + ", and tokenAddress = " + str(tokenAddress))
                    uniswapV2DataDict[exchangeAddress.lower()] = {
                        "tokenAddress": tokenAddress.lower()
                    }
                    break

        if len(exchangeAddressList) != len(uniswapV2DataDict):
            raise Exception("Length of lists did not match, response data is bad?")

        # PrintAndLog_FuncNameHeader("uniswapV2DataDict = " + str(uniswapV2DataDict))
        return uniswapV2DataDict

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()


def GetExchangeGivenTokenAddresses(tokenAddress0, tokenAddress1):
    global ContractDict_Exchange

    key = GetKeyGivenTokenAddresses(tokenAddress0, tokenAddress1)
    if key:
        return ContractDict_Exchange[key]['exchange']
    else:
        return None


def GetKeyGivenTokenAddresses(tokenAddress0, tokenAddress1):
    global ContractDict_Exchange

    from Libraries.exchanges import EnforceTokenAddressIsERC20Version

    # This exchange can trade ETH, but it still expects the path variable to contain WETH address despite supporting native ETH
    tokenAddress0 = EnforceTokenAddressIsERC20Version(tokenAddress0)
    tokenAddress1 = EnforceTokenAddressIsERC20Version(tokenAddress1)

    key = DetermineContractDictKeyGivenTokens(tokenAddress0, tokenAddress1)
    return key


def IsSpendTokenExchangesToken0(spendToken, receiveToken):
    global ContractDict_Exchange

    key = GetKeyGivenTokenAddresses(spendToken, receiveToken)
    if spendToken.lower() == ContractDict_Exchange[key]['tokenAddress0'].lower():
        return True
    else:
        return False


def IsSpendTokenExchangesToken1(spendToken, receiveToken):
    global ContractDict_Exchange

    key = GetKeyGivenTokenAddresses(spendToken, receiveToken)
    if spendToken.lower() == ContractDict_Exchange[key]['tokenAddress1'].lower():
        return True
    else:
        return False


def GetTokenAddressesGivenExchange(exchange):
    global ContractDict_Exchange

    for key in ContractDict_Exchange:
        if ContractDict_Exchange[key]['exchange'].lower() == exchange.lower():
            exchangeData = ContractDict_Exchange[key]
            return exchangeData['tokenAddress0'], exchangeData['tokenAddress1']


def DetermineContractDictKeyGivenTokens(tokenAddress0, tokenAddress1):
    global ContractDict_Exchange

    # UniswapV2 exchangers have two tokens, we need to figure out which one is tokenAddress0 and which is tokenAddress1

    keyPossibility1 = tokenAddress0.lower() + Separator + tokenAddress1.lower()
    keyPossibility2 = tokenAddress1.lower() + Separator + tokenAddress0.lower()
    # PrintAndLog_FuncNameHeader("keyPossibility1 = " + str(keyPossibility1))
    # PrintAndLog_FuncNameHeader("keyPossibility2 = " + str(keyPossibility2))

    if keyPossibility1 in ContractDict_Exchange and keyPossibility2 in ContractDict_Exchange:
        message = "Found both possible combinations of keys in UniswapV2. I should never see this? What's broken? tokenAddress0 = " + str(
            tokenAddress0) + ", tokenAddress1 = " + str(tokenAddress1)
        Libraries.core.API_PostOperatorNotification(message)
        raise Exception(message)

    elif keyPossibility1 in ContractDict_Exchange:
        return keyPossibility1

    elif keyPossibility2 in ContractDict_Exchange:
        return keyPossibility2

    else:
        # PrintAndLog_FuncNameHeader("Could not find a UniswapV2 exchange contract for tokens " + str(
        #     tokenAddress0) + " and " + str(tokenAddress1))
        return None


def DetermineContractDictBalanceGivenTokenAndKey(key, tokenAddress):
    global ContractDict_Exchange

    if tokenAddress.lower() == ContractDict_Exchange[key]['tokenAddress0']:
        return ContractDict_Exchange[key]['balance_token0']
    elif tokenAddress.lower() == ContractDict_Exchange[key]['tokenAddress1']:
        return ContractDict_Exchange[key]['balance_token1']
    else:
        raise Exception("DetermineContractDictBalanceGivenTokenAndKey: Could not find balance for key. Invalid key? key = " + str(key))


def API_GetManyExchangeTokenBalances(uniswapV2ContractList,
                                     tokensList_quoteToken, decimalsList_quoteTokens,
                                     tokensList_baseToken, decimalsList_baseTokens, blockNumber=None):
    # In order to determine UniswapV2 market prices, I need to get the token balances of the UniswapV2 contracts, then it's simple math

    threads = []
    resultsDict = {}

    key_quoteTokenBalances = "quoteTokenBalances"
    t = threading.Thread(
        target=Libraries.core.API_GetTokenBalance_Batched_Safe,
        args=(uniswapV2ContractList, tokensList_quoteToken, decimalsList_quoteTokens, resultsDict, key_quoteTokenBalances, blockNumber))
    threads.append(t)
    t.start()

    key_baseTokenBalances = "baseTokenBalances"
    t = threading.Thread(
        target=Libraries.core.API_GetTokenBalance_Batched_Safe,
        args=(uniswapV2ContractList, tokensList_baseToken, decimalsList_baseTokens, resultsDict, key_baseTokenBalances, blockNumber))
    threads.append(t)
    t.start()

    for thread in threads:
        thread.join()

    balanceList_quoteTokens_UniswapV2Contract = resultsDict[key_quoteTokenBalances]
    balanceList_baseTokens_UniswapV2Contract = resultsDict[key_baseTokenBalances]
    # PrintAndLog_FuncNameHeader("balanceList_quoteTokens_UniswapV2Contract = " + str(balanceList_quoteTokens_UniswapV2Contract))
    # PrintAndLog_FuncNameHeader("balanceList_baseTokens_UniswapV2Contract = " + str(balanceList_baseTokens_UniswapV2Contract))

    return balanceList_quoteTokens_UniswapV2Contract, balanceList_baseTokens_UniswapV2Contract


def GetListOfTokensWhoseExchangesHaveAtLeastSomeDecentBalance(quoteTokenList):
    global ContractDict_Exchange

    from Libraries.exchanges import EnforceTokenAddressIsERC20Version

    # The purpose of this function is to get a list of tokens on exchange
    # that have an exchange and that have some kind of balance on them that's tradeable.
    # So not dust and greater than zero.

    # Start by getting all the possible tokens
    allTokenList = []
    for key in ContractDict_Exchange:
        tokenAddress0 = ContractDict_Exchange[key]['tokenAddress0']
        tokenAddress1 = ContractDict_Exchange[key]['tokenAddress1']

        if tokenAddress0.lower() not in allTokenList:
            allTokenList.append(tokenAddress0.lower())

        if tokenAddress1.lower() not in allTokenList:
            allTokenList.append(tokenAddress1.lower())

    # PrintAndLog_FuncNameHeader("quoteTokenList has " + str(len(quoteTokenList)) + " items. quoteTokenList = " + str(quoteTokenList))
    # PrintAndLog_FuncNameHeader("allTokenList has " + str(len(allTokenList)) + " items. allTokenList = " + str(allTokenList))

    tokenListToReturn = []
    # Iterate over all the quoteTokens in quoteTokenList
    for quoteToken in quoteTokenList:
        # Make sure the exchange has assets worth even considering a trade
        quoteTokenBalanceThreshold = Exchanges.keeperDAO.GetExchangeMinQuoteTokenBalanceRequirementDict()[quoteToken.lower()]
        # PrintAndLog_FuncNameHeader("quoteTokenBalanceThreshold = " + str(quoteTokenBalanceThreshold) + " for " + str(quoteToken))
        quoteToken_toUseForThisExchange = EnforceTokenAddressIsERC20Version(quoteToken)

        for baseToken in allTokenList:
            # Skip tokens already added to the list
            if baseToken.lower() in tokenListToReturn:
                continue

            # cannot trade the same token with itself
            if baseToken.lower() == quoteToken_toUseForThisExchange.lower():
                continue

            key = DetermineContractDictKeyGivenTokens(quoteToken_toUseForThisExchange, baseToken)
            # Skip the pairs that do not exist
            if not key:
                continue

            # PrintAndLog_FuncNameHeader("key for the UniswapV2 exchange with quoteToken_toUseForThisExchange = " + str(
            #     quoteToken_toUseForThisExchange) + " and baseToken = " + str(baseToken) + ", key = " + str(key))
            quoteTokenBalance = DetermineContractDictBalanceGivenTokenAndKey(key, quoteToken_toUseForThisExchange)
            # PrintAndLog_FuncNameHeader("quoteTokenBalance = " + str(quoteTokenBalance))

            # Require that the quoteTokenBalance exceeds the threshold so we know this token is worth trading
            if float(quoteTokenBalance) > quoteTokenBalanceThreshold:
                # it's worth trading so add to the list
                tokenListToReturn.append(baseToken.lower())

    PrintAndLog_FuncNameHeader("UniswapV2: of the " + str(len(allTokenList)) + " tokens on UniswapV2, " + str(
        len(tokenListToReturn)) + " of them have a balance greater than the respective quoteTokenBalanceThreshold")
    return tokenListToReturn

    # # PrintAndLog_FuncNameHeader("UniswapV2: allTokenList = " + str(allTokenList))
    # PrintAndLog_FuncNameHeader("UniswapV2: of the " + str(len(allTokenList)) + " tokens on UniswapV2, we are returning all of them!")
    # return allTokenList


def API_GetReserves(exchange):
    import Contracts.contracts

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": Contracts.contracts.Contract_UniswapV2_Pair.encodeABI('getReserves', kwargs={}),
                "to": Libraries.nodes.Instance_Web3.toChecksumAddress(exchange),
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_short_seconds)
    if response.ok:
        responseData = response.content
        jData = json.loads(responseData)
        result = jData['result']
        PrintAndLog_FuncNameHeader("result = " + str(result))

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog_FuncNameHeader("response was not ok response = " + str(response))
        response.raise_for_status()
