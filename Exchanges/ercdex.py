from Libraries.loggingConfig import PrintAndLog, PrintAndLogError, PrintAndLog_Websockets
import Libraries.core
import Libraries.signingUtils

import Exchanges.zrxV2

import requests
import json
import random
import string
from Naked.toolshed.shell import muterun_js
import sys
import traceback
import websocket
import time
import threading
import copy
from threading import Lock

# URL_REST_ErcDex_Base = "https://kovan-staging.ercdex.com/api/v1/"
# URL_REST_ErcDex_Base = "https://staging.ercdex.com/api/v2/"
# URL_WS_ErcDex_Base = "wss://staging.ercdex.com/ws"
URL_REST_ErcDex_Base = "https://app.ercdex.com/api/v2/"
URL_WS_ErcDex_Base = "wss://app.ercdex.com/ws"

WebsocketClient = None
# Keep track of what i'm subscribing to so that in the case of a disconnect-reconnect, i can just re-subscribe to stuff.
WebsocketSubscriptionsList = []
# Occasionally ErcDex can remove an order of mine.  If they do that, I need to remove the local copy of my orders so I can re-submit.
OrderHashesRemoved_ByErcDex = []
Lock_OrderHashesRemoved_ByErcDex = Lock()


def API_GetAssetPairs():
    response = requests.get(URL_REST_ErcDex_Base + "asset_pairs", headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetAssetPairs responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetAssetPairs jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetAssetPairs response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetAggregatedOrders(baseSymbol, quoteSymbol):
    url = URL_REST_ErcDex_Base + "aggregated_orders?baseSymbol=" + str(baseSymbol) + "&quoteSymbol=" + str(quoteSymbol)
    # PrintAndLog("API_GetAggregatedOrders url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetAggregatedOrders responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetAggregatedOrders jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetAggregatedOrders response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetOrderConfig(makerAddress, takerAddress, makerAssetAmount, takerAssetAmount, makerTokenAddress, takerTokenAddress, exchangeAddress):
    # Convert the makerTokenAddress and takerTokenAddress to makerAssetData and takerAssetData
    makerAssetData = Exchanges.zrxV2.EncodeERC20AssetData(makerTokenAddress)
    takerAssetData = Exchanges.zrxV2.EncodeERC20AssetData(takerTokenAddress)

    url = URL_REST_ErcDex_Base + "order_config?makerAddress=" + str(makerAddress) + "&takerAddress=" + str(takerAddress) + "&makerAssetAmount=" + str(makerAssetAmount) + \
          "&takerAssetAmount=" + str(takerAssetAmount) + "&makerAssetData=" + str(makerAssetData) + "&takerAssetData=" + str(takerAssetData) + "&exchangeAddress=" + str(exchangeAddress)
    # PrintAndLog("API_GetOrderConfig url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetOrderConfig responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetOrderConfig jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetOrderConfig response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_CancelOrder(orderHashList, makerAddressList, fromPrivateKeyList):
    # Create a list of cancellations.  I'll need to sign each cancellations
    cancellations = []
    for index, orderHash in enumerate(orderHashList):
        argumentsToPass = str(orderHash.lower()) + "," + str(makerAddressList[index].lower()) + "," + str(fromPrivateKeyList[index])

        response = muterun_js('../Arby_0xv2Signer/src/cancel-order.js', arguments=argumentsToPass)

        # Trash the fromPrivateKey in memory
        fromPrivateKeyList[index] = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

        # PrintAndLog("response = " + str(response))
        responseSplit = None
        if response.exitcode == 0:
            # PrintAndLog(str(response.stdout))
            responseSplit = response.stdout.decode('utf-8').split('\n')
        else:
            sys.stderr.write(response.stderr.decode("utf-8"))

        # PrintAndLog("responseSplit[0] = " + str(responseSplit[0]))
        signature = responseSplit[0]
        # PrintAndLog("signature = " + str(signature))

        cancellations.append({
            'orderHash': orderHash,
            'signature': signature,
        })

    payload = {
        'cancellations': cancellations
    }
    PrintAndLog("API_CancelOrder Exchange_ErcDex payload = " + str(payload))
    response = requests.post(URL_REST_ErcDex_Base + "orders/cancel", data=json.dumps(payload), headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_CancelOrder responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_CancelOrder Exchange_ErcDex jData = " + str(jData))
        # [{'orderHash': '0x9a566654a4a9a3dbe78b34a6855d7566d5f8da9ae1621cfffcf962ed427b9550', 'success': True, 'message': 'order canceled in 41ms'}]

        # Use a dictionary to keep track of what cancel succeeded vs what cancel failed
        cancelSuccessDict = {}
        # init the dict to all false for each orderHash
        for index, orderHash in enumerate(orderHashList):
            cancelSuccessDict[orderHash] = False

        for canceledOrder in jData:
            cancelSuccessDict[canceledOrder['orderHash']] = canceledOrder['success']

        # Check to see if all entries are successful
        allCanceslSucceeded = True
        for orderHash in cancelSuccessDict:
            if not cancelSuccessDict[orderHash]:
                allCanceslSucceeded = False
                break

        PrintAndLog("API_CancelOrder Exchange_ErcDex allCanceslSucceeded = " + str(allCanceslSucceeded))
        return allCanceslSucceeded

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_CancelOrder response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_PostOrder(order):
    # argumentsToPass = str(order.exchangeContractAddress.lower()) + "," + str(order.maker.lower()) + "," + str(order.makerTokenAddress.lower()) + "," + \
    # str(order.takerTokenAddress.lower()) + "," + str(order.feeRecipient.lower()) + "," + str(order.makerTokenAmount) + "," + str(order.takerTokenAmount) + "," + \
    # str(order.makerFee) + "," + str(order.takerFee) + "," + str(order.expirationUnixTimestampSec) + "," + str(order.senderAddress.lower()) + "," + str(fromPrivateKey)

    # Convert the makerTokenAddress and takerTokenAddress to makerAssetData and takerAssetData
    makerAssetData = Exchanges.zrxV2.EncodeERC20AssetData(order.makerTokenAddress)
    takerAssetData = Exchanges.zrxV2.EncodeERC20AssetData(order.takerTokenAddress)

    payload = {
        'makerAddress': str(order.maker).lower(),
        'takerAddress': str(order.taker).lower(),
        'feeRecipientAddress': str(order.feeRecipient).lower(),
        'senderAddress': str(order.senderAddress).lower(),
        'makerAssetAmount': str(order.makerTokenAmount),
        'takerAssetAmount': str(order.takerTokenAmount),
        'makerFee': str(order.makerFee),
        'takerFee': str(order.takerFee),
        'makerAssetData': str(makerAssetData).lower(),
        'takerAssetData': str(takerAssetData).lower(),
        'salt': str(order.salt),
        'exchangeAddress': str(order.exchangeContractAddress).lower(),
        'expirationTimeSeconds': str(order.expirationUnixTimestampSec),
        'signature': str(order.signature),
    }
    PrintAndLog("API_PostOrder Exchange_ErcDex payload = " + str(payload))
    response = requests.post(URL_REST_ErcDex_Base + "order", data=json.dumps(payload), headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_PostOrder responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_PostOrder Exchange_ErcDex jData = " + str(jData))
        # States for the order are // State of the order: Open (0), Canceled (1), Filled (2), Expired (3), Removed (4)
        success = False
        orderState = int(jData['state'])
        if orderState == 0 or orderState == 2:
            success = True

        PrintAndLog("API_PostOrder Exchange_ErcDex for maker address = " + str(str(order.maker)) + ", orderState = " + str(orderState) + ", success = " + str(success))
        return success

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_PostOrder response was not ok for maker address = " + str(str(order.maker)) + ", response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_GetOpenOrders_ForThisAddress(traderAddress):
    # PrintAndLog("API_GetOpenOrders_ForThisAddress traderAddress = " + str(traderAddress))
    url = URL_REST_ErcDex_Base + "orders?open=" + str(True) + "&traderAddress=" + str(traderAddress).lower()
    # PrintAndLog("API_GetOpenOrders url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetOpenOrders responseData = " + str(responseData))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetOpenOrders jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetOpenOrders response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def API_CancelAllOpenOrders_ForThisAddress(traderAddress, traderPrivateKey):
    openOrders_jData = API_GetOpenOrders_ForThisAddress(traderAddress)

    orderHashList = []
    makerAddressList = []
    makerPrivateKeyList = []
    for record in openOrders_jData['records']:
        orderHash = record['order']['orderHash']
        orderHashList.append(orderHash)
        makerAddressList.append(traderAddress)
        makerPrivateKeyList.append(traderPrivateKey)

    if len(orderHashList) > 0:
        PrintAndLog("API_CancelAllOpenOrders_ForThisAddress: Preparing to cancel " + str(len(orderHashList)) + " open orders: orderHashList = " + str(orderHashList))
        API_CancelOrder(orderHashList, makerAddressList, makerPrivateKeyList)
    else:
        PrintAndLog("API_CancelAllOpenOrders_ForThisAddress: No open orders to cancel")


def API_GetNotifications(traderAddress):
    url = URL_REST_ErcDex_Base + "notifications?account=" + str(traderAddress).lower()
    # PrintAndLog("API_GetNotifications url = " + str(url))
    response = requests.get(url, headers=Libraries.core.Headers, timeout=Libraries.core.RequestTimeout_long_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetNotifications responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("API_GetNotifications jData = " + str(jData))
        return jData

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetNotifications response was not ok response = " + str(response) + ", " + str(response.content))
        response.raise_for_status()

    return None


def ConnectWebSocketClient():
    global WebsocketClient
    WebsocketClient = WebsocketClientObject()


class WebsocketClientObject(object):
    ws = None

    def __init__(self, interval=10):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

        thread_keepAlive = threading.Thread(target=self.keepAlive, args=())
        thread_keepAlive.daemon = True
        thread_keepAlive.start()

    def run(self):
        """ Method that runs forever """

        websocket.enableTrace(False)
        websocket.http_proxy_host = URL_WS_ErcDex_Base
        self.ws = websocket.WebSocketApp(URL_WS_ErcDex_Base,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)
        while True:
            self.ws.run_forever()
            PrintAndLog("I was disconnected from websockets!  Sleeping a bit then trying again")
            time.sleep(30)

    def keepAlive(self):
        while True:
            time.sleep(1)
            try:
                # keep alive for the server
                self.ws.send('ping')
            except:
                PrintAndLogError("exception in WebsocketClientObject: " + traceback.format_exc())
                pass

            time.sleep(40)

    def on_message(self, ws, message):
        PrintAndLog_Websockets("Websockets ErcDex, data received: " + message[0:90] + "....." + message[-25:])
        try:
            if message == 'pong' or message == '"pong"' or message == "'pong'":
                PrintAndLog_Websockets("Received keep alive pong from server")
            else:
                jData = json.loads(message)
                PrintAndLog_Websockets("jData = " + str(jData))

                # Check for the case where ErcDex removed one of my orders for whatever reason. If they do that, I need to know so I can handle it.
                PrintAndLog_Websockets("jData['data']['eventType'] = " + str(jData['data']['eventType']))
                if jData['data']['eventType'] == 'removed':
                    AddOrderHashTo_OrderHashesRemoved_ByErcDex(jData['data']['order']['orderHash'])

        except:
            PrintAndLogError("exception in WebsocketClientObject: " + traceback.format_exc())
            pass

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws):
        message = "ErcDex: Disconnected from websockets. Attempting to re-subscribe to all the endpoints"
        PrintAndLogError(message)
        Libraries.core.API_PostOperatorNotification(message)
        ResubscribeWebsocketsToAllEndpoints()


def SubscribeWebsockets_OrderEvents(addressList):
    ClearWebsocketSubscriptionsList()

    # Subscribe to order change events
    for address in addressList:
        PrintAndLog("Subscribing to order events for " + address)
        subscriptionEndpoint = "account-order-change/" + address.lower()
        SubscribeWebsocketsToEndpoint(subscriptionEndpoint)
        AddSubscriptionEndpointTo_WebsocketSubscriptionsList(subscriptionEndpoint)


def SubscribeWebsocketsToEndpoint(subscriptionEndpoint):
    global WebsocketClient
    PrintAndLog("Subscribing to subscriptionEndpoint = " + str(subscriptionEndpoint))
    WebsocketClient.ws.send("sub:" + subscriptionEndpoint)


def AddSubscriptionEndpointTo_WebsocketSubscriptionsList(subscriptionEndpoint):
    global WebsocketSubscriptionsList
    WebsocketSubscriptionsList.append(subscriptionEndpoint)


def ClearWebsocketSubscriptionsList():
    global WebsocketSubscriptionsList
    WebsocketSubscriptionsList = []


def ResubscribeWebsocketsToAllEndpoints():
    global WebsocketSubscriptionsList

    # PrintAndLog("ResubscribeWebsocketsToAllEndpoints: resubscribing to " + str(len(WebsocketSubscriptionsList)) + " websocket endpoints")
    for subscriptionEndpoint in WebsocketSubscriptionsList:
        SubscribeWebsocketsToEndpoint(subscriptionEndpoint)


def AddOrderHashTo_OrderHashesRemoved_ByErcDex(orderHash):
    global Lock_OrderHashesRemoved_ByErcDex
    global OrderHashesRemoved_ByErcDex

    PrintAndLog("AddOrderHashTo_OrderHashesRemoved_ByErcDex adding " + str(orderHash) + " to OrderHashesRemoved_ByErcDex")

    Lock_OrderHashesRemoved_ByErcDex.acquire()
    try:
        OrderHashesRemoved_ByErcDex.append(orderHash.lower())
    finally:
        Lock_OrderHashesRemoved_ByErcDex.release()


def GetThreadSafeCopyOf_OrderHashesRemoved_ByErcDex():
    global Lock_OrderHashesRemoved_ByErcDex
    global OrderHashesRemoved_ByErcDex

    copy_OrderHashesRemoved_ByErcDex = None

    Lock_OrderHashesRemoved_ByErcDex.acquire()
    try:
        copy_OrderHashesRemoved_ByErcDex = copy.deepcopy(OrderHashesRemoved_ByErcDex)
    finally:
        Lock_OrderHashesRemoved_ByErcDex.release()

    return copy_OrderHashesRemoved_ByErcDex


def IsOrderHashIn_OrderHashesRemoved_ByErcDex(orderHash):
    copy_OrderHashesRemoved_ByErcDex = GetThreadSafeCopyOf_OrderHashesRemoved_ByErcDex()
    if len(copy_OrderHashesRemoved_ByErcDex) > 0 and orderHash.lower() in copy_OrderHashesRemoved_ByErcDex:
        return True
    else:
        return False
