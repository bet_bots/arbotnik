import json
import random
import string
from Naked.toolshed.shell import muterun_js
import sys
from random import randint
from web3 import Web3
from eth_abi import encode_single
from enum import Enum

from Libraries.loggingConfig import PrintAndLog, PrintAndLog_FuncNameHeader
import Libraries.core
import Libraries.signingUtils
import Libraries.orders
import Libraries.ninjaEncoding
import Libraries.nodes
import Libraries.network
import Libraries.exceptions

MAINNET_Contract_WETH = Libraries.nodes.Instance_Web3.toChecksumAddress("0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2")
MAINNET_ZRX_TokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress("0xe41d2489571d322189246dafa5ebde1f4699f498")

# New 0xv3
MAINNET_Contract_Exchange = Libraries.nodes.Instance_Web3.toChecksumAddress("0x61935cbdd02287b511119ddb11aeb42f1593b7ef")
MAINNET_Contract_ERC20Proxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0x95e6f48254609a6ee006f7d493c8e5fb97094cef")

# New 0xv2.1 after emergency upgrade July 12 2019
# MAINNET_Contract_Exchange = Libraries.nodes.Instance_Web3.toChecksumAddress("0x080bf510fcbf18b91105470639e9561022937712")
# MAINNET_Contract_ERC20Proxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0x95e6f48254609a6ee006f7d493c8e5fb97094cef")

# Old 0xv2 Before exploit from July 12 2019
# MAINNET_Contract_Exchange = Libraries.nodes.Instance_Web3.toChecksumAddress("0x4f833a24e1f95d70f028921e27040ca56e09ab0b")
# MAINNET_Contract_ERC20Proxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0x2240dab907db71e64d3e0dba4800c83b5c502d4e")

MAINNET_MLN_TokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress("0xBEB9eF514a379B997e0798FDcC901Ee474B6D9A1")

KOVANTESTNET_Contract_WETH = Libraries.nodes.Instance_Web3.toChecksumAddress("0xd0a1e359811322d97991e03f863a0c30c2cf029c")
KOVANTESTNET_Contract_Exchange = Libraries.nodes.Instance_Web3.toChecksumAddress("0x35dd2932454449b14cee11a94d3674a936d5d7b2")
KOVANTESTNET_Contract_ERC20Proxy = Libraries.nodes.Instance_Web3.toChecksumAddress("0xf1ec01d6236d3cd881a0bf0130ea25fe4234003e")
KOVANTESTNET_ZRX_TokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress("0x2002d3812f58e35f0ea1ffbf80a75a38c32175fa")
KOVANTESTNET_MLN_TokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress("0x323b5d4c32345ced77393b3530b1eed0f346429d")
KOVANTESTNET_REP_TokenAddress = Libraries.nodes.Instance_Web3.toChecksumAddress("0xb18845c260f680d5b9d84649638813e342e4f8c9")

ZrxMeshFeeRecipientAddress = "0x1000000000000000000000000000000000000011"

# 0xv3
OrderTupleString_Contents = 'address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes,bytes,bytes'
OrderTupleString = '(' + OrderTupleString_Contents + ')'
OrderTupleStringArray = '(' + OrderTupleString_Contents + ')[]'

# 0xv3 encoded, when things are encoded they typically become a uint256 or uint256[]
OrderTupleString_Contents_Encoded = 'uint256,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,uint256[],uint256[],bytes,bytes'
OrderTupleString_Encoded = '(' + OrderTupleString_Contents_Encoded + ')'
OrderTupleStringArray_Encoded = '(' + OrderTupleString_Contents_Encoded + ')[]'

# 0xv2
# OrderTupleString = '(address,address,address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes,bytes)'

# Set these by calling SetNetwork
Network = None
Contract_WETH = None
Contract_Exchange = None
Contract_ERC20Proxy = None
ZRX_TokenAddress = None
MLN_TokenAddress = None

ProtocolFeeMultiplier_weiUnits = 70000


class SignatureType(Enum):
    # https://github.com/0xProject/0x-protocol-specification/blob/master/v2/v2-specification.md#signature-types
    Illegal = "0x00"
    Invalid = "0x01"
    EIP712 = "0x02"
    EthSign = "0x03"
    Wallet = "0x04"
    Validator = "0x05"
    PreSigned = "0x06"


class OrderState(Enum):
    Open = 3
    # TODO, others of interest??


def AppendSignatureTypeToSignatureString(signatureString, signatureType):
    return signatureString + Libraries.core.Remove0XfromHexString(signatureType.value)


def SetNetwork(networkToSet):
    global Network
    global Contract_WETH
    global Contract_Exchange
    global Contract_ERC20Proxy
    global ZRX_TokenAddress
    global MLN_TokenAddress

    global MAINNET_Contract_WETH
    global MAINNET_Contract_Exchange
    global MAINNET_Contract_ERC20Proxy
    global MAINNET_ZRX_TokenAddress
    global MAINNET_MLN_TokenAddress

    global KOVANTESTNET_Contract_WETH
    global KOVANTESTNET_Contract_Exchange
    global KOVANTESTNET_Contract_ERC20Proxy
    global KOVANTESTNET_ZRX_TokenAddress
    global KOVANTESTNET_MLN_TokenAddress

    Network = networkToSet
    if Network == Libraries.network.Network.mainnet:
        Contract_WETH = MAINNET_Contract_WETH
        Contract_Exchange = MAINNET_Contract_Exchange
        Contract_ERC20Proxy = MAINNET_Contract_ERC20Proxy
        ZRX_TokenAddress = MAINNET_ZRX_TokenAddress
        MLN_TokenAddress = MAINNET_MLN_TokenAddress

    elif Network == Libraries.network.Network.kovanTestnet:
        Contract_WETH = KOVANTESTNET_Contract_WETH
        Contract_Exchange = KOVANTESTNET_Contract_Exchange
        Contract_ERC20Proxy = KOVANTESTNET_Contract_ERC20Proxy
        ZRX_TokenAddress = KOVANTESTNET_ZRX_TokenAddress
        MLN_TokenAddress = KOVANTESTNET_MLN_TokenAddress

    PrintAndLog("Now using " + str(Network.value))
    PrintAndLog("Contract_WETH set to: " + str(Contract_WETH))
    PrintAndLog("Contract_Exchange set to: " + str(Contract_Exchange))
    PrintAndLog("Contract_ERC20Proxy set to: " + str(Contract_ERC20Proxy))
    PrintAndLog("ZRX_TokenAddress set to: " + str(ZRX_TokenAddress))
    PrintAndLog("MLN_TokenAddress set to: " + str(MLN_TokenAddress))


def EncodeERC20AssetData(address):
    # https://github.com/0xProject/standard-relayer-api#asset-data-encoding
    # // ERC20 Proxy ID  0xf47261b0
    # bytes4(keccak256("ERC20Token(address)"))
    # // ERC721 Proxy ID 0x08e937fa
    # bytes4(keccak256("ERC721Token(address,uint256)"))
    # encodedAddress = 0xf47261b000000000000000000000000041e5560054824ea6b0732e656e3ad64e20e94e45

    nullAddress = '0x'.lower()
    if address.lower() == nullAddress:
        return nullAddress
    else:
        addressWith0xRemoved = Libraries.core.Remove0XfromHexString(address)
        padding = Libraries.core.LengthOfDataProperty - len(addressWith0xRemoved)
        encodedAddress = '0xf47261b0' + ('0' * padding) + addressWith0xRemoved
        encodedAddress = encodedAddress.lower()

        # PrintAndLog("encodedAddress = " + str(encodedAddress))
        return encodedAddress


def GetZeroedOutAssetData():
    return '0x' + ('0' * 72)


def ExtractAssetAddressFromAssetData(assetData):
    # Some 0x relayers use '0x' as the assetData, make sure to remove that first
    assetData = assetData.replace("0x", "")
    # Then mask off any extra data leading ahead of the address contents
    addressWithout0x = assetData[-Libraries.core.LengthOfPublicAddress_Excludes0x:]
    # PrintAndLog("ExtractAssetAddressFromAssetData: addressWithout0x = " + str(addressWithout0x))
    return '0x' + addressWithout0x


def SignOrder(order, fromPrivateKey):
    argumentsToPass = str(order.exchangeContractAddress.lower()) + "," + str(order.maker.lower()) + "," + str(order.makerTokenAddress.lower()) + "," + \
                      str(order.takerTokenAddress.lower()) + "," + str(order.taker.lower()) + "," + str(order.feeRecipient.lower()) + "," + \
                      str(order.makerTokenAmount) + "," + str(order.takerTokenAmount) + "," + str(order.makerFee) + "," + str(order.takerFee) + "," + \
                      str(order.expirationUnixTimestampSec) + "," + str(order.senderAddress.lower()) + "," + str(order.salt) + "," + str(fromPrivateKey)

    print('argumentsToPass = ', argumentsToPass)
    response = muterun_js('../Arby_0xv2Signer/src/sign-order.js', arguments=argumentsToPass)

    # Trash the fromPrivateKey in memory
    fromPrivateKey = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(101))

    PrintAndLog("SignOrder: response = " + str(response))
    responseSplit = None
    if response.exitcode == 0:
        PrintAndLog(str(response.stdout))
        responseSplit = response.stdout.decode('utf-8').split('\n')
    else:
        sys.stderr.write(response.stderr.decode("utf-8"))

    # PrintAndLog("responseSplit[0] = " + str(responseSplit[0]))
    responseSplitByComma = responseSplit[0].split(',')

    order.signature = responseSplitByComma[0]
    order.hash = responseSplitByComma[1]
    # PrintAndLog("signature = " + str(order.signature))
    # PrintAndLog("hash = " + str(order.hash))


def GetOrderInfo(order):
    method_signature = Web3.sha3(text=f"getOrderInfo({OrderTupleString})")[0:4]
    method_parameters = encode_single(f"({OrderTupleString})", [order.GenerateOrderTupleForTransaction()])
    data = '0x' + (method_signature + method_parameters).hex()

    PrintAndLog("GetOrderInfo method_signature = " + str(method_signature))
    PrintAndLog("GetOrderInfo method_parameters = " + str(method_parameters))
    PrintAndLog("GetOrderInfo data = " + str(data))

    payload = {
        "jsonrpc": "2.0",
        "method": "eth_call",
        "params": [
            {
                "data": data,
                "to": Contract_Exchange
            },
        ]
    }
    response = Libraries.core.SendRequestToAllNodes(payload, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds)
    if response.ok:
        responseData = response.content
        # PrintAndLog("GetOrderInfo Exchange_0xv2 responseData = " + str(responseData))
        jData = json.loads(responseData)
        PrintAndLog("GetOrderInfo Exchange_0xv2 jData = " + str(jData))

        # filled_etherUnits = Libraries.core.ConvertWeiToEther(int(result, 16), decimals)
        # PrintAndLog("GetOrderInfo Exchange_0xv2 balance = " + str(filled_etherUnits) + " " + marketName)
        # resultDict[resultKey] = filled_etherUnits
        # return filled_etherUnits

        orderInfoResult = jData['result']
        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(orderInfoResult), Libraries.core.LengthOfDataProperty)
        # PrintAndLog("dataList = " + str(dataList))
        orderState = int(dataList[0])
        orderHash = '0x' + str(dataList[1])
        orderTakerAmountFilled_weiUnits = Libraries.core.ConvertHexToInt(dataList[2])
        PrintAndLog("orderState = " + str(orderState) + ", orderTakerAmountFilled_weiUnits = " + str(
            orderTakerAmountFilled_weiUnits) + ", orderHash = " + str(orderHash))
        return orderState, orderHash, orderTakerAmountFilled_weiUnits

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("GetOrderInfo Exchange_0xv2 response was not ok response = " + str(response))
        response.raise_for_status()


def API_GetOrderInfo_Batched_Safe(orderList, resultKey=None, resultsDict=None):
    # This is a rate limiting optimization
    # I'm splitting up the arguments to multiple calls because remote nodes do not let me send an infinite number of arguments to one single call.
    chunkSize = 80  # 100 works, 130 breaks, use less than 100 to be safe

    orderList_chunked = Libraries.core.SplitListIntoChunks_OfSizeNoGreaterThanThis(orderList, chunkSize)

    concatenatedList = []
    for i in range(len(orderList_chunked)):
        concatenatedList += API_GetOrderInfo_Batched(orderList_chunked[i])

    if resultKey:
        resultsDict[resultKey] = concatenatedList

    return concatenatedList


def API_GetOrderInfo_Batched(orderList):
    payload_total = []
    payloadIdDict = {}
    payloadId = randint(0, 99999999999999)
    for order in orderList:
        # Increment the id each time around
        payloadId += 1

        # Pair the value with the Id so I can know which is which in the response
        payloadIdDict[payloadId] = order

        method_signature = Web3.sha3(text=f"getOrderInfo({OrderTupleString})")[0:4]
        method_parameters = encode_single(f"({OrderTupleString})", [order.GenerateOrderTupleForTransaction()])
        data = '0x' + (method_signature + method_parameters).hex()

        # PrintAndLog("API_GetOrderInfo_Batched method_signature = " + str(method_signature))
        # PrintAndLog("API_GetOrderInfo_Batched method_parameters = " + str(method_parameters))
        # PrintAndLog("API_GetOrderInfo_Batched data = " + str(data))

        payload = {
            "id": payloadId,
            "jsonrpc": "2.0",
            "method": "eth_call",
            "params": [
                {
                    "data": data,
                    "to": Contract_Exchange
                },
            ]
        }
        payload_total.append(payload)

    # PrintAndLog("API_GetOrderInfo_Batched: payload_total = " + str(payload_total))
    response = Libraries.core.SendRequestToAllNodes(payload_total, Libraries.core.Headers, Libraries.core.RequestTimeout_seconds, None, True, False, True, True)
    if response.ok:
        responseData = response.content
        # PrintAndLog("API_GetOrderInfo_Batched Exchange_0xv2 responseData = " + str(responseData))
        PrintAndLog_FuncNameHeader("Exchange_0xv2 response duration = " + str(response.elapsed.total_seconds()))
        jData = json.loads(responseData)
        # PrintAndLog("API_GetOrderInfo_Batched Exchange_0xv2 jData = " + str(jData))

        # Match the ID from the payload with the ID in the response in case the batched response comes in a strange order
        returnList = []
        for id in payloadIdDict:
            # PrintAndLog("id = " + str(id))
            for batchedResult in jData:
                # PrintAndLog("searching batchedResult to find id " + str(id) + ", batchedResult = " + str(batchedResult))
                if int(batchedResult['id']) == id:
                    order = payloadIdDict[id]
                    if 'result' in batchedResult:
                        # PrintAndLog("batchedResult['result'] for order " + str(order) + " = " + str(batchedResult['result']))
                        orderInfoResult = batchedResult['result']
                        dataList = Libraries.core.BreakStringIntoSubStrings_GivenEqualLength(Libraries.core.Remove0XfromHexString(orderInfoResult),
                                                                                             Libraries.core.LengthOfDataProperty)
                        # PrintAndLog("dataList = " + str(dataList))
                        orderState = int(dataList[0])
                        orderHash = '0x' + str(dataList[1])
                        orderTakerAmountFilled_weiUnits = Libraries.core.ConvertHexToInt(dataList[2])
                        # PrintAndLog("id = " + str(id) + ", orderState = " + str(orderState) + ", orderTakerAmountFilled_weiUnits = " + str(
                        #     orderTakerAmountFilled_weiUnits) + ", orderHash = " + str(orderHash))
                        returnList.append((orderState, orderHash, orderTakerAmountFilled_weiUnits))
                        break

        # PrintAndLog("API_GetOrderInfo_Batched: returnList of len " + str(len(returnList)) + " = " + str(returnList))
        # PrintAndLog("orderList of len " + str(len(orderList)) + " = " + str(orderList))
        if len(returnList) != len(orderList):
            PrintAndLog("API_GetOrderInfo_Batched: Length of returnList did not match length of orderList.")

        return returnList

    else:
        # If response code is not ok (200), print the resulting http error code with description
        PrintAndLog("API_GetOrderInfo_Batched Exchange_0xv2 response was not ok response = " + str(response))
        response.raise_for_status()


def ParseResponseData_GetManyOrderInfos(dataList):
    # Remove the first two items from the array, we don't need them
    dataList.pop(0)
    dataList.pop(0)
    # PrintAndLog("dataList with fist two items removed = " + str(dataList))

    itemsPerResponse = 3
    returnList = []
    index = 0
    while index < len(dataList):
        orderState = int(dataList[index + 0])
        orderHash = '0x' + str(dataList[index + 1])
        orderTakerAmountFilled_weiUnits = Libraries.core.ConvertHexToInt(dataList[index + 2])
        PrintAndLog("index = " + str(index) + ", orderState = " + str(orderState) + ", orderTakerAmountFilled_weiUnits = " + str(
            orderTakerAmountFilled_weiUnits) + ", orderHash = " + str(orderHash))

        returnList.append((orderState, orderHash, orderTakerAmountFilled_weiUnits))

        index += itemsPerResponse

    return returnList


def GetOrderType_GivenMakerAndTakerTokenAssetData(makerTokenAssetData, takerTokenAssetData, quoteToken):
    makerTokenAddress = ExtractAssetAddressFromAssetData(makerTokenAssetData)
    takerTokenAddress = ExtractAssetAddressFromAssetData(takerTokenAssetData)
    return GetOrderType_GivenMakerAndTakerTokenAddresses(makerTokenAddress, takerTokenAddress, quoteToken)


def GetOrderType_GivenMakerAndTakerTokenAddresses(makerTokenAddress, takerTokenAddress, quoteToken):
    # PrintAndLog_FuncNameHeader("makerTokenAddress = " + str(makerTokenAddress))
    # PrintAndLog_FuncNameHeader("takerTokenAddress = " + str(takerTokenAddress))
    # PrintAndLog_FuncNameHeader("quoteToken = " + str(quoteToken))
    if makerTokenAddress.lower() != quoteToken.lower() and takerTokenAddress.lower() == quoteToken.lower():
        return "sell"
    elif takerTokenAddress.lower() != quoteToken.lower() and makerTokenAddress.lower() == quoteToken.lower():
        return "buy"
    else:
        raise Libraries.exceptions.InvalidOrderType("quoteToken must be either the maker or the taker, not both and not neither. quoteToken = " + str(quoteToken))


def GenerateOrderTupleForTransaction(order):
    orderTuple = (
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.maker),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.taker),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.feeRecipient),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.senderAddress),
        int(order.makerTokenAmount), int(order.takerTokenAmount), int(order.makerFee),
        int(order.takerFee), int(order.expirationUnixTimestampSec), int(order.salt),
        Web3.toBytes(hexstr=str(EncodeERC20AssetData(str(order.makerTokenAddress)))),
        Web3.toBytes(hexstr=str(EncodeERC20AssetData(str(order.takerTokenAddress)))),
        Web3.toBytes(hexstr=str(EncodeERC20AssetData(str(order.makerFeeTokenAddress)))),
        Web3.toBytes(hexstr=str(EncodeERC20AssetData(str(order.takerFeeTokenAddress)))))

    return orderTuple


def GenerateOrderTupleForTransaction_Encoded(order):
    orderTuple = (
        Libraries.ninjaEncoding.PackAddress(Libraries.nodes.Instance_Web3.toChecksumAddress(order.maker)),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.taker),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.feeRecipient),
        Libraries.nodes.Instance_Web3.toChecksumAddress(order.senderAddress),
        Libraries.ninjaEncoding.PackInt(int(order.makerTokenAmount)),
        Libraries.ninjaEncoding.PackInt(int(order.takerTokenAmount)),
        int(order.makerFee), int(order.takerFee),
        Libraries.ninjaEncoding.PackInt(int(order.expirationUnixTimestampSec)),
        Libraries.ninjaEncoding.PackInt(int(order.salt)),
        Libraries.ninjaEncoding.PackOrder0xAssetData(EncodeERC20AssetData(str(order.makerTokenAddress))),
        Libraries.ninjaEncoding.PackOrder0xAssetData(EncodeERC20AssetData(str(order.takerTokenAddress))),
        Web3.toBytes(hexstr=str(
            EncodeERC20AssetData(str(order.makerFeeTokenAddress)))),
        Web3.toBytes(hexstr=str(
            EncodeERC20AssetData(str(order.takerFeeTokenAddress)))))

    return orderTuple


def GetProtocolFee(gasPrice_weiUnits, numOfOrders):
    global ProtocolFeeMultiplier_weiUnits
    protocolFee_wei = ProtocolFeeMultiplier_weiUnits * gasPrice_weiUnits * numOfOrders
    # PrintAndLog_FuncNameHeader("gasPrice_weiUnits = " + str(gasPrice_weiUnits) + ", numOfOrders = " + str(
    #     numOfOrders) + ", protocolFee_wei = " + str(protocolFee_wei) + " wei, or " + str(
    #     Libraries.core.ConvertWeiToEther(protocolFee_wei, Libraries.core.Ether_Decimals)) + " ether")
    return protocolFee_wei


def API_VerifyValidityOfOrders(orderList):
    returnList = API_GetOrderInfo_Batched_Safe(orderList)
    # PrintAndLog_FuncNameHeader("Iterate over returnList = " + str(returnList))

    returnOrderList = []
    for index, resultTuple in enumerate(returnList):
        (orderState, orderHash, orderTakerAmountFilled_weiUnits) = resultTuple
        # TODO, also verify orderTakerAmountFilled_weiUnits to make sure it means our requirements
        if orderState == OrderState.Open.value:
            # PrintAndLog_FuncNameHeader("Appending order to returnOrderList because it was found to still be fillable. orderHash = " + str(orderHash))
            returnOrderList.append(orderList[index])
        else:
            pass
            # PrintAndLog_FuncNameHeader("NOT appending order to returnOrderList because orderState = " + str(
            #     orderState) + ". orderHash = " + str(orderHash))

    # PrintAndLog_FuncNameHeader("Found that " + str(len(returnOrderList)) + " orders of the original " + str(
    #     len(orderList)) + " are still valid and fillable")
    return returnOrderList
