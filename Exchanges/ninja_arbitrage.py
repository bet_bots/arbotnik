import os
import datetime
import threading
import time
import traceback
from enum import Enum
from random import randint
from threading import Lock

import Exchanges.ninja
import Exchanges.kyber
import Exchanges.uniswap
import Exchanges.set
import Exchanges.keeperDAO
import Exchanges.zrxV2
import Libraries.nodes
import Libraries.cache
import Libraries.core
import Libraries.gasStation
import Libraries.estimatedGas
import Libraries.timeRecorder
import Libraries.utils
import Libraries.priceOracle
import Libraries.arbyUtility
import Libraries.blackListManager
import Libraries.tokenTransferCost
import Libraries.arbAlgos
import Libraries.defaults
import Libraries.ratesGraph
import Libraries.quoteTokenProcesses
import Libraries.processCommunication
import Libraries.ninjaOps
import Libraries.exceptions
from Libraries.arbitrageOpportunityHistory import Set_ArbitrageOpportunityListTo_ArbitrageOpportunitysPerBlockDict, IsArbitrageOpportunityStillCurrentlyAvailable
from Libraries.pricesDict import *
from Libraries.accounts import CreateNewReservableEthereumAccountPoolList, TokenDict_Ninja, NinjaOpAccountDict, DedicatedTxPredictionAccount
from Libraries.exchanges import ExchangeName_Set, ExchangeName_0x, ExchangeName_Kyber, ExchangeName_Uniswap, BlacklistedOrderManager, ExchangeNamesNotPermittedToTrade, ExchangeDict, \
    ExchangeName_0xMesh, ExchangeName_UniswapV2, GetListOfInEfficientExchangesNames_ToGetMassPricesForManyTradeQuantities, ExchangeName_Balancer, EnforceTokenAddressIsERC20Version, \
    ExchangeName_Sushiswap, ExchangeName_Defiswap, ExchangeName_Sakeswap, ExchangeName_Curve
from Libraries.loggingConfig import PrintAndLog, PrintAndLogError, PrintAndLog_Detailed, PrintAndLog_FuncNameHeader, LogPricesDict
from Libraries.customDicts import DictWithValueList, DictWithZeroInitializedValue, DictWithValueAndAge
from Libraries.executeOnInterval import IsTimeToExecute, IsTimeToExecute_ReturnThread
from Libraries.readySetGo import ReadySetGoState

# For some reason, my Ninja contract cannot successfully sell certain tokens on bancor.
# I have no idea why but I haven't really had time to properly investigate it. Must be an incompatibility in that token's smart contract
# The allowance is set correctly, and the balance is fine. So for now i'm using this array to blacklist certain things to stop the reverts and gas waste
from Libraries.mempool import GetThreadSafeCopyOf_MempoolTxDict
from Libraries.orders import GetOrdersFillableAmountRemaining_0x
from Libraries.trade import AssociateAnOrderHashWithATxHash, IsValidPrice
from Libraries.transactions import API_CancelTransaction

NinjaTokenBans_SellingOnBancor = []
NinjaTokenBans_SellingOnBancor.append('mana')

# This is a dict keyed by a tradeOpportunity path string
# Value doesn't matter??
# Path will look like this:
#   ExchangeTradePath-quoteToken-baseToken
#   or
#   BuyOnUniswapSellOnUniswapV2-0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee-0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c
# The PursueTradeOpportunity function will update this to show which paths are currently being traded
# The arbitrage logic and pricesDict will alert the PursueTradeOpportunity trade loop when a trade path that's currently being traded on is no longer profitable
# That information can trigger events like canceling a trade that's already pending and in flight
PursueTradeOpportunityPathsDict = DictWithValueAndAge()
Lock_PursueTradeOpportunityPathsDict = Lock()


# TODO, move all these Enums somewhere that makes sense

class PursueTradeOpportunityPathCommand(Enum):
    Trading = "Trading"
    CancelASAP = "CancelASAP"


class FrontRunningGasPriceType(Enum):
    OpeningBid = "OpeningBid"
    MaxKeepAhead = "MaxKeepAhead"
    MaxGrimTrigger = "MaxGrimTrigger"


class TradeReason(Enum):
    OpeningBid = "OpeningBid"
    KeepAhead = "KeepAhead"
    FrontRun = "FrontRun"
    Tailgate = "Tailgate"


class TradingTechnique(Enum):
    Arbitrage = "Arbitrage"
    Tailgate = "Tailgate"


class QuoteTokenSource(Enum):
    NinjaContract = "NinjaContract"
    KeeperDAOFlashLoan = "KeeperDAOFlashLoan"


class ProfitDirection(Enum):
    Increase = "Increase"
    Decrease = "Decrease"


class TradePathIdentifierData:
    tradesUniqueIdentifier = None
    inflowToken = None
    outflowToken = None

    def __init__(self, _tradesUniqueIdentifier, _inflowToken, _outflowToken):
        self.tradesUniqueIdentifier = _tradesUniqueIdentifier
        self.inflowToken = _inflowToken
        self.outflowToken = _outflowToken

    def GetTradePathIdentifierDataString(self):
        return (str(self.tradesUniqueIdentifier) + "-" + str(self.inflowToken) + "-" + str(self.outflowToken)).lower()


class ArbitrageOpportunity:
    blockNumberDiscovered = None
    quoteToken = None
    profitPercent = None
    tradePath = None
    exchangeNamesList = None
    quoteTokensList = None
    ratesList = None
    metaDataPath = None
    index_string = None
    quoteTokenAmount = None
    quoteTokenAmount_originalEstimate = None

    sidesList_wrtPricesDict = None
    pricesList_wrtPricesDict = None
    metaDataDict_beforeTrade = None

    # profit_quoteTokens is profit in the actual quoteToken we're trading, eth/usdc/dai/etc
    profit_quoteTokens = None
    # profit_eth should only be used when comparing profit between trading opportunities
    profit_eth = None
    quoteTokenSource = None

    ninjaObject = None
    gasPriceFromConfig = None
    gasPrice = None
    gasPrice_maxKeepAhead = None
    gasPrice_maxGrimTrigger = None
    minProfitRequirement_quoteTokens = None
    gas_usedForCalculatingMinProfitRequirement = None
    tradingFee_quoteTokens = None
    costToTrade_quoteTokens = None

    def __init__(self, _blockNumberDiscovered, _quoteToken, _profitPercent, _tradePath, _exchangeNamesList, _quoteTokensList,
                 _ratesList, _index_string, _quoteTokenAmount, _metaDataPath):
        self.blockNumberDiscovered = _blockNumberDiscovered
        self.quoteToken = _quoteToken
        self.profitPercent = _profitPercent
        self.tradePath = _tradePath
        self.exchangeNamesList = _exchangeNamesList
        self.quoteTokensList = _quoteTokensList
        self.ratesList = _ratesList
        self.metaDataPath = _metaDataPath
        self.index_string = _index_string
        self.quoteTokenAmount = _quoteTokenAmount
        self.quoteTokenAmount_originalEstimate = _quoteTokenAmount

        # Determine the sidesList_wrtPricesDict
        # PrintAndLog_FuncNameHeader("_quoteTokensList = " + str(_quoteTokensList))
        # PrintAndLog_FuncNameHeader("_ratesList = " + str(_ratesList))
        # PrintAndLog_FuncNameHeader("_tradePath = " + str(_tradePath))

        self.sidesList_wrtPricesDict = []
        for pathIndex in range(0, len(_tradePath) - 1):
            quoteToken, baseToken = self.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
            inflowToken = _tradePath[pathIndex]
            outflowToken = _tradePath[pathIndex + 1]

            if quoteToken.lower() == inflowToken.lower():
                self.sidesList_wrtPricesDict.append('buy')
            elif quoteToken.lower() == outflowToken.lower():
                self.sidesList_wrtPricesDict.append('sell')
            else:
                raise Exception("not quoteToken or baseToken?")

        # PrintAndLog_FuncNameHeader("self.sidesList_wrtPricesDict = " + str(self.sidesList_wrtPricesDict))

        # Determine the pricesList_wrtPricesDict
        self.pricesList_wrtPricesDict = []
        for index_rate, rate in enumerate(_ratesList):
            side = self.sidesList_wrtPricesDict[index_rate]
            price = Libraries.utils.ConvertRateToPrice(rate, side)
            self.pricesList_wrtPricesDict.append(price)

        # PrintAndLog_FuncNameHeader("self.ratesList = " + str(self.ratesList))
        # PrintAndLog_FuncNameHeader("self.pricesList_wrtPricesDict = " + str(self.pricesList_wrtPricesDict))

        # The key is the pathIndex and init each value so they can be set later
        self.metaDataDict_beforeTrade = {}
        for pathIndex in range(0, len(_tradePath) - 1):
            self.metaDataDict_beforeTrade[pathIndex] = {}

        self.profit_quoteTokens = _profitPercent * _quoteTokenAmount

        # convert profit_quoteTokens to eth via the PriceOracle
        quoteTokenPrice = Libraries.priceOracle.GetOnChainPrice_FromCache(_quoteToken)
        self.profit_eth = self.profit_quoteTokens * quoteTokenPrice

        self.ninjaObject = None
        self.gasPrice = None
        self.gasPrice_maxKeepAhead = None
        self.gasPrice_maxGrimTrigger = None
        self.tradingFee_quoteTokens = None
        self.costToTrade_quoteTokens = None

    def SetNinjaObject(self, _ninjaObject):
        self.ninjaObject = _ninjaObject

    def SetGas_usedForCalculatingMinProfitRequirement(self, _gas_usedForCalculatingMinProfitRequirement):
        self.gas_usedForCalculatingMinProfitRequirement = _gas_usedForCalculatingMinProfitRequirement
        # PrintAndLog_FuncNameHeader("self.gas_usedForCalculatingMinProfitRequirement = " + str(self.gas_usedForCalculatingMinProfitRequirement))

    def GetBool_DoTradeWithinFlashLoan(self):
        if self.quoteTokenSource == QuoteTokenSource.NinjaContract:
            return False
        # else we're assuming any flash loan
        else:
            return True

    def SetQuoteTokenSource(self):
        # Enforce quoteToken balances here for both ninja's contract balances and flash loanable balances in the KeeperDAO LP
        # Where is it currently referencing ninja's contract balances and/or flash loanable balances in the KeeperDAO LP
        canSourceQuoteTokensForTrade = True

        # Start by getting balances for both Ninja and KeeperDAOLP
        # We're getting balances from our cache so we don't have to make an API call in this function
        balance_Ninja_quoteToken = Exchanges.ninja.NinjaInstance_Generic.GetQuoteTokenBalance_Ninja(self.quoteToken)
        borrowableBalance_KeeperDAOLP_quoteToken = Exchanges.ninja.NinjaInstance_Generic.GetQuoteTokenBalance_KeeperDAOLP(self.quoteToken)
        # PrintAndLog_FuncNameHeader("Deciding from where to source quoteTokens. self.quoteTokenAmount = " + str(
        #     self.quoteTokenAmount) + ", balance_Ninja_quoteToken = " + str(
        #     balance_Ninja_quoteToken) + ", borrowableBalance_KeeperDAOLP_quoteToken = " + str(borrowableBalance_KeeperDAOLP_quoteToken))

        if Libraries.defaults.ForceNinjaBalanceToBeZero_SourcingQuoteTokensFrom_WillForceAKeeperDAOFlashLoan:
            balance_Ninja_quoteToken = 0
            PrintAndLog_FuncNameHeader("Forced balance_Ninja_quoteToken to be zero because of ForceNinjaBalanceToBeZero_SourcingQuoteTokensFrom_WillForceAKeeperDAOFlashLoan")

        # This is where I decide whether or not this trade will be a flash loan trade or a local trade
        # First check the debug flag
        if Libraries.defaults.ForceSourcingQuoteTokensFrom_KeeperDAOFlashLoan:
            # Trade via KeeperDAO LP flash loan
            self.quoteTokenSource = QuoteTokenSource.KeeperDAOFlashLoan
            PrintAndLog_FuncNameHeader("Forced trade through flash loan because ForceSourcingQuoteTokensFrom_KeeperDAOFlashLoan was True")
            return True
        elif self.quoteTokenAmount < balance_Ninja_quoteToken:
            # Trade using Ninja contract balances
            self.quoteTokenSource = QuoteTokenSource.NinjaContract
            # PrintAndLog_FuncNameHeader("Sourcing quoteTokens from Local Ninja contract")
            return True
        elif self.quoteTokenAmount < borrowableBalance_KeeperDAOLP_quoteToken:
            # Trade via KeeperDAO LP flash loan
            self.quoteTokenSource = QuoteTokenSource.KeeperDAOFlashLoan
            # PrintAndLog_FuncNameHeader("Sourcing quoteTokens from KeeperDAO flash loan")
            return True
        else:
            message = "Sourcing quoteTokens from NOTHING because quoteTokenAmount exceeds both Ninja balance and " \
                      "KeeperDAO flash loanable balance! self.quoteTokenAmount = " + \
                      str(self.quoteTokenAmount) + ", " + str(self.GenerateDescriptionString())
            PrintAndLog_FuncNameHeader(message)
            if IsTimeToExecute("Sourcing quoteTokens from NOTHING because quoteTokenAmount exceeds both Ninja balance", 200):
                Libraries.core.API_PostOperatorNotification(message)
            return False

    def CalculateGasPrice(self, gasPriceFromConfig):
        # Save this for later in case we need it
        self.gasPriceFromConfig = gasPriceFromConfig

        # Assume the profitPercentageAdder is zero here
        self.gasPrice, gasPriceIsAboveMinimum = self.CalculateGasPrice_Generic(gasPriceFromConfig, FrontRunningGasPriceType.OpeningBid, 0)
        self.gasPrice_maxKeepAhead, dontCare = self.CalculateGasPrice_Generic(gasPriceFromConfig, FrontRunningGasPriceType.MaxKeepAhead, 0)
        self.gasPrice_maxGrimTrigger, dontCare = self.CalculateGasPrice_Generic(gasPriceFromConfig, FrontRunningGasPriceType.MaxGrimTrigger, 0)

        if Libraries.defaults.VerboseLogging_CalculateGasPrice:
            PrintAndLog_FuncNameHeader("self.gasPrice = " + str(Libraries.core.ConvertWeiToGwei(self.gasPrice)))
            PrintAndLog_FuncNameHeader("gasPriceIsAboveMinimum = " + str(gasPriceIsAboveMinimum))
            PrintAndLog_FuncNameHeader("self.gasPrice_maxKeepAhead = " + str(Libraries.core.ConvertWeiToGwei(self.gasPrice_maxKeepAhead)))
            PrintAndLog_FuncNameHeader("self.gasPrice_maxGrimTrigger = " + str(Libraries.core.ConvertWeiToGwei(self.gasPrice_maxGrimTrigger)))

        # TODO, replace this hard coded gas price with the exact gas price that's in the Ninja's properties
        # If I have gas tokens on the ninja contract
        # and if my gas price is higher than my BurnGasTokenThreshold_wei
        if Exchanges.ninja.BurnGasTokenThreshold_wei and \
                Exchanges.ninja.GasTokenQuantity_etherUnits and \
                self.gasPrice > Exchanges.ninja.BurnGasTokenThreshold_wei and \
                Exchanges.ninja.GasTokenQuantity_etherUnits > Libraries.defaults.MinGasTokenThreshold:
            # Increase my profit percentage because we're expecting to save gas costs due to burning gas tokens
            if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                PrintAndLog_FuncNameHeader("Recalculating CalculateGasPrice_Generic with Ninja_ProfitPercentageAdder_WhenGasPriceOverBurnGasTokenThreshold = " + str(
                    Libraries.defaults.Ninja_ProfitPercentageAdder_WhenGasPriceOverBurnGasTokenThreshold) + " because our base gas price was over Ninja's burn gas tokens "
                                                                                                            "threshold. self.gasPrice = " + str(self.gasPrice))

            self.gasPrice, gasPriceIsAboveMinimum = self.CalculateGasPrice_Generic(
                gasPriceFromConfig, FrontRunningGasPriceType.OpeningBid, Libraries.defaults.Ninja_ProfitPercentageAdder_WhenGasPriceOverBurnGasTokenThreshold)
            self.gasPrice_maxKeepAhead, dontCare = self.CalculateGasPrice_Generic(
                gasPriceFromConfig, FrontRunningGasPriceType.MaxKeepAhead, Libraries.defaults.Ninja_ProfitPercentageAdder_WhenGasPriceOverBurnGasTokenThreshold)
            self.gasPrice_maxGrimTrigger, dontCare = self.CalculateGasPrice_Generic(
                gasPriceFromConfig, FrontRunningGasPriceType.MaxGrimTrigger, Libraries.defaults.Ninja_ProfitPercentageAdder_WhenGasPriceOverBurnGasTokenThreshold)

        # Return the gasPriceIsAboveMinimum based on NOT grim triggering.
        return gasPriceIsAboveMinimum

    def CalculateGasPrice_Generic(self, gasPriceFromConfig, frontRunningGasPriceType, profitPercentageAdder):
        from Libraries.exchanges import ExchangeDict

        gasPriceIsAboveMinimum = True
        gasPriceToSet = None
        # If we're using DynamicGasPrice, we need to set gasPrice here!
        if gasPriceFromConfig == Libraries.gasStation.GasPriceCustomType.DynamicGasPrice:
            # Calculate gasPrice based on profit_eth
            percentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly = \
                self.CalculatePercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly(frontRunningGasPriceType, profitPercentageAdder)
            if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                PrintAndLog_FuncNameHeader("percentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly = " + str(
                    percentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly) + " for " + str(self.exchangeNamesList))
            gasPriceToSet = Libraries.gasStation.CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens(
                self.profit_quoteTokens, self.gas_usedForCalculatingMinProfitRequirement,
                percentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly, self.quoteToken)
            if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                PrintAndLog_FuncNameHeader("gasPriceToSet = " + str(Libraries.core.ConvertWeiToGwei(gasPriceToSet)) +
                                           " gwei after calling CalculateGasPrice_wei_BasedOnPercentageOfExpectedProfitInQuoteTokens")

            if frontRunningGasPriceType == FrontRunningGasPriceType.OpeningBid or \
                    frontRunningGasPriceType == FrontRunningGasPriceType.MaxKeepAhead:
                if gasPriceToSet > Libraries.gasStation.GetMaxOpeningBidGasPrice():
                    gasPriceToSet = Libraries.gasStation.GetMaxOpeningBidGasPrice()
                    if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                        PrintAndLog_FuncNameHeader("gasPriceToSet > GetMaxOpeningBidGasPrice(). We're lowering gasPriceToSet back "
                                                   "down to our GetMaxOpeningBidGasPrice(). No reason to go any higher")
                        PrintAndLog_FuncNameHeader("gasPriceToSet = " + str(Libraries.core.ConvertWeiToGwei(gasPriceToSet)) + " gwei")

            elif frontRunningGasPriceType == FrontRunningGasPriceType.MaxGrimTrigger:
                pass
            else:
                raise Exception("frontRunningGasPriceType not supported, did I add a new feature?")

            if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                PrintAndLog_FuncNameHeader("Using DynamicGasPrice: Comparing gasPriceToSet = " + str(
                    Libraries.core.ConvertWeiToGwei(gasPriceToSet)) + " with GetGasPrice_SafeLow = " + str(
                    Libraries.core.ConvertWeiToGwei(Libraries.gasStation.GetGasPrice_SafeLow())) + ". gasPrice must be larger for it to make sense")

            if gasPriceToSet < Libraries.gasStation.GetMinGasPrice_ArbitrageGeneric():
                if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                    PrintAndLog_FuncNameHeader("DynamicGasPrice calculated for this potential trade reported a gasPriceToSet that's below the min acceptable gas price. "
                                               "So we are ignoring this trade because there's not enough profit to make this trade worth doing. "
                                               "It would never get mined into the Ethereum network.")
                gasPriceIsAboveMinimum = False
        else:
            if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                PrintAndLog_FuncNameHeader("Not using Using DynamicGasPrice, gasPrice was set to " + str(
                    Libraries.core.ConvertWeiToGwei(gasPriceFromConfig)) + " gwei")
            gasPriceToSet = gasPriceFromConfig

        # Call EnforceMaxGasPrice for each exchange in this arbitrageOpportunity
        for exchangeName in self.exchangeNamesList:
            if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                PrintAndLog_FuncNameHeader("gasPriceToSet before calling EnforceMaxGasPrice on " + str(exchangeName))
            gasPriceToSet = ExchangeDict[exchangeName].EnforceMaxGasPrice(gasPriceToSet)
            if Libraries.defaults.VerboseLogging_CalculateGasPrice:
                PrintAndLog_FuncNameHeader("gasPriceToSet after calling EnforceMaxGasPrice on " + str(exchangeName))

        if Libraries.defaults.VerboseLogging_CalculateGasPrice:
            PrintAndLog_FuncNameHeader("gasPriceToSet = " + str(Libraries.core.ConvertWeiToGwei(gasPriceToSet)) + " gwei")
        return int(gasPriceToSet), gasPriceIsAboveMinimum

    def GetEstimatedGasCost_ether(self):
        return Libraries.core.GetGasCost(self.gasPrice, self.GetExpectedGasUsage_AllTradeActions())

    def DoesPassGasPriceSanityCheck_EstimatedGasCost_ether(self, estimatedGasCost_ether):
        if estimatedGasCost_ether < Libraries.defaults.GasPriceSanityCheckBuffer_AlternativeMinimumCost_ether or \
                estimatedGasCost_ether < self.profit_eth * Libraries.defaults.GasPriceSanityCheckBufferMultiplier:
            return True
        else:
            return False

    def SetMinProfitRequirement_quoteTokens(self, _minProfitRequirement_quoteTokens):
        self.minProfitRequirement_quoteTokens = _minProfitRequirement_quoteTokens
        # PrintAndLog_FuncNameHeader("self.minProfitRequirement_quoteTokens = " + str(self.minProfitRequirement_quoteTokens))

    def GenerateDescriptionString(self):
        # TODO get exchange specific stuff from the metadata here, so I can factor in things like 0x order hash and set key
        return self.GenerateDescriptionString_Exchanges() + "-" + str(self.index_string) + "-" + self.GenerateDescriptionString_Amount()

    def GenerateDescriptionString_Exchanges(self):
        # [('Balancer', '0xMesh', 'Balancer')]
        # ETH, USDC, BAL, ETH
        # ETH->Balancer->USDC->0xMesh->BAL->Balancer->ETH
        returnString = ''
        for index, exchangeName in enumerate(self.exchangeNamesList):
            returnString += self.tradePath[index] + '->' + self.exchangeNamesList[index] + '->'

        returnString += self.tradePath[len(self.tradePath) - 1]
        return returnString

    def GenerateDescriptionString_Tokens(self):
        return str(self.quoteToken) + "-" + str(self.tradePath)

    def GenerateDescriptionString_Amount(self):
        return str(self.quoteTokenAmount_originalEstimate)

    def GenerateDescriptionString_PursueTradeOpportunityPathsDict(self):
        return self.GenerateDescriptionString_Exchanges() + "-" + self.GenerateDescriptionString_Tokens()

    def GenerateDescriptionString_NumOfTradeLegs(self):
        return str(self.GetNumOfTradeLegs()) + "-leg"

    def GetNumOfTradeLegs(self):
        return len(self.GetListOfBaseTokensFromTradePath()) + 1

    def GetTokensExcludingEthBasedTokens(self):
        # We want all token addresses excluding ETH/WETH since those may be hard coded by other traders
        tradePath_lowercase = Libraries.utils.ConvertListOfStringsToLowercaseListOfStrings(self.tradePath)
        tradePath_lowercase_uniqueTokens = list(dict.fromkeys(tradePath_lowercase))

        # remove ETH and WETH
        if Exchanges.keeperDAO.EthTokenContract.lower() in tradePath_lowercase_uniqueTokens:
            tradePath_lowercase_uniqueTokens.remove(Exchanges.keeperDAO.EthTokenContract.lower())
        if Exchanges.zrxV2.Contract_WETH.lower() in tradePath_lowercase_uniqueTokens:
            tradePath_lowercase_uniqueTokens.remove(Exchanges.zrxV2.Contract_WETH.lower())

        return tradePath_lowercase_uniqueTokens

    def GetMempoolAnalysisTokens(self):
        return self.GetTokensExcludingEthBasedTokens()

    def GetTokensExcludingEthBasedTokensCommaSeparated(self):
        returnList = self.GetTokensExcludingEthBasedTokens()
        returnList_string = ''
        for index, baseToken in enumerate(returnList):
            returnList_string += baseToken
            if index < len(returnList) - 1:
                # Make it comma separated
                returnList_string += ','

        return returnList_string

    def GetListOfBaseTokensFromTradePath(self):
        # We want the baseTokens only
        # quoteToken = ETH
        # tokenPath = ETH, USDC, BAL, ETH
        # So include all items in the list except the first and last
        returnList = []
        for index, token in enumerate(self.tradePath):
            if index != 0 and index != len(self.tradePath) - 1:
                returnList.append(token)

        return returnList

    def GetListOfBaseTokensCommaSeparated(self):
        returnList = self.GetListOfBaseTokensFromTradePath()
        returnList_string = ''
        for index, baseToken in enumerate(returnList):
            returnList_string += baseToken
            if index < len(returnList) - 1:
                # Make it comma separated
                returnList_string += ','

        return returnList_string

    def GenerateDescriptionString_BaseTokensCommaSeparated(self):
        return self.GetListOfBaseTokensCommaSeparated()

    def ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(self, pathIndex):
        return Exchanges.keeperDAO.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(
            self.tradePath[pathIndex],
            self.tradePath[pathIndex + 1],
            self.quoteTokensList[pathIndex])

    def GetTokenFlow(self, pathIndex):
        quoteToken, baseToken = self.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
        side = self.sidesList_wrtPricesDict[pathIndex]
        inflowToken = None
        outflowToken = None
        if Libraries.core.IsBuy(side):
            inflowToken = quoteToken
            outflowToken = baseToken
        elif Libraries.core.IsSell(side):
            inflowToken = baseToken
            outflowToken = quoteToken
        else:
            raise Exception("Not buy or sell?")

        return inflowToken, outflowToken, quoteToken, baseToken

    def IsQuoteTokenTheInFlowToken(self, pathIndex):
        inflowToken, outflowToken, quoteToken, baseToken = self.GetTokenFlow(pathIndex)
        if quoteToken.lower() == inflowToken.lower():
            return True
        else:
            return False

    def IsQuoteTokenTheOutFlowToken(self, pathIndex):
        inflowToken, outflowToken, quoteToken, baseToken = self.GetTokenFlow(pathIndex)
        if quoteToken.lower() == outflowToken.lower():
            return True
        else:
            return False

    def GetExpectedTokenQuantitiesTraded(self, pathIndex):
        pass
        # ratesList = 387.493737984980439 * 0.051282051282051 * 0.0654159889702764
        # quoteTokensToSpend = 0.1 ETH
        # 0.1 ETH -> 38.749373798498044 USDC -> 1.987147374281951 BAL -> 0.129991210718342
        #
        # ETH, USDC, BAL, ETH
        #
        # quoteToken = ETH
        # tokenPath = ETH, USDC, BAL, ETH

        for local_pathIndex, exchangeName in enumerate(self.exchangeNamesList):
            # PrintAndLog_FuncNameHeader("   local_pathIndex = " + str(local_pathIndex) + " of " + str(len(self.exchangeNamesList)) + ", exchangeName = " + str(exchangeName))

            if local_pathIndex == 0:
                # Initial value on first loop iteration
                local_inFlowTokenAmount = self.quoteTokenAmount
                # PrintAndLog_FuncNameHeader("   local_inFlowTokenAmount = " + str(local_inFlowTokenAmount) + ", initialized based on self.quoteTokenAmount")
            else:
                # Set this based on local_outFlowTokenAmount which is the output of the previous trade
                local_inFlowTokenAmount = local_outFlowTokenAmount
                # PrintAndLog_FuncNameHeader("   local_inFlowTokenAmount = " + str(local_inFlowTokenAmount) + ", based on previous trade's outflow")

            rate = self.ratesList[local_pathIndex]
            # PrintAndLog_FuncNameHeader("   rate = " + str(rate))

            # Calculate local_outFlowTokenAmount
            local_outFlowTokenAmount = local_inFlowTokenAmount * rate
            # PrintAndLog_FuncNameHeader("   local_outFlowTokenAmount = " + str(local_outFlowTokenAmount) + ", based on local_inFlowTokenAmount and rate")
            # PrintAndLog_FuncNameHeader("   local_inFlowTokenAmount = " + str(local_inFlowTokenAmount))
            # PrintAndLog_FuncNameHeader("   local_outFlowTokenAmount = " + str(local_outFlowTokenAmount))

            if local_pathIndex == pathIndex:
                # PrintAndLog_FuncNameHeader("   we made it to the pathIndex we care about, pathIndex = " + str(pathIndex))
                quoteTokenTradeAmount = None
                baseTokenTradeAmount = None
                if self.IsQuoteTokenTheInFlowToken(pathIndex):
                    quoteTokenTradeAmount = local_inFlowTokenAmount
                    baseTokenTradeAmount = local_outFlowTokenAmount
                elif self.IsQuoteTokenTheOutFlowToken(pathIndex):
                    quoteTokenTradeAmount = local_outFlowTokenAmount
                    baseTokenTradeAmount = local_inFlowTokenAmount
                else:
                    raise Exception("I have a bug with my tradePath or pathIndex")

                # PrintAndLog_FuncNameHeader("quoteTokenTradeAmount = " + str(quoteTokenTradeAmount))
                # PrintAndLog_FuncNameHeader("baseTokenTradeAmount = " + str(baseTokenTradeAmount))

                return quoteTokenTradeAmount, baseTokenTradeAmount

    def GetExpectedTokenQuantitiesTraded_basedOnFlow(self, pathIndex):
        quoteTokenTradeAmount, baseTokenTradeAmount = self.GetExpectedTokenQuantitiesTraded(pathIndex)
        # Now convert this to flow in terms of inFlowToken and outFlowToken
        inflowToken, outflowToken, quoteToken, baseToken = self.GetTokenFlow(pathIndex)
        inFlowTokenAmount = None
        outFlowTokenAmount = None
        if quoteToken.lower() == inflowToken.lower():
            inFlowTokenAmount = quoteTokenTradeAmount
            outFlowTokenAmount = baseTokenTradeAmount
        elif baseToken.lower() == inflowToken.lower():
            inFlowTokenAmount = baseTokenTradeAmount
            outFlowTokenAmount = quoteTokenTradeAmount
        else:
            raise Exception("This should never happened. What's wrong?")

        return inFlowTokenAmount, outFlowTokenAmount

    def GetTradePathIdentifiers(self, doIncludeTokenFlow=True):
        tradePathIdentifierDataList = []
        for pathIndex, exchangeName in enumerate(self.exchangeNamesList):
            exchange = Libraries.exchanges.ExchangeDict[exchangeName]
            inflowToken, outflowToken, quoteToken, baseToken = self.GetTokenFlow(pathIndex)
            # TODO I may have a chicken and egg problem here.
            #  I thought i wanted metaDataDict_beforeTrade, but it hasn't been set yet.
            #  It doesn't get set until PursueTradingOpportunity gets called
            #  So I'm passing in metaDataPath instead. Hopefully that's what I need
            tradesUniqueIdentifier = exchange.GetTradesUniqueIdentifier(quoteToken, baseToken, exchange.GetMetaDataIdentifier(self, pathIndex))
            # PrintAndLog_FuncNameHeader("Creating TradePathIdentifierData object with:")
            # PrintAndLog_FuncNameHeader("   tradesUniqueIdentifier = " + str(tradesUniqueIdentifier))
            # PrintAndLog_FuncNameHeader("   inflowToken = " + str(inflowToken))
            # PrintAndLog_FuncNameHeader("   outflowToken = " + str(outflowToken))
            if doIncludeTokenFlow:
                tradePathIdentifierDataList.append(TradePathIdentifierData(tradesUniqueIdentifier, inflowToken, outflowToken))
            else:
                tradePathIdentifierDataList.append(tradesUniqueIdentifier)
        return tradePathIdentifierDataList

    def GetTradePathIdentifiers_ReturnStrings(self):
        tradePathIdentifierDataList = self.GetTradePathIdentifiers()
        stringList = []
        for tradePathIdentifierData in tradePathIdentifierDataList:
            stringList.append(tradePathIdentifierData.GetTradePathIdentifierDataString())

        return stringList

    def SetMyPath_PursueTradeOpportunityPathsDict(self):
        return SetPath_PursueTradeOpportunityPathsDict(self.GenerateDescriptionString_PursueTradeOpportunityPathsDict())

    # def FlagMyPathToBeCanceled_PursueTradeOpportunityPathsDict(self):
    #     FlagPathToBeCanceled_PursueTradeOpportunityPathsDict(self.GenerateDescriptionString_PursueTradeOpportunityPathsDict())

    def HasMyPathsTradeBeenSignaledToCancelTheTransaction_PursueTradeOpportunityPathsDict(self):
        return HasPathsTradeBeenSignaledToCancelTheTransaction_PursueTradeOpportunityPathsDict(self.GenerateDescriptionString_PursueTradeOpportunityPathsDict())

    def RemoveMyPath_PursueTradeOpportunityPathsDict(self):
        RemovePath_PursueTradeOpportunityPathsDict([self.GenerateDescriptionString_PursueTradeOpportunityPathsDict()])

    def CalculateCostToTrade(self):
        self.tradingFee_quoteTokens = self.CalculateTradingFee_quoteTokens()
        if Libraries.defaults.VerboseLogging_IsProfitable:
            PrintAndLog_FuncNameHeader("self.profitPercent must be greater than zero, self.profitPercent = " + str(self.profitPercent))
            PrintAndLog_FuncNameHeader("self.profit_quoteTokens must be greater than the cost to trade. self.profit_quoteTokens = " + str(self.profit_quoteTokens))
            PrintAndLog_FuncNameHeader("self.minProfitRequirement_quoteTokens = " + str(self.minProfitRequirement_quoteTokens))
            PrintAndLog_FuncNameHeader("self.tradingFee_quoteTokens = " + str(self.tradingFee_quoteTokens))

        if Libraries.defaults.Ninja_ForceTradingFeeToBeZero:
            PrintAndLog_FuncNameHeader("Ninja_ForceTradingFeeToBeZero is set to True so we're forcing self.tradingFee_quoteTokens to be zero")
            self.tradingFee_quoteTokens = 0

        # TODO these are all so poorly named, I need to rename them.
        #  minProfitRequirement_quoteTokens is actually the gas cost to trade
        #  tradingFee_quoteTokens is actually the protocol trading fee
        #  costToTrade_quoteTokens is essentially the sum of all gas costs and protocol trading fees
        # TODO RENAME the crap out of this to make sense
        #  Hard thing about renaming minProfitRequirement_quoteTokens is that that naming convention is used in so many places...
        #  This will take some VERY careful renaming
        self.costToTrade_quoteTokens = self.minProfitRequirement_quoteTokens + self.tradingFee_quoteTokens
        return self.costToTrade_quoteTokens

    def IsProfitable(self):
        # TODO, profitPercent is actually wrong here because
        #  it doesn't take into consideration tradingFee_quoteTokens which can only be calculated later on when we have the gas price
        #  But it's not the end of the world to leave it as is because i'm enforcing both profitPercent AND profit_quoteTokens
        # if self.profitPercent > 0 and self.profit_quoteTokens > self.CalculateCostToTrade():
        if self.profit_quoteTokens > self.CalculateCostToTrade():
            return True
        else:
            return False

    def DetermineHowManyTailgatingTradesToQueue(self):
        if Libraries.defaults.UseDebugMinProfitRequirement_NinjaGeneric:
            PrintAndLog_FuncNameHeader("returning a hard coded testing number because UseDebugMinProfitRequirement_NinjaGeneric "
                                       "is True and that will throw off our calculation")
            return 2
        else:
            profitNetGasAndFees = self.profit_quoteTokens - self.costToTrade_quoteTokens
            PrintAndLog_FuncNameHeader("profitNetGasAndFees = " + str(profitNetGasAndFees))
            ratio = profitNetGasAndFees / self.costToTrade_quoteTokens
            PrintAndLog_FuncNameHeader("ratio = " + str(ratio))

            # Determine how many tailgating trades to queue based on the ratio between profit and the cost to trade
            if ratio <= 1:
                return 1
            else:
                return int(ratio)

    def Prepare_Trade(self):
        from Libraries.exchanges import ExchangeDict

        doWrapEthBeforeFirstTrade = False
        doUnwrapEthAfterLastTrade = False
        quoteTokenAddressToUse = self.quoteToken

        # Only consider this if the quoteToken is ETH since ETH is the only token that can be wrapped to WETH
        if self.quoteToken.lower() == Exchanges.keeperDAO.EthTokenContract.lower():
            atLeastOneExchangeTradesETHDirectly = False
            zrxIsInvolved = False
            for exchangeName in self.exchangeNamesList:
                if ExchangeDict[exchangeName].SupportTradingEthDirectlyWithoutBeingWrapped():
                    atLeastOneExchangeTradesETHDirectly = True

                if exchangeName == ExchangeName_0xMesh:
                    zrxIsInvolved = True

            PrintAndLog_FuncNameHeader("atLeastOneExchangeTradesETHDirectly = " + str(atLeastOneExchangeTradesETHDirectly))
            PrintAndLog_FuncNameHeader("zrxIsInvolved = " + str(zrxIsInvolved))

            # TODO, there's a flaw in the logic where 0xMesh technically does not trade ETH directly
            #  but the 0x staking fee is currently being paid in ETH so i have to trade ETH as the quote token and wrap it....
            #  To prevent this from being a problem, I need to be paying 0x staking fee with WETH instead of ETH.
            #  I can then remove this hacky zrxIsInvolved variable from this if statement
            if atLeastOneExchangeTradesETHDirectly or zrxIsInvolved:
                # the quote token will remain ETH (not WETH), we just need to figure out if we need to wrap/unwrap anywhere

                # Get the first and last exchangeName in the trade path using exchangeNamesList
                exchange_first = ExchangeDict[self.exchangeNamesList[0]]
                exchange_last = ExchangeDict[self.exchangeNamesList[len(self.exchangeNamesList) - 1]]

                # If the first exchange i'm buying on does not support trading ETH directly
                if not exchange_first.SupportTradingEthDirectlyWithoutBeingWrapped():
                    PrintAndLog_FuncNameHeader("exchange_first " + exchange_first.exchangeName + " does not support trading ETH directly, "
                                                                                                 "so I must wrap it before I make the first trade")
                    # I need to wrap the ETH to WETH before I can make the first trade
                    doWrapEthBeforeFirstTrade = True

                # If the last exchange I'm selling on does not support trading ETH directly
                if not exchange_last.SupportTradingEthDirectlyWithoutBeingWrapped():
                    PrintAndLog_FuncNameHeader("exchange_last " + exchange_last.exchangeName + " does not support trading ETH directly, "
                                                                                               "so I must unwrap it after I make the last trade")
                    # I need to unwrap the ETH after I make the last trade
                    doUnwrapEthAfterLastTrade = True

            else:
                # Specify that WETH is the quoteToken, not ETH
                quoteTokenAddressToUse = EnforceTokenAddressIsERC20Version(self.quoteToken)
                # No need to wrap or unwrap ETH
                doWrapEthBeforeFirstTrade = False
                doUnwrapEthAfterLastTrade = False

        # Create the quoteTokensToSpendArray which is made up of quoteTokensToSpend for each trade
        quoteTokenAmount_etherUnits = self.quoteTokenAmount

        # Apply the multiplier in case I'm testing
        quoteTokenAmount_etherUnits *= Libraries.defaults.NinjaTakerAmountMultiplier
        PrintAndLog_FuncNameHeader("quoteTokenAmount_etherUnits = " + str(quoteTokenAmount_etherUnits) + " after applying NinjaTakerAmountMultiplier")

        quoteTokensToSpend_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(quoteTokenAmount_etherUnits, quoteTokenAddressToUse)

        # Populate quoteTokensToSpendArray with the first trade's quoteTokensToSpend and all middle trade's quoteTokensToSpend
        # The last trade will use the same value as the first, so do not include it

        quoteTokensToSpendArray = []
        # First trade
        quoteTokensToSpendArray.append(int(quoteTokensToSpend_weiUnits))

        # Middle trades
        for pathIndex in range(1, len(self.exchangeNamesList) - 1):
            quoteTokenTradeAmount, baseTokenTradeAmount = self.GetExpectedTokenQuantitiesTraded(pathIndex)
            quoteToken, baseToken = self.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
            quoteTokenTradeAmount_weiUnits = Libraries.core.ConvertEtherToWei_GivenTokenAddress(quoteTokenTradeAmount, quoteToken)
            PrintAndLog_FuncNameHeader("Determining middle trade quoteTokenAmount, quoteTokenTradeAmount = " + str(
                quoteTokenTradeAmount) + " for quoteToken " + str(quoteToken) + ", quoteTokenTradeAmount_weiUnits = " + str(quoteTokenTradeAmount_weiUnits))
            # Add the middle trade's quoteTokenAmount to quoteTokensToSpendArray
            quoteTokensToSpendArray.append(int(quoteTokenTradeAmount_weiUnits))

        # Do not add the last trade's quoteTokenAmount since it will just be assumed to be the same as the first

        PrintAndLog_FuncNameHeader("quoteTokenAddressToUse = " + str(quoteTokenAddressToUse))
        PrintAndLog_FuncNameHeader("doWrapEthBeforeFirstTrade = " + str(doWrapEthBeforeFirstTrade))
        PrintAndLog_FuncNameHeader("doUnwrapEthAfterLastTrade = " + str(doUnwrapEthAfterLastTrade))
        PrintAndLog_FuncNameHeader("quoteTokensToSpendArray = " + str(quoteTokensToSpendArray))

        return doWrapEthBeforeFirstTrade, doUnwrapEthAfterLastTrade, quoteTokenAddressToUse, quoteTokensToSpendArray

    def DetermineMaxQuoteTokenTradeLossAcceptable_etherUnits(self):
        from Libraries.exchanges import ExchangeDict

        # Iterate over all the exchanges in this trade and call GetMaxQuoteTokenTradeLossAcceptable_etherUnits
        # Use the largest value between all exchanges
        # For example, Uniswap might be 0 and Set might be 0.03, so go with 0.03
        maxQuoteTokenTradeLossAcceptable_etherUnits = 0
        for exchangeName in self.exchangeNamesList:
            # PrintAndLog_FuncNameHeader("GetMaxQuoteTokenTradeLossAcceptable_etherUnits for " + str(exchangeName) + " with quoteToken " + str(
            #     self.quoteToken) + " = " + str(ExchangeDict[exchangeName].GetMaxQuoteTokenTradeLossAcceptable_etherUnits(self.quoteToken)))
            maxQuoteTokenTradeLossAcceptable_etherUnits = max(
                ExchangeDict[exchangeName].GetMaxQuoteTokenTradeLossAcceptable_etherUnits(self.quoteToken),
                maxQuoteTokenTradeLossAcceptable_etherUnits)

        # PrintAndLog_FuncNameHeader("returning maxQuoteTokenTradeLossAcceptable_etherUnits = " + str(
        #     maxQuoteTokenTradeLossAcceptable_etherUnits))
        return maxQuoteTokenTradeLossAcceptable_etherUnits

    def GetExpectedGasUsage_AllTradeActions(self):
        from Libraries.exchanges import ExchangeDict

        gasSum = 0
        gasSum += Libraries.core.GasUsedWhenExecutingEmptyFunctionWithNothingInIt
        gasSum += Libraries.estimatedGas.EstimatedGasOverheadCost_NinjaTxCallData
        gasSum += Libraries.estimatedGas.EstimatedGasOverheadCost_General

        # TODO, this a chicken/egg problem,
        #  I need to calculate the trade profit so I can get an idea of how many quoteTokenAmount we are trading
        #  so I can decide whether or not i'm flash loaning
        # Factor in flash loan overhead gas costs only when quoteTokenSource is true
        if self.GetBool_DoTradeWithinFlashLoan():
            gasSum += Exchanges.keeperDAO.GasCost_EstimatedOverheadGasUsedForTradingViaFlashLoan

        for exchangeName in self.exchangeNamesList:
            exchange = ExchangeDict[exchangeName]
            gasSum += exchange.GetExpectedGasUsage_Check()
            gasSum += exchange.GetExpectedGasUsage_Trade()

        try:
            if Libraries.defaults.VerboseLogging_GetExpectedGasUsage_AllTradeActions:
                PrintAndLog_FuncNameHeader("gasSum BEFORE = " + str(gasSum))

            # tokenTransferOverHeadCost_quoteToken = Libraries.tokenTransferCost.GetAdditionalOverheadTokenTransferGasCost_DifferenceFromMinOfAllTokens(self.quoteToken)
            #
            # tokenTransferOverHeadCost_baseTokens = 0
            # for baseToken in self.GetListOfBaseTokensFromTradePath():
            #     tokenTransferOverHeadCost_baseToken = Libraries.tokenTransferCost.GetAdditionalOverheadTokenTransferGasCost_DifferenceFromMinOfAllTokens(baseToken)
            #     tokenTransferOverHeadCost_baseTokens += tokenTransferOverHeadCost_baseToken
            #     if VerboseLogging_GetExpectedGasUsage_AllTradeActions:
            #         PrintAndLog_FuncNameHeader("baseToken " + str(baseToken) + " costs more than the cheapest token to transfer by " + str(
            #             tokenTransferOverHeadCost_baseToken))
            #
            # gasSum += tokenTransferOverHeadCost_quoteToken
            # gasSum += tokenTransferOverHeadCost_baseTokens

            # if VerboseLogging_GetExpectedGasUsage_AllTradeActions:
            #     PrintAndLog_FuncNameHeader("self.quoteToken " + str(self.quoteToken) + " costs more than the cheapest token to transfer by " + str(
            #         tokenTransferOverHeadCost_quoteToken))
            #     PrintAndLog_FuncNameHeader("self.GetListOfBaseTokensFromTradePath() " + str(
            #         self.GetListOfBaseTokensFromTradePath()) + " all together cost more than the cheapest token to transfer by " + str(
            #         tokenTransferOverHeadCost_baseTokens))
            #     PrintAndLog_FuncNameHeader("gasSum AFTER = " + str(gasSum))

            # TODO iterate over pathIndex in exchangeNamesList
            #  get the base and quoteTokens for each pathIndex
            #  calculate the tokenTransferOverHeadCost for every single token transfer
            for pathIndex, local_exchangeName in enumerate(self.exchangeNamesList):
                quoteToken, baseToken = self.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(pathIndex)
                tokenTransferOverHeadCost_quoteToken = Libraries.tokenTransferCost.GetAdditionalOverheadTokenTransferGasCost_DifferenceFromMinOfAllTokens(quoteToken)
                tokenTransferOverHeadCost_baseToken = Libraries.tokenTransferCost.GetAdditionalOverheadTokenTransferGasCost_DifferenceFromMinOfAllTokens(baseToken)
                if Libraries.defaults.VerboseLogging_GetExpectedGasUsage_AllTradeActions:
                    PrintAndLog_FuncNameHeader("Analyzing tokenTransferOverHeadCost for pathIndex = " + str(pathIndex) + " and quoteToken " + str(
                        quoteToken) + " costs more than the cheapest token to transfer by " + str(tokenTransferOverHeadCost_quoteToken))
                    PrintAndLog_FuncNameHeader("Analyzing tokenTransferOverHeadCost for pathIndex = " + str(pathIndex) + " and baseToken " + str(
                        baseToken) + " costs more than the cheapest token to transfer by " + str(tokenTransferOverHeadCost_baseToken))

                gasSum += tokenTransferOverHeadCost_quoteToken
                gasSum += tokenTransferOverHeadCost_baseToken

            if Libraries.defaults.VerboseLogging_GetExpectedGasUsage_AllTradeActions:
                PrintAndLog_FuncNameHeader("gasSum AFTER = " + str(gasSum))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when checking gas costs in TokenTransferGasCostDict, " + traceback.format_exc())

        return int(gasSum)

    def GetSuggestedGasLimit_AllTradeActions(self):
        from Libraries.estimatedGas import Multiplier_GetSuggestedGasLimit
        # Gas tokens will be burned, so we want to allow for a lot of room when providing a gas limit
        return int(self.GetExpectedGasUsage_AllTradeActions() * Multiplier_GetSuggestedGasLimit)

    def CalculateTradingFee_quoteTokens(self):
        from Libraries.exchanges import ExchangeDict

        totalTradingFee_eth_weiUnits = 0
        for exchangeName in self.exchangeNamesList:
            exchange = ExchangeDict[exchangeName]
            tradingFee_eth_weiUnits = exchange.CalculateTradeFee_eth(self.gasPrice)
            if Libraries.defaults.VerboseLogging_CalculateTradingFee_quoteTokens:
                PrintAndLog_FuncNameHeader("tradingFee_eth_weiUnits for " + str(exchangeName) + " = " + str(tradingFee_eth_weiUnits))
            totalTradingFee_eth_weiUnits += tradingFee_eth_weiUnits

        if Libraries.defaults.VerboseLogging_CalculateTradingFee_quoteTokens:
            PrintAndLog_FuncNameHeader("totalTradingFee_eth_weiUnits = " + str(totalTradingFee_eth_weiUnits))
        totalTradingFee_eth_etherUnits = Libraries.core.ConvertWeiToEther(totalTradingFee_eth_weiUnits, Libraries.core.Ether_Decimals)
        if Libraries.defaults.VerboseLogging_CalculateTradingFee_quoteTokens:
            PrintAndLog_FuncNameHeader("totalTradingFee_eth_etherUnits = " + str(totalTradingFee_eth_etherUnits))

        # Convert totalTradingFee_eth_etherUnits to totalTradingFee_quoteTokens
        price_quoteToken_wrt_eth = Libraries.priceOracle.GetOnChainPrice_FromCache(self.quoteToken.lower())
        if Libraries.defaults.VerboseLogging_CalculateTradingFee_quoteTokens:
            PrintAndLog_FuncNameHeader("price_quoteToken_wrt_eth = " + str(price_quoteToken_wrt_eth))
        totalTradingFee_quoteTokens_etherUnits = Libraries.core.ConvertQuoteTokensToBaseTokens(totalTradingFee_eth_etherUnits, price_quoteToken_wrt_eth)
        if Libraries.defaults.VerboseLogging_CalculateTradingFee_quoteTokens:
            PrintAndLog_FuncNameHeader("totalTradingFee_quoteTokens_etherUnits = " + str(totalTradingFee_quoteTokens_etherUnits))
            PrintAndLog_FuncNameHeader("Convert totalTradingFee_eth_etherUnits to totalTradingFee_quoteTokens_etherUnits, "
                                       "they should be the same value. totalTradingFee_eth_etherUnits "
                                       "" + str(totalTradingFee_eth_etherUnits) + " ETH should equal " + str(totalTradingFee_quoteTokens_etherUnits) + " " + str(self.quoteToken))

        return totalTradingFee_quoteTokens_etherUnits

    def CalculatePercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly(self, frontRunningGasPriceType, profitPercentageAdder):
        from Libraries.exchanges import ExchangeDict

        smallestValue = None
        for exchangeName in self.exchangeNamesList:
            exchange = ExchangeDict[exchangeName]

            exchangeValue = None
            if frontRunningGasPriceType == FrontRunningGasPriceType.MaxGrimTrigger:
                exchangeValue = exchange.GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxGrimTrigger()
            elif frontRunningGasPriceType == FrontRunningGasPriceType.MaxKeepAhead:
                exchangeValue = exchange.GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_MaxKeepAhead()
            elif frontRunningGasPriceType == FrontRunningGasPriceType.OpeningBid:
                exchangeValue = exchange.GetPercentageOfExpectedProfit_toSpendOnGas_DynamicGasPriceOnly_OpeningBid()
            else:
                raise Exception("frontRunningGasPriceType not supported, did I add a new feature?")

            if not smallestValue or exchangeValue < smallestValue:
                smallestValue = exchangeValue

        # Apply the profitPercentageAdder
        smallestValue += profitPercentageAdder

        return smallestValue

    def DetermineVillainsHighestGasPriceInMemPool(self):
        mempoolAnalysisStrings = self.GetMempoolAnalysisStrings()
        PrintAndLog_FuncNameHeader("mempoolAnalysisStrings = " + str(mempoolAnalysisStrings))
        copy_MempoolTxDict = GetThreadSafeCopyOf_MempoolTxDict()

        mempoolTxsWhoseDataMatches = []
        for txHash in copy_MempoolTxDict:
            mempoolTx = copy_MempoolTxDict[txHash]
            doesMatchAll = True

            # Make sure the data at least has something in it, so we aren't front running someone sending ether
            if len(mempoolTx.data.lower()) < 40:
                doesMatchAll = False

            # If we are requiring that MempoolAnalysisStrings match in order to front run, let's verify that they do match
            if Libraries.defaults.RequireMempoolAnalysisStringsToMatchInOrderToFrontRunVillains:
                for mempoolAnalysisString in mempoolAnalysisStrings:
                    if mempoolAnalysisString.lower() not in mempoolTx.data.lower():
                        doesMatchAll = False

            if doesMatchAll:
                mempoolTxsWhoseDataMatches.append(mempoolTx)

        if not Libraries.defaults.FrontRunningTargetsRequireAWatchListVillain:
            Libraries.core.API_PostOperatorNotification("FrontRunningTargetsRequireAWatchListVillain feature not yet implemented. Fix ASAP")
            raise Exception("Not yet implemented")

        # Determine the max gas price for all mempoolTxs in mempoolTxsWhoseDataMatches
        villainsHighestGasPrice = None
        villainAddress = None
        villainToAddress = None
        villainFromAddress = None
        villainData = None
        villainValue = None
        villainGasLimit = None
        for mempoolTx in mempoolTxsWhoseDataMatches:
            try:
                # FrontRunningTargetsRequireAWatchListVillain being false is not yet implemented, so just continue if not mempoolTx.villainAddress for now
                if not mempoolTx.villainAddress:
                    continue

                PrintAndLog_FuncNameHeader("Mempool " + str(mempoolTx.source) + ", villain's gas price = " + str(
                    Libraries.core.ConvertWeiToGwei(mempoolTx.gasPrice_wei)) + " gwei, villain = " + str(
                    mempoolTx.villainAddress) + ", txHash = " + str(mempoolTx.txHash))
                doSetNewVillain = False
                if not villainsHighestGasPrice:
                    doSetNewVillain = True
                else:
                    if mempoolTx.gasPrice_wei > villainsHighestGasPrice:
                        doSetNewVillain = True

                if doSetNewVillain:
                    villainsHighestGasPrice = mempoolTx.gasPrice_wei
                    villainAddress = mempoolTx.villainAddress
                    villainToAddress = mempoolTx.toAddress
                    villainFromAddress = mempoolTx.fromAddress
                    villainData = mempoolTx.data
                    villainValue = mempoolTx.value
                    villainGasLimit = mempoolTx.gas

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception , " + traceback.format_exc())

        # Return None if there isn't any front running competition
        if not villainsHighestGasPrice:
            return None, None

        PrintAndLog_FuncNameHeader("villainsHighestGasPrice = " + str(Libraries.core.ConvertWeiToGwei(villainsHighestGasPrice)) + " gwei")
        return villainsHighestGasPrice, villainAddress

    def GetMempoolAnalysisStrings(self):
        from Libraries.exchanges import ExchangeDict

        # Build a list of strings that will be used to determine whether or not a transaction is relevant to a trade we're pursuing
        mempoolAnalysisStrings = []

        for token in self.GetMempoolAnalysisTokens():
            mempoolAnalysisStrings.append(token.replace("0x", ""))

        # Get exchange specific mempoolAnalysisStrings
        for exchangeName in self.exchangeNamesList:
            exchange = ExchangeDict[exchangeName]
            for baseToken in self.GetListOfBaseTokensFromTradePath():
                # Combine lists
                mempoolAnalysisStrings += exchange.GetMempoolAnalysisStrings(baseToken)

        return mempoolAnalysisStrings

    # def GetUniqueMetaDataString(self):
    #     from Libraries.exchanges import ExchangeDict
    #
    #     try:
    #         # Combine all the exchanges metadata in this arbitrageOpportunity into one single string
    #         combinedUniqueMetaDataString = ''
    #         for pathIndex, exchangeName in enumerate(self.exchangeNamesList):
    #             exchange = ExchangeDict[exchangeName]
    #             uniqueMetaDataString = exchange.GetUniqueMetaDataString(self, pathIndex)
    #             if uniqueMetaDataString:
    #                 # Combine them, but separate exchanges metadata by something
    #                 if combinedUniqueMetaDataString:
    #                     combinedUniqueMetaDataString += "_"
    #
    #                 combinedUniqueMetaDataString += uniqueMetaDataString
    #
    #         return combinedUniqueMetaDataString
    #
    #     except (KeyboardInterrupt, SystemExit):
    #         print('\nkeyboard interrupt caught')
    #         print('\n...Program Stopped Manually!')
    #         raise
    #
    #     except:
    #         PrintAndLogError("exception when generating a unique metadata string, " + traceback.format_exc())

    def FinalizeQuoteTokenAmount(self, resultForApiCallDict, tradingTechnique,
                                 tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        # Our original information tells us that trading this quoteTokenAmount is profitable, but it may not be exactly the most profitable amount possible.
        # Here we will try slightly higher and lower amounts until it finds the sweet spot for this particular trade
        # Then we need to update the variables in this ArbitrageOpportunity so that things like
        # quoteTokenAmount, profitPercent, gasPrice, etc all reflect the new trade amount since all of that will change as a result

        PrintAndLog_FuncNameHeader("Begin on arbitrageOpportunity " + str(self.GenerateDescriptionString()))
        PrintAndLog_FuncNameHeader("quoteTokenAmount = " + str(self.quoteTokenAmount) + ", profitPercent = " + str(self.profitPercent))
        # dict keyed by pathIndex, valued by metaData required to determine the rate
        rateFormulaMetaDataDict = {}
        threads = []
        for pathIndex, exchangeName in enumerate(self.exchangeNamesList):
            # Get the API call result from the resultForApiCallDict
            # Some exchanges will not have a result at all because they didn't need to make an API call
            resultForApiCall = None
            if pathIndex in resultForApiCallDict:
                resultForApiCall = resultForApiCallDict[pathIndex]

            ExchangeDict[exchangeName].GetRateFormulaMetaData(resultForApiCall, self, pathIndex, rateFormulaMetaDataDict, pathIndex)

        # Wait for threads to finish
        for thread in threads:
            thread.join()

        for pathIndex, exchangeName in enumerate(self.exchangeNamesList):
            # To help with readability in logs, mark the dict with the exchangeName
            # Do not let this somehow cause an exception
            try:
                rateFormulaMetaDataDict[pathIndex]['exchangeName'] = exchangeName

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                errorMessage = "exception = " + traceback.format_exc()
                PrintAndLog_FuncNameHeader(errorMessage)
                PrintAndLogError(errorMessage)

        # Before we can find the sweet spot, check to see whether or not we're supposed to flash loan based on our early trade quoteTokenAmount indications
        # If we are currently supposed to trade using Ninja assets, then we don't want FindSweetSpot_FinalizeQuoteTokenAmount to bump us up to flash loan size trades
        # because that will add about ~50k worth of gas which will ruin our profitability because we're trying to inch up our profit margin
        PrintAndLog_FuncNameHeader("doTradeWithinFlashLoan before = " + str(self.GetBool_DoTradeWithinFlashLoan()))
        maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold = None
        # If we're trading with Ninja tokens and NOT flash loaning
        if not self.GetBool_DoTradeWithinFlashLoan():
            # Make sure this doesn't bump us barely over the threshold to flash loan
            # Get the ninja balance because we know if we go over this balance, we'll get bumped up to flash loan mode
            balance_Ninja_quoteToken = Exchanges.ninja.NinjaInstance_Generic.GetQuoteTokenBalance_Ninja(self.quoteToken)
            maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold = balance_Ninja_quoteToken

        PrintAndLog_FuncNameHeader("maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold = " + str(maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold))

        # Find the sweet spot of profitability.
        # Starting at self.quoteTokenAmount, increase and decrease this amount and check profitability.
        # Continue increasing/decreasing if profitability improves. Break when profitability stops improving
        PrintAndLog_FuncNameHeader("rateFormulaMetaDataDict = " + str(rateFormulaMetaDataDict))
        PrintAndLog_FuncNameHeader("Find the sweet spot of profitability")

        quoteTokenAmount_forLargestProfit, profit_quoteTokens_forLargestProfit = self.FindSweetSpot_FinalizeQuoteTokenAmount(
            rateFormulaMetaDataDict, maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold, tradingTechnique,
            tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating)
        PrintAndLog_FuncNameHeader("quoteTokenAmount_forLargestProfit = " + str(quoteTokenAmount_forLargestProfit))
        PrintAndLog_FuncNameHeader("profit_quoteTokens_forLargestProfit = " + str(profit_quoteTokens_forLargestProfit))

        # Call SetQuoteTokenSource so that we can re-set the parameters that say whether or not we are flash loaning or using local tokens
        canSourceQuoteTokensForTrade = self.SetQuoteTokenSource()
        if not canSourceQuoteTokensForTrade:
            message = "SetQuoteTokenSource failed inside FinalizeQuoteTokenAmount because canSourceQuoteTokensForTrade came back as False. This should never happen!"
            Libraries.core.API_PostOperatorNotification(message)
            raise Exception(message)

        PrintAndLog_FuncNameHeader("doTradeWithinFlashLoan after = " + str(self.GetBool_DoTradeWithinFlashLoan()))

        # Update this arbitrageOpportunity with the new values we just calculated
        self.quoteTokenAmount = quoteTokenAmount_forLargestProfit
        self.profit_quoteTokens = profit_quoteTokens_forLargestProfit
        self.profitPercent = self.profit_quoteTokens / self.quoteTokenAmount
        PrintAndLog_FuncNameHeader("self.quoteTokenAmount = " + str(self.quoteTokenAmount))
        PrintAndLog_FuncNameHeader("self.profit_quoteTokens = " + str(self.profit_quoteTokens))
        PrintAndLog_FuncNameHeader("self.profitPercent = " + str(self.profitPercent))

        price_quoteToken_forConversion = Libraries.priceOracle.GetOnChainPrice_FromCache(self.quoteToken)
        PrintAndLog_FuncNameHeader("price_quoteToken_forConversion = " + str(price_quoteToken_forConversion))
        self.profit_eth = Libraries.core.ConvertBaseTokensToQuoteTokens(self.profit_quoteTokens, price_quoteToken_forConversion)
        PrintAndLog_FuncNameHeader("self.profit_eth = " + str(self.profit_eth))

        # CalculateGasPrice will set the gasPrice properties we need to update
        # call with the self.gasPriceFromConfig from the original time we called this function
        self.CalculateGasPrice(self.gasPriceFromConfig)

        PrintAndLog_FuncNameHeader("self.gasPrice = " + str(Libraries.core.ConvertWeiToGwei(self.gasPrice)))
        PrintAndLog_FuncNameHeader("self.gasPrice_maxKeepAhead = " + str(Libraries.core.ConvertWeiToGwei(self.gasPrice_maxKeepAhead)))
        PrintAndLog_FuncNameHeader("self.gasPrice_maxGrimTrigger = " + str(Libraries.core.ConvertWeiToGwei(self.gasPrice_maxGrimTrigger)))

        # This should still be profitable
        if not self.IsProfitable():
            message = "IsProfitable failed inside FinalizeQuoteTokenAmount. This should never happen!"
            Libraries.core.API_PostOperatorNotification(message)
            raise Exception(message)

        # Sanity check on the estimatedGasCost_ether should still pass
        estimatedGasCost_ether = self.GetEstimatedGasCost_ether()
        # If the gas cost math doesn't make any sense and we appear to be spending way more on gas than we're going to make
        if not self.DoesPassGasPriceSanityCheck_EstimatedGasCost_ether(estimatedGasCost_ether):
            message = "gasPrice sanity check failed inside FinalizeQuoteTokenAmount. This should never happen!"
            Libraries.core.API_PostOperatorNotification(message)
            raise Exception(message)

        # If we've made it this far without an exception, we are all set to continue with the trade with our new properties

    def FindSweetSpot_FinalizeQuoteTokenAmount(self, rateFormulaMetaDataDict, maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold,
                                               tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        # Call the function on both directions, increase and decrease
        local_quoteTokenAmount_forLargestProfit_increase, profit_quoteTokens_forLargestProfit_increase = \
            self.FindSweetSpot_FinalizeQuoteTokenAmount_OneDirectional(
                rateFormulaMetaDataDict, maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold,
                ProfitDirection.Increase, tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating)
        local_quoteTokenAmount_forLargestProfit_decrease, profit_quoteTokens_forLargestProfit_decrease = \
            self.FindSweetSpot_FinalizeQuoteTokenAmount_OneDirectional(
                rateFormulaMetaDataDict, maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold,
                ProfitDirection.Decrease, tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating)

        # Now just return the result for the one that has the largest profit
        # But we have to check for Nones in a few cases
        if not profit_quoteTokens_forLargestProfit_decrease and not profit_quoteTokens_forLargestProfit_increase:
            raise Exception("Neither increasing nor decreasing the quoteTokenAmount showed any profit. Is there really profit here or do I have a bug??")
        elif profit_quoteTokens_forLargestProfit_decrease and not profit_quoteTokens_forLargestProfit_increase:
            return local_quoteTokenAmount_forLargestProfit_decrease, profit_quoteTokens_forLargestProfit_decrease
        elif profit_quoteTokens_forLargestProfit_increase and not profit_quoteTokens_forLargestProfit_decrease:
            return local_quoteTokenAmount_forLargestProfit_increase, profit_quoteTokens_forLargestProfit_increase
        elif profit_quoteTokens_forLargestProfit_increase >= profit_quoteTokens_forLargestProfit_decrease:
            return local_quoteTokenAmount_forLargestProfit_increase, profit_quoteTokens_forLargestProfit_increase
        elif profit_quoteTokens_forLargestProfit_decrease > profit_quoteTokens_forLargestProfit_increase:
            return local_quoteTokenAmount_forLargestProfit_decrease, profit_quoteTokens_forLargestProfit_decrease
        else:
            raise Exception("This should not be possible. I am bad at programming")

    def FindSweetSpot_FinalizeQuoteTokenAmount_OneDirectional(self, rateFormulaMetaDataDict, maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold, profitDirection,
                                                              tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
        before = datetime.datetime.now()
        # If maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold is None, then there is no max
        # if maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold is not None, then we have to obey this as a max and do not let it go higher
        # Decrease stepPercentage for more precision, Increase stepPercentage is better for performance
        # This stepPercentage should be small, should always be less than 0.1, the smaller the better unless it hurts performance
        stepPercentage = 0.02
        # Increase numOfLoops for more precision, decrease numOfLoops is better for performance
        # Note no APi calls are being done in here so performance is not much of an issue
        numOfLoops = 150
        local_quoteTokenAmount_forLargestProfit = None
        profit_quoteTokens_forLargestProfit = None

        # Initialize to self.quoteTokenAmount
        # initial_quoteTokenAmount = self.quoteTokenAmount
        # For exchanges like 0x it makes sense to start lower than the self.quoteTokenAmount and work our way up
        # For exchanges like Uniswap/Balancer/etc, it shouldn't matter because we should still find the sweet spot regardless of where we start
        # TODO does this work? Test the hell out of it
        initial_quoteTokenAmount = self.quoteTokenAmount * 0.66

        # Set initial values to prepare for loop
        local_quoteTokenAmount = initial_quoteTokenAmount
        previous_profit_quoteTokens = None
        local_multiplier = 1.0
        for i in range(numOfLoops):
            # PrintAndLog_FuncNameHeader("local_quoteTokenAmount = " + str(local_quoteTokenAmount) + " ---------------------------------------")
            doContinueTheNumOfLoops = False

            if maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold:
                # PrintAndLog_FuncNameHeader("maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold is set so we're enforcing a max local_quoteTokenAmount. "
                #                            "maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold = " + str(maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold))
                if local_quoteTokenAmount > maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold:
                    # PrintAndLog_FuncNameHeader("Skipping this loop iteration because this local_quoteTokenAmount would push us from trading with "
                    #                            "local Ninja tokens to flash loaning and that would add ~50k gas and change the dynamics of this trade's gas costs. "
                    #                            "local_quoteTokenAmount = " + str(local_quoteTokenAmount) + ", maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold = " +
                    #                            str(maxQuoteTokenAmount_soWeDontGoOverFlashLoanThreshold))
                    continue

            local_inFlowTokenAmount = None
            local_outFlowTokenAmount = None
            flowAmountList = []
            for pathIndex, exchangeName in enumerate(self.exchangeNamesList):
                # PrintAndLog_FuncNameHeader("   pathIndex = " + str(pathIndex) + " of " + str(len(self.exchangeNamesList)) + ", exchangeName = " + str(exchangeName))

                if pathIndex == 0:
                    # Initial value on first loop iteration
                    local_inFlowTokenAmount = local_quoteTokenAmount
                    # PrintAndLog_FuncNameHeader("   local_inFlowTokenAmount = " + str(local_inFlowTokenAmount) + ", initialized based on local_quoteTokenAmount")
                    flowAmountList.append(local_inFlowTokenAmount)
                else:
                    # Set this based on local_outFlowTokenAmount which is the output of the previous trade
                    local_inFlowTokenAmount = local_outFlowTokenAmount
                    # PrintAndLog_FuncNameHeader("   local_inFlowTokenAmount = " + str(local_inFlowTokenAmount) + ", based on previous trade's outflow")

                metaData = rateFormulaMetaDataDict[pathIndex]
                # PrintAndLog_FuncNameHeader("   metaData = " + str(metaData))
                # Calculate rate
                rate = ExchangeDict[exchangeName].GetRate_FinalizeQuoteTokenAmount(
                    self, pathIndex, local_inFlowTokenAmount, metaData, tradingTechnique,
                    tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating)
                # PrintAndLog_FuncNameHeader("   rate = " + str(rate))
                if not rate:
                    # rate is None so let's skip this index,
                    # we could consider breaking here but let's just continue in case we get valid data later on in the loop
                    # it's likely that one NOne will be followed by all Nones, but just in case let's examine them all
                    # PrintAndLog_FuncNameHeader("   Rate was None, skipping this one")
                    # We are completely done with this loop and we want to continue the other loop
                    doContinueTheNumOfLoops = True
                    break

                # Calculate local_outFlowTokenAmount
                local_outFlowTokenAmount = local_inFlowTokenAmount * rate
                # PrintAndLog_FuncNameHeader("   local_outFlowTokenAmount = " + str(local_outFlowTokenAmount) + ", based on local_inFlowTokenAmount and rate")
                flowAmountList.append(local_outFlowTokenAmount)

                # Enforce the max possible amount traded through the exchange
                # If we're trying to trade more than they can handle, we need to skip this loop iteration
                pathIndexesLocal_quoteTokenAmount = None
                if self.IsQuoteTokenTheInFlowToken(pathIndex):
                    pathIndexesLocal_quoteTokenAmount = local_inFlowTokenAmount
                elif self.IsQuoteTokenTheOutFlowToken(pathIndex):
                    pathIndexesLocal_quoteTokenAmount = local_outFlowTokenAmount
                else:
                    raise Exception("Not quote or base?")

                # PrintAndLog_FuncNameHeader("   pathIndexesLocal_quoteTokenAmount = " + str(pathIndexesLocal_quoteTokenAmount))
                # PrintAndLog_FuncNameHeader("   metaData['maxAmount_quoteTokens'] = " + str(metaData['maxAmount_quoteTokens']))

                # If pathIndexesLocal_quoteTokenAmount exceeds the max
                if pathIndexesLocal_quoteTokenAmount > metaData['maxAmount_quoteTokens']:
                    # We cannot support this trade
                    # PrintAndLog_FuncNameHeader("   pathIndexesLocal_quoteTokenAmount exceeds the max, skipping this one")
                    # We are completely done with this loop and we want to continue the other loop
                    doContinueTheNumOfLoops = True
                    break

            if not doContinueTheNumOfLoops:
                # PrintAndLog_FuncNameHeader("doContinueTheNumOfLoops was False so we are updating the profit_quoteTokens")
                # PrintAndLog_FuncNameHeader("flowAmountList = " + str(flowAmountList))
                profit_quoteTokens = flowAmountList[len(flowAmountList) - 1] - flowAmountList[0]
                # PrintAndLog_FuncNameHeader("profit_quoteTokens = " + str(profit_quoteTokens))

                if not profit_quoteTokens_forLargestProfit or profit_quoteTokens > profit_quoteTokens_forLargestProfit:
                    local_quoteTokenAmount_forLargestProfit = local_quoteTokenAmount
                    profit_quoteTokens_forLargestProfit = profit_quoteTokens

                # TODO, testing without this feature in case some arbitrage curves are not what I expect
                #  I was originally assuming that the curves would have only one single peak,
                #  but i'm seeing some weird results come out of here so I'm going to turn this off to evaluate the curves myself
                #  and because it should not take more than a couple ms to run this loop 100 times anyways.
                #  It's worth a couple extra ms to ensure that I get the most effective profit without a shadow of a doubt
                # # Consider breaking from the loop
                # if previous_profit_quoteTokens:
                #     # If the profit is decreasing, there's no point in continuing this direction
                #     if previous_profit_quoteTokens > profit_quoteTokens:
                #         PrintAndLog_FuncNameHeader("breaking from the loop because profit is decreasing")
                #         break

                # Maintain previous
                previous_profit_quoteTokens = profit_quoteTokens

            # else:
            #     PrintAndLog_FuncNameHeader("doContinueTheNumOfLoops was True so we are NOT updating the profit_quoteTokens")

            # Do this regardless of doContinueTheNumOfLoops so we can advance local_quoteTokenAmount

            # Increase/Decrease the amount by a bit based on profitDirection
            if profitDirection == ProfitDirection.Increase:
                # PrintAndLog_FuncNameHeader("increase the amount by a bit")
                # Adjust the multiplier based on the stepPercentage
                local_multiplier *= 1.0 + stepPercentage
            else:
                # PrintAndLog_FuncNameHeader("decrease the amount by a bit")
                # Adjust the multiplier based on the stepPercentage
                local_multiplier *= 1.0 - stepPercentage

            # Apply the multiplier to the amount
            local_quoteTokenAmount = initial_quoteTokenAmount * local_multiplier
            # PrintAndLog_FuncNameHeader("local_quoteTokenAmount = " + str(local_quoteTokenAmount) +
            #                            " set based on initial_quoteTokenAmount and local_multiplier. local_multiplier = " + str(local_multiplier))

            # PrintAndLog_FuncNameHeader(" ------------------------------------------------------------------------------ ")

        duration_s = (datetime.datetime.now() - before).total_seconds()
        PrintAndLog_FuncNameHeader("Completed with duration_s = " + str(duration_s) + " seconds")

        return local_quoteTokenAmount_forLargestProfit, profit_quoteTokens_forLargestProfit

    def API_FineTuneTradeOpportunity(self, tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
                                     resultForApiCallDict, resultsDict=None, key=None):
        # This is where I make any last minute data gathering and analysis to fine tune the trade before we consider pursing a trade
        # Call FinalizeMetaDataBeforeTrade on all exchanges involved in the trade so we have all the info we need to check and trade
        for pathIndex, exchangeName in enumerate(self.exchangeNamesList):
            ExchangeDict[exchangeName].FinalizeMetaDataBeforeTrade(self, pathIndex)

        self.FinalizeQuoteTokenAmount(resultForApiCallDict, tradingTechnique,
                                      tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating)

        # If we've made it this far without exception, assume this was successful
        # Set the result, I don't care what the result is, I just want to make sure no exception occurred
        Libraries.utils.ConsiderSettingResultDict("completed", resultsDict, key)

    def PursueTradeOpportunity(self, header_logging, functionsStartDateTime, blockNumber,
                               reservableEthereumAccountPool, gasPriceIsAllowedToIncreaseForFrontRunning,
                               tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
                               tailgatingTxHash=None, stringToAppendToReserveAccountName='', doSubtract1WeiFromGasPrice=False,
                               readySetGoCommunicationDict=None, key_readySetGoCommunicationDict=None):
        from Libraries.exchanges import ExchangeDict, Exchange_Decentralized
        from Exchanges.ninja import PreparePackedTradeCallData_GivenContractData

        # If reservableEthereumAccountPool == None, we are purely calling this function to see the txMinePrediction outcomes (eth_estimateGas and eth_call)
        immediatelyReturnAfterTxMinePrediction = False
        if not reservableEthereumAccountPool:
            immediatelyReturnAfterTxMinePrediction = True

        # This is my debug code that prevents trading after n trades are triggered
        # Note that PursueTradeOpportunity can be called only for the txMinePrediction
        # So I want to only run this debug code if we are actually going to trade
        # Which means I need to check reservableEthereumAccountPool
        if reservableEthereumAccountPool and Libraries.defaults.PreventTradingAfterNTradesAreTriggered_MaxTradesAllowed:
            # Increment the current count
            Libraries.defaults.PreventTradingAfterNTradesAreTriggered_CurrentTradeCount += 1

            if Libraries.defaults.PreventTradingAfterNTradesAreTriggered_CurrentTradeCount > Libraries.defaults.PreventTradingAfterNTradesAreTriggered_MaxTradesAllowed:
                Libraries.core.API_PostOperatorNotification("Not trading because PreventTradingAfterNTradesAreTriggered was set")
                if immediatelyReturnAfterTxMinePrediction:
                    # Tell the readySetGoCommunicationDict we failed
                    readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Returning")
                # No cleanup needed here?? This is a unique case. Commenting out the cleanup for now
                return

        ninjaAccount_toUse = None
        accountWasInUsePriorToReserving = None

        try:
            header_logging += ": " + str(self.GenerateDescriptionString())

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Begin with immediatelyReturnAfterTxMinePrediction = " + str(immediatelyReturnAfterTxMinePrediction))
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "arbitrageOpportunity.metaDataDict['buy'] = " + str(self.metaDataDict['buy']))
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "arbitrageOpportunity.metaDataDict['sell'] = " + str(self.metaDataDict['sell']))

            # TODO REMOVEME, I moved this to API_FineTuneTradeOpportunitys
            # # Call FinalizeMetaDataBeforeTrade on all exchanges involved in the trade so we have all the info we need to check and trade
            # for pathIndex, exchangeName in enumerate(self.exchangeNamesList):
            #     ExchangeDict[exchangeName].FinalizeMetaDataBeforeTrade(self, pathIndex)
            #
            # self.FinalizeQuoteTokenAmount(tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating)

            # Preparing calldata for trade

            checkCallDataArrays_encoded = []
            # Call GenerateCallData_Check on all exchanges involved in the trade
            for pathIndex, exchangeName in enumerate(self.exchangeNamesList):
                checkCallData_Array = ExchangeDict[self.exchangeNamesList[pathIndex]].GenerateCallData_Check(self, pathIndex, tradingTechnique)
                if len(checkCallData_Array) > 0:
                    # Using the first index hard coded for now, in the future I could use more but that logic is a bit complicated and will require a refactor
                    checkCallDataArrays_encoded.append(checkCallData_Array[0])

            tradeCallDataArray_encoded_Array = Exchange_Decentralized.GenerateCallData_Trades_Generic(self)
            # Using the first index hard coded for now, in the future I could use more but that logic is a bit complicated and will require a refactor
            tradeCallDataArray_encoded = tradeCallDataArray_encoded_Array[0]
            if Libraries.defaults.VerboseLogging_PursueTradingOpportunity_CallData:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "tradeCallDataArray_encoded for " + str(self.GenerateDescriptionString()) + " = " + str(tradeCallDataArray_encoded))

            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "tradeCallDataArray_encoded_Array has " + str(len(tradeCallDataArray_encoded_Array)) + " group of trades")
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "checkCallDataArrays_encoded has " + str(len(checkCallDataArrays_encoded)) + " checks")

            # Get an account to trade with
            reserveAccountName = "NinjaGeneric" + "_" + str(tradingTechnique.value) + "_" + str(stringToAppendToReserveAccountName) + "_" + str(
                self.quoteToken) + "_" + str(self.tradePath)
            # TODO, this has caused problems, i'm firing to many trades on the same token across paths that are related
            #  Example, I'm trying to trade across both Uniswap-0x and Uniswap-UniswapV2 and only one of them can win while the other has to lose.
            #  I'm removing my uniqueMetaDataString from the reserveAccountName until I can perfect that
            # # Add metadata to the reserveAccountName
            # uniqueMetaDataString = self.GetUniqueMetaDataString()
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "uniqueMetaDataString = " + str(uniqueMetaDataString))
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "reserveAccountName before adding uniqueMetaDataString = " + str(reserveAccountName))
            # if uniqueMetaDataString:
            #     reserveAccountName += "_" + uniqueMetaDataString

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "reserveAccountName after adding uniqueMetaDataString = " + str(reserveAccountName))

            if reservableEthereumAccountPool:
                # Reserve an operator account to trade with
                ninjaAccount_toUse, accountWasInUsePriorToReserving = reservableEthereumAccountPool.ReserveAccountFromPool(reserveAccountName)
            else:
                # reservableEthereumAccountPool was not specified,
                # Assume a dedicated account for the txMinePrediction outcomes like eth_estimateGas and eth_call
                # Need to make sure this dedicated account always has a good amount of ETH in it
                ninjaAccount_toUse = DedicatedTxPredictionAccount
                accountWasInUsePriorToReserving = False

                # Determine the actual quoteTokenAddressToUse
            # This is only useful for ETH/WETH because some protocols require ETH and some require WETH
            # NOTE, this should be called AFTER we call ReserveAccountFromPool with our reserveAccountName
            dontCare, dontCare, quoteTokenAddressToUse, quoteTokensToSpendArray = self.Prepare_Trade()

            gasPrice_toUse = self.gasPrice
            if doSubtract1WeiFromGasPrice:
                gasPrice_toUse -= 1

            roundNumber_gasPrice = 4
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "gasPrice_toUse = " + str(round(Libraries.core.ConvertWeiToGwei(gasPrice_toUse), roundNumber_gasPrice)) + " gwei")
            if Libraries.defaults.Ninja_UseCheapGasPricesForTrades:
                # gasPrice_toUse = Libraries.gasStation.GetGasPrice_SafeLow()
                gasPrice_toUse = Libraries.gasStation.GetGasPrice_Cheap()
                # gasPrice_toUse = Libraries.gasStation.GetGasPrice_Fast()
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Overriding gasPrice_toUse with " + str(round(Libraries.core.ConvertWeiToGwei(gasPrice_toUse), roundNumber_gasPrice))
                                     + " gwei because Ninja_UseCheapGasPricesForTrades was set to True")

            nonce, dontCare = Libraries.cache.GetTransactionCountFromCacheIfPossible(ninjaAccount_toUse.publicAddress)

            # Delay starting the trade loop until a certain amount of time has elapsed
            # TODO, put some conditional logic around when to skip this delay loop and when to use it?
            #  For example, do not delay for high value trades but do delay for low value trades??
            while True:
                timeSinceBlockWasDiscovered_seconds = Libraries.core.GetTimeSinceBlockWasDiscovered(blockNumber)
                if timeSinceBlockWasDiscovered_seconds < Libraries.defaults.DelayTheTradeLoopUntilBlockDiscoveryTimeHasReachedThisElapsedDuration_seconds:
                    time.sleep(0.05)
                else:
                    # Break from the delay loop and enter the trade loop
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "Breaking from the delay loop, entering the trade loop. "
                                         "timeSinceBlockWasDiscovered_seconds = " + str(timeSinceBlockWasDiscovered_seconds) + " seconds")
                    break

            # Begin a loop where we trade, monitor the trade, front run that trade, auto increment gas prices, and even cancel that trade
            doSubmitNewFrontRunningTransaction = True
            # Track the trade reason to make the logs easier to follow
            tradeReason = TradeReason.OpeningBid.value
            timeout_s = 300
            duration_s_tradeLoop = 0
            # Init this to some time long in the past
            dateTimeOf_lastTrade = datetime.datetime.now() - datetime.timedelta(hours=1)
            dateTimeOf_tradeLoopBegin = datetime.datetime.now()
            transactionId_initialThatPassedTxMinePrediction = None
            # TODO add feature to this loop, when the arbitrage opportunity disappears for this path, break from this thread and move on.
            #  So my Arbitrage logic that determines the arbitrage opportunity needs to somehow communicate to this loop that there's no longer arbitrage
            #  When that happens, I need to issue a cancel using this nonce. This serves 2 purposes:
            #    1.) If my transaction didn't get mined in, it now cancels my trade attempt
            #    2.) If my transaction did get mined in and the arbitrage opportunity is gone, I can break from this loop rather than watching it go on forever
            index = 0
            # Count how many times the txMinePredictionWasSuccessful fails, we want to let it fail a few times before we quit trying
            # I've seen cases where eth_estimateGas fails the first time and works on the second time
            # so let's give it at least 4 or 5 tries before we assume it's going to fail
            failCount_txMinePrediction = 0
            # If it reaches this limit, break from the trade loop and assume it will never succeed
            maxFailLimit_txMinePrediction = 2
            # This is the last successfully shipped (or trade broadcasted tx) gas price that was used
            lastSuccessfullyBroadcasted_gasPrice_toUse = None
            # I'm going to continuously raise my gas price every x seconds until I hit a certain PercentageOfExpectedProfit market
            # Once I hit that certain market, i'll stop just blindly raising my gas price and instead just only raise it when someone else front runs me
            # This is a random duration that I'll wait between my rapid fire raise gas price using MaxKeepAhead
            frontRunning_RapidFireRaiseMyGasPriceEvery_X_milliseconds_UntilIReach_MaxKeepAhead = randint(300, 700)
            frontRunning_RapidFireRaiseMyGasPriceEvery_X_seconds_UntilIReach_MaxKeepAhead = \
                frontRunning_RapidFireRaiseMyGasPriceEvery_X_milliseconds_UntilIReach_MaxKeepAhead / 1000
            # tradeCount 0 will be the very first trade made
            # tradeCount 1 will be the first gas price increase for the same nonce
            # tradeCount 2 will be the second gas price increase for the same nonce.... etc
            # This is useful for logging
            tradeCount = 0
            while True:
                # TODO, is this right??
                #  At the beginning of this loop I need to reset gasPrice_toUse to lastSuccessfullyBroadcasted_gasPrice_toUse
                #  only if lastSuccessfullyBroadcasted_gasPrice_toUse is actually set
                #  my reasoning here is that this trade loop could have increased gasPrice_toUse but then failed to broadcast the transaction
                #  So I need to account for the fact that gasPrice_toUse shouldn't be that high since it was never broadcasted
                if lastSuccessfullyBroadcasted_gasPrice_toUse:
                    gasPrice_toUse = lastSuccessfullyBroadcasted_gasPrice_toUse

                if Libraries.defaults.VerboseLogging_PursueTradingOpportunity_TradeLoop:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "TradeLoop-" + str(ninjaAccount_toUse.publicAddress) + "-" + str(
                                             nonce) + " duration_s_tradeLoop = " + str(round(duration_s_tradeLoop, 1)) + " seconds")

                try:
                    # Consider breaking from the loop if our transaction got mined in
                    if index > 0:
                        currentTransactionCount, dontCare = Libraries.cache.GetTransactionCountFromCacheIfPossible(ninjaAccount_toUse.publicAddress)
                        if Libraries.defaults.VerboseLogging_PursueTradingOpportunity_TradeLoop:
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   Considering breaking if transaction with this nonce got mined in, comparing currentTransactionCount "
                                                 "= " + str(currentTransactionCount) + " and nonce = " + str(nonce))
                        if currentTransactionCount > nonce:
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   Breaking from the trade loop because currentTransactionCount (" + str(
                                                     currentTransactionCount) + ") exceeded our target nonce(" + str(
                                                     nonce) + "). We're going to assume this means our tx mined in")
                            break

                    # Consider canceling the trade (or attempting to at least)
                    # There are two ways I can cancel a trade.
                    # 1.) A trade path has been specifically flagged to be canceled
                    #   I can have logic that checks the next block's data and decides whether or not I want to cancel that outstanding trade
                    # 2.) A new block has been mined in so I'll cancel all pending trades that are still outstanding.
                    #   The thought here is that either my trade was hopefully just mined into this new block we were made aware of,
                    #   If my trade was mined in, than this cancel will be not work anyways because of nonce too low so it won't matter
                    #   If the trade wasn't mined in, we'll want the next block's trade loop it issue a trade because odds are the arbitrage is probably now gone or different
                    doCancelTrade = False
                    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()

                    # If this trade path has been flagged to be canceled because either profit is no longer there or maybe some other reason
                    if self.HasMyPathsTradeBeenSignaledToCancelTheTransaction_PursueTradeOpportunityPathsDict():
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   canceling trade because the path was signaled")
                        doCancelTrade = True
                    # if a new block has mined in since we got data for this trade opportunity
                    elif latestBlockNumber > blockNumber:
                        if tradeCount == 0:
                            timeSinceBlockWasDiscovered_seconds = "ExceptionOccurredWhenGetting_timeSinceBlockWasDiscovered"
                            try:
                                timeSinceBlockWasDiscovered_seconds = Libraries.core.GetTimeSinceBlockWasDiscovered(blockNumber)

                            except (KeyboardInterrupt, SystemExit):
                                print('\nkeyboard interrupt caught')
                                print('\n...Program Stopped Manually!')
                                raise

                            except:
                                PrintAndLogError("Exception when getting timeSinceBlockWasDiscovered: " + traceback.format_exc())

                            message = "canceling trade initiation because a new block was discovered before we could even ship our first trade " \
                                      "based on this tradingOpportunity. Did my API calls take a long time for some reason? " \
                                      "tradeOpportunities block number = " + str(blockNumber) + ", newly mined block number = " + \
                                      str(latestBlockNumber) + ", timeSinceBlockWasDiscovered_seconds = " + \
                                      str(round(timeSinceBlockWasDiscovered_seconds, 1)) + " seconds"
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   " + message)
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   Breaking from the trade loop because a new block mined in before we could even issue a trade")
                            break

                        else:
                            arbitrageOpportunityStillCurrentlyAvailable = IsArbitrageOpportunityStillCurrentlyAvailable(self)
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "arbitrageOpportunityStillCurrentlyAvailable = " + str(arbitrageOpportunityStillCurrentlyAvailable))

                            if arbitrageOpportunityStillCurrentlyAvailable:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   NOT canceling trade despite a newly mined block because the arbitrageOpportunity still exists.")
                            else:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   canceling trade because a new block was discovered and arbitrageOpportunity's trading path is no longer profitable. "
                                                     "arbitrageOpportunity blockNumber = " + str(blockNumber) + ", newly mined block number = " + str(latestBlockNumber))
                                doCancelTrade = True

                            # if not CancelTradesThatDontMineInWithinOneBlock:
                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                            #                          "   not canceling trade because CancelTradesThatDontMineInWithinOneBlock was set to False")
                            # else:
                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                            #                          "   canceling trade because a new block was discovered and this data is no longer valid to trade on. "
                            #                          "tradeOpportunities block number = " + str(blockNumber) + ", newly mined block number = " + str(latestBlockNumber))
                            #     doCancelTrade = True

                    if doCancelTrade:
                        try:
                            # Issue a cancel by sending 0 ether to myself with at least 10% higher gas price
                            gasPrice_toUse = int(gasPrice_toUse * 1.11)
                            PrintAndLog_Detailed(header_logging + " ShippingCancel-" + str(tradeCount) + "-" + str(blockNumber), functionsStartDateTime,
                                                 "   gasPrice_toUse for transaction cancel = " + str(gasPrice_toUse))
                            transactionId_cancel = API_CancelTransaction(ninjaAccount_toUse.publicAddress, ninjaAccount_toUse.DecryptPK(), gasPrice_toUse)
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   Issued a cancel transaction to cancel a trade attempt. transactionId_cancel = " + str(transactionId_cancel))
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   Breaking from the trade loop because we've canceled this trade")
                            break

                        except (KeyboardInterrupt, SystemExit):
                            print('\nkeyboard interrupt caught')
                            print('\n...Program Stopped Manually!')
                            raise

                        except:
                            PrintAndLogError("exception when processing a cancel signal, " + traceback.format_exc())
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   Breaking from the trade loop because we've had an exception when trying to cancel this trade")
                            break

                    if not Libraries.defaults.EnableMempoolStrategy_FrontRunning:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   EnableMempoolStrategy_FrontRunning was False, not considering incrementing gas via a FrontRunning strategy")
                    else:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   EnableMempoolStrategy_FrontRunning was True, considering incrementing gas via a FrontRunning strategy")
                        if gasPriceIsAllowedToIncreaseForFrontRunning:
                            villainsHighestGasPrice, villainAddress = self.DetermineVillainsHighestGasPriceInMemPool()
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   Analyzing gas prices for this trade")
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "      gasPrice_toUse = " + str(Libraries.core.ConvertWeiToGwei(gasPrice_toUse)) + " gwei")
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "      self.gasPrice_maxKeepAhead = " + str(Libraries.core.ConvertWeiToGwei(self.gasPrice_maxKeepAhead)) + " gwei")
                            if not villainsHighestGasPrice:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "      villainsHighestGasPrice = None")
                            else:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "      villainsHighestGasPrice = " + str(Libraries.core.ConvertWeiToGwei(villainsHighestGasPrice)) + " gwei")
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "      self.gasPrice_maxGrimTrigger = " + str(Libraries.core.ConvertWeiToGwei(self.gasPrice_maxGrimTrigger)) + " gwei")

                            # If we need to front run a villain
                            if villainsHighestGasPrice and villainsHighestGasPrice > gasPrice_toUse:
                                # Set a new gas price that will front run the villain
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "      We need to front run a villain")
                                # We must exceed their gas price in order to front run them
                                # Exceed by a slightly random number
                                frontRunGasPrice = int(villainsHighestGasPrice + randint(100000000, 1100000000))
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "      frontRunGasPrice = " + str(Libraries.core.ConvertWeiToGwei(frontRunGasPrice)) + " gwei")
                                # We have to increase our gas price by at least 10%
                                gasPrice_toUse = int(max(frontRunGasPrice, gasPrice_toUse * 1.11))
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "      Enforcing that gasPrice_toUse is at least 10% higher than our previous tx with the same nonce. "
                                                     "gasPrice_toUse = " + str(Libraries.core.ConvertWeiToGwei(gasPrice_toUse)) + " gwei")

                                # Enforce gasPrice_maxGrimTrigger
                                if gasPrice_toUse > self.gasPrice_maxGrimTrigger:
                                    message = "   Tried to front run but we exceeded our max grim trigger gas price allowed. gasPrice_toUse = " + str(
                                        Libraries.core.ConvertWeiToGwei(gasPrice_toUse)) + " gwei, gasPrice_maxGrimTrigger = " + str(
                                        Libraries.core.ConvertWeiToGwei(self.gasPrice_maxGrimTrigger)) + " gwei. Check the logs and determine what to do about this"
                                    Libraries.core.API_PostOperatorNotification(message)
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                         "   Breaking from the trade loop because we've exceeded our max grim trigger gas price. " + str(
                                                             self.GenerateDescriptionString()))

                                    # TODO, before we issue the cancel, call eth_estimateGas on the tx
                                    #  to make sure it's actually going to confirm and work so I'm not canceling on a fake tx
                                    # villainsEstimatedGas = Libraries.core.API_EstimateGas(toAddress, fromAddress, data, value, False)

                                    if tradeCount > 0:
                                        if not Libraries.defaults.CancelTradesThatGetGrimTriggeredByAVillain:
                                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                                 "Not canceling the trade because CancelTradesThatGetGrimTriggeredByAVillain was set")
                                        else:
                                            # Issue a cancel transaction here because we predict that our tx will lose since we were grim triggered by a villain
                                            # Try catch the cancel because we don't want an exception to prevent us from breaking this loop
                                            try:
                                                # Issue a cancel by sending 0 ether to myself with at least 10% higher gas price
                                                # We've already incremented our gasPrice_toUse
                                                PrintAndLog_Detailed(header_logging + " ShippingCancel-" + str(tradeCount) + "-" + str(blockNumber), functionsStartDateTime,
                                                                     "   gasPrice_toUse for transaction cancel = " + str(gasPrice_toUse))
                                                transactionId_cancel = API_CancelTransaction(ninjaAccount_toUse.publicAddress, ninjaAccount_toUse.DecryptPK(), gasPrice_toUse)
                                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                                     "   Issued a cancel transaction to cancel a trade attempt. transactionId_cancel = " + str(transactionId_cancel))

                                            except (KeyboardInterrupt, SystemExit):
                                                print('\nkeyboard interrupt caught')
                                                print('\n...Program Stopped Manually!')
                                                raise

                                            except:
                                                PrintAndLogError("exception when processing a cancel signal, " + traceback.format_exc())
                                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                                     "   Breaking from the trade loop because we've had an exception when trying to cancel this trade")

                                    break

                                # Trade with a higher gas price
                                doSubmitNewFrontRunningTransaction = True
                                tradeReason = TradeReason.FrontRun.value

                            else:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "      No need to front run, leave our gas price where it is")

                    if Libraries.defaults.VerboseLogging_PursueTradingOpportunity_TradeLoop:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   Before considering calling API_TradeSimpleRequirements: doSubmitNewFrontRunningTransaction = " + str(
                                                 doSubmitNewFrontRunningTransaction) + ", gasPrice_toUse = " + str(
                                                 Libraries.core.ConvertWeiToGwei(gasPrice_toUse)) + " gwei")

                    if not Libraries.defaults.EnableMempoolStrategy_KeepAhead:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   EnableMempoolStrategy_KeepAhead was False, not considering incrementing gas via a KeepAhead strategy")
                    else:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   EnableMempoolStrategy_KeepAhead was True, considering incrementing gas via a KeepAhead strategy")
                        if not doSubmitNewFrontRunningTransaction:
                            # If it's been a while since we've increased our gas price for this tradeOpportunity
                            duration_s_sinceLastTrade = (datetime.datetime.now() - dateTimeOf_lastTrade).total_seconds()
                            if Libraries.defaults.VerboseLogging_PursueTradingOpportunity_TradeLoop:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   duration_s_sinceLastTrade = " + str(
                                                         round(duration_s_sinceLastTrade, 1)) + " seconds. FrontRunningMaxDurationToWaitBetweenEachGasPriceIncrement_seconds = " + str(
                                                         Libraries.defaults.FrontRunningMaxDurationToWaitBetweenEachGasPriceIncrement_seconds) + " seconds")

                            # Consider incrementing the gas price despite me currently having the highest known gas price
                            doForceIncrementGasPrice = False

                            # If the current gasPrice_toUse is still below our MaxKeepAhead range (self.gasPrice_maxKeepAhead)
                            # and it's been long enough time since we last raised gas price
                            if duration_s_sinceLastTrade > frontRunning_RapidFireRaiseMyGasPriceEvery_X_seconds_UntilIReach_MaxKeepAhead and \
                                    gasPrice_toUse < self.gasPrice_maxKeepAhead and \
                                    gasPriceIsAllowedToIncreaseForFrontRunning:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   doForceIncrementGasPrice = True because we're in the MaxKeepAhead range and "
                                                     "it's been a while since we last traded "
                                                     "according to frontRunning_RapidFireRaiseMyGasPriceEvery_X_seconds_UntilIReach_MaxKeepAhead")
                                doForceIncrementGasPrice = True

                            # If it's been a while since our last gas price increase according to FrontRunningMaxDurationToWaitBetweenEachGasPriceIncrement_seconds
                            if duration_s_sinceLastTrade > Libraries.defaults.FrontRunningMaxDurationToWaitBetweenEachGasPriceIncrement_seconds and \
                                    gasPriceIsAllowedToIncreaseForFrontRunning:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   doForceIncrementGasPrice = True because it's been a while since we last traded "
                                                     "according to FrontRunningMaxDurationToWaitBetweenEachGasPriceIncrement_seconds")
                                doForceIncrementGasPrice = True

                            if doForceIncrementGasPrice and gasPriceIsAllowedToIncreaseForFrontRunning:
                                # Force doSubmitNewFrontRunningTransaction to True and increment our gas price since it's been a while since our last trade
                                # Minimum must be 1.1, so do a random number between 1.1 and say 1.3
                                randomGasPriceMultiplier = randint(1100, 1230) / 1000
                                gasPrice_toUse = int(gasPrice_toUse * randomGasPriceMultiplier)

                                # Enforce gasPrice_maxGrimTrigger
                                if gasPrice_toUse > self.gasPrice_maxGrimTrigger:
                                    message = "   Tried to increment gas price because it's been a while since we have, " \
                                              "but we exceeded our max grim trigger gas price allowed. gasPrice_toUse = " + \
                                              str(Libraries.core.ConvertWeiToGwei(gasPrice_toUse)) + " gwei, gasPrice_maxGrimTrigger = " + \
                                              str(Libraries.core.ConvertWeiToGwei(self.gasPrice_maxGrimTrigger)) + " gwei. Check the logs and determine what to do about this"
                                    Libraries.core.API_PostOperatorNotification(message)
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                         "   Breaking from the trade loop because we've exceeded our max grim trigger gas price. "
                                                         "THIS SHOULD NEVER HAPPEN, I SHOULD NEVER SEE THIS.  "
                                                         "The logic for addressing this is extremely complicated and it's way easier to just make sure that "
                                                         "there's a large enough gap between MaxKeepAhead and MaxGrimTrigger. " +
                                                         str(self.GenerateDescriptionString()))
                                    break

                                doSubmitNewFrontRunningTransaction = True
                                tradeReason = TradeReason.KeepAhead.value
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   force incrementing gas price for this trade since it's been a while since our last gas price increment. "
                                                     "This is a front running feature to fight off villains who are probably queueing a trade right now.")
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   doSubmitNewFrontRunningTransaction = " + str(doSubmitNewFrontRunningTransaction))
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   gasPrice_toUse upped to = " + str(Libraries.core.ConvertWeiToGwei(gasPrice_toUse)) + " gwei")

                    if doSubmitNewFrontRunningTransaction:
                        shippingTradeSubString = "ShippingTrade-" + str(tradeCount) + "-" + str(blockNumber)
                        message_tradeSubmittedSuccessfully = "Error when setting message_tradeSubmittedSuccessfully in PursueTradeOpportunity."
                        try:
                            symbol_quoteTokens = quoteTokenAddressToUse
                            symbolList_tradePath = []
                            try:
                                # TODO, this call to the cache is slow either optimize or just get rid off it for production
                                symbol_quoteTokens = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(quoteTokenAddressToUse)
                                for token in self.tradePath:
                                    symbolList_tradePath.append(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token).upper())

                            except (KeyboardInterrupt, SystemExit):
                                print('\nkeyboard interrupt caught')
                                print('\n...Program Stopped Manually!')
                                raise

                            except:
                                PrintAndLogError("exception when getting symbol for tokens, quoteTokenAddressToUse = " + str(
                                    quoteTokenAddressToUse) + ", self.tradePath = " + str(self.tradePath) + ", " + traceback.format_exc())

                            message_pendingTradeTx_tailgating = ""
                            if tailgatingTxHash:
                                message_pendingTradeTx_tailgating = "Tailgating tx " + str(tailgatingTxHash)

                            message_tradeSubmittedSuccessfully = "Ninja (" + str(tradingTechnique.value) + ") " + str(self.exchangeNamesList) + " " + str(
                                round(self.profit_quoteTokens, 5)) + " " + str(symbol_quoteTokens.upper()) + ", " + str(round(self.quoteTokenAmount, 5)) + " " + str(
                                symbol_quoteTokens.upper()) + " via path " + str(symbolList_tradePath) + ", gasPrice = " + str(
                                round(Libraries.core.ConvertWeiToGwei(gasPrice_toUse), 2)) + " gwei, nonce = " + str(nonce) + ", NinjaGeneric " + str(
                                self.GenerateDescriptionString()) + " " + str(shippingTradeSubString) + ", tradeReason = " + str(
                                tradeReason) + ", " + str(message_pendingTradeTx_tailgating) + ", TODO put metadata info here like setkey or 0x orderhash."

                        except (KeyboardInterrupt, SystemExit):
                            print('\nkeyboard interrupt caught')
                            print('\n...Program Stopped Manually!')
                            raise

                        except:
                            message = "exception when setting message_tradeSubmittedSuccessfully, " + traceback.format_exc()
                            Libraries.core.API_PostOperatorNotification(message)
                            PrintAndLogError(message)

                        # OPTIMIZATION
                        # We will require that the txMinePrediction must be successful only if we haven't already gotten a success
                        # Once we get a success on the txMinePrediction then we can safely assume we can trade this path without getting a revert
                        # from this point forward when we are front running we are not going to do more txMinePrediction checks
                        # because eth_estimateGas and eth_calls can be time consuming and front running must be as fast as possible
                        doRequireTxMinePredictionToBeSuccessful = True
                        if transactionId_initialThatPassedTxMinePrediction:
                            doRequireTxMinePredictionToBeSuccessful = False

                        # If True, after performing our tx mine prediction we need to call back to ArbitrageGeneric and await confirmation that we should trade
                        callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade = doRequireTxMinePredictionToBeSuccessful

                        functionArguments = \
                            ninjaAccount_toUse, quoteTokenAddressToUse, self.profit_quoteTokens, \
                            quoteTokensToSpendArray, self.DetermineMaxQuoteTokenTradeLossAcceptable_etherUnits(), \
                            self.GetBool_DoTradeWithinFlashLoan(), checkCallDataArrays_encoded, tradeCallDataArray_encoded, gasPrice_toUse, \
                            self.GetSuggestedGasLimit_AllTradeActions(), nonce, self.GenerateDescriptionString(), \
                            message_tradeSubmittedSuccessfully, self.GetTokensExcludingEthBasedTokensCommaSeparated(), \
                            doRequireTxMinePredictionToBeSuccessful, tradingTechnique, \
                            callBackAndWaitForResponseToArbitrageGenericBeforeExecutingTrade, \
                            readySetGoCommunicationDict, key_readySetGoCommunicationDict, immediatelyReturnAfterTxMinePrediction

                        if doRequireTxMinePredictionToBeSuccessful:
                            # Add useful info to the header_logging
                            PrintAndLog_Detailed(header_logging + " " + str(shippingTradeSubString), functionsStartDateTime,
                                                 "   Trade on this thread and wait for response so we can confirm that this "
                                                 "tx can possibly mine into a block without reverting")
                            # Trade on this thread and wait for response so we can confirm that this tx can possibly mine into a block without reverting
                            transactionId_initialThatPassedTxMinePrediction, txMinePredictionWasSuccessful = \
                                Exchanges.ninja.NinjaInstance_Generic.API_TradeSimpleRequirements(*functionArguments)
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   txMinePredictionWasSuccessful = " + str(
                                                     txMinePredictionWasSuccessful) + ", transactionId_initialThatPassedTxMinePrediction = " + str(
                                                     transactionId_initialThatPassedTxMinePrediction))

                            if immediatelyReturnAfterTxMinePrediction:
                                # Tell readySetGoCommunicationDict the result of the txMinePrediction
                                if txMinePredictionWasSuccessful:
                                    # Tell the readySetGoCommunicationDict we succeeded
                                    readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Set
                                else:
                                    # Tell the readySetGoCommunicationDict we failed
                                    readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

                                # Do not consider the results since we only want to know whether or not txMinePrediction succeeded or failed
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "Returning and not continuing the trade loop because we only wanted to know if the txMinePrediction would succeed")
                                # I should not need to do any cleaning up here
                                return

                            if transactionId_initialThatPassedTxMinePrediction:
                                tradeCount += 1
                                # The trade broadcast was successful
                                lastSuccessfullyBroadcasted_gasPrice_toUse = gasPrice_toUse
                                # Reset this to prep for next loop iteration
                                doSubmitNewFrontRunningTransaction = False
                                # Maintain dateTimeOf_lastTrade so we can see how long it's been since we last traded
                                dateTimeOf_lastTrade = datetime.datetime.now()

                            # If the txMinePrediction failed
                            if not txMinePredictionWasSuccessful:
                                # We want to allow it to fail a few times, i've totally seen eth_estimateGas fail several times and then succeed.
                                # So we can't fully rely on it, so let's allow some fails before we assume the tx is not going to revert
                                failCount_txMinePrediction += 1
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "txMinePredictionWasSuccessful was False, "
                                                     "we're predicting that this trade will fail so we are not trading. "
                                                     "failCount_txMinePrediction = " + str(
                                                         failCount_txMinePrediction) + ", maxFailLimit_txMinePrediction = " + str(
                                                         maxFailLimit_txMinePrediction) + ", " + str(self.GenerateDescriptionString()))

                                if failCount_txMinePrediction > maxFailLimit_txMinePrediction:
                                    message = "   Breaking from the trade loop because txMinePrediction has failed " + str(
                                        failCount_txMinePrediction) + " times and that's over our maxFailLimit_txMinePrediction"
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, message)
                                    Libraries.core.API_PostOperatorNotification("estimatedGas FAILED in API_TradeSimpleRequirements. " + str(message))
                                    break

                        else:
                            # Add useful info to the header_logging
                            PrintAndLog_Detailed(header_logging + " " + str(shippingTradeSubString), functionsStartDateTime,
                                                 "   Trade on a background thread because we don't want to wait for a response for performance optimization reasons")

                            if immediatelyReturnAfterTxMinePrediction:
                                message = "I should never see immediatelyReturnAfterTxMinePrediction = True here " \
                                          "because we're deep into the trade loop and txMinePrediction should have already been successful " \
                                          "and we should now be trading with a real ninjaAccount_toUse"
                                Libraries.core.API_PostOperatorNotification(message)
                                raise Exception(message)

                            # Trade on a background thread because we don't want to wait for a response for performance optimization reasons
                            t = threading.Thread(
                                name=threading.currentThread().getName(),  # Keep the thread name the same to help with logging consistency
                                target=Exchanges.ninja.NinjaInstance_Generic.API_TradeSimpleRequirements,
                                args=functionArguments)
                            t.start()

                            tradeCount += 1
                            # Assume the trade broadcast was successful, even though we are not waiting for the response
                            lastSuccessfullyBroadcasted_gasPrice_toUse = gasPrice_toUse
                            # Reset this to prep for next loop iteration
                            doSubmitNewFrontRunningTransaction = False
                            # Maintain dateTimeOf_lastTrade so we can see how long it's been since we last traded
                            dateTimeOf_lastTrade = datetime.datetime.now()

                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   lastSuccessfullyBroadcasted_gasPrice_toUse = " + str(lastSuccessfullyBroadcasted_gasPrice_toUse))

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                # TODO REMOVEME
                # except Libraries.exceptions.ArbitrageOpportunityAbandoned:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "   Gracefully handling the case where this arbitrageOpportunity has been told to exit, "
                #                          "likely due to a new block being mined in.")
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Returning")
                #     if immediatelyReturnAfterTxMinePrediction:
                #         # Tell the readySetGoCommunicationDict we failed
                #         readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail
                #     else:
                #         # Clean up before we return
                #         # Remove the path since this loop is no longer trading this path
                #         self.RemoveMyPath_PursueTradeOpportunityPathsDict()
                #         if reservableEthereumAccountPool:
                #             # Release the account since the trade loop has exited
                #             reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)
                #         # Putting a return here so I can find it when searching for return occurrences in this function
                #
                #     return

                except:
                    PrintAndLogError("exception in PursueTradeOpportunity's trade loop, " + traceback.format_exc())
                    if immediatelyReturnAfterTxMinePrediction:
                        # Tell the readySetGoCommunicationDict we failed
                        readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

                # Exit the loop if it's been running for too long
                duration_s_tradeLoop = (datetime.datetime.now() - dateTimeOf_tradeLoopBegin).total_seconds()
                # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   duration_s_tradeLoop = " + str(duration_s_tradeLoop))
                if duration_s_tradeLoop > timeout_s:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   Breaking from the trade loop because we've timedout")
                    break

                # Sleep a bit while we wait for something to happen
                time.sleep(0.02)

                index += 1

            # We have broken from this loop
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Exited trade loop for path " + self.GenerateDescriptionString())

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception in PursueTradeOpportunity, " + traceback.format_exc())
            if immediatelyReturnAfterTxMinePrediction:
                # Tell the readySetGoCommunicationDict we failed
                readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

        if immediatelyReturnAfterTxMinePrediction:
            # Tell the readySetGoCommunicationDict we failed
            readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail
        else:
            # Clean up before we return
            # Remove the path since this loop is no longer trading this path
            self.RemoveMyPath_PursueTradeOpportunityPathsDict()
            if reservableEthereumAccountPool:
                # Release the account since the trade loop has exited
                reservableEthereumAccountPool.ReleaseAccountBackToPool(ninjaAccount_toUse, accountWasInUsePriorToReserving)
            # Putting a return here so I can find it when searching for return occurrences in this function

        return


def NinjaGeneric_Config_A(blockNumber):
    configName = "NinjaGeneric_Config_A"
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    # Exclude exchanges that are inefficient at making calls to nodes
    exchangeNameExclusions = GetListOfInEfficientExchangesNames_ToGetMassPricesForManyTradeQuantities()
    exchangeNamesRequiredToTrade = []
    narrowDownDataSetBeforeMakingMassCall = False
    dramaticallyIncreaseNumOfDataValues = True

    NinjaGeneric(blockNumber, configName, gasPrice, exchangeNameExclusions, exchangeNamesRequiredToTrade,
                 narrowDownDataSetBeforeMakingMassCall, dramaticallyIncreaseNumOfDataValues)


def NinjaGeneric_Config_B(blockNumber):
    configName = "NinjaGeneric_Config_B"
    gasPrice = Libraries.gasStation.GasPriceCustomType.DynamicGasPrice

    # Exclude exchanges that are inefficient at making calls to nodes
    exchangeNameExclusions = GetListOfInEfficientExchangesNames_ToGetMassPricesForManyTradeQuantities()
    exchangeNamesRequiredToTrade = []
    narrowDownDataSetBeforeMakingMassCall = False
    dramaticallyIncreaseNumOfDataValues = True

    NinjaGeneric(blockNumber, configName, gasPrice, exchangeNameExclusions, exchangeNamesRequiredToTrade,
                 narrowDownDataSetBeforeMakingMassCall, dramaticallyIncreaseNumOfDataValues)


def NinjaGeneric_Config_C(blockNumber):
    configName = "NinjaGeneric_Config_C"
    gasPrice = Libraries.gasStation.GasPriceCustomType.DynamicGasPrice

    # Focus on the exchange names that are inefficient at making calls to nodes
    # assume that another NinjaGeneric has been called which focuses on the efficient ones
    # So this one will only trade on the inefficient ones (but can trade between inefficient-efficient paths), just not across efficient-efficient paths
    exchangeNameExclusions = []
    exchangeNamesRequiredToTrade = GetListOfInEfficientExchangesNames_ToGetMassPricesForManyTradeQuantities()
    # NOTE The narrow down doesn't seem to help balancer. Balancer's API call is short but it's parsing of the data is horribly inefficient
    narrowDownDataSetBeforeMakingMassCall = False
    dramaticallyIncreaseNumOfDataValues = False

    # Sleep a bit before starting this call so it doesn't slow down the more efficient NinjaGeneric calls
    time.sleep(0.02)

    NinjaGeneric(blockNumber, configName, gasPrice, exchangeNameExclusions, exchangeNamesRequiredToTrade,
                 narrowDownDataSetBeforeMakingMassCall, dramaticallyIncreaseNumOfDataValues)


def NinjaGeneric_Config_Hide(blockNumber):
    configName = "NinjaGeneric_Config_Hide"
    # gasPrice = Libraries.gasStation.GetGasPrice_SafeLow()
    # gasPrice = Libraries.gasStation.GetGasPrice_Cheap()
    gasPrice = Libraries.gasStation.GetGasPrice_Fast()

    # Exclude exchanges that are inefficient at making calls to nodes
    exchangeNameExclusions = GetListOfInEfficientExchangesNames_ToGetMassPricesForManyTradeQuantities()
    exchangeNamesRequiredToTrade = []
    narrowDownDataSetBeforeMakingMassCall = False
    dramaticallyIncreaseNumOfDataValues = True

    NinjaGeneric(blockNumber, configName, gasPrice, exchangeNameExclusions, exchangeNamesRequiredToTrade,
                 narrowDownDataSetBeforeMakingMassCall, dramaticallyIncreaseNumOfDataValues)


def NinjaGeneric(blockNumber, configName, gasPrice, exchangeNameExclusions, exchangeNamesRequiredToTrade,
                 narrowDownDataSetBeforeMakingMassCall, dramaticallyIncreaseNumOfDataValues):
    try:
        Libraries.timeRecorder.BeginRecording(configName)

        header_logging = Libraries.loggingConfig.GenerateHeaderForPrintAndLog(configName)
        functionsStartDateTime = datetime.datetime.now()

        # Initialize RatesGraphs for this blockNumber
        Libraries.ratesGraph.InitializeRatesGraphForBlock(blockNumber)

        quoteTokenList = Exchanges.keeperDAO.GetBorrowableAssetsList()
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokenList = " + str(quoteTokenList))

        quoteTokenExchangeNamesDict = Exchanges.keeperDAO.GetEligibleExchangeNamesPerQuoteTokenDict()
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokenExchangeNamesDict = " + str(quoteTokenExchangeNamesDict))

        pricesDict = None
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "narrowDownDataSetBeforeMakingMassCall = " + str(narrowDownDataSetBeforeMakingMassCall))
        if not narrowDownDataSetBeforeMakingMassCall:
            # When not narrowDownDataSetBeforeMakingMassCall, I can call GetAllPrices just one single time for all quoteTokens in quoteTokenList

            # Call GetAllPrices with only the exchanges and quoteTokens that have potential for trading opportunities
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Calling GetAllPrices on efficient exchanges only")
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "JOEYZDEBUG123 Calling GetAllPrices with blockNumber = " + str(blockNumber))
            pricesDict, pricesDictAnalysis_string = GetAllPrices(
                quoteTokenList, quoteTokenExchangeNamesDict, None,
                blockNumber, True, exchangeNameExclusions, dramaticallyIncreaseNumOfDataValues)
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Done calling GetAllPrices on efficient exchanges only")

            dataToLog = 'Turn on VerboseLogging to see'
            if Libraries.defaults.VerboseLogging_PricesDict:
                # This can take several hundred ms to log, so don't log it unless we need it
                dataToLog = pricesDict

            prependPrintString = "pricesDict (efficient exchanges only) = "
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, prependPrintString + str(dataToLog))
            if Libraries.defaults.VerboseLogging_PricesDictToFile:
                LogPricesDict(header_logging, pricesDict)

            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pricesDictAnalysis_string = " + str(pricesDictAnalysis_string))

        else:
            raise Exception("This feature is no longer supported since the multi process refactor. sI will need to upgrade this feature to support this")

            # # When narrowDownDataSetBeforeMakingMassCall, it's more optimal to call GetAllPrices twice.
            # # first call GetAllPrices with a detailed = False,
            # # then once I have an idea where arbitrage is, I can call GetAllPrices again with detailed = True and with a subset of quoteTokens and baseTokens
            #
            # # Call GetAllPrices with all exchanges and all quoteTokens
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Calling GetAllPrices on inefficient exchanges, so I need to call GetAllPrices twice")
            # inefficient_pricesDict, inefficient_pricesDictAnalysis_string = GetAllPrices(
            #     quoteTokenList, quoteTokenExchangeNamesDict, None,
            #     blockNumber, False, exchangeNameExclusions, dramaticallyIncreaseNumOfDataValues)
            #
            # dataToLog = 'Turn on VerboseLogging to see'
            # if VerboseLogging_PricesDict:
            #     # This can take several hundred ms to log, so don't log it unless we need it
            #     dataToLog = inefficient_pricesDict
            #
            # prependPrintString = "inefficient_pricesDict (all exchanges) "
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, prependPrintString + str(dataToLog))
            # if VerboseLogging_PricesDictToFile:
            #     LogPricesDict(header_logging, inefficient_pricesDict)
            #
            # arbitrageOpportunitiesDict, arbitrageOpportunitiesDictAnalysis_string = FindArbitrageOpportunities(quoteTokenList, inefficient_pricesDict, blockNumber)
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "arbitrageOpportunitiesDict.GetKeys() = " + str(arbitrageOpportunitiesDict.GetKeys()))
            #
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "inefficient_pricesDictAnalysis_string = " + str(inefficient_pricesDictAnalysis_string))
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "arbitrageOpportunitiesDictAnalysis_string = " + str(arbitrageOpportunitiesDictAnalysis_string))
            #
            # # Get detailed price information for only a subset of quoteTokens and exchanges based on arbitrageOpportunitiesDict
            # # We only want to call GetAllPrices with detailed==True for arbitragable assets since it's such a massive time consuming call
            # subset_quoteTokenList = list(arbitrageOpportunitiesDict.GetKeys())
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "subset_quoteTokenList = " + str(subset_quoteTokenList))
            # subset_quoteTokenExchangeNamesDict = {}
            # subset_quoteTokenBaseTokensDict = {}
            # # Get the information we need from arbitrageOpportunitiesDict
            # for quoteToken in arbitrageOpportunitiesDict.GetKeys():
            #     # Init the list
            #     subset_quoteTokenExchangeNamesDict[quoteToken] = []
            #     subset_quoteTokenBaseTokensDict[quoteToken] = []
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "iterating over quoteToken " + str(quoteToken))
            #     for arbitrageOpportunity in arbitrageOpportunitiesDict.GetItems(quoteToken):
            #         # TODO, this is flawed logic since adding my new arbitrage logic with the graph
            #         #  Because the graph isn't analyzed for arbitrage until later in the ArbitrageGeneric function call
            #         #  So I don't want to exclude any data without knowing where 3-leg and 4-leg trades might be going
            #         #  TODO FIXME, i should not be accessing index 0 and 1 of exchangeNamesList here
            #         raise Exception("I've refactored to support multi leg trades so this is now broken until I refactor this."
            #                         "I'm putting this refactor off since I'm not even using Kyber right now")
            #         exchangeName0 = arbitrageOpportunity.exchangeNamesList[0]
            #         exchangeName1 = arbitrageOpportunity.exchangeNamesList[1]
            #         # baseToken = arbitrageOpportunity.baseToken
            #
            #         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "iterating over arbitrageOpportunity.exchangeNamesList " + str(
            #             arbitrageOpportunity.exchangeNamesList) + ", exchangeName0 = " + str(exchangeName0) + ", exchangeName1 = " + str(exchangeName1))
            #
            #         if exchangeName0 not in subset_quoteTokenExchangeNamesDict[quoteToken]:
            #             subset_quoteTokenExchangeNamesDict[quoteToken].append(exchangeName0)
            #
            #         if exchangeName1 not in subset_quoteTokenExchangeNamesDict[quoteToken]:
            #             subset_quoteTokenExchangeNamesDict[quoteToken].append(exchangeName1)
            #
            #         # if baseToken not in subset_quoteTokenBaseTokensDict[quoteToken]:
            #         #     subset_quoteTokenBaseTokensDict[quoteToken].append(baseToken)
            #
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "subset_quoteTokenExchangeNamesDict = " + str(subset_quoteTokenExchangeNamesDict))
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "subset_quoteTokenBaseTokensDict = " + str(subset_quoteTokenBaseTokensDict))
            #
            # # Call GetAllPrices with only the exchanges and quoteTokens that have potential for trading opportunities
            # pricesDict, pricesDictAnalysis_string = GetAllPrices(
            #     subset_quoteTokenList, subset_quoteTokenExchangeNamesDict, subset_quoteTokenBaseTokensDict,
            #     blockNumber, True, exchangeNameExclusions, dramaticallyIncreaseNumOfDataValues)
            #
            # dataToLog = 'Turn on VerboseLogging to see'
            # if VerboseLogging_PricesDict:
            #     # This can take several hundred ms to log, so don't log it unless we need it
            #     dataToLog = pricesDict
            #
            # prependPrintString = "pricesDict (all exchanges) "
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, prependPrintString + str(dataToLog))
            # if VerboseLogging_PricesDictToFile:
            #     LogPricesDict(header_logging, pricesDict)
            #
            # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pricesDictAnalysis_string = " + str(pricesDictAnalysis_string))

        if pdk_quoteTokens not in pricesDict:
            if Libraries.defaults.VerboseLogging_ArbitrageGeneric_ArbitrageNo:
                # Zero trading opportunities
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Arbitrage - No. Zero quote tokens found")
        else:
            # Store pricesDict in the PricesDictDict regardless of what ScriptName this is
            Exchanges.ninja.UpdatePricesDict(pricesDict, blockNumber)

            if Libraries.utils.ActiveScriptName != Libraries.utils.ScriptName.NinjaArb:
                # We aren't in NinjaArb mode, so do not arbitrage
                pass

            elif not Libraries.defaults.EnableNinja_Arbitrage:
                message = "EnableNinja_Arbitrage is disabled, not calling ArbitrageGeneric inside NinjaGeneric"
                if IsTimeToExecute(message, 600):
                    Libraries.core.API_PostOperatorNotification(message)

            else:
                # Trading opportunities are available
                reservableEthereumAccountPool = CreateNewReservableEthereumAccountPoolList(
                    configName, Libraries.ninjaOps.NinjaOpConfig_NinjaGeneric.ninjaOpList)
                ArbitrageGeneric(header_logging, functionsStartDateTime, pricesDict, gasPrice,
                                 reservableEthereumAccountPool, blockNumber, exchangeNamesRequiredToTrade, True, TradingTechnique.Arbitrage)

        Libraries.timeRecorder.EndRecording(configName)

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        message = "exception in NinjaGeneric: " + str(traceback.format_exc())
        PrintAndLogError(message)
        raise


def GetAllPrices(quoteTokenList, quoteTokenExchangeNamesDict, quoteTokenBaseTokensDict,
                 blockNumber, detailed, exchangeNameExclusions, dramaticallyIncreaseNumOfDataValues):
    header_logging = "GetAllPrices"
    functionsStartDateTime = datetime.datetime.now()

    # Should I use unit test data?
    if Libraries.defaults.UseUnitTestData_NinjaGeneric_Quick and not detailed:
        return Libraries.utils.ReadJsonFromFile('UnitTests/PriceDict/testPriceDict_ArbyBetweenUniswapAndKyber_1.json')
    elif Libraries.defaults.UseUnitTestData_NinjaGeneric_Detailed and detailed:
        return Libraries.utils.ReadJsonFromFile('UnitTests/PriceDict/testPriceDict_ArbyBetweenUniswapAndKyber_2.json')

    # For each quoteToken, get bid/ask prices

    if Libraries.defaults.VerboseLogging_GetAllPrices:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokenExchangeNamesDict = " + str(
            quoteTokenExchangeNamesDict) + " with blockNumber " + str(blockNumber))

    pricesDict = {}
    pricesDict[pdk_quoteTokens] = {}
    # lock_pricesDict = Lock()
    # Keep track of expected results so we can compare our actual results and see which calls are failing
    # TODO Rename expectedResultsDict this name is terrible!
    expectedResultsDict = {}
    resultIdDict = {}
    graphResultDict = {}

    # Iterate over all tokens and get all prices
    for quoteToken in quoteTokenList:
        # Maintain expectedResultsDict
        expectedResultsDict[quoteToken] = {}

        # Create a dict for the results
        pricesDict[pdk_quoteTokens][quoteToken] = {}

        # TODO REMOVEME, old code before multi process refactor
        # pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges], expectedResultsDict[quoteToken] = GetAllPrices_ForQuoteToken(
        #     quoteToken, blockNumber, detailed, quoteTokenExchangeNamesDict, exchangeNameExclusions, quoteTokenBaseTokensDict)

        # Signal the processes to get price data, then i'll have to wait for the results
        # Send the process a resultId we can use to check for a result from the other processes
        # include unique things in the id + a random number just to be careful
        resultId = str(quoteToken) + "_" + str(blockNumber) + "_" + str(randint(0, 99999999999999))
        resultIdDict[quoteToken] = resultId
        Libraries.quoteTokenProcesses.SignalQuoteTokenProcess_GetAllPrices(
            resultId, quoteToken, blockNumber, detailed, quoteTokenExchangeNamesDict, exchangeNameExclusions, quoteTokenBaseTokensDict)

    if Libraries.defaults.VerboseLogging_GetAllPrices:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "resultIdDict = " + str(resultIdDict))

    # Check if a new block has already been mined in
    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    if latestBlockNumber > blockNumber:
        pricesDictAnalysis_string = "blockNumber " + str(blockNumber) + " is now old, a new block " + \
                                    str(latestBlockNumber) + " was discovered. I've stopped processing this old block"
        return {}, pricesDictAnalysis_string

    successfulResultsDict = {}
    before = datetime.datetime.now()
    elapsedTime_s = 0
    # TODO put this timeout in defaults??
    timeout_s = 10
    sleepTime_s = 0.01
    while len(successfulResultsDict) < len(quoteTokenList) and elapsedTime_s < timeout_s:
        elapsedTime_s = (datetime.datetime.now() - before).total_seconds()
        time.sleep(sleepTime_s)

        # Iterate over all quoteTokens
        for quoteToken in quoteTokenList:
            # Skip quoteTokens whose results we've already found
            if quoteToken in successfulResultsDict:
                continue

            # Check to see if results are ready from the quoteToken's process
            # results = Libraries.quoteTokenProcesses.GetAllPricesResults(resultIdDict[quoteToken])
            results = Libraries.processCommunication.GetAllPricesResults(resultIdDict[quoteToken])
            if results:
                # PrintAndLog_FuncNameHeader("results for resultId " + str(resultIdDict[quoteToken]) + " = " + str(results))
                # Mark it as successful
                successfulResultsDict[quoteToken] = True
                if Libraries.defaults.VerboseLogging_GetAllPrices:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Found GetAllPricesResults for resultId " + str(
                        resultIdDict[quoteToken]) + ", len(successfulResultsDict) = " + str(
                        len(successfulResultsDict)) + ", len(quoteTokenList) = " + str(len(quoteTokenList)))

                # Set the results in corresponding dicts
                # TODO disabling this while I debug the memory leak
                pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges] = results['exchangesResultsDict']
                expectedResultsDict[quoteToken] = results['expectedResultsDict']
                graphResultDict[quoteToken] = \
                    results['Libraries.ratesGraph.RatesGraph_Dict__Process_QuoteToken'], \
                    results['Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames__Process_QuoteToken'], \
                    results['Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens__Process_QuoteToken']

    resultLoopString = "GetAllPrices result loop for quoteTokenProcesses, "
    if elapsedTime_s >= timeout_s:
        message = resultLoopString + "took a long time, " + str(elapsedTime_s) + " seconds. Why is performance so bad??"
        Libraries.core.API_PostOperatorNotification(message)
        raise Exception(message)
    else:
        if Libraries.defaults.VerboseLogging_GetAllPrices:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 resultLoopString + "successfully got data within a reasonable period of time! elapsedTime_s = " + str(elapsedTime_s))

    # # If we've made it this far, all quoteTokenProcesses have returned valid graphs
    # # here is where I need to merge all these graphs into one single graph
    # # These graphs were separated because they were created on separate processes for optimization purposes

    # for quoteToken in graphResultDict:
    #     results = graphResultDict[quoteToken]
    #     ratesGraph_Dict, ratesGraph_Dict_ExchangeNames, ratesGraph_Dict_QuoteTokens = results
    #     if VerboseLogging_GetAllPrices:
    #         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict for quoteToken " + str(quoteToken) + " = " + str(ratesGraph_Dict))
    #         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict_ExchangeNames for quoteToken " + str(quoteToken) + " = " + str(ratesGraph_Dict_ExchangeNames))
    #         PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict_QuoteTokens for quoteToken " + str(quoteToken) + " = " + str(ratesGraph_Dict_QuoteTokens))

    before = datetime.datetime.now()
    merged_ratesGraph_Dict = {}
    merged_ratesGraph_Dict_ExchangeNames = {}
    merged_ratesGraph_Dict_QuoteTokens = {}
    # Iterate over all index_strings
    # if VerboseLogging_GetAllPrices:
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "graphResultDict = " + str(graphResultDict))
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "graphResultDict.keys() = " + str(graphResultDict.keys()))
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "list(graphResultDict.keys()) = " + str(list(graphResultDict.keys())))
    firstQuoteToken = list(graphResultDict.keys())[0]
    indexOf_ratesGraph_Dict_inResults = 0
    # Only iterate over the ratesGraph_Dict within the results
    for index_string in graphResultDict[firstQuoteToken][indexOf_ratesGraph_Dict_inResults]:
        # if VerboseLogging_GetAllPrices:
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Iterating over index_string = " + str(index_string))

        for quoteToken in graphResultDict:
            results = graphResultDict[quoteToken]
            ratesGraph_Dict, ratesGraph_Dict_ExchangeNames, ratesGraph_Dict_QuoteTokens = results
            # if VerboseLogging_GetAllPrices:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
            #                          "ratesGraph_Dict for quoteToken " + str(quoteToken) + " = " + str(ratesGraph_Dict))
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
            #                          "ratesGraph_Dict_ExchangeNames for quoteToken " + str(quoteToken) + " = " + str(ratesGraph_Dict_ExchangeNames))
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
            #                          "ratesGraph_Dict_QuoteTokens for quoteToken " + str(quoteToken) + " = " + str(ratesGraph_Dict_QuoteTokens))

            # If we haven't yet set anything for this index_string
            if index_string not in merged_ratesGraph_Dict:
                merged_ratesGraph_Dict[index_string] = ratesGraph_Dict[index_string]
                merged_ratesGraph_Dict_ExchangeNames[index_string] = ratesGraph_Dict_ExchangeNames[index_string]
                merged_ratesGraph_Dict_QuoteTokens[index_string] = ratesGraph_Dict_QuoteTokens[index_string]
                # Continue because we're just going to initialize the merged graphs with this quoteToken's graphs
                # if VerboseLogging_GetAllPrices:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "Setting merged_ratesGraph_Dict for index_string " + str(index_string) + " based on ratesGraph_Dict[index_string]")
                continue

            # iterate over all the items in the graph
            length = len(Libraries.ratesGraph.RatesGraphTokenList)
            for r in range(0, length):
                for c in range(0, length):
                    # If the merged is not the same as the one we're iterating over
                    # if VerboseLogging_GetAllPrices:
                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                    #                          "Comparing: r = " + str(r) + ", c = " + str(c))
                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                    #                          "   merged_ratesGraph_Dict[index_string][r][c] = " + str(merged_ratesGraph_Dict[index_string][r][c]))
                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                    #                          "   with ratesGraph_Dict[index_string][r][c] = " + str(ratesGraph_Dict[index_string][r][c]))

                    # Make sure the merged graph has the best rate
                    if ratesGraph_Dict[index_string][r][c] > merged_ratesGraph_Dict[index_string][r][c]:
                        # if VerboseLogging_GetAllPrices:
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Updating the merged value")
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   r = " + str(r) + ", c = " + str(c) + ", index_string = " + str(index_string) + ", blockNumber = " + str(
                        #         blockNumber) + ", quoteToken = " + str(quoteToken))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   r is " + str(Libraries.ratesGraph.RatesGraphTokenList[r]))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   c is " + str(Libraries.ratesGraph.RatesGraphTokenList[c]))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   merged_ratesGraph_Dict[index_string][r][c] = " + str(merged_ratesGraph_Dict[index_string][r][c]))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   merged_ratesGraph_Dict_ExchangeNames[index_string][r][c] = " + str(merged_ratesGraph_Dict_ExchangeNames[index_string][r][c]))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   merged_ratesGraph_Dict_QuoteTokens[index_string][r][c] = " + str(merged_ratesGraph_Dict_QuoteTokens[index_string][r][c]))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   ratesGraph_Dict[index_string][r][c] = " + str(ratesGraph_Dict[index_string][r][c]))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   ratesGraph_Dict_ExchangeNames[index_string][r][c] = " + str(ratesGraph_Dict_ExchangeNames[index_string][r][c]))
                        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                        #                          "   ratesGraph_Dict_QuoteTokens[index_string][r][c] = " + str(ratesGraph_Dict_QuoteTokens[index_string][r][c]))
                        merged_ratesGraph_Dict[index_string][r][c] = ratesGraph_Dict[index_string][r][c]
                        merged_ratesGraph_Dict_ExchangeNames[index_string][r][c] = ratesGraph_Dict_ExchangeNames[index_string][r][c]
                        merged_ratesGraph_Dict_QuoteTokens[index_string][r][c] = ratesGraph_Dict_QuoteTokens[index_string][r][c]

        # if VerboseLogging_GetAllPrices:
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
        #                          "merged_ratesGraph_Dict[" + str(index_string) + "] = " + str(merged_ratesGraph_Dict[index_string]))
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
        #                          "merged_ratesGraph_Dict_ExchangeNames[" + str(index_string) + "] = " + str(merged_ratesGraph_Dict_ExchangeNames[index_string]))
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
        #                          "merged_ratesGraph_Dict_QuoteTokens[" + str(index_string) + "] = " + str(merged_ratesGraph_Dict_QuoteTokens[index_string]))

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Merging sub process ratesGraphs for blockNumber " + str(blockNumber) + " took " + str(round(duration_s, 1)) + " seconds")

    # Update the main process's rates graphs based on the merged graphs from all the processes
    Libraries.ratesGraph.RatesGraph_Dict[blockNumber] = merged_ratesGraph_Dict
    Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames[blockNumber] = merged_ratesGraph_Dict_ExchangeNames
    Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens[blockNumber] = merged_ratesGraph_Dict_QuoteTokens
    # if VerboseLogging_GetAllPrices:
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
    #                          "Libraries.ratesGraph.RatesGraph_Dict[blockNumber] = " + str(Libraries.ratesGraph.RatesGraph_Dict[blockNumber]))
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
    #                          "Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames[blockNumber] = " + str(Libraries.ratesGraph.RatesGraph_Dict_ExchangeNames[blockNumber]))
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
    #                          "Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens[blockNumber] = " + str(Libraries.ratesGraph.RatesGraph_Dict_QuoteTokens[blockNumber]))

    # Use this to freeze things here when debugging
    # while True:
    #     # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pricesDict = " + str(pricesDict))
    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Sleeping while I debug to see wtf is happening")
    #     time.sleep(10)

    pricesDictAnalysis_string = None
    try:
        # if VerboseLogging_GetAllPrices:
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Update expectedResultsDict based on results")
        # Update expectedResultsDict based on results
        # askPrice = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][str(index_string)][pdk_ask]
        # bidPrice = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][str(index_string)][pdk_bid]
        if pdk_quoteTokens not in pricesDict:
            # if VerboseLogging_GetAllPrices:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "No quoteTokens in pricesDict. Skipping the expectedResultsDict")
            pass
        else:
            for quoteToken in pricesDict[pdk_quoteTokens]:
                for exchangeName in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges]:
                    # if VerboseLogging_GetAllPrices:
                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Iterating over " + str(
                    #         len(pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens])) + " baseTokens on block number = " + str(blockNumber))
                    for baseToken in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens]:
                        # Let's just assume that if the first index is valid then we got valid data
                        price_ask_isValid = False
                        price_bid_isValid = False
                        forLoopLength = int(len(pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices]))
                        for index in range(0, forLoopLength):
                            index_string = str(index)
                            if index_string in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices]:
                                price_ask = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][pdk_ask]
                                price_bid = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][pdk_bid]
                                # if VerboseLogging_GetAllPrices:
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "price_ask = " + str(price_ask))
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "price_bid = " + str(price_bid))

                                # Verify that they are valid
                                if IsValidPrice(price_ask):
                                    price_ask_isValid = True
                                if IsValidPrice(price_bid):
                                    price_bid_isValid = True

                                # if VerboseLogging_GetAllPrices:
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "price_ask = " + str(price_ask) + ", price_ask_isValid = " + str(price_ask_isValid))
                                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "price_bid = " + str(price_bid) + ", price_bid_isValid = " + str(price_bid_isValid))

                        # Require that we have a valid price
                        if price_bid_isValid or price_ask_isValid:
                            expectedResultsDict[quoteToken][exchangeName][baseToken] = True

            # if VerboseLogging_GetAllPrices:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "expectedResultsDict = " + str(expectedResultsDict))

            # Calculate some simple stats that will be helpful when debugging
            # expectedResultsDict[quoteToken][exchangeName][baseToken] = False
            pricesDictAnalysis_string = 'Results from expectedResultsDict\n'
            for quoteToken in expectedResultsDict:
                sum_quoteToken = 0
                len_quoteToken = len(expectedResultsDict[quoteToken])
                for exchangeName in expectedResultsDict[quoteToken]:
                    sum_exchange = 0
                    len_exchange = len(expectedResultsDict[quoteToken][exchangeName])
                    for baseToken in expectedResultsDict[quoteToken][exchangeName]:
                        isValid = bool(expectedResultsDict[quoteToken][exchangeName][baseToken])
                        if isValid:
                            # if VerboseLogging_GetAllPrices:

                            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                            #                          "Found price isValid = " + str(isValid) + " for " + str(exchangeName) + ", quoteToken = " +
                            #                          str(quoteToken) + ", baseToken = " + str(baseToken))
                            sum_exchange += 1

                    avg_exchange_percentage = sum_exchange / float(len_exchange) * 100
                    pricesDictAnalysis_string += "   Results: success rate = " + str(round(avg_exchange_percentage, 1)) + "%, " + str(
                        sum_exchange) + " out of " + str(len_exchange) + " for quoteToken " + str(quoteToken) + ", " + str(exchangeName) + '\n'
                    sum_quoteToken += 1

                avg_quoteToken_percentage = sum_quoteToken / float(len_quoteToken) * 100
                pricesDictAnalysis_string += "   Results: success rate = " + str(round(avg_quoteToken_percentage, 1)) + "%, " + str(
                    sum_quoteToken) + " out of " + str(len_quoteToken) + " for quoteToken " + str(quoteToken) + '\n'

            if Libraries.defaults.VerboseLogging_GetAllPrices:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "pricesDictAnalysis_string = " + str(pricesDictAnalysis_string))

    except (KeyboardInterrupt, SystemExit):
        print('\nkeyboard interrupt caught')
        print('\n...Program Stopped Manually!')
        raise

    except:
        PrintAndLogError("exception in GetAllPrices when updating expectedResultsDict based on results, " + traceback.format_exc())

    return pricesDict, pricesDictAnalysis_string


def GetAllPrices_ForQuoteToken(quoteToken, blockNumber, detailed, quoteTokenExchangeNamesDict, exchangeNameExclusions, quoteTokenBaseTokensDict):
    from Libraries.exchanges import ExchangeDict, GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken

    header_logging = "GetAllPrices_ForQuoteToken: pid " + str(os.getpid()) + ", blockNumber " + str(blockNumber) + ", quoteToken " + str(quoteToken)
    functionsStartDateTime = datetime.datetime.now()

    expectedResultsDict = {}
    exchangesResultsDict = {}

    if not detailed:
        # Only get prices for a small amount of quote tokens
        quoteTokensToSpendList_etherUnits = [Exchanges.keeperDAO.GetSmallestPriceCheckQuoteTokenAmount(quoteToken)]
    else:
        quoteTokensToSpendList_etherUnits = Exchanges.keeperDAO.GetQuoteTokensToSpendList_etherUnits(quoteToken)

    # This part is critical
    # Verify the validity of quoteTokensToSpendList_etherUnits here
    # If this fails we should not get prices because the index_strings could be wrong leading to mis-aligned data in the pricesDict and ratesGraph
    if not Exchanges.keeperDAO.VerifyValidityOfQuoteTokensToSpendList(quoteTokensToSpendList_etherUnits):
        message = "quoteTokensToSpendList_etherUnits failed validity verification. " \
                  "This should NOT happen randomly and is only okay to happen within the first few minutes of launching Ninja. " \
                  "blockNumber = " + str(blockNumber) + ", quoteToken = " + str(quoteToken)
        Libraries.core.API_PostOperatorNotification(message)
        # Return because we should not get prices in this condition
        return exchangesResultsDict, expectedResultsDict

    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokensToSpendList_etherUnits = " + str(quoteTokensToSpendList_etherUnits))

    threads = []
    lock_pricesDict = Lock()
    for exchangeName in quoteTokenExchangeNamesDict[quoteToken]:
        exchange = ExchangeDict[exchangeName]

        # Skip all exclusions
        if exchangeName in exchangeNameExclusions:
            continue

        if not exchange.EnforceEligibleQuoteToken(quoteToken, False):
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Skipping exchange.GetPrices for " + str(
                exchangeName) + " because it failed EnforceEligibleQuoteToken for quoteToken " + str(quoteToken))
            continue

        if not quoteTokenBaseTokensDict:
            # Only get prices for the tokens this exchange has
            baseTokenList = GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken(exchangeName, quoteToken)
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Using baseTokenList for " + str(
                exchangeName) + " from GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken, baseTokenList of len " + str(
                len(baseTokenList)))
        else:
            # quoteTokenBaseTokensDict[quoteToken] is a list that contains all the baseTokens of interest for all exchanges
            # But not all of those will exist on this particular exchange
            # So I need to find common baseTokens between quoteTokenBaseTokensDict[quoteToken] and this exchange's tradable tokens
            baseTokenList = Libraries.utils.FindIntersectingValuesInLists(
                GetBaseTokenListForSpecificExchangeTradingGivenQuoteToken(exchangeName, quoteToken),
                quoteTokenBaseTokensDict[quoteToken])
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Using baseTokenList for " + str(
                exchangeName) + " from quoteTokenBaseTokensDict, baseTokenList of len " + str(
                len(baseTokenList)))

        # Maintain expectedResultsDict
        expectedResultsDict[exchangeName] = {}
        for baseToken in baseTokenList:
            # Init the value to False since we don't yet know if we get a successful result
            expectedResultsDict[exchangeName][baseToken] = False

        # Call the exchange function's get price function
        # Populate the exchangesResultsDict with the results
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Calling exchange.GetPrices on " + str(exchange.exchangeName))
        t = threading.Thread(target=exchange.GetPrices, args=(
            quoteToken, baseTokenList, quoteTokensToSpendList_etherUnits, blockNumber,
            exchangesResultsDict, exchangeName, lock_pricesDict, detailed))
        threads.append(t)
        t.start()

    # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Waiting for threads to finish, quoteToken = " + str(quoteToken))
    for thread in threads:
        thread.join()

    return exchangesResultsDict, expectedResultsDict


def FindArbitrageOpportunities(quoteTokenList, pricesDict, blockNumber):
    from Libraries.exchanges import ExchangeDict

    # Find arbitrage opportunities
    arbitrageOpportunitiesDict = DictWithValueList()

    # This helps me prevent duplicate analysis
    # Simply add a key to this dict when you've analyzed something
    # Then to check and see whether or not you've already analyzed something, check for that key in the dict
    dictOfTradingPathsIveAlreadyAnalyzed = {}

    # Iterate over all tokens and consider all trading paths for tradingOpportunities
    for quoteToken in quoteTokenList:
        try:
            for exchangeName in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges]:
                try:
                    # Iterate over the other exchanges, aka comparable_exchangeName, and find tradingOpportunities with respect to exchangeName
                    for comparable_exchangeName in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges]:
                        try:
                            # if we're considering trading within one single exchange
                            if exchangeName == comparable_exchangeName:
                                # Ensure we can actually trade within one single exchange
                                if not ExchangeDict[exchangeName].CanArbitrageWithinThisOneSingleExchange():
                                    # if VerboseLogging_FindArbitrageOpportunities:
                                    #     PrintAndLog_FuncNameHeader("I cannot arbitrage between " + str(exchangeName) + " and itself (" + str(
                                    #         comparable_exchangeName) + ") skipping to the next exchange")
                                    continue
                                # else:
                                #     if VerboseLogging_FindArbitrageOpportunities:
                                #         PrintAndLog_FuncNameHeader("I can arbitrage between " + str(exchangeName) + " and itself (" + str(
                                #             comparable_exchangeName) + ") because CanArbitrageWithinThisOneSingleExchange() returned True")

                            for baseToken in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens]:
                                try:
                                    # Ensure that comparable_exchangeName also trades baseToken
                                    if baseToken not in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][comparable_exchangeName][pdk_baseTokens]:
                                        # if VerboseLogging_FindArbitrageOpportunities:
                                        #     PrintAndLog_FuncNameHeader("baseToken " + str(baseToken) + " is not on " + str(comparable_exchangeName) + " so let's skip it")
                                        continue

                                    for index_string in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices]:
                                        try:
                                            quoteTokensToSpend_etherUnits = \
                                                pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][
                                                    baseToken][pdk_prices][index_string][pdk_quoteTokensToSpend_etherUnits]

                                            askPrice = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][pdk_ask]
                                            bidPrice = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][pdk_bid]
                                            # if VerboseLogging_FindArbitrageOpportunities:
                                            #     PrintAndLog_FuncNameHeader("exchangeName = " + str(exchangeName) + " , index_string = " + str(
                                            #         index_string) + ", quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                                            #     PrintAndLog_FuncNameHeader("   askPrice = " + str(askPrice))
                                            #     PrintAndLog_FuncNameHeader("   bidPrice = " + str(bidPrice))

                                            if index_string not in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][comparable_exchangeName][pdk_baseTokens][baseToken][pdk_prices]:
                                                # if VerboseLogging_FindArbitrageOpportunities:
                                                #     PrintAndLog_FuncNameHeader("index_string " + str(index_string) + " is not on " + str(comparable_exchangeName) + " so let's skip it")
                                                # TODO, I could consider looking for a smaller index when this occurs but that would be complicated
                                                #  and may be pointless since the other index in exchangeName should match the other corresponding index in comparable_exchangeName
                                                #  Key question here, is it possible a batched call can partially fail in a way that requires me to cross match indexes??
                                                #  This is not worth investigating right now but should be investigated in the future when i'm nit picking trading strategies
                                                continue

                                            pairOfExchanges = [exchangeName, comparable_exchangeName]
                                            # alphabetize them for consistency
                                            pairOfExchanges.sort()

                                            # Save time by preventing us from analyzing the same exact path repeatedly
                                            # To do this, consider all things that would make a trading path unique and keep track of that as we go through trading paths
                                            tradingPathKey = str(pairOfExchanges) + "_" + quoteToken + "_" + baseToken + "_" + index_string
                                            if tradingPathKey in dictOfTradingPathsIveAlreadyAnalyzed:
                                                # TODO this can be optimized.  Once I analyze the trading path for Uniswap-Kyber for the tokens and index_string,
                                                #  can I just completely skip the next time we match those exchanges but in reverts? Kyber-Uniswap.
                                                #  Or is there a new case that we could hit going the other way?
                                                # if VerboseLogging_FindArbitrageOpportunities:
                                                #     PrintAndLog_FuncNameHeader("   We've already analyzed this trading path, " + str(tradingPathKey) + ", skipping it this time")
                                                continue
                                            else:
                                                # if VerboseLogging_FindArbitrageOpportunities:
                                                #     PrintAndLog_FuncNameHeader("   We haven't yet analyzed this trading path, " + str(tradingPathKey) + ", doing it now")
                                                dictOfTradingPathsIveAlreadyAnalyzed[tradingPathKey] = True

                                            comparable_askPrice = \
                                                pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][comparable_exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][
                                                    pdk_ask]
                                            comparable_bidPrice = \
                                                pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][comparable_exchangeName][pdk_baseTokens][baseToken][pdk_prices][index_string][
                                                    pdk_bid]
                                            # if VerboseLogging_FindArbitrageOpportunities:
                                            #     PrintAndLog_FuncNameHeader("   comparable_exchangeName = " + str(
                                            #         comparable_exchangeName) + " , quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))
                                            #     PrintAndLog_FuncNameHeader("      comparable_askPrice = " + str(comparable_askPrice))
                                            #     PrintAndLog_FuncNameHeader("      comparable_bidPrice = " + str(comparable_bidPrice))

                                            if not IsValidPrice(comparable_askPrice):
                                                # if VerboseLogging_FindArbitrageOpportunities:
                                                #     PrintAndLog_FuncNameHeader("   comparable_askPrice was not a valid price, comparable_askPrice = " + str(comparable_askPrice))
                                                continue

                                            if not IsValidPrice(comparable_bidPrice):
                                                # if VerboseLogging_FindArbitrageOpportunities:
                                                #     PrintAndLog_FuncNameHeader("   comparable_bidPrice was not a valid price, comparable_bidPrice = " + str(comparable_bidPrice))
                                                continue

                                            metaData = None
                                            if pdk_metaData in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken]:
                                                metaData = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][baseToken][pdk_metaData]
                                            # if VerboseLogging_FindArbitrageOpportunities:
                                            #     PrintAndLog_FuncNameHeader("   metaData = " + str(metaData))

                                            comparable_metaData = None
                                            if pdk_metaData in pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][comparable_exchangeName][pdk_baseTokens][baseToken]:
                                                comparable_metaData = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][comparable_exchangeName][pdk_baseTokens][baseToken][
                                                    pdk_metaData]

                                            # if VerboseLogging_FindArbitrageOpportunities:
                                            #     PrintAndLog_FuncNameHeader("      comparable_metaData = " + str(comparable_metaData))

                                            # Check to see if prices cross
                                            if comparable_bidPrice and askPrice and (comparable_bidPrice > askPrice):
                                                arbitrageOpportunity = ArbitrageOpportunity(
                                                    blockNumber, quoteToken, baseToken, pairOfExchanges, exchangeName, comparable_exchangeName, index_string,
                                                    quoteTokensToSpend_etherUnits, askPrice, comparable_bidPrice, metaData, comparable_metaData)
                                                arbitrageOpportunitiesDict.AddItemToDict(quoteToken, arbitrageOpportunity)

                                                # if VerboseLogging_FindArbitrageOpportunities_Found:
                                                #     PrintAndLog_FuncNameHeader("   Found an arbitrage opportunity for " + str(arbitrageOpportunity.GenerateDescriptionString()))

                                            # Check to see if prices cross
                                            elif bidPrice and comparable_askPrice and (bidPrice > comparable_askPrice):
                                                arbitrageOpportunity = ArbitrageOpportunity(
                                                    blockNumber, quoteToken, baseToken, pairOfExchanges, comparable_exchangeName, exchangeName, index_string,
                                                    quoteTokensToSpend_etherUnits, comparable_askPrice, bidPrice, comparable_metaData, metaData)
                                                arbitrageOpportunitiesDict.AddItemToDict(quoteToken, arbitrageOpportunity)

                                                # if VerboseLogging_FindArbitrageOpportunities_Found:
                                                #     PrintAndLog_FuncNameHeader("   Found an arbitrage opportunity for " + str(arbitrageOpportunity.GenerateDescriptionString()))

                                        except (KeyboardInterrupt, SystemExit):
                                            print('\nkeyboard interrupt caught')
                                            print('\n...Program Stopped Manually!')
                                            raise

                                        except:
                                            PrintAndLogError("exception in FindArbitrageOpportunities between exchangeNames " + str(
                                                exchangeName) + " and quoteToken " + str(
                                                quoteToken) + " and baseToken " + str(baseToken) + ", and index_string = " + str(index_string) + ", " + traceback.format_exc())

                                except (KeyboardInterrupt, SystemExit):
                                    print('\nkeyboard interrupt caught')
                                    print('\n...Program Stopped Manually!')
                                    raise

                                except:
                                    PrintAndLogError("exception in FindArbitrageOpportunities between exchangeNames " + str(
                                        exchangeName) + " and quoteToken " + str(
                                        quoteToken) + " and baseToken " + str(baseToken) + ", " + traceback.format_exc())

                        except (KeyboardInterrupt, SystemExit):
                            print('\nkeyboard interrupt caught')
                            print('\n...Program Stopped Manually!')
                            raise

                        except:
                            PrintAndLogError("exception in FindArbitrageOpportunities between exchangeNames " + str(
                                exchangeName) + " and " + str(comparable_exchangeName) + " and quoteToken " + str(
                                quoteToken) + ", " + traceback.format_exc())

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    PrintAndLogError("exception in FindArbitrageOpportunities between exchangeNames " + str(
                        exchangeName) + " and quoteToken " + str(
                        quoteToken) + ", " + traceback.format_exc())

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception in FindArbitrageOpportunities for quoteToken " + str(quoteToken) + ", " + traceback.format_exc())

    arbitrageOpportunitiesDictAnalysis_string = "arbitrageOpportunitiesDict has " + str(
        arbitrageOpportunitiesDict.GetSumOfTotalValuesInAllKeys()) + " potential tradingOpportunities out of " + str(
        len(dictOfTradingPathsIveAlreadyAnalyzed.keys())) + " possible unique trading opportunities\n"
    arbitrageOpportunitiesDictAnalysis_string += str(dictOfTradingPathsIveAlreadyAnalyzed)

    return arbitrageOpportunitiesDict, arbitrageOpportunitiesDictAnalysis_string


def FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm(pricesDict, blockNumber, custom_ratesGraph_Dict=None,
                                                                 custom_ratesGraph_Dict_ExchangeNames=None, custom_ratesGraph_Dict_QuoteTokens=None):
    from Libraries.exchanges import EnforceTokenAddressIsERC20Version

    # This function will find arbitrage opportunities based on the data in the ratesGraphs.
    # If the function caller specifies custom_ratesGraph_Dict, then they are passing in specific ratesGraphs
    # otherwise we get ratesGraphs using Libraries.ratesGraph.GetRatesGraphDicts
    # There are a variety of reasons we may want to modify the ratesGraphs and specify a custom_ratesGraph_Dict
    # One is for tailgating. In this case we're assuming a mempool trade gets mined in and we're trading based on that information
    # This requires us to modify the ratesGraph assuming the prices have changed

    # Find arbitrage opportunities
    arbitrageOpportunitiesDict = DictWithValueList()

    header_logging = "FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm: " + str(randint(0, 99999999999999))
    functionsStartDateTime = datetime.datetime.now()

    # Right now, I don't care what values I pass into GetQuoteTokensToSpendList_etherUnits, I only care about the list length
    # TODO, Refactor this so that it supports all possible quoteTokens because each quoteToken could have a different length list...
    #  For now though, i'm forcing them to have the same length
    quoteTokensToSpendList_etherUnits = Exchanges.keeperDAO.GetQuoteTokensToSpendList_etherUnits(Exchanges.keeperDAO.EthTokenContract)
    for index in range(len(quoteTokensToSpendList_etherUnits) - 1, -1, -1):
        index_string = str(index)
        # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Calling BruteForceArbitrage with index_string = " + str(index_string))

        # Determine where to get ratesGraphs from
        if custom_ratesGraph_Dict:
            # Custom ratesGraphs have been passed in, use them
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Using custom ratesGraphs with index_string = " + str(index_string))
            ratesGraph_Dict = custom_ratesGraph_Dict[blockNumber][index_string]
            ratesGraph_Dict_ExchangeNames = custom_ratesGraph_Dict_ExchangeNames[blockNumber][index_string]
            ratesGraph_Dict_QuoteTokens = custom_ratesGraph_Dict_QuoteTokens[blockNumber][index_string]
        else:
            # We're using ratesGraphs from Libraries.ratesGraph.GetRatesGraphDicts
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Using ratesGraphs from Libraries.ratesGraph.GetRatesGraphDicts index_string = " + str(index_string))
            ratesGraph_Dict, ratesGraph_Dict_ExchangeNames, ratesGraph_Dict_QuoteTokens = Libraries.ratesGraph.GetRatesGraphDicts(blockNumber, index_string)

        # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
        #     # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Libraries.ratesGraph.RatesGraphTokenList of len " + str(len(Libraries.ratesGraph.RatesGraphTokenList)))
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Libraries.ratesGraph.RatesGraphTokenList of len " + str(
        #         len(Libraries.ratesGraph.RatesGraphTokenList)) + " = " + str(Libraries.ratesGraph.RatesGraphTokenList))
        #     # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict of len " + str(len(ratesGraph_Dict)))
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict of len " + str(
        #         len(ratesGraph_Dict)) + " = " + str(ratesGraph_Dict))
        #     # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict_ExchangeNames of len " + str(len(ratesGraph_Dict_ExchangeNames)))
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict_ExchangeNames of len " + str(
        #         len(ratesGraph_Dict_ExchangeNames)) + " = " + str(ratesGraph_Dict_ExchangeNames))
        #     # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict_QuoteTokens of len " + str(len(ratesGraph_Dict_QuoteTokens)))
        #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "ratesGraph_Dict_QuoteTokens of len " + str(
        #         len(ratesGraph_Dict_QuoteTokens)) + " = " + str(ratesGraph_Dict_QuoteTokens))

        # TODO i'll want to do this for each quantity index_string but in an optimal way
        #  After I do it for the very smallest quantity, I could remove all tokens from the data set that do not have any arbitragable paths
        #  so the future runs for other indexes are much more efficient
        sourceTokensList = Exchanges.keeperDAO.GetBorrowableAssetsList()
        # In order to estimate minProfitPercentageRequirement for the arbitrage function below, I have to provide a gas expected to spend on a trade
        # Each exchange and token will have a different gas cost, so it won't be perfect.
        # I should provide a low end estimate here, so if I think the cheapest trade will cost ~150k gas, I should use ~100k just to be safe
        # What we don't want to have happen is I filter out profitable trades by putting in a number that's too high
        minGasCost_ether = Libraries.gasStation.GetGasCost(Libraries.gasStation.GetMinGasPrice_ArbitrageGeneric(),
                                                           Libraries.defaults.MinGasRequired_UsedToDetermine_MinProfitPercentageRequirementForCalculatingArbitrage)

        # Determine minProfitPercentageRequirement based on debug flags
        # Setting this to a positive non zero number will improve performance but will prevent us from analyzing those paths for arbitrage.
        minProfitPercentageRequirement = None
        # If we're debugging
        if Libraries.defaults.FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm_minProfitPercentageRequirement_HardCodeToZero:
            # Hard code minProfitPercentageRequirement to zero
            minProfitPercentageRequirement = 0
        # If we're in production
        else:
            # Set minProfitPercentageRequirement based on expected gas cost and amount we're trading
            # This logic is fine because quoteTokensToSpendList_etherUnits is always in ether units and minGasCost_ether is also always in ether
            # No need to convert to quoteTokens here since we're assuming ether always here
            minProfitPercentageRequirement = minGasCost_ether / quoteTokensToSpendList_etherUnits[index]
            # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "minGasCost_ether = " + str(minGasCost_ether))
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "quoteTokensToSpendList_etherUnits[index] = " + str(quoteTokensToSpendList_etherUnits[index]))
            #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "minProfitPercentageRequirement = " + str(minProfitPercentageRequirement))

        profitPercentageList, pair2dList, rates2dList, exchangeNames2dList, quoteTokens2dList = \
            Libraries.arbAlgos.BruteForceArbitrage(ratesGraph_Dict, ratesGraph_Dict_ExchangeNames, ratesGraph_Dict_QuoteTokens,
                                                   Libraries.ratesGraph.RatesGraphTokenList, sourceTokensList,
                                                   str(blockNumber) + "_quantityIndexGoesHere", minProfitPercentageRequirement)

        for index_profitPercentage, profitPercentage in enumerate(profitPercentageList):
            try:
                pairList = pair2dList[index_profitPercentage]
                ratesList = rates2dList[index_profitPercentage]

                # exchangeNamesList = exchangeNames2dList[index_profitPercentage]
                # quoteTokensList = quoteTokens2dList[index_profitPercentage]
                # We have to convert the indexes to actual exchangeNames
                exchangeNamesList = Libraries.ratesGraph.ConvertListOfExchangeNameIndexesToExchangeNames(exchangeNames2dList[index_profitPercentage])
                # We have to convert the indexes to actual token addresses
                quoteTokensList = Libraries.ratesGraph.ConvertListOfTokenIndexesToTokens(quoteTokens2dList[index_profitPercentage])

                # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Creating ArbitrageOpportunity object, index_string = " + str(
                #         index_string) + ", quoteTokensToSpendList_etherUnits[index] = " + str(quoteTokensToSpendList_etherUnits[index]))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profitPercentage = " + str(profitPercentage))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   pairList = " + str(pairList))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   ratesList = " + str(ratesList))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   exchangeNamesList = " + str(exchangeNamesList))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   quoteTokensList = " + str(quoteTokensList))

                quoteToken = pairList[0][0]
                # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   quoteToken = " + str(quoteToken))

                # TODO REMOVEME
                # We want to modify quoteTokensList and use the first token in pairList as the quoteToken
                # This is important! But it doesn't make sense without looking at an examples while debugging
                # quoteTokensList_toUse = quoteTokensList.copy()
                # quoteTokensList_toUse[0] = quoteToken
                # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   quoteTokensList_toUse = " + str(quoteTokensList_toUse))

                # Build the tradePath based on pairList
                tradePath = []
                # Start by adding the quoteToken
                tradePath.append(quoteToken)
                skipToggle = False
                for tokenPair in pairList:
                    for token in tokenPair:
                        if skipToggle:
                            tradePath.append(token)
                        skipToggle = not skipToggle

                # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   tradePath = " + str(tradePath))

                metaDataPath = []
                for index_exchangeName, exchangeName in enumerate(exchangeNamesList):
                    # Determine which is the quoteToken and which is the baseToken (in terms of pricesDict) so we can look up its metaData in the pricesDict
                    token1 = tradePath[index_exchangeName]
                    token2 = tradePath[index_exchangeName + 1]
                    # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                    #                          "   determining quote/base tokens for exchangeName = " + str(
                    #                              exchangeName) + ", token1 = " + str(token1) + " and token2 = " + str(token2))
                    local_quoteToken, local_baseToken = Exchanges.keeperDAO.ValidateThatExpectedQuoteTokenIsInTheseTwoTokensBeingTraded(
                        token1, token2, quoteTokensList[index_exchangeName])

                    # Extract the metaData
                    # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                    #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                    #                          "   finding metaData for exchangeName = " + str(exchangeName) + ", local_quoteToken = " + str(
                    #                              local_quoteToken) + " and local_baseToken = " + str(local_baseToken))
                    metaData = None
                    if pdk_metaData in pricesDict[pdk_quoteTokens][local_quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][local_baseToken]:
                        metaData = pricesDict[pdk_quoteTokens][local_quoteToken][pdk_exchanges][exchangeName][pdk_baseTokens][local_baseToken][pdk_metaData]

                    metaDataPath.append(metaData)

                # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   metaDataPath = " + str(metaDataPath))

                # quoteTokensToSpend_etherUnits comes from the very first trade made, so quoteToken and then the very first baseToken with respect to pricesDict
                # grab the baseToken so we can look up in the pricesDict the quoteTokensToSpend_etherUnits
                # assume all baseTokens must be ERC20s, ETH must never be actual ETH as a baseToken, it must be WETH or else we'll have lots of bugs
                baseToken_for_quoteTokensToSpend_etherUnits = EnforceTokenAddressIsERC20Version(tradePath[1]).lower()
                exchangeName_for_quoteTokensToSpend_etherUnits = exchangeNamesList[0]
                # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "   baseToken_for_quoteTokensToSpend_etherUnits = " + str(baseToken_for_quoteTokensToSpend_etherUnits))
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                #                          "   exchangeName_for_quoteTokensToSpend_etherUnits = " + str(exchangeName_for_quoteTokensToSpend_etherUnits))
                quoteTokensToSpend_etherUnits = pricesDict[pdk_quoteTokens][quoteToken][pdk_exchanges][exchangeName_for_quoteTokensToSpend_etherUnits][
                    pdk_baseTokens][baseToken_for_quoteTokensToSpend_etherUnits][pdk_prices][index_string][pdk_quoteTokensToSpend_etherUnits]
                # if VerboseLogging_FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm:
                #     PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   quoteTokensToSpend_etherUnits = " + str(quoteTokensToSpend_etherUnits))

                # TODO every item in profitPercentageList could be considered an ArbitrageOpportunity, but most of them won't meet the min requirements
                # TODO refactor ArbitrageOpportunity class to meet my new requirements
                #  quoteToken          ETH
                #  tradePath       ETH,   USDC,   DAI,   ETH  This means trade ETH -> USDC -> DAI -> ETH.  For simplicity we assume that last trade always goes back to the quoteToken
                #  exchangeNamePath    Uniswap, Curve,  0x   This tells us that ETH -> USDC on Uniswap, USDC -> DAI on Curve, DAI -> ETH on 0x.
                #  tradePricesPath     Not yet sure if I should just use the rate here, or convert to price. Any reason I should want both???
                #  index_string is fine, I will need to call BruteForceArbitrage once for each index_string
                #  quoteTokensToSpend_etherUnits is fine
                #  metaDataPath will be the metadata from the pricesDict based on index_string
                arbitrageOpportunity = ArbitrageOpportunity(
                    blockNumber, quoteToken, profitPercentage, tradePath, exchangeNamesList, quoteTokensList,
                    ratesList, index_string, quoteTokensToSpend_etherUnits, metaDataPath)
                arbitrageOpportunitiesDict.AddItemToDict(quoteToken, arbitrageOpportunity)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception while parsing arbitrage results, blockNumber = " + str(
                    blockNumber) + ", header_logging = " + str(header_logging) + ", exception = " + traceback.format_exc())
                pass

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "finished with index_string = " + str(index_string))

    arbitrageOpportunitiesDictAnalysis_string = "arbitrageOpportunitiesDict has " + str(
        arbitrageOpportunitiesDict.GetSumOfTotalValuesInAllKeys()) + " potential tradingOpportunities out of TODO possible unique trading opportunities\n"

    return arbitrageOpportunitiesDict, arbitrageOpportunitiesDictAnalysis_string


def ArbitrageGeneric(header_logging, functionsStartDateTime, pricesDict, gasPriceFromConfig,
                     reservableEthereumAccountPool, blockNumber, exchangeNamesRequiredToTrade,
                     gasPriceIsAllowedToIncreaseForFrontRunning, tradingTechnique, pendingTradeTxList_tailgating=None,
                     custom_ratesGraph_Dict=None, custom_ratesGraph_Dict_ExchangeNames=None, custom_ratesGraph_Dict_QuoteTokens=None):
    tailgatingTxHash = None
    if tradingTechnique == TradingTechnique.Tailgate:
        # We can safely assume all txHashes in the list are the same
        tailgatingTxHash = pendingTradeTxList_tailgating[0].txHash

    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Finding trading opportunities")
    arbitrageOpportunitiesDict, arbitrageOpportunitiesDictAnalysis_string = FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm(
        pricesDict, blockNumber, custom_ratesGraph_Dict, custom_ratesGraph_Dict_ExchangeNames, custom_ratesGraph_Dict_QuoteTokens)
    # arbitrageOpportunitiesDict, arbitrageOpportunitiesDictAnalysis_string = FindArbitrageOpportunities_UsingGraphBasedArbitrageAlgorithm(
    #     pricesDict, blockNumber)

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Found " + str(arbitrageOpportunitiesDict.GetSumOfTotalValuesInAllKeys()) + " potential tradingOpportunities")
    if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "arbitrageOpportunitiesDictAnalysis_string = " + str(arbitrageOpportunitiesDictAnalysis_string))

    # Check if a new block has already been mined in
    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    if latestBlockNumber > blockNumber:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "blockNumber " + str(blockNumber) + " is now old, a new block " +
                             str(latestBlockNumber) + " was discovered. I've stopped processing this old block")
        return

    # Make a list of the arbitrageOpportunitys and sort them by profit_eth so they're comparable (since we cannot compare ETH to DAI to USDC...)
    arbitrageOpportunityList = []
    for quoteToken in arbitrageOpportunitiesDict.GetKeys():
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Step 1, quoteToken = " + str(quoteToken))
        if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "iterating over " + str(
                len(arbitrageOpportunitiesDict.GetItems(quoteToken))) + " arbitrageOpportunities for quoteToken " + str(quoteToken))

        for arbitrageOpportunity in arbitrageOpportunitiesDict.GetItems(quoteToken):
            if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Found arbitrageOpportunity between exchanges " + str(arbitrageOpportunity.exchangeNamesList) + " with quoteToken " + str(
                                         quoteToken) + " and tradePath " + str(arbitrageOpportunity.tradePath) + ", quoteTokenAmount = " + str(
                                         arbitrageOpportunity.quoteTokenAmount))

            ninjaObject = Exchanges.ninja.NinjaInstance_Generic

            if not ninjaObject:
                if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "This arbitrageOpportunity corresponds with NONE of our Ninjas! "
                                         "We should implement this ninja so we can capture this trading profit "
                                         "across exchanges " + str(arbitrageOpportunity.exchangeNamesList))

            # If any exchangeNames in the arbitrageOpportunity are disabled via ExchangeNamesNotPermittedToTrade
            elif Libraries.utils.DoAnyItemsInTheseListsIntersect(ExchangeNamesNotPermittedToTrade, arbitrageOpportunity.exchangeNamesList):
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "This arbitrageOpportunity is not permitted to trade because at least one of its exchanges are disabled. ExchangeNamesNotPermittedToTrade"
                                     " = " + str(ExchangeNamesNotPermittedToTrade) + ", arbitrageOpportunity.exchangeNamesList " + str(arbitrageOpportunity.exchangeNamesList))

            else:
                if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "This arbitrageOpportunity corresponds with ninjaObject " + str(ninjaObject.ninjaType))

                canSourceQuoteTokensForTrade = arbitrageOpportunity.SetQuoteTokenSource()
                if not canSourceQuoteTokensForTrade:
                    # TODO, instead of continuing here, I should set a flag saying I'm under filling the order and attempt to trade with the max available
                    #  Or maybe I do not need to because my filters in this ArbitrageGeneric function are going to handle it and
                    #  another trade with a lower quantity on the same path fill show up as Arbitrage - Yes
                    # Do no add arbitrageOpportunity to arbitrageOpportunityList
                    continue

                # Check my debug flags to make sure these features are enabled
                if arbitrageOpportunity.GetBool_DoTradeWithinFlashLoan():
                    if not Libraries.defaults.EnableSourcingQuoteTokensFrom_KeeperDAOFlashLoan:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "NOT sourcing quoteTokens from KeeperDAO flash loan contract because EnableSourcingQuoteTokensFrom_KeeperDAOFlashLoan was False")
                        # Do no add arbitrageOpportunity to arbitrageOpportunityList
                        continue
                else:
                    if not Libraries.defaults.EnableSourcingQuoteTokensFrom_NinjaContract:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "NOT sourcing quoteTokens from Local Ninja contract because EnableSourcingQuoteTokensFrom_NinjaContract was False")
                        # Do no add arbitrageOpportunity to arbitrageOpportunityList
                        continue

                # Associate the ninjaObject with the arbitrageOpportunity
                arbitrageOpportunity.SetNinjaObject(ninjaObject)
                arbitrageOpportunityList.append(arbitrageOpportunity)

                # This is where each protocol is going to do something unique
                # Uniswap and UniswapV2: I can check get any price for any trade quantity, I can also rely on prices in pricesDict, price changes per trade quantity
                # Kyber: I have to rely on prices in pricesDict, price changes per trade quantity
                # Set: Price is always the same for all trade quantities
                # 0x: Must iterate over orders in orderbook, price is always the same for all trade quantities

    # Nulling out this variable to make sure I don't accidentally use it again now that we're out of scope and it's a commonly used variable name
    quoteToken = None

    # Sort by profit_eth here because it's consistent amount all quoteTokens
    arbitrageOpportunityList.sort(key=lambda x: x.profit_eth, reverse=True)
    for arbitrageOpportunity in arbitrageOpportunityList:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "arbitrageOpportunitys sorted: profit_eth = " + str(arbitrageOpportunity.profit_eth) + ", profit_quoteTokens = " + str(
                                 arbitrageOpportunity.profit_quoteTokens) + ": " + str(arbitrageOpportunity.GenerateDescriptionString()))

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 2, remove inferior trading paths, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    local_arbitrageOpportunityDict = {}
    new_arbitrageOpportunityList = []
    for arbitrageOpportunity in arbitrageOpportunityList.copy():
        key = arbitrageOpportunity.GenerateDescriptionString_Exchanges().lower()
        if key not in local_arbitrageOpportunityDict:
            # Use the dict to make sure we only have 1 of each key
            # Use the list so we can maintain order sorted by profit_eth
            local_arbitrageOpportunityDict[key] = arbitrageOpportunity
            new_arbitrageOpportunityList.append(arbitrageOpportunity)

    # Update arbitrageOpportunityList
    arbitrageOpportunityList = new_arbitrageOpportunityList
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Updated arbitrageOpportunityList based on new_arbitrageOpportunityList")

    if Libraries.defaults.VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug:
        PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList)

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 3, Now checking debug flags, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    # Handle RequireTradesToIncludeTheseExchangeNames for debug purposes
    if Libraries.defaults.RequireTradesToIncludeTheseExchangeNames:
        try:
            # Remove all arbitrageOpportunitys that do not match our debug requirements
            # Fill this array with exchangeNames you want to debug
            # For example, if you want to test specifically ExchangeName_0xMesh, then put only ExchangeName_0xMesh in the list and only this exchange will trade
            # Or if you want to specifically test ExchangeName_0xMesh & ExchangeName_Set, put them both in list and only they will trade

            if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit")

            for arbitrageOpportunity in arbitrageOpportunityList.copy():
                requiredExchangeIsInList = True
                for exchangeName in Libraries.defaults.RequireTradesToIncludeTheseExchangeNames:
                    if exchangeName not in arbitrageOpportunity.exchangeNamesList:
                        requiredExchangeIsInList = False
                        break

                if not requiredExchangeIsInList:
                    # let's remove it from the arbitrageOpportunityList
                    arbitrageOpportunityList.remove(arbitrageOpportunity)
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "Not considering trading on " + str(arbitrageOpportunity.GenerateDescriptionString()) +
                                         " because RequireTradesToIncludeTheseExchangeNames is turned on and this arbitrageOpportunity "
                                         "does not contain Libraries.defaults.RequireTradesToIncludeTheseExchangeNames. arbitrageOpportunity.exchangeNamesList = " +
                                         str(arbitrageOpportunity.exchangeNamesList) + ", Libraries.defaults.RequireTradesToIncludeTheseExchangeNames = " +
                                         str(Libraries.defaults.RequireTradesToIncludeTheseExchangeNames))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception during RequireTradesToIncludeTheseExchangeNames, " + traceback.format_exc())

    # Remove all arbitrageOpportunitys that do not match our exchangeNamesRequiredToTrade
    # An empty list means it can trade on any exchanges
    if len(exchangeNamesRequiredToTrade) > 0:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Enforcing exchangeNamesRequiredToTrade = " + str(exchangeNamesRequiredToTrade))
        if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit")

        for arbitrageOpportunity in arbitrageOpportunityList.copy():
            requiredExchangeIsInList = True
            for exchangeName in exchangeNamesRequiredToTrade:
                if exchangeName not in arbitrageOpportunity.exchangeNamesList:
                    requiredExchangeIsInList = False
                    break

            if not requiredExchangeIsInList:
                # let's remove it from the arbitrageOpportunityList
                arbitrageOpportunityList.remove(arbitrageOpportunity)
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Not considering trading on " + str(arbitrageOpportunity.GenerateDescriptionString()) +
                                     " because this arbitrageOpportunity does not contain an exchange from exchangeNamesRequiredToTrade. "
                                     "arbitrageOpportunity.exchangeNamesList = " + str(arbitrageOpportunity.exchangeNamesList) +
                                     ", exchangeNamesRequiredToTrade = " + str(exchangeNamesRequiredToTrade))

    # Remove all arbitrageOpportunitys that do not match our RequireThisNumberOfLegsInTrade flag
    if Libraries.defaults.RequireThisNumberOfLegsInTrade:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Enforcing RequireThisNumberOfLegsInTrade = " + str(Libraries.defaults.RequireThisNumberOfLegsInTrade))
        if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit")

        for arbitrageOpportunity in arbitrageOpportunityList.copy():
            requiredNumberOfLegsInTradeAreMet = True
            if arbitrageOpportunity.GetNumOfTradeLegs() != Libraries.defaults.RequireThisNumberOfLegsInTrade:
                requiredNumberOfLegsInTradeAreMet = False

            if not requiredNumberOfLegsInTradeAreMet:
                # let's remove it from the arbitrageOpportunityList
                arbitrageOpportunityList.remove(arbitrageOpportunity)
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Not considering trading on " + str(
                                         arbitrageOpportunity.GenerateDescriptionString()) + " because debug flag shows RequireThisNumberOfLegsInTrade = " + str(
                                         Libraries.defaults.RequireThisNumberOfLegsInTrade) + " yet arbitrageOpportunity.GetNumOfTradeLegs() = " + str(
                                         arbitrageOpportunity.GetNumOfTradeLegs()) + ". Not trading for debug purposes")

    # Remove all arbitrageOpportunitys that do not match our RequireThisAsTradeQuoteToken flag
    if Libraries.defaults.RequireThisAsTradeQuoteToken:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Enforcing RequireThisAsTradeQuoteToken = " + str(Libraries.defaults.RequireThisAsTradeQuoteToken))
        if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit")

        for arbitrageOpportunity in arbitrageOpportunityList.copy():
            requiredQuoteTokenMet = True
            if arbitrageOpportunity.quoteToken.lower() != Libraries.defaults.RequireThisAsTradeQuoteToken.lower():
                requiredQuoteTokenMet = False

            if not requiredQuoteTokenMet:
                # let's remove it from the arbitrageOpportunityList
                arbitrageOpportunityList.remove(arbitrageOpportunity)
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Not considering trading on " + str(
                                         arbitrageOpportunity.GenerateDescriptionString()) + " because debug flag shows RequireThisAsTradeQuoteToken = " + str(
                                         Libraries.defaults.RequireThisAsTradeQuoteToken) + " yet arbitrageOpportunity.quoteToken = " + str(
                                         arbitrageOpportunity.GetNumOfTradeLegs()) + ". Not trading for debug purposes")

    if Libraries.defaults.VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug:
        PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList)

    tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating = None
    if tradingTechnique == TradingTechnique.Tailgate:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Step 3.5, filtering for pendingTradeTxList_tailgating interaction only, arbitrageOpportunityList has " +
                             str(len(arbitrageOpportunityList)) + " items")
        # We only want to consider arbitrageOpportunitys where they actually follow the path that we're trying to tailgate
        # It's important to note that we dont' actually care where in teh trading path it is.
        # For example, it could be the first trade of a 2 leg trade, the middle trade of a 3 leg trade, or the last trade of a 5 leg trade
        # as long as it's in this trade and it's the exact direction, that's all we care about
        # Example, if we're tailgating someone who's selling USDC for WETH on UniswapV2,
        # that means we need to reject all arbitrageOpportunitys except ones that buy USDC for WETH on UniswapV2 on the exact pool that we're targeting
        # UniswapV2 has exactly one pool per pair so that's simple, but when we start tailgating Balancer we'll need to make sure we filter by the exact pool
        # TODO, get that unique identifier. What's that variable again and how do I get/set that??
        #  Set the unique identifier in the pendingTradeTxList_tailgating object,
        #  then all i'll have to do here is get it and compare with the arbitrageOpportunity's path's unique identifiers in the trade
        #  also compare the side
        #  also compare the tokens in the trade
        for arbitrageOpportunity in arbitrageOpportunityList.copy():
            try:
                # Determine if the tailgating trade is on the arbitrageOpportunity path
                tailgatingIsOnArbitrageOpportunityPath = False

                for pendingTradeTx_tailgating in pendingTradeTxList_tailgating:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "examining pendingTradeTx_tailgating against arbitrageOpportunity " + str(arbitrageOpportunity.GenerateDescriptionString()))
                    for pathIndex, exchangeName in enumerate(arbitrageOpportunity.exchangeNamesList):

                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   checking exchangeName = " + str(exchangeName) + " against pendingTradeTx_tailgating.exchangeName " +
                                             str(pendingTradeTx_tailgating.exchangeName))
                        # TODO, check not only exchangeName but also exchange pool unique identifier
                        #  I must do this in order to support tailgating on Balancer
                        #  For tailgating on Uniswap this isn't required though...
                        if exchangeName == pendingTradeTx_tailgating.exchangeName:
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   passed exchangeName")

                            side = arbitrageOpportunity.sidesList_wrtPricesDict[pathIndex]
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                 "   checking side = " + str(side) + " against pendingTradeTx_tailgating.side " + str(pendingTradeTx_tailgating.side))
                            if side.lower() == Libraries.core.InvertOrderType(pendingTradeTx_tailgating.side).lower():
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   passed side")

                                dontCare1, dontCare2, quoteToken_arbitrageOpportunity, baseToken_arbitrageOpportunity = arbitrageOpportunity.GetTokenFlow(pathIndex)
                                quoteToken_pendingTradeTx_tailgating, baseToken_pendingTradeTx_tailgating = pendingTradeTx_tailgating.GetTokensBeingTraded()
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   checking tokens. quoteToken_arbitrageOpportunity = " + str(quoteToken_arbitrageOpportunity) +
                                                     ", baseToken_arbitrageOpportunity = " + str(baseToken_arbitrageOpportunity) + ", quoteToken_pendingTradeTx_tailgating = " +
                                                     str(quoteToken_pendingTradeTx_tailgating) + ", baseToken_pendingTradeTx_tailgating = " + str(baseToken_pendingTradeTx_tailgating))
                                if quoteToken_arbitrageOpportunity.lower() == quoteToken_pendingTradeTx_tailgating.lower() and \
                                        baseToken_arbitrageOpportunity.lower() == baseToken_pendingTradeTx_tailgating.lower():
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   passed tokens")
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   all passed, is this correct?")
                                    tailgatingIsOnArbitrageOpportunityPath = True
                                    # Set this variable so we know which pendingTradeTx actually matters
                                    tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating = pendingTradeTx_tailgating
                                    break
                                else:
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   failed tokens")

                            else:
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   failed side. But, it can still pass if the tokens match in reverse")

                                dontCare1, dontCare2, quoteToken_arbitrageOpportunity, baseToken_arbitrageOpportunity = arbitrageOpportunity.GetTokenFlow(pathIndex)
                                quoteToken_pendingTradeTx_tailgating, baseToken_pendingTradeTx_tailgating = pendingTradeTx_tailgating.GetTokensBeingTraded()
                                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                     "   checking tokens in reverse. quoteToken_arbitrageOpportunity = " + str(quoteToken_arbitrageOpportunity) +
                                                     ", baseToken_arbitrageOpportunity = " + str(baseToken_arbitrageOpportunity) + ", quoteToken_pendingTradeTx_tailgating = " +
                                                     str(quoteToken_pendingTradeTx_tailgating) + ", baseToken_pendingTradeTx_tailgating = " + str(baseToken_pendingTradeTx_tailgating))
                                if quoteToken_arbitrageOpportunity.lower() == baseToken_pendingTradeTx_tailgating.lower() and \
                                        baseToken_arbitrageOpportunity.lower() == quoteToken_pendingTradeTx_tailgating.lower():
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   passed tokens")
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   all passed based on reverse token ordering, is this correct?")
                                    tailgatingIsOnArbitrageOpportunityPath = True
                                    # Set this variable so we know which pendingTradeTx actually matters
                                    tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating = pendingTradeTx_tailgating
                                    break
                                else:
                                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   failed tokens")

                        else:
                            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   failed exchangeName")

                    if tailgatingIsOnArbitrageOpportunityPath:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   breaking because tailgatingIsOnArbitrageOpportunityPath was already True. "
                                             "No point in examining the next one. Breaking from the outer loop as well")
                        break

                if tailgatingIsOnArbitrageOpportunityPath:
                    # if VerboseLogging_ArbitrageGeneric:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "   tailgating trade is on arbitrageOpportunity path, may proceed with tailgating consideration. " +
                                         str(arbitrageOpportunity.GenerateDescriptionString()))
                else:
                    # if VerboseLogging_ArbitrageGeneric:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "   tailgating trade is NOT on arbitrageOpportunity path, will not proceed with tailgating consideration. " +
                                         str(arbitrageOpportunity.GenerateDescriptionString()))

                    # let's remove it from the arbitrageOpportunityList
                    arbitrageOpportunityList.remove(arbitrageOpportunity)

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                # let's remove it from the arbitrageOpportunityList
                arbitrageOpportunityList.remove(arbitrageOpportunity)
                PrintAndLogError("Arbitrage - Maybe. But we had an EXCEPTION! Investigate this ASAP. No exceptions should occur here " + str(
                    arbitrageOpportunity.GenerateDescriptionString()) + ", exception when setting gas limit and gas price in ArbitrageGeneric, " + traceback.format_exc())

        if Libraries.defaults.Tailgating_CapGasPriceForTesting:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Step 3.6, enforcing Tailgating_CapGasPriceForTesting, arbitrageOpportunityList has " +
                                 str(len(arbitrageOpportunityList)) + " items")
            for arbitrageOpportunity in arbitrageOpportunityList.copy():
                try:
                    # We can assume all the gas prices in pendingTradeTxList_tailgating are exactly the same because they are in the same tx
                    gasPrice_fromTailgatingTx = pendingTradeTxList_tailgating[0].gasPrice_wei
                    if gasPrice_fromTailgatingTx > Libraries.gasStation.GetMaxGasPrice_DebuggingTailgatingAndForcingTradesThrough():
                        # if VerboseLogging_ArbitrageGeneric:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "   tailgating trade's gas price is too high. We are debugging in Tailgating_CapGasPriceForTesting."
                                             " will not proceed with tailgating consideration. " +
                                             str(arbitrageOpportunity.GenerateDescriptionString()))

                        # let's remove it from the arbitrageOpportunityList
                        arbitrageOpportunityList.remove(arbitrageOpportunity)

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    # let's remove it from the arbitrageOpportunityList
                    arbitrageOpportunityList.remove(arbitrageOpportunity)
                    PrintAndLogError("Arbitrage - Maybe. But we had an EXCEPTION! Investigate this ASAP. No exceptions should occur here " + str(
                        arbitrageOpportunity.GenerateDescriptionString()) + ", exception when setting gas limit and gas price in ArbitrageGeneric, " + traceback.format_exc())

    if Libraries.defaults.VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug:
        PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList)

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 3.7, Limiting the max number of arbitrageOpportunitys, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    # This helps with performance, because there are times when Ninja will detect 1000+ arbitrageOpportunitys and that murders performance
    maxSize = 120  # 200 was hurting performance when calculating profitability in a later step
    arbitrageOpportunityList = Libraries.utils.UpdateList_KeepOnlyTopNItemsInList(arbitrageOpportunityList, maxSize)
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "arbitrageOpportunityList now has " + str(len(arbitrageOpportunityList)) + " items")

    if Libraries.defaults.VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug:
        PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList)

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 4, gas and profitability, arbitrageOpportunityList has " +
                         str(len(arbitrageOpportunityList)) + " items. gasPriceFromConfig = " + str(gasPriceFromConfig))
    # Prepare the gasLimit and gasPrice for the trades and determine profitability
    # NOTE: This step is resource expensive. A lot of logic is processed here per arbitrageOpportunity so ideally i've
    # narrowed down arbitrageOpportunityList pretty far at this point so I'm not processing hundreds of arbitrageOpportunitys here
    for arbitrageOpportunity in arbitrageOpportunityList.copy():
        try:
            gas_usedForCalculatingMinProfitRequirement = arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()

            if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "gas_usedForCalculatingMinProfitRequirement = " + str(gas_usedForCalculatingMinProfitRequirement))

            if Libraries.defaults.UseDebugMinProfitRequirement_NinjaGeneric:
                minProfitRequirement_quoteTokens = Libraries.defaults.MinProfitRequirement_quoteToken_NinjaDebug  # Use for debugging
                message = "Using debug minProfitRequirement_quoteTokens value because UseDebugMinProfitRequirement_NinjaGeneric was True"
                if IsTimeToExecute(message, 200):
                    Libraries.core.API_PostOperatorNotification(message)
            else:
                minProfitRequirement_quoteTokens = Libraries.gasStation.GetMinProfitRequirement_GivenGasProperties(
                    gasPriceFromConfig, gas_usedForCalculatingMinProfitRequirement, arbitrageOpportunity.quoteToken)

            if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "minProfitRequirement_quoteTokens = " + str(minProfitRequirement_quoteTokens))

            arbitrageOpportunity.SetMinProfitRequirement_quoteTokens(minProfitRequirement_quoteTokens)
            arbitrageOpportunity.SetGas_usedForCalculatingMinProfitRequirement(gas_usedForCalculatingMinProfitRequirement)
            gasPriceIsAboveMinimum = arbitrageOpportunity.CalculateGasPrice(gasPriceFromConfig)

            if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Enforcing the minProfitRequirement_quoteTokens for " + str(arbitrageOpportunity.GenerateDescriptionString()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   profit_quoteTokens = " + str(arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   minProfitRequirement_quoteTokens = " + str(
                                         arbitrageOpportunity.minProfitRequirement_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profitPercent = " + str(round(100 * arbitrageOpportunity.profitPercent, 4)) + " %")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice)) + " gwei")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   expectedGasUsage = " + str(arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   suggestedGasLimit = " + str(arbitrageOpportunity.GetSuggestedGasLimit_AllTradeActions()))

            if not gasPriceIsAboveMinimum:
                # The trade has a little bit of profit available, but it's so small that at this gasPrice it would never get mined in
                # let's remove it from the arbitrageOpportunityList
                arbitrageOpportunityList.remove(arbitrageOpportunity)
                if Libraries.defaults.VerboseLogging_ArbitrageGeneric_ArbitrageNo:
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "Arbitrage - No. Failed gasPriceIsAboveMinimum. " + str(arbitrageOpportunity.GenerateDescriptionString()))
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "   profit_quoteTokens = " + str(arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                         "   minProfitRequirement_quoteTokens = " + str(
                                             arbitrageOpportunity.minProfitRequirement_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profitPercent = " + str(round(100 * arbitrageOpportunity.profitPercent, 4)) + " %")
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice)) + " gwei")
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   expectedGasUsage = " + str(arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()))
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   suggestedGasLimit = " + str(arbitrageOpportunity.GetSuggestedGasLimit_AllTradeActions()))
                    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   doTradeWithinFlashLoan = " + str(arbitrageOpportunity.GetBool_DoTradeWithinFlashLoan()))
                continue

            # Determine profitability and enforce minProfitRequirement_quoteTokens
            if arbitrageOpportunity.IsProfitable():
                # The trade is profitable, leave it be
                continue

            # If we've made it this far, the trade is not profitable
            # let's remove it from the arbitrageOpportunityList
            arbitrageOpportunityList.remove(arbitrageOpportunity)
            if Libraries.defaults.VerboseLogging_ArbitrageGeneric_ArbitrageNo:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Arbitrage - No. Failed IsProfitable(). " + str(arbitrageOpportunity.GenerateDescriptionString()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   profit_quoteTokens = " + str(arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   minProfitRequirement_quoteTokens = " + str(
                                         arbitrageOpportunity.minProfitRequirement_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profitPercent = " + str(round(100 * arbitrageOpportunity.profitPercent, 4)) + " %")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice)) + " gwei")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   expectedGasUsage = " + str(arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   suggestedGasLimit = " + str(arbitrageOpportunity.GetSuggestedGasLimit_AllTradeActions()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   doTradeWithinFlashLoan = " + str(arbitrageOpportunity.GetBool_DoTradeWithinFlashLoan()))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            # let's remove it from the arbitrageOpportunityList
            arbitrageOpportunityList.remove(arbitrageOpportunity)
            PrintAndLogError("Arbitrage - Maybe. But we had an EXCEPTION! Investigate this ASAP. No exceptions should occur here " + str(
                arbitrageOpportunity.GenerateDescriptionString()) + ", exception when setting gas limit and gas price in ArbitrageGeneric, " + traceback.format_exc())

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 4.1, gas price sanity check, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    # Prepare the gasLimit and gasPrice for the trades and determine profitability
    # NOTE: This step is resource expensive. A lot of logic is processed here per arbitrageOpportunity so ideally i've
    # narrowed down arbitrageOpportunityList pretty far at this point so I'm not processing hundreds of arbitrageOpportunitys here
    for arbitrageOpportunity in arbitrageOpportunityList.copy():
        try:
            estimatedGasCost_ether = arbitrageOpportunity.GetEstimatedGasCost_ether()

            if Libraries.defaults.VerboseLogging_ArbitrageGeneric_ArbitrageNo:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Sanity checking the initial gasPrice we've chosen against our expected profit")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice)) + " gwei")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   expectedGasUsage = " + str(arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   estimatedGasCost_ether = " + str(estimatedGasCost_ether) + " ETH")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   arbitrageOpportunity.profit_eth = " + str(arbitrageOpportunity.profit_eth) + " ETH")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   arbitrageOpportunity.profit_quoteTokens = " + str(arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   GasPriceSanityCheckBufferMultiplier = " + str(Libraries.defaults.GasPriceSanityCheckBufferMultiplier))

            # If the gas cost math doesn't make any sense and we appear to be spending way more on gas than we're going to make
            if not arbitrageOpportunity.DoesPassGasPriceSanityCheck_EstimatedGasCost_ether(estimatedGasCost_ether):
                # NOTE: This should never happen. If we make it here, this means we have a bug in either our gasPrice calculations or profit calculations and we need to fix it ASAP
                # This is our sanity check safety net that catches bugs in the event we somehow slipped through with a wrong gasPrice with respect to expected profit
                # let's remove it from the arbitrageOpportunityList
                arbitrageOpportunityList.remove(arbitrageOpportunity)

                message = "Failed gasPrice sanity check. This is bad, fix this bug ASAP!"
                Libraries.core.API_PostOperatorNotification(message)

                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Arbitrage - No. Failed gasPrice sanity check. " + str(arbitrageOpportunity.GenerateDescriptionString()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   profit_quoteTokens = " + str(arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   minProfitRequirement_quoteTokens = " + str(
                                         arbitrageOpportunity.minProfitRequirement_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profitPercent = " + str(round(100 * arbitrageOpportunity.profitPercent, 4)) + " %")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice)) + " gwei")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   expectedGasUsage = " + str(arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   estimatedGasCost_ether = " + str(estimatedGasCost_ether) + " ETH")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   arbitrageOpportunity.profit_eth = " + str(arbitrageOpportunity.profit_eth) + " ETH")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   arbitrageOpportunity.profit_quoteTokens = " + str(arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   GasPriceSanityCheckBufferMultiplier = " + str(Libraries.defaults.GasPriceSanityCheckBufferMultiplier))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            # let's remove it from the arbitrageOpportunityList
            arbitrageOpportunityList.remove(arbitrageOpportunity)
            PrintAndLogError("Arbitrage - Maybe. But we had an EXCEPTION! Investigate this ASAP. No exceptions should occur here " + str(
                arbitrageOpportunity.GenerateDescriptionString()) + ", exception when setting gas limit and gas price in ArbitrageGeneric, " + traceback.format_exc())

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 5, examining arbitrage, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    string_arbitrageDetails = str(tradingTechnique.value)
    if pendingTradeTxList_tailgating:
        string_arbitrageDetails += ' ' + tailgatingTxHash

    for index, arbitrageOpportunity in enumerate(arbitrageOpportunityList):
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Iterating index " + str(index) + " " + str(arbitrageOpportunity.GenerateDescriptionString()))
        symbol_quoteToken = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(arbitrageOpportunity.quoteToken)
        symbolList_tradePath = []
        for token in arbitrageOpportunity.tradePath:
            symbolList_tradePath.append(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token).upper())

        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Arbitrage - Maybe (" + str(string_arbitrageDetails) + ") Meets stage1 requirements. " + str(
                                 arbitrageOpportunity.GenerateDescriptionString_NumOfTradeLegs()) + " trade, " + str(
                                 symbol_quoteToken) + "-" + str(symbolList_tradePath) + ", " + str(
                                 arbitrageOpportunity.GenerateDescriptionString()))
        try:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profit_quoteTokens = " + str(
                arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   minProfitRequirement_quoteTokens = " + str(
                arbitrageOpportunity.minProfitRequirement_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   arbitrageOpportunity.quoteToken = " + str(arbitrageOpportunity.quoteToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profitPercent = " + str(round(100 * arbitrageOpportunity.profitPercent, 4)) + " %")
            expectedGasUsage = arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   expectedGasUsage = " + str(expectedGasUsage))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice)) +
                                 " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice, expectedGasUsage)) + " ETH)")
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "   gasPrice_maxKeepAhead = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice_maxKeepAhead)) +
                                 " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice_maxKeepAhead, expectedGasUsage)) + " ETH)")
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "   gasPrice_maxGrimTrigger = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice_maxGrimTrigger)) +
                                 " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice_maxGrimTrigger, expectedGasUsage)) + " ETH)")
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   suggestedGasLimit = " + str(arbitrageOpportunity.GetSuggestedGasLimit_AllTradeActions()))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   doTradeWithinFlashLoan = " + str(arbitrageOpportunity.GetBool_DoTradeWithinFlashLoan()))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   tradePath = " + str(arbitrageOpportunity.tradePath))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   exchangeNamesList = " + str(arbitrageOpportunity.exchangeNamesList))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   quoteTokensList = " + str(arbitrageOpportunity.quoteTokensList))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   sidesList_wrtPricesDict = " + str(arbitrageOpportunity.sidesList_wrtPricesDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   pricesList_wrtPricesDict = " + str(arbitrageOpportunity.pricesList_wrtPricesDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   ratesList = " + str(arbitrageOpportunity.ratesList))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            # let's remove it from the arbitrageOpportunityList
            arbitrageOpportunityList.remove(arbitrageOpportunity)
            PrintAndLogError("Arbitrage - Maybe. But we had an EXCEPTION! "
                             "Most likely that bug where minProfitRequirement_quoteTokens is None and gasPrice is None. "
                             "Why does this happen?? Investigate this ASAP. No exceptions should occur here " +
                             str(arbitrageOpportunity.GenerateDescriptionString()) +
                             ", exception when setting gas limit and gas price in ArbitrageGeneric, " + traceback.format_exc())

    # Track arbitrageOpportunitys for each block so I can consider them later for trading logic
    Set_ArbitrageOpportunityListTo_ArbitrageOpportunitysPerBlockDict(blockNumber, arbitrageOpportunityList)

    if Libraries.defaults.VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug:
        PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList)

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 6.0, Limiting the max number of arbitrageOpportunitys, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    # This helps with performance, because there are times when Ninja will detect 1000+ arbitrageOpportunitys and that murders performance
    # maxSize = 50  # Performance was awful fine tuning 50 at the same time
    maxSize = 20  # Takes almost an entire second (calling DE nodes from US) with a full 20
    arbitrageOpportunityList = Libraries.utils.UpdateList_KeepOnlyTopNItemsInList(arbitrageOpportunityList, maxSize)
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "arbitrageOpportunityList now has " + str(len(arbitrageOpportunityList)) + " items")
    # TODO limiting the size here to only 20 is pretty small and could potentially block paths since 20 is such a small number
    #  possible solutions here:
    #    1.) A lot of these API calls i'm making inside  API_FineTuneTradeOpportunitys are redundant.
    #        For example, I'm probably checking the same UniswapV2 exchange balance for 10 different trades
    #        So I could gather the data required for making the API calls, then make only 1 API call for all arbitrageOpportunities that need it.
    #        This should dramatically increase performance here and allow me to get back to a 50 or 100 count
    #        I would do this in a generic fashion where I would say something like "I need to get the token balance of this address for this block"
    #        it would take subscriptions and then fill requests then somehow reply to the subscriber with the result
    #        I could use ZMQ for this
    #    2.) Say I want to target like a maxSize of 20. I could take the actual top 10, then let the other 10 be randomly selected
    #        This would be easily to implement and decrease the odds of something getting blocked for long periods of time
    #    3.) Similar to 2, say I want to target like maxSize of 20. Always take the top 10, but for the bottom 10 look at the paths and take the more unique looking ones
    #        So have an algorithm that says "USDC and WETH are in all of hte top 20, but there are 5 that have unique tokens in them, so let's include those 5 since they're unique
    #        How is this similar or different than a conflict set??
    #        Maybe I'm essentially running my conflict set code here but allowing a certain number of conflicts on the same conflicting path then stopping

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 6.1, Fine tune trade opportunitys, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    # Fine tune the list of trading opportunities, and remove any that fail during fine tuning
    arbitrageOpportunityList = API_FineTuneTradeOpportunitys(arbitrageOpportunityList, tradingTechnique,
                                                             tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating)
    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items that survived fine tuning")

    if Libraries.defaults.VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug:
        PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList)

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 7, old final, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")

    for index, arbitrageOpportunity in enumerate(arbitrageOpportunityList):
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Iterating index " + str(index) + " " + str(arbitrageOpportunity.GenerateDescriptionString()))
        symbol_quoteToken = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(arbitrageOpportunity.quoteToken)
        symbolList_tradePath = []
        for token in arbitrageOpportunity.tradePath:
            symbolList_tradePath.append(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token).upper())

        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Arbitrage - Yes (" + str(string_arbitrageDetails) + ") Meets stage2 requirements. " + str(
                                 arbitrageOpportunity.GenerateDescriptionString_NumOfTradeLegs()) + " trade, " + str(
                                 symbol_quoteToken) + "-" + str(symbolList_tradePath) + ", " + str(
                                 arbitrageOpportunity.GenerateDescriptionString()))
        try:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profit_quoteTokens = " + str(
                arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   minProfitRequirement_quoteTokens = " + str(
                arbitrageOpportunity.minProfitRequirement_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   arbitrageOpportunity.quoteToken = " + str(arbitrageOpportunity.quoteToken))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profitPercent = " + str(round(100 * arbitrageOpportunity.profitPercent, 4)) + " %")
            expectedGasUsage = arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   expectedGasUsage = " + str(expectedGasUsage))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice)) +
                                 " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice, expectedGasUsage)) + " ETH)")
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "   gasPrice_maxKeepAhead = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice_maxKeepAhead)) +
                                 " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice_maxKeepAhead, expectedGasUsage)) + " ETH)")
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "   gasPrice_maxGrimTrigger = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice_maxGrimTrigger)) +
                                 " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice_maxGrimTrigger, expectedGasUsage)) + " ETH)")
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   suggestedGasLimit = " + str(arbitrageOpportunity.GetSuggestedGasLimit_AllTradeActions()))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   doTradeWithinFlashLoan = " + str(arbitrageOpportunity.GetBool_DoTradeWithinFlashLoan()))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   tradePath = " + str(arbitrageOpportunity.tradePath))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   exchangeNamesList = " + str(arbitrageOpportunity.exchangeNamesList))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   tradePathIdentifiers = " + str(arbitrageOpportunity.GetTradePathIdentifiers(False)))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   quoteTokensList = " + str(arbitrageOpportunity.quoteTokensList))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   sidesList_wrtPricesDict = " + str(arbitrageOpportunity.sidesList_wrtPricesDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   pricesList_wrtPricesDict = " + str(arbitrageOpportunity.pricesList_wrtPricesDict))
            PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   ratesList = " + str(arbitrageOpportunity.ratesList))

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            # let's remove it from the arbitrageOpportunityList
            arbitrageOpportunityList.remove(arbitrageOpportunity)
            PrintAndLogError("Arbitrage - Maybe. But we had an EXCEPTION! "
                             "Most likely that bug where minProfitRequirement_quoteTokens is None and gasPrice is None. "
                             "Why does this happen?? Investigate this ASAP. No exceptions should occur here " +
                             str(arbitrageOpportunity.GenerateDescriptionString()) +
                             ", exception when setting gas limit and gas price in ArbitrageGeneric, " + traceback.format_exc())

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit")

    # TODO, should I limit arbitrageOpportunityList to a max size here???
    #  It may not be needed because i'm already limited by the number of NinjaOpAccounts anyways
    #  so I may as well let it try going forward with whatever it has

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 7.1, Verify we're still on the latest block, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    # Check if a new block has already been mined in
    latestBlockNumber = Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int()
    if latestBlockNumber > blockNumber:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "blockNumber " + str(blockNumber) + " is now old, a new block " +
                             str(latestBlockNumber) + " was discovered. I've stopped processing this old block")
        return

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "Step 7.2, Verify TxMinePredictions, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
    # Prepare a dict for communicating readySetGo state between this thread and the PursueTradeOpportunity threads
    readySetGoCommunicationDict = {}
    # use a timestamp to ensure the keys are unique to help when viewing logs since blocks can get mined seconds apart
    # timeForReadySetGoCommunicationDict = time.time()
    # After all this, we'll watch to match back up the key_readySetGoCommunicationDict with the arbitrageOpportunity
    # Let's create a dict to help with that
    # Keyed by key_readySetGoCommunicationDict, valued by arbitrageOpportunity
    arbitrageOpportunityReadySetGoKeyDict = {}

    # Call PursueTradeOpportunity for all trades left in arbitrageOpportunityList
    count_PursueTradeOpportunity = 0
    for index_readySetGoCommunicationDict, arbitrageOpportunity in enumerate(arbitrageOpportunityList.copy()):
        # Generate a unique key for this arbitrageOpportunity
        key_readySetGoCommunicationDict = str(header_logging) + '_' + str(index_readySetGoCommunicationDict)
        readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Ready
        # Set this for later
        arbitrageOpportunityReadySetGoKeyDict[key_readySetGoCommunicationDict] = arbitrageOpportunity

        try:
            # Call PursueTradeOpportunity only for the txMinePrediction result
            immediatelyReturnAfterTxMinePrediction = True
            PreparePursueTradeOpportunity(header_logging, functionsStartDateTime, arbitrageOpportunity,
                                          blockNumber, reservableEthereumAccountPool,
                                          gasPriceIsAllowedToIncreaseForFrontRunning, tradingTechnique,
                                          tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
                                          tailgatingTxHash, readySetGoCommunicationDict, key_readySetGoCommunicationDict,
                                          immediatelyReturnAfterTxMinePrediction)

            count_PursueTradeOpportunity += 1

        except (KeyboardInterrupt, SystemExit):
            print('\nkeyboard interrupt caught')
            print('\n...Program Stopped Manually!')
            raise

        except:
            PrintAndLogError("exception when attempting to PursueTradeOpportunity on " + str(arbitrageOpportunity.GenerateDescriptionString()) + ", " +
                             traceback.format_exc())

    if count_PursueTradeOpportunity <= 0:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Arbitrage - No. Exited ArbitrageGeneric without successfully calling arbitrageOpportunity.PursueTradeOpportunity()")
        return

    # Loop while waiting for a response from the PursueTradeOpportunity threads we started above
    # They should callback telling us whether or not their trades will succeed or fail
    # From there we can make better trading decisions
    # This loop should never hit the timeout consistently, if it does something went wrong and I need to fix it ASAP so we're not dragging to the timeout frequently
    timeout_seconds = 2.5
    elapsedTime_seconds = 0.0
    # TODO Set this correctly when done testing
    # sleepTime_seconds = 0.2
    sleepTime_seconds = 0.02
    while True:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Sleeping while I wait for the PursueTradeOpportunitys to call back with the eth_estimateGas and eth_call results")
        # TODO, if a new block is mined in, break
        # TODO, when PursueTradeOpportunitys callback, I need to make a decision on which to GO and which to EXIT
        #  When I tell a PursueTradeOpportunity to GO, it will ship the trade
        #  When I tell a PursueTradeOpportunity to EXIT, it will NOT execute the trade and will just exit
        #  Regardless of the outcome, afterwards I need to break from this loop so this function can return
        # for key in readySetGoCommunicationDict:
        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "readySetGoCommunicationDict = " + str(readySetGoCommunicationDict))

        # Check if we're ready to proceed with any trades
        # Possible outcomes for each arbitrageOpportunity:
        #   set (all set to continue trading)
        #   failed (exception, eth_estimateGas failed, eth_call failed)
        #   timeout (PursueTradeOpportunity took way too long, either we lost internet or it exited without replying to the readySetGoCommunicationDict which is very bad)

        # Determine how many arbitrageOpportunitys have concluded
        concludedCount = 0
        for key_readySetGoCommunicationDict in readySetGoCommunicationDict:
            if readySetGoCommunicationDict[key_readySetGoCommunicationDict] != ReadySetGoState.Ready:
                concludedCount += 1

        # If we have either reached timeout or each arbitrageOpportunity has concluded, we can exit the loop
        if concludedCount == len(readySetGoCommunicationDict):
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "all arbitrageOpportunitys in readySetGoCommunicationDict have concluded: " +
                                 str(concludedCount) + " out of " + str(len(readySetGoCommunicationDict)) +
                                 ". readySetGoCommunicationDict = " + str(readySetGoCommunicationDict))
            break

        # Check timeout
        if elapsedTime_seconds > timeout_seconds:
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "We've reached timeout in the ReadySetGo loop while waiting for responses from arbitrageOpportunitys. "
                                 "This should not happen. Am I calling PursueTradeOpportunity on too many arbitrageOpportunitys? "
                                 "Did I lose connectivity to my nodes?")
            break

        time.sleep(sleepTime_seconds)
        elapsedTime_seconds += sleepTime_seconds

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "readySetGoCommunicationDict = " + str(readySetGoCommunicationDict))

    # Check to see if a new block has mined in
    # if this is the case, we need to abandon these arbitrageOpportunitys
    if Libraries.core.GetThreadSafeCopyOf_LatestBlockNumber_int() > blockNumber:
        # Tell all arbitrageOpportunitys to exit because a new block has mined in
        for key_readySetGoCommunicationDict in readySetGoCommunicationDict:
            readySetGoCommunicationDict[key_readySetGoCommunicationDict] = ReadySetGoState.Fail

    else:
        # We want to tell the remaining arbitrageOpportunities GO, but before we can do that we need to handle path conflicts
        # Let's start by removing all the arbitrageOpportunitys that failed from the arbitrageOpportunityList

        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Step 8.0, removing arbitrageOpportunitys that failed eth_estimateGas or eth_call, "
                             "arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")

        for key_readySetGoCommunicationDict in readySetGoCommunicationDict:
            # Look up the arbitrageOpportunity using arbitrageOpportunityReadySetGoKeyDict
            arbitrageOpportunity = arbitrageOpportunityReadySetGoKeyDict[key_readySetGoCommunicationDict]
            # Anything not SET
            if readySetGoCommunicationDict[key_readySetGoCommunicationDict] != ReadySetGoState.Set:
                # It must have failed a txMinePrediction for some reason
                # Delete it from the arbitrageOpportunityList since we're not trading on it
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "Removing arbitrageOpportunity because its PursueTradeOpportunity thread did not reply with SET, "
                                     "it must have either failed an eth_estimateGas or eth_call or timedout. arbitrageOpportunity = " +
                                     str(arbitrageOpportunity.GenerateDescriptionString()))
                arbitrageOpportunityList.remove(arbitrageOpportunity)
            else:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "TxMinePrediction for arbitrageOpportunity succeeded via PursueTradeOpportunity. "
                                     "Keeping it in the arbitrageOpportunityList. arbitrageOpportunity = " + str(arbitrageOpportunity.GenerateDescriptionString()))

        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "We're left with " + str(len(arbitrageOpportunityList)) + " arbitrageOpportunitys, all of which successfully passed txMinePrediction")

        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Step 8.1, check trading path conflict sets with only arbitrageOpportunitys that can succeed, "
                             "arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")

        # Check for trading path conflict sets
        # I may have multiple arbitrageOpportunity that overlap in terms of exchanges and tokens they're trading
        # This is where I make a decision which arbitrageOpportunity to ship and which to skip
        # Make a nonConflictingUniqueProfitableTrades list
        # NOTE: This step is resource expensive. A lot of logic is processed here per arbitrageOpportunity so ideally i've
        # narrowed down arbitrageOpportunityList pretty far at this point so I'm not processing hundreds of arbitrageOpportunitys here
        nonConflictingUniqueProfitableTrades = []
        for arbitrageOpportunity in arbitrageOpportunityList:
            # If the list is currently empty
            if len(nonConflictingUniqueProfitableTrades) == 0:
                # Add it to the list
                nonConflictingUniqueProfitableTrades.append(arbitrageOpportunity)
            else:
                conflictFound = False
                # Iterate over the items already in the list and check them for conflicts
                for other_arbitrageOpportunity in nonConflictingUniqueProfitableTrades:
                    conflictingTradePathIdentifierStrings = Libraries.trade.GetArbitrageOpportunityConflicts(arbitrageOpportunity, other_arbitrageOpportunity)
                    if len(conflictingTradePathIdentifierStrings) > 0:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Found a conflict between the following trades")
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   " + str(
                            len(conflictingTradePathIdentifierStrings)) + " conflicts between a " + str(
                            arbitrageOpportunity.GetNumOfTradeLegs()) + "-leg trade and a " + str(
                            other_arbitrageOpportunity.GetNumOfTradeLegs()) + "-leg trade")
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   arbitrageOpportunity: " + str(arbitrageOpportunity.GenerateDescriptionString()))
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   other_arbitrageOpportunity: " + str(other_arbitrageOpportunity.GenerateDescriptionString()))
                        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   arbitrageOpportunity.metaDataPath: " + str(arbitrageOpportunity.metaDataPath))
                        # PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   other_arbitrageOpportunity.metaDataPath: " + str(other_arbitrageOpportunity.metaDataPath))
                        conflictFound = True
                        break

                # If there's no conflict with any items already in the list
                if not conflictFound:
                    # Add it to the list
                    nonConflictingUniqueProfitableTrades.append(arbitrageOpportunity)

        # Update arbitrageOpportunityList
        arbitrageOpportunityList = nonConflictingUniqueProfitableTrades
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Updated arbitrageOpportunityList based on nonConflictingUniqueProfitableTrades")

        if Libraries.defaults.VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug:
            PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList)

        # TODO I recently moved this, this used to be step 5.x, I moved this down here because it should be right before the end so it works properly. Is this correct??
        #  The idea is that if I do this at the very last second before shipping trades,
        #    I'll properly NOT trade on a second best trade path when the first best trade path is an extremely competitive trade path
        #    like ETH-Uniswap-USDC-Sushiswap-ETH which I do not want to compete on because of people grim triggering
        #  The problem with enforcing FinalStepTradeBanList early here is because I will end up bidding on the 2nd best
        #    when I mean to intentionally NOT bid on it (assuming FinalStepTradeBanList is set with something)
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Step 8.2, Checking FinalStepTradeBanList, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")
        if Libraries.defaults.FinalStepTradeBanList:
            try:
                for trade in Libraries.defaults.FinalStepTradeBanList:
                    # if trade properties set to None means we don't care what the value is
                    if Libraries.defaults.VerboseLogging_ArbitrageGeneric:
                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                             "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit")

                    for arbitrageOpportunity in arbitrageOpportunityList.copy():
                        if arbitrageOpportunity.quoteToken.lower() == trade.quoteToken.lower():
                            numOfTradeLegs = arbitrageOpportunity.GetNumOfTradeLegs()
                            doTradeWithinFlashLoan = arbitrageOpportunity.GetBool_DoTradeWithinFlashLoan()
                            # if numOfTradeLegs matches
                            if not trade.numOfTradeLegs or trade.numOfTradeLegs == numOfTradeLegs:
                                # if doTradeWithinFlashLoan matches
                                if not trade.tradedWithinFlashLoan or trade.tradedWithinFlashLoan == doTradeWithinFlashLoan:
                                    # if exchangeName_toExclude matches
                                    if not trade.exchangeName_toExclude or trade.exchangeName_toExclude not in arbitrageOpportunity.exchangeNamesList:
                                        # let's remove it from the arbitrageOpportunityList
                                        arbitrageOpportunityList.remove(arbitrageOpportunity)
                                        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                                             "Not considering trading on " + str(arbitrageOpportunity.GenerateDescriptionString()) +
                                                             " because FinalStepTradeBanList is turned on and this arbitrageOpportunity fits banned tradeProperties "
                                                             "FinalStepTradeBanList = " + str(Libraries.defaults.FinalStepTradeBanList) + ", arbitrageOpportunity.quoteToken = " +
                                                             str(arbitrageOpportunity.quoteToken) + ", numOfTradeLegs = " + str(numOfTradeLegs) +
                                                             ", doTradeWithinFlashLoan = " + str(doTradeWithinFlashLoan))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                PrintAndLogError("exception during FinalStepTradeBanList, " + traceback.format_exc())

        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Step 9, preparing GO signals, arbitrageOpportunityList has " + str(len(arbitrageOpportunityList)) + " items")

        for index, arbitrageOpportunity in enumerate(arbitrageOpportunityList):
            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Iterating index " + str(index) + " " + str(arbitrageOpportunity.GenerateDescriptionString()))
            symbol_quoteToken = Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(arbitrageOpportunity.quoteToken)
            symbolList_tradePath = []
            for token in arbitrageOpportunity.tradePath:
                symbolList_tradePath.append(Libraries.cache.GetSymbolForTokenContract_UseCachingSystem(token).upper())

            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Arbitrage - Yes (" + str(string_arbitrageDetails) + ") Meets stage3 requirements. " + str(
                                     arbitrageOpportunity.GenerateDescriptionString_NumOfTradeLegs()) + " trade, " + str(
                                     symbol_quoteToken) + "-" + str(symbolList_tradePath) + ", " + str(
                                     arbitrageOpportunity.GenerateDescriptionString()))
            try:
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profit_quoteTokens = " + str(
                    arbitrageOpportunity.profit_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   minProfitRequirement_quoteTokens = " + str(
                    arbitrageOpportunity.minProfitRequirement_quoteTokens) + " " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   arbitrageOpportunity.quoteToken = " + str(arbitrageOpportunity.quoteToken))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   profitPercent = " + str(round(100 * arbitrageOpportunity.profitPercent, 4)) + " %")
                expectedGasUsage = arbitrageOpportunity.GetExpectedGasUsage_AllTradeActions()
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   expectedGasUsage = " + str(expectedGasUsage))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   gasPrice = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice)) +
                                     " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice, expectedGasUsage)) + " ETH)")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   gasPrice_maxKeepAhead = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice_maxKeepAhead)) +
                                     " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice_maxKeepAhead, expectedGasUsage)) + " ETH)")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                     "   gasPrice_maxGrimTrigger = " + str(Libraries.core.ConvertWeiToGwei(arbitrageOpportunity.gasPrice_maxGrimTrigger)) +
                                     " gwei (" + str(Libraries.core.GetGasCost(arbitrageOpportunity.gasPrice_maxGrimTrigger, expectedGasUsage)) + " ETH)")
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   suggestedGasLimit = " + str(arbitrageOpportunity.GetSuggestedGasLimit_AllTradeActions()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   doTradeWithinFlashLoan = " + str(arbitrageOpportunity.GetBool_DoTradeWithinFlashLoan()))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   tradePath = " + str(arbitrageOpportunity.tradePath))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   exchangeNamesList = " + str(arbitrageOpportunity.exchangeNamesList))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   tradePathIdentifiers = " + str(arbitrageOpportunity.GetTradePathIdentifiers(False)))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   quoteTokensList = " + str(arbitrageOpportunity.quoteTokensList))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   sidesList_wrtPricesDict = " + str(arbitrageOpportunity.sidesList_wrtPricesDict))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   pricesList_wrtPricesDict = " + str(arbitrageOpportunity.pricesList_wrtPricesDict))
                PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   ratesList = " + str(arbitrageOpportunity.ratesList))

            except (KeyboardInterrupt, SystemExit):
                print('\nkeyboard interrupt caught')
                print('\n...Program Stopped Manually!')
                raise

            except:
                # let's remove it from the arbitrageOpportunityList
                arbitrageOpportunityList.remove(arbitrageOpportunity)
                PrintAndLogError("Arbitrage - Maybe. But we had an EXCEPTION! "
                                 "Most likely that bug where minProfitRequirement_quoteTokens is None and gasPrice is None. "
                                 "Why does this happen?? Investigate this ASAP. No exceptions should occur here " +
                                 str(arbitrageOpportunity.GenerateDescriptionString()) +
                                 ", exception when setting gas limit and gas price in ArbitrageGeneric, " + traceback.format_exc())

        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit")

        # Now that arbitrageOpportunityList also respects trading path conflicts
        # we can make final trade decisions
        # Anything left in arbitrageOpportunityList is a GO
        # Anything still in readySetGoCommunicationDict but NOT in arbitrageOpportunityList must has failed
        for key_readySetGoCommunicationDict in readySetGoCommunicationDict.copy():
            # Look up the arbitrageOpportunity using arbitrageOpportunityReadySetGoKeyDict
            arbitrageOpportunity = arbitrageOpportunityReadySetGoKeyDict[key_readySetGoCommunicationDict]
            if arbitrageOpportunity in arbitrageOpportunityList:
                # This one passed the above conflict test
                # It's a GO to trade, we must again call PursueTradeOpportunity this time telling it to continue with the trade loop

                try:
                    # Call PursueTradeOpportunity and execute the trade
                    immediatelyReturnAfterTxMinePrediction = False
                    PreparePursueTradeOpportunity(header_logging, functionsStartDateTime, arbitrageOpportunity,
                                                  blockNumber, reservableEthereumAccountPool,
                                                  gasPriceIsAllowedToIncreaseForFrontRunning, tradingTechnique,
                                                  tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
                                                  tailgatingTxHash, readySetGoCommunicationDict, key_readySetGoCommunicationDict,
                                                  immediatelyReturnAfterTxMinePrediction)

                except (KeyboardInterrupt, SystemExit):
                    print('\nkeyboard interrupt caught')
                    print('\n...Program Stopped Manually!')
                    raise

                except:
                    PrintAndLogError("exception when attempting to PursueTradeOpportunity on " + str(arbitrageOpportunity.GenerateDescriptionString()) + ", " +
                                     traceback.format_exc())

        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit with conflicts removed")
        if Libraries.defaults.VerboseLogging_PeriodicallyCallThisDuringArbitrageCalculations_PrintArbitrageOpportunityListForDebug:
            PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList)


def PreparePursueTradeOpportunity(header_logging, functionsStartDateTime, arbitrageOpportunity,
                                  blockNumber, reservableEthereumAccountPool,
                                  gasPriceIsAllowedToIncreaseForFrontRunning, tradingTechnique,
                                  tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
                                  tailgatingTxHash, readySetGoCommunicationDict, key_readySetGoCommunicationDict,
                                  onlyPerformTxMinePrediction):
    uniqueString = arbitrageOpportunity.GenerateDescriptionString()
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   " + str(uniqueString))

    # TODO, I think this SetMyPath_PursueTradeOpportunityPathsDict logic may have a flaw, Need to investigate

    success_SetMyPath_PursueTradeOpportunityPathsDict = False
    # if we're just interested in the txMinePrediction
    if onlyPerformTxMinePrediction:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Do not call arbitrageOpportunity.SetMyPath_PursueTradeOpportunityPathsDict")
        pass
    else:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                             "Since we're actually going to trade, we want to mark the path via arbitrageOpportunity.SetMyPath_PursueTradeOpportunityPathsDict")
        success_SetMyPath_PursueTradeOpportunityPathsDict = arbitrageOpportunity.SetMyPath_PursueTradeOpportunityPathsDict()

    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "onlyPerformTxMinePrediction = " + str(onlyPerformTxMinePrediction))
    PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                         "success_SetMyPath_PursueTradeOpportunityPathsDict = " + str(success_SetMyPath_PursueTradeOpportunityPathsDict))

    if onlyPerformTxMinePrediction or success_SetMyPath_PursueTradeOpportunityPathsDict:
        # If we are only interested in the txMinPrediction, then we also want to return immediately
        immediatelyReturnAfterTxMinePrediction = onlyPerformTxMinePrediction

        PrintAndLog_Detailed(
            header_logging, functionsStartDateTime,
            "success_SetMyPath_PursueTradeOpportunityPathsDict = " + str(success_SetMyPath_PursueTradeOpportunityPathsDict) +
            ", Calling PursueTradeOpportunity because the trade loop hasn't been started yet for this path: " + str(uniqueString))

        numOfTimesToCallPursueTradeOpportunity = None
        if tradingTechnique == TradingTechnique.Arbitrage:
            numOfTimesToCallPursueTradeOpportunity = 1
        elif tradingTechnique == TradingTechnique.Tailgate:
            numOfTimesToCallPursueTradeOpportunity = arbitrageOpportunity.DetermineHowManyTailgatingTradesToQueue()
        else:
            numOfTimesToCallPursueTradeOpportunity = 1
            message = "tradingTechnique " + str(tradingTechnique) + " not yet implemented"
            Libraries.core.API_PostOperatorNotification(message)
            PrintAndLogError(message)

        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "numOfTimesToCallPursueTradeOpportunity = " + str(numOfTimesToCallPursueTradeOpportunity))
        # # We can only reserve as many accounts as we have
        # numOfTimesToCallPursueTradeOpportunity = min(numOfTimesToCallPursueTradeOpportunity, reservableEthereumAccountPool.GetSumOfTotalAccounts())
        # PrintAndLog_Detailed(header_logging, functionsStartDateTime,
        #                      "numOfTimesToCallPursueTradeOpportunity after enforcing max available ninjaOps, numOfTimesToCallPursueTradeOpportunity = " + str(
        #                          numOfTimesToCallPursueTradeOpportunity) + ", reservableEthereumAccountPool.GetSumOfTotalAccounts() = " + str(
        #                          reservableEthereumAccountPool.GetSumOfTotalAccounts()))
        for i in range(numOfTimesToCallPursueTradeOpportunity):
            stringToAppendToReserveAccountName = ''
            if tradingTechnique == TradingTechnique.Tailgate:
                stringToAppendToReserveAccountName = str(i)

            doSubtract1WeiFromGasPrice = False
            # If numOfTimesToCallPursueTradeOpportunity has at least several, and this is our last index
            if tradingTechnique == TradingTechnique.Tailgate and \
                    numOfTimesToCallPursueTradeOpportunity > 2 and \
                    i == numOfTimesToCallPursueTradeOpportunity - 1:
                # Set the doSubtract1WeiFromGasPrice flag so we also issue one with gasPrice - 1 wei.
                doSubtract1WeiFromGasPrice = True

            PrintAndLog_Detailed(header_logging, functionsStartDateTime,
                                 "Calling arbitrageOpportunity.PursueTradeOpportunity index " + str(i) + " for " + str(
                                     tradingTechnique.value) + ",  doSubtract1WeiFromGasPrice = " + str(doSubtract1WeiFromGasPrice))

            reservableEthereumAccountPool_toUse = reservableEthereumAccountPool
            if immediatelyReturnAfterTxMinePrediction:
                # If we're not actually executing the trade and we're only calling it for the txMinePrediction
                # then pass in null here so it doesn't reserve a ninjaOpAccount
                reservableEthereumAccountPool_toUse = None

            # Call this using IsTimeToExecute so it generates a log file specifically for this output
            IsTimeToExecute("PursueTradeOpportunity", 0, arbitrageOpportunity.PursueTradeOpportunity, True, True, True,
                            header_logging, functionsStartDateTime, blockNumber, reservableEthereumAccountPool_toUse,
                            gasPriceIsAllowedToIncreaseForFrontRunning, tradingTechnique,
                            tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
                            tailgatingTxHash, stringToAppendToReserveAccountName, doSubtract1WeiFromGasPrice,
                            readySetGoCommunicationDict, key_readySetGoCommunicationDict)

    else:
        message = "success_SetMyPath_PursueTradeOpportunityPathsDict = " + str(success_SetMyPath_PursueTradeOpportunityPathsDict) + \
                  "NOT calling PursueTradeOpportunity because the trade loop has already been started for this path: " + str(uniqueString)
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, message)
        Libraries.core.API_PostOperatorNotification(message)


def API_PrepareFineTuneTradeOpportunitys(arbitrageOpportunityList):
    before = datetime.datetime.now()
    # Make a dict containing all the unique API calls we need to make
    # key = the hash
    # value = a tuple of information we need to make the API call, like functionCall, functionArguments, etc
    apiCallsToMakeDict = {}
    # This is how we relate the arbitrageOpportunity with the corresponding hash
    # key = arbitrageOpportunity
    # value = another dict
    arbitrageOpportunityHashDict = {}
    for arbitrageOpportunity in arbitrageOpportunityList:
        # this inner dict is needed so we can nest data
        # key = pathIndex
        # value = hash
        arbitrageOpportunityHashDict[arbitrageOpportunity] = {}
        for pathIndex, exchangeName in enumerate(arbitrageOpportunity.exchangeNamesList):
            functionCall, functionArguments, hash = ExchangeDict[exchangeName].PrepareRateFormulaMetaData(arbitrageOpportunity, pathIndex)
            PrintAndLog_FuncNameHeader("hash = " + str(hash))
            if not functionCall:
                PrintAndLog_FuncNameHeader("skipping this one because " + str(exchangeName) + " returned None")
                continue

            # Correspond the arbitrageOpportunity with the hash so we can look this up later
            arbitrageOpportunityHashDict[arbitrageOpportunity][pathIndex] = hash
            # Set only unique API calls in the dict
            # This will ensure we aren't making the same exact API calls many times
            # And when we make one API call, the result can be sent to multiple subscribers who need that same result
            if hash not in apiCallsToMakeDict:
                apiCallsToMakeDict[hash] = (functionCall, functionArguments)

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Gathered data for API calls in " + str(duration_s) + " seconds")
    # PrintAndLog_FuncNameHeader("apiCallsToMakeDict = " + str(apiCallsToMakeDict))
    # PrintAndLog_FuncNameHeader("arbitrageOpportunityHashDict = " + str(arbitrageOpportunityHashDict))

    # Make API calls on everything unique in the dict
    threads = []
    apiCallResultsDict = {}
    for hash in apiCallsToMakeDict:
        functionCall, functionArguments = apiCallsToMakeDict[hash]

        # Libraries.utils.ExecuteGenericFunctionCall(apiCallResultsDict, key, functionCall, functionArguments)

        t = threading.Thread(
            target=Libraries.utils.ExecuteGenericFunctionCall,
            args=(apiCallResultsDict, hash, functionCall, functionArguments))
        threads.append(t)
        t.start()

    for thread in threads:
        thread.join()

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Completed in " + str(duration_s) + " seconds")
    # PrintAndLog_FuncNameHeader("apiCallResultsDict = " + str(apiCallResultsDict))
    return apiCallResultsDict, arbitrageOpportunityHashDict


def API_FineTuneTradeOpportunitys(arbitrageOpportunityList, tradingTechnique,
                                  tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):

    before = datetime.datetime.now()
    apiCallResultsDict, arbitrageOpportunityHashDict = API_PrepareFineTuneTradeOpportunitys(arbitrageOpportunityList)

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Step 1 in " + str(duration_s) + " seconds")

    threads = []
    resultsDict = {}

    # Call API_FineTuneTradeOpportunity for every single arbitrageOpportunity
    # Do so such that logs are clean and maintainable
    for key_index, arbitrageOpportunity in enumerate(arbitrageOpportunityList):
        if arbitrageOpportunity not in arbitrageOpportunityHashDict:
            PrintAndLog_FuncNameHeader("arbitrageOpportunity's result was not found in the apiCallResultsDict or arbitrageOpportunityHashDict. "
                                       "arbitrageOpportunity = " + str(arbitrageOpportunity.GenerateDescriptionString()))
            continue
        else:
            # Get the result for the API call using the hash
            arbitrageOpportunityPathIndexHashDict = arbitrageOpportunityHashDict[arbitrageOpportunity]
            # Create a dict containing all API call results
            # key = pathIndex of the arbitrageOpportunity
            # value = resultForApiCall
            resultForApiCallDict = {}
            for pathIndex in arbitrageOpportunityPathIndexHashDict:
                hash = arbitrageOpportunityPathIndexHashDict[pathIndex]
                resultForApiCall = apiCallResultsDict[hash]
                resultForApiCallDict[pathIndex] = resultForApiCall

            PrintAndLog_FuncNameHeader("Calling arbitrageOpportunity.API_FineTuneTradeOpportunity with resultForApiCallDict = " + str(resultForApiCallDict))

            # Call this using IsTimeToExecute so it generates a log file specifically for this output
            didExecute, t = IsTimeToExecute_ReturnThread("FineTuneTradeOpportunity", 0, arbitrageOpportunity.API_FineTuneTradeOpportunity, True, True,
                                                         tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
                                                         resultForApiCallDict, resultsDict, str(key_index))

            if not didExecute:
                raise Exception("API_FineTuneTradeOpportunity failed to execute, this should never happen. Did I configure this call incorrectly?")

            if not t:
                raise Exception("No thread was returned when calling API_FineTuneTradeOpportunity. Did I configure this call incorrectly?")

            # Get the thread that was started and track it so I know when it completes
            threads.append(t)

    for thread in threads:
        thread.join()

    PrintAndLog_FuncNameHeader("resultsDict = " + str(resultsDict))




    # resultsDict = {}
    # for key_index, arbitrageOpportunity in enumerate(arbitrageOpportunityList):
    #     try:
    #         if arbitrageOpportunity not in arbitrageOpportunityHashDict:
    #             PrintAndLog_FuncNameHeader("arbitrageOpportunity's result was not found in the apiCallResultsDict or arbitrageOpportunityHashDict. "
    #                                        "arbitrageOpportunity = " + str(arbitrageOpportunity.GenerateDescriptionString()))
    #             continue
    #         else:
    #             # Get the result for the API call using the hash
    #             arbitrageOpportunityPathIndexHashDict = arbitrageOpportunityHashDict[arbitrageOpportunity]
    #             # Create a dict containing all API call results
    #             # key = pathIndex of the arbitrageOpportunity
    #             # value = resultForApiCall
    #             resultForApiCallDict = {}
    #             for pathIndex in arbitrageOpportunityPathIndexHashDict:
    #                 hash = arbitrageOpportunityPathIndexHashDict[pathIndex]
    #                 resultForApiCall = apiCallResultsDict[hash]
    #                 resultForApiCallDict[pathIndex] = resultForApiCall
    #
    #             PrintAndLog_FuncNameHeader("Calling arbitrageOpportunity.API_FineTuneTradeOpportunity with resultForApiCallDict = " + str(resultForApiCallDict))
    #
    #             # Call API_FineTuneTradeOpportunity and tell it the result of the call it requested
    #             arbitrageOpportunity.API_FineTuneTradeOpportunity(
    #                 tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
    #                 resultForApiCallDict, resultsDict, str(key_index))
    #
    #     except (KeyboardInterrupt, SystemExit):
    #         print('\nkeyboard interrupt caught')
    #         print('\n...Program Stopped Manually!')
    #         raise
    #
    #     except:
    #         PrintAndLogError("exception when fine tuning arbitrageOpportunity " +
    #                          str(arbitrageOpportunity.GenerateDescriptionString()) + ", exception =  " + traceback.format_exc())

        # duration_s = (datetime.datetime.now() - before).total_seconds()
        # stepNum = "1." + str(key_index)
        # PrintAndLog_FuncNameHeader("Step " + str(stepNum) + " in " + str(duration_s) + " seconds")

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Step 2 in " + str(duration_s) + " seconds")

    # TODO do these exceptions and failures still handle correct?
    new_arbitrageOpportunityList = []
    for key_index, arbitrageOpportunity in enumerate(arbitrageOpportunityList):
        # If no result is found for this arbitrageOpportunity, we assume it failed
        if str(key_index) in resultsDict:
            PrintAndLog_FuncNameHeader("Found result for key_index = " + str(key_index))
            new_arbitrageOpportunityList.append(arbitrageOpportunity)

    duration_s = (datetime.datetime.now() - before).total_seconds()
    PrintAndLog_FuncNameHeader("Step 3 in " + str(duration_s) + " seconds")

    PrintAndLog_FuncNameHeader("Found " + str(len(new_arbitrageOpportunityList)) + " out of " + str(len(arbitrageOpportunityList)) + " results")
    return new_arbitrageOpportunityList


# def API_FineTuneTradeOpportunitys(arbitrageOpportunityList, tradingTechnique,
#                                   tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating):
#     threads = []
#     resultsDict = {}
#
#     # Call API_FineTuneTradeOpportunity for every single arbitrageOpportunity
#     # Do so such that logs are clean and maintainable
#     for key_index, arbitrageOpportunity in enumerate(arbitrageOpportunityList):
#         # TODO, Logs will be an absolute mess here, should I make a log file for each individual arbitrageOpportunity here?
#         #  Or make like 100 log files and number them 1 through 100? But that's a mess too
#         #  Or just make sure to prepend the opportunity path to the beginning of the logs
#         #  If I do the ExecuteOnInterval call here I could create a new log file just for these logs and spam that log file with this stuff
#         #  That way it's a mess but all in one place
#         # t = threading.Thread(
#         #     target=arbitrageOpportunity.API_FineTuneTradeOpportunity,
#         #     args=(tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating, resultsDict, str(key_index)))
#         # threads.append(t)
#         # t.start()
#
#         # Call this using IsTimeToExecute so it generates a log file specifically for this output
#         didExecute, t = IsTimeToExecute_ReturnThread("FineTuneTradeOpportunity", 0, arbitrageOpportunity.API_FineTuneTradeOpportunity, True, True,
#                                                      tradingTechnique, tailgatingIsOnArbitrageOpportunityPath_corresponding_pendingTradeTx_tailgating,
#                                                      resultsDict, str(key_index))
#         if not didExecute:
#             raise Exception("API_FineTuneTradeOpportunity failed to execute, this should never happen. Did I configure this call incorrectly?")
#
#         if not t:
#             raise Exception("No thread was returned when calling API_FineTuneTradeOpportunity. Did I configure this call incorrectly?")
#
#         # Get the thread that was started and track it so I know when it completes
#         threads.append(t)
#
#     for thread in threads:
#         thread.join()
#
#     PrintAndLog_FuncNameHeader("resultsDict = " + str(resultsDict))
#
#     new_arbitrageOpportunityList = []
#     for key_index, arbitrageOpportunity in enumerate(arbitrageOpportunityList):
#         # If no result is found for this arbitrageOpportunity, we assume it failed
#         if str(key_index) in resultsDict:
#             PrintAndLog_FuncNameHeader("Found result for key_index = " + str(key_index))
#             new_arbitrageOpportunityList.append(arbitrageOpportunity)
#
#     PrintAndLog_FuncNameHeader("Found " + str(len(new_arbitrageOpportunityList)) + " out of " + str(len(arbitrageOpportunityList)) + " results")
#     return new_arbitrageOpportunityList


def PrintArbitrageOpportunityListForDebug(header_logging, functionsStartDateTime, arbitrageOpportunityList):
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Iterating over arbitrageOpportunityList with " + str(len(arbitrageOpportunityList)) + " keys")
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "arbitrageOpportunityList = " + str(arbitrageOpportunityList))
    PrintAndLog_Detailed(header_logging, functionsStartDateTime, "Found " + str(len(arbitrageOpportunityList)) + " trading opportunities sorted by profit")

    for arbitrageOpportunity in arbitrageOpportunityList:
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   arbitrageOpportunity profit_quoteTokens = " + str(
            arbitrageOpportunity.profit_quoteTokens) + ", profitPercent = " + str(
            arbitrageOpportunity.profitPercent) + ", quoteTokenAmount = " + str(arbitrageOpportunity.quoteTokenAmount))
        PrintAndLog_Detailed(header_logging, functionsStartDateTime, "   " + str(arbitrageOpportunity.GenerateDescriptionString()))


def SetPath_PursueTradeOpportunityPathsDict(pathString):
    global Lock_PursueTradeOpportunityPathsDict
    global PursueTradeOpportunityPathsDict

    successful = False
    PrintAndLog_FuncNameHeader("pathString = " + str(pathString))
    Lock_PursueTradeOpportunityPathsDict.acquire()
    try:
        # Mark this path as Trading
        PursueTradeOpportunityPathsDict.SetValue(pathString, PursueTradeOpportunityPathCommand.Trading)
        successful = True

    finally:
        Lock_PursueTradeOpportunityPathsDict.release()

    PrintAndLog_FuncNameHeader("PursueTradeOpportunityPathsDict of len " + str(len(PursueTradeOpportunityPathsDict.GetKeys())) + " = " + str(PursueTradeOpportunityPathsDict))
    return successful


# def FlagPathToBeCanceled_PursueTradeOpportunityPathsDict(pathString):
#     global Lock_PursueTradeOpportunityPathsDict
#     global PursueTradeOpportunityPathsDict
#
#     PrintAndLog_FuncNameHeader("pathString = " + str(pathString))
#     Lock_PursueTradeOpportunityPathsDict.acquire()
#     try:
#         # Flag this path to be canceled
#         PursueTradeOpportunityPathsDict.SetValue(pathString, PursueTradeOpportunityPathCommand.CancelASAP)
#
#     finally:
#         Lock_PursueTradeOpportunityPathsDict.release()
#
#     PrintAndLog_FuncNameHeader("PursueTradeOpportunityPathsDict of len " + str(len(PursueTradeOpportunityPathsDict.GetKeys())) + " = " + str(PursueTradeOpportunityPathsDict))


def HasPathsTradeBeenSignaledToCancelTheTransaction_PursueTradeOpportunityPathsDict(pathString):
    global Lock_PursueTradeOpportunityPathsDict
    global PursueTradeOpportunityPathsDict

    # Don't bother locking/acquiring here, it's just not necessary until we're actually modifying the value of the dictionary

    if pathString not in PursueTradeOpportunityPathsDict.GetKeys():
        return False
        # raise Exception("pathString not found in PursueTradeOpportunityPathsDict, pathString = " + str(pathString))

    if PursueTradeOpportunityPathsDict.GetValue(pathString)[0] == PursueTradeOpportunityPathCommand.CancelASAP:
        return True
    else:
        return False


def RemovePath_PursueTradeOpportunityPathsDict(pathStrings):
    global Lock_PursueTradeOpportunityPathsDict
    global PursueTradeOpportunityPathsDict

    PrintAndLog_FuncNameHeader("pathStrings = " + str(pathStrings))
    Lock_PursueTradeOpportunityPathsDict.acquire()
    try:
        for pathString in pathStrings:
            # Delete path
            PursueTradeOpportunityPathsDict.RemoveKeyFromDict(pathString)

    finally:
        Lock_PursueTradeOpportunityPathsDict.release()

    PrintAndLog_FuncNameHeader("PursueTradeOpportunityPathsDict after removing of len " + str(
        len(PursueTradeOpportunityPathsDict.GetKeys())) + " = " + str(PursueTradeOpportunityPathsDict))


def CleanUpOldPaths_PursueTradeOpportunityPathsDict():
    global Lock_PursueTradeOpportunityPathsDict
    global PursueTradeOpportunityPathsDict

    pathsToRemove = []
    Lock_PursueTradeOpportunityPathsDict.acquire()
    try:
        for path in PursueTradeOpportunityPathsDict.GetKeys():
            dataAge_seconds = PursueTradeOpportunityPathsDict.GetValue(path)[1]
            # If the data is old
            if dataAge_seconds > 600:
                # Remove it
                pathsToRemove.append(path)

    finally:
        Lock_PursueTradeOpportunityPathsDict.release()

    if len(pathsToRemove) > 0:
        message = "CleanUpOldPaths_PursueTradeOpportunityPathsDict: pathsToRemove has contents! " \
                  "Why did this not get removed, did a transaction not get mined in or do I have a bug? pathsToRemove of " \
                  "len " + str(len(pathsToRemove)) + " = " + str(pathsToRemove)
        Libraries.core.API_PostOperatorNotification(message)

    RemovePath_PursueTradeOpportunityPathsDict(pathsToRemove)
